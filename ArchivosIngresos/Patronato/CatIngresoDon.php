<?php
	session_start();
	include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/IngresosDonativoDaoJdbc.class.php");
	include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/IngresosDonativo.class.php");
	if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    	header("Location: " . $_SESSION['RAIZ'] . "/index.php?opc=2");
	}
	$respuesta = "";
	if(isset($_POST['guardar'])){
		$elemento = new IngresosDonativo();
		$elemento->setNom_patrocinador($_POST['patrocinador']);
		$elemento->setMonto($_POST['monto']);
		$elemento->setTipo_donacion($_POST['tipo_donacion']);
		$elemento->setArea($_POST['area']);
		$elemento->setPeriodo($_POST['periodo']);
		$elemento->setProyecto_destino($_POST['proyecto']);
		//checamos si el proyecto es una exposicion temporal
		$elemento->setExpo_temp($_POST['expotemp']);
		$elemento->setDescripcion($_POST['descripcion']);
		$elemento->setStatus(1);
		//Para subir el archivo en caso de que se haya agregado.
		if(!is_null($_FILES['archivo']) && $_FILES['archivo']['tmp_name'] != ""){
			$carpeta = "/ArchivosIngresos/Patronato/";
			$nomArchivo = $_FILES['archivo']['name'];
			opendir($_SESSION['RAIZ'] . $carpeta);
			$destino = ".." . $carpeta . $nomArchivo;
			copy($_FILES['archivo']['tmp_name'], $_SESSION['RAIZ'] . $carpeta . $nomArchivo);
			$elemento->setDocumento($destino);	
		}
		if(isset($_POST['id_donativo'])){
			$elemento->setId_donativo($_POST['id_donativo']);
			$dao = new IngresosDonativoDaoJdbc();
			$res = $dao->modificaElemento($elemento);
			if($res) $respuesta = "Registro modificado con éxito.";	
			else $respuesta = "El registro no pudo modificarse con éxito.";
		}else{
			$dao = new IngresosDonativoDaoJdbc();
			$res = $dao->guardaElemento($elemento);
			if($res) $respuesta = "Éxito al guardar el ingreso.";
			else $respuesta = "Error al guardar el ingreso. Inténtelo más tarde"; 
			//echo($res);
		}
	}else if(isset($_GET['id_donativo'])){
		$dao = new IngresosDonativoDaoJdbc();
		$elemento = new IngresosDonativo();
		$elemento = $dao->obtieneElemento($_GET['id_donativo']);
		$res = $dao->eliminaElemento($elemento);
		if($res) $respuesta = "Registro borrado con éxito.";	
	}
	
	header("Location: ../../../../../donativos_palacio/lista_donativospalacio.php?respuesta=".$respuesta);
?>