﻿1.-Versiones de presupuesto: 
 1.1 Presupuesto general del Museo  --
 1.2 Como anteproyecto en INBA
 1.3 Presupuesto de INBA con autorización
 1.4 Presupuesto patronato
 1.5 Presupuesto en donativos 
 1.5.1 Presupuestos por especie (por museo)
 1.5.1 Presupuestos por especie efectivo (por museo)
 1.6 Presupuesto con Instituciones  (Cineteca)
 
2.- Gestión de presupuesto 
 2.1 Avalar proceso del sistema en la forma de gestionar el presupuesto
 2.2 Cartas de deducibles por parte del Patronato  ( recibo de donativo)

3.-Indicadores 
 3.1 Mostrar lista ya generada con la dirección y sistemas
 3.2 Número de cotizaciones reales
 3.3 Número de cotizaciones con estimaciones 
 3.4 Versiones  de cotizaciones
 3.5 Número de Actividades con cotizaciones
 3.5.1 Costo de cada una de las actividades (evento para INBA )
 3.6 Entregables por persona 
 3.7 Número de patrocinadores  por parte de Patronato  reflejado en cantidades monetarias
 3.8 Número de patrocinadores  por parte de las áreas del Museo reflejado en cantidades monetarias
 3.9 Número de entregables de cotizaciones contrato previo (cap 3000 honorarios someter a comite, adjudicación directa )
 3.10 Número de entregables por persona (Matriz de proyectos / exposiciones)

-- via INBA
Indicadores tiempo de gestion entre control presupuestal y recursos financieros del centro de trabajo - y Prisma
programar fechas de pago 
 
4.-Actividades
 4.1 Revisión de actividades para carga de presupuesto y gestión del mismo

---
indicadores de  calendario  y ministraciones 