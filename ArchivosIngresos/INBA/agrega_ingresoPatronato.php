<?php
session_start();
include_once("../src/mx/com/virreinato/dao/IngresosPatronatoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/IngresosPatronato.class.php");
include_once("../src/mx/com/virreinato/dao/LineaAccionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/LineaAccion.class.php");
include_once("../src/mx/com/virreinato/dao/ExposicionesTemporalesDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ExposicionesTemporales.class.php");
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");

if(isset($_GET['id'])){
	$dao = new IngresosPatronatoDaoJdbc();
	$IngPat = new IngresosPatronato();
	$IngPat = $dao->obtieneElemento($_GET['id']);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ingreso Patronato</title>
</head>
<script>
	
	function mostrarExpoTemp()
	{
		var id = $("#proyecto").val();
		if(id == 48){
			$("#expotemp").show();	
		}else{
			$("#expotemp").hide();
		}		
	}
</script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<body onload="mostrarExpoTemp()">
	<div class="contenido"> 
        <p class="titulo_cat1">Menú de Catálogos &gt; Menú de Ingresos del Patronato > Nuevo Ingreso</p>
        <form name="ingresoP" id="ingresoP" method="post" action="../src/mx/com/virreinato/web/CatIngresoPat.php" enctype="multipart/form-data">
            <table width="55%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
            	<tr>
                	<td colspan="2" align="center"><h3>Ingreso del Patronato</h3></td>
    			</tr>
                <tr>
                	<td width="20%">Nombre del Patrocinador*:</td>
                    <td><input type="text" name="patrocinador" id="patrocinador" value="<?php if(!is_null($IngPat)) echo($IngPat->getPatrocinador());?>"/></td>
                </tr>
                <tr>
                	<td width="20%">Monto*:</td>
                    <td><input type="text" name="monto" id="monto" value="<?php if(!is_null($IngPat)) echo($IngPat->getMonto());?>"/></td>
                </tr>
				<td width="25%">Periodo:</td>
                    <td><select name="periodo" id="periodo" style="width:110px" > 
                    <?php
                    $daoPer = new PeriodoDaoJdbc();
                    $listaPer = $daoPer->obtieneListado();
                    $elemento = new Periodo();
                    foreach ($listaPer as $elemento) {
                        $sel = "";
                        if (isset($_GET['periodo']) && (int) ( $_GET['periodo'] ) == $elemento->getId())
                            $sel = "selected='selected'";
                        else if ($_GET['periodo'] == NULL && (int) ($elemento->getPeriodo()) == date('Y')) {
                            $sel = "selected='selected'";
                            $periodo = (String) ($elemento->getId() );
                        }
                        echo("<option value=" . $elemento->getId() . " " . $sel . " >" . $elemento->getPeriodo() . "</option>");
                    }
                    ?>  
                </select></td>
                <tr>
                	<td width="20%">Proyecto*:</td>
                    <td>
                    	<select name="proyecto" id="proyecto" onchange="mostrarExpoTemp()">
                            
                            <?php
							if(is_null($IngPat))
                        		echo('<option value="0">----Selecciona un Proyecto----</option>');
							
                            $daoProy = new LineaAccionDaoJdbc();
							$lista = $daoProy->obtieneListado();
							$elemento = new LineaAccion();
                            foreach($lista as $elemento){
								if(is_null($IngPat) || $IngPat->getIdProyecto() == $elemento->getId()){
                            ?>
                            <option value="<?php echo($elemento->getId());?>"><?php echo($elemento->getLineaAccion());?></option>
                            <?php
								}
                            }
                            ?>
                        </select>
                    </td>
					
                </tr>
                <tr>
                	<td width="20%">Exposición Temporal:</td>
                    <td>
                    	<select name="expotemp" id="expotemp" <?php if(is_null($IngPat) || is_null($IngPat->getIdExpoTemp())){?>style="display:none;"<?php }?>>
                    	<?php
						$daoET = new ExposicionesTemporalesDaoJdbc();
						$lista = $daoET->getListado();
						$elemento = new ExposicionesTemporales();
						foreach($lista as $elemento){
							if(is_null($IngPat) || $IngPat->getIdExpoTemp() == $elemento->getIdExposicionT()){
						?>
                        	<option value="<?php echo($elemento->getIdExposicionT());?>"><?php echo($elemento->getNombre())?></option>
                        <?php }}?>
                    	</select>
                    </td>
                </tr>
                 <tr>
                	<td width="20%">Justificante (Menor a 2MB): <?php if(!is_null($IngPat) && !is_null($IngPat->getArchivo())) echo("<a href='" . $IngPat->getArchivo() . "'>Ver Documento</a>");?></td>
                	<td><input name="archivo" type="file" id="archivo"/></td>
                 </tr>
                <tr>
                	<td colspan="2" align="center"><input type="submit" value="Guardar" style="cursor:pointer" class='btn' name="guardar" id="guardar"/>
                    </td>
                </tr>
            </table>
            <?php if(isset($_GET['id']))echo('<input type="hidden" id="id" name="id" value="' . $_GET['id'] . '" />');?>
        </form>
        <br /><br />
        <div align="center"> <a href='lista_ingresosPatronato.php' class='liga_btn'>Regresar</a> </div>
    </div>
</body>
<script>
 var frmvalidator  = new Validator("ingresoP");
 frmvalidator.addValidation("patrocinador", "req", "Por favor ingrese el nombre del patrocinador.");
 frmvalidator.addValidation("monto", "req", "Por favor ingrese el valor del monto.");
 frmvalidator.addValidation("monto", "num", "El monto debe ser númerico.");
 frmvalidator.addValidation("proyecto", "dontselect=0", "Por favor seleccione un proyecto.");

</script>
</html>