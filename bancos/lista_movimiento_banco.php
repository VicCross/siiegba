<?php

$id_perfil = null;

session_start();
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/MovimientoBancoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/MovimientoBanco.class.php");
include_once("../src/mx/com/virreinato/dao/CatProyectoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatProyecto.class.php");
include_once("../src/mx/com/virreinato/dao/CuentaBancariaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CuentaBancaria.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}else{
    $id_perfil = $_SESSION['id'];
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<link href="../css/calendario.css" type="text/css" rel="stylesheet">
<script src="../js/calendar.js" type="text/javascript"></script>
<script src="../js/calendar-es.js" type="text/javascript"></script>
<script src="../js/calendar-setup.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript">
function verifica_destino(){
	destino=document.getElementById('destino').value;
	
	if(destino=='Proyectos'){
		//muestro el listado de proyectos
		document.getElementById('label_proyecto').style.display='table-cell';
		document.getElementById('select_proyecto').style.display='table-cell';
	}
	else{
		//oculto el listado de proyectos
		document.getElementById('label_proyecto').style.display='none';
		document.getElementById('select_proyecto').style.display='none';
	}
	
}
</script>

<!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="../css/lista.css">
        <script type="text/javascript" src="../media/js/complete.js"></script>
        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
        <script type="text/javascript" src="../js/lista.js"></script>
		
    <input type="hidden" id="columnas_visibles" name="columnas_visibles" value="9"/>
    <input type="hidden" id="total_columnas" name="total_columnas" value="11"/>



</head>
<body>
<?php

    $ffin = $finicio = $conciliado = $id = $id_periodo = $mes = null;

    if(isset($_GET['finicio'])){ $finicio = $_GET['finicio']; }
    if(isset($_GET['ffin'])){ $ffin = $_GET['ffin']; }
    if(isset($_GET['conciliado'])){ $conciliado = $_GET['conciliado']; }
    if(isset($_GET['id_periodo'])){ $id_periodo = $_GET['id_periodo']; }
    if(isset($_GET['id'])){ $id = $_GET['id']; }
    if(isset($_GET['mes'])){ $mes = $_GET['mes']; }
    
    $meses = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    
    $dao=new MovimientoBancoDaoJdbc();
    
    $finiciod = date("d-m-Y",strtotime($finicio));
    $ffind = date("d-m-Y",strtotime($ffin));

    $respuesta = null;
    if(isset($_GET['respuesta'])){ $respuesta = $_GET['respuesta']; }
?>
<div class="contenido">
    <br/><p class="titulo_cat1">Bancos > Movimientos Bancarios</p> 
    <?php if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>"); ?>
    <h3 class="titulo_cat" style='padding-left:2%'>Mes: <?php echo($meses[(int)$mes]);?>, Año:<?php echo(date("Y",strtotime($finiciod)));?>   </h3>
	<table  border="0" cellspacing="0" cellpadding="5" class='dataTable' align='center' >
	  
	  <thead>
	  
	  <tr>
	    <th width="">Número de Operación</th>
	    <th>Cargo</th>
	    <th>Abono</th>
	    <th>Fecha</th>
	    <th>Número de Cuenta</th>
	    <th>Proyecto</th>
	    <th>Forma de Carga</th>
	    <th>Período</th>
	    <th>Referencia</th>
	    <th></th>
	    <th></th>
	   </tr>
	   
	   </thead>
	   <tfoot>
	   <tr>
	    <th width=""> </th>
	    <th> </th>
	    <th> </th>
	    <th> </th>
	    <th> </th>
	    <th> </th>
	    <th> </th>
	    <th> </th>
	    <th> </th>
	    <th></th>
	    <th></th>
	   </tr>
	   
	   </tfoot>
	   <tbody>
	  <?php
	  
  	    $lista=$dao->obtieneListado($finiciod,$ffind);
  	  
  	    $elemento=new MovimientoBanco();

            foreach($lista as $elemento){
  			
            ?>
            <tr>
              <td><?php if($elemento->getNumeroOperacion()!=null && $elemento->getNumeroOperacion() != "null" ) echo($elemento->getNumeroOperacion());?></td>
              <td><?php echo($elemento->getCargo());?></td>
              <td><?php echo($elemento->getAbono());?></td>
              <td><?php echo(date("d-m-Y",strtotime($elemento->getFecha())));?></td>
              <td><?php 
                $cuenta = $elemento->getCuenta();
                $banco = $cuenta->getBanco();
                echo($banco->getDescripcion());?> <br><?php echo($cuenta->getNumeroCuenta());?></td>
              <td><?php 
                $proyecto = $elemento->getProyecto();
                if($elemento->getProyecto()!=null && $proyecto->getDescripcion()!=null && $proyecto->getDescripcion() != "null"){ echo($proyecto->getDescripcion()); }?></td>
              <td><?php echo($elemento->getFormaCarga());?></td>
              <td><?php 
                $periodo = $elemento->getPeriodo();
                echo($periodo->getPeriodo());?></td>
              <td><?php echo($elemento->getReferencia());?></td>

              <?php 
                  //verifico si este mes y periodo no esta conciliado 
                  if($conciliado!=null && $conciliado != "1" &&  ( $id_perfil!=null && $id_perfil != 3 && $id_perfil != 4)) {
                          ?>
                  <td><a href='../src/mx/com/virreinato/web/MovimientosBanco.php?modificar=1&id=<?php echo($elemento->getId());?>&finicio=<?php echo($finicio);?>&ffin=<?php echo($ffin);?>&id_periodo=<?php echo($id_periodo);?>&mes=<?php echo($mes);?>' class='liga_cat'><img src="../img/Pencil3.png" alt="Editar" style="border: 0px none;" ></a></td>
                  <td><a href='../src/mx/com/virreinato/web/MovimientosBanco.php?id=<?php echo($elemento->getId());?>&finicio=<?php echo($finicio);?>&ffin=<?php echo($ffin);?>&id_periodo=<?php echo($id_periodo);?>&mes=<?php echo($mes);?>' class='liga_cat' onclick='return confirm("¿Esta seguro que desea eliminar este registro?")'><img src="../img/DeleteRed.png" alt="Borrar" style="border: 0px none;"></a></td>
                  <?php
                  }
              ?>
            </tr>	
            <?php			
		}
            ?>
			</tbody>
	</table>
	<br>
	<br>
        
        <form id="fbanco" name="fbanco" method="post" action="../src/mx/com/virreinato/web/MovimientosBanco.php">

	<?php 
	//verifico si este mes y periodo no esta conciliado para mostrar el formulario de captura.
	
	if($conciliado!=null && $conciliado != "1" && ($id_perfil!=null && $id_perfil != 3 && $id_perfil != 4)){
            $elem=null;
            //verifico si existe el id
            if($id!=null ){
                $mvo=new MovimientoBancoDaoJdbc();
                $elem=$mvo->obtieneElemento($id);
            }	
        ?>
        <table width="60%%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
          <tr>
            <td width="">Número de Operación*:</td>
            <td width="">
              <input type="text" name="numoperacion" id="numoperacion" size="20" value='<?php if($elem!=null && $elem->getNumeroOperacion()!= null) echo($elem->getNumeroOperacion());?>'   />
                </td>
                <td width="">Cargo:</td>
            <td width=""><input type="text" name="cargo" id="cargo" size="20" value='<?php if($elem!=null && $elem->getCargo()!= null) echo($elem->getCargo());?>' <?php if($elem!=null && $elem->getFormaCarga() == "Archivo")  echo("readonly='readonly'");?> />
                </td>
                <td width="">Abono:</td>
            <td width=""><input type="text" name="abono" id="abono" size="20" value='<?php if($elem!=null && $elem->getAbono()!= null) echo($elem->getAbono());?>' <?php if($elem!=null && $elem->getFormaCarga() == "Archivo")  echo("readonly='readonly'");?> />
                </td>
          </tr>

          <tr>
            <td width="">Fecha*:</td>
            <td width="">
              <input type="text" name="fecha" id="fecha" size="20" value='<?php if($elem!=null && $elem->getFecha()!= null) echo(date("d-m-Y",strtotime($elem->getFecha())));?>' <?php if($elem!=null && $elem->getFormaCarga() == "Archivo")  echo("readonly='readonly'");?> />
                </td>
            <td width="">Número de Cuenta*:</td>
            <td width="">
                <select name="id_cuentabancaria" id="id_cuentabancaria" <?php if($elem!=null && $elem->getFormaCarga() == "Archivo")  echo("readonly='readonly'");?>>
                        <option value='0'>Selecciona</option>		      
                <?php
                    $cuenta=new CuentaBancariaDaoJdbc();
                    $cuentas=$cuenta->obtieneListado(); 
                    $b = new CuentaBancaria();

                    foreach($cuentas as $b){
                        $sel="";
                        if($elem!= null && $elem->getCuenta()!= null ){
                            $cuenta = $elem->getCuenta();
                            if($b->getId()==$cuenta->getId())
                                $sel="selected='selected'";
                        }
			$banco = $b->getBanco();			
                        echo("<option value='".$b->getId()."' ".$sel." >".$b->getNumeroCuenta()." (".$banco->getDescripcion().") </option>");
                    }
                ?>
                </select>
			
            </td>
            <td width="">Destino*:</td>
            <td width="">
              <select name="destino" id="destino" onchange='verifica_destino();' >
              <option value='0'>Selecciona</option>
              <option <?php if($elem!=null && $elem->getDestino()!= null &&  $elem->getDestino() == "Proyectos") echo("selected='selected'");?> >Proyectos</option>
              <option <?php if($elem!=null && $elem->getDestino()!= null &&  $elem->getDestino() == "Gasto Básico") echo("selected='selected'");?>>Gasto Básico</option>
              <option <?php if($elem!=null && $elem->getDestino()!= null &&  $elem->getDestino() == "Logística") echo("selected='selected'");?>>Logística</option>
              <option <?php if($elem!=null && $elem->getDestino()!= null &&  $elem->getDestino() == "Terceros") echo("selected='selected'");?>>Terceros</option>
              <option <?php if($elem!=null && $elem->getDestino()!= null &&  $elem->getDestino() == "Donativos") echo("selected='selected'");?>>Donativos</option>
              <option <?php if($elem!=null && $elem->getDestino()!= null &&  $elem->getDestino() == "Préstamos") echo("selected='selected'");?>>Préstamos</option>
              <option <?php if($elem!=null && $elem->getDestino()!= null &&  $elem->getDestino() == "Ajustes") echo("selected='selected'");?>>Ajustes</option>
              <option <?php if($elem!=null && $elem->getDestino()!= null &&  $elem->getDestino() == "No Identificado") echo("selected='selected'");?>>No Identificado</option>
              </select>
            </td>
          </tr>
		  
          <tr>
            <td width="">Referencia:</td>
            <td width="">
                <input type="text" name="referencia" id="referencia" size="20" value='<?php if($elem!=null && $elem->getReferencia()!= null) echo($elem->getReferencia());?>'  />
            </td>
            <td width="">Período*:</td>
            <td width="">
              <select name="id_periodo" id="id_periodo">
                <option value='0'>Selecciona</option>		      
                <?php
                    $periodo=new PeriodoDaoJdbc();
                    $periodos=$periodo->obtieneListadoAbierto(); 
                    $b = new Periodo();

                    foreach($periodos as $b){
                        echo("<option value='".$b->getId()."' ");
                        if($id_periodo!=null && (int)($id_periodo) == $b->getId())
                        {        echo(" selected='selected' "); }

                        if($elem!= null && $elem->getPeriodo()!= null ){
                            $periodo = $elem->getPeriodo();
                            if($b->getId()==$periodo->getId())
                            {    echo(" selected='selected' "); }
                        }
                        echo(">".$b->getPeriodo()."</option>");
                    }
                ?>
              </select>
            </td>
            <td width="" style='display:none' id='label_proyecto'>Proyecto:</td>
            <td width="" style='display:none' id='select_proyecto'>
              <select name="id_proyecto" id="id_proyecto">
                <option value='0'>Selecciona</option>		      
                <?php
                    $proyecto=new CatProyectoDaoJdbc();
                    $proyectos=$proyecto->obtieneListado2(); 
                    $b = new CatProyecto();

                    foreach($proyectos as $b){
                        echo("<option value='".$b->getId()."'");
                        if($elem!= null && $elem->getProyecto()!= null ){
                            $proyecto = $elem->getProyecto();
                            if($b->getId()==$proyecto->getId())
                            {    echo(" selected='selected' "); }
                        }
                        echo(">".$b->getDescripcion()."</option>");
                    }
                ?>
              </select>
            </td>
          </tr>
		  
          <tr>
            <td align="center" colspan="6"><input name="guardar" type="submit" value="Guardar"  class='btn' /></td>
          </tr>
        </table>
	
        <?php 		
            }
            else if ($id_perfil==null || $id_perfil == 3 || $id_perfil == 4){
				
            }
	else{
		
            echo("<div align='center' class='msj'>No es posible agregar mas movimientos a este mes porque ya fue conciliado.</div>");
	}
            if($finicio!=null ) echo("<input type='hidden' name='finicio' value='".$finicio."' />");
            if($ffin!=null ) echo("<input type='hidden' name='ffin' value='".$ffin."' />");
            if($conciliado!=null ) echo("<input type='hidden' name='conciliado' value='".$conciliado."' />");
            if($id_periodo!=null ) echo("<input type='hidden' name='id_periodo' value='".$id_periodo."' />");
            if($mes!=null ) echo("<input type='hidden' name='mes' value='".$mes."' />");
            if($id!=null ) echo("<input type='hidden' name='id' value='".$id."' />");
	?>
	</form>
	<br>
	<div align='center'>
            <a href='selecciona_periodo.php' class='liga_btn'> Regresar</a>
	</div>
</div>
<script type="text/javascript">
var frmvalidator  = new Validator("fbanco");
 frmvalidator.addValidation("numoperacion","req","Por favor capture el número de operación.");
 frmvalidator.addValidation("fecha","req","Por favor capture la fecha.");
 frmvalidator.addValidation("id_cuentabancaria","req","Por favor capture el número de cuenta.");
 frmvalidator.addValidation("destino","dontselect=0","Por favor seleccione el destino.");
 frmvalidator.addValidation("id_periodo","dontselect=0","Por favor seleccione el período.");
 function DoCustomValidation(){
  var frm = document.forms["fbanco"];
  
  if((frm.cargo.value == 0 || frm.cargo.value == "")  && (frm.abono.value==0 || frm.abono.value =="" ) )
  {
    sfm_show_error_msg('Es necesario que capture el monto del cargo o abono',frm.cargo);
    return false;
  }
  else
  {
    return true;
  }
}

 frmvalidator.setAddnlValidationFunction(DoCustomValidation);
 frmvalidator.DoCustomValidation;
 
 Calendar.setup({ inputField : "fecha", ifFormat : "%d-%m-%Y", button: "fecha" }); 
 </script>

</body>
</html>
