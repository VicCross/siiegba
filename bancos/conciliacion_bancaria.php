<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/ConciliacionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Conciliacion.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/MovimientoMuseoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/MovimientoMuseo.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/MovimientoBancoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/MovimientoBanco.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/PHPExcel.php");

include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);


if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}else{
   $id_perfil = $_SESSION['id']; 
}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<script language="JavaScript" type="text/javascript" >
function calculatotales(){
	var saldo_ini=document.getElementById('saldo_inicial').value;
	if(isNaN(saldo_ini)){
		alert("El saldo inicial sólo debe de contener números, por favor verifíquelo.");
	}

	saldo_ini=saldo_ini*1;
	var saldo_final_m=saldo_ini+((document.getElementById('total_am').value)*1) - ((document.getElementById('total_cm').value)*1);
	var saldo_final_b=saldo_ini+((document.getElementById('total_ab').value)*1) - ((document.getElementById('total_cb').value)*1);
	var diferencia=saldo_final_m-saldo_final_b;
	
	document.getElementById('saldo_final_m').value=Math.round(saldo_final_m * 100) / 100 ;
	document.getElementById('saldo_final_b').value=Math.round(saldo_final_b * 100) / 100 ;
	document.getElementById('diferencia').value=Math.round(diferencia * 100) / 100 ; 
	if(diferencia==0 || diferencia==0.0){
		document.getElementById('boton_concilia').innerHTML = "<input type='submit' name='conciliar' value='Conciliar'>";
	}
		
}

</script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<body>
<?php
if($id_perfil==null || $id_perfil == 3 || $id_perfil == 4)
    echo("<div align='center' class='msj'>Usted no tiene los permisos suficientes para ingresar a este módulo del sistema.</div>");
else{
    
    $mes = $mes2 = $id_periodo = $respuesta = null;
    if(isset($_GET['mes']))
    {   $mes=(int)((String)$_GET['mes']);    
        $mes2 = $mes + 1;
    }
	
    if(isset($_GET['id_periodo']))
    {   $id_periodo=(int)((String)$_GET['id_periodo']);    }

    if(isset($_GET['respuesta']))
    {   $respuesta=((String)$_GET['respuesta']);    }
    
    //obtengo la descripcion del periodo
    $dp=new PeriodoDaoJdbc();
    $periodo=$dp->obtieneElemento((String)($id_periodo));
    
    if(strlen((String)$mes) == 1){
        $mes = "0".$mes;
    }
    if(strlen((String)$mes2) == 1){
        $mes2 = "0".$mes2;
    }
    $calInicio = $periodo->getPeriodo()."-".$mes."-01";
    $calFin = $periodo->getPeriodo()."-".$mes2."-01";
    
    $fechaIni = date("Y-m-d",strtotime($calInicio));
    $fechaFin = date("Y-m-d",strtotime($calFin));
    
    $daoc=new ConciliacionDaoJdbc();
    $concilia= $daoc->obtieneElemento((int)($mes),(int)($id_periodo));
?>
    <div class="contenido">
        <br/><p class="titulo_cat1">Bancos > Conciliación Bancaria</p> 
        <?php if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>"); ?>
        <form id="fconcilia" name="fconcilia" method="post" action="../src/mx/com/virreinato/web/ConciliacionBancaria.php">
            <table width="97%" border="0" cellspacing="5" cellpadding="0" class='tb_cat' align='center' >

                <tr>
                  <td colspan='2' align='center'>Saldo inicial: <input type='text' name='saldo_inicial' id='saldo_inicial' <?php if($concilia!=null && $concilia->getSaldoInicial()!=null) { echo("value='".$concilia->getSaldoInicial()."' readonly='readonly'");} else{ echo("onkeyup='calculatotales();'");} ?>   ></td>
                </tr>

                <tr>
                  <th width="50%" style='background-color:#585D9D'>Movimientos Registrado por el Museo</th>
                  <th width="50%" style='background-color:#6A85B4'>Movimientos Registrados en Bancos</th>
                </tr>

                <tr>
                  <td valign='top'>
                    <table width="100%" border="0" cellspacing="0" cellpadding="5" class='tb_cat' align='center' style='background-color:#DEE6EF'>
                        <tr>
                          <th width="20%" style='background-color:#585D9D'>Fecha</th>
                          <th width="40%" style='background-color:#585D9D'>Tipo de Movimiento</th>
                          <th width="20%" style='background-color:#585D9D'>Cargo</th>
                          <th width="20%" style='background-color:#585D9D'>Abono</th>
                        </tr>
					
                        <?php
                            //obtengo la fecha inicial y final para conciliar

                            $dao=new MovimientoMuseoDaoJdbc();

                            $lista=$dao->obtieneListado($fechaIni,$fechaFin);
                            $elemento=new MovimientoMuseo();

                            $total_cargos_m=0.0;
                            $total_abonos_m=0.0;

                            foreach($lista as $elemento){
                            $total_cargos_m+=$elemento->getCargo();
                            $total_abonos_m+=$elemento->getAbono();
                        ?>
                        <tr>
                          <td><?php echo(date("d-m-Y",strtotime($elemento->getFecha())));?></td>
                          <td><?php echo($elemento->getDescripcion());?></td>
                          <td align='right'><?php echo($elemento->getCargo());?></td>
                          <td align='right'><?php echo($elemento->getAbono());?></td>
                        </tr>	
                        <?php			
                            }
                        ?>

                        <tr>
                          <td width="50%"></td>
                          <td width="50%">Totales</td>
                          <td width="50%"><input type="text" name='total_cm' id='total_cm' readonly='readonly'  style='text-align:right' value='<?php echo(number_format($total_cargos_m,2));?>'></td>
                          <td width="50%"><input type="text" name='total_am' id='total_am' readonly='readonly' style='text-align:right' value='<?php echo(number_format($total_abonos_m,2));?>'></td>
                        </tr>
							
                    </table>
				    
                  </td>
		    
		  <td valign='top'>
                    <table width="100%" border="0" cellspacing="0" cellpadding="5" class='tb_cat' align='center' style='background-color:#E6EEF2'>
                      <tr>
                        <th width="50%" style='background-color:#6A85B4'>Fecha</th>
                        <th width="50%" style='background-color:#6A85B4'>Tipo de Movimiento</th>
                        <th width="50%" style='background-color:#6A85B4'>Cargo</th>
                        <th width="50%" style='background-color:#6A85B4'>Abono</th>
                      </tr>
				  
                      <?php
                        $daob=new MovimientoBancoDaoJdbc();

			$lista1=$daob->obtieneListado($fechaFin,$fechaIni);
			$elemento1=new MovimientoBanco();

                        $total_cargos_b=0.0;
                        $total_abonos_b=0.0;
			  	    
                        foreach($lista1 as $elemento1){
                            $total_cargos_b+=$elemento1->getCargo();
                            $total_abonos_b+=$elemento1->getAbono();

                      ?>
                    <tr>
                      <td><?php echo(date("d-m-Y",strtotime($elemento1->getFecha())));?></td>
                      <td><?php echo($elemento1->getReferencia());?></td>
                      <td align='right'><?php echo($elemento1->getCargo());?></td>
                      <td align='right'><?php echo($elemento1->getAbono());?></td>
                    </tr>	
                    <?php			
                      }
                    ?>
				  
                        <tr>
                          <td width="50%"></td>
                          <td width="50%">Totales</td>
                          <td width="50%"><input type="text" name='total_cb' id='total_cb' readonly='readonly' style='text-align:right' value="<?php echo(number_format($total_cargos_b,2));?>"></td>
                          <td width="50%"><input type="text" name='total_ab' id='total_ab' readonly='readonly' style='text-align:right' value="<?php echo(number_format($total_abonos_b,2));?>"></td>
                        </tr>
				
                    </table>    
		    </td>
		  </tr>
		  <tr>
		    <th style='background-color:#585D9D'>Saldo Final: <input type="text" name='saldo_final_m' id='saldo_final_m' style='text-align:right' readonly='readonly' <?php if($concilia!=null && $concilia->getSaldoFinalMuseo()!=null) echo("value='".number_format($concilia->getSaldoFinalMuseo(),2)."'"); ?>> </th>
		    <th style='background-color:#6A85B4'>Saldo Final: <input type="text" name='saldo_final_b' id='saldo_final_b' style='text-align:right' readonly='readonly' <?php if($concilia!=null && $concilia->getSaldoFinalBanco()!=null) echo("value='".number_format($concilia->getSaldoFinalBanco(),2)."'"); ?>> </th>
		  </tr>
		 
		  <tr>
		    <td colspan='2' align='center'>Diferencia: <input type="text" name='diferencia' id='diferencia' style='text-align:right' readonly='readonly' <?php if($concilia!=null && $concilia->getDiferencia()!=null) echo("value='".number_format($concilia->getDiferencia(),2)."'"); ?>> </td>
		  </tr>
		  
		  <tr>
		    <td colspan='2' align='center'>Observaciones: <textarea name='observaciones' id='observaciones' rows='2' cols='40'><?php if($concilia!=null && $concilia->getObservaciones()!=null) echo($concilia->getObservaciones()); ?></textarea></td>
		  </tr>
		  
            </table>
		
            <br>
            <div id='boton_concilia' align='center'></div>
            <br>
            <input type='hidden' name='mes' value='<?php echo($mes);?>'>
            <input type='hidden' name='id_periodo' value='<?php echo($id_periodo);?>'>
        </form>
	</div>
	<script type="text/javascript">
	 var frmvalidator  = new Validator("fconcilia");
	 frmvalidator.addValidation("saldo_inicial","req","Por favor capture el saldo inicial.");
	 </script>
<?php
}
?>
</body>
</html>