<?php
include_once("../src/mx/com/virreinato/dao/ProductosVentaDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/dao/ProductosDaoJdbc.class.php");

 // ProveedorDaoJdbc.class.php
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProductosVenta.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProductosPatronato.class.php");
// proveedor.class.php
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>

<script type="text/javascript">
	function ActualizaInventario()
	{		
		var idProd=document.getElementById("nom_producto").value;
		//alert(idProd);
		var xHttp;
		if (window.XMLHttpRequest) 
		{
			xHttp = new XMLHttpRequest();
		}
		else
		{
			xHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xHttp.onreadystatechange = function()
		{
			if (xHttp.readyState == 4 && xHttp.status == 200) 
			{
				
				document.getElementById("inventario").innerHTML=xHttp.responseText;
			}
		};
		xHttp.open("GET","recuperaInv.php?idProducto="+ idProd,true);
		xHttp.send();
	}
</script>

<script>

	function calculaTotal()
	{
		if (document.getElementById("precio_unitario") != "" && document.getElementById("piezas_vendidas") != "" )
		{
			var total= document.getElementById("precio_unitario").value *  document.getElementById("piezas_vendidas").value; 
			document.getElementById("monto_total").value= total;
			document.getElementById("monto_totalaux").value= total;
		}
		
	}



</script>


</head>
<body>
 <?php
    $error = NULL;
    if (isset($_REQUEST['error'])) {
        $error = $_REQUEST['error'];
    }
    ?>
<div class="contenido"> 
    <p class="titulo_cat1">Catálogos > Patronato >
    <?php
	$dao=new ProductosVentaDaoJdbc();
	$elemento=new ProductosVenta();

	if(isset($_GET['id'])){
		echo("Modificar Venta");
		$elemento=$dao->obtieneElementoProdVenta($_GET['id']);	
	}	
	else{
		echo("Alta Venta");
	}	
    ?>
    </p>
    <br/>
    <?php if ($error != null) echo("<div align='center' class='msj'>" . $error . "</div>"); ?>
    <form id="fproducto" name="fproducto" method="post" action="../src/mx/com/virreinato/web/CatProductosVenta.php">
        <table width="90%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
          <tr>
            <td width='15%'>
			<label for="nom_producto" id="ej">Nombre Producto*:</label>
			    <select class="form-control" name="nom_producto" id="nom_producto" onchange="ActualizaInventario()">
			        <option value="0" selected="selected">----Selecciona un Producto----</option>
			            <?php
			            $dao = new ProductosDaoJdbc();
					    $lista = $dao->obtieneListadoProd();
					    $elemento1 = new Productos();
			            foreach($lista as $elemento1)
						{
							echo "<option value=".$elemento1->getId_prod().">".$elemento1->getNombre_prod()."</option>";
						}
						?>
			    </select>
			</td>
			
            <!--<td width='35%'><input type="text" name="nom_producto" id="nom_producto" size='50' value='< ?php if($elemento!=null && $elemento->getNom_producto()!= null) echo($elemento->getNom_producto());?>'/></td>
              -->
		   <td width='15%'>Inventario Actual*:</td>
            <td width='35%'>
			<div id="inventario">
				<input type="text" name="inventario_act" id="inventario_act" size='20' value='<?php if($elemento!=null && $elemento->getInventario_act()!= null) echo($elemento->getInventario_act());?>'/>
			</div>
			</td>
          </tr>

          <tr>
            <td>Precio Unitario*:</td>
            <td><input type="text" name="precio_unitario" id="precio_unitario" onchange="calculaTotal()" value='<?php if($elemento!=null && $elemento->getPrecio_unitario()!= null) echo($elemento->getPrecio_unitario());?>' /></td>
            <td>Piezas Vendidas*</td>
            <td><input type="text" name="piezas_vendidas"  id="piezas_vendidas" onchange="calculaTotal()"  value='<?php if($elemento!=null && $elemento->getPiezas_vendidas()!= null) echo($elemento->getPiezas_vendidas());?>'/></td>
          </tr>

          <tr>
			<td>Monto Total*:</td>
            <td><input type="text" name="monto_totalaux" id="monto_totalaux" disabled value='<?php if($elemento!=null && $elemento->getMonto_total()!= null) echo($elemento->getMonto_total());?>' /></td>
            <td><input type="hidden" name="monto_total" id="monto_total" /></td>
           
		  
		  <!--
            <td>Estatus Producto:</td>
            <td>
				<select name="estatus_prod" id="estatus_prod">
					<option value='1' < ?php if($elemento!=null && $elemento->getEstatus_prod()!= null && $elemento->getEstatus_prod()==1 ) echo("selected='selected'"); ?>>Activo</option>
					<option value='0' < ?php if($elemento!=null && $elemento->getEstatus_prod()!= null && $elemento->getEstatus_prod()==0 ) echo("selected='selected'"); ?>>Inactivo</option>
                </select>
				-->
          </tr>
		  <tr>
			 <td>Monto Museo*</td>
             <td><input type="text" name="monto_museo"  id="monto_museo"  value='<?php if($elemento!=null && $elemento->getMonto_museo()!= null) echo($elemento->getMonto_museo());?>'/></td>
         
		  </tr>
		  <tr>
			 <td>Monto Patronato*</td>
             <td><input type="text" name="monto_patronato"  id="monto_patronato"  value='<?php if($elemento!=null && $elemento->getMonto_patronato()!= null) echo($elemento->getMonto_patronato());?>'/></td>
         
		  </tr>
		  

          <tr>
            <td align="center" colspan="4"><input name="guardar" type="submit" value="Guardar"  class='btn' />
            &nbsp;&nbsp;&nbsp;&nbsp;<a href="lista_patronato.php" target= "destino" class='liga_btn'> Cancelar </a>
            </td>
          </tr>

        </table>
        <?php if($elemento!=null && $elemento->getId_venta()!=null) echo("<input type='hidden' name='id' value='".$elemento->getId_venta()."' />");?>
    </form>
</div>
<!--< ?php include 'footer.php' ?>-->
<script type="text/javascript">
 var frmvalidator  = new Validator("fproducto");
 //
 //frmvalidator.addValidation("nombre_prod","req","Por favor capture el nombre del producto.");
 //frmvalidator.addValidation("descripcion_prod","req","Por favor capture la descripción.");
 //frmvalidator.addValidation("inventario_inicial","req","Por favor capture el inventario inicial.");

 
</script>

</body>
</html>
