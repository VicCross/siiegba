<?php
include_once("../src/mx/com/virreinato/dao/ProveedorDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Proveedor.class.php");
include_once("../src/classes/Catalogo.class.php");
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<body>
 <?php
    $error = NULL;
    if (isset($_REQUEST['error'])) {
        $error = $_REQUEST['error'];
    }
    ?>
<div class="contenido"> 
    <p class="titulo_cat1">Catálogos > Catálogos de Operación >
    <?php
	$dao=new ProveedorDaoJdbc();
	$elemento=new Proveedor();

	if(isset($_GET['id'])){
		echo("Modificar Proveedor");
		$elemento=$dao->obtieneElemento($_GET['id']);	
		$idAreaExt = $_GET['area'];
	}	
	else{
		echo("Alta de Nuevo Proveedor");
	}	
    ?>
    </p>
    <br/>
    <?php if ($error != null) echo("<div align='center' class='msj'>" . $error . "</div>"); ?>
    <form id="fproveedor" name="fproveedor" method="post" action="../src/mx/com/virreinato/web/CatProveedor.php">
        <table width="90%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
          <tr>
            <td width='15%'>Clave*:</td>
            <td width='35%'><input type="text" name="clave" id="clave" size='50' value='<?php if($elemento!=null && $elemento->getClave()!= null) echo($elemento->getClave());?>'/></td>
            <td width='15%'>Beneficiario*:</td>
            <td width='35%'><input type="text" name="beneficiario" id="beneficiario" size='100' value='<?php if($elemento!=null && $elemento->getBeneficiario()!= null) echo($elemento->getBeneficiario());?>'/></td>
          </tr>

          <tr>
            <td>Cuenta*:</td>
                <td><input type="text" name="cuenta" id="cuenta" maxlength='5' value='<?php if($elemento!=null && $elemento->getCuenta()!= null) echo($elemento->getCuenta());?>' /></td>
            <td>Unidad CTA:</td>
            <td><input type="text" name="unidadcta"  id="unidadcta" size='50' value='<?php if($elemento!=null && $elemento->getUnidadCTA()!= null) echo($elemento->getUnidadCTA());?>'/></td>
          </tr>

          <tr>
          <td>Activo BEN:</td>
            <td ><select name="activoben" id="activoben">

                <option value='1' <?php if($elemento!=null && $elemento->getActivoBEN()!= null && $elemento->getActivoBEN()==1 ) echo("selected='selected'"); ?>>Activo</option>
                <option value='0' <?php if($elemento!=null && $elemento->getActivoBEN()!= null && $elemento->getActivoBEN()==0 ) echo("selected='selected'"); ?>>Inactivo</option>
                </select>
                
            </td>
            
            <td>Estatus CTA:</td>
            <td ><select name="estatuscta" id="estatuscta">

                <option value='1' <?php if($elemento!=null && $elemento->getEstatusCTA()!= null && $elemento->getEstatusCTA()==1 ) echo("selected='selected'"); ?>>Activo</option>
                <option value='0' <?php if($elemento!=null && $elemento->getEstatusCTA()!= null && $elemento->getEstatusCTA()==0 ) echo("selected='selected'"); ?>>Inactivo</option>
                </select>
                
            </td>
             </tr>


          <tr>
            <td>Curp*:</td>
            <td ><input type="text" name="curp" id="curp" size='50' value='<?php if($elemento!=null && $elemento->getCurp()!= null) echo($elemento->getCurp());?>'/></td>
            <td>Clave Banco:</td>
            <td ><input type="text" name="clavebanco" id="clavebanco" size='50' value='<?php if($elemento!=null && $elemento->getClaveBanco()!= null) echo($elemento->getClaveBanco());?>'/></td>
            </tr>
            
             <tr>
            <td>Banco*:</td>
            <td ><input type="text" name="banco" id="banco" size='50' value='<?php if($elemento!=null && $elemento->getBanco()!= null) echo($elemento->getBanco());?>'/></td>
            <td>Sicop:</td>
            <td ><input type="text" name="sicop" id="sicop" size='50' value='<?php if($elemento!=null && $elemento->getSicop()!= null) echo($elemento->getSicop());?>'/></td>
            </tr>
            
            
          
			
          <tr>
            <td align="center" colspan="4"><input name="guardar" type="submit" value="Guardar"  class='btn' />
            &nbsp;&nbsp;&nbsp;&nbsp;<a href='lista_proveedor.php' class='liga_btn'> Cancelar </a>
            </td>
          </tr>

        </table>
        <?php if($elemento!=null && $elemento->getId()!=null) echo("<input type='hidden' name='id' value='".$elemento->getId()."' />");?>
    </form>
</div>
<div class="footer" align='center'> 
<a href='lista_proveedor.php' class='liga_btn'> Regresar </a>
</div>
<script type="text/javascript">
 var frmvalidator  = new Validator("fproveedor");
 
 frmvalidator.addValidation("clave","req","Por favor capture la clave del proveedor.");
 frmvalidator.addValidation("beneficiario","req","Por favor capture el beneficiario.");
 frmvalidator.addValidation("cuenta","req","Por favor capture la cuenta.");
 frmvalidator.addValidation("curp","req","Por favor capture el curp.");
 frmvalidator.addValidation("banco","req","Por favor capture el banco.");
</script>

</body>
</html>
