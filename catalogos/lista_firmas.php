<?php
session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once("../src/mx/com/virreinato/dao/LineaAccionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/LineaAccion.class.php");
include_once("../src/mx/com/virreinato/dao/AreaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Area.class.php");
include_once("../src/mx/com/virreinato/dao/EmpleadoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Empleado.class.php");
include_once("../src/classes/Catalogo.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
/*Checamos su perfil*/
$esLiderArea = false;
if($_SESSION['id'] == 5) $esLiderArea = true;
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>


<!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="../css/lista.css">
        <script type="text/javascript" src="../media/js/complete.js"></script>
        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
        <script type="text/javascript" src="../js/lista.js"></script>
			   <input type="hidden" id="columnas_visibles" name="columnas_visibles" value="2"/>
    <input type="hidden" id="total_columnas" name="total_columnas" value="5"/>
</head>
<body>
    <?php $respuesta = NULL;
        if(isset($_GET['respuesta'])){ $respuesta = $_GET['respuesta']; }?>  
    <div class="contenido">
    <br>
	 <p class="titulo_cat1">Catálogos > Catálogos de Operación ></p><br/>
         <?php 
            if($respuesta!=null){
                echo("<div align='center' class='msj'>".$respuesta."</div>");
            }
        ?>
        <div align="right"><a href='agrega_firma.php' class='liga_btn'> Agregar Firma </a></div>
    <br>
         <!--div align="right"><a href='agrega_usuario.php' class='liga_btn'> Agregar Usuario </a></div-->
    <table  border="0" cellspacing="0" cellpadding="5" class='dataTable' align='center' >
	  <thead>
	  <tr>
	    <th width='40%'>Nombre del líder</th>
	    <th width='40%'>Nombre del Área/Proyecto</th>
	    <th width='10%'>Firma</th>
	    <th width='5%'></th>
	    <th width='5%'></th>
	  </tr>
	  </thead>
	  <tfoot>
	  <tr>
	    <th width='40%'></th>
	    <th width='40%'></th>
	    <th width='10%'></th>
	    <th width='5%'></th>
	    <th width='5%'></th>
	  </tr>
	  </tfoot>
	  <tbody>
		<?php
			$daoEmpleado = new EmpleadoDaoJdbc();
			$empleado = new Empleado();
    		$dao = new LineaAccionDaoJdbc();
			$elemento = new LineaAccion();
			$lista = $dao->obtieneListado();
            foreach($lista as $elemento){
				if(!is_null($elemento->getFirma())){
				$empleado = $daoEmpleado->obtieneElemento($elemento->getIdLider());
            ?>
    	<tr class="SizeText">
            <td align="center" ><?php echo($empleado->getNombre() . " " . $empleado->getApPaterno() . " " . $empleado->getApMaterno());?></td>
            <td align="center"><?php echo($elemento->getLineaAccion());?></td>
            <td align="center"><a href="#" onClick="window.open('verFirma.php?ruta=<?php echo($elemento->getFirma());?>','ventana', 'width=362,height=202,scrollbars=NO,menubar=NO,resizable=NO,titlebar=NO,status=NO');">Ver Imágen</a></td>
            <td align="center"><a href="edita_firma.php?tipo=1&id=<?php echo($elemento->getId());?>" class='liga_cat'><img src="../img/Pencil3.png" alt="Aceptar" style="border: 0px none;" ></a></td>
            <td align="center"><a href="insertaFirma.php?tipo=1&id=<?php echo($elemento->getId());?>&accion=borrar" onclick='return confirm("¿Esta seguro que desea eliminar este registro?")' class='liga_cat'><img src="../img/DeleteRed.png" alt="Borrar" style="border: 0px none;"></a></td>
		</tr>	
		<?php
				}
		}
	  	?>
        <?php
    		$dao = new AreaDaoJdbc();
			$elemento = new Area();
			$lista = $dao->obtieneAreasFiltro();
            foreach($lista as $elemento){
				if(!is_null($elemento->getFirma())){
				$empleado = $daoEmpleado->obtieneElemento($elemento->getIdLider());
            ?>
    	<tr class="SizeText">
            <td align="center" ><?php echo($empleado->getNombre() . " " . $empleado->getApPaterno() . " " . $empleado->getApMaterno());?></td>
            <td align="center"><?php echo($elemento->getDescripcion());?></td>
            <td align="center"><a href="#" onClick="window.open('verFirma.php?ruta=<?php echo($elemento->getFirma());?>','ventana', 'width=362,height=202,scrollbars=NO,menubar=NO,resizable=NO,titlebar=NO,status=NO');">Ver Imágen</a></td>
            <td align="center"><a href="edita_firma.php?tipo=2&id=<?php echo($elemento->getId());?>" class='liga_cat'><img src="../img/Pencil3.png" alt="Aceptar" style="border: 0px none;" ></a></td>
            <td align="center"><a href="insertaFirma.php?tipo=2&id=<?php echo($elemento->getId());?>&accion=borrar" onclick='return confirm("¿Esta seguro que desea eliminar este registro?")' class='liga_cat'><img src="../img/DeleteRed.png" alt="Borrar" style="border: 0px none;"></a></td>
		</tr>	
		<?php
				}
		}
	  	?>
	  </tbody>
    </table>
	<br>
	
</div>
</body>
</html>
