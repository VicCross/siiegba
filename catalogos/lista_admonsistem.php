<?php
session_start();
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script >
	function verificarSesion(){	
		$("#destino").load("../verificaSession.php", function(data){
			if(data.toString().indexOf("false") === -1){}
			else{
				window.location = "index.php?err=2";	
			}
		});
	}
</script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<body>
<div class="contenido">
	<div align='center' >
	  	<table align='center' width='90%' >
	  		<tr valign='top'>
	  			<td width='100%'>
				
				<h3 class="titulo_cat">Catálogos de Estructura</h3>
				<ul class='menu_catalogos'>
	  				<li><a href='lista_ccostos.php'>Centros de Costos</a></li> 
	  				<li><a href='lista_directivo.php'>Directivos</a></li> 
					<li><a href='lista_area.php'>Áreas</a></li> 
					<li><a href='lista_periodo.php'>Períodos</a></li> 
					<li><a href='lista_capitulo.php'>Capítulos Presupuestales</a></li>
					<li><a href='lista_partida.php'>Partidas Presupuestales</a></li> 
					<li><a href='lista_usuario.php'>Usuarios</a></li> 
					<li><a href='lista_permisosusuario.php'>Perfiles</a></li> 
					<li><a href='lista_firmas.php'>Firmas</a></li> 
					<li><a href='lista_parametro.php'>Parámetros del Sistema</a></li> 
		    		
		    				    		</ul>
	  			</td>
	  		</tr>
	  	</table>
	</div>
	<br>
</div>
</body>
</html>


