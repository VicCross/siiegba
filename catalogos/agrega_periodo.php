<?php
session_start();
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<link href="../css/calendario.css" type="text/css" rel="stylesheet">
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
<script src="../js/calendar.js" type="text/javascript"></script>
<script src="../js/calendar-es.js" type="text/javascript"></script>
<script src="../js/calendar-setup.js" type="text/javascript"></script>
</head>
<body>
    <?php
        $error = NULL;
        if (isset($_REQUEST['error'])) {
            $error = $_REQUEST['error'];
        }
    ?> 
<div class="contenido"> 
	<p class="titulo_cat1">Catálogos > Catálogos de Estructura >
<?php
	$dao=new PeriodoDaoJdbc();
	$elemento=new Periodo();

	if (isset($_GET['id'])){
		echo("Modificar Período");
		$elemento=$dao->obtieneElemento($_GET['id']);	
	}	
	else{
		echo("Alta de Nuevo Período");
	}	
	?>
    </p>
    <br/>       
    <?php if ($error != null) echo("<div align='center' class='msj'>" . $error . "</div>"); ?>
    <form id="fperiodo" name="fperiodo" method="post" action="../src/mx/com/virreinato/web/CatPeriodo.php">
    <table width="60%%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
      <tr>
        <td width="15%">Período*:</td>
        <td width="35%">
          <input type="text" name="periodo" id="periodo" value='<?php if($elemento!=null && $elemento->getPeriodo()!= null) echo($elemento->getPeriodo());?>'>
            </td>
        <td width="15%">Cerrar período:</td>
        <td width="35%">
          <input type="checkbox" name="cerrado" id="cerrado" <?php if($elemento!=null && $elemento->getCerrado()!= null && $elemento->getCerrado()== 1){ echo(" checked='checked' "); }else{ echo("onclick='return confirm(\"¿Esta seguro que desea cerrar este periodo?, recuerde que una vez cerrado ya no es posible modificar la información capturada para este periodo.\")' "); }?> >
            </td>
      </tr>

      <tr>
        <td>Fecha de Inicio*(dd-mm-aaaa): </td>
        <td>
          <input type="text" name="finicio" id="finicio" value='<?php if($elemento!=null && $elemento->getFechaInicial()!= null) { echo(date("d-m-Y", strtotime($elemento->getFechaInicial()))); }?>'>
            </td>
            <td>Fecha Final*(dd-mm-aaaa):</td>
        <td>
          <input type="text" name="ffin" id="ffin" value='<?php if($elemento!=null && $elemento->getFechaFinal()!= null){ echo(date("d-m-Y", strtotime($elemento->getFechaFinal()))); }?>'>

            </td>
      </tr>

      <tr>
        <td align="center" colspan="4"><input name="guardar" type="submit" value="Guardar"  class='btn' />
        &nbsp;&nbsp;&nbsp;&nbsp;<a href='lista_periodo.php' class='liga_btn'> Cancelar </a>
        </td>
      </tr>
    </table>
    <?php if($elemento!=null && $elemento->getId()!=null) {echo("<input type='hidden' name='id' value='".$elemento->getId()."' />");}?>
</form>
</div>
<?php include 'footer.php' ?>
<script type="text/javascript">

			
 var frmvalidator  = new Validator("fperiodo");
 frmvalidator.addValidation("periodo","req","Por favor capture el período.");
frmvalidator.addValidation("periodo","minlen=4","El período debe ser de 4 dígitos, que corresponden al año.");
 frmvalidator.addValidation("periodo","numeric","El período debe ser numérico."); 
 frmvalidator.addValidation("finicio","req","Por favor capture la fecha de inicio del período.");
 frmvalidator.addValidation("ffin","req","Por favor capture la fecha final del período.");
 
 Calendar.setup({ inputField : "finicio", ifFormat : "%d-%m-%Y", button: "finicio" });
 Calendar.setup({ inputField : "ffin", ifFormat : "%d-%m-%Y", button: "ffin" });
</script>
</body>
</html>

