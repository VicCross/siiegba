<?php
	include_once("../src/classes/Catalogo.class.php");
	$idProyecto = $_POST['proyecto'];
	$idExpoTemp = $_POST['expotemp'];
	$ampliacion = $_POST['ampliacion'];
	$catalogo = new Catalogo();
	$expot = "";
	$hayexpot = "";
	if($idProyecto == 48){
		$hayexpot = "idExpoTemp, ";
		$expot = ", " . $idExpoTemp;	
	}
	$insert = "INSERT INTO sie_ingresos_INBA (idProyecto, " . $hayexpot . "fecha, monto) VALUES (" . $idProyecto . $expot . ", curdate(), " . $ampliacion . ")";
	$res = $catalogo->obtenerLista($insert);
	if($res == "1"){
		$update = "UPDATE sie_cat_linea_accion SET saldoInicial = saldoInicial + " . $ampliacion . ", saldoAnterior = saldoAnterior + " . $ampliacion . " WHERE cla_id_lineaaccion = " . $idProyecto;
		$res = $catalogo->obtenerLista($update);
		$update = "UPDATE sie_cat_expo_temp SET saldo = saldo + " . $ampliacion . ", saldoAnterior = saldoAnterior + " . $ampliacion . " WHERE id_linea_accion = " . $idProyecto . " AND id_Subproyecto = " . $idExpoTemp;
		$catalogo->obtenerLista($update);
		$respuesta = "Éxito al ingresar la ampliación al Proyecto.";
	}else{
		$respuesta = "Error al ingresar la ampliación al Proyecto.";	
	}
	header("Location: lista_linea_accion.php?respuesta=".$respuesta);
?>