<?php
session_start();

if (!isset($_SESSION['idUsuario']) || is_null($_SESSION['idUsuario'])) {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php?err=2");}

header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
//include_once("../src/classes/PermisosMenu.class.php");
include_once("../src/classes/Catalogo.class.php");

$pagina_lista = "catalogos/lista_permisosusuario.php";
$id = "";
$id1 = "";
$idPuesto = "";
$alta = "";
$baja = "";
$modificacion = "";
$consulta = "";
$read = "";
$idPerfil = $_GET['idPerfil'];
$idSubmenu = $_GET['idSubmenu'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<body>


<?php $respuesta = NULL;
    if(isset($_GET['respuesta'])){ $respuesta = $_GET['respuesta']; }?>  
<div class="contenido">
    <p class="titulo_cat1">Catálogos > Catálogos de Estructura > Permisos de Usuario > Modificar Perfil</p><br/> 
    <?php 
		//Vamos a revisar si ya está en la BD o no.
		$catalogo = new Catalogo();
		$query = $catalogo->obtenerLista("SELECT COUNT(*) as num FROM sie_permisos_usuario WHERE cper_id_perfil = " . $idPerfil . " AND id_submenu = " . $idSubmenu);
		$yaEnBD = false;
		if($rs = mysql_fetch_array($query)){
			if($rs['num'] > 0) $yaEnBD = true; 	
		}
        if($respuesta!=null){
            echo("<div align='center' class='msj'>".$respuesta."</div>");
        }
		
    ?>
	<form name="permisos" method="post" action="inserta_permisos_usuario.php">
    <table width="55%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
    <tr>
    <td width="20%">Perfil:</td>
    <td>
        <select id="perfil" name="perfil">
            <?php
            $query = $catalogo->obtenerLista("SELECT CPER_ID_PERFIL, CPER_DESCRIPCION FROM sie_cat_perfiles WHERE CPER_ID_PERFIL = " . $idPerfil);
            while ($rs = mysql_fetch_array($query)) {
                echo "<option value='" . $rs['CPER_ID_PERFIL'] . "'>" . $rs['CPER_DESCRIPCION'] . "</option>";
            }
            ?>
        </select>
    </td>      
  	</tr>
	<tr>
        <td width="20%">Submenu:         </td>
        <td>
        <select id="submenu" name="submenu">
            <?php
            $query = $catalogo->obtenerLista("SELECT ID_SUBMENU, NOM_SUBMENU FROM sie_cat_submenu where ID_SUBMENU = ".$idSubmenu);
            while ($rs = mysql_fetch_array($query)) {
                echo "<option value='" . $rs['ID_SUBMENU'] . "'>" . $rs['NOM_SUBMENU'] . " </option>";
            }
            ?>
        </select>
                        </td>
      </tr>
      
      <tr>
      <?php 
	  	//Ver si ya tiene permisos
		if($yaEnBD){
			$query = $catalogo->obtenerLista("SELECT alta, baja, cambio, consulta FROM sie_permisos_usuario WHERE cper_id_perfil = " . $idPerfil . " AND id_submenu = " . $idSubmenu);
			if($rs = mysql_fetch_array($query)){
				if($rs['alta'] == 1) $alta = "checked";
				if($rs['baja'] == 1) $baja = "checked";
				if($rs['cambio'] == 1) $modificacion = "checked";
				if($rs['consulta'] == 1) $consulta = "checked";		
			}
		}
	  ?>
     <td><input type="checkbox" name="permisos[]" id="alta" <?php echo $alta; ?> value="1"/>Alta</td>
        <td><input type="checkbox" name="permisos[]" id="baja" <?php echo $baja; ?> value="2"/>Baja</td>
        
        
      </tr>
 <tr>
     <td><input type="checkbox" name="permisos[]" id="modificacion" <?php echo $modificacion;?> value="3"/>Modificar</td>
     <td><input type="checkbox" name="permisos[]" id="consulta" <?php echo $consulta; ?> value="4"/>Consulta</td>
      </tr> 
 <td align="center" colspan="2"><input name="guardar" type="submit" value="Guardar"  class='btn' /><input type="hidden" name="yaenBD" value="<?php echo $yaEnBD;?>"/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='lista_permisosusuario.php' class='liga_btn'> Cancelar </a>
        </td>
	  
	 </table>
      </form>        
	 
	
			<br> </br>
		<br> </br>
				<div align="center"><a href='menu_catalogos.php' class='liga_btn'> Regresar Menú Catálogos </a></div>
		
		
   </div>
</body>
</html>