<?php
session_start();
include_once("../src/classes/Catalogo.class.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Nueva Gestión</title>
</head>
<script>
	function mostrarLista()
	{
		var id = $("#tipo").val();	
		if(id != 0) $("#lider").show();
		else $("#lider").hide();
		$.ajax({
		 	url: "../src/mx/com/virreinato/web/CatFirmas.php",
		 	type: "POST",
		 	data: "opc=" + id,//opc 1: Proyecto; opc 2: Área
		 	success: function(data){ $("#areaproy").html(data);}
		})	
	}
	
	function mostrarLider()
	{
		var id = $("#tipo").val();
		var id2 = $("#areaproy").val();	
		$.ajax({
		 	url: "../src/mx/com/virreinato/web/CatFirmas.php",
		 	type: "POST",
		 	data: "opc=" + id + "&idAP=" + id2,//opc 1: Proyecto; opc 2: Área
		 	success: function(data){ $("#lider").html(data);}
		})
	}
</script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<?php
$catalogo = new Catalogo();
//$select = "SELECT MAX(gp.folio) AS noFolio FROM sie_cat_gestpresup gp, sie_cat_areas a WHERE gp.id_area = a.car_id_area";
?>
<body>
	<div class="contenido"> 
        <p class="titulo_cat1">Catálogos > Catálogos de Operación > Catálogo de Firmas ></p>
        <form name="firmas" id="firmas" method="post" action="insertaFirma.php" enctype="multipart/form-data">
            <table width="55%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
            	<tr>
                	<td colspan="2" align="center"><h3>Alta Firma</h3></td>
    			</tr>
                <tr>
                	<td width="20%">Líder de:</td>
                    <td>
                    	<select name="tipo" id="tipo" onchange="mostrarLista()">
                        	<option value="0">---- Selecciona ----</option>
                            <option value="1">Proyecto</option>
                            <option value="2">Área</option>
                        </select>
                    </td>
                </tr>
                <tr>
                	<td width="20%">Area/Proyecto*:</td>
                    <td><select name="areaproy" id="areaproy" onchange="mostrarLider()"></select></td>
                </tr>
                <tr>
                	<td width="20%">Líderes sin firma*:</td>
                    <td><select name="lider" id="lider" style="display:none;"></select></td>
                </tr>
                <tr>
                	<td width="20%">Firma* (Menor a 2MB):</td>
                	<td><input name="firma" type="file" id="firma" accept="image/*"/></td>
                 </tr>
                <tr>
                	<td colspan="2" align="center"><input type="submit" value="Enviar" style="cursor:pointer" class='btn'/>       <a href='lista_firmas.php' class='liga_btn'> Cancelar </a></td>
                </tr>
            </table>
        </form>
    </div>
    <?php include 'footer.php' ?>
</body>
<script>
 var frmvalidator  = new Validator("firmas");
 frmvalidator.addValidation("tipo", "dontselect=0", "Por favor seleccione el tipo de líder.");
 frmvalidator.addValidation("areaproy", "dontselect=0", "Por favor seleccione el Area/Proyecto.");
 frmvalidator.addValidation("lider", "dontselect=0", "Por favor seleccione al líder que se desea agregar firma.");
 frmvalidator.addValidation("firma", "req=0", "Por favor seleccione la imagen de su firma.");
</script>
</html>