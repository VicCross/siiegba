<?php

include_once("../src/mx/com/virreinato/dao/ExposicionesTemporalesDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/dao/AreaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ExposicionesTemporales.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Area.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos del Palacio de Bellas Artes</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<link href="../css/calendario.css" type="text/css" rel="stylesheet">
<script src="../js/calendar.js" type="text/javascript"></script>
<script src="../js/calendar-es.js" type="text/javascript"></script>
<script src="../js/calendar-setup.js" type="text/javascript"></script>


<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<body>
 <?php
    $error = NULL;
    if (isset($_REQUEST['error'])) {
        $error = $_REQUEST['error'];
    }
    ?>
<div class="contenido"> 
    <br/>
	<p class="titulo_cat1"> Catálogos > Catálogos de Proyectos > Exposición Temporal >
    <?php
	$dao=new ExposicionesTemporalesDaoJdbc();
	$elemento=new ExposicionesTemporales();

	if (isset($_GET['id'])){
		echo("Modificar Exposición Temporal");
		$elemento=$dao->getElemento($_GET['id']);	
	}	
	else{
		echo("Alta de Exposición Temporal");
	}	
	?>
     </p>
    
    <br/>
    <?php if ($error != null) echo("<div align='center' class='msj'>" . $error . "</div>"); ?>
    <form id="fproyecto" name="fproyecto" method="post" action="../src/mx/com/virreinato/web/CatExpo.php">
          
		<table width="90%%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
		 <tr>  </tr>
		
		<tr>
		   <td width="%">Nombre  Exposición*:</td>
		      <td width="%">
              <input type="text" name="nombre" id="nombre" size="20" maxlength='40' value='<?php if ($elemento != null && $elemento->getNombre() != null) echo($elemento->getNombre()); ?>' >
              </td>
		</tr>	  
			  
		 <tr>
            <td>Fecha de inicio* </td> 
		    <td>
                <input name="fechaI" id="fechaI" type="date" size="20" maxlength='10' value='<?php if($elemento!=null && $elemento->getFechaI()!= null) {echo(date("Y-m-d",strtotime($elemento->getFechaI()))); }?>'>
            </td>
		</tr>    
<tr>		
		<td>Fecha clausura* :</td>  <!-- año mesy dia -->
		    <td>
		    <input name="fechaF" id="fechaF" type="date" size="20" maxlength='10' value='<?php if($elemento!=null && $elemento->getFechaF()!= null) { echo(date("Y-m-d",strtotime($elemento->getFechaF()))); }?>'>
     		</td>
        </tr>
		
		
	<!--  <tr>
		   <td>Área que coordina*:</td>
		    <td>
                           
            <select name="area_coordina" id="area_coordina" style="width:260px">
		      <option value='0'>Selecciona</option>    
                  <?php
                   $area=new AreaDaoJdbc();
                        $areas=$area->obtieneAreas();
                        $a = new Area();
                            foreach($areas as $a){
                              $sel="";
                              if($elemento!= null && $elemento->getAreaCoordina()!= null ){
                                      $aux3 = $elemento->getAreaCoordina();
                                      if($a->getId() == $aux3->getId())
                                      {        $sel="selected='selected'"; }
                              }
                             // echo("<option value='".$a->getId()."' ".$sel." >".$a->getDescripcion()."</option>");
                            }
                        ?> 
                    </select>
             
		    </td> 	
		</tr> -->
		<!--<tr>
		<td>Tipo*:</td>
        <td>
         <?php
		//if(!is_null($elemento)){
			//$tipo = $elemento->getTipo();	
		//)}
		?>
        	<select id="tipo" name="tipo" style="width:260px">
        <option value="0">Selecciona</option>
                <option value="Artes Gráficas" <?php// if(!is_null($Tipo) && $Tipo=="Artes Gráficas") echo("selected");?>>Artes Gráficas</option>
                 <option value="Artes Decorativas" <?php //if(!is_null($Tipo) && $Tipo=="Artes Decorativas") echo("selected");?>>Artes Decorativas</option>
                <option value="Pintura" <?php //if(!is_null($Tipo) && $Tipo=="Pintura") echo("selected");?>>Pintura</option>
                <option value="Dibujo" <?php //if(!is_null($Tipo) && $Tipo=="Dibujo") echo("selected");?>>Dibujo</option>
                <option value="Fotografía" <?php //if(!is_null($Tipo) && $Tipo=="Fotografía") echo("selected");?>>Fotografía</option>
                <option value="Manuscritos" <?php //if(!is_null($Tipo) && $Tipo=="Manuscritos") echo("selected");?>>Manuscritos</option>
                <option value="Libros" <?php //if(!is_null($Tipo) && $Tipo=="Libros") echo("selected");?>>Libros</option>
				<option value="Grabados" <?php //if(!is_null($Tipo) && $Tipo=="Grabados") echo("selected");?>>Grabados</option>
               
               
        	</select>
		
		</td>
		</tr>
        
        
		<tr>
        <!-- no se puede modificar -->
		<!--td>Saldo*:</td>
		    <td>
		    <input name="saldo" id="saldo" type="text" size="20" maxlength='10' value='<?php// if ($elemento != null && $elemento->getSaldo() != null) echo($elemento->getSaldo()); ?>'>
		    </td>
           <td>Saldo Anterior*:</td>
            <td>
		    <input name="saldoAnterior" id="saldoAnterior" type="text" size="20" maxlength='10' value='<?php //if ($elemento != null && $elemento->getSaldoAnterior() != null) echo($elemento->getSaldoAnterior()); ?>'>
		    </td>
		<!--</tr>-->  
        <tr>
		<td>Año*:</td>
            <td>
             <select name="periodo" id="periodo" style="width:260px"> <!--se selecciona el año -->
                <option value='0'>Selecciona</option>
                 <?php
                    $periodo=new PeriodoDaoJdbc();
                    $periodos=$periodo->obtieneListadoAbierto(); 
                    $b = new Periodo();
                    foreach($periodos as $b){
                       $sel="";
                        if($elemento!= null && $elemento->getPeriodo()!= null ){
                              $aux = $elemento->getPeriodo();
                              if($b->getId() == $aux->getId())
                              {  $sel="selected='selected'"; }
                      }
                      echo("<option value='".$b->getId()."' ".$sel." >".$b->getPeriodo()."</option>");
                    }
                ?>
                </select>
                </td>
                </tr>
		<tr>
        <td align="center" colspan="8" ><input name="guardar" type="submit" value="Guardar"  class='btn' />
        &nbsp;&nbsp;&nbsp;&nbsp;<a href='lista_expo.php' class='liga_btn'> Cancelar </a>
        </td>
      </tr>
		   
        </table>
        <?php if($elemento!=null && $elemento->getIdExposicionT()!=null) {echo("<input  name='id' value='".$elemento->getIdExposicionT()."' />");}?>  <!-- tenia type='hidden' -->
    </form>
	
	     
  
</div>
<?php include 'footer.php' ?>
<script type="text/javascript">

function DoCustomValidation(){
 	
 	var numero=document.getElementById('numero').value;
 	var costo=document.getElementById('costo').value;
 	
 }

 var frmvalidator  = new Validator("fproyecto");
 
 
 
 frmvalidator.addValidation("id_periodo","dontselect=0","Por favor seleccione el año.");
 frmvalidator.addValidation("finicio_tot","req","Por favor capture la fecha de inicio de la exposición.");
 frmvalidator.addValidation("ffin_tot","req","Por favor capture la fecha final de la exposición.");
 
 
 
 frmvalidator.setAddnlValidationFunction(DoCustomValidation);
 frmvalidator.DoCustomValidation;
 
 
 Calendar.setup({ inputField : "finicio_tot", ifFormat : "%Y-%m-%d", button: "finicio_tot" });
 Calendar.setup({ inputField : "ffin_tot", ifFormat :"%Y-%m-%d", button: "ffin_tot" });
 Calendar.setup({ inputField : "finicio_eje", ifFormat : "%Y-%m-%d", button: "finicio_eje" });
 Calendar.setup({ inputField : "ffin_eje", ifFormat : "%Y-%m-%d", button: "ffin_eje" });
 
 function Regresar(){
 	window.location="lista_proyecto.php";
 }

 
</script>

</body>
</html>