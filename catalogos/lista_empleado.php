<?php
session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once("../src/mx/com/virreinato/dao/EmpleadoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Empleado.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="../css/lista.css">
        <script type="text/javascript" src="../media/js/complete.js"></script>
        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
        <script type="text/javascript" src="../js/lista.js"></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
<input type="hidden" id="columnas_visibles" name="columnas_visibles" value="8"/>
    <input type="hidden" id="total_columnas" name="total_columnas" value="10"/>
</head>
<body>
<?php $respuesta = NULL;
    if(isset($_GET['respuesta'])){ $respuesta = $_GET['respuesta']; }?> 
<div class="contenido">
    <p class="titulo_cat1">Catálogos > Catálogos de Operación  > Catálogo de Empleados</p><br/>
    <?php 
        if($respuesta!=null){
            echo("<div align='center' class='msj'>".$respuesta."</div>");
        }
    ?>
	  <div align="right"><a href='agrega_empleado.php' class='liga_btn'> Agrega Empleado</a></div>
	  <br> </br>
    <table border="0" cellspacing="0" cellpadding="5" class='dataTable' align='center' >
    	<thead>
            <tr>
            	<th width="11.25%">Apellido Paterno</th>
                <th width="11.25%">Apellido Materno</th>
                <th width="11.25%">Nombre</th>
                <th width="11.25%">RFC</th>
                <th width="11.25%">Puesto</th>
                <th width="11.25%">Tipo de Nómina</th>
                <th width="11.25%">Sueldo Neto</th>
                <th width="11.25%">Número de Empleado</th>
                <th width="5%"></th>
                <th width="5%"></th>
            </tr>
        </thead>
        <tfoot>
        	<tr>
                <th width="11.25%"></th>
                <th width="11.25%"></th>
                <th width="11.25%"></th>
                <th width="11.25%"></th>
                <th width="11.25%"></th>
                <th width="11.25%"></th>
                <th width="11.25%"></th>
                <th width="11.25%"></th>
                <th width="5%"></th>
                <th width="5%"></th>
            </tr>
        </tfoot>
    <?php
        $dao = new EmpleadoDaoJdbc();
        $lista=$dao->obtieneListado();
        $elemento=new Empleado();
 
        foreach($lista as $elemento){
    ?>
    <tr>
        <td><?php echo($elemento->getApPaterno());?></td>
        <td><?php echo($elemento->getApMaterno());?></td>
        <td><?php echo($elemento->getNombre());?></td>
        <td><?php if($elemento->getRfc() !=null) echo($elemento->getRfc());?></td>
        <td><?php if($elemento->getPuesto() !=null) echo($elemento->getPuesto());?></td>
        <td><?php if($elemento->getTipoNomina() !=null) echo($elemento->getTipoNomina());?></td>
        <td><?php if($elemento->getSueldoNeto() !=null && $elemento->getSueldoNeto()!= 0.0){ echo(number_format($elemento->getSueldoNeto(),2)); } ?></td>
        <td><?php if($elemento->getNumero() !=null) echo($elemento->getNumero());?></td>
        <td><a href='agrega_empleado.php?id=<?php  echo($elemento->getId());?>' class='liga_cat'><img src="../img/Pencil3.png" alt="Editar" style="border: 0px none;" ></a></td>
        <td><a href='../src/mx/com/virreinato/web/CatEmpleado.php?id=<?php echo($elemento->getId());?>' onclick='return confirm("¿Esta seguro que desea eliminar este registro?")' class='liga_cat'><img src="../img/DeleteRed.png" alt="Borrar" style="border: 0px none;"></a></td>
      </tr>	
    <?php			
		}
	  ?>
    </table>
	<br>
	
</div>
<?php include 'footer.php' ?>
</body>
</html>


