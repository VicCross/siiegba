<?php
include_once("../src/mx/com/virreinato/dao/ProductosDaoJdbc.class.php");

 // ProveedorDaoJdbc.class.php
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProductosPatronato.class.php");
// proveedor.class.php
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<body>
 <?php
    $error = NULL;
    if (isset($_REQUEST['error'])) {
        $error = $_REQUEST['error'];
    }
    ?>
<div class="contenido"> 
    <p class="titulo_cat1">Catálogos > Catálogos de Operación >
    <?php
	$dao=new ProductosDaoJdbc();
	$elemento=new Productos();

	if(isset($_GET['id'])){
		echo("Modificar Producto");
		$elemento=$dao->obtieneElementoProd($_GET['id']);	
	}	
	else{
		echo("Alta de Nuevo Producto");
	}	
    ?>
    </p>
    <br/>
    <?php if ($error != null) echo("<div align='center' class='msj'>" . $error . "</div>"); ?>
    <form id="fproducto" name="fproducto" method="post" action="../src/mx/com/virreinato/web/CatProductos.php">
        <table width="90%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
          <tr>
            <td width='15%'>Nombre Producto*:</td>
            <td width='35%'><input type="text" name="nombre_prod" id="nombre_prod" size='50' value='<?php if($elemento!=null && $elemento->getNombre_prod()!= null) echo($elemento->getNombre_prod());?>'/></td>
            <td width='15%'>Descripcion del Producto*:</td>
            <td width='35%'><input type="text" name="descripcion_prod" id="descripcion_prod" size='50' value='<?php if($elemento!=null && $elemento->getDescripcion_prod()!= null) echo($elemento->getDescripcion_prod());?>'/></td>
          </tr>

          <tr>
            <td>Inventario Inicial*:</td>
            <td><input type="text" name="inventario_inicial" id="inventario_inicial" maxlength='100' value='<?php if($elemento!=null && $elemento->getInventario_ini()!= null) echo($elemento->getInventario_ini());?>' /></td>
            <td>Inventario Actual*</td>
            <td><input type="text" name="inventario_actual"  id="inventario_actual"  value='<?php if($elemento!=null && $elemento->getInventario_act()!= null) echo($elemento->getInventario_act());?>'/></td>
          </tr>

          <tr>
            <td>Estatus Producto:</td>
            <td>
				<select name="estatus_prod" id="estatus_prod">
					<option value='1' <?php if($elemento!=null && $elemento->getEstatus_prod()!= null && $elemento->getEstatus_prod()==1 ) echo("selected='selected'"); ?>>Activo</option>
					<option value='0' <?php if($elemento!=null && $elemento->getEstatus_prod()!= null && $elemento->getEstatus_prod()==0 ) echo("selected='selected'"); ?>>Inactivo</option>
                </select>
          </tr>

          <tr>
            <td align="center" colspan="4"><input name="guardar" type="submit" value="Guardar"  class='btn' />
            &nbsp;&nbsp;&nbsp;&nbsp;<a href='lista_Patronatoprod.php' class='liga_btn'> Cancelar </a>
            </td>
          </tr>

        </table>
        <?php if($elemento!=null && $elemento->getId_prod()!=null) echo("<input type='hidden' name='id' value='".$elemento->getId_prod()."' />");?>
    </form>
</div>
<?php include 'footer.php' ?>
<script type="text/javascript">
 var frmvalidator  = new Validator("fproducto");
 
 frmvalidator.addValidation("nombre_prod","req","Por favor capture el nombre del producto.");
 frmvalidator.addValidation("descripcion_prod","req","Por favor capture la descripción.");
 frmvalidator.addValidation("inventario_inicial","req","Por favor capture el inventario inicial.");

 
</script>

</body>
</html>
