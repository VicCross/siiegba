<?php
include_once("../src/mx/com/virreinato/dao/CapituloDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Capitulo.class.php");
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<body>
<?php
    $error = NULL;
    if (isset($_REQUEST['error'])) {
        $error = $_REQUEST['error'];
    }
?>   
<div class="contenido">
    <p class="titulo_cat1">Catálogos > Catálogos de Estructura >
        <?php
            $dao=new CapituloDaoJdbc();
            $elemento=new Capitulo(null,null,null);

            if(isset($_GET['id'])){
                    echo("Modificar Capítulo Presupuestal");
                    $elemento=$dao->obtieneElemento($_GET['id']);	
            }	
            else{
                    echo("Alta de Nuevo Capítulo Presupuestal");
            }	
	?>
        </p>
	<br/>
	<?php if ($error != null) echo("<div align='center' class='msj'>" . $error . "</div>"); ?>
            <form id="fcapitulo" name="fcapitulo" method="post" action="../src/mx/com/virreinato/web/CatCapitulo.php">
            <table width="60%%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
              <tr>
                <td width="19%">Capítulo Presupuestal*:</td>
                <td width="81%">
                  <input type="text" name="capitulo" id="capitulo" value='<?php if($elemento!=null && $elemento->getCapitulo()!= null) echo($elemento->getCapitulo());?>'>
                    </td>
              </tr>
              <tr>
                <td>Descripción*:</td>
                <td><input name="descripcion" type="text" size="50" value='<?php if($elemento!=null && $elemento->getDescripcion()!= null) echo($elemento->getDescripcion());?>' /></td>
              </tr>
              <tr>
                <td align="center" colspan="2"><input name="guardar" type="submit" value="Guardar"  class='btn' />
                &nbsp;&nbsp;&nbsp;&nbsp;<a href='lista_capitulo.php' class='liga_btn'> Cancelar </a>
                </td>
              </tr>
            </table>
            <?php if($elemento!=null && $elemento->getId()!=null) echo("<input type='hidden' name='id' value='".$elemento->getId()."' />");?>
	</form>
</div>
<?php include 'footer.php' ?>
<script type="text/javascript">
 var frmvalidator  = new Validator("fcapitulo");
 frmvalidator.addValidation("capitulo","req","Por favor capture el número del capítulo presupuestal.");
 frmvalidator.addValidation("descripcion","req","Por favor capture la descripcion del capítulo presupuestal.");
</script>
</body>
</html>