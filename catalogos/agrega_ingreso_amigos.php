<?php
include_once("../src/mx/com/virreinato/dao/AmigosDaoJdbc.class.php"); // ProductosDaoJdbc.class.php

 // ProveedorDaoJdbc.class.php
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/AmigosPatronato.class.php"); // ProductosPatronato.class.php
// proveedor.class.php
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<body>
 <?php
    $error = NULL;
    if (isset($_REQUEST['error'])) {
        $error = $_REQUEST['error'];
    }
    ?>
<div class="contenido"> 
    <p class="titulo_cat1">Catálogos > Catálogos de Operación >
    <?php
	$dao=new AmigosDaoJdbc(); // ProductosDaoJdbc()
	$elemento=new Amigos(); // Amigos

	if(isset($_GET['id'])){
		echo("Modificar Tipo de Ingreso ");
		$elemento=$dao->obtieneElementoAmigo($_GET['id']);	
	}	
	else{
		echo("Alta de Nuevo Tipo de Ingreso");
	}	
    ?>
    </p>
    <br/>
    <?php if ($error != null) echo("<div align='center' class='msj'>" . $error . "</div>"); ?>
    <form id="famigos" name="famigos" method="post" action="../src/mx/com/virreinato/web/CatAmigos.php">  <!--CatProductos.php -->
        <table width="90%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
          <tr>
            <td width='15%'>Nombre *:</td>
            <td width='35%'><input type="text" name="nombre_tipo_amigos" id="nombre_tipo_amigos" size='50' value='<?php if($elemento!=null && $elemento->getNombre_tipo_amigos()!= null) echo($elemento->getNombre_tipo_amigos());?>'/></td>
            <td width='15%'>Descripcion del tipo de Ingreso*:</td>
            <td width='35%'><input type="text" name="descripcion_tipo_amigos" id="descripcion_tipo_amigos" size='50' value='<?php if($elemento!=null && $elemento->getDescripcion_tipo_amigos()!= null) echo($elemento->getDescripcion_tipo_amigos());?>'/></td>
          </tr>

          
          <tr>
            <td align="center" colspan="4"><input name="guardar" type="submit" value="Guardar"  class='btn' />
            &nbsp;&nbsp;&nbsp;&nbsp;<a href='lista_amigos.php' class='liga_btn'> Cancelar </a>
            </td>
          </tr>

        </table>
        <?php if($elemento!=null && $elemento->getId_tipo_amigos()!=null) echo("<input type='hidden' name='id' value='".$elemento->getId_tipo_amigos()."' />");?>
    </form>
</div>
<?php include 'footer.php' ?>
<script type="text/javascript">
 var frmvalidator  = new Validator("famigos"); // fproducto
 
 frmvalidator.addValidation("nombre_tipo_amigos","req","Por favor capture el nombre del tipo de ingreso.");
 frmvalidator.addValidation("descripcion_tipo_amigos","req","Por favor capture la descripción.");
 

 
</script>

</body>
</html>
