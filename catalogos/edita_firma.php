<?php
session_start();
include_once("../src/mx/com/virreinato/dao/LineaAccionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/LineaAccion.class.php");
include_once("../src/mx/com/virreinato/dao/AreaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Area.class.php");
include_once("../src/mx/com/virreinato/dao/EmpleadoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Empleado.class.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Nueva Gestión</title>
</head>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<?php
$catalogo = new Catalogo();
//$select = "SELECT MAX(gp.folio) AS noFolio FROM sie_cat_gestpresup gp, sie_cat_areas a WHERE gp.id_area = a.car_id_area";
?>
<body>
	<div class="contenido"> 
        <p class="titulo_cat1">Catálogos > Catálogos de Operación > Catálogo de Firmas ></p>
        <form name="firmas" id="firmas" method="post" action="insertaFirma.php" enctype="multipart/form-data">
            <table width="55%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
            	<tr>
                	<td colspan="2" align="center"><h3>Nueva Gestión</h3></td>
    			</tr>
                <tr>
                	<td width="20%">Líder de:</td>
                    <td>
                    	<?php
							$idTipo = 0; 
							$tipo = "";
							$id = 0;							
							if(isset($_GET['tipo']) && isset($_GET['id'])){
								$idTipo = $_GET['tipo'];
								$id = $_GET['id'];
								if($idTipo == "1"){
									$tipo = "Proyecto";
									$dao = new LineaAccionDaoJdbc();
									$elemento = new LineaAccion();
									$elemento = $dao->obtieneElemento($id);
								}
								else if($idTipo == "2"){
									$tipo = "Área";
									$dao = new AreaDaoJdbc();
									$elemento = new Area();
									$elemento = $dao->obtieneArea($id);
								}
							}
						?>
                    	<select name="tipo" id="tipo">
                        	<option value="<?php echo($idTipo);?>"><?php echo($tipo);?></option>
                        </select>
                    </td>
                </tr>
                <tr>
                	<td width="20%"><?php echo($tipo);?>:</td>
                    <td>
                    	<select name="areaproy" id="areaproy">
                        	<option value="<?php echo($id);?>"><?php if($idTipo == "1")echo($elemento->getLineaAccion()); else if($idTipo == "2")echo($elemento->getDescripcion());?></option>
                    	</select>
                    </td>
                </tr>
                <tr>
                	<td width="20%">Líder del <?php echo($tipo);?>:</td>
                    <td>
                    	<?php
							$daoEmp = new EmpleadoDaoJdbc();
							$empleado = new Empleado();
							$empleado = $daoEmp->obtieneElemento($elemento->getIdLider());
						?>
                    	<select name="lider" id="lider">
                        	<option value="<?php echo($elemento->getIdLider());?>"><?php echo($empleado->getNombre() . " " . $empleado->getApPaterno() . " " . $empleado->getApMaterno());?></option>
                    	</select>
                    </td>
                </tr>
                <tr>
                	<td width="20%">Firma* (Menor a 2MB):</td>
                	<td><input name="firma" type="file" id="firma" accept="image/*"/></td>
                 </tr>
                <tr>
                	<td colspan="2" align="center"><input type="submit" value="Enviar" style="cursor:pointer" class='btn'/>       <a href='lista_firmas.php' class='liga_btn'> Cancelar </a></td>
                </tr>
            </table>
        </form>
    </div>
    <?php include 'footer.php' ?>
</body>
<script>
 var frmvalidator  = new Validator("firmas");
 frmvalidator.addValidation("tipo", "dontselect=0", "Por favor seleccione el tipo de líder.");
 frmvalidator.addValidation("areaproy", "dontselect=0", "Por favor seleccione el Area/Proyecto.");
 frmvalidator.addValidation("lider", "dontselect=0", "Por favor seleccione al líder que se desea agregar firma.");
 frmvalidator.addValidation("firma", "req=0", "Por favor seleccione la imagen de su firma.");
</script>
</html>