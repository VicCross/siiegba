<?php
$perfil = null;
$user = null;
session_start();
include_once("../src/mx/com/virreinato/dao/ProyectoMetaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProyectoMeta.class.php");
include_once("../src/mx/com/virreinato/dao/ProgramaInstitucionalDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProgramaOperativo.class.php");
include_once("../src/mx/com/virreinato/dao/ProgramaOperativoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProgramaInstitucional.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>

<!DOCTYPE html">
<html>
<head>

<link rel="stylesheet" type="text/css" href="../css/style.css" />
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
<script language="JavaScript">
	
	var cheque = "";
	var idCh = "";
	var perfil;
</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Editar Presupuesto</title>
</head>
<body>
<?php 
    $respuesta = null;
    $error = null;
    $id = null;
    
   if(isset($_GET["respuesta"]))
        {$respuesta= (String) $_GET["respuesta"];}
   if(isset($_GET["error"]))
        {$error = (String) $_GET["error"];}
		
	$idProyecto = "";
   	if(isset($_GET['idProyecto'])){ $idProyecto = $_GET['idProyecto']; }
    $desc = NULL;
    if(isset($_GET['desc'])){ $desc = $_GET['desc']; }
	$nomMeta = null;
	if(isset($_GET['Nmetas'])){ $nomMeta = $_GET['Nmetas'];}
	$PI = NULL;
	$PO = NULL;
	$idMeta = NULL;
	if(isset($_GET['PI'])){ $PI = (int)$_GET['PI'];}
	if(isset($_GET['PO'])){ $PO = (int)$_GET['PO'];}
	if(isset($_GET['idMeta'])){$idMeta = $_GET['idMeta'];}
?>
<div class="contenido">
<br/>
<p class="titulo_cat1">Cat&aacute;logos > Cat&aacute;logos de Proyectos > <a class="linkTitulo_cat1" href="agrega_proyecto.php?id=<?php $idProyecto ?>" > Definición de  Proyecto</a> > Metas de Proyectos </p>
<h2 class="titulo_cat2">&nbsp;</h2>
<br/>
<form name="frmMetas" id="frmMetas" method="post" action="../src/mx/com/virreinato/web/WebProyectoMeta.php">
    <table width="48%" border="0" cellspacing="0" cellpadding="5" class='tb_add_cat' align='center'>
    <tr class="SizeText">
        <td>
        	<?php if($idMeta != null){ ?><input type="hidden" name="id" value="<?php echo$idMeta;?>"/><?php } ?> 
            <br/><h4>Meta: </h4>
            <input type="text" id="meta" name="meta" size="40" maxlenght="10" value="<?php if($nomMeta != null){echo$nomMeta;}?>"/>
          <br/><br/>
            <h4>Programa Institucional: </h4>
            <select name="programa_inst" id="programa_inst">
            <option>--Selecciona--</option>
            <?php
  			$daoIns = new ProgramaInstitucionalDaoJdbc();
 			$listaIns = $daoIns->obtieneListado();
 			$elementoIns=new ProgramaInstitucional();
 	  		$aux = 1;
 	  		foreach($listaIns as $elementoIns){
				echo("<option value='".$elementoIns -> getIdProgramaInstitucional());
				if($aux == $PI){
					echo("' selected> ");
				}else{echo("'> ");}
					echo($elementoIns -> getProgramaInstitucional()."</option>");
				$aux++;
			}
 		  		?>
        	</select>
            <br/><br/>
            <h4>Programa Operativo </h4>
            <select name="programa_op" id="programa_op">
            <option value='0'>--Selecciona--</option>
            <?php
			$daoOp = new ProgramaOperativoDaoJdbc();
			$listaOp = $daoOp -> obtieneListado();
			$elementoOp = new ProgramaOperativo();
			$aux = 1;
			foreach($listaOp as $elementoOp){
				echo("<option value='".$elementoOp -> getIdProgramaOperativo());
				if($aux == $PO){
					echo("' selected> ");
				}else{echo("'> ");}
					echo($elementoOp -> getProgramaOperativo()."</option>");
				$aux++;
			}
			?>
            </select>
        </td>
    </tr>    
    <tr>
        <td align="center" colspan="3">
            <input type="submit" id="guardar" name="guardar" value="guardar" class="btn" /><input type="hidden" name="idProyecto" value="<?php echo$idProyecto;?>"/>
            <a href='lista_metas_proyecto.php?idProyecto=<?php echo$idProyecto?>&desc=<?php echo$desc?>' class='liga_btn'> Cancelar </a>            
        </td></tr>
            &nbsp; &nbsp; &nbsp;
            
            <?php  if( $perfil == 2 )  echo( " &nbsp; &nbsp; &nbsp;<a class='btn' href='../src/mx/com/virreinato/web/CatPresupuestoGb.php?VoboSA=".$p->getId()."'> VoBo Administrativo </a> &nbsp; &nbsp; &nbsp; <a class='btn' href='../src/mx/com/virreinato/web/CatPresupuestoGb.php?NoAceptado=".$p->getId()."&Vobo=sa '> No Aceptado </a>" );
               if( $perfil == 3 )  echo( "&nbsp; &nbsp; &nbsp; <a class='btn' href='../src/mx/com/virreinato/web/CatPresupuestoGb.php?VoboST=".$p->getId()."'> VoBo Técnico </a> &nbsp; &nbsp; &nbsp; <a class='btn' href='../src/mx/com/virreinato/web/CatPresupuestoGb.php?NoAceptado=".$p->getId()."&Vobo=st '> No Aceptado </a>" );
               if( $perfil == 4 )  echo( "&nbsp; &nbsp; &nbsp; <a class='btn' href='../src/mx/com/virreinato/web/CatPresupuestoGb.php?VoboDirector=".$p->getId()."'> VoBo Director </a> &nbsp; &nbsp; &nbsp; <a class='btn' href='../src/mx/com/virreinato/web/CatPresupuestoGb.php?NoAceptado=".$p->getId()."&Vobo=dr '> No Aceptado </a>" );?>
        </td>
    </tr>
    </table>
    <br/><br/>
    <p align="CENTER"><a href='menu_catalogos.php' class='liga_btn'> Regresar al Menú Catálogos </a>  	 
    </p>
  </form>
 </div>
 <br/><br/>
</body>
</html>
<script>
  
 Calendar.setup({ 
       inputField : "fecha", ifFormat :"%d-%m-%Y", button :"fecha"      
    });
    
</script>

