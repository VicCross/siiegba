<?php
session_start();
include_once("../src/classes/Catalogo.class.php");
include_once("../src/mx/com/virreinato/dao/IngresosINBADaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/IngresosINBA.class.php");
include_once("../src/mx/com/virreinato/dao/LineaAccionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/LineaAccion.class.php");
include_once("../src/mx/com/virreinato/dao/ExposicionesTemporalesDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ExposicionesTemporales.class.php");
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Nueva Gestión</title>
</head>
<script>
	function mostrarSaldoProy()
	{
		var id = $("#proyecto").val();
		$.ajax({
		 	 url: "../src/mx/com/virreinato/web/IngresoINBA.php",
		 	 type: "POST",
		 	 data: "idProy="+id+"&opc=1",//opc 1: Mostrar Presupuesto
		 	 success: function(data){ $("#saldoP").html(data);}
		 })
		 //mostrarSaldo(id);
		 mostrarExpoTemp();
		 mostrarDestino();
	}
	
	function mostrarExpoTemp()
	{
		var id = $("#proyecto").val();
		if(id == 48){
			$("#expotemp").show();
			var id2 = $("#expotemp").val();
			$("#saldoE").show();
			$.ajax({
		 	 url: "../src/mx/com/virreinato/web/IngresoINBA.php",
		 	 type: "POST",
		 	 data: "idProy="+id+"&idET="+id2+"&opc=2",//opc 1: Mostrar Presupuesto
		 	 success: function(data){ $("#saldoE").html(data);}
		 	})	
		}else{
			$("#expotemp").hide();
			$("#saldoE").hide();
		}	
	}
	
	function x(){
		$("#ampliacion").val("900");	
	}
	
	function mostrarDestino()
	{
		var id = $("#proyecto").val();
		
		if(id == 57){
			$("#presupOrig").show();

		}else{
			$("#presupOrig").hide();
			
		}	
	}
	
	
	
	
	
	
</script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<?php
$catalogo = new Catalogo();
if(isset($_GET['id'])){
	$dao = new IngresosINBADaoJdbc();
	$ingINBA = new IngresosINBA();
	$ingINBA = $dao->obtieneElemento($_GET['id']);
}
?>
<body onload="mostrarSaldoProy()">
	<div class="contenido"> 
        <p class="titulo_cat1">Catálogos > Catálogos de Proyectos > Catálogo de Proyectos Anuales</p>
        <form name="ingreso" id="ingreso" method="post" action="../src/mx/com/virreinato/web/CatIngresoINBA.php" enctype="multipart/form-data">
            <table width="55%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
            	<tr>
                	<td colspan="2" align="center"><h3>Ingresos INBA</h3></td>
    			</tr>
                <tr>
                	<td width="25%">Destino*:</td>
                    <td>
                    	<select name="proyecto" id="proyecto" onchange="mostrarSaldoProy()">
                        	<?php
							if(is_null($ingINBA))
                        		echo('<option value="0">----Selecciona Destino----</option>');
							?>
                            <?php
                           	$daoProy = new LineaAccionDaoJdbc();
							$elemento = new LineaAccion();
							$lista = $daoProy->obtieneListado();
                            foreach($lista as $elemento){
								if(is_null($ingINBA) || $ingINBA->getIdProyecto() == $elemento->getId()){
                            ?>
                            <option value="<?php echo($elemento->getId());?>"><?php echo($elemento->getLineaAccion());?></option>
                            <?php   
								}
                            }
							
                            ?>
                        </select>
						
                    </td>
                </tr>
				<tr>
					<td width="25%">Destino Presupuesto Original:</td>
					<td>
						<select name="presupOrig" id="presupOrig" style="display:none;" onchange="mostrarDestino()">
							<option value ="0">HONORARIOS</option>
							<option value ="1">FONDO ROTATORIO</option>
							
						
						</select>
						
					</td>
					
				</tr>
                <tr>
                	<td width="25%">Asignación del Proyecto:</td>
                    <td><select name="saldoP" id="saldoP"></select></td>
                </tr>
                <tr>
                	<td width="25%">Exposición Temporal:</td>
                    <td>
                    	<select name="expotemp" id="expotemp" style="display:none;" onchange="mostrarExpoTemp()">
                            <?php
                            $daoExpoTemp = new ExposicionesTemporalesDaoJdbc();
							$elemento = new ExposicionesTemporales();
							$lista  = $daoExpoTemp->getListado();
                            foreach ($lista as $elemento){
								if(is_null($ingINBA) || $ingINBA->getIdExpoTemp() == $elemento->getIdExposicionT()){
                            ?>
                            <option value="<?php echo($elemento->getIdExposicionT());?>"><?php echo($elemento->getNombre())?></option>
                            <?php
								}
                            }
                            ?>
                        </select>
                    </td>
                </tr>
				 <tr>
                	<td width="25%">Periodo:</td>
                    <td><select name="periodo" id="periodo" style="width:110px" > 
                    <?php
                    $daoPer = new PeriodoDaoJdbc();
                    $listaPer = $daoPer->obtieneListado();
                    $elemento = new Periodo();
                    foreach ($listaPer as $elemento) {
                        $sel = "";
                        if (isset($_GET['periodo']) && (int) ( $_GET['periodo'] ) == $elemento->getId())
                            $sel = "selected='selected'";
                        else if ($_GET['periodo'] == NULL && (int) ($elemento->getPeriodo()) == date('Y')) {
                            $sel = "selected='selected'";
                            $periodo = (String) ($elemento->getId() );
                        }
                        echo("<option value=" . $elemento->getId() . " " . $sel . " >" . $elemento->getPeriodo() . "</option>");
                    }
                    ?>  
                </select></td>
                </tr>
                <tr>
                	<td width="25%">Asignación de la Exposición Temporal:</td>
                    <td><select name="saldoE" id="saldoE" style="display:none;"></select></td>
                </tr>
                <tr>
                	<td width="25%">Asignación Inicial/Ampliación*:</td>
                    <td><input type="text" name="ampliacion" id="ampliacion" value="<?php if(!is_null($ingINBA))echo($ingINBA->getMonto());?>"/></td>
                </tr>
                <tr>
                	<td width="20%">Justificante (Menor a 2MB): <?php if(!is_null($ingINBA) && !is_null($ingINBA->getArchivo())) echo("<a href='" . $ingINBA->getArchivo() . "'>Ver Documento</a>");?></td>
                	<td><input name="archivo" type="file" id="archivo"/></td>
                 </tr>
              <tr>
                	<td colspan="2" align="center"><input type="submit" value="Enviar" style="cursor:pointer" class='btn' id="guardar" name="guardar"/></td>
                </tr>
            </table>
            <?php if(isset($_GET['id']))echo('<input type="hidden" id="id" name="id" value="' . $_GET['id'] . '" />');?>
        </form>
        <br /><br />
        <div align="center"> <a href='lista_ingresosINBA.php' class='liga_btn'>Regresar</a> </div>
    </div>
</body>
<script>
 var frmvalidator  = new Validator("ingreso");
 frmvalidator.addValidation("proyecto", "dontselect=0", "Por favor seleccione un proyecto.");
 frmvalidator.addValidation("ampliacion", "req", "Por favor ingrese el monto de la ampliación.");
 frmvalidator.addValidation("ampliacion", "num", "El valor de la ampliación debe ser númerico.");
 frmvalidator.addValidation("ampliacion", "gt=0", "La ampliación debe ser mayor a cero.");
</script>
</html>