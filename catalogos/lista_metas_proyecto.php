<?php
session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once("../src/mx/com/virreinato/dao/ProyectoMetaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProyectoMeta.class.php");
include_once("../src/mx/com/virreinato/dao/ProgramaInstitucionalDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProgramaOperativo.class.php");
include_once("../src/mx/com/virreinato/dao/ProgramaOperativoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProgramaInstitucional.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="../css/lista.css">
        <script type="text/javascript" src="../media/js/complete.js"></script>
        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
        <script type="text/javascript" src="../js/lista.js"></script>
		
		   <input type="hidden" id="columnas_visibles" name="columnas_visibles" value="3"/>
    <input type="hidden" id="total_columnas" name="total_columnas" value="5"

</script>
</head>
<body>
<?php   //  para hacer la consulta  y mostrar los registros 
   $respuesta = NULL;
    if(isset($_GET['respuesta'])){ $respuesta = $_GET['respuesta']; }
   $idProyecto = "";
   if(isset($_GET['idProyecto'])){ $idProyecto = $_GET['idProyecto']; }
    $desc = NULL;
    if(isset($_GET['desc'])){ $desc = $_GET['desc']; }
?>
<div class="contenido">
	<br/>
	<p class="titulo_cat1">Cat&aacute;logos > Cat&aacute;logos de Proyectos > <a class="linkTitulo_cat1" href="agrega_proyecto.php?id=<?php $idProyecto ?>" > Definición de  Proyecto</a> > Metas de Proyectos </p>
	 <!-- <p class="titulo_cat2" align="left" > Proyecto: <?php //echo $desc; ?> </p>-->
    <?php 
        if($respuesta!=null){
            echo("<div align='center' class='msj'>".$respuesta."</div>");
        }
    ?>
	<br/>
	<form name="frmMetas" id="frmMetas" method="post" action="../src/mx/com/virreinato/web/WebProyectoMeta.php">
	<br>  </br>
            <div align="right">
   			<a href='Agrega_Nueva_Meta.php?idProyecto=<?php echo$idProyecto; ?>&desc=<?php echo$desc;?>' class='liga_btn'> Agregar Nueva Meta </a>       
            </div>
            <br> </br>
        <table  border="0" cellspacing="0" cellpadding="5" class='dataTable' align='center' id="metasProy" >
<thead>
	   <tr>
            <th align="center" width="40%">Metas</th>
            <th width="20%">Programa Institucional</th>
            <th width="20%">Programa Operativo</th>
            <th width="10%"></th>
            <th width="10%"></th>
        </tr>
  

</thead>           
<tfoot>
		   <tr>
            <th align="center" width="40%"></th>
            <th width="20%"></th>
            <th width="20%"></th>
            <th width="10%"></th>
            <th width="10%"></th>
        </tr>
		</tfoot>
		<tbody>
        <?php
        $dao = new ProyectoMetaDaoJdbc();
        $lista=$dao->obtenerListado($idProyecto);
        $elemento=new ProyectoMeta();
        $indice = 0;
 
        foreach($lista as $elemento){
           ?>
         <tr class="SizeText">
            <td align="center" id="<?php echo $indice; ?>" ><?php echo$elemento->getDescripcion();?></td>
            <td align="center" id="programaInst<?php echo $indice; ?>"><?php echo$elemento->getProgramaInstitucional();?></td>
            <td align="center" id="programaOp<?php echo $indice; ?>"><?php echo$elemento->getProgramaOperativo();?></td>
            <td align="center" id="editMeta<?php echo $indice; ?>">
            <a href='Agrega_Nueva_Meta.php?idProyecto=<?php echo$idProyecto;?>&desc=<?php echo$desc;?>&Nmetas=<?php echo$elemento->getDescripcion();?>&idMeta=<?php echo$elemento->getIdMeta();?>&PI=<?php echo$elemento -> getIdProgramaInstitucional();?>&PO=<?php echo$elemento -> getIdProgramaOperativo();?>'>
            <acronym title="Editar"><img src="../img/Pencil3.png" width="16" height="16" alt="Editar" style="border:0;" /></acronym> </a></td>
            <td align="center" id="deleteMeta<?php echo $indice; ?>"><a href='../src/mx/com/virreinato/web/WebProyectoMeta.php?id=<?php echo($elemento->getIdMeta());?>&idProyecto=<?php echo($elemento->getIdProyecto()); ?>&desc=<?php echo($desc); ?>' class='liga_cat' onclick='return confirm("¿Esta seguro que desea eliminar este registro?")'><acronym title="Borrar"><img src="../img/DeleteRed.png" width="16" height="16" alt="Borrar" style="border:0;" /></acronym></a></td>
           <!-- <td colspan="2" id="modificarMetaProyecto<?php //echo $indice; ?>" style="display:none"><input name="guardar" type="submit" value="Guardar"  class='btn' /></td>			    -->
        </tr>	
        <?php  $indice++;
        } ?>  
		<tbody>
        </table>
	</form>
	<br>
	<div align="center">
	   <a href='agrega_proyecto.php?id=<?php echo$idProyecto; ?>' class='liga_btn'> Regresar </a>
	   &nbsp; &nbsp; &nbsp;
	   
	   
	</div>
</div>
</body>
<script>
   function Modificar(value,id,idProgramaInst,idProgramaOp ){
           //alert("Hola "+value+" "+id+" "+idProgramaInst+" "+idProgramaOp);
	   var idProyecto = <?php echo $idProyecto.";" ?>
	   var texto = $("#"+value+" ").text();
	   var programaInst = $("#programaInst"+value).text();
	   var programaOp = $("#programaOp"+value).text();	   	   	   
	   var tds1 = ' <input type="text" id="meta" name="meta" size="40" value="'+texto+'"  /> ';
           
	   var tds2 = '<select name="programa_inst" id="programa_inst"> <option>Seleccione</option>';            
	 <?php
  			$daoInst = new ProgramaInstitucionalDaoJdbc();
 			$listaInst = $daoInst->obtieneListado();
 			$elementoInst=new ProgramaInstitucional();
 	  		
 	  		foreach($listaInst as $elementoInst){
 		  		?>
		  		if(<?php echo$elementoInst->getIdProgramaInstitucional()?> == idProgramaInst)
		  			tds2 +='<option value="<?php echo$elementoInst->getIdProgramaInstitucional() ?>" selected="selected" ><?php echo$elementoInst->getProgramaInstitucional() ?></option>';
				else
					tds2 +='<option value="<?php echo$elementoInst->getIdProgramaInstitucional() ?>" ><?php echo$elementoInst->getProgramaInstitucional() ?></option>';
				<?php
 	  		}	
 		?>
	   tds2 += '</select>';
	   
	   var tds3 = ' <select name="programa_op" id="programa_op"> <option>Seleccione</option>';
	   <?php
	   		$daoOpe = new ProgramaOperativoDaoJdbc();
 			$listaOpe = $daoOpe->obtieneListado();
 			$elementoOpe=new ProgramaOperativo();
 	  		
                         foreach ($listaOpe as $elementoOpe) {
  
 		  		?>
 		  		if(<?php echo$elementoOpe->getIdProgramaOperativo()?>== idProgramaOp)
		  			tds3 +='<option value="<?php echo$elementoOpe->getIdProgramaOperativo()?>" selected="selected" ><?php echo$elementoOpe->getProgramaOperativo(); ?></option>';
		  		else
		  			tds3 +='<option value="<?php echo$elementoOpe->getIdProgramaOperativo()?>" ><?php echo$elementoOpe->getProgramaOperativo(); ?></option>';	
				<?php
 	  		}	
 		?>
	   
	   
	   tds3 += '</select><input type="hidden" name="id" value="'+id+'" /> <input type="hidden" name="idProyecto" value="'+idProyecto+'"/>';
	   
	   $("#"+value+" ").text("");
	   $("#"+value+" ").html(tds1);
	   
	   $("#programaInst"+value).text("");
	   $("#programaInst"+value).html(tds2);
	   
	   $("#programaOp"+value).text("");
           
	   $("#programaOp"+value).html(tds3);
	   
	   $("#editMeta"+value).hide();
	   $("#deleteMeta"+value).hide();
	   $("#modificarMetaProyecto"+value).show();
	   	   
   }
   
   
   
   function Add(key){
	    var unicode
	    if (key.charCode){ unicode=key.charCode;}
	    else {unicode=key.keyCode;}
	    //alert(unicode); // Para saber que codigo de tecla presiono , descomentar
	 
	    if (unicode == 13){
	    	//Enter 
	    }
	    
   }
   
  
</script>
</html>