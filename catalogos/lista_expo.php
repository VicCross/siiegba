<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/LineaAccionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/LineaAccion.class.php");
include_once("../src/mx/com/virreinato/dao/ExposicionesTemporalesDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ExposicionesTemporales.class.php");

if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="../css/lista.css">
<script type="text/javascript" src="../media/js/complete.js"></script>
<script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
<script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
<script type="text/javascript" src="../js/lista.js"></script>
  <title>Sistema de Ingresos y Egresos</title>

</head>
<body>
<?php $respuesta = NULL;
    if(isset($_GET['respuesta'])){ $respuesta = $_GET['respuesta']; }?>  
<div class="contenido">
    <p class="titulo_cat1">Catálogos > Catálogos de Proyectos >Exposiciones Temporales</p><br/>
    <?php 
        if($respuesta!=null){
            echo("<div align='center' class='msj'>".$respuesta."</div>");
        }
    ?>
    <div align="right"><a href='agrega_expotemp.php' class='liga_btn'> Agregar Exposición Temporal </a></div>
    <br> </br>
    <?php
                if(isset($_GET['recargado']) && $_GET['recargado'] == "1"){
                    echo '<input type="hidden" id="nombre_form" name="nombre_form" value=""/>';
                }else{
                    echo '<input type="hidden" id="nombre_form" name="nombre_form" value="frmFiltroResmuen"/>';
                }
            ?>            
            <input type="hidden" id="no_sort" name="no_sort" value="1"/>
            <form name="frmFiltroResumen" id="frmFiltroResmuen" method="POST" action="../src/mx/com/virreinato/web/consulta_expo_temp.php" >
                <?php
                $periodo = "";
        $hayNumerosRojos = false;
        
                ?>
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Período:  
                <select name="periodo" id="periodo" style="width:110px" > 
                    <?php
                    $daoPer = new PeriodoDaoJdbc();
                    $listaPer = $daoPer->obtieneListado();
                    $elementoPer = new Periodo();
                    foreach ($listaPer as $elementoPer) {
                        $sel = "";
                        if (isset($_GET['periodo']) && (int) ( $_GET['periodo'] ) == $elementoPer->getId())
                            $sel = "selected='selected'";
                        else if ($_GET['periodo'] == NULL && (int) ($elementoPer->getPeriodo()) == date('Y')) {
                            $sel = "selected='selected'";
                            $periodo = (String) ($elementoPer->getId() );
                        }
                        echo("<option value=" . $elementoPer->getId() . " " . $sel . " >" . $elementoPer->getPeriodo() . "</option>");
                    }
                    ?>  
                </select>
                &nbsp; <input name="Filtro" style="cursor:pointer" type="submit" value="Filtrar"  class='btn' />
            </form>
            <br/>
            <?php echo("Periodo: " . $_GET['periodo']);?>
            <br/> 
    <table  border="0" cellspacing="0" cellpadding="5" class='dataTable' align='center' >
        <thead>
    
    <tr>
          <th width="20%">Nombre</th>
          <th width="20%">Año</th>
          <th width="20%">Saldo</th>
          <th width="20%">Saldo Anterior</th>
          <th width="10%"></th>
          <th width="10%"></th>
        </tr>
    </thead>
    <tfoot>
    <tr>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
        </tr>
    </tfoot>
    <tbody>
    <?php
        $dao = new ExposicionesTemporalesDaoJdbc();
        $lista=$dao->getListado();
        $elemento=new ExposicionesTemporales();
 
        foreach($lista as $elemento){
                ?>
    <tr>
        <td><?php echo($elemento->getNombre());?></td>
        <td align="center"><?php if($elemento->getPeriodo()!=null && strtoupper($elemento->getPeriodo()) == 41){ echo("2015"); }else if($elemento->getPeriodo()!=null && strtoupper($elemento->getPeriodo()) == 42) {echo("2016"); }?></td>
        <td><?php echo($elemento->getSaldo());?></td>
        <td><?php echo($elemento->getSaldoAnterior());?></td>
        <td><a href='agrega_expotemp.php?id=<?php echo($elemento->getIdExposicionT());?>' class='liga_cat'><img src="../img/Pencil3.png" alt="Editar" style="border: 0px none;" ></a></td>
        <td><a href='../src/mx/com/virreinato/web/CatExpo.php?id=<?php echo($elemento->getIdExposicionT());?>' onclick='return confirm("¿Esta seguro que desea eliminar este registro?")' class='liga_cat'><img src="../img/DeleteRed.png" alt="Borrar" style="border: 0px none;"></a></td>
      </tr> 
    <?php     
    }
    ?>
    </tbody>
  </table>
  <br>
  
</div>
<?php include 'footer.php' ?>
</body>
</html>