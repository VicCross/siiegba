<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once("../src/mx/com/virreinato/dao/CatProyectoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatProyecto.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>

<!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="../css/lista.css">
        <script type="text/javascript" src="../media/js/complete.js"></script>
        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
        <script type="text/javascript" src="../js/lista.js"></script>
		
			   <input type="hidden" id="columnas_visibles" name="columnas_visibles" value="9"/>
    <input type="hidden" id="total_columnas" name="total_columnas" value="11"/>

</head>
<body>
<?php $respuesta = NULL;
    if(isset($_GET['respuesta'])){ $respuesta = $_GET['respuesta']; }?>  
    <div class="contenido">
     <br/>
	<p class="titulo_cat1">Catálogos > Catálogos de Proyectos >Catálogo de Proyectos</p><br/>
	<form id="fproyecto" name="fproyecto" method="post" action="./lista_proyecto.php">
            <p class="titulo_cat1"> &nbsp; &nbsp; &nbsp; &nbsp; Selecciona el Año:
                <select id="AnioProyecto" name="AnioProyecto" onchange="this.form.submit();" >
                   <?php 
                        $indice = 0;
		          for($indice = 2000 ; $indice <= (int)(date('Y')); $indice++ ){
                            $sel = "";
                                 if( $_POST["AnioProyecto"] != null  && $indice == (int)($_POST["AnioProyecto"])){
                                         $sel = "selected='selected'";
                                         $seleccionado=$_POST["AnioProyecto"]; 		
                                 }	
                                 else{
                                    if( $indice == (int)(date('Y')) && $_POST["AnioProyecto"] == null){
                                            $sel = "selected='selected'";
                                            $seleccionado = (String)date('Y');
                                    }
                                 }

                        echo("<option value='".$indice."' ".$sel." >".$indice."&nbsp; &nbsp; </option>"); 
	 	          }
	 	          
		       ?>
                </select		      
		></p>
    <?php 
        if($respuesta!=null){
            echo("<div align='center' class='msj'>".$respuesta."</div>");
        }
    ?>
                <div align="right"><a href='agrega_proyecto.php' class='liga_btn'> Agregar Proyecto</a></div>
                <br> </br>
    <table  border="0" cellspacing="0" cellpadding="5" class='dataTable' align='center' >
<thead>       
	   <tr>
          <th width="3%">#</th>
          <th width="20%">Descripción</th>
          <th width="10%">Área del proyecto</th>
          <th width="10%">Área Normativa</th>
          <th width="12%">Centro de Costos</th>
          <th width="10%">Origen del Presupuesto</th>
          <th width="10%">Lider de proyecto</th>
          <th width="10%">Costo</th>
          <th width="10%">Programa operativo</th>

          <th width="2%"></th>
          <th width="2%"></th>
        </tr>
		</thead>
		<tfoot>
       <tr>
          <th width="3%"></th>
          <th width="20%"></th>
          <th width="10%"></th>
          <th width="10%"></th>
          <th width="12%"></th>
          <th width="10%"></th>
          <th width="10%"></th>
          <th width="10%"></th>
          <th width="10%"></th>

          <th width="2%"></th>
          <th width="2%"></th>
        </tr>
		 
		
		</tfoot>
		<tbody>
    <?php
        $aniosel="";
        if(isset($_POST["AnioProyecto"]) && $_POST["AnioProyecto"] != null ){
                $aniosel=$_POST["AnioProyecto"];
        }
        else{

                $aniosel=$seleccionado;
        }
        $dao=new CatProyectoDaoJdbc();
        $lista=$dao->obtieneListadoByAnio($aniosel);
        $elemento=new CatProyecto();
        foreach($lista as $elemento){

    ?>
        <tr class="SizeText">
            <td align="center"><?php echo($elemento->getNumero());?></td>
            <td align="center"><?php echo($elemento->getDescripcion());?></td>
            <td align="center"><?php $area = $elemento->getArea(); echo($area->getDescripcion());?></td>
            <td align="center"><?php $areaNormativa = $elemento->getAreaNormativa(); echo($areaNormativa->getDescripcion());?></td>
            <td align="center"><?php $centroCostos = $elemento->getCentroCostos(); echo($centroCostos->getDescripcion());?></td>
            <td align="center"><?php if($elemento->getOrigen()!=null && strtoupper($elemento->getOrigen()) == "I"){ echo("INAH"); }else if($elemento->getOrigen()!=null && strtoupper($elemento->getOrigen()) == "T") {echo("Terceros"); }?></td>
            <td align="center"><?php $lider = $elemento->getLider(); echo($lider->getNombre()." ".$lider->getApPaterno()." ".$lider->getApMaterno());?></td>
            <td align="center"><?php echo($elemento->getMonto());?></td>
            <td align="center"><?php $lineaAccion = $elemento->getLineaAccion(); echo($lineaAccion->getLineaAccion());?></td>
            <td align="center"><a href='agrega_proyecto.php?id=<?php echo($elemento->getId());?>' class='liga_cat'><img src="../img/Pencil3.png" alt="Editar" style="border: 0px none;" ></a></td>
            <td align="center"><a href='../src/mx/com/virreinato/web/CatProyecto.php?id=<?php echo($elemento->getId());?>' onclick='return confirm("¿Esta seguro que desea eliminar este registro?")' class='liga_cat'><img src="../img/DeleteRed.png" alt="Borrar" style="border: 0px none;"></a></td>
          </tr>	
        <?php		
        }
        ?>
		</tbody>
        </table>
        <br>
        
        </form>	
</div>
<?php include 'footer.php' ?>
</body>
</html>