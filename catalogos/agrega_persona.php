<?php

include_once("../src/mx/com/virreinato/dao/PersonaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Persona.class.php");
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<body>
 <?php
    $error = NULL;
    if (isset($_REQUEST['error'])) {
        $error = $_REQUEST['error'];
    }
    ?>
<div class="contenido"> 
    <p class="titulo_cat1">Catálogos > Catálogos de Operación >
        <?php
	$dao=new PersonaDaoJdbc();
	$elemento=new Persona();

	if(isset($_GET['id'])){
		echo("Modificar Persona");
		$elemento=$dao->obtieneElemento($_GET['id']);	
	}	
	else{
		echo("Alta de Nueva Persona");
	}	
    ?>
    </p>
    <br/>
    <?php if ($error != null) echo("<div align='center' class='msj'>" . $error . "</div>"); ?>
    <form id="fpersona" name="fpersona" method="post" action="../src/mx/com/virreinato/web/CatPersona.php">
        <table width="90%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
          <tr>
            <td width='14%'>Nombre(s)*:</td>
            <td width='20%'><input type="text" name="nombre" id="nombre" size='30' value='<?php if($elemento!=null && $elemento->getNombre()!= null) echo($elemento->getNombre());?>'/></td>
            <td width='13%'>Apellido Paterno*:</td>
            <td width='20%'><input type="text" name="apaterno" id="apaterno" size='30' value='<?php if($elemento!=null && $elemento->getApPaterno()!= null) echo($elemento->getApPaterno());?>'/></td>
            <td width='13%'>Apellido Materno*:</td>
            <td width='20%'><input type="text" name="amaterno" id="amaterno" size='30' value='<?php if($elemento!=null && $elemento->getApMaterno()!= null) echo($elemento->getApMaterno());?>'/></td>
          </tr>

          <tr>
            <td>Correo electrónico*:</td>
            <td><input type="text" name="correo" id="correo" size='30' value='<?php if($elemento!=null && $elemento->getCorreo()!= null) echo($elemento->getCorreo());?>' /></td>
            <td>Observaciones:</td>
            <td colspan='3'><textarea name="observaciones" id="observaciones" rows='3' cols='50'/><?php if($elemento!=null && $elemento->getComentario()!= null) echo($elemento->getComentario());?></textarea></td>
          </tr>

          <tr>
            <td align="center" colspan="6" ><input name="guardar" type="submit" value="Guardar"  class='btn' />
            &nbsp;&nbsp;&nbsp;&nbsp;<a href='lista_persona.php' class='liga_btn'> Cancelar </a>
            </td>
          </tr>

        </table>
        <?php if($elemento!=null && $elemento->getId()!=null) echo("<input type='hidden' name='id' value='".$elemento->getId()."' />");?>
    </form>
</div>
<?php include 'footer.php' ?>
<script type="text/javascript">
 var frmvalidator  = new Validator("fpersona");
 frmvalidator.addValidation("nombre","req","Por favor capture el nombre de la persona.");
 frmvalidator.addValidation("apaterno","req","Por favor capture el apellido paterno de la persona.");
 frmvalidator.addValidation("amaterno","req","Por favor capture el apellido materno de la persona.");
 frmvalidator.addValidation("correo","req","Por favor capture el correo electrónico.");
 frmvalidator.addValidation("correo","email","El correo electrónico no es válido");
 </script>

</body>
</html>