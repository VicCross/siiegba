<?php
	include_once("../src/classes/Catalogo.class.php");
	$perfil = $_POST['perfil'];
	$menu = $_POST['menu'];
	$submenu = $_POST['submenu'];
	$respuesta = "";
	$idPerfilMenu = 0;
	$catalogo = new Catalogo();
	$tieneSubmenus = true;
	//checamos lo que nos pasaron en submenu
	if($submenu == "" || $submenu == "No") $tieneSubmenus = false;
	//Corroboramos que no esté ya asignado el menu al perfil.
	$query = "SELECT id_perfil_menu FROM sie_cat_perfilmenu WHERE id_perfil = " . $perfil . " AND id_menu = " . $menu;
	$result = $catalogo->obtenerLista($query);
	$noFilas = mysql_num_rows($result);
	if($noFilas == 1 && $tieneSubmenus){//ya estaba
		while($rs = mysql_fetch_array($result)){
			$idPerfilMenu = $rs['id_perfil_menu'];
		}
		$query = "SELECT * FROM sie_cat_MSM WHERE id_perfil_menu = " . $idPerfilMenu . " AND id_submenu = " . $submenu;
		$result = $catalogo->obtenerLista($query);
		$noFilas2 = mysql_num_rows($result);
		if($noFilas2 == 1){//ya estaba.
			$respuesta = "El perfil ya tiene el permiso ingresado.";	
		}else if($noFilas2 == 0){//no estaba.
			$insert = "INSERT INTO sie_cat_MSM (id_perfil_menu, id_submenu) VALUES (".$idPerfilMenu.", ".$submenu.")";
			$catalogo->insertarRegistro($insert);
			$respuesta = "Permiso para el perfil guardado con éxito.";
		}else{//otra razon
			$respuesta = "Error el permiso no pudo ser creado.";	
		}
	}else if($noFilas == 0){//noestaba
		$insert = "INSERT INTO sie_cat_perfilmenu (id_perfil, id_menu) VALUES (".$perfil.", ".$menu.")";
		$catalogo->insertarRegistro($insert);
		if($tieneSubmenus){
			$query = "SELECT id_perfil_menu FROM sie_cat_perfilmenu WHERE id_perfil = " . $perfil . " AND id_menu = " . $menu;
			$result = $catalogo->obtenerLista($query);
			if($rs = mysql_fetch_array($result)){
				$idPerfilMenu = $rs['id_perfil_menu'];
			}
			$insert = "INSERT INTO sie_cat_MSM (id_perfil_menu, id_submenu) VALUES (".$idPerfilMenu.", ".$submenu.")";
			//echo('El insert es: '.$insert);
			$catalogo->insertarRegistro($insert);
			$respuesta = "Permiso para el perfil guardado con éxito.";
		}else{
			$respuesta = "Se ha guardado con éxito el permiso del perfil pero no tiene submenús que guardar.";	
		}
	}else if(!$tieneSubmenus){//no tiene submenus. pobrecito!!!
		$respuesta = "El perfil ya tiene el menú seleccionado pero no tiene submenús que guardar.";
	}else{//otra razon
		$respuesta = "Error el permiso no pudo ser creado.";			
	}
	//echo($respuesta);	
	header("Location: lista_permisosusuario.php?respuesta=".$respuesta);	
	
?>