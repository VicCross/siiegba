<?php
session_start();

if (!isset($_SESSION['idUsuario']) || is_null($_SESSION['idUsuario'])) {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php?err=2");}

header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
//include_once("../src/classes/PermisosMenu.class.php");
include_once("../src/classes/Catalogo.class.php");

$pagina_lista = "catalogos/lista_permisosusuario.php";
$id = "";
$id1 = "";
$idPuesto = "";
$alta = "";
$baja = "";
$modificacion = "";
$consulta = "";
$read = "";
$idPerfil = $_GET['idPerfil'];
$idSubmenu = $_GET['idSubmenu'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
<script>
	function ChecaMenu()
	{
		var id = $("#perfil").val();
		$.ajax({
		 	 url: "../src/mx/com/virreinato/web/AgregarPermisosUsuario.php",
		 	 type: "POST",
		 	 data: "idPerfil="+id,
		 	 success: function(data){ $("#menu").html(data);}
		 })	
	}
	
	function ChecaSubMenus(){
		var idP = $("#perfil").val();
		var idM = $("#menu").val();
		$.ajax({
		 	 url: "../src/mx/com/virreinato/web/AgregarPermisosUsuario.php",
		 	 type: "POST",
		 	 data: "idPerfil="+idP+"&idMenu="+idM,
		 	 success: function(data){ $("#submenu").html(data);}
		 })	
	}
	function checarIDs(){
		var idP = $("#perfil").val();
		var idM = $("#menu").val();
		var idSM = $("#submenu").val();
		if(idP == 0 || idM == 0 || idSM == 0){
			alert ("Error. Por favor complete todos los campos.");
		}else{
			$("#permisos").submit();	
		}
	}
</script>
</head>
<body>


<?php $respuesta = NULL;
    if(isset($_GET['respuesta'])){ $respuesta = $_GET['respuesta']; }?>  
<div class="contenido">
    <p class="titulo_cat1">Catálogos > Catálogos de Estructura > Permisos de Usuario > Agregar Nuevo Permiso</p><br/> 
    <?php 
		//Vamos a revisar si ya está en la BD o no.
		$catalogo = new Catalogo();
		
    ?>
	<form name="permisos" id="permisos" method="post" action="inserta_permisosusuario.php">
    <table width="55%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
        <tr>
            <td width="20%">Perfil:</td>
            <td>
                <select id="perfil" name="perfil" onchange="ChecaMenu()">
                	<option value="0">----Selecciona un perfil----</option>
                    <?php
                    $query = $catalogo->obtenerLista("SELECT CPER_ID_PERFIL, CPER_DESCRIPCION FROM sie_cat_perfiles");
                    while ($rs = mysql_fetch_array($query)) {
                        echo "<option value='" . $rs['CPER_ID_PERFIL'] . "' ". $select .">" . $rs['CPER_DESCRIPCION'] . "</option>";
                    }
                    ?>
                </select>
            </td>      
        </tr>
        <tr>
            <td width="20%">Menú: </td>
            <td>
                <select id="menu" name="menu" onchange="ChecaSubMenus()"></select>
            </td>      
        </tr>
        <tr>
            <td width="20%">Submenu:         </td>
            <td><select id="submenu" name="submenu"></select></td>
        </tr>
        <tr align="center">
        	<td align="center"><input name="guardar" type="button" value="Guardar"  class='btn' onClick="checarIDs()"/></td>
            <td align="center"><a href='lista_permisosusuario.php' class='liga_btn'> Cancelar </a></td>
        </tr>	  
	 </table>
      </form>      
	<br> </br>
	<br> </br>
	<div align="center"><a href='menu_catalogos.php' class='liga_btn'> Regresar Menú Catálogos </a></div>
		
		
   </div>
</body>
</html>