<?php
session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
// include_once("../src/mx/com/virreinato/dao/IngresosPatronatoDaoJdbc.class.php");  
// include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/IngresosPatronato.class.php");
include_once("../src/mx/com/virreinato/dao/ExposicionesTemporalesDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/dao/ProductosDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ExposicionesTemporales.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProductosPatronato.class.php");
include_once("../src/mx/com/virreinato/dao/LineaAccionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/LineaAccion.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}

header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC >

<html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Sistema de Ingresos y Egresos</title>

<link rel="stylesheet" type="text/css" href="../css/style.css">

<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>



<!-- DataTables -->

        <link rel="stylesheet" type="text/css" href="../css/lista.css">

        <script type="text/javascript" src="../media/js/complete.js"></script>

        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>

        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>

        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>

        <script type="text/javascript" src="../js/lista.js"></script>

		

			   <input type="hidden" id="columnas_visibles" name="columnas_visibles" value="8"/>

    <input type="hidden" id="total_columnas" name="total_columnas" value="10"/>



</head>

<body>

<?php $respuesta = NULL;

    if(isset($_GET['respuesta'])){ $respuesta = $_GET['respuesta']; }?> 

<div class="contenido">

	<p class="titulo_cat1">Menú de Catálogos &gt; Patronato catálogo de productos</p><br/>

    <?php 

        if($respuesta!=null){

            echo("<div align='center' class='msj'>".$respuesta."</div>");

        }

    ?>

        <div align="right"><a href='agrega_producto.php' class='liga_btn'> Agregar Producto</a></div>

        <br> </br>
    <div class="table-responsive table-collapse">
    <table  border="0" cellspacing="0" cellpadding="5" class='	dataTable' align='center' >
        <thead>
			<tr>
				<th width='15%'>Nombre del Producto</th>
				<th width='15%'>Descripciónn</th>
				<th width='15%'>Inventario Inicial</th> 
				<th width='15%'>Inventario Actual</th> 
	    
				<th width='5%'></th>
				<th width='5%'></th>
			</tr>
		</thead>
		<tfoot>
		<tr>
			<th width='15%'></th>
			<th width="15%"></th>
			<th width="15%"></th>
			<th width='15%'></th>
        
			<th width='5%'></th>
			<th width='5%'></th>
        </tr>
		</tfoot>
		<tbody>
			<?php
				$dao = new ProductosDaoJdbc();
				$lista=$dao->obtieneListadoProd();
				$elemento=new Productos();
	
				foreach($lista as $elemento){
                ?>
			<tr>

            <td><?php echo($elemento->getNombre_prod());?></td>
            <td><?php echo($elemento->getDescripcion_prod());?></td>
            <td><?php echo($elemento->getInventario_ini());?></td> 
			<td><?php echo($elemento->getInventario_act());?></td> 
            
            <td><a href='agrega_producto.php?id=<?php echo($elemento->getId_prod());?>' class='liga_cat'><img src="../img/Pencil3.png" alt="Editar" style="border: 0px none;" ></a></td>
            <td><a href='../src/mx/com/virreinato/web/CatProductos.php?id=<?php echo($elemento->getId_prod());?>' onclick='return confirm("¿Esta seguro que desea eliminar este registro?")' class='liga_cat'><img src="../img/DeleteRed.png" alt="Borrar" style="border: 0px none;"></a></td>

            </tr>	

    <?php			

    	}

    ?>

	</tbody>

    </table>

	<br>

	
</div>
</div>

<?php include 'footer.php' ?>

</body>

</html>

