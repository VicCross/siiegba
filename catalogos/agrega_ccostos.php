<?php
session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once("../src/mx/com/virreinato/dao/CentroCostosDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CentroCostos.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sistema de Ingresos y Egresos</title>
        <link rel="stylesheet" type="text/css" href="../css/style.css">
		<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
        <script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
        <script>
		$(document).ready(function() {                    
			$("#Perfil").css({
				background: "<?php echo $parametro->getValor(); ?>"
			});
			$(".div_menu").css({
				background: "<?php echo $parametro->getValor(); ?>"
			});
			$(".tb_presupuesto th").css({
				background: "<?php echo $parametro->getValor(); ?>"
			});
			$(".tb_presupuestoResumen th").css({
				background: "<?php echo $parametro->getValor(); ?>"
			});
			$(".tb_cat th").css({
				background: "<?php echo $parametro->getValor(); ?>"
			});
			$(".tb_add_cat").css({
				background: "<?php echo $parametro->getValor(); ?>"
			});
		});
    </script>
    </head>
    <body>
        <?php
        $error = NULL;
        if (isset($_REQUEST['error'])) {
            $error = $_REQUEST['error'];
        }
        ?>  
        <div class="contenido">
            <p class="titulo_cat1">Catálogos > Catálogos de Estructura > 
                <?php
                $dao = new CentroCostosDaoJdbc();
                $elemento = new CentroCostos();

                if (isset($_GET['id'])) {
                    echo("Modificar Centro de Costos");
                    $elemento = $dao->obtieneElemento($_GET['id']);
                } else {
                    echo("Alta de Nuevo Centro de Costos");
                }
                ?>
            </p>
            <br/>
            <?php if ($error != null) echo("<div align='center' class='msj'>" . $error . "</div>"); ?>
            <form id="fccostos" name="fccostos" method="post" action="../src/mx/com/virreinato/web/CatCentroCostos.php" target="destino">
                <table width="70%"  border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
                    <tr>
                        <td width="30%">Clave del Centro de Costos*:</td>
                        <td width="70%">
                            <input type="text" name="cve_ccostos" id="cve_ccostos" size="20" value='<?php if ($elemento != null && $elemento->getClave() != null) echo($elemento->getClave()); ?>'>
                        </td>
                    </tr>
                    <tr>
                        <td>Nombre del Centro de Costos*:</td>
                        <td><input name="nombre_ccostos" id="nombre_ccostos" type="text" size="70" value='<?php if ($elemento != null && $elemento->getDescripcion() != null) echo($elemento->getDescripcion()); ?>' /></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2"><input name="guardar" type="submit" value="Guardar"  class='btn' />
                            &nbsp;&nbsp;&nbsp;&nbsp;<a href='lista_ccostos.php' class='liga_btn'> Cancelar </a>
                        </td>
                    </tr>
                </table>
                <?php if ($elemento != null && $elemento->getId() != null) echo("<input type='hidden' name='id' value='" . $elemento->getId() . "' />"); ?>
            </form>
        </div>
        <?php include 'footer.php' ?>
        <script type="text/javascript">
            var frmvalidator = new Validator("fccostos");
            frmvalidator.addValidation("cve_ccostos", "req", "Por favor capture la clave del centro de costos.");
            frmvalidator.addValidation("nombre_ccostos", "req", "Por favor capture el nombre del centro de costos.");
        </script>
    </body>
</html>
