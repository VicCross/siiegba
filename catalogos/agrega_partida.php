<?php
session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once("../src/mx/com/virreinato/dao/CapituloDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/dao/PartidaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Capitulo.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<body>
<?php
        $error = NULL;
        if (isset($_REQUEST['error'])) {
            $error = $_REQUEST['error'];
        }
        ?>
<div class="contenido"> 
	<p class="titulo_cat1">Catálogos > Catálogos de Estructura >
<?php
	$dao=new PartidaDaoJdbc();
	$elemento=new Partida();

	if(isset($_GET['id'])){
		echo("Modificar Partida Presupuestal");
		$elemento=$dao->obtieneElemento($_GET['id']);	
	}	
	else{
		echo("Alta de Nueva Partida Presupuestal");
	}	
	?>
            </p>
            <br/>
            <?php if ($error != null) echo("<div align='center' class='msj'>" . $error . "</div>"); ?>
            <?php 
                $capitulo=new CapituloDaoJdbc();
                $capitu=$capitulo->obtieneListado();
                $b = new Capitulo("","","");
                
                ?>
        	<form id="fpartida" name="fpartida" method="post" action="../src/mx/com/virreinato/web/CatPartida.php">
		<table width="60%%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
		  <tr>
		    <td width="19%">Capítulo Presupuestal*:</td>
		    <td width="81%">
		      <select name="id_capitulo" id="id_capitulo" >
		      <option value='0'>Selecciona</option>
         <?php
                    foreach($capitu as $b ) {
                        $sel="";
                        if($elemento!= null && $elemento->getCapitulo()!= null ){
                                $elemento2 = $elemento->getCapitulo();
                                if($b->getId()==$elemento2->getId())
                                    {     $sel="selected='selected'"; }
                        }
                        echo("<option value='".$b->getId()."' ".$sel." >".$b->getCapitulo()."</option>");
                    }
            ?>
            </select>
            </td>
		  </tr>
		  <tr>
		    <td>Partida Presupuestal*:</td>
		    <td><input type="text" name="partida" id="partida" value='<?php if($elemento!=null && $elemento->getPartida()!= null) echo($elemento->getPartida());?>'/></td>
		  </tr>
          <tr>
		    <td>Presupuestal Generica*:</td>
		    <td><input type="text" name="generica" id="generica" value='<?php if($elemento!=null && $elemento->getGenerica()!= null) echo($elemento->getGenerica());?>'/></td>
		  </tr>
          <tr>
		    <td>Presupuestal Especifica*:</td>
		    <td><input type="text" name="especifica" id="especifica" value='<?php if($elemento!=null && $elemento->getEspecifica()!= null) echo($elemento->getEspecifica());?>'/></td>
		  </tr>
		  <tr>
		    <td>Nombre de la Cuenta*:</td>
		    <td><input name="descripcion" type="text" size="50" value='<?php if($elemento!=null && $elemento->getDescripcion()!= null) echo($elemento->getDescripcion());?>'/></td>
		  </tr>
		  <tr>
		    <td align="center" colspan="2"><input name="guardar" type="submit" value="Guardar"  class='btn' />
		    &nbsp;&nbsp;&nbsp;&nbsp;<a href='lista_partida.php' class='liga_btn'> Cancelar </a>
		    </td>
		  </tr>
		</table>
                <?php if($elemento!=null && $elemento->getId()!=null) echo("<input type='hidden' name='id' value='".$elemento->getId()."' />");?>
	</form>
</div>
<?php include 'footer.php' ?>
<script type="text/javascript">
 var frmvalidator  = new Validator("fpartida");
 frmvalidator.addValidation("id_capitulo","dontselect=0","Por favor seleccione el Capítulo.");
 frmvalidator.addValidation("partida","req","Por favor capture la partida.");
 frmvalidator.addValidation("descripcion","req","Por favor capture el nombre de la partida.");
 
 </script>
</body>
</html>

