<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
//include_once("../src/mx/com/virreinato/dao/PermisosusuarioDaoJdbc.class.php");
//include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/AreaNormativa.class.php");

if (!isset($_SESSION['idUsuario']) || is_null($_SESSION['idUsuario'])) {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php?err=2");
}
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
include_once("../src/classes/Catalogo.class.php");
include_once("../src/classes/Menu.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);

//$controlador = "WEB-INF/Controllers/Controler_PermisosMenu.php";
//$same_page = "catalogos/lista_permisosusuario.php";
$alta = "catalogos/agrega_permisosusuario.php";

//$permisos_grid = new Permisosusuario();
//$permisos_grid->getPermisosusuario($_SESSION['idUsuario'], $same_page;


?>
<!DOCTYPE html PUBLIC>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="../css/lista.css">
        <script type="text/javascript" src="../media/js/complete.js"></script>
        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
        <script type="text/javascript" src="../js/lista.js"></script>
		
			   <input type="hidden" id="columnas_visibles" name="columnas_visibles" value="3"/>
    <input type="hidden" id="total_columnas" name="total_columnas" value="5"/>

</head>
<body>


<?php $respuesta = NULL;
    if(isset($_GET['respuesta'])){ $respuesta = $_GET['respuesta']; }?>  
<div class="contenido">
    <p class="titulo_cat1">Catálogos > Catálogos de Estructura > Permisos de Usuario</p><br/> 
    <?php 
        if($respuesta!=null){
            echo("<div align='center' class='msj'>".$respuesta."</div>");
        }
    ?>
	
	 <div align="right"><a href='agrega_permisos_usuario.php' class='liga_btn'> Agregar un nuevo permiso </a></div>
	 <br>  </br>
	 <br>  </br>
    <table border="0" cellspacing="0" cellpadding="5" class='dataTable' align='center' >
     <thead>
	   <tr>
		  <th width="30%"> Perfil</th>
		  <th width="30%"> Menú</th>
		  <th width="30%"> Submenú</th>
		  <th width="5%"> </th>
		  <th width="5%"> </th>
		  
        </tr>
		  </thead>
		  <tfoot>
		   <tr>
		  <th width="30%"> </th>
		  <th width="30%"> </th>
		  <th width="30%"> </th>
		  <th width="5%"> </th>
		  <th width="5%"> </th>
		  
        </tr>
		  </tfoot>
		
		  <tbody>
                    <?php
                    $catalogo = new Catalogo();
					$consulta = "SELECT p.CPER_ID_PERFIL, p.CPER_DESCRIPCION, m.ID_MENU,m.NOM_MENU, s.ID_SUBMENU,s.NOM_SUBMENU FROM sie_cat_perfiles p, sie_cat_menu m, sie_cat_perfilmenu x, sie_cat_MSM msm, sie_cat_submenu s WHERE s.ID_SUBMENU = msm.ID_SUBMENU AND msm.ID_PERFIL_MENU = x.ID_PERFIL_MENU AND x.ID_PERFIL = p.CPER_ID_PERFIL AND x.ID_MENU = m.ID_MENU ORDER BY p.CPER_ID_PERFIL, m.ID_MENU, s.ID_SUBMENU";
					$query = $catalogo->obtenerLista($consulta);						
					while ($rs = mysql_fetch_array($query)) {
                        echo "<tr>";
                        echo "<td align='center' scope='row'>" . $rs['CPER_DESCRIPCION'] . "</td>";
                        echo "<td align='center' scope='row'>" . $rs['NOM_MENU'] . "</td>";
                        echo "<td align='center' scope='row'>" . $rs['NOM_SUBMENU'] . "</td>";  
					
						?>
                        <td align='center' scope='row'> 
                          <a href='agrega_permisosusuario.php?idPerfil=<?php echo $rs['CPER_ID_PERFIL'];?>&idSubmenu=<?php echo $rs['ID_SUBMENU'];?>' title='Editar permiso' ><img src="../img/Pencil3.png" alt="Editar" style="border: 0px none;" /></a>
            			</td>
                    <td align='center' scope='row'> 
                          <a href="eliminar_permisosusuario.php?idPerfil=<?php echo $rs['CPER_ID_PERFIL'];?>&idMenu=<?php echo $rs['ID_MENU'];?>&idSubmenu=<?php echo $rs['ID_SUBMENU']?>"><img src="../img/DeleteRed.png" alt="Eliminar" style="border: 0px none;" /></a>
						
                        <?php	
						echo "</tr>";
                }
                ?>
             </tbody>
			
			
	
	
	</table>
		<br> </br>
		 <div align="center"><a href='agrega_permisos_usuario.php' class='liga_btn'> Agregar un nuevo permiso </a></div>
		<br> </br>
		<br> </br>
	  <div align="center"><a href='menu_catalogos.php' class='liga_btn'> Regresar al Menú de Cátalogos </a></div>	
		
		
   </div>
</body>
</html>