<?php
include_once("../src/mx/com/virreinato/dao/CuentaBancariaDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/dao/BancoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CuentaBancaria.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Banco.class.php");
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<body>
 <?php
    $error = NULL;
    if (isset($_REQUEST['error'])) {
        $error = $_REQUEST['error'];
    }
    ?>
<div class="contenido"> 
    <p class="titulo_cat1">Catálogos > Catálogos de Operación >
    <?php
	$dao=new CuentaBancariaDaoJdbc();
	$elemento=new CuentaBancaria();

	if(isset($_GET['id'])){
		echo("Modificar Cuenta Bancaria");
		$elemento=$dao->obtieneElemento($_GET['id']);	
	}	
	else{
		echo("Alta de Nueva Cuenta Bancaria");
	}	
    ?>
    </p>
    <br/>
    <?php if ($error != null) echo("<div align='center' class='msj'>" . $error . "</div>"); ?>
    <form id="fcuenta" name="fcuenta" method="post" action="../src/mx/com/virreinato/web/CatCuentaBancaria.php">
    <table width="90%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
      <tr>
        <?php
            $banco=new BancoDaoJdbc();
            $bancos=$banco->obtieneListado(); 
            $b = new Banco();
        ?>
        <td width='15%'>Banco*:</td>
        <td width='35%'>
          <select name="id_banco" id="id_banco">
            <option value='0'>Selecciona</option>
            <?php
                foreach($bancos as $b){
                  $sel="";
                  if($elemento!= null && $elemento->getBanco()!= null ){
                          $aux = $elemento->getBanco();
                          if($b->getId()==$aux->getId()){
                                $sel="selected='selected'";
                          }	
                  }	  
                  echo("<option value='".$b->getId()."' ".$sel." >".$b->getDescripcion()."</option>");
                }
            ?>
            </select>
                    </td>
                <td width='15%'>Número de Cuenta*:</td>
                <td width='35%'><input type="text" name="cuenta" id="cuenta" value='<?php if($elemento!=null && $elemento->getNumeroCuenta()!= null) echo($elemento->getNumeroCuenta());?>' /></td>
              </tr>

              <tr>
                <td>Tipo de Cuenta*:</td>
                    <td><input type="text" name="tipo_cuenta" id="tipo_cuenta" value='<?php if($elemento!=null && $elemento->getTipoCuenta()!= null) echo($elemento->getTipoCuenta());?>' /></td>
                <td>Clabe*:</td>
                <td><input type="text" name="clabe"  id="clabe"  size="20" maxlength='18' value='<?php if($elemento!=null && $elemento->getClabe()!= null) echo($elemento->getClabe());?>' /></td>
              </tr>

              <tr>
                <td>Sucursal*:</td>
                    <td><input type="text" name="sucursal" id="sucursal" size='50' value='<?php if($elemento!=null && $elemento->getSucursal()!= null) echo($elemento->getSucursal());?>'/></td>
                <td>Ejecutivo de Cuenta:</td>
                <td><input type="text" name="ejecutivo" id="ejecutivo" value='<?php if($elemento!=null && $elemento->getEjecutivoCuenta()!= null) echo($elemento->getEjecutivoCuenta());?>' /></td>
              </tr>


              <tr>
                <td>Teléfono del Ejecutivo de Cta.:</td>
                    <td><input type="text" name="ejecutivotel" id="ejecutivotel" value='<?php if($elemento!=null && $elemento->getEjecutivoTel()!= null) echo($elemento->getEjecutivoTel());?>' /></td>
                <td>Correo del Ejecutivo de Cta.:</td>
                <td><input type="text" name="ejecutivocorreo" id="ejecutivocorreo" value='<?php if($elemento!=null && $elemento->getEjecutivoCorreo()!= null) echo($elemento->getEjecutivoCorreo());?>' /></td>
              </tr>

              <tr>
                <td>Descripción:</td>
                <td colspan='3'><input name="descripcion" type="text" size="50" value='<?php if($elemento!=null && $elemento->getDescripcion()!= null) echo($elemento->getDescripcion());?>' /></td>
              </tr>

              <tr>
                <td align="center" colspan="4"><input name="guardar" type="submit" value="Guardar"  class='btn' />
                &nbsp;&nbsp;&nbsp;&nbsp;<a href='lista_cuenta.php' class='liga_btn'> Cancelar </a>
                </td>
              </tr>
    </table>
    <?php if($elemento!=null && $elemento->getId()!=null) echo("<input type='hidden' name='id' value='".$elemento->getId()."' />");?>
    </form>
</div>
<?php include 'footer.php' ?>
<script type="text/javascript">
 var frmvalidator  = new Validator("fcuenta");
 frmvalidator.addValidation("id_banco","dontselect=0","Por favor seleccione el banco.");
 frmvalidator.addValidation("cuenta","req","Por favor capture el número de cuenta.");
 frmvalidator.addValidation("cuenta","numeric","El número de cuenta debe contener sólo números.");
 frmvalidator.addValidation("tipo_cuenta","req","Por favor capture el tipo de cuenta.");
 frmvalidator.addValidation("clabe","req","Por favor capture la clabe.");
 frmvalidator.addValidation("clabe","numeric","La clabe debe ser numerica.");
 frmvalidator.addValidation("clabe","minlen=18","La clabe debe ser de 18 dígitos");
 frmvalidator.addValidation("sucursal","req","Por favor capture la sucursal.");
 </script>
</body>
</html>