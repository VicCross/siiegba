<?php
session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once("../src/mx/com/virreinato/dao/UsuarioDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Usuario.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>


<!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="../css/lista.css">
        <script type="text/javascript" src="../media/js/complete.js"></script>
        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
        <script type="text/javascript" src="../js/lista.js"></script>
			   <input type="hidden" id="columnas_visibles" name="columnas_visibles" value="3"/>
    <input type="hidden" id="total_columnas" name="total_columnas" value="5"/>
</head>
<body>
    <?php $respuesta = NULL;
        if(isset($_GET['respuesta'])){ $respuesta = $_GET['respuesta']; }?>  
    <div class="contenido">
    <br>
	 <p class="titulo_cat1">Catálogos > Catálogos de Estructura > Catálogo de Usuarios</p><br/>
         <?php 
            if($respuesta!=null){
                echo("<div align='center' class='msj'>".$respuesta."</div>");
            }
        ?>
         <div align="right"><a href='agrega_usuario.php' class='liga_btn'> Agregar Usuario </a></div>
         <br> </br>
    <table  border="0" cellspacing="0" cellpadding="5" class='dataTable' align='center' >
	  <thead>
	  <tr>
	    <th width='40%'>Persona</th>
	    <th width='40%'>Perfil</th>
	    <th width='40%'>Usuario</th>
	    <th width='2%'></th>
	    <th width='2%'></th>
	  </tr>
	  </thead>
	  <tfoot>
	  <tr>
	    <th width='40%'></th>
	    <th width='40%'></th>
	    <th width='40%'></th>
	    <th width='2%'></th>
	    <th width='2%'></th>
	  </tr>
	  </tfoot>
	  <tbody>
    <?php
        $dao=new UsuarioDaoJdbc();
        $lista=$dao->obtieneListado();
        $elemento=new Usuario();
            foreach($lista as $elemento){
            ?>
    <tr class="SizeText">
        <td align="center" ><?php $persona = $elemento->getPersona(); echo($persona->getNombre()." ".$persona->getApPaterno()." ".$persona->getApMaterno());?></td>
        <td align="center"><?php $perfil = $elemento->getPerfil(); echo($perfil->getDescripcion());?></td>
        <td align="center"><?php echo($elemento->getUsuario());?></td>
        <td align="center"><a href='agrega_usuario.php?id=<?php echo($elemento->getId());?>' class='liga_cat'><img src="../img/Pencil3.png" alt="Editar" style="border: 0px none;" ></a></td>
        <td align="center"><a href='../src/mx/com/virreinato/web/CatUsuario.php?id=<?php echo($elemento->getId());?>' onclick='return confirm("¿Esta seguro que desea eliminar este registro?")' class='liga_cat'><img src="../img/DeleteRed.png" alt="Borrar" style="border: 0px none;"></a></td>
      </tr>	
    <?php			
		}
	  ?>
	  </tbody>
    </table>
	<br>
	
</div>
<?php include 'footer.php' ?>
</body>
</html>
