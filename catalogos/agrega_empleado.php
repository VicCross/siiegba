<?php

include_once("../src/mx/com/virreinato/dao/EmpleadoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Empleado.class.php");
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<body>
 <?php
    $error = NULL;
    if (isset($_REQUEST['error'])) {
        $error = $_REQUEST['error'];
    }
    ?>
<div class="contenido"> 
    <p class="titulo_cat1">Catálogos > Catálogos de Operación >
    <?php
	$dao=new EmpleadoDaoJdbc();
	$elemento=new Empleado();

	if(isset($_GET['id'])){
		echo("Modificar Empleado");
		$elemento=$dao->obtieneElemento($_GET['id']);	
	}	
	else{
		echo("Alta de Nuevo Empleado");
	}	
    ?>
    </p>
    <br/>
    <?php if ($error != null) echo("<div align='center' class='msj'>" . $error . "</div>"); ?>
    <form id="fempleado" name="fempleado" method="post" action="../src/mx/com/virreinato/web/CatEmpleado.php">
    <table width="90%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
      <tr>
        <td width='10%'>Nombre(s)*:</td>
        <td width='15%'><input type="text" name="nombre" id="nombre" size='30' value='<?php if($elemento!=null && $elemento->getNombre()!= null) echo($elemento->getNombre());?>'/></td>
        <td width='10%'>Apellido Paterno*:</td>
        <td width='15%'><input type="text" name="apaterno" id="apaterno" size='30' value='<?php if($elemento!=null && $elemento->getApPaterno()!= null) echo($elemento->getApPaterno());?>'/></td>
        <td width='10%'>Apellido Materno*:</td>
        <td width='15%'><input type="text" name="amaterno" id="amaterno" size='30' value='<?php if($elemento!=null && $elemento->getApMaterno()!= null) echo($elemento->getApMaterno());?>'/></td>
      </tr>


      <tr>
        <td width='10%'>RFC*:</td>
        <td width='15%'><input type="text" name="rfc" id="rfc" size='17' value='<?php if($elemento!=null && $elemento->getRfc()!= null) echo($elemento->getRfc());?>'/></td>

        <td>Puesto*:</td>
        <td><input type="text" name="puesto" id="puesto" size='30' value='<?php if($elemento!=null && $elemento->getPuesto()!= null) echo($elemento->getPuesto());?>'/></td>
        <td>Tipo de nómina*:</td>
        <td>
        <?php
		if(!is_null($elemento)){
			$tipoNomina = $elemento->getTipoNomina();	
		}
		?>
        	<select id="tipo_nomina" name="tipo_nomina" style="width:160px">
            	<option value="0">---Selecciona---</option>
                <option value="Proyectos" <?php if(!is_null($tipoNomina) && $tipoNomina=="Proyectos") echo("selected");?>>Proyectos</option>
                <option value="ATM" <?php if(!is_null($tipoNomina) && $tipoNomina=="ATM") echo("selected");?>>ATM</option>
                <option value="Apoyo a confianza" <?php if(!is_null($tipoNomina) && $tipoNomina=="Apoyo a confianza") echo("selected");?>>Apoyo a confianza</option>
                <option value="Mandos medios" <?php if(!is_null($tipoNomina) && $tipoNomina=="Mandos medios") echo("selected");?>>Mandos medios</option>
                <option value="Compactados" <?php if(!is_null($tipoNomina) && $tipoNomina=="Compactados") echo("selected");?>>Compactados</option>
                <option value="Pensión alimenticia" <?php if(!is_null($tipoNomina) && $tipoNomina=="Pensión alimenticia") echo("selected");?>>Pensión alimenticia</option>
                <option value="Capítulo 3000" <?php if(!is_null($tipoNomina) && $tipoNomina=="Capítulo 3000") echo("selected");?>>Capítulo 3000</option>
                <option value="Servicios Profesionales" <?php if(!is_null($tipoNomina) && $tipoNomina=="Servicios Profesionales") echo("selected");?>>Servicios Profesionales</option>
                <option value="Estructura" <?php if(!is_null($tipoNomina) && $tipoNomina=="Estructura") echo("selected");?>>Estructura</option>
                <option value="Base" <?php if(!is_null($tipoNomina) && $tipoNomina=="Base") echo("selected");?>>Base</option>
                <option value="Apoyo a confianza" <?php if(!is_null($tipoNomina) && $tipoNomina=="Apoyo a confianza") echo("selected");?>>Apoyo a confianza</option>
                <option value="Honorarios" <?php if(!is_null($tipoNomina) && $tipoNomina=="Honorarios") echo("selected");?>>Honorarios</option>
        	</select>
        </td>

      </tr>

      <tr>
      <td>Sueldo neto*:</td>
        <td colspan='1'><input type="text" name="sueldo_neto" id="sueldo_neto" size='30' value='<?php if($elemento!=null && $elemento->getSueldoNeto()!= null && $elemento->getSueldoNeto()!= 0.0) echo($elemento->getSueldoNeto());?>'/></td>
        <td>Sueldo neto con letra *:</td>
        <td colspan='3'><input type="text" name="sueldo_letra" id="sueldo_letra" size='85' value='<?php if($elemento!=null && $elemento->getSueldoLetra()!= null) echo($elemento->getSueldoLetra());?>'/></td>
      </tr>

     <tr>
            <td>Número de Empleado:</td>
        <td colspan='3'><input type="text" name="numero" id="numero" size='30' value='<?php if($elemento!=null && $elemento->getNumero()!= null){ echo($elemento->getNumero());} else {echo null;}?>'/></td>
    </tr>    


      <tr>
        <td align="center" colspan="8" ><input name="guardar" type="submit" value="Guardar"  class='btn' />
        &nbsp;&nbsp;&nbsp;&nbsp;<a href='lista_empleado.php' class='liga_btn'> Cancelar </a>
        </td>
      </tr>

    </table>
    <?php if($elemento!=null && $elemento->getId()!=null) echo("<input type='hidden' name='id' value='".$elemento->getId()."' />");?>
    </form>
</div>
<?php include 'footer.php' ?>
<script type="text/javascript">
 var frmvalidator  = new Validator("fempleado");
 frmvalidator.addValidation("nombre","req","Por favor capture el nombre del empleado.");
 frmvalidator.addValidation("apaterno","req","Por favor capture el apellido paterno del empleado.");
 frmvalidator.addValidation("amaterno","req","Por favor capture el apellido materno del empleado.");
 frmvalidator.addValidation("rfc","req","Por favor capture el RFC del empleado.");
 frmvalidator.addValidation("rfc","minlength=12","Por favor verifique el RFC del empleado, la longitud mínima permitida es de 12 caracteres.");
 frmvalidator.addValidation("puesto","req","Por favor capture el puesto del empleado.");
 frmvalidator.addValidation("tipo_nomina","dontselect=0","Por favor seleccione el tipo de nómina para el empleado.");
 frmvalidator.addValidation("sueldo_neto","req","Por favor capture el sueldo del empleado.");
 frmvalidator.addValidation("sueldo_neto","numeric","Por favor capture sólo números para el sueldo del empleado.");
 frmvalidator.addValidation("sueldo_letra","req","Por favor capture el sueldo del empleado con letra.");
 
 </script>

</body>
</html>