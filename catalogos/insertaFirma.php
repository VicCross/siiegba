<?php
	session_start();
	//include_once("../src/classes/Catalogo.class.php");
	include_once("../src/mx/com/virreinato/dao/LineaAccionDaoJdbc.class.php");
	include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/LineaAccion.class.php");
	include_once("../src/mx/com/virreinato/dao/AreaDaoJdbc.class.php");
	include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Area.class.php");
	if(isset($_GET['tipo']) && $_GET['tipo'] > 0 && isset($_GET['id']) && isset($_GET['accion']) && $_GET['accion'] == "borrar"){
		$firma = NULL;
		if($_GET['tipo'] == 1){//proyecto
			$dao = new LineaAccionDaoJdbc();
			$elemento = new LineaAccion();
			$elemento = $dao->obtieneElemento($_GET['id']);
		}else if($_GET['tipo'] == 2){//area
			$dao = new AreaDaoJdbc();
			$elemento = new Area();
			$elemento = $dao->obtieneArea($_GET['id']);
		}
		$elemento->setFirma($firma);
		$res = $dao->guardaFirma($elemento);
		if($res)$respuesta = "La firma fue borrada con éxito.";
		else $respuesta = "La firma no pudo ser borrada.";
	}else if(isset($_POST['tipo']) && $_POST['tipo'] > 0){	
		/*Para el archivo*/
		$carpeta = "../firmas/";
		$extension = explode("/",$_FILES['firma']['type']);
		$nombreArch = $_POST['areaproy'] . "." . $extension[1];
		if($_POST['tipo'] == 1){//proyecto
			$dao = new LineaAccionDaoJdbc();
			$elemento = new LineaAccion();
			$elemento = $dao->obtieneElemento($_POST['areaproy']);
			$carpeta .= "proyecto/";
		}else if($_POST['tipo'] == 2){//area
			$dao = new AreaDaoJdbc();
			$elemento = new Area();
			$elemento = $dao->obtieneArea($_POST['areaproy']);
			$carpeta .= "area/";
		}
		opendir($carpeta);
		$destino = $carpeta.$nombreArch;
		copy($_FILES['firma']['tmp_name'],$destino);
		$elemento->setFirma($destino);
		$res = $dao->guardaFirma($elemento);
		if($res)$respuesta = "Firma subida y archivada con éxito.";
		else $respuesta = "Se subio la firma pero no pudo ser guardada.";
	}else{
		$respuesta = "¡Error!";	
	}
	
	header("Location: lista_firmas.php?respuesta=" . $respuesta);
?>
