<?php
session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once("../src/mx/com/virreinato/dao/ExpoTempDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatExpoTemp.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
include_once("../src/classes/Catalogo.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>

<!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="../css/lista.css">
        <script type="text/javascript" src="../media/js/complete.js"></script>
        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
        <script type="text/javascript" src="../js/lista.js"></script>
		
			   <input type="hidden" id="columnas_visibles" name="columnas_visibles" value="9"/>
    <input type="hidden" id="total_columnas" name="total_columnas" value="11"/>

</head>
<body>
<?php $respuesta = NULL;
    if(isset($_GET['respuesta'])){ $respuesta = $_GET['respuesta']; }?> 
<div class="contenido">
	<p class="titulo_cat1">Catálogos > Catálogos de Proyectos >Exposiciones Temporales</p><br/>
    <?php 
        if($respuesta!=null){
            echo("<div align='center' class='msj'>".$respuesta."</div>");
        }
    ?>
        <div align="right"><a href='agrega_expotemp.php' class='liga_btn'> Agregar Exposición Temporal</a></div>
        <br> </br>
    <table  border="0" cellspacing="0" cellpadding="5" class='	dataTable' align='center' >
        <thead>
		<tr>
          <th width="3%">Nombre</th>
          <th width="20%">Fecha de inicio</th>
          <th width="10%">Fecha clausura</th>
          <th width="10%">Título final</th>
          <th width="12%">Tipo</th>
          <th width="10%">Periodo</th>
          <th width="10%">Area que coordina</th>
          <th width="10%">Proyecto</th>
          <th width="10%">Saldo</th> 
          <th></th>
          <th></th>
        </tr>
		</thead>
		<tfoot>
		<tr>
          <th width="19%"></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
        </tr>
		</tfoot>
		<tbody>
    <?php
         $dao=new ExpoTempDaoJdbc();
        $lista=$dao->obtieneListado();
        $elemento=new ExpoTemp();
        foreach($lista as $elemento){
                ?>
        <tr>
        	<td><?php echo($elemento->getNombre());?></td>
            <td><?php echo($elemento->getInicio());?></td>
            <td><?php echo($elemento->getFin());?></td>
            <td><?php echo($elemento->getTitulo());?></td>
            <td><?php echo($elemento->getTipo());?></td>
            <td><?php echo($elemento->getPeriodo());?></td>
            <td><?php echo($elemento->getArea());?></td>
            <td><?php echo($elemento->getAccion());?></td>
            <td><?php echo($elemento->getSaldo());?></td>
            
                        
            <td><a href='agrega_expotemp.php?id=<?php echo($elemento->getId());?>' class='liga_cat'><img src="../img/Pencil3.png" alt="Editar" style="border: 0px none;" ></a></td>
            <td><a href='../src/mx/com/virreinato/web/CatProveedor.php?id=<?php echo($elemento->getId());?>' onclick='return confirm("¿Esta seguro que desea eliminar este registro?")' class='liga_cat'><img src="../img/DeleteRed.png" alt="Borrar" style="border: 0px none;"></a></td>
          </tr>	
    <?php			
    	}
    ?>
	</tbody>
    </table>
	<br>
	
</div>
<?php include 'footer.php' ?>
</body>
</html>
