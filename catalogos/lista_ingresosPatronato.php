<?php
session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once("../src/mx/com/virreinato/dao/AmigosDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/AmigosPatronato.class.php");
include_once("../src/mx/com/virreinato/dao/IngresosPatronatoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/IngresosPatronato.class.php");
include_once("../src/mx/com/virreinato/dao/ExposicionesTemporalesDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ExposicionesTemporales.class.php");
include_once("../src/mx/com/virreinato/dao/LineaAccionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/LineaAccion.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}

header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>


<!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="../css/lista.css">
        <script type="text/javascript" src="../media/js/complete.js"></script>
        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
        <script type="text/javascript" src="../js/lista.js"></script>
		<input type="hidden" id="columnas_visibles" name="columnas_visibles" value="5"/>
    	<input type="hidden" id="total_columnas" name="total_columnas" value="7"/>
</head>
<body>
    <?php $respuesta = NULL;
        if(isset($_GET['respuesta'])){ $respuesta = $_GET['respuesta']; }?>  
    <div class="contenido">
    <br>
	 <p class="titulo_cat1">Menú de Catálogos &gt; Menú de Ingresos del Patronato</p><br/>
         <?php 
            if($respuesta!=null){
                echo("<div align='center' class='msj'>".$respuesta."</div>");
            }
        ?>
         
         <br> </br>
    <table  border="0" cellspacing="0" cellpadding="5" class='dataTable' align='center' >
	  <thead>
	  <tr>
	    <th width='15%'>Tipo / Origen </th>
	    <th width='15%'>Patrocinador</th>
	    <th width='15%'>Monto</th>
	    <th width='15%'>Fecha</th>
        <th width="15%">Destino</th> <!--Proyecto destino -->
        <th width="15%">Documento</th>
	    <th width='5%'></th>
	    <th width='5%'></th>
	  </tr>
	  </thead>
	  <tfoot>
	  <tr>
	    <th width='15%'></th>
	    <th width='15%'></th>
	    <th width='15%'></th>
        <th width="15%"></th>
        <th width='15%'></th>
        <th width="15%"></th>
	    <th width='5%'></th>
	    <th width='5%'></th>
	  </tr>
	  </tfoot>
	  <tbody>
		<?php
			
        $dao = new IngresosPatronatoDaoJdbc();
        $lista = $dao->obtieneLista();
        $elemento = new IngresosPatronato();
		$daoLA = new LineaAccionDaoJdbc();
		$daoET = new ExposicionesTemporalesDaoJdbc();
		$daoTA = new AmigosDaoJdbc();
        foreach($lista as $elemento){
			$tipoAmigos = $daoTA->obtieneElementoAmigo($elemento->getId_Tipo_Amigos());
			$proyecto = $daoLA->obtieneElemento($elemento->getIdProyecto());
			$expoT = NULL;
			if(!is_null($elemento->getIdExpoTemp())) $expoT = $daoET->getElemento($elemento->getIdExpoTemp());
        ?>
    <tr class="SizeText">
	    <td align="center" ><?php echo($tipoAmigos->getNombre_tipo_amigos());?></td> <!--tipo origen -->
        <td align="center" ><?php echo($elemento->getPatrocinador());?></td>
        <td align="center"><?php echo($elemento->getMonto());?></td>
        <td align="center"><?php echo($elemento->getFecha());?></td>
        <td align="center"><?php echo($proyecto->getLineaAccion());?></td> <!--destino -->
        <!--<td align="center"><?php //if(!is_null($expoT))echo($expoT->getNombre()); else echo("-");?></td>-->
        <td align="center"><?php if(!is_null($elemento->getArchivo()))echo("<a href='".$elemento->getArchivo()."'>Ver Documento</a>"); else echo("Archivo No Disponible");?></td>
        <td align="center"><a href='agrega_ingresoPatronato.php?id=<?php echo($elemento->getId());?>' class='liga_cat'><img src="../img/Pencil3.png" alt="Aceptar" style="border: 0px none;" ></a></td>
        <td align="center"><a href='../src/mx/com/virreinato/web/CatIngresoPat.php?id=<?php echo($elemento->getId());?>' onclick='return confirm("¿Esta seguro que desea eliminar este registro?")' class='liga_cat'><img src="../img/DeleteRed.png" alt="Borrar" style="border: 0px none;"></a></td>
      </tr>	
    <?php			
		}
	  ?>
	  </tbody>
    </table>
    
    
        <br /><br />
        <div align="center"> <a href='agrega_ingresoPatronato.php' class='liga_btn'>Agregar Ingreso</a> </div>
	<br>
	
</div>
</body>
</html>
