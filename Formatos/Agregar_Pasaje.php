<?php

session_start();
include_once("../src/mx/com/virreinato/dao/PasajeDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Pasaje.class.php");
include_once("../src/mx/com/virreinato/dao/CentroCostosDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CentroCostos.class.php");
include_once("../src/mx/com/virreinato/dao/EmpleadoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Empleado.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
header('Content-Type: text/html; charset=UTF-8');
?>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link href="../css/calendario.css" type="text/css" rel="stylesheet">
<script src="../js/calendar.js" type="text/javascript"></script>
<script src="../js/calendar-es.js" type="text/javascript"></script>
<script src="../js/calendar-setup.js" type="text/javascript"></script>
<script src="../js/gen_validatorv4.js" ></script>
<script src="../js/jquery-1.7.2.js"></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>

<title>Consultar Pasajes</title>
</head>
<body>
<div class="contenido">
<br/>
<p class="titulo_cat1">Formatos > <a class="linkTitulo_cat1" href="lista_Pasaje.php" > Pasajes </a></p>
<h2 class="titulo_cat2">
    <?php
	$id = "";
        $folio = "";
        if(isset($_REQUEST["respuesta"]))
        {$respuesta= (String) $_REQUEST["respuesta"];}
	
	if (isset($_REQUEST["id"]) && $_REQUEST["id"]!= null)
        {	$id = (String)$_REQUEST["id"]; }
	else
        {	$id=null; }
	
	
	$dao= new PasajeDaoJdbc();
	$elemento = new Pasaje();

	if($id!= null){
		echo("");
		$elemento = $dao->obtieneElemento($id);
		
	}	
	else{
		echo(" ");
	}	
    ?>
</h2>
<?php $respuesta = NULL;
    if(isset($_GET['respuesta'])){ $respuesta = $_GET['respuesta']; }?> 
<form id="frmComprobacion" id="frmComprobacion" method="POST" action="../src/mx/com/virreinato/web/WebPasajes.php">
  <table width="95%" border="0" cellspacing="0" cellpadding="5" class='tb_add_cat' align='center'>
    <tr>
		<td>Fecha de la minuta*:</td>
		 <td>
		 	<input onblur="validar('Fecha','fecha')" type="text" maxlength="20" size="20" name="Fecha" id="Fecha" value="<?php if($elemento!=null && $elemento->getFecha()!= null) echo(date("d-m-Y",strtotime($elemento->getFecha()))); ?>" >
 	     </td>

		<td>U. Administrativa o Adscripción*:</td>
		<td>
			<select name="ccosto" id="ccosto" SIZE=1 >
                <?php
                 $dao2=new CentroCostosDaoJdbc();
                 $lista2=$dao2->obtieneListado();
                 $c = new CentroCostos();

                        foreach($lista2 as $c) {
                          $sel="";
                          if($elemento!= null && $elemento->getIdCCosto()!= null ){
                                  if($c->getId() == $elemento->getIdCCosto()){
                                        $sel="selected='selected'";
                                  }	
                          }	  
                          echo("<option value='".$c->getId()."' ".$sel." >".$c->getDescripcion()."</option>");
                        }
                ?> 
            </select>
		</td>
		
		<td>Nombre *:</td>
		<td>
                    <select name="empleado" id="empleado">
                        <option value="0"> Seleecciona  </option>
                        <?php
                            $daoEm=new EmpleadoDaoJdbc();
                            $listaEm = $daoEm->obtieneListado();
                            $elementoEm=new Empleado();
                            foreach($listaEm as $elementoEm){
                                $sel = "";
                                if($elemento!= null && $elemento->getEmpleado()!= null ){
                                       $empleado = $elemento->getEmpleado();
                                        if( $empleado->getId() == $elementoEm->getId()){ $sel = "selected='selected'"; }
                                    }
                                        echo("<option value=".$elementoEm->getId()." ".$sel." >" .$elementoEm->getNombre()." ".$elementoEm->getApPaterno()." ".$elementoEm->getApMaterno()."</option>");
                                }
                        ?>
                </select>
		</td>
	
	</tr>
	
	<tr>
		<td>Mes*:</td>
		 <td>
		 	<input onblur="validar('mes','fecha')" type="text" maxlength="20" size="20" name="mes" id="mes" value="<?php if($elemento!=null && $elemento->getMes()!= null) echo(date("d-m-Y",strtotime($elemento->getMes())));?>" >
 	         </td>
		
		<td>Tarifa diaria de pasajes*:</td>
		 <td>
		 	<input type="text" size="20" name="tarifaDiaria" id="tarifaDiaria" onblur='totalPasajes();'  value="<?php if($elemento!=null && $elemento->getTarifaDiaria()!= null) echo($elemento->getTarifaDiaria());?>">			
		 </td>
		
		<td>Número de días de pasajes*:</td>
		 <td>
		 	<input type="text" size="20" name="numDias"  id="numDias"  onblur='totalPasajes();' value="<?php if($elemento!=null && $elemento->getNumeroDias()!= null) echo($elemento->getNumeroDias());?>">			
		 </td>
	</tr>
	
	<tr>
	    <td>Total de Pasajes*:</td>
		<td>
		 	<input type="text" size="20" name="montoTotal" id="montoTotal" onfocus='totalPasajes();' value="<?php if($elemento!=null && $elemento->getMontoTotal()!= null) echo($elemento->getMontoTotal());?>">			
		</td>
		
		 <td>Monto total con letra*:</td>
		 <td>
		 	<input type="text"  size="40" name="letra" id="letra" value="<?php if($elemento!=null && $elemento->getLetraMonto()!= null) echo($elemento->getLetraMonto());?>">			
		 </td>
	
		 <td>Por concepto de *:</td>
		 <td>
		 	<input type="text" size="40" name="concepto" id="concepto" value="<?php if($elemento!=null && $elemento->getConcepto()!= null) echo($elemento->getConcepto());?>">			
		 </td>
		
	</tr>
	
		
	<tr>
   	  <?php  if($id == null){?>
	       <td align="center" colspan="6">
		      <input name="guardar" style="cursor:pointer" type="submit" value="Guardar"  class='btn' />
		       &nbsp; &nbsp; &nbsp;
	       	  &nbsp;&nbsp;&nbsp;<a href='lista_Pasaje.php' class='liga_btn'> Cancelar </a>
          </td>
       <?php  } ?>
     
        <?php  if($id != null){?>
	       <td align="center" colspan="6">
	       		<input name="guardar" style="cursor:pointer" type="button" onClick = "Validar()" value="Guardar"  class='btn' />
	       		&nbsp; &nbsp; &nbsp;
	       		&nbsp;&nbsp;&nbsp;<a href='lista_Pasaje.php' class='liga_btn'> Cancelar </a>
          </td>
      <?php  } ?>
	</tr>
        </table>
        <?php if($elemento!=null && $elemento->getId()!=null) echo("<input type='hidden' name='id' value='".$elemento->getId()."' />");?>
        </form>
	</div> 
</body>
<script>
    Calendar.setup({ inputField : "Fecha", ifFormat : "%d-%m-%Y", button: "Fecha" });
    Calendar.setup({ inputField : "mes", ifFormat : "%d-%m-%Y", button: "mes" });
       
    
    var frmvalidator  = new Validator("frmComprobacion");
   
	frmvalidator.addValidation("empleado","dontselect=0","Por favor seleccione el nombre del empleado.");
	frmvalidator.addValidation("Fecha","req","Por favor capture la fecha de la minuta.");
	frmvalidator.addValidation("mes","req","Por favor seleccione la fecha para indicar el mes para los pasajes.");
	frmvalidator.addValidation("tarifaDiaria","req","Por favor capture la tarifa diaria de los pasajes.");
	frmvalidator.addValidation("numDias","req","Por favor capture el número de días de pasajes.");
	frmvalidator.addValidation("montoTotal","req","Por favor capture el monto total de los pasajes.");
	frmvalidator.addValidation("letra","req","Por favor capture el monto con letra de los pasajes.");
	frmvalidator.addValidation("concepto","req","Por favor capture el concepto.");
	
    function Regresar(){
    	window.location="lista_Pasaje.php";
    }
	
	function totalPasajes(){
		var tarifaDiaria=document.getElementById('tarifaDiaria').value;
		var numDias=document.getElementById('numDias').value;
		
		if(tarifaDiaria!='' && numDias!='')
			document.getElementById('montoTotal').value=tarifaDiaria*numDias; 
		
	}
	function Validar(){
		var frmvalidator  = new Validator("frmComprobacion");
   
	
		frmvalidator.addValidation("empleado","dontselect=0","Por favor seleccione el nombre del empleado.");
		frmvalidator.addValidation("Fecha","req","Por favor capture la fecha de la minuta.");
		frmvalidator.addValidation("mes","req","Por favor seleccione la fecha para indicar el mes para los pasajes.");
		frmvalidator.addValidation("tarifaDiaria","req","Por favor capture la tarifa diaria de los pasajes.");
		frmvalidator.addValidation("numDias","req","Por favor capture el número de días de pasajes.");
		frmvalidator.addValidation("montoTotal","req","Por favor capture el monto total de los pasajes.");
		frmvalidator.addValidation("letra","req","Por favor capture el monto con letra de los pasajes.");
		frmvalidator.addValidation("concepto","req","Por favor capture el concepto.");
			    
    	 
    	 document.forms["frmComprobacion"].submit(); 
    }
	function validar(elemento,tipo)
	{
		var campo=document.getElementById(elemento);
		var texto=document.getElementById(elemento).value;
		
		switch(tipo)
		{
			case 'numerico':
				if(texto.match(/[a-z]+/gi).length!=null)
				{
					alert('El campo no puede contener letras');
					campo.value="0000";
				}
				break;
			case 'fecha':
				if(texto.match(/[a-z]+/gi).length!=null)
				{
					alert('El campo no puede contener letras');
					var date = new Date();
					var dia = date.getDate();      if( dia.toString().length == 1 ) dia = "0"+dia; 
					var mes = (date.getMonth()+1); if( mes.toString().length == 1 )  mes = "0"+mes;
					campo.value= dia+"-"+mes+"-"+date.getFullYear() ;
				}
				break;
			default:
				break;
		}
	}
	
	
</script>
</html>