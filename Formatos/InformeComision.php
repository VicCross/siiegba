<?php
include_once("../src/mx/com/virreinato/dao/ComisionDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/dao/CentroCostosDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/dao/DirectivoDaoJdbc.class.php");
session_start();
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: ../index.php");
}
header('Content-Type: text/html; charset=UTF-8');
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="/css/style_Impresion.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>

        <title> Informe de Comisión </title>
    </head>
    <body>
<?php
$dao = new ComisionDaoJdbc();
$c = $dao->obtieneElemento($_GET["folio"]);
$ccos = new CentroCostos();
$ccosDao = new CentroCostosDaoJdbc();

$ccos = $ccosDao->obtieneElemento($c->getIdCCosto());

$dirDao = new DirectivoDaoJdbc();
$sub = $dirDao->obtieneElementoCargo("2");
$dir = $dirDao->obtieneElementoCargo("1");

$meses = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
?>

        <table align="center" width="90%" border="0" style='text-transform:uppercase;font-family:arial;word-spacing:2px; letter-spacing:1px;font-size:11px'>
            <tr>
                <td align="left" colspan='2'> 
                    <img src="../img/Conaculta_INAH.png" width='180' height='35' alt="Logo" style="border:0;" />
                    <div align="center" style='float:right;text-align:center;width:70%;' > <strong> INSTITUTO NACIONAL DE ANTROPOLOGÍA E HISTORIA <br> SECRETARÍA ADMINISTRATIVA </strong> <br> INFORME DE COMISIONES </div>
                </td>

            </tr>


            <tr>
                <td align="center">&nbsp;</td>
                <td align="center">
                    <br>
                    <table align="right" width="70%" border="1" class='TablaTarjeta' style='border-style: double;	border-color: black;	border-spacing: 0px;' >
                        <tr>		
                            <td align="center" colspan='3' style='background-color:#aaa;'> FECHA </td>
                        </tr>
                        <tr>		
                            <td align="center" > DIA </td>
                            <td align="center" > MES </td>
                            <td align="center" > AÑO </td>
                        </tr>
                        <tr>		
                            <td align="center" ><?php echo date_format(date_create($c->getFechaOficio()), 'd'); ?>  </td>
                            <td align="center" ><?php echo $meses[date_format(date_create($c->getFechaOficio()), 'n')]; ?>  </td>
                            <td align="center" ><?php echo date_format(date_create($c->getFechaOficio()), 'Y'); ?>  </td>
                        </tr>		
                    </table>
                </td>
            </tr>	

            <tr>	
                <td colspan='2'> 
                    <p>
                        <strong>1. UNIDAD ADMINISTRATIVA: </strong> <span style='border-bottom:1px solid black;width:600px;display:inline-block;'><?php echo $ccos->getDescripcion(); ?></span>   
                    </p>  
                </td>
            </tr>

            <tr>	
                <td colspan='2'> 
                    <p><strong>2. NOMBRE: </strong><span style='border-bottom:1px solid black;width:745px;display:inline-block;'><?php $empleado = $c->getEmpleado(); echo $empleado->getNombre()." ".$empleado->getApPaterno()." ".$empleado->getApMAterno() ;?> </span></p> 
                </td>
            </tr>

            <tr>	
                <td colspan='2'> 
                    <p><strong>3. CARGO: </strong><span style='border-bottom:1px solid black;width:753px;display:inline-block;'><?php echo $empleado->getPuesto() ;?></span></p> 
                </td>
            </tr>

            <tr>	
                <td colspan='2'> 
                    <p><strong>4. PERIODO: </strong> Durante <span style='border-bottom:1px solid black;display:inline-block;'>
                         &nbsp;&nbsp;<?php echo $c->getNumeroDias();?>&nbsp;&nbsp;</span>   días, del 
                         <span style='border-bottom:1px solid black;display:inline-block;'>&nbsp;&nbsp;
                         <?php echo date_format(date_create($c->getPeriodoInicial()), 'd'); ?>&nbsp;&nbsp;</span> de  
                         <span style='border-bottom:1px solid black;display:inline-block;'>&nbsp;&nbsp;
                         <?php echo $meses[date_format(date_create($c->getPeriodoInicial()), 'n')]; ?>&nbsp;&nbsp;</span> al 
                         <span style='border-bottom:1px solid black;display:inline-block;'>
                        &nbsp;&nbsp;<?php echo date_format(date_create($c->getPeriodoFinal()), 'd');?>&nbsp;&nbsp;</span> de 
                        <span style='border-bottom:1px solid black;display:inline-block;'>&nbsp;&nbsp;
                         <?php echo $meses[date_format(date_create($c->getPeriodoFinal()), 'n')]; ?>&nbsp;&nbsp;
                        </span> del <span style='border-bottom:1px solid black;display:inline-block;'>
                            &nbsp;&nbsp;<?php echo date_format(date_create($c->getPeriodoFinal()), 'Y'); ?>&nbsp;&nbsp;</span>    

                    </p> 
                </td>
            </tr>

            <tr>	
                <td colspan='2'> 
                    <p><strong>5. OBJETIVO DE LA COMISIÓN: </strong><br> <?php echo $c->getObjetivo();?>
                    </p> 
                </td>
            </tr>

            <tr>	
                <td colspan='2'> 
                    <p><strong>6. RESULTADOS OBTENIDOS: </strong> <br> <?php echo $c->getResultados();?>
                    </p> 
                    <br><br><br>
                </td>
            </tr>


            <tr>	
                <td colspan='2' align='center'> 
                    <p><strong>COMISIONADO</strong></p>
                    <br><br><br><br>
                </td>
            </tr>

            <tr>	
                <td colspan='2' align='center'> 
                    <p><span style='border-bottom:1px solid black;display:inline-block;'>&nbsp;&nbsp;&nbsp;&nbsp;
                    <?php echo ($empleado->getNombre()." ".$empleado->getApPaterno()." ".$empleado->getApMaterno() );?>&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
                    <?php echo ($empleado->getPuesto() );?><br><br>
                </td>
            </tr>

            <tr>	
                <td colspan='2' align='center'> 
                    <p><strong>Vo.Bo.</strong></p>
                    <br><br><br><br>
                </td>
            </tr>

            <tr>	
                <td align='center'> 
                    <p><span style='border-bottom:1px solid black;display:inline-block;'>&nbsp;&nbsp;&nbsp;&nbsp;
                        <?php echo ($sub->getNombre()." ".$sub->getApPaterno()." ".$sub->getApMaterno() );?>&nbsp;&nbsp;&nbsp;&nbsp;</span></p><?php echo ($sub->getCargo());?>
                </td>
                <td align='center'> 
                    <p><span style='border-bottom:1px solid black;display:inline-block;'>&nbsp;&nbsp;&nbsp;&nbsp;
                        <?php echo ($dir->getNombre()." ".$dir->getApPaterno()." ".$dir->getApMaterno() );?>&nbsp;&nbsp;&nbsp;&nbsp;</span></p><?php echo ($dir->getCargo());?>
                </td>
            </tr>

        </table>

    </body>
</html>