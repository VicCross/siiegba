<?php

session_start();
include_once("../src/mx/com/virreinato/dao/AreaDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/beans/Area.class.php");
include_once("../src/mx/com/virreinato/dao/LineaAccionDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/beans/LineaAccion.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
header('Content-Type: text/html; charset=UTF-8');
?>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link href="../css/calendario.css" type="text/css" rel="stylesheet">
<script src="../js/calendar.js" type="text/javascript"></script>
<script src="../js/calendar-es.js" type="text/javascript"></script>
<script src="../js/calendar-setup.js" type="text/javascript"></script>
<script src="../js/gen_validatorv4.js" ></script>
<script src="../js/jquery-1.7.2.js"></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>

<title>Consultar Pasajes</title>
</head>
<body>
<div class="contenido">
<br/>
<p class="titulo_cat1">Formatos >  Documentación</p>
	<form id="frmDoc" name="frmDoc" method="POST" action="../src/mx/com/virreinato/web/WebDocumentacion.php" enctype="multipart/form-data">
		<table width="60%" border="0" class='tb_add_cat' align='center'>
        	<tr>
            	<td align="center">Proyecto</td>
                <td>
                	<select name="proyecto" id="proyecto">
                        <option value="0">----Selecciona un Proyecto----</option>
                        <?php
                        $dao = new LineaAccionDaoJdbc();
						$lista = $dao->obtieneListado();
						$elemento = new LineaAccion();
                        foreach($lista as $elemento){
                        ?>
                        <option value="<?php echo($elemento->getId());?>"><?php echo($elemento->getLineaAccion());?></option>
                        <?php
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
            	<td align="center">Área</td>
                <td>
                	<select name="area" id="area">
                    	<option value="0">----Selecciona una Área----</option>
                        <?php
						$dao = new AreaDaoJdbc();
						$lista = $dao->obtieneAreas();
						$elemento = new Area();
						foreach($lista as $elemento){
						?>
                        <option value="<?php echo($elemento->getId());?>"><?php echo($elemento->getDescripcion());?></option>
						<?php }?>
                    </select>
                </td>
            </tr>
            <tr>
            	<td align="center">Número de Versión*:</td>
                <td><input type="text" id="version" name="version"> </td>
            </tr>
            <tr>
            	<td align="center">Archivo(Tamaño máximo 2MB)*:</td>
                <td><input type="file" id="archivo" name="archivo"> </td>
            </tr>            
            <tr>
            	<td align="center">Observaciones:</td>
                <td><textarea id="observaciones" name="observaciones" rows="5" cols="40"></textarea></td>
            </tr>
    		<tr>
	       		<td align="center" colspan="2">
	       			<input name="guardar" id="guardar" style="cursor:pointer" type="submit" value="Guardar"  class='btn' />
	       			&nbsp; &nbsp; &nbsp;
	       			&nbsp;&nbsp;&nbsp;<a href='lista_Documentos.php' class='liga_btn'> Cancelar </a>
          		</td>
      		</tr>
        </table>
    </form>
	</div> 
</body>
<script>

		var frmvalidator  = new Validator("frmDoc");	
		frmvalidator.addValidation("version","req","Por favor ingrese el número de versión.");	
		frmvalidator.addValidation("version","num","El formato del número de versión es incorrecto. Por favor, modifíquelo.");
		frmvalidator.addValidation("archivo","req","Por favor ingrese el documento.");	
</script>
</html>