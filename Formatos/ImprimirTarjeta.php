<?php

session_start();
include_once("../src/mx/com/virreinato/dao/TarjetaRegistroDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/TarjetaRegistro.class.php");
include_once("../src/mx/com/virreinato/dao/CatProyectoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatProyecto.class.php");
include_once("../src/mx/com/virreinato/dao/TarjetaRecAutorizadoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/TarjetaRecAutorizados.class.php");
include_once("../src/mx/com/virreinato/dao/TarjetaMetasDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/TarjetaMetas.class.php");
include_once("../src/mx/com/virreinato/dao/TarjetaCalendarioDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/TarjetaCalendario.class.php");
include_once("../src/mx/com/virreinato/dao/TarjetaPersonalDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/TarjetaPersonal.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="../css/style_Impresion.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
  $().ready(function(){
  	   var elaboracion = $("#fechaElaboracion").val();
  	   var auxElaboracion = elaboracion.split("-");
  	   $("#diaFechaElaboracion").text(auxElaboracion[0]);
  	   $("#mesFechaElaboracion").text(auxElaboracion[1]);
  	   $("#anioFechaElaboracion").text(auxElaboracion[2]);
  	   
  	   var autorizacion = $("#fechaAutorizacion").val();
  	   if(autorizacion != ""){
  	   	 var auxAutorizacion = autorizacion.split("-");
  	   	 $("#diaFechaAutorizacion").text(auxAutorizacion[0]);
  	   	 $("#mesFechaAutorizacion").text(auxAutorizacion[1]);
  	   	 $("#anioFechaAutorizacion").text(auxAutorizacion[2]);
  	   }
  	   
  	   var InicioTotal = $("#fechaIniTotal").val();
  	   if( InicioTotal!= "" ){
  	   	 var auxIniTotal = InicioTotal.split("-");
  	   	 $("#mesIniTotal").text(auxIniTotal[1]);
  	   	 $("#anioIniTotal").text(auxIniTotal[2]);
  	   }
  	   
  	   var FinTotal = $("#fechaFinTotal").val();
  	   if( FinTotal!= "" ){
  	   	 var auxFinTotal = FinTotal.split("-");
  	   	 $("#mesFinTotal").text(auxFinTotal[1]);
  	   	 $("#anioFinTotal").text(auxFinTotal[2]);
  	   }
  	   
  	   var InicioEjer = $("#fechaIniEjer").val();
  	   if( InicioEjer!= "" ){
  	   	 var auxInicioEjer = InicioEjer.split("-");
  	   	 $("#mesIniEjer").text(auxInicioEjer[1]);
  	   	 $("#anioIniEjer").text(auxInicioEjer[2]);
  	   }
  	   
  	   var FinEjer = $("#fechaFinEjer").val();
  	   if( FinEjer!= "" ){
  	   	 var auxFinEjer = FinEjer.split("-");
  	   	 $("#mesFinEjer").text(auxFinEjer[1]);
  	   	 $("#anioFinEjer").text(auxFinEjer[2]);
  	   }  	   
  	   
  	});
</script>
<title>Impresión Tarjeta de Registro</title>
</head>
<?php
  $folio= (String) $_GET['folio'];
  $daoTarjeta = new TarjetaRegistroDaoJdbc();
  $elemento = new TarjetaRegistro();
  $tarjeta = new TarjetaRegistro();

  $elemento = $daoTarjeta->obtieneElemento($folio);
  $tarjeta = $daoTarjeta->Imprimir( (String)($elemento->getIdProyecto()));

?>
<body>
    <div id="encabezado">
        <table class="TablaTarjeta" align="left" width="68%" border="1" cellspacing="0" cellpadding="0">
            <tr>
                <td class="CabezaraINA" align="center">
                    <br/><br/>
                    INSTITUTO NACIONAL DE ANTROPOLOGIA E HISTORIA
                    <br/> SECRETARIA TÉCNIA
                    <br/>TARJETA DE REGISTRO DE PROYECTO DE EJECICIO PRESUPUESTAL <?php echo( date('Y') );  ?>
                    <br/><br/>
                </td>
            </tr>	
        </table>
        &nbsp; 
        <table align="right" class="TablaTarjeta" width="30%" border="1" cellspacing="0" cellpadding="0">
            <tr>
                    <td class="ResaltarTarjeta" align="center" colspan="3">FECHA DE ELABORACIÓN</td>
            </tr>
            <tr >				
                <td class="FechaDesTarjeta" width="4%" align="center">DÍA</td>
                <td class="FechaDesTarjeta" width="4%" align="center">MES</td>
                <td class="FechaDesTarjeta" width="4%" align="center">AÑO</td>
            </tr>

            <tr>
               <input type="hidden" id="fechaElaboracion" value="<?php if($elemento!=null && $elemento->getFechaElaboracion()!=null ) echo( date("d-m-Y",strtotime($elemento->getFechaElaboracion())));  ?>"/>
                <td class="TarjetaFecha" width="4%" id="diaFechaElaboracion" align="center"></td>
                <td class="TarjetaFecha" width="4%" id="mesFechaElaboracion" align="center"></td>
                <td class="TarjetaFecha" width="4%" id="anioFechaElaboracion" align="center"></td>
            </tr>	

            <tr>
                <td class="ResaltarTarjeta" align="center" colspan="3">FECHA DE AUTORIZACIÓN</td>
            </tr>
            <tr>				
                <td class="FechaDesTarjeta" width="4%" align="center">DÍA</td>
                <td class="FechaDesTarjeta" width="4%" align="center">MES</td>
                <td class="FechaDesTarjeta" width="4%" align="center">AÑO</td>
            </tr>

            <tr>
               <input type="hidden" id="fechaAutorizacion" value="<?php if($elemento!=null && $elemento->getFechaAutorizacion()!=null ) echo( date("d-m-Y",strtotime($elemento->getFechaAutorizacion())));  ?>"/>
                <td class="TarjetaFecha" width="4%" id="diaFechaAutorizacion" align="center"><br/></td>
                <td class="TarjetaFecha" width="4%" id="mesFechaAutorizacion"  align="center"><br/></td>
                <td class="TarjetaFecha" width="4%" id="anioFechaAutorizacion" align="center"><br/></td>
            </tr>	
  	</table>
	</div>
	
	<div id="descripcion" >
	   <br/><br/><br/><br/>
	  <table class="TablaTarjeta" align="center" width="100%" border="1" cellspacing="0" cellpadding="0">
            <tr>
                <td class="TarjetaDatos" align="center" colspan="8">DATOS GENERALES DEL PROYECTO</td>
            </tr>
			
            <tr>
                <td class="ResaltarTarjeta" width="20%" align="center"> UNIDAD ADMINISTRATIVA </td>
                <td class="TarjetaFecha" align="center" colspan="7"  ><?php if($elemento!=null && $elemento->getUnidadAdministrativa()!= null) echo($elemento->getUnidadAdministrativa());?></td>

            </tr>

            <tr>
                <td class="ResaltarTarjeta" width="20%" align="center"> ÁREA NORMATIVA </td>
                <td class="TarjetaFecha" align="center" colspan="7" ><?php if($tarjeta!=null && $tarjeta->getAreaNormativa()!=null) echo($tarjeta->getAreaNormativa()); ?></td>

            </tr>
            <tr>
                <td class="ResaltarTarjeta" width="20%" align="center"> NOMBRE DEL PROYECTO </td>
                <td class="TarjetaFecha" align="center" colspan="7" >
                   <?php
                        $dao=new CatProyectoDaoJdbc();
                        $lista=$dao->obtieneListado2();
                        $elementoLista=new CatProyecto();

                        foreach($lista as $elementoLista){
                            if($elemento!= null && $elemento->getIdProyecto()!= null ){
                                if( $elemento->getIdProyecto() == $elementoLista->getId() ){ echo($elementoLista->getDescripcion()); }
                            }
                        }
                    ?>
                </td>

            </tr>
            <tr>
                <td class="ResaltarTarjeta" width="20%" align="center"> RESPONSABLE </td>
                <td class="TarjetaFecha" align="center" colspan="7" ><?php if($elemento!=null && $elemento->getResponsable()!= null) echo($elemento->getResponsable());?></td>

            </tr>
			
            <tr>
                <td class="ResaltarTarjeta" width="20%" align="center"> LINEA DE ACCIÓN
                 <br/> &nbsp; CLAVE: <?php if($elemento!=null && $elemento->getClaveAccion()!= null) echo($elemento-> getClaveAccion());?> </td>
                <td class="TarjetaFecha" align="center" colspan="7" ><?php if($tarjeta!=null && $tarjeta->getLineaAccion()!=null)echo($tarjeta->getLineaAccion()); ?></td>

            </tr>

            <tr>
                <td class="ResaltarTarjeta" width="20%" align="center"> CLAVE SECRETARIA TÉCNIA </td>
                <td class="ResaltarTarjeta"  colspan="2"  align="center" >NÚMERO DE PROYECTO</td>
                <td class="ResaltarTarjeta"  colspan="5"  align="center" >AUTORIZADO SIN FINANCIAMIENTO DE RECURSOS</td>

            </tr>

            <tr>
                <td class="TarjetaFecha" width="20%" align="center"> <?php if($elemento!=null && $elemento->getClaveTecnica()!= null) echo($elemento->getClaveTecnica());?> <br/> </td>
                <td class="TarjetaFecha" colspan="2" align="center" ><?php if($tarjeta!=null && $tarjeta->getNum_Proyecto()!=null) echo($tarjeta->getNum_Proyecto()); ?></td>
                <td class="TarjetaFecha" colspan="5"  align="center" ><?php if($elemento!=null && $elemento->getAutorizadoSinFinanciamiento()!= null) echo($elemento->getAutorizadoSinFinanciamiento());?> <br/> </td>
            </tr>
			
            <tr>
                <td class="TarjetaDatos" width="20%" align="center" colspan="2" >CLAVE PROGRAMATICA</td>
                <td class="TarjetaDatos" align="center" colspan="6">MARQUE CON UNA (X) EN TIPO DE PROYECTO</td>
            </tr>

            <tr>
               <td class="ResaltarTarjeta"  align="center" style="font-size: 7pt"> FUNCIÓN</td>
               <td class="TarjetaFecha" align="center"  width="20%" style="font-size: 7pt" ><?php if($elemento!=null && $elemento->getFuncion()!= null) echo($elemento->getFuncion());?></td>
               <td class="ResaltarTarjeta" align="center" style="font-size: 7pt"> PERMANENTE</td>
               <td class="TarjetaFecha" align="center" width="15%" width="5%" style="font-size: 7pt" >
                 <?php if($elemento!=null && $elemento->getTipoProyecto()!=null && $elemento->getTipoProyecto() == "1") echo("X"); ?>
               </td>
               <td class="ResaltarTarjeta" colspan="4" rowspan="3" align="center" style="font-size: 7pt"></td>
            </tr>
			
			
            <tr>
               <td class="ResaltarTarjeta" align="center" style="font-size: 7pt"> SUBFUNCIÓN</td>
               <td class="TarjetaFecha" align="center"  style="font-size: 7pt" ><?php if($elemento!=null && $elemento->getSubFuncion()!= null) echo($elemento->getSubFuncion());?></td>
               <td class="ResaltarTarjeta" align="center" style="font-size: 7pt"> CON FECHA DE TERMINO </td>
               <td class="TarjetaFecha" align="center" style="font-size: 7pt" >
                 <?php if($elemento!=null && $elemento->getTipoProyecto()!=null && $elemento->getTipoProyecto() == "2") echo("X"); ?>
               </td>
            </tr>

            <tr>
               <td class="ResaltarTarjeta" align="center" style="font-size: 7pt"> PROGRAMA GENERAL</td>
               <td class="TarjetaFecha" align="center"   style="font-size: 7pt" ><?php if($elemento!=null && $elemento->getProgramaGeneral()!= null) echo($elemento->getProgramaGeneral());?></td>
               <td class="ResaltarTarjeta" width="18%" align="center" style="font-size: 7pt"> NUEVA CREACIÓN</td>
               <td class="TarjetaFecha" align="center"  style="font-size: 7pt" >
                  <?php if($elemento!=null && $elemento->getTipoProyecto()!=null && $elemento->getTipoProyecto() == "3") echo("X"); ?>
               </td>
            </tr>
			
            <tr>
               <td class="ResaltarTarjeta" align="center" style="font-size: 7pt"> ACTIVIDAD INSTITUCIONAL</td>
               <td class="TarjetaFecha" align="center"  style="font-size: 7pt" ><?php if($elemento!=null && $elemento->getActividadInstitucional()!= null) echo($elemento->getActividadInstitucional());?></td>
               <td class="ResaltarTarjeta" align="center" colspan="6" style="font-size: 7pt"> SEGUIMIENTO DEL PROYECTO</td>
            </tr>

            <tr>
               <td class="ResaltarTarjeta" align="center" style="font-size: 7pt"> ACTIVIDAD PRIORITATIA</td>
               <td class="TarjetaFecha" align="center" style="font-size: 7pt" ><?php if($elemento!=null && $elemento->getActividadPrioritaria()!= null) echo($elemento->getActividadPrioritaria());?></td>
               <td class="ResaltarTarjeta"  align="center" style="font-size: 7pt">PROGRAMADO A CONCUIR</td>
               <td class="TarjetaFecha" align="center" style="font-size: 7pt" ><?php if($elemento!=null && $elemento->getConcluir()!=null) echo($elemento->getConcluir()); ?></td>
               <td class="ResaltarTarjeta" align="center"  style="font-size: 7pt"> REACTIVADO</td>
               <td class="TarjetaFecha" align="center"  style="font-size: 7pt" colspan="3" ><?php if($elemento!=null && $elemento->getReactivado()!=null) echo($elemento->getReactivado()); ?></td>
            </tr>

            <tr>
                    <td class="ResaltarTarjeta" width="20%" align="center" colspan="2" style="font-size: 7pt" >COSTO TOTAL DEL PROYECTO <br/> (MONTO APROX. $)</td>
                    <td class="ResaltarTarjeta" align="center" colspan="6" style="font-size: 7pt">PRODUCTO PRINCIPAL DEL PROYECTO (AL TERMINO DE SU DURACIÓN)</td>
            </tr>
			
            <tr>
                    <td class="TarjetaFecha" width="20%" colspan="2" align="center" ><?php if($tarjeta!=null ) echo("$".number_format($tarjeta->getCostoTotal(),2) ); ?><br/></td>
                    <td class="TarjetaFecha" align="center" colspan="6"><?php if($elemento!=null && $elemento->getProductoPrincipal()!=null) echo($elemento->getProductoPrincipal());?><br/></td>
            </tr>

            <tr>
                    <td class="TarjetaDatos" align="center" colspan="8">DURACIÓN DEL PROYECTO</td>
            </tr>

            <tr>
                    <td class="TarjetaDatos" align="center" colspan="8">TOTAL</td>
            </tr>

            <tr>
               <td class="ResaltarTarjeta"  rowspan="2" align="center" style="font-size: 7pt"> FECHA DE INICIO</td>
               <td class="ResaltarTarjeta"  align="center" style="font-size: 7pt"> MES</td>
               <td class="ResaltarTarjeta"  align="center" style="font-size: 7pt"> AÑO</td>
               <td class="ResaltarTarjeta"  rowspan="2" align="center" style="font-size: 7pt"> FECHA DE TERMINO</td>
               <td class="ResaltarTarjeta" width="20%" align="center" style="font-size: 7pt"> MES</td>
               <td class="ResaltarTarjeta"  align="center" style="font-size: 7pt" colspan="3"> AÑO</td>
            </tr>
			
            <tr>
               <input type="hidden" id="fechaIniTotal" value="<?php if($tarjeta!=null && $tarjeta->getFechaInicio_Total()!=null)echo( date("d-m-Y",strtotime( $tarjeta->getFechaInicio_Total())));  ?>"/>
               <input type="hidden" id="fechaFinTotal" value="<?php if($tarjeta!=null && $tarjeta->getFechaFin_Total()!=null)echo( date("d-m-Y",strtotime( $tarjeta->getFechaFin_Total())));  ?>"/>
               <td class="TarjetaFecha" align="center" id="mesIniTotal"  style="font-size: 7pt" ></td>
               <td class="TarjetaFecha" align="center" id="anioIniTotal" style="font-size: 7pt" ></td>
               <td class="TarjetaFecha" align="center" id="mesFinTotal"  style="font-size: 7pt" ></td>
               <td class="TarjetaFecha" align="center" id="anioFinTotal"  style="font-size: 7pt" colspan="3" ></td>
            </tr>

            <tr>
                    <td class="TarjetaDatos" align="center" colspan="8">PRESENTE EJERCICIO</td>
            </tr>

            <tr>
               <td class="ResaltarTarjeta" rowspan="2" align="center" style="font-size: 7pt"> FECHA DE INICIO</td>
               <td class="ResaltarTarjeta" align="center" style="font-size: 7pt"> MES</td>
               <td class="ResaltarTarjeta" align="center" style="font-size: 7pt"> AÑO</td>
               <td class="ResaltarTarjeta" rowspan="2" align="center" style="font-size: 7pt"> FECHA DE TERMINO</td>
               <td class="ResaltarTarjeta" align="center" style="font-size: 7pt"> MES</td>
               <td class="ResaltarTarjeta" align="center" style="font-size: 7pt" colspan="3"> AÑO</td>
            </tr>
			
            <tr>
               <input type="hidden" id="fechaIniEjer" value="<?php if($tarjeta!=null && $tarjeta->getFechaInicio_Presente()!=null)echo( date("d-m-Y",strtotime( $tarjeta->getFechaInicio_Presente())) );  ?>"/>
               <input type="hidden" id="fechaFinEjer" value="<?php if($tarjeta!=null && $tarjeta->getFechaFin_Presente()!=null)echo( date("d-m-Y",strtotime( $tarjeta->getFechaFin_Presente()) ));  ?>"/>
               <td class="TarjetaFecha" align="center" id="mesIniEjer"  style="font-size: 7pt" ></td>
               <td class="TarjetaFecha" align="center" id="anioIniEjer" style="font-size: 7pt" ></td>
               <td class="TarjetaFecha" align="center" id="mesFinEjer"  style="font-size: 7pt" ></td>
               <td class="TarjetaFecha" align="center" id="anioFinEjer"  style="font-size: 7pt" colspan="3" ></td>
            </tr>

            <tr>
               <td class="ResaltarTarjeta" align="center" colspan="2" style="font-size: 7pt"> AVANCE PORCENTUAL<BR/>DE METAS DEL PROYECTO</td>
               <td class="ResaltarTarjeta" align="center" style="font-size: 7pt"> ACUMULADO AL AÑO ANTERIOR</td>
               <td class="TarjetaFecha" align="center" style="font-size: 7pt" ><?php if($elemento!=null && $elemento->getAcumulado()!=null) echo($elemento->getAcumulado()); ?></td>
               <td class="ResaltarTarjeta"  align="center" style="font-size: 7pt"> PROGRAMADO PARA EL 2013</td>
               <td class="TarjetaFecha" align="center" style="font-size: 7pt" colspan="3" ><?php if($elemento!=null && $elemento->getProgramado()!=null) echo($elemento->getProgramado()); ?></td>
            </tr>

            <tr>
                <td class="TarjetaDatos" align="center" colspan="8">RECURSOS AUTORIZADOS</td>
            </tr>
			
            <tr>
                <td class="TarjetaDatos" align="center">TIPO DE FINANCIAMIENTO</td>
                <?php
                   $limite = (int)date('Y') - 6;
                   $TotalRecursos = 0;
                    $daoRecursos = new TarjetaRecAutorizadoDaoJdbc();
                    $listaRecursos = $daoRecursos->Imprimir($folio,$limite );
                    $Recursos = new TarjetaRecAutorizados();

                    foreach($listaRecursos as $Recursos){
                ?>
                <td class="TarjetaDatos" align="center"><?php echo($Recursos->getAnio());?></td>      
                <?php  }  ?>
			  
                <td class="TarjetaDatos" align="center">TOTAL</td>
				  
            </tr>
			
            <tr>
                <td class="ResaltarTarjeta" align="center" style="font-size: 7pt"> INGRESOS AUTOGENERADOS<br/>(ESPECIFICOS)</td>
                <?php 
                   $TotalIngresos = 0;
                   foreach($listaRecursos as $Recursos){
                        $TotalIngresos = $Recursos->getIngresoAutogenerado() + $TotalIngresos;
                    ?> 
                 <td class="TarjetaFecha" align="center"><?php echo("$".number_format( $Recursos->getIngresoAutogenerado(),2) );?></td>
                <?php   
                    }
                 ?>            
                <td class="TarjetaFecha"  align="center"><?php echo("$".number_format($TotalIngresos,2)); $TotalRecursos = $TotalIngresos + $TotalRecursos; ?></td>  
                
            </tr>
			
            <tr>
                <td class="ResaltarTarjeta" align="center" style="font-size: 7pt"> APORTACIONES DE TERCEROS </td>
                <?php
                    $TotalTerceros = 0;
                    foreach($listaRecursos as $Recursos){
                        $TotalTerceros = $Recursos->getAportacionesTerceros() + $TotalTerceros;
	  		      	 	 
                ?> 
                <td class="TarjetaFecha" align="center"><?php echo("$".number_format( $Recursos->getAportacionesTerceros(),2) );?></td>
                <?php 	}?>            
                <td class="TarjetaFecha" align="center"><?php echo("$".number_format($TotalTerceros,2));  $TotalRecursos = $TotalTerceros + $TotalRecursos; ?></td>
            </tr>
			
            <tr>
                <td class="ResaltarTarjeta" align="center" width="15%" style="font-size: 7pt"> TOTAL </td>
                <?php
                    foreach($listaRecursos as $Recursos){
               ?>
                    <td class="TarjetaFecha" align="center"><?php echo("$".number_format($Recursos->getSumaRecursos(),2) ); ?></td>
               <?php }?>

                <td class="TarjetaFecha" align="center"><?php echo("$".number_format($TotalRecursos,2));?></td>
            </tr>
			
            <tr>
                <td class="ResaltarTarjeta" align="center" style="font-size: 6pt"> EN CASO DE RECURSOS APORTADOS<br/>POR TERCEROS, ANOTAR EL NOMBRE<br/> DE LA PERSONA QUE APORTA </td>
                <td class="TarjetaFecha" align="center" colspan="7" ><?php if($elemento!=null && $elemento->getRecursos_Terceros()!=null) echo($elemento->getRecursos_Terceros()); ?></td>
            </tr>

            <tr>
                <td class="TarjetaDatos" align="center" colspan="8">OBJETIVOS DEL PROYECTO (SINTESIS)</td>
            </tr>

            <tr>
                <td class="TarjetaFecha" align="center" colspan="8" >&nbsp; &nbsp;<br/><?php if($elemento!=null && $elemento->getObjetivo()!=null) echo($elemento->getObjetivo());?><br/><br/> </td>
            </tr>

            <tr>
                <td class="TarjetaDatos" align="center" colspan="3" >FASES DEL PROYECTO CONCLUIDAS</td>
                <td class="TarjetaDatos" align="center" colspan="5">N&UacuteMERO DE OFICIO CON QUE LA SECRETARIA TÉCNICA<br/>AUTORIZA MODIFICACIÓN EN EL PROYECTO</td>
            </tr>
			
            <tr>
                <td class="TarjetaDatos" width="5%" align="center" >FASES</td>
                <td class="TarjetaDatos" width="5%" align="center" >DEL AÑO</td>
                <td class="TarjetaDatos" width="5%" align="center" >AL AÑO</td>
                <td class="TarjetaDatos" align="center" style="font-size: 7pt" >COSTO TOTAL</td>
                <td class="TarjetaFecha" align="center" colspan="4" ><?php if($elemento!=null && $elemento->getCostoTotal_Modificado()!= null) echo($elemento->getCostoTotal_Modificado());?></td>
            </tr>

            <tr>
               <td class="TarjetaFecha" align="center" >I</td>
               <td class="TarjetaFecha" align="center" ><?php if($elemento!=null && $elemento->getDel_Fase1()!= null) echo($elemento->getDel_Fase1());?></td>
               <td class="TarjetaFecha" align="center" ><?php if($elemento!=null && $elemento->getAl_Fase1()!= null) echo($elemento->getAl_Fase1());?></td>
               <td class="TarjetaDatos" align="center" style="font-size: 7pt" >PRODUCTO PRINCIPAL</td>
               <td class="TarjetaFecha" align="center" colspan="4" ><?php if($elemento!=null && $elemento->getProductoPrincipal_Modificado()!= null) echo($elemento->getProductoPrincipal_Modificado());?></td>
            </tr>
			
            <tr>
               <td class="TarjetaFecha" align="center" >II</td>
               <td class="TarjetaFecha" align="center" ><?php if($elemento!=null && $elemento->getDel_Fase2()!= null) echo($elemento->getDel_Fase2());?></td>
               <td class="TarjetaFecha" align="center" ><?php if($elemento!=null && $elemento->getAl_Fase2()!= null) echo($elemento->getAl_Fase2());?></td>
                <td class="TarjetaDatos" align="center" style="font-size: 7pt" >LINEA DE ACCION</td>
               <td class="TarjetaFecha" align="center" colspan="4" ><?php if($elemento!=null && $elemento->getLineaAccion_Modificado()!= null) echo($elemento->getLineaAccion_Modificado());?></td>
            </tr>

            <tr>
               <td class="TarjetaFecha" align="center" >III</td>
               <td class="TarjetaFecha" align="center" ><?php if($elemento!=null && $elemento->getDel_Fase3()!= null) echo($elemento->getDel_Fase3());?></td>
               <td class="TarjetaFecha" align="center" ><?php if($elemento!=null && $elemento->getAl_Fase3()!= null) echo($elemento->getAl_Fase3());?></td>
                <td class="TarjetaDatos" align="center" style="font-size: 7pt" >NOMBRE DEL PROYECTO</td>
               <td class="TarjetaFecha" align="center" colspan="4" ><?php if($elemento!=null && $elemento->getNombreProyecto_Modificado()!= null) echo($elemento->getNombreProyecto_Modificado());?></td>
            </tr>

            <tr>
               <td class="TarjetaFecha" align="center" >IV</td>
               <td class="TarjetaFecha" align="center" ><?php if($elemento!=null && $elemento->getDel_Fase4()!= null) echo($elemento->getDel_Fase4());?></td>
               <td class="TarjetaFecha" align="center" ><?php if($elemento!=null && $elemento->getAl_Fase4()!= null) echo($elemento->getAl_Fase4());?></td>
                <td class="TarjetaDatos" align="center" style="font-size: 7pt" >A PERMANENTE</td>
               <td class="TarjetaFecha" align="center" colspan="4" ><?php if($elemento!=null && $elemento->getPermanente_Modificado()!= null) echo($elemento->getPermanente_Modificado());?></td>
            </tr>
			
            <tr>
               <td class="TarjetaFecha" align="center" >V</td>
               <td class="TarjetaFecha" align="center" ><?php if($elemento!=null && $elemento->getDel_Fase5()!= null) echo($elemento->getDel_Fase5());?></td>
               <td class="TarjetaFecha" align="center" ><?php if($elemento!=null && $elemento->getAl_Fase5()!= null) echo($elemento->getAl_Fase5());?></td>
                <td class="TarjetaDatos" align="center" style="font-size: 7pt" >PROGRAMADO A CONCLUIR</td>
               <td class="TarjetaFecha" align="center" colspan="4" ><?php if($elemento!=null && $elemento->getConcluir_Modificado()!= null) echo($elemento->getConcluir_Modificado());?></td>
            </tr>

            <tr>
               <td class="TarjetaFecha"  align="center" >VI</td>
               <td class="TarjetaFecha"  align="center" ><?php if($elemento!=null && $elemento->getDel_Fase6()!= null) echo($elemento->getDel_Fase6());?></td>
               <td class="TarjetaFecha"  align="center" ><?php if($elemento!=null && $elemento->getAl_Fase6()!= null) echo($elemento->getAl_Fase6());?></td>
                <td class="TarjetaDatos" align="center" style="font-size: 7pt" >AÑO DE</td>
               <td class="TarjetaFecha"  align="center" ><?php if($elemento!=null && $elemento->getAnio_Modificacion() != null) echo($elemento->getAnio_Modificacion() );?></td>
                <td class="TarjetaDatos" align="center" style="font-size: 7pt" width="7%" >AL</td>
               <td class="TarjetaFecha"  align="center" width="35%" colspan="2" ><?php if($elemento!=null && $elemento->getAl_Modificacion()!= null) echo($elemento->getAl_Modificacion());?></td>
            </tr>
			
            <tr>
                <td class="TarjetaDatos" align="center" colspan="2" >METAS Y/O UNIDADES DE MEDIDA A REALIZAR<br/> EN EL AÑO</td>
                <td class="TarjetaDatos" align="center" colspan="6">PROGRAMACI&OacuteN TRIMESTRAL</td>
            </tr>
			
            <tr>
                <td class="TarjetaDatos"  align="center" style="font-size: 7pt" >CLAVE</td>
                <td class="TarjetaDatos" align="center" style="font-size: 7pt">NOMBRE DE ACUERDO AL CATÁLOGO DE UNIDADES<br/>DE MEDIDA AUTORIZADO</td>
                <td class="TarjetaDatos" align="center" style="font-size: 7pt">PRIMERO</td>
                <td class="TarjetaDatos" align="center" style="font-size: 7pt">SEGUNDO</td>
                <td class="TarjetaDatos" align="center" style="font-size: 7pt">TERCERO</td>
                <td class="TarjetaDatos" align="center" style="font-size: 7pt">CUARTO</td>
                <td class="TarjetaDatos" align="center" style="font-size: 7pt" colspan="2">TOTAL <br/>(ABSOLUTO)</td>
            </tr>						
			
            <?php
                $daoMetas = new TarjetaMetasDaoJdbc();
                $listaMetas = $daoMetas->obtieneListado($folio);
                $Metas = new TarjetaMetas();

                $TotalMetas = 0;
                $num=0;
                foreach($listaMetas as $Metas){
                    $num++;
                    $TotalMetas = $Metas->getTrimestral1() . $Metas->getTrimestral2() . $Metas->getTrimestral3() . $Metas->getTrimestral4();

            ?>
            <tr>
                <td class="TarjetaFecha"  align="center" ><?php echo($Metas->getClave());  ?></td>
                <td class="TarjetaFecha"  align="center" ><?php echo($Metas->getNombreAcuerdo());  ?></td>
                <td class="TarjetaFecha"  align="center" ><?php echo($Metas->getTrimestral1());  ?></td>
                <td class="TarjetaFecha"  align="center" ><?php echo($Metas->getTrimestral2());  ?></td>
                <td class="TarjetaFecha"  align="center" ><?php echo($Metas->getTrimestral3());  ?></td>
                <td class="TarjetaFecha"  align="center" ><?php echo($Metas->getTrimestral4());  ?></td>
                <td class="TarjetaFecha"  align="center" colspan="2"><?php echo($TotalMetas);  ?></td>
            </tr>
            <?php $TotalMetas = 0; }?>
			      
			
			
	  </table>
	  <label class="Nota"> <b>NOTAS:</b> 1) ESTA TARJETA  DEBERA SER REQUISITADA EN SU TOTALIDAD POR EL RESPONSABLE DEL PROYECTO Y 2)  SERA TRAMITADA ANTE LA SECRETARIA ADMINISTRATIVA DEL INAH, SOLAMENTE SI TIENE LAS FIRMAS AUTOGRAFAS DE LOS INTERESADOS.</label>
	</div>
	
    <?php
        $i = 0;
       for($i=0;$i<8;$i++)
    	echo("<p><br/><br/></p>");
    ?>
	
    <div id="continuacion" >
        <br/><br/>
         <table class="TablaTarjeta" align="center" width="100%" border="1" cellspacing="0" cellpadding="0">
            <tr>
                <td class="TarjetaDatos" colspan="16">
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                    &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; CALENDARIO FINANCIERO (PESOS)
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; <label style="font-size: 6pt">(ANEXAR HOJAS SI SE REQUIEREN)</label>
                </td>
            </tr> 
			 
            <tr>
               <td class="TarjetaDatos"  align="center" rowspan="2" style="font-size: 6.5pt" >PARTIDA<br/>PRESUPUESTAL</td>
               <td class="TarjetaDatos"  align="center" rowspan="2" style="font-size: 6.5pt" >ENERO</td>
               <td class="TarjetaDatos"  align="center" rowspan="2" style="font-size: 6.5pt" >FEBRERO</td>
               <td class="TarjetaDatos"  align="center" rowspan="2" style="font-size: 6.5pt" >MARZO</td>
               <td class="TarjetaDatos"  align="center" rowspan="2" style="font-size: 6.5pt" >ABRIL</td>
               <td class="TarjetaDatos"  align="center" rowspan="2" style="font-size: 6.5pt" >MAYO</td>
               <td class="TarjetaDatos"  align="center" rowspan="2" style="font-size: 6.5pt" >JUNIO</td>
               <td class="TarjetaDatos"  align="center" rowspan="2" style="font-size: 6.5pt" >JULIO</td>
               <td class="TarjetaDatos"  align="center" rowspan="2" style="font-size: 6.5pt" >AGOSTO</td>
               <td class="TarjetaDatos"  align="center" rowspan="2" style="font-size: 6.5pt" >SEPT</td>
               <td class="TarjetaDatos"  align="center" rowspan="2" style="font-size: 6.5pt" >OCTUBRE</td>
               <td class="TarjetaDatos"  align="center" rowspan="2" style="font-size: 6.5pt" >NOVIEM</td>
               <td class="TarjetaDatos"  align="center" rowspan="2" style="font-size: 6.5pt" >DICIEM</td>
               <td class="TarjetaDatos"  align="center" colspan="2" style="font-size: 6.5pt" >TOTAL POR</td>
               <td class="TarjetaDatos"  align="center" rowspan="2" style="font-size: 6.5pt" >GRAN <br/> TOTAL</td>
            </tr>
			 
            <tr>
               <td class="TarjetaDatos"  align="center" style="font-size: 6pt" >PARTIDA</td>
                <td class="TarjetaDatos"  align="center" style="font-size: 6pt" >CAPITULO</td>
            </tr>
			 
            <?php
                $daoCalendario = new TarjetaCalendarioDaoJdbc();
                $listaCalendario = $daoCalendario->obtieneListado($folio);
                $Calendario = new TarjetaCalendario();

                $GranTotal = 0;
                $enero = 0;
                $feb = 0;
                $mar = 0;
                $abr = 0;
                $may = 0; 
                $jun = 0;
                $jul = 0;
                $ago = 0;
                $sep = 0;
                $oct = 0;
                $nov = 0;
                $dic = 0;
                $Total = 0;
                foreach($listaCalendario as $Calendario){
                    $GranTotal = $Calendario->getEnero() + $Calendario->getFebrero() + $Calendario->getMarzo() + $Calendario->getAbril() + $Calendario->getMayo() + $Calendario->getJunio() + $Calendario->getJulio() + $Calendario->getAgosto() + $Calendario->getSeptiembre() + $Calendario->getOctubre() + $Calendario->getNoviembre()  + $Calendario->getDiciembre();
                    $enero = $enero + $Calendario->getEnero();
                    $feb = $feb + $Calendario->getFebrero();
                    $mar = $mar + $Calendario->getMarzo();
                    $abr = $abr + $Calendario->getAbril();
                    $may = $may + $Calendario->getMayo();
                    $jun = $jun + $Calendario->getJunio();
                    $jul = $jul + $Calendario->getJulio();
                    $ago = $ago + $Calendario->getAgosto();
                    $sep = $sep + $Calendario->getSeptiembre();
                    $oct = $oct + $Calendario->getOctubre();
                    $nov = $nov + $Calendario->getNoviembre();
                    $dic = $dic + $Calendario->getDiciembre();
             ?>
	  		      	 
            <tr>
                <td class="TarjetaFecha"  align="center" ><?php echo($Calendario->getNum_Partida());?></td>
                <td class="TarjetaFecha"  align="center" ><?php echo("$".number_format( $Calendario->getEnero(),2) );?></td>
                <td class="TarjetaFecha"  align="center" ><?php echo("$".number_format( $Calendario->getFebrero(),2));?></td>
                <td class="TarjetaFecha"  align="center" ><?php echo("$".number_format( $Calendario->getMarzo(),2));?></td>
                <td class="TarjetaFecha"  align="center" ><?php echo("$".number_format( $Calendario->getAbril(),2));?></td>
                <td class="TarjetaFecha"  align="center" ><?php echo("$".number_format( $Calendario->getMayo(),2));?></td>
                <td class="TarjetaFecha"  align="center" ><?php echo("$".number_format( $Calendario->getJunio(),2));?></td>
                <td class="TarjetaFecha"  align="center" ><?php echo("$".number_format(  $Calendario->getJulio(),2));?></td>
                <td class="TarjetaFecha"  align="center" ><?php echo("$".number_format( $Calendario->getAgosto(),2));?></td>
                <td class="TarjetaFecha"  align="center" ><?php echo("$".number_format( $Calendario->getSeptiembre(),2));?></td>
                <td class="TarjetaFecha"  align="center" ><?php echo("$".number_format( $Calendario->getOctubre(),2));?></td>
                <td class="TarjetaFecha"  align="center" ><?php echo("$".number_format( $Calendario->getNoviembre(),2));?></td>
                <td class="TarjetaFecha"  align="center" ><?php echo("$".number_format( $Calendario->getDiciembre(),2));?></td>
                <td class="TarjetaFecha"  align="center" ><br/></td>
                <td class="TarjetaFecha"  align="center" ><br/></td>
                <td class="TarjetaFecha"  align="center" ><?php echo("$".number_format($GranTotal,2));?></td> 
            </tr>
			 
            <?php $GranTotal = 0; }?>
            <tr>
               <td class="TarjetaDatos"  align="center"  style="font-size: 6.5pt" >TOTAL</td>
               <td class="TarjetaDatos"  align="center"  style="font-size: 6.5pt" ><?php echo("$".number_format($enero,2));?></td>
               <td class="TarjetaDatos"  align="center"  style="font-size: 6.5pt" ><?php echo("$".number_format($feb,2));?></td>
               <td class="TarjetaDatos"  align="center"  style="font-size: 6.5pt" ><?php echo("$".number_format($mar,2));?></td>
               <td class="TarjetaDatos"  align="center"  style="font-size: 6.5pt" ><?php echo("$".number_format($abr,2));?></td>
               <td class="TarjetaDatos"  align="center"  style="font-size: 6.5pt" ><?php echo("$".number_format($may,2));?></td>
               <td class="TarjetaDatos"  align="center"  style="font-size: 6.5pt" ><?php echo("$".number_format($jun,2));?></td>
               <td class="TarjetaDatos"  align="center"  style="font-size: 6.5pt" ><?php echo("$".number_format($jul,2));?></td>
               <td class="TarjetaDatos"  align="center"  style="font-size: 6.5pt" ><?php echo("$".number_format($ago,2));?></td>
               <td class="TarjetaDatos"  align="center"  style="font-size: 6.5pt" ><?php echo("$".number_format($sep,2));?></td>
               <td class="TarjetaDatos"  align="center"  style="font-size: 6.5pt" ><?php echo("$".number_format($oct,2));?></td>
               <td class="TarjetaDatos"  align="center"  style="font-size: 6.5pt" ><?php echo("$".number_format($nov,2));?></td>
               <td class="TarjetaDatos"  align="center"  style="font-size: 6.5pt" ><?php echo("$".number_format($dic,2));?></td>
               <td class="TarjetaDatos"  align="center"  style="font-size: 6.5pt" ></td>
               <td class="TarjetaDatos"  align="center"  style="font-size: 6.5pt" ></td>
               <?php $Total = $enero + $feb + $mar + $abr + $may + $jun + $jul + $ago + $sep + $oct + $nov + $dic;  ?>
               <td class="TarjetaDatos"  align="center"  style="font-size: 6.5pt" ><?php echo("$".number_format($Total,2));?></td>

            </tr>
			 
            <tr>
                <td class="TarjetaDatos" align="center" colspan="16">JUSTIFICACIÓN PRESUPUESTARIA POR CAPÍTULO</td>
            </tr> 
            <tr>
                <td class="TarjetaFecha" align="center" colspan="16" >&nbsp; &nbsp;<br/><br/>
                 <?php if($elemento!=null && $elemento->getJustificacion()!=null) echo($elemento->getJustificacion());?>
                 <br/><br/> </td>
           </tr>

           <tr>
                   <td class="TarjetaDatos" align="center" colspan="16">PERSONAL QUE PARTICIPA DIRECTAMENTE EN EL PROYECTO    </td>
           </tr>
			
            <tr>
                <td class="TarjetaDatos"  align="center"  style="font-size: 6.5pt" >No.</td>
                <td class="TarjetaDatos"  align="center" colspan="6" style="font-size: 6.5pt" >NOMBRE</td>
                <td class="TarjetaDatos"  align="center" colspan="3" style="font-size: 6.5pt" >GRADO ACADEMICO</td>
                <td class="TarjetaDatos"  align="center" colspan="2" style="font-size: 6.5pt" >PUESTO Y <br/> CATEGORÍA</td>
                <td class="TarjetaDatos"  align="center" style="font-size: 6.5pt" >POR <br/>EL <br/>  INAH</td>
                <td class="TarjetaDatos"  align="center" style="font-size: 6.5pt" >POR <br/>EL <br/> PROY</td>
                <td class="TarjetaDatos"  align="center" style="font-size: 6.5pt" >EXTERNO</td>
                <td class="TarjetaDatos"  align="center" style="font-size: 6.5pt" >INSTITUCIONES EXTERNOS Y/O <br/> DEPENDENCIAS INTERNAS QUE <br/> CONLABORAN EN EL PROYECTO</td>
            </tr>
			
            <?php 
	  	$daoPersonal = new TarjetaPersonalDaoJdbc();
	  	$listaPersonal = $daoPersonal->obtieneListado($folio);
	  	$Personal = new TarjetaPersonal();

                $PersonalINAH = 0;
                $PersonalProy = 0;
                $PersonalExterno = 0;
                foreach($listaPersonal as $Personal){
	     ?>
            <tr>
                <td class="TarjetaFecha"  align="center" ><?php echo($Personal->getNum_Personal());?></td>
                <td class="TarjetaFecha" colspan="6" align="center" ><?php echo($Personal->getNombre());?></td>
                <td class="TarjetaFecha" colspan="3" align="center" ><?php echo($Personal->getGrado());?></td>
                <td class="TarjetaFecha" colspan="2" align="center" ><?php echo($Personal->getPuesto());?></td>
                <?php if($Personal->getAsignado() == "INAH"){  $PersonalINAH++; ?> <td class="TarjetaFecha"  align="center" >X</td>
                <?php }else{ ?> <td class="TarjetaFecha"  align="center" ></td> <?php }?>
                <?php if($Personal->getAsignado() == "Proyecto"){  $PersonalProy++; ?> <td class="TarjetaFecha"  align="center" >X</td>
                <?php }else{ ?> <td class="TarjetaFecha"  align="center" ></td> <?php }?>
                <?php if($Personal->getAsignado() == "Externo"){  $PersonalExterno++; ?> <td class="TarjetaFecha"  align="center" >X</td>
                <?php }else{ ?> <td class="TarjetaFecha"  align="center" ></td> <?php }?>
                <td class="TarjetaFecha"  align="center" ><?php if($Personal->getInstituciones()!=null) echo($Personal->getInstituciones());?></td>
             </tr>

             <?php }?>
			 
            <tr>
                <td class="TarjetaDatos"  colspan="12" align="right" colspan="16">TOTAL &nbsp; &nbsp;</td>
                <td class="TarjetaFecha"  align="center" ><?php echo($PersonalINAH);?></td>
                <td class="TarjetaFecha"  align="center" ><?php echo($PersonalProy);?></td>
                <td class="TarjetaFecha"  align="center" ><?php echo($PersonalExterno);?></td>
                <td class="TarjetaDatos"  align="center" ></td>
            </tr> 
			 
        </table>
    </div>
	
    <div id="elaboracion" >
       <br/><br/>
        <table class="TablaTarjeta" align="center" width="100%" border="1" cellspacing="0" cellpadding="0">
            <tr>
                <td class="TarjetaDatos" width="28%" align="center" >ELABORA</td>
                <td class="TarjetaDatos" align="center" >PROPONE</td>
                <td class="TarjetaDatos" align="center" colspan="2">AUTORIZAN</td>
            </tr>

            <tr>
                <td class="TarjetaDatos" align="center" >RESPONSABLE DEL PROYECTO</td>
                <td class="TarjetaDatos" align="center" >TITULAR DE LA UNIDAD <br/> ADMINISTRATIVA</td>
                <td class="TarjetaDatos" align="center" >SECRETARIO TÉCNICO</td>
                <td class="TarjetaDatos" align="center" >SECRETARIO <br/> ADMINISTRATIVO</td>

            </tr>

            <tr>
                <td class="TarjetaFecha"  align="center" ><br/><br/><br/><br/><br/><br/>___________________________<br/><?php if($elemento!=null && $elemento->getElabora()!= null) echo($elemento->getElabora());?></td>
                <td class="TarjetaFecha"  align="center" ><br/><br/><br/><br/><br/><br/>___________________________<br/><?php if($elemento!=null && $elemento->getPropone()!= null) echo($elemento->getPropone());?></td>
                <td class="TarjetaFecha"  align="center" ><br/><br/><br/><br/><br/><br/>___________________________<br/><?php if($elemento!=null && $elemento->getAutoriza_Tecnico()!= null) echo($elemento->getAutoriza_Tecnico());?></td>
                <td class="TarjetaFecha"  align="center" ><br/><br/><br/><br/><br/><br/>___________________________<br/><?php if($elemento!=null && $elemento->getAutoriza_Adminstrativo()!= null) echo($elemento->getAutoriza_Adminstrativo());?></td>

            </tr>

          <tr>
              <td class="TarjetaDatos" align="center" >NOMBRE Y FIRMA</td>
              <td class="TarjetaDatos" align="center" >NOMBRE Y FIRMA</td>
              <td class="TarjetaDatos" align="center" >NOMBRE Y FIRMA</td>
              <td class="TarjetaDatos" align="center" >NOMBRE Y FIRMA</td>
          </tr>

        </table>
    </div>
</body>
</html>