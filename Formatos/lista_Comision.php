<?php
    session_start();
    include_once("../src/mx/com/virreinato/dao/ComisionDaoJdbc.class.php");
    header('Content-Type: text/html; charset=UTF-8'); 
    if(isset($_SESSION['id']) && $_SESSION['id']!=NULL){
	include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC >
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link href="../css/calendario.css" type="text/css" rel="stylesheet">
<script src="../js/calendar.js" type="text/javascript"></script>
<script src="../js/calendar-es.js" type="text/javascript"></script>
<script src="../js/calendar-setup.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="../css/lista.css">
        <script type="text/javascript" src="../media/js/complete.js"></script>
        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
        <script type="text/javascript" src="../js/lista.js"></script>
		
  <input type="hidden" id="columnas_visibles" name="columnas_visibles" value="5"/>
    <input type="hidden" id="total_columnas" name="total_columnas" value="10"/>



<title>Comisión</title>
</head>
<body>
    <?php
        $respuesta = NULL;
        if(isset($_REQUEST['respuesta'])){ $respuesta = $_REQUEST['respuesta']; }
        $dia = (String)(date('d'));
        $mes = (String)(date('m'));
        $mesInicio= "01";
         $diaInicio="01";
         
		 
		 /*if(date('n')>1){
            $mesInicio= (String)( date('n') - 1 ); if(strlen($mesInicio) == 1){ $mesInicio = "0".$mesInicio; }

         }
         else{
            $diaInicio="01";
         } */
    ?>
    
    <div class="contenido">

    <br/>
    <p class="titulo_cat1">Formatos > Comisiones</p>
	 
	 <!--<?php
       // if (isset($_GET['recargado']) && $_GET['recargado'] == "1") {
         //   echo '<input type="hidden" id="nombre_form" name="nombre_form" value=""/>';
        //} else {
          //  echo '<input type="hidden" id="nombre_form" name="nombre_form" value="frmFiltroComprobacion"/>';
        //}
        ?>  -->
	
<h2 class="titulo_cat2"> </h2>

<p class="titulo_cat1">
<blockquote>
    <blockquote>
        <blockquote> 
            <form name="frmFiltroComprobacion" id="frmFiltroComprobacion" method="POST" action="../src/mx/com/virreinato/web/WebComission.php" >
           &nbsp; &nbsp; Fecha de Inicio: 
           <input type="text" size="10" name="inicioFecha" id="inicioFecha" maxlength="20" value=" <?php  if(  isset($_REQUEST['inicio']) ){ echo $_REQUEST['inicio']; } ?>"  >
            Fecha de Fin: <input type="text" size="10" name="finFecha" id="finFecha" maxlength="20" value=" <?php  if(  isset($_REQUEST['fin'])){ echo $_REQUEST['fin']; }  ?>"  >
            <?php 
                    $inicio =  $diaInicio."-".$mesInicio."-".date('Y');
                    $fin =   $dia."-".$mes."-".date('Y');
                    $periodo = "";
            ?>

                &nbsp; <input name="filtro" style="cursor:pointer" type="submit" value="Filtrar"  class='btn' />
            </form>
        </blockquote>
    </blockquote>	   
</blockquote>	
<div align="right"><a href='Agregar_Comision.php' class='liga_btn'> Agregar Nueva Comisi&oacute;n</a></div>
<br></br>

<?php 
    if($respuesta!=null){ 
        echo "<div align='center' class='msj'>".$respuesta."</div>";        
    }
	
    if( isset($_GET['inicio']) ){
        $inicio = ( String ) $_GET['inicio'];
        $fin = (String) $_GET['fin'];	
    }
?>
<table  border="0" cellspacing="0" cellpadding="5" class='dataTable' align='center' >
<thead>

<tr >
                <th align="center">Nombre</th>
                <th align="center">Fecha del Oficio</th>
                <th align="center">No. de Oficio</th>
                <th align="center">Total de Viáticos</th>
                <th align="center">Saldo pendiente</th>
                <th width="2%" align="center"></th>
                <th width="2%" align="center"></th>
                <th width="2%" align="center"></th>
                <th width="2%" align="center"></th>
                <th width="2%" align="center"></th>			
        </tr>
		
		</thead>
		<tfoot>
		<tr >
                <th align="center"></th>
                <th align="center"></th>
                <th align="center"></th>
                <th align="center"></th>
                <th align="center"></th>
                <th width="2%" align="center"></th>
                <th width="2%" align="center"></th>
                <th width="2%" align="center"></th>
                <th width="2%" align="center"></th>
                <th width="2%" align="center"></th>			
        </tr>
		
		
		</tfoot>
		
		<tbody>
        <?php  
                $dao= new ComisionDaoJdbc();
                $lista= $dao->obtieneListado($inicio,$fin);
                $elemento = new Comision();
                //$it = lista.iterator();

                foreach ($lista as $elemento) {                                    
                    ?>
                    <tr class="SizeText">
                        <td align="center"><?php 
                        $empleado = $elemento->getEmpleado();
                        echo $empleado->getNombre()." ".$empleado->getApPaterno()." ".$empleado->getApMaterno();?></td>
                        <td align="center"><?php echo $elemento->getFechaOficio();?></td>
                        <td align="center"><?php echo $elemento->getNumeroOficio();?></td>
                        <td align="center"><?php echo $elemento->getMontoTotal();?></td>
                        <td align="center"><?php if($elemento->getMontoSinDoctos()!=0.0){ 
                            echo $elemento->getMontoSinDoctos();
                        }
                        ?></td>

                        <td><a href='Agregar_Comision.php?id=<?php echo $elemento->getId(); ?>' class='liga_cat'><acronym title="Editar"><img src="../img/Pencil3.png" width="16" height="16" alt="Editar" style="border:0;" /></acronym> </a></td>
                        <td><a href='../src/mx/com/virreinato/web/WebComission.php?id=<?php echo($elemento->getId());?>' class='liga_cat' onclick='return confirm("¿Esta seguro que desea eliminar este registro?")'><acronym title="Borrar"><img src="../img/DeleteRed.png" width="16" height="16" alt="Borrar" style="border:0;" /></acronym></a></td>
                        <td><a style="cursor:pointer" class='liga_cat' onclick="Imprimir('../Formatos/InformeComision.php','<?php echo $elemento->getId(); ?>')"><acronym title="Imprimir">Informe de Comisión</acronym></a></td>
                        <td><a style="cursor:pointer" class='liga_cat' onclick="Imprimir('../Formatos/LiquidacionViaticos.php','<?php echo $elemento->getId(); ?>')"><acronym title="Imprimir">Liquidación de Viáticos</acronym></a></td>
                        <td><a style="cursor:pointer" class='liga_cat' onclick="Imprimir('../Formatos/ViaticosNoComprobables.php','<?php echo $elemento->getId(); ?>')"><acronym title="Imprimir">Viáticos No Comprobables</acronym></a></td>
                    </tr>
        <?php } ?>
		</tbody>
</table>
<br>

</body>
<script language="JavaScript">
   Calendar.setup({ inputField :"finFecha", ifFormat : "%d-%m-%Y", button:"finFecha" });
    
   Calendar.setup({ inputField : "inicioFecha", ifFormat : "%d-%m-%Y", button:"inicioFecha" });
   
   var ini = '<?php echo $inicio ?>';
   var fin = '<?php echo $fin ?>';
   $("#inicioFecha").val(ini);
   $("#finFecha").val(fin);
   
   function Imprimir(pagina,folio) 
 	{
		//alert(pagina)
		var dir = pagina + "?folio=" + folio;
		var left = Math.floor((screen.width - 980) / 2);
		var opciones = 'titlebar=0, menubar=0, toolbar=0, location=0, directories=0, status=0, scrollbars=0, resizable=0, width=980, height=750,top=50,left='
				+ left + '';
		window.open(dir, "", opciones);
	}
</script>
</html>
<?php }else{
	    header("Location: ../index.php"); 
   }
?>
    

