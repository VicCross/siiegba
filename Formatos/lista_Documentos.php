<?php

session_start();
    include_once("../src/mx/com/virreinato/dao/DocumentacionDaoJdbc.class.php");
    include_once("../src/mx/com/virreinato/beans/Documentacion.class.php");
	include_once("../src/mx/com/virreinato/dao/AreaDaoJdbc.class.php");
    include_once("../src/mx/com/virreinato/beans/Area.class.php");
	include_once("../src/mx/com/virreinato/dao/LineaAccionDaoJdbc.class.php");
    include_once("../src/mx/com/virreinato/beans/LineaAccion.class.php");
	include_once("../src/mx/com/virreinato/dao/EmpleadoDaoJdbc.class.php");
    include_once("../src/mx/com/virreinato/beans/Empleado.class.php");
    header('Content-Type: text/html; charset=UTF-8'); 
    if(isset($_SESSION['id']) && $_SESSION['id']!=NULL){
	include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
        $parametro = new ParametroDaoJdbc();
        $parametro = $parametro->obtieneElemento(5);
?>

<!DOCTYPE html PUBLIC >

<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link href="../css/calendario.css" type="text/css" rel="stylesheet">
<script src="../js/calendar.js" type="text/javascript"></script>
<script src="../js/calendar-es.js" type="text/javascript"></script>
<script src="../js/calendar-setup.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="../css/lista.css">
        <script type="text/javascript" src="../media/js/complete.js"></script>
        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
        <script type="text/javascript" src="../js/lista.js"></script>
		
	   <input type="hidden" id="columnas_visibles" name="columnas_visibles" value="3"/>
    <input type="hidden" id="total_columnas" name="total_columnas" value="8"/>




<title>Comisión</title>
</head>
<body>
    <?php
        $respuesta = NULL;
        if(isset($_REQUEST['respuesta'])){ $respuesta = $_REQUEST['respuesta']; }
        $dia = (String)(date('d'));
        $mes = (String)(date('m'));
        $mesInicio="01";
         $diaInicio="01";
         
		 /*if(date('n')>1){
            $mesInicio= (String)( date('n') - 1 ); if(strlen($mesInicio) == 1){ $mesInicio = "0".$mesInicio; }

         }
         else{
            $diaInicio="01";
         }*/
        //echo $dia . " - ".$mes." - ".$mesInicio; 
        $meses = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    ?>
    <div class="contenido">

    <br/>
    <p class="titulo_cat1">Formatos > Documentación</p>
    <h2 class="titulo_cat2"></h2>

<p class="titulo_cat1">
<blockquote>
    <blockquote>
        <blockquote>
		
		  <?php
       // if (isset($_GET['recargado']) && $_GET['recargado'] == "1") {
         //   echo '<input type="hidden" id="nombre_form" name="nombre_form" value=""/>';
        //} else {
          //  echo '<input type="hidden" id="nombre_form" name="nombre_form" value="frmFiltroComprobacion"/>';
        //}
        ?>
		
          <form name="frmFiltroComprobacion" id="frmFiltroComprobacion" method="GET" action="../src/mx/com/virreinato/web/WebDocumentacion.php" >
           &nbsp; &nbsp; Fecha de Inicio: <input type="text" size="10" name="inicioFecha" id="inicioFecha" maxlength="20" value=" <?php  if(isset($_GET["inicio"]) && $_GET["inicio"] !=null){ echo($_GET["inicio"]); } ?>"  >
                Fecha de Fin: <input type="text" size="10" name="finFecha" id="finFecha" maxlength="20" value=" <?php  if(isset($_GET["fin"]) && $_GET["fin"] !=null ){ echo($_GET["fin"]); }  ?>"  >
                <?php 
                        $inicio =  $diaInicio."-".$mesInicio."-".date('Y');
                        $fin =   $dia."-".$mes."-".date('Y');
                        $periodo = "";
                ?>
                &nbsp; <input name="filtro" style="cursor:pointer" type="submit" value="Filtrar"  class='btn' />
	   </form>
	</blockquote>
    </blockquote>	   
</blockquote>	
<div align="right"><a href='Agregar_Documento.php' class='liga_btn'> Agregar Nuevo Documento</a></div>
<br></br>

<?php 
    if($respuesta!=null){ 
        echo "<div align='center' class='msj'>".$respuesta."</div>";        
    }
	
    if( isset($_GET['inicio']) ){
        $inicio = ( String ) $_GET['inicio'];
        $fin = (String) $_GET['fin'];	
    }
?>
<table  border="0" cellspacing="0" cellpadding="5" class='dataTable' align='center' >
    <thead>
	<tr >
        <th width="14%" align="center">Proyecto</th>
        <th width="14%" align="center">Área</th>
        <th width="14%" align="center">Empleado que subió el archivo</th>
        <th width="14%" align="center">Fecha</th>
        <th width="14%" align="center">Número de Versión</th>
        <th width="14%" align="center">Archivo</th>
        <th width="14%" align="center">Observaciones</th>
        <th width="2%" align="center"></th>
        <th width="2%" align="center">
    </tr>
	</thead>
	<tfoot>
	<tr >
        <th width="14%" align="center"></th>
        <th width="14%" align="center"></th>
        <th width="14%" align="center"></th>
        <th width="14%" align="center"></th>
        <th width="14%" align="center"></th>
        <th width="14%" align="center"></th>
        <th width="14%" align="center"></th>
        <th width="2%" align="center"></th>
        <th width="2%" align="center">
    </tr>
	</tfoot>
	<tbody>
	
	
    <?php  
                $dao= new DocumentacionDaoJdbc();
                $lista= $dao->obtieneListado($inicio,$fin);
                $elemento = new Documentacion();
				$daoA = new AreaDaoJdbc();
				$daoE = new EmpleadoDaoJdbc();
				$daoLA = new LineaAccionDaoJdbc();
                foreach ($lista as $elemento) {                                    
                        ?>
    <tr class="SizeText">
        <td align="center"><?php if(is_null($elemento->getIdProyecto())) echo("-");else {$proy = new LineaAccion(); $proy = $daoLA->obtieneElemento($elemento->getIdProyecto());
		echo($proy->getLineaAccion());}?></td>
        <td align="center"><?php if(is_null($elemento->getIdArea())) echo ("-"); else{$area = new Area(); $area = $daoA->obtieneArea($elemento->getIdArea());
		echo($area->getDescripcion());}?></td>
        <td align="center"><?php  $empleado = new Empleado(); $empleado = $daoE->obtieneElemento($elemento->getIdEmpleado());		
		echo($empleado->getNombre()." ".$empleado->getApPaterno()." ".$empleado->getApMaterno());?></td>
        <td align="center"><?php echo($elemento->getFecha());?></td>
        <td align="center"><?php echo($elemento->getVersion());?></td>
        <td align="center"><a href='<?php echo($elemento->getArchivo());?>'>Ver Archivo</a></td>
        <td align="center"><?php if(is_null($elemento->getObservaciones())) echo ("-"); else echo($elemento->getObservaciones());?></td>
        <td><a href='Agregar_Documento2.php?id=<?php echo($elemento->getId());?>' class='liga_cat'><acronym title="Editar"><img src="../img/Pencil3.png" width="16" height="16" alt="Editar" style="border:0;" /> </acronym> </a> </td>
        <td><a href='../src/mx/com/virreinato/web/WebDocumentacion.php?id=<?php echo($elemento->getId());?>' class='liga_cat' onclick='return confirm("¿Esta seguro que desea eliminar este registro?")'><acronym title="Borrar"><img src="../img/DeleteRed.png" width="16" height="16" alt="Borrar" style="border:0;" /></acronym></a></td>
        <!--td><a style="cursor:pointer" class='liga_cat' onclick="Imprimir('../Formatos/FormatoPasajes.php','<!?php echo $elemento->getId(); ?>')"><acronym title="Imprimir"><img src="../img/printer.png" width="18" height="18" alt="Imprimir" style="border:0;" /></acronym></a>-->
    </tr>
    <?php } ?>
	</tbody>
    </table>
	<br>
	
</div>
</body>
<script language="JavaScript">
   Calendar.setup({ inputField :"finFecha", ifFormat : "%d-%m-%Y", button:"finFecha" });
    
   Calendar.setup({ inputField : "inicioFecha", ifFormat : "%d-%m-%Y", button:"inicioFecha" });
   
   var ini = '<?php echo $inicio ?>';
   var fin = '<?php echo $fin ?>';
   $("#inicioFecha").val(ini);
   $("#finFecha").val(fin);
   
</script>	
</html>
<?php }else{
	    header("Location: ../index.php?err=2"); 
   }
?>
