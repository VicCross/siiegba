<?php

session_start();
include_once("../src/mx/com/virreinato/dao/CatChequeDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatCheque.class.php");
include_once("../src/mx/com/virreinato/dao/ComprobacionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Comprobacion.class.php");
include_once("../src/mx/com/virreinato/dao/DetComprobacionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/DetComprobacion.class.php");
include_once("../src/mx/com/virreinato/dao/PartidaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Partida.class.php");
include_once("../src/mx/com/virreinato/dao/ProveedorDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Proveedor.class.php");
include_once("../src/mx/com/virreinato/dao/CatProyectoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatProyecto.class.php");
include_once("../src/mx/com/virreinato/dao/AreaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Area.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="/css/style_Impresion.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>	

        <title>RELACION DE COMPROBANTES</title>
    </head>
    <body>
        <font size=1> 
            <table border="1" align="center" width="100%" cellspacing="0">
                <caption align="top"> <br> RELACION DE COMPROBANTES <br><br /> </caption>				
                <tr>
                    <th> COMPROBACION </th>
                    <th> CHEQUE </th>
                    <th width="12%"> AREA </th>
                    <th> FOLIO </th>
                    <th> FECHA </th>
                    <th width="15%"> PROVEEDOR </th>
                    <th width="15%"> DIRECCION </th>
                    <th> TELEFONO </th>
                    <th width="15%"> CONCEPTO </th>
                    <th> PARTIDA </th>
                    <th> IMPORTE DEL CHEQUE </th>
                    <th> IMPORTE DEL COMPROBANTE</th>
                    <th> DIFERENCIA </th>
                </tr>
                <?php

                    $daoComprobacion= new ComprobacionDaoJdbc();
                    $comprobacion = $daoComprobacion->obtieneElemento($_GET['folio']);

                    $daoCheque= new CatChequeDaoJdbc();
                    $cheque= $daoCheque->obtieneElemento((String)$comprobacion->getIdCheque());

                    $daoPartida= new PartidaDaoJdbc();
                    $partida= new Partida();

                    $daoProveedor= new ProveedorDaoJdbc();
                    $proveedor= new Proveedor();

                    $daoProyecto= new CatProyectoDaoJdbc();
                    $proyecto= $daoProyecto->obtieneElemento((String)$comprobacion->getIdProyecto());

                    $daoDetComprobacion= new DetComprobacionDaoJdbc();
                    $listComprobacion= $daoDetComprobacion->obtieneListado((String)$comprobacion->getIdComprobacion());
                    $detalleComprobacion= new DetComprobacion();

                    $first=true;
                    $diferencia= (double)$comprobacion->getMontoComprobacion();

                    foreach($listComprobacion as $detalleComprobacion){
                        $diferencia-=$detalleComprobacion->getMonto();
                    }

                    foreach($listComprobacion as $detalleComprobacion)
                    { 
                        $partida= $daoPartida->obtieneElemento((String)$detalleComprobacion->getIdPartida());
                        $proveedor= $daoProveedor->obtieneElemento((String)$detalleComprobacion->getIdProveedor());
            ?>
            <tr>	
                <td align="center"> <?php echo$comprobacion->getFolio(); ?> </td>
                <td align="center"> <?php echo$cheque->getFolio() ?></td>
                <td> <?php
                if($comprobacion->getDestino() == "PR"){
                        echo($proyecto->getDescripcion());
                }else{
                        $adao=new AreaDaoJdbc();
                        $area=$adao->obtieneArea((String)($cheque->getArea()));
                        echo($area->getDescripcion());								
                }

                ?></td>
                <td align="center"> <?php echo$detalleComprobacion->getFolio() ?> </td>
                <td align="center"> <?php echo(date("d-m-Y",strtotime($detalleComprobacion->getFecha()))); ?> </td>
                <td> <?php echo$proveedor->getProveedor(); ?> </td>
                <td> <?php echo$proveedor->getDomicilio(); ?> </td>
                <td align="center"> <?php if($proveedor->getTelefono() != null) echo($proveedor->getTelefono()); ?> </td>
                <td> <?php echo$detalleComprobacion->getDescripcion(); ?> </td>
                <td align="center"> <?php echo$partida->getPartida(); ?> </td>
                <?php 
                    if($first)
                    {
                    ?>			
                <td rowspan="<?php echo(count($listComprobacion)); ?>" align="center"> <?php echo("$".number_format($cheque->getMonto(),2)); ?></td>
                <?php 
                    }
                ?>			
                <td align="center"> <?php echo("$".number_format($detalleComprobacion->getMonto(),2)); ?></td>
                <?php 
                    if($first)
                    {
                ?>
                <td rowspan="<?php echo(count($listComprobacion)); ?>" align="center"> <?php echo("$".number_format($diferencia)) ?> </td>
                <?php 
                    $first=false;
                    } 
                ?>
                </tr>
                <?php 
                    } 
                ?>
                <tr>	
                    <td align="center" height="10"></td>
                    <td align="center"></td>
                    <td></td>
                    <td align="center"></td>
                    <td align="center"></td>
                    <td></td>
                    <td></td>
                    <td align="center"></td>
                    <td></td>
                    <td align="center"></td>
                    <td align="center"><?php echo("$".number_format($cheque->getMonto(),2)); ?></td>
                    <td align="center"><?php echo("$".number_format((double)($cheque->getMonto())-$diferencia,2)); ?></td>
                    <td align="center"><?php echo("$".number_format($diferencia,2)); ?></td>
                </tr>		
				
            </table>
        </font>
    </body>
</html>
