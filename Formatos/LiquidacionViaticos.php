<?php
include_once("../src/mx/com/virreinato/dao/ComisionDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/dao/CentroCostosDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/dao/DirectivoDaoJdbc.class.php");
session_start();
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: ../index.php");
}
header('Content-Type: text/html; charset=UTF-8');
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="/css/style_Impresion.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>

        <title>Liquidación de Viáticos</title>
    </head>
    <body>
        <?php
        $dao = new ComisionDaoJdbc();
        $c = $dao->obtieneElemento($_GET["folio"]);
        $ccos = new CentroCostos();
        $ccosDao = new CentroCostosDaoJdbc();

        $ccos = $ccosDao->obtieneElemento($c->getIdCCosto());

        $dirDao = new DirectivoDaoJdbc();
        $sub = $dirDao->obtieneElementoCargo("2");
        $dir = $dirDao->obtieneElementoCargo("1");

        $meses = array("", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        ?>

        <table align="center" width="90%" border="0" style='text-transform:uppercase;font-family:arial;word-spacing:2px; letter-spacing:1px;font-size:11px'>
            <tr>
                <td align="left" colspan='2'> 
                    <img src="../img/Conaculta_INAH.png" width='180' height='35' alt="Logo" style="border:0;" />
                    <div align="center" style='float:right;text-align:center;width:70%;' > <strong> INSTITUTO NACIONAL DE ANTROPOLOGÍA E HISTORIA <br> SECRETARÍA ADMINISTRATIVA  <br> FORMATO DE LIQUIDACIÓN DE VIÁTICOS </strong><br></div>
                </td>

            </tr>
        </table>	
        <br>
        <br>	
        <table align="center" width="90%" border="1" style='text-transform:uppercase;font-family:arial;word-spacing:2px; letter-spacing:1px;font-size:11px; border-style: double;border-color: black;border-spacing:0px;'>	
            <tr>
                <td align="center" colspan='2'>CENTRO DE TRABAJO Y/O UNIDAD ADMINISTRATIVA</td>
            </tr>

            <tr>
                <td align="center">CLAVE</td>
                <td align="center" width='70%'>DENOMINACIÓN</td>
            </tr>

            <tr>
                <td align="center"><?php echo ($ccos->getClave()); ?></td>
                <td align="center" ><?php echo ($ccos->getDescripcion()); ?></td>
            </tr>
        </table>	

        <br>
        <br>
        <table align="center" width="90%" border="0" cellpadding='4' cellspacing='0' style='text-transform:uppercase;font-family:arial;word-spacing:2px; letter-spacing:1px;font-size:11px;border:2px solid black;'>	
            <tr >
                <td align="center" colspan='4' style='border-bottom:2px solid black;'> DATOS DEL COMISIONADO </td>
            <tr>	

            <tr>		

                <td colspan='4'> 
                    <br> NOMBRE DEL COMISIONADO: <span style='border-bottom:1px solid black;width:620px;display:inline-block;'>
                        &nbsp;&nbsp;<?php $empleado = $c->getEmpleado(); echo ($empleado->getNombre()  ." " . $empleado->getApPaterno() . " " . $empleado->getApMaterno()); ?> </span> 
                </td>
            </tr>

            <tr>		
                <td> PUESTO:</td>
                <td> 
                    <span style='border-bottom:1px solid black;width:320px;display:inline-block;'>&nbsp;&nbsp;<?php echo ($empleado->getPuesto()); ?> </span> 
                </td>
                <td> R.F.C.:</td>
                <td> 
                    <span style='border-bottom:1px solid black;width:320px;display:inline-block;'>&nbsp;&nbsp;<?php echo ($empleado->getRfc()); ?> </span> 
                </td>
            </tr>

            <tr>		
                <td > CATEGORÍA:</td>
                <td> 
                    <span style='border-bottom:1px solid black;width:320px;display:inline-block;'> &nbsp;&nbsp;</span> 
                </td>
                <td >ADSCRIPCION:</td>
                <td> 
                    <span style='border-bottom:1px solid black;width:320px;display:inline-block;'>&nbsp;&nbsp;<?php echo ($ccos->getDescripcion()); ?> </span> 
                </td>
            </tr>
            <tr>		
                <td align="center" colspan='4' > &nbsp;</td>
            </tr>
        </table>	

        <br>
        <br>
        <table align="center" width="90%" border="0">	
            <tr>	
                <td align="center" > 
                    <table align="center" width="100%" border="0" cellpadding='4' cellspacing='0' style='text-transform:uppercase;font-family:arial;word-spacing:2px; letter-spacing:1px;font-size:11px;border:2px solid black;'>
                        <tr>
                            <td align="center" colspan='2' style='border-bottom:2px solid black;'> RESUMEN</td>
                        </tr>	

                        <tr>
                            <td align="center" > OFICIO DE COMISIÓN No.</td>
                            <td> 
                                <span style='border-bottom:1px solid black;width:345px;display:inline-block;'><?php echo ($c->getNumeroOficio()); ?> </span> 
                            </td>
                        </tr>
                        <tr>
                            <td align="center" >DE FECHA:</td>
                            <td> 
                                <span style='border-bottom:1px solid black;width:345px;display:inline-block;'> 
                                    <?php echo date_format(date_create($c->getFechaOficio()), 'd') . " DE " . $meses[date_format(date_create($c->getFechaOficio()), 'n')] . " DE " .  date_format(date_create($c->getFechaOficio()), 'Y'); ?> </span> 
                            </td>
                        </tr>
                        <tr>
                            <td align="center" > PERIODO DE LA COMISION DEL:</td>
                            <td> 
                                <span style='border-bottom:1px solid black;display:inline-block;'>&nbsp;&nbsp;
                                    <?php echo date_format(date_create($c->getPeriodoInicial()), 'd'); ?>-<?php echo substr($meses[date_format(date_create($c->getPeriodoInicial()), 'n')], 0,3); ?>
                                    &nbsp;&nbsp;</span> AL <span style='border-bottom:1px solid black;display:inline-block;'>&nbsp;&nbsp;
                                    <?php echo date_format(date_create($c->getPeriodoFinal()), 'd'); ?>-<?php echo substr($meses[date_format(date_create($c->getPeriodoFinal()), 'n')], 0,3); ?>&nbsp;&nbsp;</span> 
                            </td>
                        </tr>
                        <tr>
                            <td align="center" > MONTO DE LOS VIATICOS:</td>
                            <td> 
                                <span style='border-bottom:1px solid black;width:345px;display:inline-block;'>
                                    <?php echo "$".number_format($c->getMontoTotal(),2); ?> </span> 
                            </td>
                        </tr>
                        <tr>
                            <td align="center" > MONTO DEVENGADO CON DOCUMENTACION ORIGINAL:</td>
                            <td> 
                                <span style='border-bottom:1px solid black;width:345px;display:inline-block;'>
                                    <?php echo "$".(number_format($c->getMontoDoctos(),2)); ?> </span> 
                            </td>
                        </tr>
                        <tr>
                            <td align="center" > MONTO  DEVENGADO CON RECIBO DEL 40 % SIN COMPROBANTES:</td>
                            <td> 
                                <span style='border-bottom:1px solid black;width:345px;display:inline-block;'>
                                    <?php echo "$".(number_format($c->getMontoSinDoctos(),2)); ?> </span> 
                            </td>
                        </tr>
                        <tr>
                            <td align="center" >SALDO:</td>
                            <td> 
                                <span style='border-bottom:1px solid black;width:345px;display:inline-block;'>
                                    <?php echo "$".(number_format($c->getSaldoPendiente(),2)); ?> </span> 
                            </td>
                        </tr>

                    </table>
                </td>
                <td align="center" >&nbsp;&nbsp;&nbsp; </td>
                <td align="center" > 
                    <table align="center" width="100%" border="0" cellpadding='4' cellspacing='0' style='text-transform:uppercase;font-family:arial;word-spacing:2px; letter-spacing:1px;font-size:11px;border:2px solid black;'>
                        <tr>
                            <td align="center" colspan='2' style='border-bottom:2px solid black;'> CARACTERISTICA DE LOS VIATICOS </td>
                        </tr>	

                        <tr>
                            <td align="center" > NACIONALES <INPUT type='checkbox' <?php if ($c->getNacional() == 1) echo ("checked='checked'"); ?> > </td>
                            <td align="center" > INTERNACIONALES <INPUT type='checkbox' <?php if ($c->getNacional() == 2) echo ("checked='checked'"); ?> > </td>
                        </tr>
                    </table>
                    <br>
                    <table align="center" width="100%" border="0" cellpadding='4' cellspacing='0' style='text-transform:uppercase;font-family:arial;word-spacing:2px; letter-spacing:1px;font-size:11px;border:2px solid black;'>
                        <tr>
                            <td align="center" colspan='2' style='border-bottom:2px solid black;'> TARIFA OTORGADA</td>
                        </tr>	

                        <tr>
                            <td align="center" > ZONA: </td>
                            <td> 
                                <?php echo ($c->getLugar()); ?>  
                            </td>
                        </tr>
                        <tr>
                            <td align="center" >TARIFA DÍARIA:</td>
                            <td> 
                                <?php echo "$".(number_format($c->getTarifaDiaria(),2)); ?>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" > NÚMERO DE DÍAS: </td>
                            <td> 
                                <?php echo ($c->getNumeroDias()); ?> 
                            </td>
                        </tr>
                        <tr>
                            <td align="center" > TOTAL:</td>
                            <td> 
                                <?php echo "$".(number_format($c->getMontoTotal(),2)); ?>  
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
        </table>
        <br>
        <br>
        <table align="center" width="90%" border="0" cellpadding='4' cellspacing='0' style='text-transform:uppercase;font-family:arial;word-spacing:2px; letter-spacing:1px;font-size:11px;border:2px solid black;'>
            <tr>		
                <td align="center" width='50%' style='border-bottom:2px solid black;border-right:2px solid black;'>TITULAR DE LA UNIDAD ADMINISTRATIVA O SERVIDOR PUBLICO FACULTADO </td>
                <td align="center" width='50%' style='border-bottom:2px solid black;' >COMISIONADO  </td>
            </tr>
            <tr>	
                <td align='center' style='border-right:2px solid black;'> 
                    <br><br><br><br><p><span style='border-bottom:1px solid black;display:inline-block;'>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <?php echo ($sub->getNombre() . " " . $sub->getApPaterno() . " " . $sub->getApMaterno()); ?>&nbsp;&nbsp;&nbsp;&nbsp;</span></p>FIRMA
                </td>

                <td align='center'> 
                    <br><br><br><br><p><span style='border-bottom:1px solid black;display:inline-block;'>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <?php echo ($empleado->getNombre() . " " . $empleado->getApPaterno() . " " . $empleado->getApMaterno()); ?>&nbsp;&nbsp;&nbsp;&nbsp;</span></p>FIRMA
                </td>


            </tr>

        </table>
        <br>
    </body>
</html>