<?php

session_start();
include_once("../src/mx/com/virreinato/dao/PasajeDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/dao/CentroCostosDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/dao/DirectivoDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/beans/Pasaje.class.php");
include_once("../src/mx/com/virreinato/beans/CentroCostos.class.php");
include_once("../src/mx/com/virreinato/beans/Directivo.class.php");
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
header('Content-Type: text/html; charset=UTF-8'); 
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(4);
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="/css/style_Impresion.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>

        <title> Formato Pasajes </title>
    </head>
<body>
    <?php
        $dao=new PasajeDaoJdbc();
        $c=$dao->obtieneElemento($_GET['folio']);
        $ccos=new CentroCostos();
        $ccosDao=new CentroCostosDaoJdbc();

        $ccos=$ccosDao->obtieneElemento((String)($c->getIdCCosto()));

        $dirDao=new DirectivoDaoJdbc();
        $sub=$dirDao->obtieneElementoCargo("2");

        $meses = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

    $cfechaMes=null;
    ?>
    <table align="center" width="80%" border="0" style='font-family:arial;word-spacing:2px; letter-spacing:1px;font-size:11px'>
			
        <tr>
                <td align="center" colspan='2'>
                <strong> <?php echo $parametro->getValor(); ?> </strong> <br>
                Calle 20 de Agosto S/N, San Diego Churubusco, 04120 Ciudad de México, D.F., a  <?php echo(date('d')." de ".$meses[(int)(date('n'))]." de ".date('Y'));?>.
                <br><br><br><br><br>
                </td>
        </tr>

        <tr>

        <tr>				
                <td align="left" colspan='2'>Recibí de la Administración del <?php echo $parametro->getValor(); ?> la cantidad de:
                <br><br><br>
                </td>
        </tr>	

        <tr>				
            <td align="left" colspan='2'><?php echo(number_format($c->getMontoTotal(),2));?>  <br><br><br></td>
        </tr>
        <tr>	
                <td colspan='2'> 
                        <p style='text-align:justify'>
                                Por concepto de <?php echo($c->getConcepto());?>, con base en la minuta de fecha
                                <?php echo(date("d",strtotime($c->getFecha()))." de ".$meses[date("n",strtotime($c->getFecha()))]." de ".date('Y',strtotime($c->getFecha())));?>,
                                 que establece el pago de pasajes.
                        </p>
                        <br><br><br>			  
                </td>
        </tr>
        <tr>				
                <td align="left" >Periodo: <strong> Mes de <?php echo($meses[(date('n',strtotime($c->getMes())))]);?> <strong> </td>
                <td align="right" >No. de Dias: <?php echo($c->getNumeroDias());?>  </td>

        </tr>
        </table>

        <br><br>

        <table align="center" width="80%" border="1" class='TablaTarjeta' style='border-style: double;	border-color: black;	border-spacing: 0px;' >

        <tr>
        <?php
        $i = 0;
            for($i=1; $i <=15; $i++ ){
                    $day= (String)($i);
                    if(strlen($day)==1){
                            $day="0".$day;
                    }
                    $cfechaMes=$day."-".date("m",strtotime($c->getMes()))."-".date("Y",strtotime($c->getMes()));

                $fechaEnviar = null;
                $fechaEnviar = date("d-m-Y",strtotime($cfechaMes));

                   // Calendar calendar = Calendar.getInstance();  
            //calendar.setTime(fechaEnviar);  
            echo("<td  align='center'>".$i."</td>");
            }
        ?>
        </tr>
				
        <tr>	
        <?php
        $i = 0;
            for($i=1; $i<=15; $i++ ){
                    $day= (String)($i);
                    if(strlen($day)==1){
                            $day="0".$day;
                    }

                    $cfechaMes=$day."-".date("m",strtotime($c->getMes()))."-".date("Y",strtotime($c->getMes()));
                $fechaEnviar = null;
                $fechaEnviar = date("d-m-Y",strtotime($cfechaMes));

            echo("<td  align='center'><input type='text' size='2'  align='center' style='text-align:center' value='");
            if(date('N',strtotime($fechaEnviar)) == 6){
                    echo("S");
            }
            else if(date('N',strtotime($fechaEnviar))== 7){
                    echo("D");
            }
            echo("'></td>");
            }
        ?>
        </tr>
        </table>

            <br><br>

            <table align="center" width="80%" border="1" class='TablaTarjeta' style='border-style: double;	border-color: black;	border-spacing: 0px;' >
            <tr>
            <?php
                $limite=30;
                if(date("m",strtotime($c->getMes())) == "01" || date("m",strtotime($c->getMes())) == "03" || date("m",strtotime($c->getMes())) == "05" || date("m",strtotime($c->getMes())) == "07" || date("m",strtotime($c->getMes())) == "08" || date("m",strtotime($c->getMes())) == "10" || date("m",strtotime($c->getMes())) == "12")
                {    $limite=31;    }
                else if(date("m",strtotime($c->getMes())) == "02"){
                     $limite=28; 
                }
                
                $i = 0;
                for($i=16; $i <=$limite; $i++ ){
                        $day=(String)($i);
                        if(strlen($day)==1){
                                $day="0".$day;
                        }
                        $cfechaMes=$day."-".date("m",strtotime($c->getMes()))."-".date("Y",strtotime($c->getMes()));

                    $fechaEnviar = null;
                    $fechaEnviar = date("d-m-Y",strtotime($cfechaMes));
  
                echo("<td  align='center'>".$i."</td>");
                }
            ?>
            </tr>
				
                <tr>	
                <?php
                $i = 0;
                for($i=16; $i <=$limite; $i++ ){
                        $day= (String)($i);
                        if(strlen($day)==1){
                                $day="0".$day;
                        }

                        $cfechaMes=$day."-".date("m",strtotime($c->getMes()))."-".date("Y",strtotime($c->getMes()));

                    $fechaEnviar = null;
                    $fechaEnviar = date("d-m-Y",strtotime($cfechaMes));

                echo("<td  align='center'><input type='text' size='2'  align='center' style='text-align:center' value='");
                if(date('N',strtotime($fechaEnviar)) == 6){
                        echo("S");
                }
                if(date('N',strtotime($fechaEnviar)) == 7){
                        echo("D");
                }
                echo("'></td>");
                }
                ?>
            </tr>
            </table>
            
            <br><br>
            <hr style='width:300px;text-align:left;margin-left:70px' align='left'>
            <div  align='left' style='text-transform:uppercase;padding-left:70px'>NOMBRE: <?php $emp = $c->getEmpleado();
                                                                                            echo($emp->getNombre()." ".$emp->getApPaterno()." ".$emp->getApMaterno() );?>		</div>
            <div  align='left' style='text-transform:uppercase;padding-left:70px'>RFC: <?php echo($emp->getRfc()." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; No. EMP.  ".$emp->getNumero() );?></div>
            <br><br><br><br>
            <div  align='CENTER' >AUTORIZO<div>
            <br><br><br><br>
            <div  align='CENTER' style='text-transform:uppercase'><?php echo($sub->getNombre()." ".$sub->getApPaterno()." ".$sub->getApMaterno() );?> <br> <?php echo($sub->getCargo());?> <div>
            <br><br>
</body>
</html>

