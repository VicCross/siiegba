<?php
include_once("../src/mx/com/virreinato/dao/ComisionDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/dao/CentroCostosDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/dao/DirectivoDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
session_start();
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: ../index.php");
}
header('Content-Type: text/html; charset=UTF-8');
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(4);
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="/css/style_Impresion.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>

        <title> Viáticos No Comprobables </title>
    </head>
    <body>
        <?php
        $dao = new ComisionDaoJdbc();
        $c = $dao->obtieneElemento($_GET["folio"]);
        $ccos = new CentroCostos();
        $ccosDao = new CentroCostosDaoJdbc();

        $ccos = $ccosDao->obtieneElemento($c->getIdCCosto());

        $dirDao = new DirectivoDaoJdbc();
        $sub = $dirDao->obtieneElementoCargo("2");
        $dir = $dirDao->obtieneElementoCargo("1");

        $meses = array("", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        ?>

        <table align="center" width="80%" border="0" style='font-family:arial;word-spacing:2px; letter-spacing:1px;font-size:11px'>
            <tr>
                <td align="left" > 
                    <img src="../img/Conaculta_INAH.png" width='180' height='35' alt="Logo" style="border:0;" />
                    <br><br><br><br>
                </td>
            </tr>

            <tr>
                <td align="right">
                    <strong> INSTITUTO NACIONAL DE ANTROPOLOGÍA E HISTORIA <br> SECRETARÍA ADMINISTRATIVA </strong> <br>
                </td>
            </tr>

            <tr>

            <tr>				
                <td align="right" >México D.F. a <?php echo date_format(date_create($c->getFechaOficio()), 'd') . " DE " . $meses[date_format(date_create($c->getFechaOficio()), 'n')] . " DE " .  date_format(date_create($c->getFechaOficio()), 'Y'); ?>  <br><br><br><br><br><br></td>
            </tr>	

            <tr>				
                <td align="right" >BUENO POR: &nbsp;&nbsp;&nbsp; <?php echo "$".(number_format($c->getMontoSinDoctos(),2)); ?>  <br><br><br><br><br><br><br><br></td>
            </tr>
            <tr>	
                <td colspan='2'> 
                    <p style='text-align:justify'>
                        Recibí del Instituto Nacional de Antropología e Historia a través del <?php echo $parametro->getValor(); ?>, 
                        la cantidad de $<?php echo (number_format($c->getMontoSinDoctos(),2)); ?> ( <?php echo ($c->getLetraSinDoctos()); ?> 00/100 M. N.), por concepto de gastos no comprobables, 
                        conforme lo establece la Circular SA-001/2001 Apartado 1 CNRF, Disposición CNRF-IV.3. VIÁTICOS, 
                        numeral QUINTO; originados en la comisión realizada en <?php echo ($c->getLugar()); ?>, 
                        en el periodo comprendido del   <?php echo date_format(date_create($c->getPeriodoInicial()), 'd'); ?> de <?php echo $meses[date_format(date_create($c->getPeriodoInicial()), 'n')]; ?> al <?php echo date_format(date_create($c->getPeriodoFinal()), 'd'); ?> de <?php echo $meses[date_format(date_create($c->getPeriodoFinal()), 'n')]; ?> del <?php echo date_format(date_create($c->getPeriodoFinal()), 'Y'); ?>.
                    </p>

                </td>
            </tr>
        </table>

        <br><br><br><br><br><br><br><br>

        <table align="center" width="80%" border="1" class='TablaTarjeta' style='border-style: double;	border-color: black;	border-spacing: 0px;' >

            <tr>	
                <td  align='center'>ATENTAMENTE<br><br><br><br><br><br><br></td>
                <td  align='center'>VISTO BUENO<br><br><br><br><br><br><br></td>
                <td  align='center'>AUTORIZA<br><br><br><br><br><br><br></td>
            </tr>


            <tr>	
                <td  align='center'><?php $empleado = $c->getEmpleado(); echo ($empleado->getNombre() . " " . $empleado->getApPaterno() . " " . $empleado->getApMaterno()); ?>		</td>
                <td  align='center'><?php echo ($sub->getNombre() . " " . $sub->getApPaterno() . " " . $sub->getApMaterno()); ?></td>
                <td  align='center'><?php echo ($dir->getNombre() . " " . $dir->getApPaterno() . " " . $dir->getApMaterno()); ?></td>

            </tr>

            <tr>	
                <td  align='center'><?php echo ($empleado->getPuesto()); ?></td>
                <td  align='center'><?php echo ($sub->getCargo()); ?></td>
                <td  align='center'><?php echo ($dir->getCargo()); ?></td>

            </tr>

        </table>

    </body>
</html>