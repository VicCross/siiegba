<?php

include_once("../src/mx/com/virreinato/dao/ProveedorDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Proveedor.class.php");
include_once("../src/mx/com/virreinato/dao/CatChequeDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatCheque.class.php");
include_once("../src/mx/com/virreinato/dao/AreaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Area.class.php");
include_once("../src/mx/com/virreinato/dao/EmpleadoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Empleado.class.php");
include_once("../src/mx/com/virreinato/dao/PresupuestoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Presupuesto.class.php");
include_once("../src/mx/com/virreinato/dao/SueldoGbDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/SueldoGB.class.php");
include_once("../src/mx/com/virreinato/dao/DetalleSueldoGbDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/DetalleSueldoGB.class.php");
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<link href="../css/style_Impresion.css" type="text/css" rel="stylesheet">
<script>
   function Mes(num){
   	 if(num == "01") return "ENERO";
   	 if(num == "02") return "FEBRERO";
   	 if(num == "03") return "MARZO";
   	 if(num == "04") return "ABRIL";
   	 if(num == "05") return "MAYO";
   	 if(num == "06") return "JUNIO";
   	 if(num == "07") return "JULIO";
   	 if(num == "08") return "AGOSTO";
   	 if(num == "09") return "SEPTIEMBRE";
		 if(num == "10") return "OCTUBRE";
		 if(num == "11") return "NOVIEMBRE";
		 if(num == "12") return "DICIEMBRE";
   }

   $().ready(function(){
   	 var fecha =  $("#fecha").val();
   	 if( fecha != "" ){
   	 	var aux = fecha.split("-");
   	 	$("#fechaEmision").text(aux[0]+" "+Mes(aux[1])+" "+aux[2] );
   	 }
   	 
   });
</script>
<title>Impresión de Cheques - Nómina de Gasto Básico</title>
</head>
<?php
    $idSueldos = null;
    if(isset($_GET['idSueldos']))
    {    $idSueldos = (String) $_GET["idSueldos"];  }

    //con el id_sueldos obtengo los cheques
    $daoCheque=new CatChequeDaoJdbc();
    $cheque=new CatCheque();	

    $listaCheques = $daoCheque->obtieneChequesGB($idSueldos);

    $aux = 0;

    $meses = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    
?>
<body>
<?php
    $contador=1;
    //itero la lista de cheques
    $x = count($listaCheques);
    $x2 = 1;
    foreach($listaCheques as $cheque){

?>

    <div id="solicitud" style='paddinq:0px; margin:0px'>
		
        <table align="center" width="100%" style='font-family:arial;font-size:8pt;text-transform:uppercase'>
                <tr>

         <td align="right" id="fechaEmision" colspan='2' style='padding-right:45px;padding-top:5pt' > <?php echo(date("d",strtotime($cheque->getFechaEmision()))."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$meses[date("n",strtotime($cheque->getFechaEmision()))]."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".date("Y",strtotime($cheque->getFechaEmision())) ); ?></td>			
            </tr>

            <tr >
                <td width="70%" align='left' style='font-family:arial;font-size:9pt;text-transform:uppercase;padding-top:0pt'>
                <br>
                <br>
                <br>
                <?php
			       
                    if( (String)($cheque->getIdEmpleado())!="0" && $cheque->getIdEmpleado()!= null){
                       $aux = 1;

                            $daoEm=new EmpleadoDaoJdbc();
                            $elementoEm = $daoEm->obtieneElemento((String)($cheque->getIdEmpleado()));
                            echo($elementoEm->getNombre()." ". $elementoEm->getApPaterno(). " ".$elementoEm->getApMaterno() ); 

                    }
                ?>

                </td>
                    <td  width="30%" align="right" style='font-family:arial;font-size:9pt;text-transform:uppercase;padding-top:0pt' >				   	
                    <br>
                    <br>
                    <br><?php echo("$".number_format($cheque->getMonto(),2)); ?></td>
            </tr>
				
            <tr>

               <td style='font-family:arial;font-size:9pt;text-transform:uppercase;padding-top:2.2ex;padding-bottom:1ex' width="20%" >( &nbsp; <?php echo( $cheque->getMontoLetra() );?> &nbsp; ) </td>
            </tr>
        </table>
    </div>
    
    <?php
        if($x > $x2){
            if($contador==3){
                        ?>
                        <span class="page-break">&nbsp;</span>
                        <?php			
                        $contador=1;
                }
                else{
                        ?>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <?php
                        $contador++;
                }

        }
    ?>
		
    <?php
    $x2++;
    }
    ?>
	
	
</body>
</html>