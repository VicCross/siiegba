<?php

session_start();
include_once("../src/mx/com/virreinato/dao/NominaProyDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/NominaProy.class.php");
include_once("../src/mx/com/virreinato/dao/CatProyectoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatProyecto.class.php");
include_once("../src/mx/com/virreinato/dao/PersonalProyDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/PersonalProy.class.php");
include_once("../src/mx/com/virreinato/dao/DirectivoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Directivo.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../css/style_Impresion.css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title> NOMINA </title>
</head>
<?php 
    $folio = null;

    if(isset($_GET['folio'])){ $folio = (String) $_GET['folio']; }
    $dao=new NominaProyDaoJdbc();
    $elemento=new NominaProy();
    $elemento=$dao->obtieneElemento($folio);

    $dirDao=new DirectivoDaoJdbc();
    $sub=$dirDao->obtieneElementoCargo("2");
    $dir=$dirDao->obtieneElementoCargo("1");	
	   
?>
<body valign="center">
    </br></br>
    <div id="encabezado">
        <table align="center" width="90%" border="1" cellspacing="0" cellpadding="2" >
            <tr>
                <td>
                <table align="center" width="100%" border="0" cellspacing="0" cellpadding="2">
                <tr>
                    <td align="center" width="40%"> <img src="../img/lors.png" width="85%"> </td>
                    <td> </td>
                    <td width="50%"> </td>
                </tr>
                <tr>
                    <td> </td>
                    <td class="Contenido" width="45%"> PROYECTO: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <u> &nbsp; 
                    <?php 
                        $catProyecto=new CatProyectoDaoJdbc();
                        $catProyectos=$catProyecto->obtieneListado2(); 
                        $cp = new CatProyecto();
                        
                        foreach($catProyectos as $cp){
                            if($elemento!= null && $elemento->getIdProy()!= null ){
                                if($cp->getId()==$elemento->getIdProy()){  echo( $cp->getDescripcion() ); }	
                            }	  

                        }
                    ?>&nbsp; </u>
                    </br> RESPONSABLE: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <u>&nbsp; <?php if($elemento!=null && $elemento->getResponsable()!= null) echo($elemento->getResponsable());?> &nbsp;</u>
                    </br> PERIODO DE PAGO: &nbsp; <u> &nbsp; &nbsp; <b> <?php if($elemento!=null && $elemento->getPeriodoPago()!= null) echo($elemento->getPeriodoPago());?> </b> &nbsp; &nbsp; </u>

                    </td> 
                </tr>
						
                </table>
                </td>
            </tr>
        </table>
        </div>
        </br>
        </br>
        <div id="despliegue">
            <table align="center" width="90%" border="1" cellspacing="0" cellpadding="2" class="Contenido">
                <tr> 
                    <th align="center"> R.F.C. </th>
                    <th align="center" width="25%"> NOMBRE </th>
                    <th align="center"> DIAS </th>
                    <th align="center"> SALARIO </br> DIARIO </th>
                    <th align="center"> IMPORTE </th>
                    <th align="center"> CREDITO AL </br> SALARIO </th>
                    <th align="center"> BONIFICACION </br> S/AGUINALDO </th>
                    <th align="center"> NETO </th> 
                    <th align="center" width="15%"> FIRMA </th>
                </tr>
				
                <tr> <!-- Colocar los datos del empleado-->
                   <?php 
                        $personalProy=new PersonalProyDaoJdbc();
                        $personalProys=$personalProy->obtieneListado(); 
                        $c = new PersonalProy();
                        
                        $total = "";
                        foreach($personalProys as $c){
                            if($elemento!= null && $elemento->getIdPersonal()!= null ){
                                if($c->getId()==$elemento->getIdPersonal()){
                                    echo( "<td align='center' height='80'>".$c->getRfc()."</td>" );
                                    echo( "<td align='center'>".$c->getApp().' '.$c->getApm().' '.$c->getNombre()."</td>" );
                                    echo( "<td align='center'>".$elemento->getDias()."</td>" );
                                    echo( "<td align='center'>". "$".number_format(  $elemento->getSalarioDiario(),2 )."</td>" );
                                    echo( "<td align='center'>". "$".number_format(  $elemento->getImporte(),2 )."</td>" );
                                    echo( "<td align='center'>". "$".number_format(  $elemento->getCreditoAlSalario(),2 )."</td>" );
                                    echo( "<td align='center'>". "$".number_format(  $elemento->getBonificacion(),2 )."</td>" );
                                    echo( "<td align='center'>"."$".number_format($elemento->getNeto(),2 )."</td>" );
                                    echo( "<td rowspan='2'></td>" );
                                    $total = "$".number_format($elemento->getNeto(),2 );

                                }	
                            }	
                        }
                  ?>
                </tr>
				
                <tr>
                   <td align="center" colspan="7" >(TOTAL NETO A PAGAR) &nbsp; &nbsp; &nbsp; <label id="TotalNetoLetra"></label></td>

                        <td align="center"><?php  echo( $total ); ?> </td> <!--Colocar el total a pagar -->
                </tr>
            </table>
        </div>
		
        </br>
        <blockquote><blockquote><blockquote><blockquote>
        <label class="Contenido">	R.F.C. INA 460815GV1 </label> <!-- Aqui va el rfc-->
        </blockquote></blockquote></blockquote></blockquote>

        </br></br></br></br></br>

        <div>		

        <table align="center" width="90%" border="0" cellspacing="0" class="Contenido">
            <tr> <!-- nombre de quienes autorizan -->
                <td align="center" width="33%"> <?php  echo($sub->getNombre()." ".$sub->getApPaterno()." ".$sub->getApMaterno() );?> </br> FORMULO </td>
                <td align="center" width="33%"> <?php  echo($dir->getNombre()." ".$dir->getApPaterno()." ".$dir->getApMaterno() );?> </br> AUTORIZO </td>
                <td align="center" width="33%"> GERARDO MONTELONGO CAMACHO </br> Vo. Bo. </td>
            </tr>
        </table>
        </div>
</body>
</html>
    

