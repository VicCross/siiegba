<?php
session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/DocumentacionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Documentacion.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php?err=2");
}

$dao = new DocumentacionDaoJdbc();
	$elemento = new Documentacion();
	$elemento = $dao->obtieneElemento($_GET['id']);
	$res = $dao->eliminaElemento($elemento);
	
	
	
	
if(isset($_POST['inicioFecha']) && $_POST['inicioFecha']!=NULL){
    $inicio = $_POST["inicioFecha"];
    $fin= $_POST["finFecha"];
    header("Location: ../../../../../Formatos/lista_Documentos.php?inicio=".$inicio."&fin=".$fin);
}else if(isset($_POST['guardar'])){
	$dao = new DocumentacionDaoJdbc();
	$elemento = new Documentacion();
	$elemento->setIdEmpleado($_SESSION['idEmpleado']);
	$elemento->setVersion($_POST['version']);
	$elemento->setIdArea($_POST['area']);
	$elemento->setIdProyecto($_POST['proyecto']);
	$elemento->setObservaciones($_POST['observaciones']);
	/*Para subir el archivo*/
	$carpeta = "/Documentos/";
	$nomArchivo = $_FILES['archivo']['name'];
	opendir($_SESSION['RAIZ'] . $carpeta);
	$destino = ".." . $carpeta . $nomArchivo;
	copy($_FILES['archivo']['tmp_name'], $_SESSION['RAIZ'] . $carpeta . $nomArchivo);
	$elemento->setArchivo($destino);
	$res = $dao->guardaElemento($elemento);
	if($res) $respuesta = "Registro guardado con éxito.";
	else $respuesta = "Registro no se pudo guardar.";
	header("Location: lista_Documentos.php?respuesta=".$respuesta);	
}else if(isset($_GET['id'])){
	$dao = new DocumentacionDaoJdbc();
	$elemento = new Documentacion();
	$elemento = $dao->obtieneElemento($_GET['id']);
	$res = $dao->eliminaElemento($elemento);
	if($res) $respuesta = "Registro eliminado con éxito.";
	else $respuesta = "El registro no se pudo eliminar";
	
	
	header("Location: lista_Documentos.php?respuesta=".$respuesta);	
}

?>