 <!DOCTYPE html>
<html>
<head>
	<title>Aplicaciones</title>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
    <meta http-equiv="X-UA-Compatible" content="IE=7" />
    <link rel="stylesheet" type="text/css" href="css/aplicaciones.css">
</head>
<body>
    <div class="liston">Aplicaciones</div>
	<div class="aplicaciones">

                    <div class="gestion_de_presupuesto">
                        <ul class="gestion_de_presupuesto_ul"> 
                            <ul><a href="GestionPresupuesto/nvaGestion.php" target= "destino">Solicitar Gestión</a></ul>
                            <ul><a href="GestionPresupuesto/lista_GestArch.php" target= "destino" target= "destino">Solicitudes</a></ul> 
                            <ul><a href="GestionPresupuesto/lista_GestArchAcept.php" target= "destino">Solicitudes Aceptadas</a></ul>
                            <ul><a href="GestionPresupuesto/lista_GestArchRechaz.php" target= "destino">Solicitudes Rechazadas</a></ul>
                            <ul><a href="GestionPresupuesto/lista_GestArchGeneral.php" target= "destino">Todas las Solicitudes</a></ul>
                        </ul>
                    </div>

                    <div class= "consulta_presupuesto">
                        <ul class="consulta_presupuesto_ul"> 
                            <ul><a href="ConsultaPresupuesto/Resumen.php" target= "destino">Resumen</a></ul> 
                            <ul><a href="ConsultaPresupuesto/resumeninba.php" target= "destino">INBA</a></ul>
                            <ul><a href="ConsultaPresupuesto/resumenpatro.php" target= "destino" >Patronatos</a></ul>
							<ul><a href="#">Productos</a></ul>
                        </ul>
                    </div>

                    <div class="ingresos_inba">
                    	<ul class="ingresos_inba_ul">
                            <ul><a href= "catalogos/lista_ingresosINBA.php" target= "destino">Ingresos INBA</a></ul>
                        </ul>
                    </div>

                    <div class="patronato">
                        <ul class="patronato_ul">
                    	    <ul><a href= "patronato/lista_patronato.php" target= "destino">Patronato</a></ul>
                            <ul><a href= "catalogos/lista_ingresosPatronato.php" target= "destino">Ingresos Patronato</a></ul>
                        </ul>
                    </div>

                    <div class= "administracion_del_sistema">
                        <ul class="administracion_del_sistema_ul">
						    <ul><a href= "catalogos/lista_admonsistem.php" target = "destino" >Administracion del Sistema</a></ul>
                            <ul><a href= "catalogos/lista_expo.php" target= "destino">Exposiciones Temporales</a></ul>
                        </ul>
					</div>

                    <div class= "donativos"> 
                        <ul class="donativos_ul">           
						  <ul><a href= "donativos_palacio/lista_donativospalacio.php" target= "destino">Donativos</a></ul>
                        </ul>  
					</div>

					<div class= "catalogos">
                        <ul class="catalogos_ul">
						  <ul><a href= "catalogos/menu_catalogos.php" target= "destino">Catalogos</a></ul>
                        </ul>
					</div>

					<div class= "documentos">
                    	<ul class="documentos_ul">
						  <ul><a href= "Formatos/lista_Documentos.php" target= "destino">Documentos</a></ul>
                        </ul>
					</div>
					
					<div class= "presupuestos_por_area">
                    	<ul class="presupuestos_por_area_ul">
							<ul><a href= "Presupuesto/AltaMeta.php" target= "destino">Presupuestos por Área</a></ul>
                        </ul>
					</div>

									
    </div>
</body>
</html>