<?php
$id = null;
$user = null;

session_start();
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/ConsultaResumenDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ConsultaResumen.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
} else {
    if (isset($_SESSION['id'])) {
        $id = $_SESSION['id'];
    }
    if (isset($_SESSION['user'])) {
        $user = $_SESSION['user'];
    }
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>

<!DOCTYPE html PUBLIC >
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="../css/style.css" />
        <script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
       <!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="../css/lista.css">
        <script type="text/javascript" src="../media/js/complete.js"></script>
        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
        <script type="text/javascript" src="../js/lista.js"></script>
	   
	   
        <title>Consulta de Presupuesto (Resumen) </title>
    </head>
    <body>
        <div class="contenido">
            <br/>
            <p class="titulo_cat1">Consulta de Presupuesto > Resumen </p>
            <form name="frmFiltroResumen" id="frmFiltroResmuen" method="POST" action="../src/mx/com/virreinato/web/WebConsultaPresupuesto.php" >
                <?php
                    $periodo = "";
                ?>
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Período:  
                <select name="periodo" id="periodo" style="width:110px" > 
                    <?php
                    $daoPer = new PeriodoDaoJdbc();
                    $listaPer = $daoPer->obtieneListado();
                    $elementoPer = new Periodo();
                    foreach ($listaPer as $elementoPer) {
                        $sel = "";
                        if (isset($_GET['periodo']) && (int) ( $_GET['periodo'] ) == $elementoPer->getId())
                            $sel = "selected='selected'";
                        else if ($_GET['periodo'] == null && (int) ($elementoPer->getPeriodo()) == date('Y')) {
                            $sel = "selected='selected'";
                            $periodo = (String) ($elementoPer->getId() );
                        }
                        echo("<option value=" . $elementoPer->getId() . " " . $sel . " >" . $elementoPer->getPeriodo() . "</option>");
                    }
                    ?>  
                </select>

                &nbsp; <input name="Filtro" style="cursor:pointer" type="submit" value="Filtrar"  class='btn' />
            </form>
            <!-- /p -->
            <br/>	
            <table   border="1" cellspacing="0" cellpadding="3" class='dataTable' align='center' class="SizeText">
            <thead>              
			  <tr >
                    <th width="25%" >Metas</th>
                    <th width="9.5%">Autorizado</th>
                    <th	width="9.5%">Ministrado</th>
                    <th width="9.5%" >Por Ministrar</th>
                    <th width="9.5%" >Gastado</th>
                    <th width="9.5%" >Por gastar ministrado</th>
                    <th width="9.5%" >Por gastar</th>
                    <th width="9.5%" >Comprobado (INAH)</th>
                    <th width="10%" >Por comprobar (INAH)</th>
                </tr>
				<thead>              
				<tfoot>
				<tr >
                    <th width="25%" >Metas</th>
                    <th width="9.5%">Autorizado</th>
                    <th	width="9.5%">Ministrado</th>
                    <th width="9.5%" >Por Ministrar</th>
                    <th width="9.5%" >Gastado</th>
                    <th width="9.5%" >Por gastar ministrado</th>
                    <th width="9.5%" >Por gastar</th>
                    <th width="9.5%" >Comprobado (INAH)</th>
                    <th width="10%" >Por comprobar (INAH)</th>
                </tr>
				</tfoot>              
				<tbody>

                <?php
                if (isset($_GET['periodo'])) {
                    $periodo = (String) $_GET['periodo'];
                } else {
                    $aniosel = (String) date('Y');
                    $per = new PeriodoDaoJdbc();

                    $periodo = $per->obtieneidElemento($aniosel);
                }

                $dao = new ConsultaResumenDaoJdbc(); //Traemos todos los nombres de proyecto
                $lista = $dao->obtieneMetas2($user, $periodo);
                $elemento = new ConsultaResumen();

                $indexPartidas = 1;
                $idMinistracion = 0;

                foreach ($lista as $elemento) {
                    $elemento->setPorGastar($elemento->getAutorizado() - $elemento->getGastado());
                    ?>
                    <tr >
                        <th align="left" > <img src="../img/abrir.png" title="Mostrar Partidas" width="14" height="14" style="cursor:pointer" id="abrir<?php echo$elemento->getIdMeta(); ?>" onclick="mostrarPartidas(<?php echo$elemento->getIdMeta(); ?>)"  > <img style="display:none;cursor:pointer" title="Ocultar Partidas" id="cerrar<?php echo$elemento->getIdMeta(); ?>" src="../img/cerrar.png" width="12" height="10" style="cursor:pointer" onclick="ocultarPartidas(<?php echo$elemento->getIdMeta(); ?>)"  > &nbsp;  <?php echo($elemento->getMeta()); ?></th>
                        <td align="right"><?php echo(number_format($elemento->getAutorizado(), 2)); ?></td>
                        <td align="right"></td>
                        <td align="right"></td>
                        <td align="right"><?php echo(number_format($elemento->getGastado(), 2)); ?></td>
                        <td align="right"></td>
                        <td align="right"><?php echo(number_format($elemento->getPorGastar(), 2)); ?></td>
                        <td align="right"></td>
                        <td align="right"></td>
                    </tr>
                    <?php
                    $partidasLt = $dao->obtienePartidas((String) ($elemento->getIdMeta()));
                    $partidas = new ConsultaResumen();

                    foreach ($partidasLt as $partidas) {
                        $partidas->setPorMinistrar($partidas->getAutorizado() - $partidas->getMinistrado());

                        echo("<tr id='" . $elemento->getIdMeta() . "" . $indexPartidas . "' style='display:none' >");
                        echo("<th align='left' style='cursor:pointer' > &nbsp; &nbsp; &nbsp; &nbsp; <acronym title='" + $partidas->getDescPartida() + "' > Partida: " + $partidas->getPartida() + "</acronym> </th>");
                        echo("<td align='right' >" . number_format($partidas->getAutorizado(), 2) . "</td>");
                        echo("<td align='right' >" . number_format($partidas->getMinistrado(), 2) . "</td>");
                        echo("<td align='right' >" . number_format($partidas->getPorMinistrar(), 2) . "</td>");
                        echo("<td align='right' ></td>");
                        echo("<td align='right' ></td>");
                        echo("<td align='right' ></td>");
                        echo("<td align='right' ></td>");
                        echo("<td align='right' ></td>");
                        echo("</tr>");

                        $indexPartidas = $indexPartidas + 1;

                        /* if( idMinistracion != $partidas->getIdMinistracion()){
                          List<ConsultaResumen> comprobacionLt = $dao->obtienePartidasComprobadas( $partidas->getIdMinistracion() );
                          ConsultaResumen comprobacion =new ConsultaResumen();
                          Iterator<ConsultaResumen> itcomprobacion = comprobacionLt.iterator();

                          while(itcomprobacion.hasNext()){
                          comprobacion = itcomprobacion.next();

                          echo("<tr id='"+$elemento->getIdMeta()+""+indexPartidas+"' style='display:none'  >");
                          echo("<th align='left' style='cursor:pointer' > &nbsp; &nbsp; &nbsp; &nbsp; <acronym title='"+comprobacion.getDescPartida()+"' > Partida: "+comprobacion.getPartida()+"</acronym> </th>");
                          echo("<td align='right' ></td>");
                          echo("<td align='right' ></td>");
                          echo("<td align='right' ></td>");
                          echo("<td align='right' ></td>");
                          echo("<td align='right' ></td>");
                          echo("<td align='right' ></td>");
                          echo("<td align='right' >"+number_format(comprobacion.getComprobadoINAH())+"</td>");
                          echo("<td align='right' ></td>");
                          echo("</tr>");

                          indexPartidas = indexPartidas + 1;
                          idMinistracion = $partidas->getIdMinistracion();
                          }
                          } //END IF COMPROBADAS */
                    } //END WHILE PARTIDAS

                    echo("<input type='hidden' value='" . $indexPartidas . "' id='" . $elemento->getIdMeta() . "'/> ");
                } //END WHILE METAS (ELEMENTO)
                ?>
				<tbody>
            </table>
        </div>
        <script>
            function mostrarPartidas(id) {
                var partidas = parseInt($("#" + id).val());
                for (i = 1; i < partidas; i++) {
                    var nombre = id + "" + i;
                    $("#" + nombre).show();
                }
                $("#cerrar" + id).show();
                $("#abrir" + id).hide();
            }


            function ocultarPartidas(id) {
                var metas = parseInt($("#" + id).val());
                for (k = 1; k < metas; k++) {
                    var nombre = id + "" + k;
                    $("#" + nombre).hide();
                }

                $("#cerrar" + id).hide();
                $("#abrir" + id).show();
            }

        </script>
    </body>
</html>