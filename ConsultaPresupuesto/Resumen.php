<?php

$id = null;
$user = null;
  
session_start();
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/LineaAccionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/LineaAccion.class.php");
include_once("../src/mx/com/virreinato/dao/AreaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Area.class.php");
include_once("../src/mx/com/virreinato/dao/ExposicionesTemporalesDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ExposicionesTemporales.class.php");
include_once("../src/mx/com/virreinato/dao/GestionPresupuestalDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/GestionPresupuestal.class.php");
include_once("../src/mx/com/virreinato/dao/IngresosINBADaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/IngresosINBA.class.php");
include_once("../src/mx/com/virreinato/dao/IngresosPatronatoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/IngresosPatronato.class.php");
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
} else {
    if (isset($_SESSION['id'])) {
        $id = $_SESSION['id'];
    }
    if (isset($_SESSION['user'])) {
        $user = $_SESSION['user'];
    }
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);

$array_numero_partidas = array();
$array_numero_metas = array();

?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="../css/style.css" />
        <script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
        <!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="../css/lista.css">
        <script type="text/javascript" src="../media/js/complete.js"></script>
        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
        <script type="text/javascript" src="../js/lista.js"></script>
    
    <!-- graficas -->
    
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

        <meta name ="author" content ="Norfi Carrodeguas">
    
    <!-- fin      -->
    
    
        <title>Consulta de Presupuesto (Resumen) </title>
    
    <!-- d3js -->
    <meta charset="utf-8">
    <style>

           .node {
                   cursor: pointer;
         }

           .node circle {
                          fill: #fff;
                          stroke: steelblue;
                          stroke-width: 1.5px;
                        }

           .node text {
                        font: 10px sans-serif;
                      }

           .link {
                   fill: none;
                   stroke: #ccc;
                   stroke-width: 1.5px;
                 }

        </style>
    
    
    <!--fin d3js-->
    
    
    
    </head>
    <body>
        <div class="contenido">
            <br/>
            <p class="titulo_cat1">Consulta de Presupuesto > Resumen</p>
            <?php
      
                if(isset($_GET['recargado']) && $_GET['recargado'] == "1"){
                    echo '<input type="hidden" id="nombre_form" name="nombre_form" value=""/>';
                }else{
                    echo '<input type="hidden" id="nombre_form" name="nombre_form" value="frmFiltroResmuen"/>';
                }
        
        
            ?>            
            <input type="hidden" id="no_sort" name="no_sort" value="1"/>
            <form name="frmFiltroResumen" id="frmFiltroResmuen" method="POST" action="../src/mx/com/virreinato/web/WebConsultaPresupuesto.php" >
                <?php
        
                $periodo= "";
        $hayNumerosRojos = false;
        
                ?>
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Período:  
                <select name="periodo" id="periodo" style="width:110px" > 
                    <?php
          
          
          
                    $daoPer = new PeriodoDaoJdbc();
                    $listaPer = $daoPer->obtieneListado();
                    $elementoPer = new Periodo();
                    foreach ($listaPer as $elementoPer) {
                        $sel = "";
                        if (isset($_GET['periodo']) && (int) ( $_GET['periodo'] ) == $elementoPer->getId())
                            $sel = "selected='selected'";
                        else if ($_GET['periodo'] == NULL && (int) ($elementoPer->getPeriodo()) == date('Y')) {
                            $sel = "selected='selected'";
                            $periodo = (String) ($elementoPer->getId() );
                        }
                        echo("<option value=" . $elementoPer->getId() . " " . $sel . " >" . $elementoPer->getPeriodo() . "</option>");
                    }
          
          
                    ?>  
                </select>
                &nbsp; <input name="Filtro" style="cursor:pointer" type="submit" value="Filtrar"  class='btn' />
            </form>
      
            <br/>
           <?php echo("Periodo: " . $_GET['periodo']);  ?> <!-- esto es lo que nos marcaba el 42 para 16  y 42 para 1?> --> 
            <br/> 
            <table border="1" cellspacing="0" cellpadding="3" class="dataTable" align="right">
              <thead>
                  <tr >
                      <th width="23%" >Destino</th>
                        <th width="11%">Autorizado</th>
                        <th width="11%">Gestionado</th>
                        <th width="11%" >Por Gestionar</th>
                        <th width="11%" >Pagado</th>
                       <!-- <th width="11%" >Por gastar</th> -->
                        <th width="11%" >Ingresos (INBA)</th>
                        <th width="11%" >Ingresos (Patronato)</th>
                    </tr>
                </thead>  
                <tfoot>
                    <tr >
                        <th width="23%" ></th>
                        <th width="11%"></th>
                        <th width="11%"></th>
                        <th width="11%" ></th>
                        <th width="11%" ></th>
                        <!-- <th width="11%" ></th> -->
            <th width="11%" ></th>
                        <th width="11%" ></th>
                    </tr>

                </tfoot>
                <tbody>
                  <?php
          

            
            $tot1 = 0.0; $tot2 = 0.0; $tot3 = 0.0; $tot4 = 0.0; $tot5 = 0.0; $tot6 = 0.0; $tot7 = 0.0;
            $daoLA = new LineaAccionDaoJdbc(); 
            
            $lista = $daoLA->obtieneListadofiltro($_GET['periodo']);
            
            $elemento = new LineaAccion();
            $daoGP = new GestionPresupuestalDaoJdbc();
            $daogp2= $daoGP->obtieneLista();
            $elemento2 = new IngresosINBADaoJdbc();
            $inbabean=new IngresosINBA();
            $daoPatronato = new IngresosPatronatoDaoJdbc();
            
            
            $patronato = $daoPatronato->obtieneMontoProy($elemento->getId(), $_GET['periodo']);
            
            echo $inba ;
            echo $patronato;
            foreach($lista as $elemento){
              $autorizado = $elemento->getSaldoInicial();
              $porgestionar = $elemento->getSaldoAnterior();
              $gestionado = $autorizado - $porgestionar;
              $inba=  $elemento->getInba();
              $patronato= $elemento->getPatronato();
              $pagado = $elemento->getPagado();
              
              //$porpagar = $daoGP->obtieneMontoPorEstatusProy($elemento->getId(), 1, $_GET['periodo']) + $daoGP->obtieneMontoPorEstatusProy($elemento->getId(), 2, $_GET['periodo']) + $daoGP->obtieneMontoPorEstatusProy($elemento->getId(), 3, $_GET['periodo']);
              //$inba = $daoINBA->obtieneMontoProy($elemento->getId(), $_GET['periodo']);
              /*totales*/
              
              //echo "<br>esto es lo de porgestionar".$porgestionar;
            //  echo "<br>esto es lo de pagado".$pagado;
            //  echo "<br>esto es lo de porpagar".$porpagar;
            //  echo "<br>esto es lo de por gestionar".$porgestionar;
            //  echo "<br>esto es lo de inba".$inba;
              $tot1 += $autorizado;
              $tot2 += $gestionado;
              $tot3 += $porgestionar;
              $tot4 += $pagado;
              //$tot5 += $porpagar;
              $tot6 += $inba;
              $tot7 += $patronato;
              
          ?>
                    <tr>
                      <td><?php if($elemento->getId() == 48){?><img src="../img/cerrar.png" style="cursor:pointer" id="cerrar" name="cerrar" onClick="cambiaStatus(true, 26)" width="14" height="14"><img src="../img/abrir.png" style="display:none;cursor:pointer" id="abrir" name="abrir" onClick="cambiaStatus(false, 26)" width="14" height="14"> <?php }?><?php echo($elemento->getLineaAccion());?></td>
                      
              <td align="right">$<?php echo(number_format($autorizado,2));?></td>
                        <td align="right">$<?php echo(number_format($gestionado,2));?></td>
                        <td align="right">$<?php echo(number_format($porgestionar,2));?></td>
                        <td align="right">$<?php echo(number_format($pagado,2));?></td>
                        <!-- <td align="right">$< ?//php echo(number_format($porpagar,2));?></td> -->
                        <td align="right">$<?php echo(number_format($inba,2));?></td> 
                        <td align="right">$<?php echo(number_format($patronato,2));?></td>
                    </tr>
                    <?php if($elemento->getId() == 48){
                $daoET = new ExposicionesTemporalesDaoJdbc();
                $lista2 = $daoET->getListado();
                $exposicion = new ExposicionesTemporales();
                $i = 1;
                foreach ($lista2 as $exposicion){
                  $autorizado = $exposicion->getSaldo();
                  
                  $porgestionar = $exposicion -> getSaldoAnterior();
                  
                  $gestionado = $autorizado - $porgestionar;
                  $pagado = $exposicion->getPagado();
                  $inba=$exposicion->getInba();
                  $patronato=$exposicion->getPatronato();
                  //$pagado = $daoGP->obtieneMontoPorEstatusExpos($exposicion->getIdExposicionT(), 4, $_GET['periodo']);
                  //$porpagar = $daoGP->obtieneMontoPorEstatusExpos($exposicion->getIdExposicionT(), 1, $_GET['periodo']) + $daoGP->obtieneMontoPorEstatusExpos($exposicion->getIdExposicionT(), 2, $_GET['periodo']) + $daoGP->obtieneMontoPorEstatusExpos($exposicion->getIdExposicionT(), 3, $_GET['periodo']);
                  //$inba = $daoINBA->obtieneMontoExpos($exposicion->getIdExposicionT(), $_GET['periodo']);
                  //$patronato = $daoPatronato->obtieneMontoExpos($exposicion->getIdExposicionT(), $_GET['periodo']);
                  
          ?>
                    <tr style="display:none" id="expos<?php echo($i);?>">
                      <td><?php echo($exposicion->getNombre());?></td>
                      <td align="right">$<?php echo(number_format($autorizado,2));?></td>
                        <td align="right">$<?php echo(number_format($gestionado,2));?></td>
                        <td align="right">$<?php echo(number_format($porgestionar,2));?></td>
                        <td align="right">$<?php echo(number_format($pagado,2));?></td>
                      <!--  <td align="right">$< ?//php echo(number_format($porpagar,2));?></td> -->
                        <td align="right">$<?php echo (number_format($inba,2));?></td> 
                        <td align="right">$<?php echo (number_format($patronato,2));?></td> 
                    </tr>
                    <?php $i += 1;}}} ?>    
          
          
          
          
                 
                  <tr>
                      <td>Totales:</td>
                        <td align="right">$<?php echo(number_format($tot1,2));?></td>
                        <td align="right">$<?php echo(number_format($tot2,2));?></td>
                        <td align="right">$<?php echo(number_format($tot3,2));?></td> 
                        <td align="right">$<?php echo(number_format($tot4,2));?></td>
                       <!-- <td align="right">$<?//php echo(number_format($tot5,2));?></td>-->
                        <td align="right">$<?php echo(number_format($tot6,2));?></td>
                        <td align="right">$<?php echo(number_format($tot7,2));?></td>
                    </tr>   
                </tbody>
            </table>
            
        </div>
        <script>
            function cambiaStatus(id, valor) {
                if(id == true){
          $("#cerrar").hide();
          $("#abrir").show();
          for(i = 1; i <= valor; i++){
            $("#expos" + i).show();
          }
        }else{
          $("#cerrar").show();
          $("#abrir").hide();
          for(i = 1; i <= valor; i++){
            $("#expos" + i).hide();
          }       
        }
            }

            
        </script>
    
<!-- --------------------------------------chart.js------------------------------------>

    <script src="../js/Chart.js"></script>
<div id="canvas-holder">
<canvas id="chart-area" width="300" height="300"></canvas>
<canvas id="chart-area2" width="300" height="300"></canvas>
<canvas id="chart-area3" width="600" height="300"></canvas>
<canvas id="chart-area4" width="600" height="300"></canvas>
</div>
<script>


  var barChartData = {
    labels : ["Autorizado","Gestionado","Por Gestinar","Pagado"],
    datasets : [
      {
        fillColor : "#3ADF00",
        strokeColor : "#ffffff",
        highlightFill: "#1864f2",
        highlightStroke: "#ffffff",
        data : [<?php echo ($tot1);?> ,<?php echo ($tot2);?>,<?php echo ($tot3);?>,<?php echo ($tot4);?>]
      },
      {
        fillColor : "#e9e225",
        strokeColor : "#ffffff",
        highlightFill : "#ee7f49",
        highlightStroke : "#ffffff",
        data : [40,50,70,40]
      }
    ]

  } 
    

var ctx3 = document.getElementById("chart-area3").getContext("2d");


      
window.myPie = new Chart(ctx3).Bar(barChartData, {responsive:true});

</script>

    
<!-- --------------------------------------fin chart.js------------------------------------>  

    
<!-- --------------------------------------d3js.js------------------------------------> 

<script src="https://d3js.org/d3.v3.min.js"></script>
<script>

var margin = {top: 20, right: 120, bottom: 20, left: 120},
    width = 960 - margin.right - margin.left,
    height = 800 - margin.top - margin.bottom;

var i = 0,
    duration = 750,
    root;

var tree = d3.layout.tree()
    .size([height, width]);

var diagonal = d3.svg.diagonal()
    .projection(function(d) { return [d.y, d.x]; });

var svg = d3.select("body").append("svg")
    .attr("width", width + margin.right + margin.left)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

d3.json("http://bl.ocks.org/mbostock/raw/4063550/flare.json", function(error, flare) {
  if (error) throw error;

  root = flare;
  root.x0 = height / 2;
  root.y0 = 0;

  function collapse(d) {
    if (d.children) {
      d._children = d.children;
      d._children.forEach(collapse);
      d.children = null;
    }
  }

  root.children.forEach(collapse);
  update(root);
});

d3.select(self.frameElement).style("height", "800px");

function update(source) {

  // Compute the new tree layout.
  var nodes = tree.nodes(root).reverse(),
      links = tree.links(nodes);

  // Normalize for fixed-depth.
  nodes.forEach(function(d) { d.y = d.depth * 180; });

  // Update the nodes…
  var node = svg.selectAll("g.node")
      .data(nodes, function(d) { return d.id || (d.id = ++i); });

  // Enter any new nodes at the parent's previous position.
  var nodeEnter = node.enter().append("g")
      .attr("class", "node")
      .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
      .on("click", click);

  nodeEnter.append("circle")
      .attr("r", 1e-6)
      .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

  nodeEnter.append("text")
      .attr("x", function(d) { return d.children || d._children ? -10 : 10; })
      .attr("dy", ".35em")
      .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
      .text(function(d) { return d.name; })
      .style("fill-opacity", 1e-6);

  // Transition nodes to their new position.
  var nodeUpdate = node.transition()
      .duration(duration)
      .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

  nodeUpdate.select("circle")
      .attr("r", 4.5)
      .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

  nodeUpdate.select("text")
      .style("fill-opacity", 1);

  // Transition exiting nodes to the parent's new position.
  var nodeExit = node.exit().transition()
      .duration(duration)
      .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
      .remove();

  nodeExit.select("circle")
      .attr("r", 1e-6);

  nodeExit.select("text")
      .style("fill-opacity", 1e-6);

  // Update the links…
  var link = svg.selectAll("path.link")
      .data(links, function(d) { return d.target.id; });

  // Enter any new links at the parent's previous position.
  link.enter().insert("path", "g")
      .attr("class", "link")
      .attr("d", function(d) {
        var o = {x: source.x0, y: source.y0};
        return diagonal({source: o, target: o});
      });

  // Transition links to their new position.
  link.transition()
      .duration(duration)
      .attr("d", diagonal);

  // Transition exiting nodes to the parent's new position.
  link.exit().transition()
      .duration(duration)
      .attr("d", function(d) {
        var o = {x: source.x, y: source.y};
        return diagonal({source: o, target: o});
      })
      .remove();

  // Stash the old positions for transition.
  nodes.forEach(function(d) {
    d.x0 = d.x;
    d.y0 = d.y;
  });
}

// Toggle children on click.
function click(d) {
  if (d.children) {
    d._children = d.children;
    d.children = null;
  } else {
    d.children = d._children;
    d._children = null;
  }
  update(d);
}

</script>

<!-- --------------------------------------fin d3js.js------------------------------------> 
    
    </body>
</html>