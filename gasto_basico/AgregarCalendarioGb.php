<?php

session_start();
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/dao/PresupuestoAsignadoGbDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/beans/PresupuestoAsignadoGb.class.php");
header('Content-Type: text/html; charset=UTF-8'); 
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Calendario</title>
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
	<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
	<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
	<script>
	function OnlyNumber(value, elemnt){
			if( isNaN(value) ){
				elemnt.value = ""
			}
	}
	
	</script>
</head>
<body>
<div class="contenido">
<br/>
<p class="titulo_cat1">Gasto Básico > Asignación de Presupuesto </p>
<h2 class="titulo_cat2">
<?php
    $dao=new PresupuestoAsignadoGbDaoJdbc();
    $elemento = new PresupuestoAsignadoGb();

    if(isset($_GET['id']) && $_GET['id']!= null){
            echo(" ");
            $elemento = $dao->obtieneElemento($_GET['id']);
    }	
    else{
           echo(" ");
    }	
?>
</h2>

<form id="frmCalendario" name="frmCalendario" method="post" action="../src/mx/com/virreinato/web/CalendarioGb.php">
        <table width="65%" border="0" cellspacing="4" cellpadding="4" class='tb_add_cat' align='center'>
          <tr>

            <td class="SizeText" >
                Periodo*: 
            </td>
            <td colspan='5'>
                <select name="periodo" id="periodo">
                <option value='0'>Selecciona</option>		      
                <?php
                    $periodo=new PeriodoDaoJdbc();
                    $periodos=$periodo->obtieneListadoAbierto(); 
                    $p = new Periodo();

                    foreach($periodos as $p) {
                      $sel="";
                      if($elemento!= null && $elemento->getIdPeriodo()!= null ){
                        if($p->getId() == $elemento->getIdPeriodo()){
                              $sel="selected='selected'";
                        }	
                      }	  
                    echo("<option value='".$p->getId()."' ".$sel." >".$p->getPeriodo()."</option>");
                    }
                ?>
            </select>
            </td>
              </tr>

              <tr>
                 <td class="SizeText" width="30%" >Enero*:</td>
                 <td><input type="text" name="enero_Calendario" id="enero_Calendario" onkeyup="OnlyNumber(this.value,this)" size="15" maxlength="13" value="<?php if($elemento!=null && $elemento->getMontoEne()!=null) echo($elemento->getMontoEne());?>" /> </td>
                     <td class="SizeText" width="30%" >Febrero*:</td>
                     <td><input type="text" name="febrero_Calendario" id="febrero_Calendario" onkeyup="OnlyNumber(this.value,this)" size="15" maxlength="13" value="<?php if($elemento!=null && $elemento->getMontoFeb()!=null) echo($elemento->getMontoFeb());?>" /></td>
                 <td class="SizeText" >Marzo*:</td>
                 <td><input type="text" name="marzo_Calendario" id="marzo_Calendario" onkeyup="OnlyNumber(this.value,this)" size="15" maxlength="13" value="<?php if($elemento!=null && $elemento->getMontoMar()!=null) echo($elemento->getMontoMar());?>" /></td>
              </tr>

              <tr>

                 <td class="SizeText">Abril*:</td>
                 <td><input type="text" name="abril_Calendario" id="abril_Calendario" onkeyup="OnlyNumber(this.value,this)" size="15" maxlength="13" value="<?php if($elemento!=null && $elemento->getMontoAbr()!=null) echo($elemento->getMontoAbr());?>" /></td>
                     <td class="SizeText" >Mayo*:</td>
                     <td><input type="text" name="mayo_Calendario" id="mayo_Calendario" onkeyup="OnlyNumber(this.value,this)" size="15" maxlength="13" value="<?php if($elemento!=null && $elemento->getMontoMay()!=null) echo($elemento->getMontoMay());?>" /></td>
                 <td class="SizeText" >Junio*:</td>
                 <td><input type="text" name="junio_Calendario" id="junio_Calendario" onkeyup="OnlyNumber(this.value,this)" size="15" maxlength="13" value="<?php if($elemento!=null && $elemento->getMontoJun()!=null) echo($elemento->getMontoJun());?>" /></td>
              </tr>

              <tr>

                 <td class="SizeText" >Julio*:</td>
                 <td><input type="text" name="julio_Calendario" id="julio_Calendario" onkeyup="OnlyNumber(this.value,this)" size="15" maxlength="13" value="<?php if($elemento!=null && $elemento->getMontoJul()!=null) echo($elemento->getMontoJul());?>" /></td>
                     <td class="SizeText" >Agosto*:</td>
                     <td><input type="text" name="agosto_Calendario" id="agosto_Calendario" onkeyup="OnlyNumber(this.value,this)" size="15" maxlength="13" value="<?php if($elemento!=null && $elemento->getMontoAgo()!=null) echo($elemento->getMontoAgo());?>" /></td>
                 <td class="SizeText" >Septiembre*: </td>
                 <td><input type="text" name="septiembre_Calendario" id="septiembre_Calendario" onkeyup="OnlyNumber(this.value,this)" size="15" maxlength="13" value="<?php if($elemento!=null && $elemento->getMontoSep()!=null) echo($elemento->getMontoSep());?>" /></td>
              </tr>

              <tr>

                 <td class="SizeText" >Octubre*:</td>
                 <td><input type="text" name="octubre_Calendario" id="octubre_Calendario" onkeyup="OnlyNumber(this.value,this)" size="15" maxlength="13" value="<?php if($elemento!=null && $elemento->getMontoOct()!=null) echo($elemento->getMontoOct());?>" /></td>
                     <td class="SizeText" >Noviembre*: </td>
                     <td><input type="text" name="noviembre_Calendario" id="noviembre_Calendario" onkeyup="OnlyNumber(this.value,this)" size="15" maxlength="13" value="<?php if($elemento!=null && $elemento->getMontoNov()!=null) echo($elemento->getMontoNov());?>" /></td>
                 <td class="SizeText" >Diciembre*: </td>
                 <td><input type="text" name="diciembre_Calendario" id="diciembre_Calendario" onkeyup="OnlyNumber(this.value,this)" size="15" maxlength="13" value="<?php if($elemento!=null && $elemento->getMontoDic()!=null) echo($elemento->getMontoDic());?>" /></td>
              </tr>


              <tr>
                 <td align="center" colspan="6"><br/>
                 <input type="hidden" name="destino" value="GB"  />
                   <input name="guardar" style="cursor:pointer" type="button" value="Guardar" onclick="Validar()"  class='btn' />
                   &nbsp; &nbsp; &nbsp;
                   <input name="cancelar" style="cursor:pointer" type="button" value="Cancelar" onclick="Regresar()"  class='btn' />
                </td>
              </tr>

              <tr>
                <td><br/></td>
              </tr>
        </table>
        <?php 

            if($elemento!=null && $elemento->getId()!=null) echo("<input type='hidden' name='id' value='".$elemento->getId()."' />");
        ?>
	</form>
</div>
<br/><br/>
</body>
<script>

function Regresar(){
	window.location="lista_CalendarioGb.php";
}

function Validar(){
	var msj = "";
	if(  $("#periodo").val() == 0 ){
		msj = "Por favor seleccione el periodo.";
	}
	else if(  $("#enero_Calendario").val() == "" && $("#febrero_Calendario").val() == "" && $("#marzo_Calendario").val() == "" && $("#abril_Calendario").val() == "" && $("#mayo_Calendario").val() == "" && $("#junio_Calendario").val() == "" && $("#julio_Calendario").val() == "" && $("#agosto_Calendario").val() == "" && $("#septiembre_Calendario").val() == "" && $("#octubre_Calendario").val() == "" && $("#noviembre_Calendario").val() == "" && $("#diciembre_Calendario").val() == ""  ){
	   msj = "Por favor capture al menos un Mes del Calendario.";	
	}
	
	if(msj != ""){
	  alert(msj);
	}else{
		document.forms['frmCalendario'].submit();
	}
}
 
</script>
</html>