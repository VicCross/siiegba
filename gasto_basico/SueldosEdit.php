<?php

$perfil = null;
$user = null;

session_start();
include_once("../src/mx/com/virreinato/dao/SueldoGbDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/SueldoGB.class.php");
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/MinistraDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Ministra.class.php");
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}else{
    if(isset($_SESSION['id'])){ $perfil = $_SESSION['id'];}
    if(isset($_SESSION['user'])){ $user = $_SESSION['user'];}
}
?>

<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" type="text/css" href="../css/style.css" />
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<link href="../css/calendario.css" type="text/css" rel="stylesheet">
<script src="../js/calendar.js" type="text/javascript"></script>
<script src="../js/calendar-es.js" type="text/javascript"></script>
<script src="../js/calendar-setup.js" type="text/javascript"></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
<script language="JavaScript">
	
	var cheque = "";
	var idCh = "";
	var perfil;
	
		
	function Mostrar(value,name){
			$("#idProveedor").hide();
			$("#idEmpleado").hide();
			$("#otro").hide();
			$("#cat_proveedor").hide();
			$("#idProveedor").val(0);
			$("#idEmpleado").val(0);
			$("#otro").val("");
			cheque = value;
		
		  $("#"+value).show();		
	}
	function MostrarCampo(id){
		/* alert('id:'+id); */
		  document.getElementById(id).style.display='block';		
	}
	
	function MostrarTabla(id){
		/* alert('id:'+id); */
		  check= document.getElementById('catalogo_proveedor').checked;
		  if(check)
		  	document.getElementById(id).style.display='block';
		  else 			
		  document.getElementById(id).style.display='none';
	}
	function Regresar(){
		window.location="Sueldos.php";
	}
	
	function valida_fecha(dia,mes,anio){
		//alert('dia'+dia);
		//alert('mes'+mes);
		//alert('anio'+anio);
		if( isNaN(dia) || isNaN(mes) || isNaN(anio) ){
			//alert('no son números el mes el dia o el año');
			alert('El formato de la fecha es incorrecto, recuerde que el formato debe ser dd-mm-aaaa.');
			return false;	
		}
		else if( dia.length != 2 || mes.length !=2 || anio.length !=4 ){
			//alert('la longitud del dia mes o año no es la correcta');
			alert('El formato de la fecha es incorrecto, recuerde que el formato debe ser dd-mm-aaaa.');
			return false;	
		}
		else if( mes > 12 ){
			//alert('el mes no es válido, es mayor a 12');
			alert('El formato de la fecha es incorrecto, recuerde que el formato debe ser dd-mm-aaaa.');
			return false;	
		}
		else if( dia > 31 ){
			//alert('el dia no es válido, es mayor a 31');
			alert('El formato de la fecha es incorrecto, recuerde que el formato debe ser dd-mm-aaaa.');
			return false;	
		}
		else if( dia == 31 && (mes!= 1 && mes!= 3 && mes!= 5 && mes!= 7 &&  mes!= 8 &&  mes!= 10 &&  mes!= 12 ) ){
			//alert('el dia no es válido porq este mes no tiene 31 dias');
			alert('El formato de la fecha es incorrecto, recuerde que el formato debe ser dd-mm-aaaa.');
			return false;	
		}
		else if( mes==2 && dia== 30 ) {
			//alert('el dia no es válido porq febrero no tiene 30 dias');
			alert('El formato de la fecha es incorrecto, recuerde que el formato debe ser dd-mm-aaaa.');
			return false;	
		}
		else if( mes==2 && dia== 29 && (anio%4)!=0){
			//alert('el dia no es válido porq el año no es bisiesto');
			alert('El formato de la fecha es incorrecto, recuerde que el formato debe ser dd-mm-aaaa.');
			return false;	
		}
		else{
			return true;	
		}
 	}
	function Validar(){
	
		var mifecha=$("#fecha").val();
		var amd=mifecha.split('-');
		
		if	(amd.length <= 0 ){
				amd=mifecha.split('/');
		}
			
		
					
		if( $("#fecha").val() == "" ){
			alert("Por favor ingrese la fecha de solicitud.");
			$("#fecha").focus();
			return false;
		}
		else if( !valida_fecha(amd[0],amd[1],amd[2]) ){
				return false; 
		}
		else if( $("#idMinistracion").val() == "0"){
				 alert("Por favor seleccione la ministración.");
				 $("#idMinistracion").focus();
				 return false;
		}
		else if( $("#tipoNomina").val() == "0"){
				 alert("Por favor seleccione el tipo de nómina.");
				 $("#tipoNomina").focus();
				 return false;
		}
		else if( $("#quincena").val() == "0"){
				 alert("Por favor seleccione la quincena.");
				 $("#quincena").focus();
				 return false;
		}
		else{
			
			document.frmSueldos.submit();
			return true;
		}	
	}
	
	
	function obtenerMetas(sel){
		var idProyecto = $("#idProyecto").val();
		perfil = '<?php echo$perfil?>';
		
		if(perfil == '7'){
			var user='<?php echo$user?>';
			$.ajax({
				url: "Presupuesto.do",
				type: "POST",
				data: "obtieneMetas="+idProyecto+"&sel="+sel+"&idUsuario="+user,
				success: function(data){ $("#idMeta").html(data); }
			});
		}else{
			$.ajax({
				url: "Presupuesto.do",
				type: "POST",
				data: "obtieneMetas="+idProyecto+"&sel="+sel,
				success: function(data){ $("#idMeta").html(data); }
			});
		}
	}
	
	
		
</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Editar Sueldo</title>
</head>
<body>
<?php 
    $respuesta = null;
    $error = null;
    $id = null;
    
   if(isset($_GET["respuesta"]))
        {$respuesta= (String) $_GET["respuesta"];}
   if(isset($_GET["error"]))
        {$error = (String) $_GET["error"];}
?>
<div class="contenido">
<br/>
<p class="titulo_cat1">Gasto Básico > <a class="linkTitulo_cat1" href="Solicitud.php" >Agregar Sueldos</a> </p>
<h2 class="titulo_cat2">
<?php

    $dao = new SueldoGbDaoJdbc();
    $p = new SueldoGb();

    if(isset($_GET["idSueldo"])){
        $id = (String)$_GET["idSueldo"];
        echo("Sueldos");
        $p = $dao->obtieneElemento($id);
		 //out.println(id);
    }else if(isset($_GET["id"])){
        $id = (String)$_GET["id"];
        echo("");
        $p = $dao->obtieneElemento($id);

    }else{
            echo("");

    }	
?>
</h2>
<br/>
<?php 
     if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>");
     if($error!=null) echo("<div align='center' class='msj'>".$error."</div>");
?>

<form name="frmSueldos" id="frmSueldos" method="POST" action="../src/mx/com/virreinato/web/WebSueldos.php">
    <table width="48%" border="0" cellspacing="0" cellpadding="5" class='tb_add_cat' align='center'>
    <tr class="SizeText">
        <td width='30%'>Período*:</td>
        <td > <select name="periodo" id="periodo" style="width: 110px" onchange="obtenerMinistra();">
             <?php 

                $periodo=new PeriodoDaoJdbc();
                $periodos=$periodo->obtieneListadoAbierto(); 
                $per = new Periodo();

                foreach($periodos as $per) {
                  $sel="";
                  if($p!= null && $p->getIdPeriodo()!= null ){
                          if($per->getId() == (int)($p->getIdPeriodo())){
                                $sel="selected='selected'";
                          }	

                  }	  
                  echo("<option value='".$per->getId()."' ".$sel." >".$per->getPeriodo()."</option>");
                }
            ?>
            </select>
                </td>
                </tr>
                <tr>
                    <td>Fecha(dd-mm-aaaa)*:</td>
                    <td><input type="text" name="fecha" id="fecha" size="20" value='<?php if($p != null && $p->getFecha()!= null){ echo(date("d-m-Y",strtotime($p->getFecha()))); } ?>' /></td>
                </tr>	
                <tr>
                <?php
                    $daoMin=new MinistraDaoJdbc();
                    $listada=$daoMin->obtieneListadoDestinoPer("SL","2013");
                    $elementoLista=new Ministra();
                ?>
                <td>Ministración*:</td>
                <td><select name="idMinistracion" id="idMinistracion" style="width:160px" >
                 <option value="0"> Selecciona  </option>
                 <?php
				
                    foreach($listada as $elementoLista){

                        $sel = "";
                        if($p != null && $p->getIdMinistracion()!= null ){
                            if($elementoLista->getId() == $p->getIdMinistracion()){ $sel = "selected='selected'"; }
                        }
                        echo("<option value=".$elementoLista->getId()." ".$sel." >".$elementoLista->getDescripcion()." - ".$elementoLista->getMonto()." </option>");
                    }			  
                ?> 
                 </select>
                </td>
                </tr>
                <tr>
                <td>Tipo de Nómina*:</td>
                <td>
                <select id="tipoNomina" name="tipoNomina" style="width:160px">
                <option value="0"> Selecciona  </option>

                <?php if(  $p->getTipoNomina()!= null &&  $p->getTipoNomina() == "ATM" ){?>
                        <option value="ATM" selected="selected">  ATM </option>
                <?php }else{?>
                <option value="ATM">  ATM </option>
                <?php }if(  $p->getTipoNomina()!= null &&  $p->getTipoNomina() == "Apoyo a confianza" ){?>
                <option value="Apoyo a confianza" selected="selected">Apoyo a confianza</option>
                <?php }else{?>
                <option value="Apoyo a confianza">Apoyo a confianza</option>
                <?php }if(  $p->getTipoNomina()!= null &&  $p->getTipoNomina() == "Mandos medios" ){?>
                        <option value="Mandos medios" selected="selected">Mandos medios</option>
                <?php }else{?>
                <option value="Mandos medios">Mandos medios</option>
                <?php }if(  $p->getTipoNomina()!= null &&  $p->getTipoNomina() == "Compactados" ){?>
                <option value="Compactados" selected="selected">Compactados</option>
                <?php }else{?>
                <option value="Compactados">Compactados</option>
                <?php }if(  $p->getTipoNomina()!= null &&  $p->getTipoNomina() == "Pensión alimenticia" ){?>
                        <option value="Pensión alimenticia" selected="selected">Pensión alimenticia</option>
                <?php }else{?>
                <option value="Pensión alimenticia">Pensión alimenticia</option>
                        <?php } ?>
                </select>
                </td>
                </tr>
                <tr>
                <td>Quincena*:</td>
                    <td>
                    <select id="quincena" name="quincena" style="width:160px">
                    <option value="0"> Selecciona  </option>
                    <?php if(  $p->getQuincena()!= null && $p->getQuincena() == "Primera"){ ?>
                                <option value="Primera" selected="selected" >  Primera </option>
                        <?php }else{ ?>
                        <option value="Primera"  >  Primera </option>

                        <?php }if( $p->getQuincena()!= null && $p->getQuincena() == "Segunda" ){?>
                    <option value="Segunda" selected="selected">Segunda</option>
                    <?php }else{?>
                    <option value="Segunda">Segunda</option>
                    <?php }?>	
                    </select>
                    </td>
		</tr>
		
		<tr>
                <td align="center" colspan="3">
                    <input name="guardar" style="cursor:pointer" type="button" onClick = "Validar()" value="Continuar"  class='btn' />
                    &nbsp; &nbsp; &nbsp;
                    <input name="cancelar" type="button" style="cursor:pointer"  onclick="Regresar()" value="Regresar"  class='btn' />
	        </td>
	    </tr>
            </table>
	   <?php if($p!= null && $p->getId()!= null) echo("<input type='hidden' name='id' id='id' value='".$p->getId()."' />"); ?>
</form>
</div>
<br/><br/>
<script>
Calendar.setup({ 
  inputField : "fecha", ifFormat :"%d-%m-%Y", button :"fecha"      
});
</script>	 
</body>
</html>	 