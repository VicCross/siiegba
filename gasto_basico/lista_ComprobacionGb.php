<?php

session_start();
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/ComprobacionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Comprobacion.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html>

<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link href="../css/calendario.css" type="text/css" rel="stylesheet">
<script src="../js/calendar.js" type="text/javascript"></script>
<script src="../js/calendar-es.js" type="text/javascript"></script>
<script src="../js/calendar-setup.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="../css/lista.css">
        <script type="text/javascript" src="../media/js/complete.js"></script>
        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
        <script type="text/javascript" src="../js/lista.js"></script>

			   <input type="hidden" id="columnas_visibles" name="columnas_visibles" value="6"/>
    <input type="hidden" id="total_columnas" name="total_columnas" value="9"/>
		



<title>Comprobacion</title>
</head>
<body>
<?php 
    $respuesta = null;
    
   if(isset($_GET["respuesta"]))
        {$respuesta= (String) $_GET["respuesta"];}
        
   $dia = (String)(date('d'));
   $mes = (String)(date('m'));
   $mesInicio= "01";
    $diaInicio="01";
     /*if(date('n')>1){
            $mesInicio= (String)( date('n') - 1 ); if(strlen($mesInicio) == 1){ $mesInicio = "0".$mesInicio; }

    }
    else{
            $diaInicio="01";
    } */
?>
<div class="contenido">

<br/>
<p class="titulo_cat1">Gasto Básico > Comprobaci&oacute;n Museo</p>

 <?php
        if (isset($_GET['recargado']) && $_GET['recargado'] == "1") {
            echo '<input type="hidden" id="nombre_form" name="nombre_form" value=""/>';
        } else {
            echo '<input type="hidden" id="nombre_form" name="nombre_form" value="frmFiltroComprobacion"/>';
        }
        ?>
<h2 class="titulo_cat2""> </h2>

<p class="titulo_cat1">
<blockquote>
    <blockquote>
        <blockquote>
            <form name="frmFiltroComprobacion" id="frmFiltroComprobacion" method="POST" action="../src/mx/com/virreinato/web/CatComprobacionGb.php" >
            &nbsp; &nbsp; Fecha de Inicio: <input type="text" size="10" name="inicio" id="inicioFecha" maxlength="20" value=" <?php  if(isset($_GET["inicio"]) && $_GET["inicio"] !=null ){ echo( $_GET["inicio"]); } ?>"  >
            Fecha de Fin: <input type="text" size="10" name="fin" id="finFecha" maxlength="20" value=" <?php  if(isset($_GET["fin"]) && $_GET["fin"]!=null ){ echo( $_GET["fin"] ); }  else { echo($dia."-".$mes."-".date('Y'));} ?>"  >
            <?php 

                $aux = $diaInicio;

                $inicio =  $aux."-".$mesInicio."-".date('Y');
                $fin = $dia."-".$mes."-".date('Y');
                $periodo = "";
            ?> 
            &nbsp; Período: 
            <select name="periodo" id="periodo" style="width:110px" > 
            <?php
                $daoPer=new PeriodoDaoJdbc();
                $listaPer = $daoPer->obtieneListado();
                $elementoPer = new Periodo();

                foreach($listaPer as $elementoPer){

                    $sel = "";
                    if(isset($_GET["periodo"]) && $_GET['periodo']!=null && (int)( $_GET["periodo" ] ) == $elementoPer->getId()){ $sel = "selected='selected'"; }
                    else if( $_GET["periodo"] == null && (int)($elementoPer->getPeriodo()) == date('Y')  ){ $sel = "selected='selected'";  $periodo = (String)($elementoPer->getId() ); }
                    echo("<option value=".$elementoPer->getId()." ".$sel." >".$elementoPer->getPeriodo()."</option>");
                }
            ?>
            </select>
            &nbsp; <input name="filtro" style="cursor:pointer" type="submit" value="Filtrar"  class='btn' />
            </form>
        </blockquote>
    </blockquote>	   
</blockquote>
<div align="right"><a href='Agregar_ComprobacionGb.php' class='liga_btn'> Agregar Nueva Comprobación</a></div>
<br></br>
<?php
    if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>");
    if(isset($_GET['inicio']) && $_GET["inicio"] !=null ){
       $inicio = ( String )$_GET["inicio"];
       $fin = (String)$_GET["fin"];
    }
    if(isset($_GET['periodo']) && $_GET['periodo'] != null ) { $periodo = (String)$_GET['periodo'];  }
        else {
	      	$aniosel = (String)(date('Y'));
	      	$per=new PeriodoDaoJdbc();
	      	
                $periodo=$per->obtieneidElemento($aniosel);
	      }
?>

<table border="0" cellspacing="0" cellpadding="5" class='dataTable' align='center' >
	
	<thead>
	<tr >
            <th align="center">Número<br>comprobaci&oacute;n</th>
            <th align="center">N&uacute;mero de<br>cheque</th>
            <th align="center">Fecha</th>
            <th align="center">A nombre de:</th>
            <th align="center">Monto total</th>
            <th align="center">Estatus</th>
            <th width="2%" align="center"></th>
            <th width="2%" align="center"></th>
            <th width="2%" align="center"></th>			
        </tr>
		</thead>
		<tfoot>
		<tr >
            <th align="center"> </th>
            <th align="center"> </th>
            <th align="center"> </th>
            <th align="center"> </th>
            <th align="center"> </th>
            <th align="center"></th>
            <th width="2%" align="center"></th>
            <th width="2%" align="center"></th>
            <th width="2%" align="center"></th>			
        </tr>
		
		
		</tfoot>
		<tbody>
        
		
		<?php  
            $dao= new ComprobacionDaoJdbc();
            $lista=$dao->obtieneListadoDestino2($inicio,$fin,$periodo,"GB");
            $elemento = new Comprobacion();
            
            $beneficiario = "";

            $estatus= "";

            foreach($lista as $elemento){

                if ( $elemento->getEmpleado()!=null && $elemento->getEmpleado() != "null" && $elemento->getEmpleado() != "0" && $elemento->getEmpleado() != "" ){  
                    $beneficiario =$elemento->getEmpleado();
                }
                else if ($elemento->getProveedor()!= null && $elemento->getProveedor() != "null" && $elemento->getProveedor() != "0" && $elemento->getProveedor() != ""){   
                    $beneficiario = $elemento->getProveedor();	
                }
                else if ($elemento->getBeneficiario()!=null && $elemento->getBeneficiario() != "null" && $elemento->getBeneficiario() != "0" && $elemento->getBeneficiario() != ""){  
                    $beneficiario = $elemento->getBeneficiario();
                }

                if ($elemento->getEstatus()!= null){
                        if($elemento->getEstatus() == "P")
                        {$estatus = "Pendiente"; }
                        else{
                            $estatus = "Completo";
                        }
                }
        ?>
        <tr class="SizeText">
            <td align="center"><?php echo($elemento->getFolio());?></td>
            <td align="center"><?php echo($elemento->getFolioCheque());?></td>
            <td align="center"><?php echo( date("d-m-Y",strtotime( $elemento->getFecha())));?></td>
            <td align="center"><?php echo($beneficiario);?></td>
            <td align="center"><?php  echo( "$".number_format($elemento->getMontoComprobacion(),2));?></td>
            <td align="center"><?php echo$estatus; ?>
            <td><a href='Agregar_ComprobacionGb.php?id=<?php echo($elemento->getIdComprobacion());?>&periodo=<?php echo($elemento->getIdPeriodo());?>' class='liga_cat'><acronym title="Editar"><img src="../img/Pencil3.png" width="16" height="16" alt="Editar" style="border:0;" /></acronym> </a></td>
        <td><a href='../src/mx/com/virreinato/web/CatComprobacionGb.php?id=<?php echo($elemento->getIdComprobacion());?>' class='liga_cat' onclick='return confirm("¿Esta seguro que desea eliminar este registro?")'><acronym title="Borrar"><img src="../img/DeleteRed.png" width="16" height="16" alt="Borrar" style="border:0;" /></acronym></a></td>
        <td><a style="cursor:pointer" class='liga_cat' onclick="Imprimir('../Formatos/Comprobantes.php','<?php echo$elemento->getIdComprobacion(); ?>')"><acronym title="Imprimir"><img src="../img/printer.png" width="18" height="18" alt="Imprimir" style="border:0;" /></acronym></a></td>
        </tr>
        <?php } ?>
		</tbody>
        </table>
	<br>
	
</div>
<br/><br/>
</body>
<script language="JavaScript">
   Calendar.setup({ inputField :"finFecha", ifFormat : "%d-%m-%Y", button:"finFecha" });
    
   Calendar.setup({ inputField : "inicioFecha", ifFormat : "%d-%m-%Y", button:"inicioFecha" });
   
   var ini = '<?php echo$inicio?>';
   var fin = '<?php echo$fin?>';
   $("#inicioFecha").val(ini);
   $("#finFecha").val(fin);
   
   function Imprimir(pagina,folio) 
 	{
		//alert(pagina)
		var dir = pagina + "?folio=" + folio;
		var left = Math.floor((screen.width - 980) / 2);
		var opciones = 'titlebar=0, menubar=0, toolbar=0, location=0, directories=0, status=0, scrollbars=0, resizable=0, width=980, height=750,top=50,left='
				+ left + '';
		window.open(dir, "", opciones);
	}
</script>	
</html>