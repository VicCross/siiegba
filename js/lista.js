$(document).ready(function () {
    var espanol = {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ning\u00fan dato disponible en esta tabla",
        "sInfo": "Mostrando de _START_ a _END_ de  _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 registros",
        "sInfoFiltered": "(filtrado de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst": "Primero",
            "sLast": "\u00daltimo",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    };

    var columnas_visibles = 0;
    var columnas_totals = 0;
    if($("#columnas_visibles").length && $("#total_columnas").length){//Si los campos de # de columnas estan definidas
        columnas_visibles = parseInt($("#columnas_visibles").val());
        columnas_totals = parseInt($("#total_columnas").val());
    }
    
    var configuracion = [];
    for(var i=0;i<columnas_visibles;i++){        
        configuracion.push({ type: "text" });
    }
    for(var i=5;i<columnas_totals;i++){
        configuracion.push(null);
    }        

    if ($("#no_sort").length && $("#no_sort").val() == "1") {
        $('.dataTable').dataTable({
            "bJQueryUI": true,
            "bRetrieve": true,
            "bDestroy": true,
            "oLanguage": espanol,
            "sPaginationType": "full_numbers",
            "bDeferRender": true,
            "iDisplayLength": 100, // antes tenia 10, 
            "bSort": false
        }).columnFilter({
            aoColumns: configuracion
        });
    } else {
        $('.dataTable').dataTable({
            "bJQueryUI": true,
            "bRetrieve": true,
            "bDestroy": true,
            "oLanguage": espanol,
            "sPaginationType": "full_numbers",
            "bDeferRender": true,
            "iDisplayLength": 100, // antes tenia 10, 
            "aaSorting": [[0, "desc"]]
        }).columnFilter({
            aoColumns: configuracion
        });
    }

    if ($("#parametro_color").length) {
        var color = $("#parametro_color").val();
        $("#Perfil").css({
            background: color
        });
        $(".div_menu").css({
            background: color
        });
        $(".tb_presupuesto th").css({
            background: color
        });
        $(".tb_presupuestoResumen th").css({
            background: color
        });
        $(".tb_cat th").css({
            background: color
        });
        $(".tb_add_cat").css({
            background: color
        });
    }

    if ($("#nombre_form").length && $("#nombre_form").val() !== "") {//Si se especifico un nombre de form, se hará un submit en automático al cargar la página
        $("#nombre_form").hide();
        var nombre_form = $("#nombre_form").val();
        $("#" + nombre_form).submit();
    }
});