<?php

session_start();
include_once("../src/mx/com/virreinato/dao/TarjetaPersonalDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/TarjetaPersonal.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Calendario</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
    <script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
    <script>
   $(document).ready(function(){
   	if( $("#Asignado").val() != "" ){
         if( $.trim( $("#Asignado").val() ) == $.trim("Externo") ){
         	   $("#externo_personal").attr("checked", true);
         	}
         else if( $.trim( $("#Asignado").val() ) == $.trim("Proyecto") ){
         	   $("#proyecto_personal").attr("checked", true);
         	}
        else if( $.trim( $("#Asignado").val() ) == $.trim("INAH") ){
        	    $("#tipo_personal").attr("checked", true);
        	} 
      }
  	});	
	
	function OnlyNumber(value, elemnt){
			if( isNaN(value) ){
				elemnt.value = ""
			}
	}
	
	function Personal(value){
   	  $("#Asignado").val(value);
   }
    </script>
	<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<?php 
    $idTarjeta = null;

   if(isset($_GET["idTarjeta"]))
        {$idTarjeta= (String) $_GET["idTarjeta"];}
?>
<body>
<div class="contenido">
<br/>
<p class="titulo_cat1">Proyectos >  Información de Proyectos >  <a class="linkTitulo_cat1" href="TarjetaRegistroMod.php?idTarjeta=<?php echo($idTarjeta); ?>"> Tarjeta de Registro </a> > <a class="linkTitulo_cat1" href="lista_Personal.php?idTarjeta=<?php echo($idTarjeta);?>"> Personal </a> </p>
<h2 class="titulo_cat2">
<?php
    $dao=new TarjetaPersonalDaoJdbc();
    $elemento = new TarjetaPersonal();
	
    if(isset($_GET['id'])){
        echo("Modificar Personal del Proyecto");
        $elemento = $dao->obtieneElemento($_GET['id']);
    }	
    else{
        echo("Alta del Personal del Proyecto");
    }	
?>
</h2>

    <form id="frmPersonal" name="frmPersonal" method="post" action="../src/mx/com/virreinato/web/CatTarjetaPersonal.php">
        <table width="35%" border="0" cellspacing="0" cellpadding="0" class='tb_add_cat' align='center'>
            <tr>
		  	       
                <td class="SizeText">
		        
                    <br/>&nbsp; &nbsp; &nbsp; No. Personal*: <input type="text" name="numero_personal" id="numero_personal" onkeyup="OnlyNumber(this.value,this)" size="5" maxlength="4"  value="<?php if($elemento!=null && $elemento->getNum_Personal()!=null) echo($elemento->getNum_Personal()); ?>"  />

                    <br/><br/>&nbsp; &nbsp; &nbsp; Nombre*: <input type="text" name="nombre_personal" id="nombre_personal" size="30" maxlength="255" value="<?php if($elemento!=null && $elemento->getNombre()!=null) echo($elemento->getNombre()); ?>" />

                    <br/><br/>&nbsp; &nbsp; &nbsp;Grado Académico*: <input type="text" name="grado_personal" id="grado_personal" size="30" maxlength="255" value="<?php if($elemento!=null && $elemento->getGrado()!=null) echo($elemento->getGrado()); ?>" />

                            <br/><br/>&nbsp; &nbsp; &nbsp; Puesto y Categoría*: <input type="text" name="puesto_personal" id="puesto_personal" size="30" maxlength="255" value="<?php if($elemento!=null && $elemento->getPuesto()!=null) echo($elemento->getPuesto()); ?>" />

                            <br/><br/>&nbsp; &nbsp; &nbsp; Instituciones Externas*: <input type="text" name="instituciones_personal" id="instituciones_personal" size="30" maxlength="255" value="<?php if($elemento!=null && $elemento->getInstituciones()!=null) echo($elemento->getInstituciones()); ?>" />

                    <br/><br/>&nbsp; &nbsp; &nbsp; Asignado por:
                    <br/>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Por el INAH*: &nbsp;&nbsp;&nbsp;&nbsp; <input type="radio" onclick="Personal(this.value)" value="INAH"  name="tipo_personal" id="inah_personal"/>
                            <br/>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Por el Proyecto*: <input type="radio" name="tipo_personal" id="proyecto_personal" onclick="Personal(this.value)" value="Proyecto"  />
                    <br/>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Externo*:&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="radio" name="tipo_personal" id="externo_personal" onclick="Personal(this.value)" value="Externo" />

                </td>
            </tr>
		  
		    
            <tr>
                <td style="display:none" > <input type="text" name="Asignado" id="Asignado" value="<?php if($elemento!=null && $elemento->getAsignado()!=null) echo($elemento->getAsignado()); ?>" /> </td>		  
		  
                <td align="center" colspan="6"><br/>
                <input name="guardar" style="cursor:pointer" type="submit" value="Guardar"  class='btn' />
                &nbsp; &nbsp;
                <input name="cancelar" style="cursor:pointer" type="button" onclick="Regresar()" value="Cancelar"  class='btn' />
                <br/><br/>
                </td>
            </tr>
		  
        </table>
        <?php
             echo("<input type='hidden' name='id_Tarjeta' value='".$idTarjeta."' /> ");
             if( $elemento!=null && $elemento->getIdPersonal()!=null  ) echo("<input type='hidden' name='id' value='".$elemento->getIdPersonal()."'  />");
	  ?>
    </form>
</div>
<br/><br/>
</body>
<script>
  
function Regresar(){
    var tarjeta = '<?php echo$idTarjeta ?>'
    window.location="lista_Personal.php?idTarjeta="+tarjeta;
}
 
 var frmvalidator  = new Validator("frmPersonal");
 frmvalidator.addValidation("nombre_personal","req","Por favor capture el nombre del Personal.");
</script>
</html>
