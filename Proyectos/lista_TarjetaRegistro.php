<?php

$id = null;

session_start();
include_once("../src/mx/com/virreinato/dao/TarjetaRegistroDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/TarjetaRegistro.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}else{
    if(isset($_SESSION['id'])){ $id = $_SESSION['id'];}
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<link rel="stylesheet" type="text/css" href="../css/pro_dop_1.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
	function Imprimir(pagina,folio) {
		var dir = pagina + "?folio=" + folio;
		var left = Math.floor((screen.width - 980) / 2);
		var opciones = 'titlebar=0, menubar=0, toolbar=0, location=0, directories=0, status=0, scrollbars=0, resizable=0, width=980, height=750,top=50,left='
				+ left + '';
		window.open(dir, "", opciones);
	}
	
	function Change(){
		document.forms["frmFiltroTarjeta"].submit();
		
	}
</script>
<script>
	$(document).ready(function() {
		$(".menu ul").css({
			background: "<?php echo $parametro->getValor(); ?>"
		});
		$("#.menu a").css({
			background: "<?php echo $parametro->getValor(); ?>"
		});
	});
</script>
<!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="../css/lista.css">
        <script type="text/javascript" src="../media/js/complete.js"></script>
        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
        <script type="text/javascript" src="../js/lista.js"></script>
		
    <input type="hidden" id="columnas_visibles" name="columnas_visibles" value=" 2"/>
    <input type="hidden" id="total_columnas" name="total_columnas" value="5"/>
		

</head>
<body>
<?php 
    $respuesta = null;
    $idCarta = null;
    $seleccionado = null;
    $anio = null;
    
   if(isset($_GET["respuesta"]))
        {$respuesta= (String) $_GET["respuesta"];}
   if(isset($_GET["idCarta"]))
        {$idCarta= (String) $_GET["idCarta"];}
?>

<div class="contenido">
    <br/>
    <p class="titulo_cat1">Proyectos > Información de Proyectos > Tarjeta de Registro</p>
    <h2 class="titulo_cat2"> </h2>
    <?php 
        if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>");
    ?>
    <form name="frmFiltroTarjeta" id="frmFiltroTarjeta" method="POST" action="../src/mx/com/virreinato/web/CatTarjetaRegistro.php" >
        <p class="titulo_cat1"> &nbsp; &nbsp; &nbsp; &nbsp; Selecciona el Año:
          <select id="AnioTarjeta" name="AnioTarjeta" onchange="Change()">
              <?php 
	          $indice = 2000;
	          for($indice = 2000 ; $indice <= (int)date('Y'); $indice++ ){
                    $sel = "";
                    if(isset($_GET['anioTarjeta']) && (int)($_GET['anioTarjeta']) == $indice ){ $sel = "selected='selected'"; $anio = $_GET['anioTarjeta'];  }
                    else if( $anio == null && $indice == (int)date('Y')){ $sel = "selected='selected'"; $anio =(String)($indice);  }
                    echo("<option value='".$indice."' ".$sel." >".$indice."&nbsp; &nbsp; </option>"); 
 	          }
 	          $seleccionado = (String)(date('Y'));
	       ?>
        </select>
                        
        </p>
        <div align="right"><a href='TarjetaRegistroMod.php' class='liga_btn'> Agregar Tarjeta </a></div>
        <br> </br>
    </form>
    
        <div id="lista_Tarjetas" >
	   <table  border="0" cellspacing="0" cellpadding="5" class='dataTable' align='center' >
<thead>           
		   <tr class="SizeText">
                <th width="40%">Tarjeta de Registro</th>
                <th width="40%">Responsable</th>
                <th width="2%"></th>
                <th width="2%"></th>
                <th width="2%"></th>
            </tr>
			</thead>           
			<tfoot>
			<tr class="SizeText">
                <th width="40%"></th>
                <th width="40%"></th>
                <th width="2%"></th>
                <th width="2%"></th>
                <th width="2%"></th>
            </tr>
			</tfoot>
			<tbody>
            <?php
                $dao=new TarjetaRegistroDaoJdbc();
                $lista=$dao->obtieneListado( $anio );
                $elemento = new TarjetaRegistro();
                
                foreach($lista as $elemento){

            ?>
             <tr class="SizeText">
                <td align="center" ><?php echo($elemento->getNombreProyecto());?></td>
                <td align="center" ><?php echo($elemento->getResponsable());?></td>
                <td><a href='TarjetaRegistroMod.php?idTarjeta=<?php echo($elemento->getId());?>' class='liga_cat'><acronym title="Editar"><img src="../img/Pencil3.png" width="16" height="16" alt="Editar" style="border:0;" /></acronym></a></td>
                <td><a href='../src/mx/com/virreinato/web/CatTarjetaRegistro.php?id=<?php echo($elemento->getId());?>' class='liga_cat'><acronym title="Borrar" onclick='return confirm("ï¿½Esta seguro que desea eliminar este registro?")'><img src="../img/DeleteRed.png" width="16" height="16" alt="Borrar" style="border:0;" /></acronym> </a></td>
                <td><a href='#' class='liga_cat' onclick="Imprimir('../Formatos/ImprimirTarjeta.php',<?php echo($elemento->getId());?>)" ><acronym title="Imprimir"><img src="../img/printer.png" width="18" height="18" alt="Imprimir" style="border:0;" /></acronym></a></td>
            </tr>	
			    
            <?php } ?>
			</tbody>
            </table>
	</div>
	<br>
	
</div>
<br/><br/>
</body>
</html>