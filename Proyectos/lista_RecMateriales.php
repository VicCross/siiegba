<?php

session_start();
include_once("../src/mx/com/virreinato/dao/RecMaterialDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/RecMateriales.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<link rel="stylesheet" type="text/css" href="../css/pro_dop_1.css">
<script>
	function Eliminar(idRecurso){
		alert("Registro Eliminado!");
	}
</script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>

<!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="../css/lista.css">
        <script type="text/javascript" src="../media/js/complete.js"></script>
        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
        <script type="text/javascript" src="../js/lista.js"></script>
		
			   <input type="hidden" id="columnas_visibles" name="columnas_visibles" value="2"/>
    <input type="hidden" id="total_columnas" name="total_columnas" value="4"/>


</head>
<body>
<?php 
    $respuesta = null;
    $idCarta = null;
    
   if(isset($_GET["respuesta"]))
        {$respuesta= (String) $_GET["respuesta"];}
   if(isset($_GET["idCarta"]))
        {$idCarta= (String) $_GET["idCarta"];}
?>
<div class="contenido">
    <br/>
    <p class="titulo_cat1">Proyectos > Información de Proyectos > <a class="linkTitulo_cat1" href="CartaConsAdd.php?idCarta=<?php echo($idCarta); ?>" >Carta Constitutiva de Proyectos</a> > Recursos Materiales </p>
    <h2 class="titulo_cat2"></h2>
    <?php
        if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>");
    ?>  
    <div align="right"><a href='RecMaterial_Carta.php?idCarta=<?php echo($idCarta);?>' class='liga_btn'> Agregar Nuevo Recurso</a></div>
    <br> </br>
    <table  border="0" cellspacing="0" cellpadding="5" class=' dataTable' align='center' >
        <thead>
		<tr>
          <th width="45%">Recurso</th>
          <th width="10%">Costo Total</th>
          <th width="2%"></th>
          <th width="2%"></th>
        </tr>
		</thead>
		<tfoot>
		<tr>
          <th width="45%"></th>
          <th width="10%"></th>
          <th width="2%"></th>
          <th width="2%"></th>
        </tr>
		</tfoot>	  
		<tbody>
    <?php 	  		 	     
        $dao=new RecMaterialDaoJdbc();
        $lista=$dao->obtieneListado( $idCarta );
        $elemento = new RecMateriales();

        foreach($lista as $elemento){

    ?>
      <tr>
        <td align='center'><?php echo($elemento->getRecurso());?></td>
        <td align='center'><?php echo( number_format(  $elemento->getCosto(),2));?></td>
        <td><a href='RecMaterial_Carta.php?id=<?php echo($elemento->getId()); ?>&idCarta=<?php echo($idCarta);?>' class='liga_cat'><acronym title="Editar"><img src="../img/Pencil3.png" width="16" height="16" alt="Editar" style="border:0;" /></acronym> </a></td>
        <td><a href='../src/mx/com/virreinato/web/CatRecMaterial.php?id=<?php echo($elemento->getId()); ?>&idCarta=<?php echo($idCarta); ?>' class='liga_cat' ><acronym title="Borrar"><img src="../img/DeleteRed.png" width="16" height="16" alt="Borrar" style="border:0;" /></acronym></a></td>
      </tr>	
					
    <?php  } ?>
			</tbody>
    </table>
    <br>
    <div align="center">
         <a href='CartaConsAdd.php?idCarta=<?php echo($idCarta);?>' class='liga_btn'> Regresar </a>
         &nbsp; &nbsp; &nbsp;
         
    </div>
</div>
<br/><br/>
</body>
</html>