<?php

session_start();
include_once("../src/mx/com/virreinato/dao/CatCartaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatCarta.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html>

<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="../css/lista.css">
        <script type="text/javascript" src="../media/js/complete.js"></script>
        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
        <script type="text/javascript" src="../js/lista.js"></script>

<script language="JavaScript">


	function Imprimir(pagina,folio) {
		var dir = pagina + "?folio=" + folio;
		var left = Math.floor((screen.width - 980) / 2);
		var opciones = 'titlebar=0, menubar=0, toolbar=0, location=0, directories=0, status=0, scrollbars=0, resizable=0, width=980, height=750,top=50,left='
				+ left + '';
		window.open(dir, "", opciones);
	}
	function CambiarPeriodo(){
		  $.ajax({
			url:  "DetCarta.php",
			type: "POST",
			data: "Modificado=mod&Anio="+$("#AnioProyecto").val(),
			success: function(data){ $("#Respuesta").html(data); }
		});
	}
	
	$().ready(function(){
		 $.ajax({
			url:  "DetCarta.php",
			type: "POST",
			data: "Modificado=mod&Anio="+$("#AnioProyecto").val(),
			success: function(data){ $("#Respuesta").html(data); }
		});
	});
</script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Carta Constitutiva de Proyectos</title>
</head>
<body>
<?php
$respuesta = null;
$seleccionado = "";
if(isset($_GET['respuesta']))
{    $respuesta = (String)$_GET['respuesta']; }
?>
<div class="contenido">
<br/>
<p class="titulo_cat1">Proyectos >Información de Proyectos > Carta Constitutiva</p>
<h2 class="titulo_cat2"> </h2>
<?php if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>");?>
<p class="titulo_cat1"> &nbsp; &nbsp; &nbsp; &nbsp; Selecciona el Año:
    <select id="AnioProyecto" name="AnioProyecto" onchange="CambiarPeriodo()" >
       <?php 
       $indice = 2000;
          for($indice = 2000 ; $indice <= (int)date('Y'); $indice++ ){
                   $sel = "";
                   if( $indice == (int)date('Y')){ $sel = "selected='selected'"; }
               echo("<option value='".$indice."' ".$sel." >".$indice."&nbsp; &nbsp; </option>"); 
          }
          $seleccionado = (String)(date('Y'));
       ?>
    </select>
</p>
<div align="right"><a href='CartaConsAdd.php' class='liga_btn'>Crear Nueva Carta Constitutiva</a></div>
<br> </br>
<div id="Respuesta" ></div> 
     <br>
	
</div>
<br/><br/>
</body>
</html>
