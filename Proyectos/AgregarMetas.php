<?php

session_start();
include_once("../src/mx/com/virreinato/dao/TarjetaMetasDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/TarjetaMetas.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Metas</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
	<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
	<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
    <script>
    function OnlyNumber(value, elemnt){
                    if( isNaN(value) ){
                            elemnt.value = ""
                    }
    }
    </script>
</head>
<?php 
    $idTarjeta = null;

   if(isset($_GET["idTarjeta"]))
        {$idTarjeta= (String) $_GET["idTarjeta"];}
?>
<body>
<div class="contenido">
<br/>
<p class="titulo_cat1">Proyectos > <a class="linkTitulo_cat1" href="Informacion.php"> Información de Proyectos </a> >  <a class="linkTitulo_cat1" href="TarjetaRegistroMod.php?idTarjeta=<?php echo($idTarjeta);?>"> Tarjeta de Registro </a> > <a class="linkTitulo_cat1" href="lista_Metas.php?idTarjeta=<?php echo($idTarjeta);?>"> Metas del Proyecto </a> </p>
<h2 class="titulo_cat2">
    <?php
        $dao=new TarjetaMetasDaoJdbc();
	$elemento = new TarjetaMetas();
	
	if(isset($_GET['id'])){
		echo(" ");
		$elemento = $dao->obtieneElemento($_GET['id']);
	}	
	else{
		echo(" ");
	}	
    ?>
    </h2>
	<form id="frmMetas" name="frmMetas" method="post" action="../src/mx/com/virreinato/web/CatTarjetaMetas.php">
            <table width="30%" border="0" cellspacing="0" cellpadding="5" class='tb_add_cat' align='center'>
              <tr>
                  <td class="SizeText">
                   <br/>&nbsp; &nbsp; No. de Orden*:  
                   <input type="text" name="orden" id="orden" onkeyup="OnlyNumber(this.value,this)" size="5" maxlength="2" value="<?php if($elemento!= null && $elemento->getNum_orden()!=null) echo($elemento->getNum_orden());?>"  />

                    <br/><br> &nbsp;&nbsp;&nbsp; Clave*: &nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="clave_Meta" id="clave_Meta" size="30" maxlength="100" value="<?php if($elemento!=null && $elemento->getClave()!=null) echo($elemento->getClave());?>"  />

                   <br/><br> &nbsp;&nbsp;&nbsp; Acuerdo*: &nbsp;&nbsp;<input type="text" name="acuerdo_Meta" id="aucerdo_Meta" size="30" maxlength="100" value="<?php if($elemento!=null && $elemento->getNombreAcuerdo()!= null) echo($elemento->getNombreAcuerdo());?>" />
		  
                    <br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; PROGRAMACIÓN TRIMESTRAL 		  
		  
                    <br>&nbsp;&nbsp;&nbsp;&nbsp;Primero*: <input type="text" name="primero_Programacion" id="primero_Programacion" onkeyup="OnlyNumber(this.value,this)" size="11" maxlength="10" value="<?php if($elemento!=null && $elemento->getTrimestral1()!=null) echo($elemento->getTrimestral1()); ?>" />

                    <br><br>&nbsp;&nbsp;&nbsp;Segundo*: <input type="text" name="segundo_Programacion" id="segundo_Programacion"  onkeyup="OnlyNumber(this.value,this)" size="11" maxlength="10" value="<?php if($elemento!=null && $elemento->getTrimestral2()!=null) echo($elemento->getTrimestral2()); ?>" />

                    <br><br>&nbsp;&nbsp;&nbsp;Tercero*:&nbsp; <input type="text" name="tercero_Programacion" id="tercero_Programacion" onkeyup="OnlyNumber(this.value,this)" size="11" maxlength="10" value="<?php if($elemento!=null && $elemento->getTrimestral3()!=null) echo($elemento->getTrimestral3()); ?>" />

                    <br><br>&nbsp;&nbsp;&nbsp;Cuarto*:&nbsp;&nbsp; <input type="text" name="cuarto_Programacion" id="cuarto_Programacion" onkeyup="OnlyNumber(this.value,this)" size="11" maxlength="10" value="<?php if($elemento!=null && $elemento->getTrimestral4()!=null) echo($elemento->getTrimestral4()); ?>" />
		    	 
                   </td>
              </tr>
		    
                <tr>
                  <td align="center" colspan="6"><br/>
                        <input name="guardar" style="cursor:pointer" type="submit" value="Guardar"  class='btn' />
                        &nbsp; &nbsp; &nbsp;
                        <input name="cancelar" style="cursor:pointer" type="button" value="Cancelar" onclick="Regresar()"  class='btn' />
                  </td>
                </tr>
            </table>
      <?php 
          echo("<input type='hidden' name='id_Tarjeta' value='".$idTarjeta."' />");
          if( $elemento!=null && $elemento->getIdMeta()!= null ) echo("<input type='hidden' name='id' value='".$elemento->getIdMeta()."' />"); 
      ?>		
	</form>
</div>
<br/><br/>
</body>
<script>

function Regresar(){
	var tarjeta = '<?php echo$idTarjeta ?>'
	window.location="lista_Metas.php?idTarjeta="+tarjeta;
}

 var frmvalidator  = new Validator("frmMetas");
 frmvalidator.addValidation("clave_Meta","req","Por favor capture el No. de Clave de la Meta.");
 frmvalidator.addValidation("acuerdo_Meta","req","Por favor capture el acuerdo de la Meta.");
</script>
</html>
        
