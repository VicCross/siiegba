<?php

$id = null;

session_start();
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/ReintegroDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Reintegro.class.php");
include_once("../src/mx/com/virreinato/dao/PersonalProyDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/PersonalProy.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}else{
    if(isset($_SESSION['id'])){ $id = $_SESSION['id'];}
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<link href="../css/calendario.css" type="text/css" rel="stylesheet">
<script src="../js/calendar.js" type="text/javascript"></script>
<script src="../js/calendar-es.js" type="text/javascript"></script>
<script src="../js/calendar-setup.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<body>
<div class="contenido">
    <br/>
    <p class="titulo_cat1">Proyectos > <a class="linkTitulo_cat1" href="lista_Reintegro.php" > Reintegro de Impuestos </a></p>
    <h2 class="titulo_cat2">
    <?php

        $respuesta = null;
    
        if(isset($_GET["respuesta"]))
            {$respuesta= (String) $_GET["respuesta"];}
    
        $dao=new ReintegroDaoJdbc();
        $elemento=new Reintegro();

        if(isset($_GET['id'])){
            echo(" ");
            $elemento=$dao->obtieneElemento($_GET['id']);	
        }	
        else{
            echo("");
        }	
    ?>
    </h2>

     <?php
        if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>");
    ?>

    <br/>
        <form id="freintegro" name="freintegro" method="post" action="../src/mx/com/virreinato/web/WebReintegro.php">
            <table width="40%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
                <tr>
                    <td>Referencia Numérica*:</td>
                    <td><input type="text" name="refNumerica" id="refNumerica" size="50" value='<?php if($elemento!=null && $elemento->getRefNumerica()!= null) echo($elemento->getRefNumerica());?>'/></td>
                </tr>
                <tr>
                    <td>Fecha*:</td>
                    <td>
                    <input type="text" name="fecha" id="fecha" size="20" value='<?php if($elemento!=null && $elemento->getFecha()!= null) echo(date("d-m-Y",strtotime($elemento->getFecha())));?>'/>
                    </td>
                </tr>
                <tr>
                    <td width="19%">Folio*:</td>
                    <td width="81%"><input type="text" name="folio" id="folio" value='<?php if($elemento!=null && $elemento->getFolio()!= null) echo($elemento->getFolio());?>'></td>
                </tr>
                <tr>
                    <td>Honorarios*:</td>
                    <td><input type="text" name="honorarios" id="honorarios" value='<?php if($elemento!=null) echo($elemento->getHonorarios());?>'/></td>
                </tr>
		  <tr>
		    <td>Proveedor*:</td>
		    <td>
		    <select name="idPersonal" id="idPersonal">
				<option value='0'>Selecciona</option>		      
		      	<?php 
                            $personal=new PersonalProyDaoJdbc();
                            $personals=$personal->obtieneListado(); 
                            $p = new PersonalProy();

                            foreach($personals as $p){
                              $sel="";
                              if($elemento!= null && $elemento->getIdPersonal()!= null ){
                                      if($p->getId()==$elemento->getIdPersonal()){
                                            $sel="selected='selected'";
                                      }	
                              }	  
                              echo("<option value='".$p->getId()."' ".$sel." >".$p->getApp().' '.$p->getApm().' '.$p->getNombre()."</option>");
                            }
                        ?>
		    </select>
		    </td>
		  </tr>
		  
		  <tr>
		    <td>Período*:</td>
		    <td>
		    <select name="periodo" id="periodo">
				<option value='0'>Selecciona</option>
                    <?php 
                        if(!isset($_GET['id'])){
                            $daoPer=new PeriodoDaoJdbc();
                            $listaPer = $daoPer->obtieneListadoAbierto();
                            $elementoPer = new Periodo();

                            foreach($listaPer as $elementoPer){
                                $sel = "";
                                if( $elemento!= null && $elemento->getPeriodo()!= null){
                                    if( $elemento->getPeriodo() == $elementoPer->getId() ) $sel = "selected='selected'"; 
                                }
                                echo("<option value=".$elementoPer->getId()." ".$sel." >" .$elementoPer->getPeriodo()."</option>");
                            }
                        }
                        else{
                            $daoPer=new PeriodoDaoJdbc();
                            $listaPer = $daoPer->obtieneListadoAbiertoIdPeriodo($elemento->getPeriodo());
                            $elementoPer = new Periodo();
                            
                            foreach($listaPer as $elementoPer){
                                $sel = "";
                                if( $elemento!= null && $elemento->getPeriodo()!= null){
                                    if( $elemento->getPeriodo() == $elementoPer->getId() ) $sel = "selected='selected'"; 
                                }
                                echo("<option value=".$elementoPer->getId()." ".$sel." >" .$elementoPer->getPeriodo()."</option>");
                            }
                        }
                    ?>  		      
		    </select>
		    </td>
		  </tr>
		  
		  <tr>
		    <td align="center" colspan="2">
		        <input name="guardar" type="submit" value="Guardar"  class='btn' />
		        &nbsp; &nbsp; &nbsp;
		        <input name="cancelar" style="cursor:pointer" type="button" onclick="Regresar()"  value="Cancelar"  class='btn' />
		    </td>
		  </tr>
		</table>
		<?php if($elemento!=null && $elemento->getId()!=null) echo("<input type='hidden' name='id' value='".$elemento->getId()."' />");?>
	</form>
    </div>
<script type="text/javascript">

    function Regresar(){

          window.location="lista_Reintegro.php";
    }

        Calendar.setup({ inputField :"fecha", ifFormat : "%d-%m-%Y", button:"fecha" });

        var frmvalidator  = new Validator("freintegro");
        frmvalidator.addValidation("refNumerica","req","Por favor capture la referencia numérica.");
        frmvalidator.addValidation("fecha","req","Por favor capture la fecha.");
        frmvalidator.addValidation("folio","req","Por favor capture el folio.");
        frmvalidator.addValidation("honorarios","req","Por favor capture los honorarios.");
        frmvalidator.addValidation("idPersonal","dontselect=0","Por favor selecciona el Proveedor");
        frmvalidator.addValidation("periodo","dontselect=0","Por favor selecciona el Períodor");
</script>
<br/><br/>
</body>
</html>