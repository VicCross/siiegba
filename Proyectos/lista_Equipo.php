<?php
session_start();
include_once("../src/mx/com/virreinato/dao/CartaEquipoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CartaEquipo.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html>
<html>
    <head>

        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="../css/lista.css">
        <script type="text/javascript" src="../media/js/complete.js"></script>
        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
        <script type="text/javascript" src="../js/lista.js"></script>
		
			   <input type="hidden" id="columnas_visibles" name="columnas_visibles" value="1"/>
    <input type="hidden" id="total_columnas" name="total_columnas" value="2"/>
		
        <script>
		
		
		
		
            //function Agregar() {
        //        $("#AgregarMiembros").show().load("Equipo_Carta.php");
          //  }
        </script>
        <title>Sistema de Ingresos y Egresos</title>
    </head>
    <body>
        <?php
        $respuesta = null;
        $idCarta = null;

        if (isset($_GET["respuesta"])) {
            $respuesta = (String) $_GET["respuesta"];
        }
        if (isset($_GET["idCarta"])) {
            $idCarta = (String) $_GET["idCarta"];
        }
        ?>
        <div class="contenido">
            <br/>
            <p class="titulo_cat1">Proyectos > Información de Proyectos > <a class="linkTitulo_cat1" href="CartaConsAdd.php?idCarta=<?php echo($idCarta); ?>" >Carta Constitutiva de Proyectos</a> > Equipo </p>
            <h2 class="titulo_cat2"> </h2>

<?php if ($respuesta != null) echo("<div align='center' class='msj'>" . $respuesta . "</div>"); ?>
            
            <div  align="right"><a href='Equipo_Carta.php?idCarta=<?php echo($idCarta); ?>' class='liga_btn'> Agregar nuevo Miembro </a></div>
            <br> </br>

            <table  border="0" cellspacing="0" cellpadding="5" class='dataTable' align='center' >
                <thead>	 
                    <tr>
                        <th width="48%">Miembros del Equipo</th>
                        <th width="2%"></th>
                    </tr>
                </thead>	 
                <tfoot>
                    <tr>
                        <th width="48%"> </th>
						<th width="2%"></th>
				

                       
                    </tr>
                </tfoot>
                <tbody>

                    <?php
                    $dao = new CartaEquipoDaoJdbc();
                    $lista = $dao->obtieneListado($idCarta);
                    $elemento = new CartaEquipo();

                    foreach ($lista as $elemento) {
                        ?>
                        <tr class="SizeText">
                            <td align="center"><?php echo($elemento->getNombre()); ?></td>
                            <td><a href='../src/mx/com/virreinato/web/CatCartaEquipo.php?id=<?php echo($elemento->getId()); ?>&idCarta=<?php echo($idCarta); ?>' class='liga_cat'  > <acronym title="Borrar"><img src="../img/DeleteRed.png" width="16" height="16" alt="Borrar" style="border:0;" /></acronym> </a></td>
                        </tr>	
                    <?php } ?>			
                </tbody>					
            </table>
            <br>
            <div align="center">
                <a href='CartaConsAdd.php?idCarta=<?php echo($idCarta); ?>' class='liga_btn'> Regresar </a>
                &nbsp; &nbsp; &nbsp;
				
		        
				
			<!--<a style="cursor:pointer" onclick="Agregar()" class='liga_btn'> Agregar Nuevo Miembro</a>
			-->
            </div>
        </div>
        
		<!--<div id="AgregarMiembros" style="display:none">
            <br/><br/>
            
        </div> -->
		
		
		
        <br/><br/>
    </body>
</html>
</body>