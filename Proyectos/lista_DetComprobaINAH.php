<?php

include_once("../src/mx/com/virreinato/dao/ComprobaINAHDetDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ComprobaINAHDet.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
?>

<table width="80%" border="0" cellspacing="0" cellpadding="5" class='tb_cat' align='center' >

    <tr bgcolor="#9FB1CB">
        <th width="65%" align="center">Partida</th>
        <th width="5%" align="center">No. Notas</th>
        <th width="5%" align="center">Monto</th>
        <th width="2%"></th>
        <th width="2%"></th>
    </tr>
    
    <?php	
        $dao2=new ComprobaINAHDetDaoJdbc();
        $monto = $dao2->MontoMinistracion( $idComprobacion );

        $lista=$dao2->obtieneListado($idComprobacion);
        $elemento2=new ComprobaINAHDet();
  	  	
       $suma = 0;

        foreach($lista as $elemento2){
            $suma = $suma + $elemento2->getMonto();	
        }

        foreach($lista as $elemento2){
    ?>		
    <tr class="SizeText">
        <td align="center"><?php echo($elemento2->getPartida()." ".$elemento2->getDesPartida());?></td>
        <td align="center"><?php echo($elemento2->getNum_notas());?></td>
        <td align="center"><?php echo("$".  number_format($elemento2->getMonto(),2));?></td>
        <td align="center"><a href='AgregarComprobaINAHDet.php?idMaestro=<?php echo($_GET['id']);?>&id=<?php echo($elemento2->getId());?>&diferencia=<?php echo($monto-$suma);?>' class='liga_cat'><acronym title="Editar"><img src="../img/Pencil3.png" width="16" height="16" alt="Editar" style="border:0;" /></acronym></a></td>
        <td align="center"><a href='../src/mx/com/virreinato/web/WebComprobaINAHDet.php?idMaestro=<?php echo($_GET['id']);?>&id=<?php echo($elemento2->getId());?>' class='liga_cat' onclick='return confirm("¿Esta seguro que desea eliminar este registro?")' ><acronym title="Borrar"><img src="../img/DeleteRed.png" width="16" height="16" alt="Borrar" style="border:0;" /></acronym></a></td>
    </tr>
<?php			
			}//while
?>
</table>
<br/><br/>
<?php if( $suma < $monto ){  ?>
    <div align="center"><a href='AgregarComprobaINAHDet.php?idMaestro=<?php echo($idComprobacion);?>&diferencia=<?php echo($monto-$suma);?>' class='liga_btn'>Agregar Detalle de Comprobación INAH</a></div>
<?php } ?>

