<?php

include_once("../src/mx/com/virreinato/dao/FondosDetDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/FondosDet.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
?>

<table width="60%" border="0" cellspacing="0" cellpadding="5" class='tb_cat' align='center' >

    <tr bgcolor="#9FB1CB">
        <th width="50%" align="center">Partida</th>
        <th width="10%"align="center">Monto</th>
    </tr>
		
<?php
    $dao=new FondosDetDaoJdbc();
    $lista=$dao->obtieneListado($idFondo,$elemento->getMesCalendario());
    $elemento2=new FondosDet();
    	  
    foreach($lista as $elemento2){
?>

     <tr class="SizeText">
        <td align="center"><?php echo($elemento2->getDesPartida());?></td>
        <td align="center"><?php echo("$".number_format($elemento2->getMonto(),2));?></td>		
    </tr>

<?php }?>
</table>
