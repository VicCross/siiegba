<?php

session_start();
include_once("../src/mx/com/virreinato/dao/CartaCronogramaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CartaCronograma.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE HTML>

<html>
<head>
    <title>Calendario</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
    <link href="../css/calendario.css" type="text/css" rel="stylesheet">
    <script src="../js/calendar.js" type="text/javascript"></script>
    <script src="../js/calendar-es.js" type="text/javascript"></script>
    <script src="../js/calendar-setup.js" type="text/javascript"></script>
	<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
	<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
    <script language="JavaScript">

    function OnlyNumber(value, elemnt){
                    if( isNaN(value) ){
                            elemnt.value = ""
                    }
    }
    </script>
</head>
<body>
<?php 
    $error = null;
    $idCarta = null;
    
   if(isset($_GET["error"]))
        {$error= (String) $_GET["error"];}
   if(isset($_GET["idCarta"]))
        {$idCarta= (String) $_GET["idCarta"];}
?>
<div class="contenido">
<br/>
<p class="titulo_cat1">Proyectos > Información de Proyectos  > <a class="linkTitulo_cat1" href="CartaConsAdd.php?idCarta=<?php echo($idCarta); ?>" >Cartas Constitutiva de Proyectos</a> > <a class="linkTitulo_cat1" href="lista_Cronograma.php?idCarta=<?php echo($idCarta); ?>" > Cronograma de Actividades</a> </p>
<h2 class="titulo_cat2">
<?php
    $daoCarta=new CartaCronogramaDaoJdbc();
    $elemento=new CartaCronograma();	
	
	if(isset($_GET['id'])){
            echo("Modificar Actividad");
            $elemento = $daoCarta->obtieneElemento($_GET['id']);	
	}	
	else{
            echo("Alta de Nueva Actividad");
	}	
    ?>
</h2>
<?php
    if($error!=null) echo("<div align='center' class='msj'>".$error."</div>");
?>
<form id="frmCronograma_carta" name="frmCronograma_carta" method="post" action="../src/mx/com/virreinato/web/Cronograma.php">
    <table width="50%" border="0" cellspacing="0" cellpadding="5" class='tb_add_cat' align='center'>

      <tr>

        <td class="SizeText"><br/>&nbsp; &nbsp; &nbsp; No. Orden*: <input type="text" name="orden_act" id="orden_act" onkeyup="OnlyNumber(this.value,this)" size="4" maxlength="2" value="<?php if($elemento!=null && $elemento->getOrden()!= null) echo($elemento->getOrden());?>" />

            <br/><br/> &nbsp; &nbsp; &nbsp; Actividad*: 
            <br/>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <textarea name="actividad_carta" id="actividad_carta" cols="40" rows="5" maxlength="255" ><?php if($elemento!=null && $elemento->getActividad()!= null) echo($elemento->getActividad());?></textarea>

            <br/><br/>&nbsp; &nbsp; &nbsp; Fecha Inicio*: <input type="text" name="inicioActividad" id="inicioActividad" size="20" maxlength="255" value="<?php if($elemento!=null && $elemento->getFechaInicio()!= null) echo( date("d-m-Y",strtotime( $elemento->getFechaInicio())));?>" />

           <br/><br/>&nbsp; &nbsp; &nbsp; Fecha Fin*: &nbsp;&nbsp; <input type="text" name="finActividad" id="finActividad" size="20" maxlength="255" value="<?php if($elemento!=null && $elemento->getFechaFin()!= null) echo( date("d-m-Y",strtotime( $elemento->getFechaFin()))); ?>" />

           <br/><br/> &nbsp; &nbsp; &nbsp; Fase*: <input type="text" name="fase_cronograma" id="fase_cronograma" size="40" maxlength="255" value="<?php if($elemento!=null && $elemento->getFase()!=null) echo($elemento->getFase());?>" />
        </td>
		    
      </tr>
		  
		    
        <tr>
          <td align="center" colspan="6"><br/>
             <input name="guardar" style="cursor:pointer" type="submit" value="Guardar"  class='btn' />
             &nbsp; &nbsp; &nbsp;
             <input name="cancelar" style="cursor:pointer" type="button" value="Cancelar" onclick="Regresar()"  class='btn' />		      
          </td>
        </tr>
		  
    </table>
		
    <?php
      if($elemento!=null && $elemento->getId()!=null) echo("<input type='hidden' name='id' value='".$elemento->getId()."' />");
      echo("<input type='hidden' name='id_Carta'  value='".$idCarta."' />");
   ?>
</form>
</div>
<script>
 function Regresar(){
	 var carta = '<?php echo$idCarta ?>'
	 window.location="lista_Cronograma.php?idCarta="+carta;
 }

 var frmvalidator  = new Validator("frmCronograma_carta");
 frmvalidator.addValidation("actividad_carta","req","Por favor capture la actividad a desempeñar.");
 frmvalidator.addValidation("inicioActividad","req","Por favor capture la fecha de inicio.");
 frmvalidator.addValidation("finActividad","req","Por favor capture la fecha de fin.");
 
 Calendar.setup({ inputField : "inicioActividad", ifFormat : "%d-%m-%Y", button: "inicioActividad" });
 Calendar.setup({ inputField : "finActividad", ifFormat : "%d-%m-%Y", button: "finActividad" });
</script>
<br/><br/>
</body>
</html>