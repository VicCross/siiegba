<?php

session_start();
include_once("../src/mx/com/virreinato/dao/RecMaterialDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/RecMateriales.class.php");
include_once("../src/mx/com/virreinato/dao/PartidaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Partida.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE HTML>

<html>
<head>
    <title>Recursos Materiales</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
    <link href="../css/calendario.css" type="text/css" rel="stylesheet">
    <script src="../js/calendar.js" type="text/javascript"></script>
    <script src="../js/calendar-es.js" type="text/javascript"></script>
    <script src="../js/calendar-setup.js" type="text/javascript"></script>
	<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
	<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
    <script language="JavaScript">

    function OnlyNumber(value, elemnt){
                    if( isNaN(value) ){
                            elemnt.value = ""
                    }
    }
    </script>
</head>
<body>
<?php 
    $idCarta = null;

   if(isset($_GET["idCarta"]))
        {$idCarta= (String) $_GET["idCarta"];}
?>
<div class="contenido">
<br/>
<p class="titulo_cat1">Proyectos > Información de Proyectos > <a class="linkTitulo_cat1" href="CartaConsAdd.php?idCarta=<?php echo($idCarta); ?>" >Carta Constitutiva</a> > <a class="linkTitulo_cat1" href="lista_RecMateriales.php?idCarta=<?php echo($idCarta); ?>" > Recursos Materiales </a> </p>
<h2 class="titulo_cat2">
<?php 
	$daoMaterial=new RecMaterialDaoJdbc();
	$elemento=new RecMateriales();
	
	if(isset($_GET['id'])){
            echo(" ");
            $elemento = $daoMaterial->obtieneElemento($_GET['id']);
	}	
	else{
            echo(" ");
	}	
    ?>
</h2>


    <form id="frmRecMaterial" name="frmRecMaterial" method="post" action="../src/mx/com/virreinato/web/CatRecMaterial.php">
        <table width="63%" border="0" cellspacing="2" cellpadding="0" class='tb_add_cat' align='center' >
            <tr>
                <td style="display:none"><input type="text" name="id_Carta" id="id_Carta" value="<?php echo($idCarta);?>" /> </td>
            </tr>
		 		  
            <tr>
		  	 
                <td class="SizeText" width="%"> &nbsp; &nbsp; Partida*:</td>
                 <td width="%">
                 <select style="width:150px" name="partida" id="partida">
                    <option value="0" >  Selecciona </option>
                    <?php 
                        $daoPartida = new PartidaDaoJdbc();
                        $listaPartida = $daoPartida->obtieneListado();
                        $partida = new Partida();
                        foreach($listaPartida as $partida){
                            $sel = "";
                            if($elemento!= null && $elemento->getPartida()!= null ){
                                if( $elemento->getPartida() == $partida->getPartida() ){ $sel = "selected='selected'"; }
                            }
                            echo("<option value=".$partida->getPartida()." ".$sel." >" .$partida->getPartida()." ".$partida->getDescripcion()."</option>");
                        }
                        ?>
                    </select>
                    </td>

                    <td class="SizeText" width="%">No. Orden*: </td>
		     <td width="%"><input type="text" name="orden_material" id="orden_material" onkeyup="OnlyNumber(this.value,this)" size="4" maxlength="2" value="<?php if($elemento!=null && $elemento->getNum_orden()!= null) echo($elemento->getNum_orden());?>" /></td>
		  </tr>
		  
		  <tr>   
		     
		     <td class="SizeText" width="%"> &nbsp; &nbsp; Precio Unitario*: </td>
		     <td width="%"><input type="text" name="unitario" id="unitario"  onkeyup="OnlyNumber(this.value,this)" size="30" maxlength="13" value="<?php if($elemento!=null && $elemento->getUnitario()!= 0) echo($elemento->getUnitario());?>" /></td>
		     
		     <td class="SizeText" width="%">Costo Total*:</td>
		    <td width="%"><input type="text" name="total" id="total" size="30" onkeyup="OnlyNumber(this.value,this)"  maxlength="13" value="<?php if($elemento!=null && $elemento->getCosto()!= 0) echo($elemento->getCosto());?>" /></td>
		 </tr>
		 
		 <tr> 
		    <td class="SizeText" width="%"> &nbsp; &nbsp; Fecha Ejercicio*:</td>
		    <td width="%"><input type="text" name="ejercico_Material" id="ejercicio_Material" size="30" maxlength="255" value="<?php if($elemento!=null && $elemento->getFecha()!= null) echo( date("d-m-Y",strtotime( $elemento->getFecha())) );?>" /></td>  
			 
		    <td class="SizeText" width="%">Factura Entregada*: </td>
		     <td width="%"><input type="text" name="fac_entregada" id="fac_entregada"  size="30" maxlength="255" value="<?php if($elemento!=null && $elemento->getFacturaEntregada()!= null) echo($elemento->getFacturaEntregada());?>" /></td>
			 
		  </tr>
		  
		  <tr>
		     <td class="SizeText" width="%" > &nbsp; &nbsp; No. Cheque*:</td>
		    <td width="%"><input type="text" name="noCheque" id="noCheque" size="30" maxlength="255" value="<?php if($elemento!=null && $elemento->getNo_cheque()!= null) echo($elemento->getNo_cheque());?>" /></td>     
		     
			 <td class="SizeText" width="%">Cheque a  Nombre*:</td>
		    <td width="%"><input type="text" name="cheque_nombre" id="cheque_nombre" size="30" maxlength="255" value="<?php if($elemento!=null && $elemento->getNombreCheque()!= null) echo($elemento->getNombreCheque());?>" /></td>
		 </tr>
		 
		 <tr> 
           <td class="SizeText" width="%"> &nbsp; &nbsp; Descripción*:</td>
		     <td width="%">
		       <textarea name="descripcion_material" id="descripcion_material" cols="30" rows="3" maxlength="255"><?php if($elemento!=null && $elemento->getRecurso()!= null) echo($elemento->getRecurso());?></textarea>
		     </td>
		 
		 </tr>
		  <tr>
		    <td align="center" colspan="6"><br/>
		       <input name="guardar" style="cursor:pointer" type="submit" value="Guardar"  class='btn' />
		       &nbsp; &nbsp; &nbsp;
		       <input name="cancelar" style="cursor:pointer" type="button" onclick="Regresar()"  value="Cancelar"  class='btn' />
		    </td>
		  </tr>
		</table>
		
		<?php if($elemento!=null && $elemento->getId()!=null) echo("<input type='hidden' name='id' value='".$elemento->getId()."' />");?>
		
	</form>
</div>
<script>
 function Regresar(){
	 var carta = '<?php echo$idCarta ?>'
	 window.location="lista_RecMateriales.php?idCarta="+carta;
 }

 var frmvalidator  = new Validator("frmRecMaterial");
 frmvalidator.addValidation("descripcion_material","req","Por favor capture la descripción.");
 frmvalidator.addValidation("partida","dontselect=0","Por favor capture la $partida->");
 frmvalidator.addValidation("unitario","req","Por favor capture el valor unitario.");
 frmvalidator.addValidation("total","req","Por favor capture el Total.");
 
  Calendar.setup({ inputField : "ejercicio_Material", ifFormat : "%d-%m-%Y", button:"ejercicio_Material" });
 </script>
 <br/><br/>
</body>
</html>