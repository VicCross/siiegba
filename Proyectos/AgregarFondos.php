<?php

session_start();
include_once("../src/mx/com/virreinato/dao/FondosDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Fondos.class.php");
include_once("../src/mx/com/virreinato/dao/CentroCostosDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CentroCostos.class.php");
include_once("../src/mx/com/virreinato/dao/CatProyectoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatProyecto.class.php");
include_once("../src/mx/com/virreinato/dao/PartidaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Partida.class.php");
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html>

<html>
<script>
function Cambiar( a1, a2, txt){
	document.getElementById(a1).innerHTML = txt . document.getElementById(a2).value;		
	document.getElementById("Afavor").innerHTML = (document.getElementById("Centro")).options[(document.getElementById("Centro")).selectedIndex].text;
}
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link href="../css/calendario.css" type="text/css" rel="stylesheet">
<script src="../js/calendar.js" type="text/javascript"></script>
<script src="../js/calendar-es.js" type="text/javascript"></script>
<script src="../js/calendar-setup.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
<script>
  function OnlyNumber(value, elemnt){
			if( isNaN(value) ){
				elemnt.value = ""
			}
	}
	
	function Add_Periodo(sel){
		 var id = $("#periodo").val();
		 $.ajax({
		 	 url: "../src/mx/com/virreinato/web/AgregarFondos.php",
		 	 type: "POST",
		 	 data: "Proyecto=Proyecto&idPeriodo="+id+"&Sel="+sel,
		 	 success: function(data){ $("#Proyecto").html(data); }
		 })
	}
	
	
	function Add_Mes(proyecto,sel){
		$("#Error").hide();
		$("#mesFondos").val(0);
		$("#mesFondos").show();
		
		if( sel != 0 ){
			$("#mesFondos").val(sel);
		}
	}
	
	function Regresar(){
		window.location="lista_Fondos.php";
	}
	
	function Guardar(){
		 $("#periodo").removeAttr('disabled');
	 	 $("#Proyecto").removeAttr('disabled');
	 	 $("#mesFondos").removeAttr('disabled');
	 	 document.forms["frmSolicitud"].submit(); 
	}
	
	function Datos(){
		var mes = $("#mesFondos").val();
		$("#Error").hide();
		
		 if( mes != 0 ){
	 	 $.ajax({
                    url: "../src/mx/com/virreinato/web/AgregarFondos.php", type: "POST", data: "ValidaMes="+mes+"&idProyecto="+$("#Proyecto").val(),
                    success: function(data){
                          if( data != "" ){ var aux = data.split(":"); $("#Error").text(aux[1]); $("#Error").show();
                          }else{  $("#Maestro").show();  
                                          $("#periodo").attr('disabled','disabled');
                                          $("#Proyecto").attr('disabled','disabled');
                                          $("#mesFondos").attr('disabled','disabled');
                                          $("#btnGuardar").removeAttr('disabled');
                                   } 
                    }
                });
		 	 
		 }else{   $("#mesFondos").focus(); }
		 
	}
</script>
<title>Solicitud de Fondo</title>
</head>
<body>
<?php 
    $respuesta = null;
    $idFondo = null;
    
   if(isset($_GET["respuesta"]))
        {$respuesta= (String) $_GET["respuesta"];}
?>
<div class="contenido">
<br/>
<p class="titulo_cat1">Proyectos > <a class="linkTitulo_cat1" href="lista_Fondos.php" >Solicitud de Fondo</a> </p>
<h2 class="titulo_cat2">
<?php
    $dao=new FondosDaoJdbc();
    $elemento=new Fondos();

    if(isset($_GET['id'])){
        echo(" ");
        $idFondo = (String)$_GET['id'];
        $elemento=$dao->obtieneElemento($_GET['id']);	
    }	
    else{
        echo(" ");
    }	
?>
</h2>
<?php if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>");?>
<div align='center' class='msj' id="Error" style="display:none" ></div>
<br/>
<form action="../src/mx/com/virreinato/web/WebFondos.php" method="post" name="frmSolicitud" id="frmSolicitud">
    <table width="65%" border="0" cellspacing="0" cellpadding="5" class='tb_add_cat' align='center'>
        <tr>        	 	
            <td>
			   
            <br/> &nbsp; &nbsp; &nbsp; Perído* :
            <select name="periodo" id="periodo" onchange="Add_Periodo(0)"  > 
            <option value="0" >Selecciona</option> 
              <?php
              if(!isset($_GET['id'])){
                $daoPer=new PeriodoDaoJdbc();
                $listaPer = $daoPer->obtieneListadoAbierto();
                $elementoPer = new Periodo();

                foreach($listaPer as $elementoPer){
                    $sel2 = "";
                     if($elemento!= null && $elemento->getIdPeriodo()!= null ){
                          if($elemento->getIdPeriodo() == $elementoPer->getId() ){ $sel2 = "selected='selected'"; }
                    }	
                    echo("<option value=".$elementoPer->getId()."  ".$sel2." >" . $elementoPer->getPeriodo() ."</option>");
                }
              }
              else{
                $daoPer=new PeriodoDaoJdbc();
                $listaPer = $daoPer->obtieneListado();
                $elementoPer = new Periodo();

                foreach($listaPer as $elementoPer){
                    $sel2 = "";
                     if($elemento!= null && $elemento->getIdPeriodo()!= null ){
                          if($elemento->getIdPeriodo() == $elementoPer->getId() ){ $sel2 = "selected='selected'"; }
                    }	
                    echo("<option value=".$elementoPer->getId()."  ".$sel2." >" . $elementoPer->getPeriodo() ."</option>");
                }

              }
            ?>  
            </select>
		     	 
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Proyecto*:          	
            <select name="Proyecto" style="width:170px" id="Proyecto" onchange="Add_Mes(0,0)" ></select>

            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Mes*:
            <select name="mesFondos" id="mesFondos" style="width:110px; display:none;" onchange="Datos()" >
                <option value="0" > Selecciona </option>
                <option value="trdc_ene" >Enero </option>
                    <option value="trdc_feb" > Febrero </option>
                    <option value="trdc_mar" > Marzo </option>
                    <option value="trdc_abr" > Abril </option>
                    <option value="trdc_may" > Mayo </option>
                    <option value="trdc_jun" > Junio </option>
                    <option value="trdc_jul" > Julio </option>
                    <option value="trdc_ago" > Agosto </option>
                    <option value="trdc_sep" > Septiembre </option>
                    <option value="trdc_oct" > Octubre </option>
                    <option value="trdc_nov" > Noviembre </option>
                    <option value="trdc_dic" > Diciembre </option>
            </select>  
		    	   
            <div id="Maestro" style="display:none">
            <br/> &nbsp; &nbsp; &nbsp; Número de Solicitud:  <input type="text" onkeyup="OnlyNumber(this.value,this)" name="txtNumSolicitud" id="txtNumSolicitud" maxlength="50" value="<?php if($elemento!=null && $elemento->getNumSol()!= null) echo($elemento->getNumSol());?>"/>
                &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; Fecha:  <input type="text" name="txtFecha" id="txtFecha" value="<?php if($elemento!=null && $elemento->getFecha()!= null) echo(date("d-m-Y",strtotime($elemento->getFecha())));?>"/>
 		
            <br/><br/> &nbsp; &nbsp; &nbsp; Centro de Trabajo:
            <select name="Centro" id="Centro" style="width:22	0px">
                <?php
                    $centro=new CentroCostosDaoJdbc();
                    $centros=$centro->obtieneListado(); 
                    $c = new CentroCostos();

                    foreach($centros as $c){
                        $sel="";
                        if($elemento!= null && $elemento->getIdCCosto()!= null ){
                                         if($c->getId()==$elemento->getIdCCosto()){ $sel="selected='selected'"; }	
                        }	  
                        echo("<option value='".$c->getId()."' ".$sel." >".$c->getDescripcion()."</option>");
                    }
               ?> 
            </select>
		      		    
			 
            <div style="display:none" >
            <br/><br/>&nbsp; &nbsp; &nbsp; Tipo de Proyecto:
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            <br/> &nbsp; &nbsp; &nbsp; &nbsp;
            <?php
                $val= "";
                if($elemento != null && $elemento->getTipo() != null && $elemento->getTipo() == 1){
                    $val=" checked";
                }else{ $val= ""; }
                echo("<INPUT readonly='readonly'  TYPE= 'radio' id='chkOpera' name='Tipo' value='1' ". $val .">Operación<BR>");
                if($elemento != null && $elemento->getTipo() != null && $elemento->getTipo() == 2){
                    $val=" checked";
                }else{ $val= ""; }
                echo(" &nbsp; &nbsp; &nbsp; &nbsp; <INPUT TYPE= 'radio' readonly='readonly' id='chkInver' name='Tipo' value='2' ". $val .">Inversion<BR>");
                if($elemento != null && $elemento->getTipo() != null && $elemento->getTipo() == 3){
                    $val=" checked";
                }else{ $val= ""; }
                echo("&nbsp; &nbsp; &nbsp; &nbsp; <INPUT TYPE= 'radio' readonly='readonly' id='chkProy'  name='Tipo' checked='checked' value='3' ". $val .">Proyectos<BR>");
                if($elemento != null && $elemento->getTipo() != null && $elemento->getTipo() == 4){
                    $val=" checked";
                }else{ $val= ""; }
                echo(" &nbsp; &nbsp; &nbsp; &nbsp; <INPUT TYPE= 'radio' readonly='readonly' id='chkTerc'  name='Tipo' value='4' ". $val .">Terceros<P>");
            ?>
            </div>
        	
            <br/><br/>&nbsp; &nbsp; &nbsp; Descripción:
                <br/>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                <textarea name="txtDescripcion" id="txtDescripcion" cols="70" rows="3" maxlength="250" ><?php if($elemento!=null && $elemento->getDescripcion()!= null) echo($elemento->getDescripcion());?></textarea>
			
                <br/><br/>&nbsp; &nbsp; &nbsp; Nombre del Titular del Área: <input type="text" name="txtTitular" id="txtTitular" size="60" maxlength="200" value="<?php if($elemento!=null && $elemento->getTitular()!= null) echo($elemento->getTitular()); else echo("Cecilia Genel Velasco"); ?>"/> 
            </div>
        			
            </td>     
        </tr>
         
        <tr>
            <td align="center" colspan="4"> 
                <input name="guardar" type="button" id="btnGuardar" onclick="Guardar()" value="Guardar"  class='btn'  />
                &nbsp; &nbsp; &nbsp;
                <input name="cancelar" style="cursor:pointer" type="button" value="Cancelar"  onclick="Regresar()"  class='btn' />
                <?php if( $idFondo!= null ) echo("<input type='hidden' id='mes' value='".$elemento->getMesCalendario()."' >"); ?>
                <br/><br/>
            </td>
        </tr>
         
  </table>
  <?php if($elemento!=null && $elemento->getId()!=null) echo("<input type='hidden' name='id' value='".$elemento->getId()."' />");?>  
 </form>
</div>
<br/>
<?php if( $idFondo!= null){
   include "lista_DetFondos.php";
}?>
<script>
 var frmvalidator  = new Validator("frmSolicitud");
 frmvalidator.addValidation("txtNumSolicitud","req","Por favor capture el Número de solicitud");
 
 Calendar.setup({ inputField : "txtFecha", ifFormat : "%d-%m-%Y", button: "txtFecha" });
 
 if( $("#periodo").val() != 0 ){
    var idProyecto = '<?php echo$elemento->getIdProyecto()?>'
    Add_Periodo( idProyecto );

   
    Add_Mes( idProyecto,$("#mes").val());

    $("#periodo").attr('disabled','disabled');
    $("#Proyecto").attr('disabled','disabled');
    $("#mesFondos").attr('disabled','disabled');
    $("#Maestro").show();
 	 
 	 
 } 

</script>
<br/><br/> 
</body>
</html>