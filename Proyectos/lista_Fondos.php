<?php

session_start();
include_once("../src/mx/com/virreinato/dao/FondosDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Fondos.class.php");
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/CatProyectoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatProyecto.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html>

<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link href="../css/calendario.css" type="text/css" rel="stylesheet">
<script src="../js/calendar.js" type="text/javascript"></script>
<script src="../js/calendar-es.js" type="text/javascript"></script>
<script src="../js/calendar-setup.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="../css/lista.css">
        <script type="text/javascript" src="../media/js/complete.js"></script>
        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
        <script type="text/javascript" src="../js/lista.js"></script>
		
			   <input type="hidden" id="columnas_visibles" name="columnas_visibles" value="5"/>
    <input type="hidden" id="total_columnas" name="total_columnas" value="8"/>

<title>Solicitud de Fondo</title>
<script>
 
  function Imprimir(pagina,folio) {
		var dir = pagina + "?folio=" + folio;
		var left = Math.floor((screen.width - 980) / 2);
		var opciones = 'titlebar=0, menubar=0, toolbar=0, location=0, directories=0, status=0, scrollbars=0, resizable=0, width=980, height=750,top=50,left='
				+ left + '';
		window.open(dir, "", opciones);
	}
  
 
</script>
</head>

<body>
<?php 
  $respuesta = null;
  if(isset($_GET["respuesta"]))
        {$respuesta= (String) $_GET["respuesta"];}
        
  $diaf = (String)(date('d'));
  $dia ="01";
  
  $mes = (String)(date('m'));
  //$mesInicio = (String)( fecha.get(java.util.Calendar.MONTH) - 1 ); if(mesInicio.length() == 1){ mesInicio = "0"+mesInicio; } 
  $mesInicio ="01";	
?>
<div class="contenido">

<br/>
<p class="titulo_cat1">Proyectos > Solicitud de Fondos</p>
 <?php
        if (isset($_GET['recargado']) && $_GET['recargado'] == "1") {
            echo '<input type="hidden" id="nombre_form" name="nombre_form" value=""/>';
        } else {
            echo '<input type="hidden" id="nombre_form" name="nombre_form" value="frmFiltroFondo"/>';
        }
        ?>


<h2 class="titulo_cat2"> </h2>

<p class="titulo_cat1">
    <form name="frmFiltroFondo" id="frmFiltroFondo" method="POST" action="../src/mx/com/virreinato/web/WebFondos.php" >
     &nbsp; &nbsp; Fecha de Inicio: <input type="text" size="10" name="inicio" id="inicioFecha" maxlength="20" value="<?php  if(isset($_GET["inicio"]) && $_GET["inicio"] !=null ){ echo( $_GET["inicio"]); } ?>"  >
        Fecha de Fin: <input type="text" size="10" name="fin" id="finFecha" maxlength="20" value=" <?php  if(isset($_GET["fin"]) && $_GET["fin"]!=null ){ echo( $_GET["fin"] ); }  else { echo($dia."-".$mes."-".date('Y'));} ?>"  >
        <?php 
          $inicio =  $dia."-".$mesInicio."-".date('Y');
          $fin = $diaf."-".$mes."-".date('Y');
          $periodo = "";
          $proyecto = "";
        ?>
        &nbsp; Período: 
        <select name="periodo" id="periodo" style="width:110px" > 
          <?php
              $daoPer=new PeriodoDaoJdbc();
              $listaPer = $daoPer->obtieneListado();
              $elementoPer = new Periodo();

              foreach($listaPer as $elementoPer){
                  $sel = "";
                  if(isset($_GET['periodo']) && (int)( $_GET["periodo" ] ) == $elementoPer->getId()) $sel = "selected='selected'"; 
                  else if( (!isset($_GET['periodo'])) && (int)($elementoPer->getPeriodo()) == (int)date('Y')  ){ $sel = "selected='selected'"; $periodo = (String)($elementoPer->getId()); }
                  echo("<option value=".$elementoPer->getId()." ".$sel." >" .$elementoPer->getPeriodo()."</option>");
              }
          ?>  
        </select>
        
        &nbsp; Proyecto:
        <select name="proyecto" id="proyecto"style="width:210px">
            <option value="TD">Todos los Proyectos</option>
            <?php
                $proyecto = "TD";

                $catProyecto=new CatProyectoDaoJdbc();
                $catProyectos=$catProyecto->obtieneListado2(); 
                $aux = 0;
                $cp = new CatProyecto();
                foreach($catProyectos as $cp){
                    $selProyecto="";
                    if( $_POST["proyecto"] != null && $_POST["proyecto"] == ((String)($cp->getId()) )){ $selProyecto="selected='selected'";  $proyecto = (String)($cp->getId()); }
                    echo("<option value='".$cp->getId()."' ".$selProyecto." >".$cp->getDescripcion()."</option>");
                    $aux++;
                }
            ?>
        </select>  
		
		
        &nbsp; <input name="Filtro" style="cursor:pointer" type="submit" value="Filtrar"  class='btn' />
    </form>
<div align="right"><a href='AgregarFondos.php' class='liga_btn'>Agregar Solicitud de Fondos</a></div>
<br> </br>
<br>

<?php
    if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>");
?>
<table  border="0" cellspacing="0" cellpadding="5" class='dataTable' align='center' >
<thead>
    <tr bgcolor="#9FB1CB">
        <th width="7%" align="center">Nº de Solicitud</th>
        <th width="20%" align="center">Proyecto</th>
        <th width="7%" align="center">Mes</th>
        <th width="20%" align="center">Descripción</th>			
        <th width="12%" align="center">Fecha</th>
        <th width="2%"></th>
        <th width="2%"></th>
        <th width="2%"></th>
    </tr>
	</thead>
	<tfoot>
	<tr bgcolor="#9FB1CB">
        <th width="7%" align="center"> </th>
        <th width="20%" align="center"></th>
        <th width="7%" align="center"></th>
        <th width="20%" align="center"></th>			
        <th width="12%" align="center"></th>
        <th width="2%"></th>
        <th width="2%"></th>
        <th width="2%"></th>
    </tr>
	</tfoot>
		<tbody>
    <?php			

        if(isset($_GET['inicio']) && $_GET["inicio"] !=null ){
 	       $inicio = ( String )$_GET["inicio"];
 	       $fin = (String)$_GET["fin"];
        }

        if(isset($_GET['periodo']) && $_GET['periodo'] != null ){  $periodo = (String)$_GET['periodo'];  }
        else {
            $aniosel = (String)(date('Y'));
            $per=new PeriodoDaoJdbc();

            $periodo=$per->obtieneidElemento($aniosel);	        
        }
			
        $dao=new FondosDaoJdbc();
        $lista=$dao->obtieneListado($inicio,$fin,$periodo,$proyecto);
        $elemento=new Fondos();	  	  	
	 
	foreach($lista as $elemento){
    ?>		
    <tr class="SizeText">
        <td align="center"><?php echo($elemento->getNumSol());?></td>
        <td align="center"><?php echo($elemento->getDesProyecto());?></td>
        <td align="center">
         <?php 
            if( $elemento->getMesCalendario() == "trdc_ene" )  echo("Enero");
            else if( $elemento->getMesCalendario() == "trdc_feb" )  echo("Febrero");
            else if( $elemento->getMesCalendario() == "trdc_mar" )  echo("Marzo");
            else if( $elemento->getMesCalendario() == "trdc_abr" )  echo("Abril");
            else if( $elemento->getMesCalendario() == "trdc_may" )  echo("Mayo");
            else if( $elemento->getMesCalendario() == "trdc_jun" )  echo("Junio");
            else if( $elemento->getMesCalendario() == "trdc_jul" )  echo("Julio");
            else if( $elemento->getMesCalendario() == "trdc_ago" )  echo("Agosto");
            else if( $elemento->getMesCalendario() == "trdc_sep" )  echo("Septiembre");
            else if( $elemento->getMesCalendario() == "trdc_oct" )  echo("Octubre");
            else if( $elemento->getMesCalendario() == "trdc_nov" )  echo("Noviembre");
            else if( $elemento->getMesCalendario() == "trdc_dic" )  echo("Diciembre");

        ?>
        </td>
        <td align="center"><?php echo($elemento->getDescripcion());?></td>
        <td align="center"><?php echo( date("d-m-Y",strtotime($elemento->getFecha())));?></td>
        <td><a href='AgregarFondos.php?id=<?php echo($elemento->getId());?>' class='liga_cat'><acronym title="Editar"><img src="../img/Pencil3.png" width="16" height="16" alt="Editar" style="border:0;" /></acronym> </a></td>
        <td><a href='../src/mx/com/virreinato/web/WebFondos.php?id=<?php echo($elemento->getId());?>' class='liga_cat'><acronym title="Borrar"  onclick='return confirm("¿Esta seguro que desea eliminar este registro?")'><img src="../img/DeleteRed.png" width="16" height="16" alt="Borrar" style="border:0;" /></acronym></a></td>
        <td><a href="javascript:Imprimir('../Formatos/SolicitudFondos.php','<?php  echo( $elemento->getId() ); ?>')" class='liga_cat' ><acronym title="Imprimir"><img src="../img/printer.png" width="18" height="18" alt="Imprimir" style="border:0;" /></acronym></a></td>
    </tr>
    <?php			
        }//while
    ?>
	</tbody>
</table>
    <br>
    	
</div>
<script language="JavaScript">
   Calendar.setup({ inputField :"finFecha", ifFormat : "%d-%m-%Y", button:"finFecha" });
    
   Calendar.setup({ inputField : "inicioFecha", ifFormat : "%d-%m-%Y", button:"inicioFecha" });
   
   var ini = '<?php echo$inicio ?>';
   var fin = '<?php echo$fin ?>';
   $("#inicioFecha").val(ini);
   $("#finFecha").val(fin);
   

</script>	
<br/><br/>
</body>
</html>
          