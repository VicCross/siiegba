<?php

session_start();
include_once("../src/mx/com/virreinato/dao/CatChequeDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatCheque.class.php");
include_once("../src/mx/com/virreinato/dao/CatProyectoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatProyecto.class.php");
include_once("../src/mx/com/virreinato/dao/ProveedorDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Proveedor.class.php");
include_once("../src/mx/com/virreinato/dao/EmpleadoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Empleado.class.php");
include_once("../src/mx/com/virreinato/dao/DetComprobacionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/DetComprobacion.class.php");
include_once("../src/mx/com/virreinato/dao/PresupuestoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Presupuesto.class.php");
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/ComprobacionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Comprobacion.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link href="../css/calendario.css" type="text/css" rel="stylesheet">
<script src="../js/calendar.js" type="text/javascript"></script>
<script src="../js/calendar-es.js" type="text/javascript"></script>
<script src="../js/calendar-setup.js" type="text/javascript"></script>
<script src="../js/gen_validatorv4.js" ></script>
<script src="../js/jquery-1.7.2.js"></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
<title>Consultar Comprobacion</title>
</head>
<body>
<div class="contenido">
<br/>
<p class="titulo_cat1">Proyectos > <a class="linkTitulo_cat1" href="lista_Comprobacion.php" > Comprobaci&oacute;n  Museo </a></p>
<h2 class="titulo_cat2">
<?php 
    $respuesta = null;
    $folio = null;
    $id = null;
    
   if(isset($_GET["respuesta"]))
        {$respuesta= (String) $_GET["respuesta"];}
    if(isset($_GET["id"]))
        {$id = (String) $_GET["id"];}
    
    $dao= new ComprobacionDaoJdbc();
    $elemento = new Comprobacion();

	if($id!= null){
		echo(" ");
		$elemento = $dao->obtieneElemento($id);
		$folio =  $elemento->getFolio();
	}	
	else{
		echo(" ");
	}
?>
</h2>
<?php
    if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>");
    
    ?>
<form id="frmComprobacion" id="frmComprobacion" method="POST" action="../src/mx/com/virreinato/web/CatComprobacion.php">
            <table width="50%" border="0" cellspacing="0" cellpadding="5" class='tb_add_cat' align='center'>
                <tr>
                    <td>
	            <br>&nbsp;&nbsp;&nbsp;Número de Comprobación* <input onblur="validar('Folio','numerico')" type="text" maxlength="20" size="20" name="Folio" id="Folio" value="<?php if($elemento!=null && $elemento->getFolio()!= null) echo($elemento->getFolio()); ?>">			
				
                    <br/><br/>&nbsp;&nbsp;&nbsp;Período*:
                    <?php
                    if(isset($_GET['periodo'])){
                     ?>   
                     <input type='hidden' name="Periodo" id="Periodo" value='<?php echo($_GET['periodo']); ?>' />
                     <?php
		       	$dao2=new PeriodoDaoJdbc();
                        $per=$dao2->obtieneElemento($_GET['periodo']);
                        echo($per->getPeriodo());
                    }else{
                    ?>
                <select name="Periodo" id="Periodo" SIZE=1 >
                <option value='0'>Selecciona</option>
            <?php
                $dao2=new PeriodoDaoJdbc();
                $lista2=$dao2->obtieneListadoAbierto();
                $c = new Periodo();
                
                foreach ($lista2 as $c){
                    echo("<option value='".$c->getId()."'  >".$c->getPeriodo()."</option>");
                }
                echo("</select>");
                }
            ?> 
                     <br/><br/> &nbsp;&nbsp;&nbsp;Fecha*:
                    <input onblur="validar('Fecha','fecha')" type="text" maxlength="20" size="20" name="Fecha" id="Fecha" value="<?php if($elemento!=null && $elemento->getFecha()!= null) echo(date("d-m-Y",strtotime($elemento->getFecha()))); ?>" >

                   <br/><br/>&nbsp;&nbsp;&nbsp;Destino*: 
                   <?php
                        $val= "";
                        $elemento->setDestino("PR");


                        if($elemento != null && $elemento->getDestino() != null && $elemento->getDestino() == "GB"){
                            $val=" checked";
                        }else{ $val= ""; }
                        echo("&nbsp;&nbsp;<INPUT TYPE= 'radio' id='chkGB' name='Destino' value='GB' ".$val.">Gasto Basico");
                        if($elemento != null && $elemento->getDestino() != null && $elemento->getDestino() == "PR"){
                            $val=" checked";
                        }else{ $val= ""; }
                        echo("&nbsp;&nbsp;<INPUT TYPE= 'radio' id='chkProy' name='Destino' value='PR' ".$val.">Proyecto");
                    ?>
                    <br><br>&nbsp;&nbsp;&nbsp;Descripci&oacute;n* 
                    <br/>&nbsp; &nbsp;  &nbsp; &nbsp;<textarea name="Descripcion" id="Descripcion" rows="4"  maxlength="255" cols="50" /><?php if($elemento!=null && $elemento->getDescripcion()!= null) echo($elemento->getDescripcion());?></textarea>


                    <br/><br/>&nbsp;&nbsp;&nbsp;Folio del Cheque*
                    <?php if($elemento!= null && $elemento->getIdCheque()!= null ){ ?>
                       <select name="Cheque" id="Cheque" SIZE=1 onChange="Cambiar()" disabled="disabled" >
                    <?php }else{ ?>
                      <select name="Cheque" id="Cheque" SIZE=1 onChange="Cambiar()">
                    <?php } ?>

                     <option value='0' id="Combo">Selecciona</option>
                     
                <?php
			          
                    //obtiene todo el listado de comprobaciones de la base de datos
                    $daoComprobacion= new ComprobacionDaoJdbc();
                    $comprobacion= new Comprobacion();

                    //obtiene todo el listado de cheques emitidos de la base de datos
                    $dao4=new CatChequeDaoJdbc();
                    $lista4=null;
                    if($elemento!= null && $elemento->getIdCheque()!= null ){
                            $lista4=$dao4->obtieneListadoEmitidoSinCmpDestChe("PR",(String)($elemento->getIdCheque()));
                    }
                    else{

                            $lista4=$dao4->obtieneListadoEmitidoSinCmpDest("PR");
                    }

                    $ch= new CatCheque();
                    
                    $dao3=new CatProyectoDaoJdbc();
                    $cp= new CatProyecto();

                    $sel="";

                    //itera todos los cheques 
                    foreach($lista4 as $ch) 
                    {
                        $destinoCh=$ch->getDestino();	
                        if($destinoCh!=null && $destinoCh == "GB"){
                            $destinoCh="Gasto Básico";	
                        }else if($destinoCh!=null && $destinoCh == "PR"){
                            $cp= $dao3->obtieneElemento((String)($ch->getIdProyecto()) );
                            if($cp->getDescripcion()!= null)
                                $destinoCh= $cp->getDescripcion();
                            else
                                $destinoCh= "";
                         }
                        else if($destinoCh!=null && $destinoCh == "TR"){
                            $destinoCh="Terceros";	
                        }
                        else if($destinoCh!=null && $destinoCh == "DN"){
                            $destinoCh="Donativos";	
                        }


                              //si estoy editando
                        if($elemento!= null && $elemento->getIdCheque()!= null )
                        {
                            //si el cheque iterado es el que pertenece esta comprobación lo pongo como seleccionado
                            if($ch->getId() == $elemento->getIdCheque())
                            {
                                $sel="selected='selected'";
                            }	
                            else
                            {      $sel=""; }
                            echo("<option value='"."$".number_format($ch->getMonto(),2)."/".$ch->getId()."/".$ch->getMonto()."' ".$sel."> ".$ch->getFolio()." / ".$destinoCh."</option>");
                              }
                              //si voy a capturar
                        else
                        {

                            echo("<option value='"."$".number_format($ch->getMonto())."/".$ch->getId()."/".$ch->getMonto()."' ".$sel."> ".$ch->getFolio()." / ".$destinoCh."</option>");

                        }

                              //echoln("<option value='"+ch.getId()+"' "+sel+" >"+ch.getFolio()+"</option>");
                    }
						
                ?> 
                </select>
                <br>
                &nbsp; &nbsp; <input type="hidden" name="Monto" id="Monto">
                <label id="txtMonto" > Monto: 0 </label>
                
                <input type="hidden" name="Proyecto" id="Proyecto">
                <br/><br/>&nbsp; &nbsp; <label id="txtProyecto" > Proyecto: </label>
                 <?php
                    $estatus= "Pendiente";

                    //si estoy editando veo el estatus
                    if($elemento!=null && $elemento->getEstatus()!= null)
                    {
                        //obtengo el listado de detalles de la comprobacion que estoy editando
                        $daoDet=new DetComprobacionDaoJdbc();
                        $listaDet= $daoDet->obtieneListado($id);
                        $detalle = new DetComprobacion();

                        //obtengo el monto total a comprobar
                        $montoComprobaciones=0;
                        $aux= (double)$elemento->getMontoComprobacion();
                        $montoTotal= (double)$aux;

                        //itero los detalles para ver el monto comprobado 
                        foreach($listaDet as $detalle)
                        {
                            $montoComprobaciones+=$detalle->getMonto();
                        }

                        //si aun no compruebo todo el monto
                        if($montoTotal > $montoComprobaciones) 
                        {
                                $estatus="Pendiente";
                                $comprobacion->setEstatus("P");
                                $daoComprobacion->actualizaElemento($comprobacion);
                    ?>
                    <input type="hidden" id="Estatus" name="Estatus" value="P">
                    <?php
                        }
                            //Ya esta todo comprobado
                        else 
                        {
                            $estatus="Completo";
                            $comprobacion->setEstatus("C");
                            $daoComprobacion->actualizaElemento($comprobacion);
                    ?>
                    <input type="hidden" id="Estatus" name="Estatus" value="C">
                    <?php 
                        }      	
                    }
                    else
                    {
                ?>
                <input type="hidden" id="Estatus" name="Estatus" value="P">
                <?php
                        }
                ?>
                <br><br>&nbsp;&nbsp;&nbsp;
                <label id="textEstatus" name="textestatus">Estatus: <?php echo$estatus ?></label><!--  <input type="text" maxlength="1" name="Estatus" id="Estatus" size="42" value="<?php if($elemento!=null && $elemento->getEstatus()!= null) echo($elemento->getEstatus());?>"/> -->
					
					
                        <br><br>&nbsp;&nbsp;&nbsp;Observaciones*
                        <br/>&nbsp; &nbsp;  &nbsp; &nbsp;<textarea name="Observaciones" id="Observaciones" rows="4"  maxlength="255" cols="50" /><?php if($elemento!=null && $elemento->getObservaciones()!= null) echo($elemento->getObservaciones());?></textarea> 
                    </td>	
                </tr>
			
                <tr>
	   	  <?php if($id == null){?>
		       <td align="center" colspan="3">
			      <input name="guardar" style="cursor:pointer" type="submit" value="Guardar"  class='btn' />
			       &nbsp; &nbsp; &nbsp;
		       	  <input name="cancelar" type="reset" style="cursor:pointer" value="Cancelar"  onclick="Regresar()"  class='btn' />
	          </td>
	       <?php } ?>
	     
	        <?php if($id != null){?>
		       <td align="center" colspan="4">
                            <input name="guardar" style="cursor:pointer" type="button" onClick = "Validar()" value="Guardar"  class='btn' />
                            &nbsp; &nbsp; &nbsp;
                            <input name="cancelar" style="cursor:pointer"  type="button"  onclick="Regresar()"  value="Cancelar"  class='btn' />
	          </td>
	      <?php } ?>
		</tr>
		
		</table>
		<?php if($elemento!=null && $elemento->getIdComprobacion()!=null) echo("<input type='hidden' name='id' value='".$elemento->getIdComprobacion()."' />");?>
		</form>
		</div> 
		<?php 
                    if( $id != null)
                    { 
		?>
	   <br/><br/><br/><br/>
            <?php
            include "lista_DetComprobacion.php";
            }
            ?> 
        <br/><br/>
</body>
<script>
    Calendar.setup({ inputField : "Fecha", ifFormat : "%d-%m-%Y", button: "Fecha" });
    Cambiar();
    
    var frmvalidator  = new Validator("frmComprobacion");
	frmvalidator.addValidation("Folio","req","Por favor capture el folio.");
	frmvalidator.addValidation("Periodo","dontselect=0","Por favor seleccione el período.");
	frmvalidator.addValidation("Fecha","req","Por favor capture la fecha.");
	frmvalidator.addValidation("Descripcion","req","Por favor capture la descripción.");
	frmvalidator.addValidation("Cheque","dontselect=0","Por favor seleccione un cheque.");
	frmvalidator.addValidation("Observaciones","req","Por favor capture las observaciones.");
    
    function Validar(){
    	 $("#Cheque").removeAttr('disabled');
    	 
    	 document.forms["frmComprobacion"].submit(); 
    }
    
    function Cambiar()
	{
		var cheque = $("#Cheque").val();
		var aux = cheque.split("/");

		
		$("#txtMonto").text("Monto: "+aux[0]);
		$("#Monto").val(aux[2]);
	
		
	}
    
    function Regresar(){
    	window.location="lista_Comprobacion.php";
    }
	
	function validar(elemento,tipo)
	{
		var campo=document.getElementById(elemento);
		var texto=document.getElementById(elemento).value;
		
		switch(tipo)
		{
			case 'numerico':
				if(texto.match(/[a-z]+/gi).length!=null)
				{
					alert('El campo no puede contener letras');
					campo.value="0000";
				}
				break;
			case 'fecha':
				if(texto.match(/[a-z]+/gi).length!=null)
				{
					alert('El campo no puede contener letras');
					var date = new Date();
					var dia = date.getDate();      if( dia.toString().length == 1 ) dia = "0"+dia; 
					var mes = (date.getMonth()+1); if( mes.toString().length == 1 )  mes = "0"+mes;
					campo.value= dia+"-"+mes+"-"+date.getFullYear() ;
				}
				break;
			default:
				break;
		}
	}
	
	
</script>

</html>