<?php

session_start();
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");}
include_once("PermisosMenu.class.php");
include_once("Catalogo.class.php");

$catalogo = new Catalogo();
$obj = new PermisosMenu();
if (isset($_POST['id']) && isset($_POST['id2']) && isset($_POST['erase'])) {
    
    $obj->setcper($_POST['id']);
    $obj->setIdSubmenu($_POST['id2']);
    if ($obj->deleteRegistro()) {
        echo "El permiso se eliminó correctamente";
    } else {
        echo "El permiso no se pudo eliminar, ya que contiene datos asociados.";
    }
} else {
    if (isset($_POST['form'])) {
        $parametros = "";
        parse_str($_POST['form'], $parametros);
    }
    $obj->setIdPuesto($catalogo->satinizar_input($parametros['puesto']));
    $obj->setIdSubmenu($catalogo->satinizar_input($parametros['submenu']));
    if (isset($parametros['alta']) && $parametros['alta'] == "on") {
        $obj->setAlta(1);
    } else {
        $obj->setAlta(0);
    }
    if (isset($parametros['baja']) && $parametros['baja'] == "on") {
        $obj->setBaja(1);
    } else {
        $obj->setBaja(0);
    }
    if (isset($parametros['modificacion']) && $parametros['modificacion'] == "on") {
        $obj->setModificacion(1);
    } else {
        $obj->setModificacion(0);
    }
    if (isset($parametros['consulta']) && $parametros['consulta'] == "on") {
        $obj->setConsulta(1);
    } else {
        $obj->setConsulta(0);
    }

    if (isset($parametros['idP']) && $parametros['idP'] == "") {
        $obj2 = new PermisosMemu();
        if ($obj2->getRegistroById($obj->getIdPuesto(), $obj->getIdSubmenu())) {
            echo "Error: este permiso ya está dado de alta";
        } else {
            if ($obj->newRegistro()) {
                echo "El permiso se registró correctamente";
            } else {
                echo "Error: El permiso ya se encuentra registrado";
            }
        }
    } else {
        $obj->setId($parametros['idS']);
        if ($obj->editRegistro()) {
            echo "El permiso se modificó correctamente";
        } else {
            echo "Error: El permiso ya se encuentra registrado";
        }
    }
}
?>
