<?php

include_once("Catalogo.class.php");

class Menu{
	public function obtieneMenu($perfil){
		$consulta = "SELECT x.id_perfil_menu, m.nom_menu, m.referencia FROM sie_cat_menu m, sie_cat_perfilmenu x WHERE x.id_menu = m.id_menu AND x.id_perfil = ".$perfil." ORDER BY x.id_menu";
		$catalogo = new Catalogo();
		$query = $catalogo->obtenerLista($consulta);
        return $query;
	}
	
	public function obtienePerfil($perfil){
		$consulta = "SELECT cper_descripcion FROM sie_cat_perfiles WHERE cper_id_perfil = ".$perfil;
		$catalogo = new Catalogo();
		$query = $catalogo->obtenerLista($consulta);
		return $query;
	}
	
	public function obtienePerfiles(){
		$consulta = "SELECT cper_id_perfil, cper_descripcion FROM sie_cat_perfiles ORDER BY cper_id_perfil";
		$catalogo = new Catalogo();
		$query = $catalogo->obtenerLista($consulta);
		return $query;	
	}
	
	public function tieneSubMenus($idMenu){
		$consulta = "SELECT COUNT(*) AS numero FROM sie_cat_MSM WHERE id_perfil_menu = ".$idMenu;
		$catalogo = new Catalogo();
		$query = $catalogo->obtenerLista($consulta);
		if($rs = mysql_fetch_array($query)){
			if($rs['numero'] > 0) return true;
			else return false;	
		}
	}
	
	public function obtieneSubMenu($idMenu){
		//$consulta = "SELECT id_submenu, nom_submenu, referencia FROM sie_cat_submenu WHERE id_menu = ".$idMenu;	
		$consulta = "SELECT s.id_submenu, s.nom_submenu, s.referencia FROM sie_cat_submenu s, sie_cat_MSM x WHERE x.id_submenu = s.id_submenu AND x.id_perfil_menu = ".$idMenu;
		$catalogo = new Catalogo();
		$query = $catalogo->obtenerLista($consulta);
		return $query;
	}
	
	public function tieneSSubMenus($idSMenu){
		$consulta = "SELECT COUNT(*) AS numero FROM sie_cat_ssubmenu WHERE id_submenu = ".$idSMenu;
		$catalogo = new Catalogo();
		$query = $catalogo->obtenerLista($consulta);
		if($rs = mysql_fetch_array($query)){
			if($rs['numero'] > 0) return true;
			else return false;	
		}
	}
	
	public function obtieneSSubMenu($idSMenu){
		$consulta = "SELECT nom_ssubmenu, referencia FROM sie_cat_ssubmenu WHERE id_submenu = ".$idSMenu;	
		$catalogo = new Catalogo();
		$query = $catalogo->obtenerLista($consulta);
		return $query;
	}
	
}
?>
