<?php

class Reintegro {
    private $id;
    private $refNumerica;
    private $fecha;
    private $folio;
    private $honorarios;
    private $idPersonal;
    private $rfc;
    private $curp;
    private $nombre;
    private $app;
    private $apm;
    private $periodo;
    
    function setAll($id, $refNumerica, $fecha, $folio, $honorarios, $idPersonal, $rfc, $curp, $nombre, $app, $apm, $periodo) {
        $this->id = $id;
        $this->refNumerica = $refNumerica;
        $this->fecha = $fecha;
        $this->folio = $folio;
        $this->honorarios = $honorarios;
        $this->idPersonal = $idPersonal;
        $this->rfc = $rfc;
        $this->curp = $curp;
        $this->nombre = $nombre;
        $this->app = $app;
        $this->apm = $apm;
        $this->periodo = $periodo;
    }

    function constructor2($id, $refNumerica, $fecha, $folio, $honorarios, $idPersonal, $rfc, $curp, $nombre, $app, $apm) {
        $this->id = $id;
        $this->refNumerica = $refNumerica;
        $this->fecha = $fecha;
        $this->folio = $folio;
        $this->honorarios = $honorarios;
        $this->idPersonal = $idPersonal;
        $this->rfc = $rfc;
        $this->curp = $curp;
        $this->nombre = $nombre;
        $this->app = $app;
        $this->apm = $apm;
    }

    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getRefNumerica() {
        return $this->refNumerica;
    }

    public function getFecha() {
        return $this->fecha;
    }

    public function getFolio() {
        return $this->folio;
    }

    public function getHonorarios() {
        return $this->honorarios;
    }

    public function getIdPersonal() {
        return $this->idPersonal;
    }

    public function getRfc() {
        return $this->rfc;
    }

    public function getCurp() {
        return $this->curp;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function getApp() {
        return $this->app;
    }

    public function getApm() {
        return $this->apm;
    }

    public function getPeriodo() {
        return $this->periodo;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setRefNumerica($refNumerica) {
        $this->refNumerica = $refNumerica;
    }

    public function setFecha($fecha) {
        $this->fecha = $fecha;
    }

    public function setFolio($folio) {
        $this->folio = $folio;
    }

    public function setHonorarios($honorarios) {
        $this->honorarios = $honorarios;
    }

    public function setIdPersonal($idPersonal) {
        $this->idPersonal = $idPersonal;
    }

    public function setRfc($rfc) {
        $this->rfc = $rfc;
    }

    public function setCurp($curp) {
        $this->curp = $curp;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function setApp($app) {
        $this->app = $app;
    }

    public function setApm($apm) {
        $this->apm = $apm;
    }

    public function setPeriodo($periodo) {
        $this->periodo = $periodo;
    }


}
