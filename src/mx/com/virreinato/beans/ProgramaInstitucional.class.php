<?php

class ProgramaInstitucional {
    private $idProgramaInstitucional;
    private $programaInstitucional;
    private $estatus;
    
    function setAll($idProgramaInstitucional, $programaInstitucional, $estatus) {
        $this->idProgramaInstitucional = $idProgramaInstitucional;
        $this->programaInstitucional = $programaInstitucional;
        $this->estatus = $estatus;
    }
    
    function __construct() {
        
    }

    public function getIdProgramaInstitucional() {
        return $this->idProgramaInstitucional;
    }

    public function getProgramaInstitucional() {
        return $this->programaInstitucional;
    }

    public function getEstatus() {
        return $this->estatus;
    }

    public function setIdProgramaInstitucional($idProgramaInstitucional) {
        $this->idProgramaInstitucional = $idProgramaInstitucional;
    }

    public function setProgramaInstitucional($programaInstitucional) {
        $this->programaInstitucional = $programaInstitucional;
    }

    public function setEstatus($estatus) {
        $this->estatus = $estatus;
    }


}
