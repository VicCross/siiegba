<?php

/**
 * Description of CentroCostos
 *
 * @author HPdv6
 */
class CentroCostos {
    private $id;
    private $clave;
    private $descripcion;
    
    function __construct() {
        
    }

    
    public function setAll($id, $clave, $descripcion){
        $this->id = $id;
        $this->clave = $clave;
        $this->descripcion = $descripcion;
    }
    
    public function getId() {
        return $this->id;
    }

    public function getClave() {
        return $this->clave;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setClave($clave) {
        $this->clave = $clave;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }


}
