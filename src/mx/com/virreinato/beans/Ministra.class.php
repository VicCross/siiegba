<?php

class Ministra {
    private $id;
    private $monto;
    private $fecha;
    private $descripcion;
    private $destino;
    private $observaciones;
    private $formaCarga;
    private $idCuentaBancaria;
    private $idPeriodo;
    private $idProyecto;
    private $numOperacion;
    private $desPeriodo;
    private $desProyecto;
    private $desCuentaBancaria;
    private $idSolicitud;
    
    function setAll($id, $monto, $fecha, $descripcion, $destino, $observaciones, $formaCarga, $idCuentaBancaria, $idPeriodo, $idProyecto, $numOperacion, $desPeriodo, $desProyecto, $desCuentaBancaria, $idSolicitud) {
        $this->id = $id;
        $this->monto = $monto;
        $this->fecha = $fecha;
        $this->descripcion = $descripcion;
        $this->destino = $destino;
        $this->observaciones = $observaciones;
        $this->formaCarga = $formaCarga;
        $this->idCuentaBancaria = $idCuentaBancaria;
        $this->idPeriodo = $idPeriodo;
        $this->idProyecto = $idProyecto;
        $this->numOperacion = $numOperacion;
        $this->desPeriodo = $desPeriodo;
        $this->desProyecto = $desProyecto;
        $this->desCuentaBancaria = $desCuentaBancaria;
        $this->idSolicitud = $idSolicitud;
    }

    function __construct() {
        
    }
    
    public function getId() {
        return $this->id;
    }

    public function getMonto() {
        return $this->monto;
    }

    public function getFecha() {
        return $this->fecha;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function getDestino() {
        return $this->destino;
    }

    public function getObservaciones() {
        return $this->observaciones;
    }

    public function getFormaCarga() {
        return $this->formaCarga;
    }

    public function getIdCuentaBancaria() {
        return $this->idCuentaBancaria;
    }

    public function getIdPeriodo() {
        return $this->idPeriodo;
    }

    public function getIdProyecto() {
        return $this->idProyecto;
    }

    public function getNumOperacion() {
        return $this->numOperacion;
    }

    public function getDesPeriodo() {
        return $this->desPeriodo;
    }

    public function getDesProyecto() {
        return $this->desProyecto;
    }

    public function getDesCuentaBancaria() {
        return $this->desCuentaBancaria;
    }

    public function getIdSolicitud() {
        return $this->idSolicitud;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setMonto($monto) {
        $this->monto = $monto;
    }

    public function setFecha($fecha) {
        $this->fecha = $fecha;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function setDestino($destino) {
        $this->destino = $destino;
    }

    public function setObservaciones($observaciones) {
        $this->observaciones = $observaciones;
    }

    public function setFormaCarga($formaCarga) {
        $this->formaCarga = $formaCarga;
    }

    public function setIdCuentaBancaria($idCuentaBancaria) {
        $this->idCuentaBancaria = $idCuentaBancaria;
    }

    public function setIdPeriodo($idPeriodo) {
        $this->idPeriodo = $idPeriodo;
    }

    public function setIdProyecto($idProyecto) {
        $this->idProyecto = $idProyecto;
    }

    public function setNumOperacion($numOperacion) {
        $this->numOperacion = $numOperacion;
    }

    public function setDesPeriodo($desPeriodo) {
        $this->desPeriodo = $desPeriodo;
    }

    public function setDesProyecto($desProyecto) {
        $this->desProyecto = $desProyecto;
    }

    public function setDesCuentaBancaria($desCuentaBancaria) {
        $this->desCuentaBancaria = $desCuentaBancaria;
    }

    public function setIdSolicitud($idSolicitud) {
        $this->idSolicitud = $idSolicitud;
    }



}
