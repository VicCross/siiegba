<?php

class Area {

    private $id;
    private $descripcion;
	private $idLider;
	private $firma;
	private $presupuesto;
	private $saldo;
    
    function setAll($id, $descripcion, $idLider, $firma, $presupuesto, $saldo) {
        $this->id = $id;
        $this->descripcion = $descripcion;
		$this->idLider = $idLider;
		$this->firma = $firma;
		$this->presupuesto = $presupuesto;
		$this->saldo = $saldo;
    }

    public function __construct() {
        
    }
    
    public function getId() {
        return $this->id;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }
	
	public function getIdLider(){
		return $this->idLider;	
	}
	
	public function setIdLider($id){
		$this->idLider = $id;
	}

	public function getPresupuesto(){
		return $this->presupuesto;	
	}
	
	public function setPresupuesto($presupuesto){
		$this->presupuesto = $presupuesto;
	}
	
	public function getSaldo(){
		return $this->saldo;	
	}
	
	public function setSaldo($saldo){
		$this->saldo = $saldo;
	}
	
	public function getFirma(){
		return $this->firma;	
	}
	
	public function setFirma($firma){
		$this->firma = $firma;	
	}
}
