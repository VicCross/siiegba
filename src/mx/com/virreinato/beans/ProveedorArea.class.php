<?php

class ProveedorArea {
    
    private $id;
    private $proveedor;
    private $domicilio;
    private $cp;
    private $representante;
    private $telefono;
    private $otroTelefono;
    private $correo;
    private $estatus;
	private $area;
    
    function setAll($id, $proveedor, $domicilio, $cp, $representante, $telefono, $otroTelefono, $correo, $estatus, $area) {
        $this->id = $id;
        $this->proveedor = $proveedor;
        $this->domicilio = $domicilio;
        $this->cp = $cp;
        $this->representante = $representante;
        $this->telefono = $telefono;
        $this->otroTelefono = $otroTelefono;
        $this->correo = $correo;
        $this->estatus = $estatus;
		$this->area = $area;
    }

    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getProveedor() {
        return $this->proveedor;
    }

    public function getDomicilio() {
        return $this->domicilio;
    }

    public function getCp() {
        return $this->cp;
    }

    public function getRepresentante() {
        return $this->representante;
    }

    public function getTelefono() {
        return $this->telefono;
    }

    public function getOtroTelefono() {
        return $this->otroTelefono;
    }

    public function getCorreo() {
        return $this->correo;
    }

    public function getEstatus() {
        return $this->estatus;
    }
	
	public function getArea() {
        return $this->area;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setProveedor($proveedor) {
        $this->proveedor = $proveedor;
    }

    public function setDomicilio($domicilio) {
        $this->domicilio = $domicilio;
    }

    public function setCp($cp) {
        $this->cp = $cp;
    }

    public function setRepresentante($representante) {
        $this->representante = $representante;
    }

    public function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    public function setOtroTelefono($otroTelefono) {
        $this->otroTelefono = $otroTelefono;
    }

    public function setCorreo($correo) {
        $this->correo = $correo;
    }

    public function setEstatus($estatus) {
        $this->estatus = $estatus;
    }
	
	public function setArea($area) {
        $this->area = $area;
    }


}
