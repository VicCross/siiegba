<?php

class Eje {
    private $id;
    private $eje;
    private $descripcion;
	private $periodo;
    
    function setAll($id, $eje, $descripcion,$periodo) {
        $this->id = $id;
        $this->eje = $eje;
        $this->descripcion = $descripcion;
		$this->periodo=$periodo;
    }

    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getEje() {
        return $this->eje;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

	public function getPeriodo() {
        return $this->periodo;
    }
	
	
    public function setId($id) {
        $this->id = $id;
    }

    public function setEje($eje) {
        $this->eje = $eje;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }
	
	public function setPeriodo ($periodo) {
        $this->periodo = $periodo;
    }

}
