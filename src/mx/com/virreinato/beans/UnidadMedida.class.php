<?php

class UnidadMedida {
    private $id;
    private $clave;
    private $descripcion;
    private $orden;
    
    function setAll($id, $clave, $descripcion, $orden) {
        $this->id = $id;
        $this->clave = $clave;
        $this->descripcion = $descripcion;
        $this->orden = $orden;
    }

    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getClave() {
        return $this->clave;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function getOrden() {
        return $this->orden;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setClave($clave) {
        $this->clave = $clave;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function setOrden($orden) {
        $this->orden = $orden;
    }


}
