<?php

	class PresupuestoArea
	{
		private $id;
		private $proyecto;
		private $area;
		private $descripcion;
		private $proveedor;
		private $cotizacion;
	    private $mes;
		private $partida;
		private $status;
		private $pagosforaneos;
		private $moneda;
		private $inba;
		private $pasivo;
		
		
		function setAll($id, $proyecto, $area, $descripcion, $proveedor, $cotizacion, $mes, $partida, $status, $pagosforaneos, $moneda, $inba, $pasivo)
		{
			$this->id = $id;
			$this->proyecto = $proyecto;
			$this->area = $area;
			$this->descripcion = $descripcion;
			$this->proveedor = $proveedor;
			$this->cotizacion = $cotizacion;
			$this->mes = $mes;
			$this->partida = $partida;
			$this->status = $status;
			$this->pagosforaneos = $pagosforaneos;
			$this->moneda = $moneda;
			$this->inba = $inba;
			$this->pasivo = $pasivo;
			
		}
		
		function __construct() {}
		
		public function getId() 
		{
        return $this->id;
        }
		
		public function getProyecto() 
		{
        return $this->proyecto;
		}
		
		public function getArea() 
		{
        return $this->area;
		}
		
		public function getDescripcion() 
		{
        return $this->descripcion;
		}
		
		public function getProveedor() 
		{
        return $this->proveedor;
		}
		
		public function getCotizacion() 
		{
        return $this->cotizacion;
		}
		
		public function getMes() 
		{
        return $this->mes;
		}
		
		public function getPartida() 
		{
        return $this->partida;
		}
		
		public function getStatus() 
		{
        return $this->status;
		}
		
		public function getPagosforaneos() 
		{
        return $this->pagosforaneos;
		}
		
		public function getMoneda() 
		{
        return $this->moneda;
		}
		
		public function getInba() 
		{
        return $this->inba;
		}
		
		public function getPasivo() 
		{
        return $this->pasivo;
		}
		
		
		
		public function setId($id) 
		{
			$this->id = $id;
		}
		
		public function setProyecto($proyecto) 
		{
			$this->proyecto = $proyecto;
		}
		
		public function setArea($area) 
		{
			$this->area = $area;
		}
		
		public function setDescripcion($descripcion) 
		{
			$this->descripcion = $descripcion;
		}
		
		public function setProveedor($proveedor) 
		{
			$this->proveedor = $proveedor;
		}
		
		public function setCotizacion($cotizacion) 
		{
			$this->cotizacion = $cotizacion;
		}
		
		public function setMes($mes) 
		{
			$this->mes = $mes;
		}
		
		public function setPartida($partida) 
		{
			$this->partida = $partida;
		}
		
		public function setStatus($status) 
		{
			$this->status = $status;
		}
		
		public function setPagosforaneos($pagosforaneos) 
		{
			$this->pagosforaneos = $pagosforaneos;
		}
		
		public function setMoneda($moneda) 
		{
			$this->moneda = $moneda;
		}
		
		public function setInba($inba) 
		{
			$this->inba = $inba;
		}
		
		public function setPasivo($pasivo) 
		{
			$this->pasivo = $pasivo;
		}
		
	}


?>