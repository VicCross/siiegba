<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Area.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class AreaDaoJdbc {
    
    public function obtieneAreas() {


        $lista= array();

        $query="SELECT * FROM cat_areas WHERE car_estatus=1 ORDER BY car_area";

        $con=new Catalogo();
        $result = $con->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs["car_id_area"];
            $descripcion= $rs[strtoupper("car_area")];
			$idLider = $rs[strtoupper("id_lider")];
			$presupuesto = $rs['presupuesto_inicial'];
            $elemento= new Area();
            $elemento->setAll($id, $descripcion, $idLider, $presupuesto);
            array_push($lista,$elemento);
        }	
        return $lista;
    }
        
    public function obtieneArea($idArea) {
		
		
        $elemento=new Area();

        $query="SELECT * FROM cat_areas WHERE car_id_area=".$idArea;

        $con=new Catalogo();
        $result = $con->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs["car_id_area"];
            $descripcion= $rs[strtoupper("car_area")];
            $elemento= new Area();
            $elemento->setAll($id, $descripcion);
        }

        return $elemento;		
    }
    
    public function guardaArea($area) {
		
        $con=new Catalogo();
        $query="INSERT INTO cat_areas(car_area) VALUES ('".mb_strtoupper($area->getDescripcion(), 'UTF-8')."')";
        $res = $con->obtenerLista($query);

        if($res==1)
        {        return true; }
        else
        {        return false; }
		
    }
    
    public function actualizaArea($area) {

        $con=new Catalogo();
        $query="UPDATE cat_areas set  car_area='".mb_strtoupper($area->getDescripcion(),'UTF-8')."' WHERE car_id_area=".$area->getId();
        $res = $con->obtenerLista($query);
        
        if($res==1)
        {        return true; }
        else
        {        return false; }

    }
    public function eliminaArea($idElemento) {

        $con=new Catalogo();
        $query="UPDATE cat_areas set car_estatus=0 WHERE car_id_area=".$idElemento;
        $res = $con->obtenerLista($query);

        if($res==1)
        {        return true; }
        else
        {        return false; }

    }
}
