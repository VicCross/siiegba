<?php

class TarjetaMetas {
    private $idMeta;
    private $idTarjeta;
    private $num_orden;
    private $clave;
    private $nombreAcuerdo;
    private $trimestral1;
    private $trimestral2;
    private $trimestral3;
    private $trimestral4;
    
    function setAll($idMeta, $idTarjeta, $num_orden, $clave, $nombreAcuerdo, $trimestral1, $trimestral2, $trimestral3, $trimestral4) {
        $this->idMeta = $idMeta;
        $this->idTarjeta = $idTarjeta;
        $this->num_orden = $num_orden;
        $this->clave = $clave;
        $this->nombreAcuerdo = $nombreAcuerdo;
        $this->trimestral1 = $trimestral1;
        $this->trimestral2 = $trimestral2;
        $this->trimestral3 = $trimestral3;
        $this->trimestral4 = $trimestral4;
    }

    function constructor2($idMeta, $num_orden, $clave, $nombreAcuerdo, $trimestral1, $trimestral2, $trimestral3, $trimestral4) {
        $this->idMeta = $idMeta;
        $this->num_orden = $num_orden;
        $this->clave = $clave;
        $this->nombreAcuerdo = $nombreAcuerdo;
        $this->trimestral1 = $trimestral1;
        $this->trimestral2 = $trimestral2;
        $this->trimestral3 = $trimestral3;
        $this->trimestral4 = $trimestral4;
    }

    function __construct() {
        
    }

    public function getIdMeta() {
        return $this->idMeta;
    }

    public function getIdTarjeta() {
        return $this->idTarjeta;
    }

    public function getNum_orden() {
        return $this->num_orden;
    }

    public function getClave() {
        return $this->clave;
    }

    public function getNombreAcuerdo() {
        return $this->nombreAcuerdo;
    }

    public function getTrimestral1() {
        return $this->trimestral1;
    }

    public function getTrimestral2() {
        return $this->trimestral2;
    }

    public function getTrimestral3() {
        return $this->trimestral3;
    }

    public function getTrimestral4() {
        return $this->trimestral4;
    }

    public function setIdMeta($idMeta) {
        $this->idMeta = $idMeta;
    }

    public function setIdTarjeta($idTarjeta) {
        $this->idTarjeta = $idTarjeta;
    }

    public function setNum_orden($num_orden) {
        $this->num_orden = $num_orden;
    }

    public function setClave($clave) {
        $this->clave = $clave;
    }

    public function setNombreAcuerdo($nombreAcuerdo) {
        $this->nombreAcuerdo = $nombreAcuerdo;
    }

    public function setTrimestral1($trimestral1) {
        $this->trimestral1 = $trimestral1;
    }

    public function setTrimestral2($trimestral2) {
        $this->trimestral2 = $trimestral2;
    }

    public function setTrimestral3($trimestral3) {
        $this->trimestral3 = $trimestral3;
    }

    public function setTrimestral4($trimestral4) {
        $this->trimestral4 = $trimestral4;
    }


}
