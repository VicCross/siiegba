<?php

class ComprobaINAH {
    private $id;
    private $idPeriodo;
    private $desPeriodo;
    private $monto;
    private $idMinistracion;
    private $desMinistracion;
    private $url;
    private $fecha;
    private $idCCosto;
    private $desCCosto;
    
    function setAll($id, $idPeriodo, $desPeriodo, $monto, $idMinistracion, $desMinistracion, $url, $fecha, $idCCosto, $desCCosto) {
        $this->id = $id;
        $this->idPeriodo = $idPeriodo;
        $this->desPeriodo = $desPeriodo;
        $this->monto = $monto;
        $this->idMinistracion = $idMinistracion;
        $this->desMinistracion = $desMinistracion;
        $this->url = $url;
        $this->fecha = $fecha;
        $this->idCCosto = $idCCosto;
        $this->desCCosto = $desCCosto;
    }

    function comproba($id, $idPeriodo, $idMinistracion, $url, $fecha, $idCCosto) {
        $this->id = $id;
        $this->idPeriodo = $idPeriodo;
        $this->idMinistracion = $idMinistracion;
        $this->url = $url;
        $this->fecha = $fecha;
        $this->idCCosto = $idCCosto;
    }

    function __construct() {
        
    }
    
    public function getId() {
        return $this->id;
    }

    public function getIdPeriodo() {
        return $this->idPeriodo;
    }

    public function getDesPeriodo() {
        return $this->desPeriodo;
    }

    public function getMonto() {
        return $this->monto;
    }

    public function getIdMinistracion() {
        return $this->idMinistracion;
    }

    public function getDesMinistracion() {
        return $this->desMinistracion;
    }

    public function getUrl() {
        return $this->url;
    }

    public function getFecha() {
        return $this->fecha;
    }

    public function getIdCCosto() {
        return $this->idCCosto;
    }

    public function getDesCCosto() {
        return $this->desCCosto;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setIdPeriodo($idPeriodo) {
        $this->idPeriodo = $idPeriodo;
    }

    public function setDesPeriodo($desPeriodo) {
        $this->desPeriodo = $desPeriodo;
    }

    public function setMonto($monto) {
        $this->monto = $monto;
    }

    public function setIdMinistracion($idMinistracion) {
        $this->idMinistracion = $idMinistracion;
    }

    public function setDesMinistracion($desMinistracion) {
        $this->desMinistracion = $desMinistracion;
    }

    public function setUrl($url) {
        $this->url = $url;
    }

    public function setFecha($fecha) {
        $this->fecha = $fecha;
    }

    public function setIdCCosto($idCCosto) {
        $this->idCCosto = $idCCosto;
    }

    public function setDesCCosto($desCCosto) {
        $this->desCCosto = $desCCosto;
    }

}
