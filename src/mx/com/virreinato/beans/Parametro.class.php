<?php

class Parametro {
    private $id;
    private $parametro;
    private $valor;
    
    function setAll($id, $parametro, $valor) {
        $this->id = $id;
        $this->parametro = $parametro;
        $this->valor = $valor;
    }

    function __construct() {
        
    }
    
    public function getId() {
        return $this->id;
    }

    public function getParametro() {
        return $this->parametro;
    }

    public function getValor() {
        return $this->valor;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setParametro($parametro) {
        $this->parametro = $parametro;
    }

    public function setValor($valor) {
        $this->valor = $valor;
    }

}
