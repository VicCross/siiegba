<?php

class Metas {
    private $id;
    private $idActividadGral;
    private $anio;
    private $descripcion;
    
    function setAll($id, $idActividadGral, $anio, $descripcion) {
        $this->id = $id;
        $this->idActividadGral = $idActividadGral;
        $this->anio = $anio;
        $this->descripcion = $descripcion;
    }

    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getIdActividadGral() {
        return $this->idActividadGral;
    }

    public function getAnio() {
        return $this->anio;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setIdActividadGral($idActividadGral) {
        $this->idActividadGral = $idActividadGral;
    }

    public function setAnio($anio) {
        $this->anio = $anio;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }


}
