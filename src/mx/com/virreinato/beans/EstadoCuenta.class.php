<?php

class EstadoCuenta {
    private $fecha;
    private $descripcion;
    private $depositos;
    private $retiros;
    private $saldo;
    
    function __construct() {
        
    }

    public function getFecha() {
        return $this->fecha;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function getDepositos() {
        return $this->depositos;
    }

    public function getRetiros() {
        return $this->retiros;
    }

    public function getSaldo() {
        return $this->saldo;
    }

    public function setFecha($fecha) {
        $this->fecha = $fecha;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function setDepositos($depositos) {
        $this->depositos = $depositos;
    }

    public function setRetiros($retiros) {
        $this->retiros = $retiros;
    }

    public function setSaldo($saldo) {
        $this->saldo = $saldo;
    }


}
