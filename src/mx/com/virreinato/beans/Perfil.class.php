<?php

class Perfil {
    
    private $id;
    private $descripcion;
    
    function setAll($id, $descripcion) {
        $this->id = $id;
        $this->descripcion = $descripcion;
    }

    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }


}
