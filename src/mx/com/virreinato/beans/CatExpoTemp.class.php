<?php

class ExpoTemp {
    
    private $id;
    private $nombre; 
    private $inicio;
    private $fin;
    private $titulo;
    private $tipo;
    private $periodo;
    private $area;
    private $accion;
	private $saldo;
    
    
    function setAll($id, $nombre, $inicio, $fin, $titulo, $tipo, $periodo, $area, $accion, $saldo) {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->inicio = $inicio;
        $this->fin = $fin;
        $this->titulo = $titulo;
        $this->tipo = $tipo;
        $this->periodo = $periodo;
        $this->area = $area;
        $this->accion = $accion;
		$this->saldo = $saldo;
    }

    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function getInicio() {
        return $this->inicio;
    }

    public function getFin() {
        return $this->fin;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getTipo() {
        return $this->tipo;
    }

    public function getPeriodo() {
        return $this->periodo;
    }

    public function getArea() {
        return $this->area;
    }

    public function getAccion() {
        return $this->accion;
    }
	 public function getSaldo() {
        return $this->saldo;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function setInicio($inicio) {
        $this->inicio = $inicio;
    }

    public function setFin($fin) {
        $this->fin = $fin;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    public function setPeriodo($periodo) {
        $this->periodo = $periodo;
    }

    public function setArea($area) {
        $this->area = $area;
    }

    public function setAccion($accion) {
        $this->accion = $accion;
    }
	public function setSaldo($saldo) {
        $this->saldo = $saldo;
    }
	
	
}
