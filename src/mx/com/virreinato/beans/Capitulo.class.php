<?php


class Capitulo {
    
    private $id;
    private $descripcion;
    private $capitulo;
    
    function __construct($id, $descripcion, $capitulo) {
        $this->id = $id;
        $this->descripcion = $descripcion;
        $this->capitulo = $capitulo;
    }

    public function getId() {
        return $this->id;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function getCapitulo() {
        return $this->capitulo;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function setCapitulo($capitulo) {
        $this->capitulo = $capitulo;
    }
}
