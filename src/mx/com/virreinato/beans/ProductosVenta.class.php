<?php

class ProductosVenta {

    private $id_venta;
	private $id_producto;
    private $nom_producto;
	private $inventario_act;
	private $precio_unitario;
	private $piezas_vendidas;
	private $monto_total;
	private $monto_museo;
	private $monto_patronato;
   

    function setAll($id_venta, $id_producto, $nom_producto, $inventario_act, $precio_unitario, $piezas_vendidas, $monto_total, $monto_museo, $monto_patronato) {
        
		$this->id_venta = $id_venta;
        $this->id_producto = $id_producto;
        $this->nom_producto = $nom_producto;
        $this->inventario_act = $inventario_act;
        $this->precio_unitario = $precio_unitario;
        $this->piezas_vendidas = $piezas_vendidas;
		$this->monto_total = $monto_total;
        $this->monto_museo = $monto_museo;
        $this->monto_patronato = $monto_patronato;
      
    }
    
    function __construct() {
        
    }

        public function getId_venta() {
        return $this->id_venta;
    }

    public function setId_venta($id_venta) {
        $this->id_venta = $id_venta;
    }

    public function getId_producto() {
        return $this->id_producto;
    }

    public function setId_producto($id_producto) {
        $this->id_producto = $id_producto;
    }

    public function getNom_producto() {
        return $this->nom_producto;
    }

    public function setNom_producto($nom_producto) {
        $this->nom_producto = $nom_producto;
    }


    public function getInventario_act() {
        return $this->inventario_act;
    }

    public function setInventario_act($inventario_act) {
        $this->inventario_act = $inventario_act;
    }

    public function getPrecio_unitario() {
        return $this->precio_unitario;
    }

    public function setPrecio_unitario($precio_unitario) {
        $this->precio_unitario = $precio_unitario;
    }
	
	 public function getPiezas_vendidas() {
        return $this->piezas_vendidas;
    }

    public function setPiezas_vendidas($piezas_vendidas) {
        $this->piezas_vendidas = $piezas_vendidas;
    }
	
	public function getMonto_total() {
        return $this->monto_total;
    }

    public function setMonto_total($monto_total) {
        $this->monto_total = $monto_total;
    }
	
	public function getMonto_museo() {
        return $this->monto_museo;
    }

    public function setMonto_museo($monto_museo) {
        $this->monto_museo = $monto_museo;
    }
	
	public function getMonto_patronato() {
        return $this->monto_patronato;
    }

    public function setMonto_patronato($monto_patronato) {
        $this->monto_patronato = $monto_patronato;
    }



}

?>
