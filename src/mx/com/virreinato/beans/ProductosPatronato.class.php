<?php

class Productos {

    private $id_prod;
    private $nombre_prod;
    private $descripcion_prod;
	private $inventario_ini;
	private $inventario_act;
	private $estatus_prod;
   

    function setAll($id_prod, $nombre_prod, $descripcion_prod, $inventario_ini, $inventario_act, $estatus_prod) {
        $this->id_prod = $id_prod;
        $this->nombre_prod = $nombre_prod;
        $this->descripcion_prod = $descripcion_prod;
        $this->inventario_ini = $inventario_ini;
        $this->inventario_act = $inventario_act;
        $this->estatus_prod = $estatus_prod;
      
    }
    
    function __construct() {
        
    }

        public function getId_prod() {
        return $this->id_prod;
    }

    public function setId_prod($id_prod) {
        $this->id_prod = $id_prod;
    }

    public function getNombre_prod() {
        return $this->nombre_prod;
    }

    public function setNombre_prod($nombre_prod) {
        $this->nombre_prod = $nombre_prod;
    }

    public function getDescripcion_prod() {
        return $this->descripcion_prod;
    }

    public function setDescripcion_prod($descripcion_prod) {
        $this->descripcion_prod = $descripcion_prod;
    }

    public function getInventario_ini() {
        return $this->inventario_ini;
    }

    public function setInventario_ini($inventario_ini) {
        $this->inventario_ini = $inventario_ini;
    }

    public function getInventario_act() {
        return $this->inventario_act;
    }

    public function setInventario_act($inventario_act) {
        $this->inventario_act = $inventario_act;
    }

    public function getEstatus_prod() {
        return $this->estatus_prod;
    }

    public function setEstatus_prod($estatus_prod) {
        $this->estatus_prod = $estatus_prod;
    }



}

?>
