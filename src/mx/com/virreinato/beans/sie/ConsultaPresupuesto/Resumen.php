<?php

$id = null;
$user = null;

session_start();
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/ConsultaResumenDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ConsultaResumen.class.php");
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}else{
    if(isset($_SESSION['id'])){ $id = $_SESSION['id'];}
    if(isset($_SESSION['user'])){ $user = $_SESSION['user']; }
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
<title>Consulta de Presupuesto (Resumen) </title>
</head>
<body>
<div class="contenido">
    <br/>
    <p class="titulo_cat1">Consulta de Presupuesto > Resumen</p>
    <form name="frmFiltroResumen" id="frmFiltroResmuen" method="POST" action="../src/mx/com/virreinato/web/WebConsultaPresupuesto.php" >
        <?php  
            $periodo = "";
        ?>
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Período:  
        <select name="periodo" id="periodo" style="width:110px" > 
            <?php 
                $daoPer=new PeriodoDaoJdbc();
                $listaPer = $daoPer->obtieneListado();
                $elementoPer = new Periodo();
                foreach($listaPer as $elementoPer){
                    $sel = "";
                    if( isset($_GET['periodo']) && (int)( $_GET['periodo'] ) == $elementoPer->getId()) $sel = "selected='selected'"; 
                    else if( $_GET['periodo'] == null && (int)($elementoPer->getPeriodo()) == date('Y')  ){ $sel = "selected='selected'";  $periodo = (String)($elementoPer->getId() ); }
                    echo("<option value=".$elementoPer->getId()." ".$sel." >" . $elementoPer->getPeriodo() ."</option>");
                }
            ?>  
        </select>

        &nbsp; <input name="Filtro" style="cursor:pointer" type="submit" value="Filtrar"  class='btn' />
    </form>

    <br/>	
    <table  width="95%" border="1" cellspacing="0" cellpadding="3" class='tb_presupuestoResumen' align='center' class="SizeText">
        <tr >
            <th width="25%" >Destino</th>
            <th width="9.5%">Autorizado</th>
            <th width="9.5%">Ministrado</th>
            <th width="9.5%" >Por Ministrar</th>
            <th width="9.5%" >Gastado</th>
            <th width="9.5%" >Por gastar ministrado</th>
            <th width="9.5%" >Por gastar</th>
            <th width="9.5%" >Comprobado (INAH)</th>
            <th width="10%" >Por comprobar (INAH)</th>
        </tr>
	
        <?php
            if(isset($_GET['periodo']))  
            {    $periodo = (String)$_GET['periodo']; }
            else {
                $aniosel = (String)(date('Y'));
                $per=new PeriodoDaoJdbc();

                $periodo=$per->obtieneidElemento($aniosel);	        
            } 	  	

            $dao=new ConsultaResumenDaoJdbc(); //Traemos todos los nombres de proyecto
            $lista=$dao->obtieneListado($periodo);
            $elemento=new ConsultaResumen();

            $totAutorizado=0.0;	//variables para $totales
            $totMinistrado=0.0;
            $totPorMinistrar=0.0;
            $totGastado=0.0;
            $totPorGastarMinistrado=0.0;
            $totPorGastar=0.0;
            $totComprobadoINAH=0.0;
            $totPorComprobarINAH=0.0;

            $totAutorizadoP=0.0;	//variables para $totales
            $totMinistradoP=0.0;
            $totPorMinistrarP=0.0;
            $totGastadoP=0.0;
            $totPorGastarMinistradoP=0.0;
            $totPorGastarP=0.0;
            $totComprobadoINAHP=0.0;
            $totPorComprobarINAHP=0.0;

            $indexMetas = 1;
            $indexPartidas = 1;
            $idMinistracion = 0;



            foreach($lista as $elemento){

                $elemento->setPorMinistrar($elemento->getAutorizado()-$elemento->getMinistrado());		//se calculan los campos que se tienen que calcular
                $elemento->setPorGastarMinistrado($elemento->getMinistrado()-$elemento->getGastado());
                $elemento->setPorGastar($elemento->getAutorizado()-$elemento->getGastado());
                $elemento->setPorComprobarINAH($elemento->getAutorizado()-$elemento->getComprobadoINAH());

                $totAutorizado=$totAutorizado+$elemento->getAutorizado();								//Se sumarizan para $totales
                $totMinistrado=$totMinistrado+$elemento->getMinistrado();
                $totPorMinistrar=$totPorMinistrar+$elemento->getPorMinistrar();
                $totGastado=$totGastado+$elemento->getGastado();
                $totPorGastarMinistrado=$totPorGastarMinistrado+$elemento->getPorGastarMinistrado();
                $totPorGastar=$totPorGastar+$elemento->getPorGastar();
                $totComprobadoINAH=$totComprobadoINAH+$elemento->getComprobadoINAH();
                $totPorComprobarINAH=$totPorComprobarINAH+$elemento->getPorComprobarINAH();

                if($elemento->getId()>0){
                    $totAutorizadoP=$totAutorizadoP+$elemento->getAutorizado();								//Se sumarizan para $totales
                    $totMinistradoP=$totMinistradoP+$elemento->getMinistrado();
                    $totPorMinistrarP=$totPorMinistrarP+$elemento->getPorMinistrar();
                    $totGastadoP=$totGastadoP+$elemento->getGastado();
                    $totPorGastarMinistradoP=$totPorGastarMinistradoP+$elemento->getPorGastarMinistrado();
                    $totPorGastarP=$totPorGastarP+$elemento->getPorGastar();
                    $totComprobadoINAHP=$totComprobadoINAHP+$elemento->getComprobadoINAH();
                    $totPorComprobarINAHP=$totPorComprobarINAHP+$elemento->getPorComprobarINAH();

                }

                if($elemento->getId()==0){
        ?>
        <tr >
            <th align="right"  >Total de Proyectos:</th>
            <th align="right"  ><?php echo("$".number_format($totAutorizadoP,2));?></th>
            <th align="right"  ><?php echo("$".number_format($totMinistradoP,2));?></th>
            <th align="right"  ><?php echo("$".number_format($totPorMinistrarP,2));?></th>
            <th align="right"  ><?php echo("$".number_format($totGastadoP,2));?></th>
            <th align="right"  ><?php echo("$".number_format($totPorGastarMinistradoP,2));?></th>
            <th align="right"  ><?php echo("$".number_format($totPorGastarP,2));?></th>
            <th align="right"  ><?php echo("$".number_format($totComprobadoINAHP,2));?></th>
            <th align="right"  ><?php echo("$".number_format($totPorComprobarINAHP,2));?></th>
        </tr>
        <?php			
            }
        ?>
        <tr >
            <th align="left" >
            <?php		
                if($elemento->getId()==0){
                    echo(" <img title='Ver Partidas' src='../img/abrir.png' width='14' height='14' style='cursor:pointer' id='abrirPartidasgb' onclick='mostrarPartidas(gb)'  > <img title='Ocultar Partidas' style='display:none;cursor:pointer' id='cerrarPartidasgb' src='../img/cerrar.png' width='12' height='10' style='cursor:pointer' onclick='ocultarPartidas(gb)' >");
                }		
                else{
            ?>
            <img src="../img/abrir.png" title="Mostrar Metas" width="14" height="14" style="cursor:pointer" id="abrir<?php echo($elemento->getId()); ?>" onclick="mostrarMetas(<?php echo($elemento->getId()); ?>)"  > <img style="display:none;cursor:pointer" title="Ocultar Metas" id="cerrar<?php echo($elemento->getId()); ?>" src="../img/cerrar.png" width="12" height="10" style="cursor:pointer" onclick="ocultarMetas(<?php echo($elemento->getId()); ?>)"  >
            <?php
                }
            ?>
            &nbsp;  <?php echo($elemento->getProyecto() );?></th>
            <td align="right"><?php echo("$".number_format($elemento->getAutorizado(),2));?></td>
            <td align="right"><?php echo("$".number_format($elemento->getMinistrado(),2));?></td>
            <td align="right"><?php echo("$".number_format($elemento->getPorMinistrar(),2));?></td>
            <td align="right"><?php echo("$".number_format($elemento->getGastado(),2));?></td>
            <td align="right"><?php echo("$".number_format($elemento->getPorGastarMinistrado(),2));?></td>
            <td align="right"><?php echo("$".number_format($elemento->getPorGastar(),2));?></td>
            <td align="right">
            <?php 

                if($elemento->getId()==-2 || $elemento->getId()==-3 ){
                    echo(" ");
                }
                else{
                    echo("$".number_format($elemento->getComprobadoINAH(),2));
                }	
            ?></td>
            <td align="right">
            <?php
                if($elemento->getId()==-2 || $elemento->getId()==-3 ){
                        echo(" ");
                }
                else{
                        echo("$".number_format($elemento->getPorComprobarINAH(),2));
                }	
            ?></td>
        </tr>
        <?php		
            if($elemento->getId()==0){
                $partidasLt = $dao->obtienePartidas("gb");
                $partidas =new ConsultaResumen();

                foreach($partidasLt as $partidas){
                        //$partidas->setPorMinistrar($partidas->getAutorizado()-$partidas->getMinistrado());

                    echo("<tr id='gb' style='display:none'  >");
                    echo("<th align='left' style='cursor:pointer' > &nbsp; &nbsp; &nbsp; &nbsp; <acronym title='".$partidas->getDescPartida()."' > Partida: ".$partidas->getPartida()."</acronym> </th>");
                    echo("<td align='right' >"."$".number_format($partidas->getAutorizado(),2)."</td>");
                    echo("<td align='right' ></td>");
                    echo("<td align='right' ></td>");
                    //echo("<td align='right' >"+"$".number_format($partidas->getMinistrado())+"</td>");
                    //echo("<td align='right' >"+"$".number_format($partidas->getPorMinistrar())+"</td>");
                    echo("<td align='right' >"."$".number_format($partidas->getGastado(),2)."</td>");
                    echo("<td align='right' ></td>");
                    echo("<td align='right' ></td>");
                    echo("<td align='right' ></td>");
                    echo("<td align='right' ></td>");
                    echo("</tr>");

                    $indexPartidas = $indexPartidas + 1;

                }
			
                echo("<input type='hidden' value='".$indexPartidas."' id='".$indexMetas."".$elemento->getId()."'/> ");
		
			
		}		
		else{
                    $metasLt = $dao->obtieneMetas((String)($elemento->getId()));		
                    $metas =new ConsultaResumen();

                    foreach($metasLt as $metas){
                        $metas->setPorGastar($metas->getAutorizado()-$metas->getGastado());

                        echo("<tr id='".$elemento->getId()."".$indexMetas."' style='display:none' >");
                        echo("<th align='left'> &nbsp; <img title='Ver Partidas' src='../img/abrir.png' width='14' height='14' style='cursor:pointer' id='abrirPartidas".$indexMetas."".$elemento->getId()."' onclick='mostrarPartidas(".$indexMetas."".$elemento->getId().")'  > <img title='Ocultar Partidas' style='display:none;cursor:pointer' id='cerrarPartidas".$indexMetas."".$elemento->getId()."' src='../img/cerrar.png' width='12' height='10' style='cursor:pointer' onclick='ocultarPartidas(".$indexMetas."".$elemento->getId().")' > &nbsp; ".$metas->getMeta()."</th>");
                        echo("<td align='right' >"."$".number_format($metas->getAutorizado(),2)."</td>");
                        echo("<td align='right' ></td>");
                        echo("<td align='right' ></td>");
                        echo("<td align='right' >"."$".number_format($metas->getGastado(),2)."</td>");
                        echo("<td align='right' ></td>");
                        echo("<td align='right' >"."$".number_format($metas->getPorGastar(),2)."</td>");
                        echo("<td align='right' ></td>");
                        echo("<td align='right' ></td>");
                        echo("</tr>");

                        $partidasLt = $dao->obtienePartidas((String)($metas->getIdMeta()));
                        $partidas = new ConsultaResumen();

			foreach($partidasLt as $partidas){
                            //$partidas->setPorMinistrar($partidas->getAutorizado()-$partidas->getMinistrado());

                            echo("<tr id='".$indexMetas."".$elemento->getId()."".$indexPartidas."' style='display:none'  >");
                            echo("<th align='left' style='cursor:pointer' > &nbsp; &nbsp; &nbsp; &nbsp; <acronym title='".$partidas->getDescPartida()."' > Partida: ".$partidas->getPartida()."</acronym> </th>");
                            echo("<td align='right' >"."$".number_format($partidas->getAutorizado(),2)."</td>");
                            echo("<td align='right' ></td>");
                            echo("<td align='right' ></td>");
                            //echo("<td align='right' >"+"$".number_format($partidas->getMinistrado())+"</td>");
                            //echo("<td align='right' >"+"$".number_format($partidas->getPorMinistrar())+"</td>");
                            echo("<td align='right' >"."$".number_format($partidas->getGastado(),2)."</td>");
                            echo("<td align='right' ></td>");
                            echo("<td align='right' ></td>");
                            echo("<td align='right' ></td>");
                            echo("<td align='right' ></td>");
                            echo("</tr>");

                            $indexPartidas = $indexPartidas + 1;
						
						/*if( idMinistracion != $partidas->getIdMinistracion()){
							List<ConsultaResumen> comprobacionLt = dao.obtienePartidasComprobadas( $partidas->getIdMinistracion() );
							ConsultaResumen comprobacion =new ConsultaResumen();
							Iterator<ConsultaResumen> itcomprobacion = comprobacionLt.iterator();
							
							while(itcomprobacion.hasNext()){
								comprobacion = itcomprobacion.next();
								
								echo("<tr id='"+indexMetas+""+$elemento->getId()+""+indexPartidas+"' style='display:none'  >");
								echo("<th align='left' style='cursor:pointer' > &nbsp; &nbsp; &nbsp; &nbsp; <acronym title='"+comprobacion.getDescPartida()+"' > Partida: "+comprobacion.getPartida()+"</acronym> </th>");
								echo("<td align='right' ></td>");
								echo("<td align='right' ></td>");
								echo("<td align='right' ></td>");
								echo("<td align='right' ></td>");
								echo("<td align='right' ></td>");
								echo("<td align='right' ></td>");
								echo("<td align='right' >"+"$".number_format(comprobacion.getComprobadoINAH())+"</td>");
								echo("<td align='right' ></td>");
								echo("</tr>");
								
								indexPartidas = indexPartidas + 1;
								idMinistracion = $partidas->getIdMinistracion();
							}
						}*/
						
												
                        }
					
                        echo("<input type='hidden' value='".$indexPartidas."' id='".$indexMetas."".$elemento->getId()."'/> ");

                        $indexMetas = $indexMetas + 1;
                    }
                    echo("<input type='hidden' value='".$indexMetas."' id='".$elemento->getId()."'/> ");
                }
	   }//while
        ?>
	
        <tr >
            <th align="right"  >Totales:</th>
            <th align="right"  ><?php echo("$".number_format($totAutorizado,2));?></th>
            <th align="right"  ><?php echo("$".number_format($totMinistrado,2));?></th>
            <th align="right"  ><?php echo("$".number_format($totPorMinistrar,2));?></th>
            <th align="right"  ><?php echo("$".number_format($totGastado,2));?></th>
            <th align="right"  ><?php echo("$".number_format($totPorGastarMinistrado,2));?></th>
            <th align="right"  ><?php echo("$".number_format($totPorGastar,2));?></th>
            <th align="right"  ><?php echo("$".number_format($totComprobadoINAH,2));?></th>
            <th align="right"  ><?php echo("$".number_format($totPorComprobarINAH,2));?></th>
        </tr>

    </table>
</div>
<script>
	function mostrarMetas(id){
		   var metas = parseInt($("#"+id).val());
		   for( i=1;i<metas;i++ ){
			  var nombre = id+""+i;
			  $("#"+nombre).show();
		   }
		   $("#cerrar"+id).show();
		   $("#abrir"+id).hide();
	   }
	   
	   function mostrarPartidas(id){
		   var partidas = parseInt($("#"+id).val());
		   
		   for( i=1;i<partidas;i++ ){
			  var nombre = id+""+i;
			  $("#"+nombre).show();
		    }
		   
		   $("#cerrarPartidas"+id).show();
		   $("#abrirPartidas"+id).hide();
	   }
	   
	   function ocultarMetas(id){
		   var metas = parseInt($("#"+id).val());
	
		   
		   for( k=1;k<metas;k++ ){
			  var nombre = id+""+k;
			  $("#"+nombre).hide();
			  
			  ocultarPartidas(k+""+id);
		   }
		   
		   $("#cerrar"+id).hide();
		   $("#abrir"+id).show();
	   }
	   
	   function ocultarPartidas(id){
		   var partidas = parseInt($("#"+id).val());
		   for( i=1;i<partidas;i++ ){
			  var nombre = id+""+i;
			  $("#"+nombre).hide();
		   }
		   
		   $("#cerrarPartidas"+id).hide();
		   $("#abrirPartidas"+id).show();
	   }
</script>
</body>
</html>