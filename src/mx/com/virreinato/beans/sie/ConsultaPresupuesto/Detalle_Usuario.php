<?php

$id = null;
$user = null;

session_start();
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/ConsultaResumenDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ConsultaResumen.class.php");
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}else{
    if(isset($_SESSION['id'])){ $id = $_SESSION['id'];}
    if(isset($_SESSION['user'])){ $user = $_SESSION['user']; }
}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
<title>Presupuesto Resumen</title>
</head>
<body>
<div class="contenido">
    <br/>
    <p  class="titulo_cat1">Consulta de Presupuesto > Detalle </p>
    <!--  p class="titulo_cat1" --> 
        <form name="frmFiltroDetalle" id="frmFiltroDetalle" method="POST" action="../src/mx/com/virreinato/web/WebConsultaPresupuestoDet.php" >
        <?php  
            $periodo = "";
        ?>
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Período:  
        <select name="periodo" id="periodo" style="width:110px" > 
            <?php 
                $daoPer=new PeriodoDaoJdbc();
                $listaPer = $daoPer->obtieneListado();
                $elementoPer = new Periodo();
                foreach($listaPer as $elementoPer){
                    $sel = "";
                    if( isset($_GET['periodo']) && (int)( $_GET['periodo'] ) == $elementoPer->getId()) $sel = "selected='selected'"; 
                    else if( $_GET['periodo'] == null && (int)($elementoPer->getPeriodo()) == date('Y')  ){ $sel = "selected='selected'";  $periodo = (String)($elementoPer->getId() ); }
                    echo("<option value=".$elementoPer->getId()." ".$sel." >" . $elementoPer->getPeriodo() ."</option>");
                }
            ?>  
        </select>
        &nbsp; <input name="Filtro" style="cursor:pointer" type="submit" value="Filtrar"  class='btn' />
    </form>
    
    <br>
    
    <table width="95%" border="1" cellspacing="0" cellpadding="5" class='tb_presupuesto' align='center' >
        <tr >
            <th>Proyectos</th>
            <th>Autorizado</th>
            <th>Enero</th>
            <th>Febrero</th>
            <th>Marzo</th>
            <th>Abril</th>
            <th>Mayo</th>
            <th>Junio</th>
            <th>Julio</th>
            <th>Agosto</th>
            <th>Septiembre</th>
            <th>Octubre</th>
            <th>Noviembre</th>
            <th>Diciembre</th>
        </tr>
    </table>
    </div>
    <script>
	   function mostrarPartidas(id){
		   var partidas = parseInt($("#"+id).val());
		   for( i=1;i<partidas;i++ ){
			  var nombre = id+""+i;
			  $("#"+nombre).show();
		   }
		   $("#cerrar"+id).show();
		   $("#abrir"+id).hide();
	   }
	   
	      
	   function ocultarPartidas(id){
		   var metas = parseInt($("#"+id).val());
		   for( k=1;k<metas;k++ ){
			  var nombre = id+""+k;
			  $("#"+nombre).hide();
			}
		   
		   $("#cerrar"+id).hide();
		   $("#abrir"+id).show();
	   }
	   
	   
</script>
</body>
</html>