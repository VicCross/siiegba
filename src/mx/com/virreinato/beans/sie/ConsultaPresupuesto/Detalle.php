<?php

$id = null;
$user = null;

session_start();
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/ConsultaResumenDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ConsultaResumen.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}else{
    if(isset($_SESSION['id'])){ $id = $_SESSION['id'];}
    if(isset($_SESSION['user'])){ $user = $_SESSION['user']; }
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html>
<html>
    <body>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="../css/style.css" />
    <script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
	<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
    <title>Presupuesto Resumen</title>
    </head>
    <body>
    <div class="contenido">
        <br/>
        <p  class="titulo_cat1">Consulta de Presupuesto > Detalle</p>
        <form name="frmFiltroResumen" id="frmFiltroResmuen" method="POST" action="../src/mx/com/virreinato/web/WebConsultaPresupuestoDet.php" >
        <?php  
            $periodo = "";
        ?>
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Período:  
        <select name="periodo" id="periodo" style="width:110px" > 
            <?php 
                $daoPer=new PeriodoDaoJdbc();
                $listaPer = $daoPer->obtieneListado();
                $elementoPer = new Periodo();
                foreach($listaPer as $elementoPer){
                    $sel = "";
                    if( isset($_GET['periodo']) && (int)( $_GET['periodo'] ) == $elementoPer->getId()) $sel = "selected='selected'"; 
                    else if( $_GET['periodo'] == null && (int)($elementoPer->getPeriodo()) == date('Y')  ){ $sel = "selected='selected'";  $periodo = (String)($elementoPer->getId() ); }
                    echo("<option value=".$elementoPer->getId()." ".$sel." >" . $elementoPer->getPeriodo() ."</option>");
                }
            ?>  
        </select>

        &nbsp; <input name="Filtro" style="cursor:pointer" type="submit" value="Filtrar"  class='btn' />
    </form>
        
    <br/>
    
    <table width="95%" border="1" cellspacing="0" cellpadding="5" class='tb_presupuesto' align='center' >
        <tr >
            <th>Proyectos</th>
            <th>Autorizado</th>
            <th>Enero</th>
            <th>Febrero</th>
            <th>Marzo</th>
            <th>Abril</th>
            <th>Mayo</th>
            <th>Junio</th>
            <th>Julio</th>
            <th>Agosto</th>
            <th>Septiembre</th>
            <th>Octubre</th>
            <th>Noviembre</th>
            <th>Diciembre</th>
        </tr>
        
        <?php
            if(isset($_GET['periodo']))  
            {    $periodo = (String)$_GET['periodo']; }
            else {
                $aniosel = (String)(date('Y'));
                $per=new PeriodoDaoJdbc();

                $periodo=$per->obtieneidElemento($aniosel);	        
            } 	  	

            $dao=new ConsultaResumenDaoJdbc(); //Traemos todos los nombres de proyecto
            $lista=$dao->obtieneListado($periodo);
            $elemento=new ConsultaResumen();
            
            $totAutorizado=0.0;	//variables para$totales
            $totEne=0.0;
            $totFeb=0.0;
            $totMar=0.0;
            $totAbr=0.0;
            $totMay=0.0;
            $totJun=0.0;
            $totJul=0.0;
            $totAgo=0.0;
            $totSep=0.0;
            $totOct=0.0;
            $totNov=0.0;
            $totDic=0.0;

            $totAutorizadoP=0.0;	//variables para$totales
            $totEneP=0.0;
            $totFebP=0.0;
            $totMarP=0.0;
            $totAbrP=0.0;
            $totMayP=0.0;
            $totJunP=0.0;
            $totJulP=0.0;
            $totAgoP=0.0;
            $totSepP=0.0;
            $totOctP=0.0;
            $totNovP=0.0;
            $totDicP=0.0;


            $indexMetas = 1;
            $indexPartidas = 1;

            foreach($lista as $elemento){
                
               $totAutorizado=$totAutorizado+$elemento->getAutorizado();								//Se sumarizan para$totales
               $totEne=$totEne+$elemento->getAutEne();
               $totFeb=$totFeb+$elemento->getAutFeb();
               $totMar=$totMar+$elemento->getAutMar();
               $totAbr=$totAbr+$elemento->getAutAbr();
               $totMay=$totMay+$elemento->getAutMay();
               $totJun=$totJun+$elemento->getAutJun();
               $totJul=$totJul+$elemento->getAutJul();
               $totAgo=$totAgo+$elemento->getAutAgo();
               $totSep=$totSep+$elemento->getAutSep();
               $totOct=$totOct+$elemento->getAutOct();
               $totNov=$totNov+$elemento->getAutNov();
               $totDic=$totDic+$elemento->getAutDic();

                if($elemento->getId()>0){

                    $totAutorizadoP=$totAutorizadoP+$elemento->getAutorizado();								//Se sumarizan para$totales
                    $totEneP=$totEneP+$elemento->getAutEne();
                    $totFebP=$totFebP+$elemento->getAutFeb();
                    $totMarP=$totMarP+$elemento->getAutMar();
                    $totAbrP=$totAbrP+$elemento->getAutAbr();
                    $totMayP=$totMayP+$elemento->getAutMay();
                    $totJunP=$totJunP+$elemento->getAutJun();
                    $totJulP=$totJulP+$elemento->getAutJul();
                    $totAgoP=$totAgoP+$elemento->getAutAgo();
                    $totSepP=$totSepP+$elemento->getAutSep();
                    $totOctP=$totOctP+$elemento->getAutOct();
                    $totNovP=$totNovP+$elemento->getAutNov();
                    $totDicP=$totDicP+$elemento->getAutDic();

                }
                    if($elemento->getId()==0){
        ?>
        <tr>
            <th align="right">Total de Proyectos:</th>
            <th align="right"><?php echo(number_format($totAutorizadoP,2));?></th>
            <th align="right"><?php echo(number_format($totEneP,2));?></th>
            <th align="right"><?php echo(number_format($totFebP,2));?></th>
            <th align="right"><?php echo(number_format($totMarP,2));?></th>
            <th align="right"><?php echo(number_format($totAbrP,2));?></th>
            <th align="right"><?php echo(number_format($totMayP,2));?></th>
            <th align="right"><?php echo(number_format($totJunP,2));?></th>
            <th align="right"><?php echo(number_format($totJulP,2));?></th>
            <th align="right"><?php echo(number_format($totAgoP,2));?></th>
            <th	align="right"><?php echo(number_format($totSepP,2));?></th>
            <th align="right"><?php echo(number_format($totOctP,2));?></th>
            <th align="right"><?php echo(number_format($totNovP,2));?></th>
            <th align="right"><?php echo(number_format($totDicP,2));?></th>
        </tr>
        <?php
            }
        ?>
        <tr>
            <th align="left"><img title="Ver Metas" src="../img/abrir.png" width="14" height="14" style="cursor:pointer" id="abrir<?php echo($elemento->getId()); ?>" onclick="mostrarMetas(<?php echo($elemento->getId()); ?>)"  > <img title="Ocultar Metas" style="display:none;cursor:pointer" id="cerrar<?php echo($elemento->getId()); ?>" src="../img/cerrar.png" width="12" height="10" style="cursor:pointer" onclick="ocultarMetas(<?php echo($elemento->getId()); ?>,<?php echo$indexMetas; ?>,<?php echo$indexPartidas; ?>)"  > &nbsp; <?php echo($elemento->getProyecto());?></th>
            <td align="right"><?php echo(number_format($elemento->getAutorizado(),2));?></td>
            <td align="right"><?php echo(number_format($elemento->getAutEne(),2));?></td>
            <td align="right"><?php echo(number_format($elemento->getAutFeb(),2));?></td>
            <td align="right"><?php echo(number_format($elemento->getAutMar(),2));?></td>
            <td align="right"><?php echo(number_format($elemento->getAutAbr(),2));?></td>
            <td align="right"><?php echo(number_format($elemento->getAutMay(),2));?></td>
            <td align="right"><?php echo(number_format($elemento->getAutJun(),2));?></td>
            <td align="right"><?php echo(number_format($elemento->getAutJul(),2));?></td>
            <td align="right"><?php echo(number_format($elemento->getAutAgo(),2));?></td>
            <td align="right"><?php echo(number_format($elemento->getAutSep(),2));?></td>
            <td align="right"><?php echo(number_format($elemento->getAutOct(),2));?></td>
            <td align="right"><?php echo(number_format($elemento->getAutNov(),2));?></td>
            <td	 align="right"><?php echo(number_format($elemento->getAutDic(),2));?></td>
        </tr>
        <?php
            $metasLt = $dao->obtieneMetas((String)($elemento->getId()));		
            $metas =new ConsultaResumen();

            foreach($metasLt as $metas){
                echo("<tr id='".$elemento->getId()."".$indexMetas."' style='display:none'  >");
                echo("<th align='left'> &nbsp; <img title='Ver Partidas' src='../img/abrir.png' width='14' height='14' style='cursor:pointer' id='abrirPartidas".$indexMetas."".$elemento->getId()."' onclick='mostrarPartidas(".$indexMetas."".$elemento->getId().")'  > <img title='Ocultar Partidas' style='display:none;cursor:pointer' id='cerrarPartidas".$indexMetas."".$elemento->getId()."' src='../img/cerrar.png' width='12' height='10' style='cursor:pointer' onclick='ocultarPartidas(".$indexMetas."".$elemento->getId().")' > &nbsp; ".$metas->getMeta()."</th>");
                echo("<td align='right' >".number_format($metas->getAutorizado(),2)."</td>");
                echo("<td align='right' >".number_format($metas->getAutEne(),2)."</td>");
                echo("<td align='right' >".number_format($metas->getAutFeb(),2)."</td>");
                echo("<td align='right' >".number_format($metas->getAutMar(),2)."</td>");
                echo("<td align='right' >".number_format($metas->getAutAbr(),2)."</td>");
                echo("<td align='right' >".number_format($metas->getAutMay(),2)."</td>");
                echo("<td align='right' >".number_format($metas->getAutJun(),2)."</td>");
                echo("<td align='right' >".number_format($metas->getAutJul(),2)."</td>");
                echo("<td align='right' >".number_format($metas->getAutAgo(),2)."</td>");
                echo("<td align='right' >".number_format($metas->getAutSep(),2)."</td>");
                echo("<td align='right' >".number_format($metas->getAutOct(),2)."</td>");
                echo("<td align='right' >".number_format($metas->getAutNov(),2)."</td>");
                echo("<td align='right' >".number_format($metas->getAutDic(),2)."</td>");
                echo("</tr>");

                $partidasLt = $dao->obtienePartidas((String)($metas->getIdMeta()));
                $partidas = new ConsultaResumen();

                foreach($partidasLt as $partidas){
                    echo("<tr id='".$indexMetas."".$elemento->getId()."".$indexPartidas."' style='display:none'  >");
                    echo("<th align='left' style='cursor:pointer' > &nbsp; &nbsp; &nbsp; &nbsp; <acronym title='".$partidas->getDescPartida()."' > Partida: ".$partidas->getPartida()."</acronym> </th>");
                    echo("<td align='right' >".number_format($partidas->getAutorizado(),2)."</td>");
                    echo("<td align='right' >".number_format($partidas->getAutEne(),2)."</td>");
                    echo("<td align='right' >".number_format($partidas->getAutFeb(),2)."</td>");
                    echo("<td align='right' >".number_format($partidas->getAutMar(),2)."</td>");
                    echo("<td align='right' >".number_format($partidas->getAutAbr(),2)."</td>");
                    echo("<td align='right' >".number_format($partidas->getAutMay(),2)."</td>");
                    echo("<td align='right' >".number_format($partidas->getAutJun(),2)."</td>");
                    echo("<td align='right' >".number_format($partidas->getAutJul(),2)."</td>");
                    echo("<td align='right' >".number_format($partidas->getAutAgo(),2)."</td>");
                    echo("<td align='right' >".number_format($partidas->getAutSep(),2)."</td>");
                    echo("<td align='right' >".number_format($partidas->getAutOct(),2)."</td>");
                    echo("<td align='right' >".number_format($partidas->getAutNov(),2)."</td>");
                    echo("<td align='right' >".number_format($partidas->getAutDic(),2)."</td>");
                    echo("</tr>");


                    $indexPartidas = $indexPartidas + 1;
												
                }
                
                echo("<input type='hidden' value='".$indexPartidas."' id='".$indexMetas."".$elemento->getId()."'/> ");

                $indexMetas = $indexMetas + 1;
                
            }
            
            echo("<input type='hidden' value='".$indexMetas."' id='".$elemento->getId()."'/> ");
            }//while
        ?>

        <tr>
            <th align="right">Totales:</th>
            <th align="right"><?php echo(number_format($totAutorizado,2));?></th>
            <th align="right"><?php echo(number_format($totEne,2));?></th>
            <th align="right"><?php echo(number_format($totFeb,2));?></th>
            <th align="right"><?php echo(number_format($totMar,2));?></th>
            <th align="right"><?php echo(number_format($totAbr,2));?></th>
            <th align="right"><?php echo(number_format($totMay,2));?></th>
            <th align="right"><?php echo(number_format($totJun,2));?></th>
            <th align="right"><?php echo(number_format($totJul,2));?></th>
            <th align="right"><?php echo(number_format($totAgo,2));?></th>
            <th	align="right"><?php echo(number_format($totSep,2));?></th>
            <th align="right"><?php echo(number_format($totOct,2));?></th>
            <th align="right"><?php echo(number_format($totNov,2));?></th>
            <th align="right"><?php echo(number_format($totDic,2));?></th>
        </tr>	
    </table>
    </div>
<script>
    function mostrarMetas(id){
            var metas = parseInt($("#"+id).val());
            for( i=1;i<metas;i++ ){
                   var nombre = id+""+i;
                   $("#"+nombre).show();
            }
            $("#cerrar"+id).show();
            $("#abrir"+id).hide();
    }

    function mostrarPartidas(id){
            var partidas = parseInt($("#"+id).val());

            for( i=1;i<partidas;i++ ){
                   var nombre = id+""+i;
                   $("#"+nombre).show();
             }

            $("#cerrarPartidas"+id).show();
            $("#abrirPartidas"+id).hide();
    }

    function ocultarMetas(id, idMetas, idPartidas){
            var metas = parseInt($("#"+id).val());


            for( k=1;k<metas;k++ ){
                   var nombre = id+""+k;
                   $("#"+nombre).hide();

                   ocultarPartidas(k+""+id);
            }

            $("#cerrar"+id).hide();
            $("#abrir"+id).show();
    }

    function ocultarPartidas(id){
            var partidas = parseInt($("#"+id).val());
            for( i=1;i<partidas;i++ ){
                   var nombre = id+""+i;
                   $("#"+nombre).hide();
            }

            $("#cerrarPartidas"+id).hide();
            $("#abrirPartidas"+id).show();
    }
</script>
</body>
</html>