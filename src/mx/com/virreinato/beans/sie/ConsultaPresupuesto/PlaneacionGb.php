<?php

$id = null;
$user = null;

session_start();
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/ConsultaResumenDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ConsultaResumen.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}else{
    if(isset($_SESSION['id'])){ $id = $_SESSION['id'];}
    if(isset($_SESSION['user'])){ $user = $_SESSION['user']; }
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="../css/style.css" />
    <script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
	<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
    <title>Planeación Gasto Básico</title>
    </head>
    <body>
    <div class="contenido">
        <br/>
        <p  class="titulo_cat1">Consulta de Presupuesto > Planeación Gasto Básico</p>
        <!-- p class="titulo_cat1" -->
            <form name="frmFiltroDetalle" id="frmFiltroDetalle" method="POST" action="../src/mx/com/virreinato/web/WebConsultaPresupuestoDet.php" >
            <?php  
            $periodo = "";
            ?>
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Período:  
            <select name="periodo" id="periodo" style="width:110px" > 
                <?php 
                    $daoPer=new PeriodoDaoJdbc();
                    $listaPer = $daoPer->obtieneListado();
                    $elementoPer = new Periodo();
                    foreach($listaPer as $elementoPer){
                        $sel = "";
                        if( isset($_GET['periodo']) && (int)( $_GET['periodo'] ) == $elementoPer->getId()) $sel = "selected='selected'"; 
                        else if( $_GET['periodo'] == null && (int)($elementoPer->getPeriodo()) == date('Y')  ){ $sel = "selected='selected'";  $periodo = (String)($elementoPer->getId() ); }
                        echo("<option value=".$elementoPer->getId()." ".$sel." >" . $elementoPer->getPeriodo() ."</option>");
                    }
                ?>  
            </select>
            
            <input name="filtro_gb" style="cursor:pointer" type="hidden" value="filtrar" />
            &nbsp; <input name="FiltroGB" style="cursor:pointer" type="submit" value="Filtrar"  class='btn' />
            </form>
	    <!-- /p -->
        <br/>
        
        <table width="95%" border="1" cellspacing="0" cellpadding="5" class='tb_presupuesto' align='center' >
            <tr >
                <th></th>
                <th>Enero</th>
                <th>Febrero</th>
                <th>Marzo</th>
                <th>Abril</th>
                <th>Mayo</th>
                <th>Junio</th>
                <th>Julio</th>
                <th>Agosto</th>
                <th>Septiembre</th>
                <th>Octubre</th>
                <th>Noviembre</th>
                <th>Diciembre</th>
            </tr>
            
        <?php
            if(isset($_GET['periodo']))  
            {    $periodo = (String)$_GET['periodo']; }
            else {
                $aniosel = (String)(date('Y'));
                $per=new PeriodoDaoJdbc();

                $periodo=$per->obtieneidElemento($aniosel);	        
            } 	  	

            $dao=new ConsultaResumenDaoJdbc(); //Traemos todos los nombres de proyecto
            $lista=$dao->obtienePlaneacionGb($periodo);
            $elemento=new ConsultaResumen();
            
            $totautEne=0.0;
            $totautFeb=0.0;
            $totautMar=0.0;
            $totautAbr=0.0;
            $totautMay=0.0;
            $totautJun=0.0;
            $totautJul=0.0;
            $totautAgo=0.0;
            $totautSep=0.0;
            $totautOct=0.0;
            $totautNov=0.0;
            $totautDic=0.0;		

            $totminEne=0.0;
            $totminFeb=0.0;
            $totminMar=0.0;
            $totminAbr=0.0;
            $totminMay=0.0;
            $totminJun=0.0;
            $totminJul=0.0;
            $totminAgo=0.0;
            $totminSep=0.0;
            $totminOct=0.0;
            $totminNov=0.0;
            $totminDic=0.0;
			
            foreach($lista as $elemento){
				 
        ?>
        <tr>
            <th align="left"> <?php echo($elemento->getProyecto());?></th>
            <td align="right"><?php echo("$".number_format($elemento->getAutEne(),2));?></td>
            <td align="right"><?php echo("$".number_format($elemento->getAutFeb(),2));?></td>
            <td align="right"><?php echo("$".number_format($elemento->getAutMar(),2));?></td>
            <td align="right"><?php echo("$".number_format($elemento->getAutAbr(),2));?></td>
            <td align="right"><?php echo("$".number_format($elemento->getAutMay(),2));?></td>
            <td align="right"><?php echo("$".number_format($elemento->getAutJun(),2));?></td>
            <td align="right"><?php echo("$".number_format($elemento->getAutJul(),2));?></td>
            <td align="right"><?php echo("$".number_format($elemento->getAutAgo(),2));?></td>
            <td align="right"><?php echo("$".number_format($elemento->getAutSep(),2));?></td>
            <td align="right"><?php echo("$".number_format($elemento->getAutOct(),2));?></td>
            <td align="right"><?php echo("$".number_format($elemento->getAutNov(),2));?></td>
            <td	 align="right"><?php echo("$".number_format($elemento->getAutDic(),2));?></td>
        </tr>
        <?php	

            if($elemento->getNumProyecto() == 1){

                //disponible
                $totautEne=$elemento->getAutEne();
                $totautFeb=$elemento->getAutFeb();
                $totautMar=$elemento->getAutMar();
                $totautAbr=$elemento->getAutAbr();
                $totautMay=$elemento->getAutMay();
                $totautJun=$elemento->getAutJun();
                $totautJul=$elemento->getAutJul();
                $totautAgo=$elemento->getAutAgo();
                $totautSep=$elemento->getAutSep();
                $totautOct=$elemento->getAutOct();
                $totautNov=$elemento->getAutNov();
                $totautDic=$elemento->getAutDic();
            }
				
            if($elemento->getNumProyecto() == 2){
                //ministrado
                $totacEne=$elemento->getAutEne();
                $totacFeb=$totacEne+$elemento->getAutFeb();
                $totacMar=$totacFeb+$elemento->getAutMar();
                $totacAbr=$totacMar+$elemento->getAutAbr();
                $totacMay=$totacAbr+$elemento->getAutMay();
                $totacJun=$totacMay+$elemento->getAutJun();
                $totacJul=$totacJun+$elemento->getAutJul();
                $totacAgo=$totacJul+$elemento->getAutAgo();
                $totacSep=$totacAgo+$elemento->getAutSep();
                $totacOct=$totacSep+$elemento->getAutOct();
                $totacNov=$totacOct+$elemento->getAutNov();
                $totacDic=$totacNov+$elemento->getAutDic();

                $totminEne=$totacEne;
                $totminFeb=$totacFeb;
                $totminMar=$totacMar;
                $totminAbr=$totacAbr;
                $totminMay=$totacMay;
                $totminJun=$totacJun;
                $totminJul=$totacJul;
                $totminAgo=$totacAgo;
                $totminSep=$totacSep;
                $totminOct=$totacOct;
                $totminNov=$totacNov;
                $totminDic=$totacDic;
        ?>
        <tr>
            <th align="left">Ministrado Acumulado:</th>
            <td align="right"><?php echo("$".number_format($totacEne,2));?></td>
            <td align="right"><?php echo("$".number_format($totacFeb,2));?></td>
            <td align="right"><?php echo("$".number_format($totacMar,2));?></td>
            <td align="right"><?php echo("$".number_format($totacAbr,2));?></td>
            <td align="right"><?php echo("$".number_format($totacMay,2));?></td>
            <td align="right"><?php echo("$".number_format($totacJun,2));?></td>
            <td align="right"><?php echo("$".number_format($totacJul,2));?></td>
            <td align="right"><?php echo("$".number_format($totacAgo,2));?></td>
            <td	align="right"><?php echo("$".number_format($totacSep,2));?></td>
            <td align="right"><?php echo("$".number_format($totacOct,2));?></td>
            <td align="right"><?php echo("$".number_format($totacNov,2));?></td>
            <td align="right"><?php echo("$".number_format($totacDic,2));?></td>
        </tr>		
        <?php		
					
            }
            if($elemento->getNumProyecto() == 3){
					
                $totacEne=$elemento->getAutEne();
                $totacFeb=$totacEne+$elemento->getAutFeb();
                $totacMar=$totacFeb+$elemento->getAutMar();
                $totacAbr=$totacMar+$elemento->getAutAbr();
                $totacMay=$totacAbr+$elemento->getAutMay();
                $totacJun=$totacMay+$elemento->getAutJun();
                $totacJul=$totacJun+$elemento->getAutJul();
                $totacAgo=$totacJul+$elemento->getAutAgo();
                $totacSep=$totacAgo+$elemento->getAutSep();
                $totacOct=$totacSep+$elemento->getAutOct();
                $totacNov=$totacOct+$elemento->getAutNov();
                $totacDic=$totacNov+$elemento->getAutDic();
        ?>
        <tr>
            <th align="left">Gastado Acumulado:</th>
            <td align="right"><?php echo("$".number_format($totacEne,2));?></td>
            <td align="right"><?php echo("$".number_format($totacFeb,2));?></td>
            <td align="right"><?php echo("$".number_format($totacMar,2));?></td>
            <td align="right"><?php echo("$".number_format($totacAbr,2));?></td>
            <td align="right"><?php echo("$".number_format($totacMay,2));?></td>
            <td align="right"><?php echo("$".number_format($totacJun,2));?></td>
            <td align="right"><?php echo("$".number_format($totacJul,2));?></td>
            <td align="right"><?php echo("$".number_format($totacAgo,2));?></td>
            <td align="right"><?php echo("$".number_format($totacSep,2));?></td>
            <td align="right"><?php echo("$".number_format($totacOct,2));?></td>
            <td align="right"><?php echo("$".number_format($totacNov,2));?></td>
            <td align="right"><?php echo("$".number_format($totacDic,2));?></td>
        </tr>		
        <?php		
            //obtengo por gastar acumunaldo
					
            $totpgaEne=$totautEne - $totacEne;
            $totpgaFeb=$totautFeb - $totacFeb;
            $totpgaMar=$totautMar - $totacMar;
            $totpgaAbr=$totautAbr - $totacAbr;
            $totpgaMay=$totautMay - $totacMay;
            $totpgaJun=$totautJun - $totacJun;
            $totpgaJul=$totautJul - $totacJul;
            $totpgaAgo=$totautAgo - $totacAgo;
            $totpgaSep=$totautSep - $totacSep;
            $totpgaOct=$totautOct - $totacOct;
            $totpgaNov=$totautNov - $totacNov;
            $totpgaDic=$totautDic - $totacDic;

        ?>
        <tr>
            <th align="left">Por Gastar Acumulado:</th>
            <td align="right"><?php echo("$".number_format($totpgaEne,2));?></td>
            <td align="right"><?php echo("$".number_format($totpgaFeb,2));?></td>
            <td align="right"><?php echo("$".number_format($totpgaMar,2));?></td>
            <td align="right"><?php echo("$".number_format($totpgaAbr,2));?></td>
            <td align="right"><?php echo("$".number_format($totpgaMay,2));?></td>
            <td align="right"><?php echo("$".number_format($totpgaJun,2));?></td>
            <td align="right"><?php echo("$".number_format($totpgaJul,2));?></td>
            <td align="right"><?php echo("$".number_format($totpgaAgo,2));?></td>
            <td	align="right"><?php echo("$".number_format($totpgaSep,2));?></td>
            <td align="right"><?php echo("$".number_format($totpgaOct,2));?></td>
            <td align="right"><?php echo("$".number_format($totpgaNov,2));?></td>
            <td align="right"><?php echo("$".number_format($totpgaDic,2));?></td>
        </tr>		
        <?php
        //obtengo por gastar disp - Autorizado acumulado - Ministrado acumulado
					
            $totpgdEne=$totautEne - $totminEne;
            $totpgdFeb=$totautFeb - $totminFeb;;
            $totpgdMar=$totautMar - $totminMar;;
            $totpgdAbr=$totautAbr - $totminAbr;;
            $totpgdMay=$totautMay - $totminMay;;
            $totpgdJun=$totautJun - $totminJun;;
            $totpgdJul=$totautJul - $totminJul;;
            $totpgdAgo=$totautAgo - $totminAgo;;
            $totpgdSep=$totautSep - $totminSep;;
            $totpgdOct=$totautOct - $totminOct;;
            $totpgdNov=$totautNov - $totminNov;;
            $totpgdDic=$totautDic - $totminDic;;

        ?>
        <tr>
            <th align="left">Por Gastar Disponible:</th>
            <td align="right"><?php echo("$".number_format($totpgdEne));?></td>
            <td align="right"><?php echo("$".number_format($totpgdFeb));?></td>
            <td align="right"><?php echo("$".number_format($totpgdMar));?></td>
            <td align="right"><?php echo("$".number_format($totpgdAbr));?></td>
            <td align="right"><?php echo("$".number_format($totpgdMay));?></td>
            <td align="right"><?php echo("$".number_format($totpgdJun));?></td>
            <td align="right"><?php echo("$".number_format($totpgdJul));?></td>
            <td align="right"><?php echo("$".number_format($totpgdAgo));?></td>
            <td	align="right"><?php echo("$".number_format($totpgdSep));?></td>
            <td align="right"><?php echo("$".number_format($totpgdOct));?></td>
            <td align="right"><?php echo("$".number_format($totpgdNov));?></td>
            <td align="right"><?php echo("$".number_format($totpgdDic));?></td>
        </tr>		
        <?php		
					
            }
				
            if($elemento->getNumProyecto() == 4){
					
                $totacEne=$elemento->getAutEne();
                $totacFeb=$totacEne+$elemento->getAutFeb();
                $totacMar=$totacFeb+$elemento->getAutMar();
                $totacAbr=$totacMar+$elemento->getAutAbr();
                $totacMay=$totacAbr+$elemento->getAutMay();
                $totacJun=$totacMay+$elemento->getAutJun();
                $totacJul=$totacJun+$elemento->getAutJul();
                $totacAgo=$totacJul+$elemento->getAutAgo();
                $totacSep=$totacAgo+$elemento->getAutSep();
                $totacOct=$totacSep+$elemento->getAutOct();
                $totacNov=$totacOct+$elemento->getAutNov();
                $totacDic=$totacNov+$elemento->getAutDic();
        ?>
        <tr>
            <th align="left">Comprobado INAH Acumulado:</th>
            <td align="right"><?php echo("$".number_format($totacEne,2));?></td>
            <td align="right"><?php echo("$".number_format($totacFeb,2));?></td>
            <td align="right"><?php echo("$".number_format($totacMar,2));?></td>
            <td align="right"><?php echo("$".number_format($totacAbr,2));?></td>
            <td align="right"><?php echo("$".number_format($totacMay,2));?></td>
            <td align="right"><?php echo("$".number_format($totacJun,2));?></td>
            <td align="right"><?php echo("$".number_format($totacJul,2));?></td>
            <td align="right"><?php echo("$".number_format($totacAgo,2));?></td>
            <td align="right"><?php echo("$".number_format($totacSep,2));?></td>
            <td align="right"><?php echo("$".number_format($totacOct,2));?></td>
            <td align="right"><?php echo("$".number_format($totacNov,2));?></td>
            <td align="right"><?php echo("$".number_format($totacDic,2));?></td>
        </tr>		
        <?php		
					
            }	
		
            }
        ?>
			
        </table>
    </div>

    </body>
</html>