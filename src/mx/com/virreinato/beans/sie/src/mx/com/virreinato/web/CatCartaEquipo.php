<?php

session_start();
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/CartaEquipoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CartaEquipo.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$respuesta = "";
if (isset($_POST['idCarta'])){
    $elemento = new CartaEquipo();
    $elemento->setIdCarta( (int)($_POST['idCarta'] ));
    $elemento->setId ((int)( $_POST["id_equipo_carta"]));

    $dao=new CartaEquipoDaoJdbc();
    $res=$dao->guardaElemento($elemento);

    if ($res) {
        $respuesta = "Su información se almacenó exitosamente.";
    } else {
        $respuesta = "No fue posible almacenar su información.";
    }
    header("Location: ../../../../../Proyectos/lista_Equipo.php?respuesta=" . $respuesta."&idCarta=".$_POST['idCarta']);
}else if (isset($_GET['id'])){
    $dao=new CartaEquipoDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
    header("Location: ../../../../../Proyectos/lista_Equipo.php?respuesta=" . $respuesta."&idCarta=".$_GET['idCarta']);
} else {
    $respuesta = "No se detecto la acción a realizar.";
    header("Location: ../../../../../Proyectos/lista_Equipo.php?respuesta=" . $respuesta);
}
?>


