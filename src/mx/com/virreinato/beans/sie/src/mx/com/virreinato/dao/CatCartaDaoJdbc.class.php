<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatCarta.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class CatCartaDaoJdbc {
    
    public function obtieneListado($anio) {
		
	$lista= array();
		
	$query="SELECT c.cco_id_carta, p.cpr_descripcion , e.cem_nombre, e.cem_appaterno, e.cem_apmaterno, p.cpr_fecha_ini_total, p.cpr_fecha_fin_total, c.cco_versiondoc,c.cco_costototal, m.proyecto_desc_meta  FROM sie_carta_constitutiva c, sie_cat_proyectos p , cat_empleado e, sie_proyecto_metas m WHERE p.cpr_estatus='1' AND c.cpr_id_proyecto = p.cpr_id_proyecto AND c.proyecto_id_meta=m.proyecto_id_meta AND m.cpr_id_proyecto=p.cpr_id_proyecto AND p.cpr_fecha_ini_total BETWEEN '".$anio."-01-01-' AND '".$anio."-12-31' AND p.cpr_id_lider = e. cem_id_empleado AND c.cco_estatus = 'Y'";		

	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs["cco_id_carta"];
            $descripcion = $rs["cpr_descripcion"];
            $lider = $rs["cem_nombre"]. " " .$rs["cem_appaterno"]. " " .$rs["cem_apmaterno"];
            $fechaInicio  = $rs["cpr_fecha_ini_total"];
            $fechaFin =  $rs["cpr_fecha_fin_total"];
            $version = $rs["cco_versiondoc"];
            $costo=$rs["cco_costototal"];
            $meta =  $rs["proyecto_desc_meta"];

            $elemento = new CatCarta();
            $elemento->catCart($id,$meta,$version,$costo,$descripcion, $lider,$fechaInicio,$fechaFin);
            array_push($lista, $elemento);
        }	
        return $lista;
    }
    
    public function obtieneElemento($idElemento) {
		
	$elemento = new CatCarta();
	$query="SELECT c.cco_id_carta, c.cpr_id_proyecto , c.cco_dependencia , c.cco_versiondoc ,c.cco_costototal, c.cco_objetivosespecificos , e.cej_eje, c.cco_ef_descripcion, c.cco_ef_caracteristicas, c.cco_ef_entregadopor, c.cco_ef_validadopor,m.proyecto_desc_meta , c.proyecto_id_meta FROM sie_carta_constitutiva c, sie_cat_linea_accion a, sie_cat_ejes e, sie_cat_proyectos p, sie_proyecto_metas m  WHERE  c.cco_estatus = 'Y' AND c.cco_id_carta = ".$idElemento." AND c.cpr_id_proyecto = p.cpr_id_proyecto AND p.cla_id_lineaaccion = a.cla_id_lineaaccion AND a.cej_id_eje = e.cej_id_eje AND c.proyecto_id_meta=m.proyecto_id_meta AND m.cpr_id_proyecto=p.cpr_id_proyecto  ";
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){				
            $id= $rs["cco_id_carta"];
            $idProyecto= $rs["cpr_id_proyecto"];
            $idMeta =  $rs["proyecto_id_meta"];
            $meta =  $rs["proyecto_desc_meta"];
            $dependencia = $rs["cco_dependencia"];
            $version = $rs["cco_versiondoc"];
            $costo=$rs["cco_costototal"];
            $objetivos = $rs["cco_objetivosespecificos"];
            $eje = $rs["cej_eje"];
            $desc = $rs["cco_ef_descripcion"];
            $carac = $rs["cco_ef_caracteristicas"];
            $entregado = $rs["cco_ef_entregadopor"];
            $validado = $rs["cco_ef_validadopor"];

            $elemento->setAll($id,$idProyecto,$idMeta, $meta,$dependencia,$version,$costo,$objetivos,$eje, $desc, $carac,$entregado, $validado);
							
        }		
        return $elemento;		
    }
    
    public function obtieneIdentificador(){

        $idCarta = 0;
        $query="SELECT MAX(cco_id_carta) as cco_id_carta FROM sie_carta_constitutiva";
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);
        if($rs = mysql_fetch_array($result)){
            $idCarta = $rs["cco_id_carta"];
        }
 		
        return $idCarta;
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $query="INSERT INTO sie_carta_constitutiva (CPR_ID_PROYECTO, CCO_NOMBREPROYECTO, CCO_ORIGENPRESUPUESTO, ".
		"CCO_DEPENDENCIA, CCO_FECHAINICIO, CCO_FECHATERMINO, CCO_LIDERPROY, CCO_MIEMBROSEQUIPO, CCO_VERSIONDOC, CCO_COSTOTOTAL, ".
		"CCO_LINEAACCION, CCO_EJE, CCO_ANTECEDENTES, CCO_OBJETIVOGENERAL, CCO_LIMITES, CCO_OBJETIVOSESPECIFICOS,CCO_IMPACTOSOCIAL, ".
		"CCO_IMPACTOORGANIZACIONAL, CCO_IMPACTOESTRATEGICO, CCO_BENEFICIARIOS, CCO_EF_DESCRIPCION, CCO_EF_CARACTERISTICAS, CCO_EF_ENTREGADOPOR, ".
		"CCO_EF_VALIDADOPOR, CCO_ESTATUS, PROYECTO_ID_META) VALUES (".$elemento->getIdProyecto().", null, null,'".
		$elemento->getDependencia()."',null,null,null,null,'". $elemento->getVersion() ."',".$elemento->getCosto().",null, null ,null,null,null, '". $elemento->getObjetivoEsp() ."'," .
		"null, null, null ,null ,'".$elemento->getDescEntregable()."','".$elemento->getCaracteristicas_Entregable()."','".$elemento->getEntregado()."','".$elemento->getValidado()."','Y', '". $elemento->getIdMeta()."')";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
	$query="UPDATE sie_carta_constitutiva SET  cpr_id_proyecto=".$elemento->getIdProyecto().", proyecto_id_meta='".$elemento->getIdMeta()."', cco_versiondoc = '".$elemento->getVersion()."', cco_costototal=".$elemento->getCosto().", cco_dependencia = '".$elemento->getDependencia()."', cco_objetivosespecificos ='".$elemento->getObjetivoEsp()."', cco_ef_descripcion='".$elemento->getDescEntregable()."', cco_ef_caracteristicas='".$elemento->getCaracteristicas_Entregable()."', cco_ef_entregadopor = '".$elemento->getEntregado()."', cco_ef_validadopor = '".$elemento->getValidado()."' WHERE cco_id_carta = ". $elemento->getId() ."";
	$res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_carta_constitutiva set  cco_estatus = 'N' WHERE cco_id_carta = ".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}
