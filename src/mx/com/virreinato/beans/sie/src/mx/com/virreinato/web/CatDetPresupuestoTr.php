<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/DetPresupuestoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/DetPresupuesto.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
if(isset($_POST['validarPartida']) && $_POST['validarPartida']!=null){
    $total = 0;
    $partida = 0;

    $query = " SELECT SUM( cal.trdc_ene + cal.trdc_feb + cal.trdc_mar + cal.trdc_abr + cal.trdc_may + cal.trdc_jun + cal.trdc_jul + cal.trdc_ago + cal.trdc_sep + cal.trdc_oct + cal.trdc_nov + cal.trdc_dic ) as partida FROM sie_tarjeta_reg_det_cal cal, sie_cat_partidas p, sie_cat_capitulos c, sie_tarjeta_registro t WHERE t.proyecto_id_meta = ".$_POST['idMeta']." AND t.tre_estatus = 1 AND t.tre_id_tarjeta = cal.tre_id_tarjeta AND cal.trdc_estatus = 1 AND cal.cpa_id_partida = p.cpa_id_partida AND p.cca_id_capitulo = c.cca_id_capitulo AND c.cca_id_capitulo = (SELECT c.cca_id_capitulo FROM sie_cat_capitulos c, sie_cat_partidas p WHERE p.cpa_id_partida = ".$_POST['validarPartida']." AND p.cca_id_capitulo = c.cca_id_capitulo ) ";

    $catalogo = new Catalogo();
        
    $result = $catalogo->obtenerLista($query);

    while ($rs = mysql_fetch_array($result)){
        $total = $rs["partida"];
    }

    $query = "SELECT SUM(pre.spr_monto) as monto FROM sie_det_sol_presup pre, sie_cat_partidas p, sie_cat_capitulos c  WHERE pre.spr_id_solicitud = ".$_POST["idSolicitud"]." AND pre.spr_estatus = 1 AND pre.cpa_id_partida = p.cpa_id_partida AND p.cca_id_capitulo = c.cca_id_capitulo AND c.cca_id_capitulo = (SELECT c.cca_id_capitulo FROM sie_cat_capitulos c, sie_cat_partidas p WHERE p.cpa_id_partida = ".$_POST["validarPartida"]." AND p.cca_id_capitulo = c.cca_id_capitulo )";
    $result = $catalogo->obtenerLista($query);

    while ($rs = mysql_fetch_array($result)){
        $partida = $rs["monto"];
    }

    echo($total."/".$partida);
}else if(isset($_POST['guardar'])){
    $elemento = new DetPresupuesto();
			
    $elemento->setIdPresupuesto((int)($_POST["idSolicitud"]));
    $elemento->setIdActiviad((int)($_POST["actividades_sol"]));
    $elemento->setIdPartida((int)($_POST["partida_sol"]));
    $elemento->setMonto((float)( $_POST["monto_det"]));
    $elemento->setProductos_obtenidos((int)($_POST["productos"]));
    $elemento->setDetalle_productos($_POST["materiales_sol"]);
    
    if ($_POST["id"] != null){
        $elemento->setId((int)($_POST["id"]));
        $dao=new DetPresupuestoDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new DetPresupuestoDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
    header("Location: ../../../../../Terceros/SolicitudEditTr.php?respuesta=" . $respuesta."&id=".$elemento->getIdPresupuesto());
}else if (isset($_GET['id'])){
    $dao=new DetPresupuestoDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
    header("Location: ../../../../../Terceros/SolicitudEditTr.php?respuesta=".$respuesta."&id=".$_GET['idSol']);
} else {
    $respuesta = "No se detecto la acción a realizar.";
    header("Location: ../../../../../Terceros/SolicitudEditTr.php?respuesta=" . $respuesta);
}
?>

