<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CartaEquipo.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class CartaEquipoDaoJdbc {
    
    public function obtieneListado($id_carta) {
		
        $lista= array();
        $query="SELECT cco_id_carta, cce_id_equipo, cem_nombre, cem_appaterno, cem_apmaterno FROM sie_carta_cons_det_equipo, cat_empleado WHERE cce_estatus = 1 AND  cco_id_carta = ".(int)($id_carta)."  AND cce_id_equipo = cem_id_empleado";

        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $idCarta= $rs["cco_id_carta"];
            $idEquipo = $rs["cce_id_equipo"];
            $nombre = $rs["cem_nombre"] . " " . $rs["cem_appaterno"] . " " . $rs["cem_apmaterno"];

            $elemento = new CartaEquipo();
            $elemento->setAll($idEquipo,$idCarta,$nombre);
            array_push($lista, $elemento);
        }	
        return $lista;
    }
    
    public function obtieneElemento($idElemento) {
		
		
        $elemento = new CartaEquipo();
        $query="SELECT * FROM sie_carta_cons_det_equipo WHERE  cce_estatus = 1 AND cce_id_equipo = ".$idElemento;

        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){

            $idCarta= $rs[strtoupper("cco_id_carta")];
            $idEquipo = $rs[strtoupper("ccde_id_equipo")];

            $elemento->cartaEqui($idEquipo,$idCarta);

        }

        return $elemento;
		
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $query="INSERT INTO sie_carta_cons_det_equipo VALUES (".$elemento->getId().",".$elemento->getIdCarta().", 1)";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_carta_cons_det_equipo set  cce_estatus = 0 WHERE cce_id_equipo = ".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}
