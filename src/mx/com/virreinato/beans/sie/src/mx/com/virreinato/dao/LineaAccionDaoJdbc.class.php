<?php
if(session_id() == '') {
    session_start();
}

include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Eje.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/LineaAccion.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class LineaAccionDaoJdbc {
    
    public function obtieneListado() {
		
	$lista= array();
		
	$query="SELECT * FROM sie_cat_ejes E, sie_cat_linea_accion LA WHERE E.cej_id_eje=LA.cej_id_eje AND cla_estatus=1 ORDER BY cej_eje ";
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cla_id_lineaaccion")];
            $id_eje= $rs[strtoupper("cej_id_eje")];
            $cej_eje= $rs[strtoupper("cej_eje")];
            $cej_descripcion= $rs[strtoupper("cej_descripcion")];
            $lineaaccion= $rs[strtoupper("cla_lineaaccion")];
            $descripcion= $rs[strtoupper("cla_descripcion")];

            $eje = new Eje();
            $eje->setAll($id_eje,$cej_eje,$cej_descripcion);

            $elemento = new LineaAccion();
            $elemento->setAll($id,$eje,$lineaaccion,$descripcion);
            array_push($lista, $elemento);
        }	
	return $lista;
    }
    
    public function obtieneElemento($idElemento) {
		
		
	$elemento=new LineaAccion();
		
	$query="SELECT * FROM sie_cat_ejes E, sie_cat_linea_accion LA WHERE E.cej_id_eje=LA.cej_id_eje AND LA.cla_id_lineaaccion=".$idElemento;
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cla_id_lineaaccion")];
            $id_eje= $rs[strtoupper("cej_id_eje")];
            $cej_eje= $rs[strtoupper("cej_eje")];
            $cej_descripcion= $rs[strtoupper("cej_descripcion")];
            $lineaaccion= $rs[strtoupper("cla_lineaaccion")];
            $descripcion= $rs[strtoupper("cla_descripcion")];

            $eje = new Eje();
            $eje->setAll($id_eje,$cej_eje,$cej_descripcion);

            $elemento->setAll($id,$eje,$lineaaccion,$descripcion);

        }	
	return $elemento;
		
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $aux = $elemento->getEje();
        $query="INSERT INTO sie_cat_linea_accion(cej_id_eje, cla_lineaaccion, cla_descripcion) VALUES ('".$aux->getId()."', '".$elemento->getLineaAccion()."', '".$elemento->getDescripcion()."')";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
        $aux = $elemento->getEje();
	$query="UPDATE sie_cat_linea_accion set  cej_id_eje='".$aux->getId()."', cla_descripcion='".$elemento->getDescripcion()."', cla_lineaaccion='".$elemento->getLineaAccion()."' WHERE cla_id_lineaaccion=".$elemento->getId();
	$res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_cat_linea_accion set  cla_estatus=0 WHERE cla_id_lineaaccion=".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}
