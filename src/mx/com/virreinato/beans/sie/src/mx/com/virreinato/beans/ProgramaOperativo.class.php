<?php

class ProgramaOperativo {
    
    private $idProgramaOperativo;
    private $programaOperativo;
    private $estatus;
    
    function setAll($idProgramaOperativo, $programaOperativo, $estatus) {
        $this->idProgramaOperativo = $idProgramaOperativo;
        $this->programaOperativo = $programaOperativo;
        $this->estatus = $estatus;
    }

    function __construct() {
        
    }
    
    public function getIdProgramaOperativo() {
        return $this->idProgramaOperativo;
    }

    public function getProgramaOperativo() {
        return $this->programaOperativo;
    }

    public function getEstatus() {
        return $this->estatus;
    }

    public function setIdProgramaOperativo($idProgramaOperativo) {
        $this->idProgramaOperativo = $idProgramaOperativo;
    }

    public function setProgramaOperativo($programaOperativo) {
        $this->programaOperativo = $programaOperativo;
    }

    public function setEstatus($estatus) {
        $this->estatus = $estatus;
    }



}
