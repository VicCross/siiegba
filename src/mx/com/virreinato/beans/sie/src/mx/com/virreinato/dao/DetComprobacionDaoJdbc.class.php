<?php
if(session_id() == '') {
    session_start();
}

include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Comprobacion.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class DetComprobacionDaoJdbc {
    
    public function obtieneListado($IdComprobacion) {
        
	$lista = array();

	$query =  "select " . 
            "DCO_ID_COMPROBANTE as idComprobante, " .
            "COM_ID_COMPROBACION as idComprobacion, " .
            "DCO_TIPOCOMPROBANTE as tipo, " .
            "DCO_FOLIOCOMPROBANTE as folio, " .
            "DCO_FECHACOMPROBANTE as fecha, " .
            "CPA_ID_PARTIDA as idPartida, " .
            "DCO_MONTO as monto, " .
            "DCO_DESCRIPCION as descripcion, " .
            "DCO_URLUBICACION as url, " .
            "CPR_ID_PROVEEDOR as idProveedor " .
            "from sie_det_comprobaciones " . 
            "where COM_ID_COMPROBACION = " . $IdComprobacion;

        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)) {

            $idComprobante = $rs["idComprobante"]; 
            $idComprobacion = $rs["idComprobacion"]; 
            $tipo = $rs["tipo"];
            $folio = $rs["folio"];
            $fecha = $rs["fecha"]; 
            $idPartida = $rs["idPartida"]; 
            $monto = $rs["monto"];
            $descripcion = $rs["descripcion"];
            $url = $rs["url"];
            $idProveedor=$rs["idProveedor"];

            $elemento = new DetComprobacion();
            $elemento->setAll($idComprobante, $idComprobacion, $tipo, $folio, $fecha, $idPartida, $monto, $descripcion, $url,$idProveedor);

            array_push($lista, $elemento);
				
        }

        return $lista;
    }
    
    public function obtieneElemento($idElemento) {
		
	$elemento = new DetComprobacion();
		
	$query =  "select " . 
            "DCO_ID_COMPROBANTE as idComprobante, " .
            "COM_ID_COMPROBACION as idComprobacion, " .
            "DCO_TIPOCOMPROBANTE as tipo, " .
            "DCO_FOLIOCOMPROBANTE as folio, " .
            "DCO_FECHACOMPROBANTE as fecha, " .
            "CPA_ID_PARTIDA as idPartida, " .
            "DCO_MONTO as monto, " .
            "DCO_DESCRIPCION as descripcion, " .
            "DCO_URLUBICACION as url, " .
            "CPR_ID_PROVEEDOR as idProveedor " .
            "from sie_det_comprobaciones " . 
            "where DCO_ID_COMPROBANTE = " . $idElemento;
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)) {

            $idComprobante = $rs["idComprobante"]; 
            $idComprobacion = $rs["idComprobacion"]; 
            $tipo = $rs["tipo"];
            $folio = $rs["folio"];
            $fecha = $rs["fecha"]; 
            $idPartida = $rs["idPartida"]; 
            $monto = $rs["monto"];
            $descripcion = $rs["descripcion"];
            $url = $rs["url"];
            $idProveedor=$rs["idProveedor"];

            $elemento = new DetComprobacion();
            $elemento->setAll($idComprobante, $idComprobacion, $tipo, $folio, $fecha, $idPartida, $monto, $descripcion, $url,$idProveedor);
				
        }

        return $elemento;
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        
	$fecha = "";
		
        if($elemento->getFecha()!=null)
        {        $fecha= "'".date("Y-m-d",strtotime($elemento->getFecha()))."'"; }
        else
        {        $fecha = "null"; }
        
        $query="insert into sie_det_comprobaciones(COM_ID_COMPROBACION,DCO_TIPOCOMPROBANTE,DCO_FOLIOCOMPROBANTE,DCO_FECHACOMPROBANTE,CPA_ID_PARTIDA,DCO_MONTO,DCO_DESCRIPCION,DCO_URLUBICACION,CPR_ID_PROVEEDOR) values (" .
            $elemento->getIdComprobacion() . ",'" .
            $elemento->getTipo() . "','" .
            $elemento->getFolio() . "'," .
            $fecha . "," .
            $elemento->getIdPartida() . "," .
            $elemento->getMonto() . ",'" .
            $elemento->getDescripcion() . "', '" .
            $elemento->getUrl().  "'," .
            $elemento->getIdProveedor(). ")";
        
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
        
        $fecha = "";
		
        if($elemento->getFecha()!=null)
        {        $fecha= "'".date("Y-m-d",strtotime($elemento->getFecha()))."'"; }
        else
        {        $fecha = "null"; }
		
	$con=new Catalogo();
	$query="update sie_det_comprobaciones set ".
            "DCO_TIPOCOMPROBANTE = '" . $elemento->getTipo() . "', ".
            "DCO_FOLIOCOMPROBANTE = '" . $elemento->getFolio() . "', ".
            "DCO_FECHACOMPROBANTE = " . $fecha . ", ".
            "CPA_ID_PARTIDA = " . $elemento->getIdPartida() . ", ".
            "DCO_MONTO = " . $elemento->getMonto() . ", ".
            "DCO_DESCRIPCION = '" . $elemento->getDescripcion() . "', ".
            "DCO_URLUBICACION = '" . $elemento->getUrl() . "',".
            "CPR_ID_PROVEEDOR = '" . $elemento->getIdProveedor() . "' ".
            "where DCO_ID_COMPROBANTE = " . $elemento->getIdComprobante();
	$res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="delete from sie_det_comprobaciones where DCO_ID_COMPROBANTE=".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function obtieneId() {
        
        $idComprobacion = 0;
        $query="SELECT MAX(DCO_ID_COMPROBANTE) as idComprobante FROM sie_det_comprobaciones";
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)) {
                $idComprobacion = $rs["idComprobante"];
        }

        return $idComprobacion;
    }
}
