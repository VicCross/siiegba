<?php
if(session_id() == '') {
    session_start();
}

include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/AreaNormativa.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class AreaNormativaDaoJdbc {
    
    public function obtieneListado() {
			
        $lista= array();
		
	$query="SELECT * FROM sie_cat_areanormativa WHERE can_estatus=1 ORDER BY can_descripcion ";
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("can_id_areanormativa")];
            $descripcion= $rs[strtoupper("can_descripcion")];
            
            $elemento = new AreaNormativa();
            $elemento->setAll($id,$descripcion);
            array_push($lista, $elemento);
        }	
	return $lista;
    }
    
    public function obtieneElemento($idElemento) {
		
	$elemento=new AreaNormativa();
		
	$query="SELECT * FROM sie_cat_areanormativa WHERE can_id_areanormativa=".$idElemento;
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("can_id_areanormativa")];
            $descripcion= $rs[strtoupper("can_descripcion")];
            
            $elemento = new AreaNormativa();
            $elemento->setAll($id,$descripcion);
	}
		
	return $elemento;
		
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $query="INSERT INTO sie_cat_areanormativa(can_descripcion) VALUES ('".$elemento->getDescripcion()."')";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
	$query="UPDATE sie_cat_areanormativa set  can_descripcion='".$elemento->getDescripcion()."' WHERE can_id_areanormativa=".$elemento->getId();
	$res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_cat_areanormativa set  can_estatus=0 WHERE can_id_areanormativa=".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}
