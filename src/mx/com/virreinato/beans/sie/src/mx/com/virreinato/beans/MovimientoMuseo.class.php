<?php

class MovimientoMuseo {
    private $fecha;
    private $cargo;
    private $abono;
    private $descripcion;
    
    function setAll($fecha, $cargo, $abono, $descripcion) {
        $this->fecha = $fecha;
        $this->cargo = $cargo;
        $this->abono = $abono;
        $this->descripcion = $descripcion;
    }

    function __construct() {
        
    }

    public function getFecha() {
        return $this->fecha;
    }

    public function getCargo() {
        return $this->cargo;
    }

    public function getAbono() {
        return $this->abono;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function setFecha($fecha) {
        $this->fecha = $fecha;
    }

    public function setCargo($cargo) {
        $this->cargo = $cargo;
    }

    public function setAbono($abono) {
        $this->abono = $abono;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }


}
