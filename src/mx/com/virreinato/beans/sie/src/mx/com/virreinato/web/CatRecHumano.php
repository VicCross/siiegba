<?php

session_start();
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/RecHumanoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/RecHumanos.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$respuesta = "";
if (isset($_POST['id_Carta'])){
    $elemento = new RecHumanos();

    $elemento->setIdCarta( (int)( $_POST["id_Carta"] ));
    if(  $_POST["orden_humano"] != "" ) $elemento->setNum_orden( (int)( $_POST["orden_humano"] ) );
    $elemento->setRec_humano( $_POST["descripcion_humano"]);
    if( $_POST["nomina"]!= "" )$elemento->setNomina((float)( $_POST["nomina"] ) );
    if( $_POST["honorarios"]!= "" ) $elemento->setHonorarios( (float)( $_POST["honorarios"] ));
    $elemento->setFactura( $_POST["factura_humano"] );
    $elemento->setPartida( $_POST["partida_humano"] );
    if( $_POST["fecha_Ejercicio"] != "" ) $elemento->setFechaEjercicio( date("Y-m-d",strtotime( $_POST["fecha_Ejercicio"])));
    $elemento->setFiscal( $_POST["fiscal_humano"] );
    $elemento->setCheque( $_POST["cheque_humano"] );
    $elemento->setRetencion( $_POST["retencion"] );
    if (isset($_POST['id'])){
        $elemento->setId((int)($_POST["id"]));
        $dao=new RecHumanoDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new RecHumanoDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
    header("Location: ../../../../../Proyectos/lista_RecHumanos.php?respuesta=" . $respuesta."&idCarta=".$_POST['id_Carta']);
}else if (isset($_GET['id'])){
    $dao=new RecHumanoDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
    header("Location: ../../../../../Proyectos/lista_RecHumanos.php?respuesta=" . $respuesta."&idCarta=".$_GET['idCarta']);
} else {
    $respuesta = "No se detecto la acción a realizar.";
    header("Location: ../../../../../Proyectos/lista_RecHumanos.php?respuesta=" . $respuesta);
}
?>