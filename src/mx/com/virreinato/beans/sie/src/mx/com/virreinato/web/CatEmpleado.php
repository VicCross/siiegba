<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/EmpleadoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Empleado.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$respuesta = "";
if (isset($_POST['guardar'])){
    $elemento=new Empleado();
    $elemento->setNombre($_POST["nombre"]);
    $elemento->setApPaterno($_POST["apaterno"]);
    $elemento->setApMaterno($_POST["amaterno"]);

    $monto2=$_POST["sueldo_neto"];
    $monto_letra=$_POST["sueldo_letra"];
    $monto=  str_replace(",","", $monto2);
    $elemento->setSueldoNeto((double)($monto));
    $elemento->setSueldoLetra($monto_letra);
    $elemento->setPuesto($_POST["puesto"]);
    $elemento->setRfc($_POST["rfc"]);
    $elemento->setTipoNomina($_POST["tipo_nomina"]);
    if($_POST["numero"] == 0){
        $elemento->setNumero(null);
    }else{
        $elemento->setNumero($_POST["numero"]);
    }
    if ($_POST["id"] != null){
        $elemento->setId((int)($_POST["id"]));
        $dao=new EmpleadoDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new EmpleadoDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
}else if (isset($_GET['id'])){
    $dao=new EmpleadoDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
} else {
    $respuesta = "No se detecto la acción a realizar.";
}
header("Location: ../../../../../catalogos/lista_empleado.php?respuesta=" . $respuesta);
?>

