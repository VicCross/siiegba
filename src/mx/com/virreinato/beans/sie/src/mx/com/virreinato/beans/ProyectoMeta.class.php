<?php

class ProyectoMeta {
    
    private $idMeta;
    private $idProyecto;
    private $descripcion;
    private $nombreProyecto;
    private $idProgramaInstitucional;
    private $programaInstitucional;
    private $idProgramaOperativo;
    private $programaOperativo;
    
    function constructor1($idMeta, $idProyecto, $descripcion, $idProgramaInstitucional, $idProgramaOperativo) {
        $this->idMeta = $idMeta;
        $this->idProyecto = $idProyecto;
        $this->descripcion = $descripcion;
        $this->idProgramaInstitucional = $idProgramaInstitucional;
        $this->idProgramaOperativo = $idProgramaOperativo;
    }

    function constructor2($idMeta, $idProyecto, $descripcion, $idProgramaInstitucional,$idProgramaOperativo, $programaInstitucional, $programaOperativo) {
        $this->idMeta = $idMeta;
        $this->idProyecto = $idProyecto;
        $this->descripcion = $descripcion;
        $this->idProgramaInstitucional = $idProgramaInstitucional;
        $this->idProgramaOperativo = $idProgramaOperativo;
        $this->programaInstitucional = $programaInstitucional;
        $this->programaOperativo = $programaOperativo;
    }

    function constructor3($idMeta, $idProyecto, $descripcion, $nombreProyecto, $idProgramaInstitucional, $idProgramaOperativo) {
        $this->idMeta = $idMeta;
        $this->idProyecto = $idProyecto;
        $this->descripcion = $descripcion;
        $this->nombreProyecto = $nombreProyecto;
        $this->idProgramaInstitucional = $idProgramaInstitucional;
        $this->idProgramaOperativo = $idProgramaOperativo;
    }

    
    function setAll($idMeta, $idProyecto, $descripcion, $nombreProyecto, $idProgramaInstitucional, $programaInstitucional, $idProgramaOperativo, $programaOperativo) {
        $this->idMeta = $idMeta;
        $this->idProyecto = $idProyecto;
        $this->descripcion = $descripcion;
        $this->nombreProyecto = $nombreProyecto;
        $this->idProgramaInstitucional = $idProgramaInstitucional;
        $this->programaInstitucional = $programaInstitucional;
        $this->idProgramaOperativo = $idProgramaOperativo;
        $this->programaOperativo = $programaOperativo;
    }

    public function getIdMeta() {
        return $this->idMeta;
    }

    public function getIdProyecto() {
        return $this->idProyecto;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function getNombreProyecto() {
        return $this->nombreProyecto;
    }

    public function getIdProgramaInstitucional() {
        return $this->idProgramaInstitucional;
    }

    public function getProgramaInstitucional() {
        return $this->programaInstitucional;
    }

    public function getIdProgramaOperativo() {
        return $this->idProgramaOperativo;
    }

    public function getProgramaOperativo() {
        return $this->programaOperativo;
    }

    public function setIdMeta($idMeta) {
        $this->idMeta = $idMeta;
    }

    public function setIdProyecto($idProyecto) {
        $this->idProyecto = $idProyecto;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function setNombreProyecto($nombreProyecto) {
        $this->nombreProyecto = $nombreProyecto;
    }

    public function setIdProgramaInstitucional($idProgramaInstitucional) {
        $this->idProgramaInstitucional = $idProgramaInstitucional;
    }

    public function setProgramaInstitucional($programaInstitucional) {
        $this->programaInstitucional = $programaInstitucional;
    }

    public function setIdProgramaOperativo($idProgramaOperativo) {
        $this->idProgramaOperativo = $idProgramaOperativo;
    }

    public function setProgramaOperativo($programaOperativo) {
        $this->programaOperativo = $programaOperativo;
    }

}
