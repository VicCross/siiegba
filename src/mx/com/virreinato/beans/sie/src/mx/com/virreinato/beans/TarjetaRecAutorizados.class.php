<?php

class TarjetaRecAutorizados {
    private $idRecurso;
    private $idTarjeta;
    private $anio;
    private $ingresoAutogenerado;
    private $aportacionesTerceros;
    private $SumaRecursos;
    
    function setAll($idRecurso, $idTarjeta, $anio, $ingresoAutogenerado, $aportacionesTerceros, $SumaRecursos) {
        $this->idRecurso = $idRecurso;
        $this->idTarjeta = $idTarjeta;
        $this->anio = $anio;
        $this->ingresoAutogenerado = $ingresoAutogenerado;
        $this->aportacionesTerceros = $aportacionesTerceros;
        $this->SumaRecursos = $SumaRecursos;
    }

    function constructor2($idRecurso, $idTarjeta, $anio, $ingresoAutogenerado, $aportacionesTerceros) {
        $this->idRecurso = $idRecurso;
        $this->idTarjeta = $idTarjeta;
        $this->anio = $anio;
        $this->ingresoAutogenerado = $ingresoAutogenerado;
        $this->aportacionesTerceros = $aportacionesTerceros;
    }
    
    function __construct() {
        
    }

    public function getIdRecurso() {
        return $this->idRecurso;
    }

    public function getIdTarjeta() {
        return $this->idTarjeta;
    }

    public function getAnio() {
        return $this->anio;
    }

    public function getIngresoAutogenerado() {
        return $this->ingresoAutogenerado;
    }

    public function getAportacionesTerceros() {
        return $this->aportacionesTerceros;
    }

    public function getSumaRecursos() {
        return $this->SumaRecursos;
    }

    public function setIdRecurso($idRecurso) {
        $this->idRecurso = $idRecurso;
    }

    public function setIdTarjeta($idTarjeta) {
        $this->idTarjeta = $idTarjeta;
    }

    public function setAnio($anio) {
        $this->anio = $anio;
    }

    public function setIngresoAutogenerado($ingresoAutogenerado) {
        $this->ingresoAutogenerado = $ingresoAutogenerado;
    }

    public function setAportacionesTerceros($aportacionesTerceros) {
        $this->aportacionesTerceros = $aportacionesTerceros;
    }

    public function setSumaRecursos($SumaRecursos) {
        $this->SumaRecursos = $SumaRecursos;
    }


}
