<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Eje.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class EjeDaoJdbc {
    
    public function obtieneListado() {
		
	$lista= array();
		
	$query="SELECT * FROM sie_cat_ejes WHERE cej_estatus=1 ORDER BY cej_eje";
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cej_id_eje")];
            $eje= $rs[strtoupper("cej_eje")];
            $descripcion= $rs[strtoupper("cej_descripcion")];

            $elemento= new Eje();
            $elemento->setAll($id, $eje, $descripcion);
            array_push($lista, $elemento);
	}	
	return $lista;
    }
    
    public function obtieneElemento($idElemento){
        
        $elemento = new Eje();
        
        $query="SELECT * FROM sie_cat_ejes WHERE cej_id_eje=".$idElemento;
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cej_id_eje")];
            $eje= $rs[strtoupper("cej_eje")];
            $descripcion= $rs[strtoupper("cej_descripcion")];

            $elemento= new Eje();
            $elemento->setAll($id, $eje, $descripcion);

	}	
	return $elemento;
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $query="INSERT INTO sie_cat_ejes(cej_eje,cej_descripcion) VALUES ('".$elemento->getEje()."', '".$elemento->getDescripcion()."')";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
	$query="UPDATE sie_cat_ejes set  cej_descripcion='".$elemento->getDescripcion()."', cej_eje='".$elemento->getEje()."' WHERE cej_id_eje=".$elemento->getId();
	$res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_cat_ejes set  cej_estatus=0 WHERE cej_id_eje=".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}
