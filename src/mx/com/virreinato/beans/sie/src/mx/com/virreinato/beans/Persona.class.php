<?php

class Persona {
    private $id;
    private $nombre;
    private $apPaterno;
    private $apMaterno;
    private $correo;
    private $comentario;
    
    function setAll($id, $nombre, $apPaterno, $apMaterno, $correo, $comentario) {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->apPaterno = $apPaterno;
        $this->apMaterno = $apMaterno;
        $this->correo = $correo;
        $this->comentario = $comentario;
    }

    function __construct() {
        
    }
    
    public function getId() {
        return $this->id;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function getApPaterno() {
        return $this->apPaterno;
    }

    public function getApMaterno() {
        return $this->apMaterno;
    }

    public function getCorreo() {
        return $this->correo;
    }

    public function getComentario() {
        return $this->comentario;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function setApPaterno($apPaterno) {
        $this->apPaterno = $apPaterno;
    }

    public function setApMaterno($apMaterno) {
        $this->apMaterno = $apMaterno;
    }

    public function setCorreo($correo) {
        $this->correo = $correo;
    }

    public function setComentario($comentario) {
        $this->comentario = $comentario;
    }

}
