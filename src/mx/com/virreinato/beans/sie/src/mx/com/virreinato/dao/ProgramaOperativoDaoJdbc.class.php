<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProyectoMeta.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProgramaInstitucional.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProgramaOperativo.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class ProgramaOperativoDaoJdbc {
    
    public function obtieneListado(){
        
	$lista =  array();
	$query = "SELECT * FROM sie_cat_programa_operativo  " .
                " WHERE cpo_estatus =  1  ORDER BY cpo_programa ";
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
				 
            $idProyectoOp=$rs[strtoupper("cpo_id_programa")];
            $programaOperativo = $rs[strtoupper("cpo_programa")];
            $estatus= $rs[strtoupper("cpo_estatus")];

            $elemento = new ProgramaOperativo();
            $elemento->setAll($idProyectoOp,$programaOperativo,$estatus);
            array_push($lista, $elemento);
				 
	 }
        return $lista;
    }
    
    public function obtieneElemento($idPrograma){
        
        $elemento  =  null;
        $query = "SELECT * FROM sie_cat_programa_operativo  " .
				" WHERE cpo_id_programa = ".$idPrograma;
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
				 
            $idProyectoOp=$rs[strtoupper("cpo_id_programa")];
            $programaOperativo = $rs[strtoupper("cpo_programa")];
            $estatus= $rs[strtoupper("cpo_estatus")];

            $elemento = new ProgramaOperativo();
            $elemento->setAll($idProyectoOp,$programaOperativo,$estatus);
				 
	 }
        return $elemento;	
    }
}
