<?php

class Eje {
    private $id;
    private $eje;
    private $descripcion;
    
    function setAll($id, $eje, $descripcion) {
        $this->id = $id;
        $this->eje = $eje;
        $this->descripcion = $descripcion;
    }

    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getEje() {
        return $this->eje;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEje($eje) {
        $this->eje = $eje;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }


}
