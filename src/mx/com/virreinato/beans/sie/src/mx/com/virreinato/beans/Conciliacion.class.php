<?php

class Conciliacion {
    
    private $id;
    private $mes;
    private $idPeriodo;
    private $saldoInicial;
    private $ingresosMuseo;
    private $egresosMuseo;
    private $saldoFinalMuseo;
    private $ingresosBanco;
    private $egresosBanco;
    private $saldoFinalBanco;
    private $diferencia;
    private $fechaConciliacion;
    private $observaciones;
    private $status;
    
    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getMes() {
        return $this->mes;
    }

    public function getIdPeriodo() {
        return $this->idPeriodo;
    }

    public function getSaldoInicial() {
        return $this->saldoInicial;
    }

    public function getIngresosMuseo() {
        return $this->ingresosMuseo;
    }

    public function getEgresosMuseo() {
        return $this->egresosMuseo;
    }

    public function getSaldoFinalMuseo() {
        return $this->saldoFinalMuseo;
    }

    public function getIngresosBanco() {
        return $this->ingresosBanco;
    }

    public function getEgresosBanco() {
        return $this->egresosBanco;
    }

    public function getSaldoFinalBanco() {
        return $this->saldoFinalBanco;
    }

    public function getDiferencia() {
        return $this->diferencia;
    }

    public function getFechaConciliacion() {
        return $this->fechaConciliacion;
    }

    public function getObservaciones() {
        return $this->observaciones;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setMes($mes) {
        $this->mes = $mes;
    }

    public function setIdPeriodo($idPeriodo) {
        $this->idPeriodo = $idPeriodo;
    }

    public function setSaldoInicial($saldoInicial) {
        $this->saldoInicial = $saldoInicial;
    }

    public function setIngresosMuseo($ingresosMuseo) {
        $this->ingresosMuseo = $ingresosMuseo;
    }

    public function setEgresosMuseo($egresosMuseo) {
        $this->egresosMuseo = $egresosMuseo;
    }

    public function setSaldoFinalMuseo($saldoFinalMuseo) {
        $this->saldoFinalMuseo = $saldoFinalMuseo;
    }

    public function setIngresosBanco($ingresosBanco) {
        $this->ingresosBanco = $ingresosBanco;
    }

    public function setEgresosBanco($egresosBanco) {
        $this->egresosBanco = $egresosBanco;
    }

    public function setSaldoFinalBanco($saldoFinalBanco) {
        $this->saldoFinalBanco = $saldoFinalBanco;
    }

    public function setDiferencia($diferencia) {
        $this->diferencia = $diferencia;
    }

    public function setFechaConciliacion($fechaConciliacion) {
        $this->fechaConciliacion = $fechaConciliacion;
    }

    public function setObservaciones($observaciones) {
        $this->observaciones = $observaciones;
    }

    public function setStatus($status) {
        $this->status = $status;
    }


}
