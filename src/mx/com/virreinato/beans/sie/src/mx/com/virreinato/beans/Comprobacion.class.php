<?php

class Comprobacion {
    private $idComprobacion; 
    private $idPeriodo;
    private $folio;
    private $fecha;
    private $descripcion;
    private $montoComprobacion;
    private $destino;
    private $idProyecto;
    private $idCheque;
    private $folioCheque;
    private $empleado;
    private $proveedor;
    private $beneficiario;
    private $observaciones;
    private $estatus;
    
    function setAll($idComprobacion, $idPeriodo, $folio, $fecha, $descripcion, $montoComprobacion, $destino, $idProyecto, $idCheque, $folioCheque, $empleado, $proveedor, $beneficiario, $observaciones, $estatus) {
        $this->idComprobacion = $idComprobacion;
        $this->idPeriodo = $idPeriodo;
        $this->folio = $folio;
        $this->fecha = $fecha;
        $this->descripcion = $descripcion;
        $this->montoComprobacion = $montoComprobacion;
        $this->destino = $destino;
        $this->idProyecto = $idProyecto;
        $this->idCheque = $idCheque;
        $this->folioCheque = $folioCheque;
        $this->empleado = $empleado;
        $this->proveedor = $proveedor;
        $this->beneficiario = $beneficiario;
        $this->observaciones = $observaciones;
        $this->estatus = $estatus;
    }

    function __construct() {
        
    }

    public function getIdComprobacion() {
        return $this->idComprobacion;
    }

    public function getIdPeriodo() {
        return $this->idPeriodo;
    }

    public function getFolio() {
        return $this->folio;
    }

    public function getFecha() {
        return $this->fecha;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function getMontoComprobacion() {
        return $this->montoComprobacion;
    }

    public function getDestino() {
        return $this->destino;
    }

    public function getIdProyecto() {
        return $this->idProyecto;
    }

    public function getIdCheque() {
        return $this->idCheque;
    }

    public function getFolioCheque() {
        return $this->folioCheque;
    }

    public function getEmpleado() {
        return $this->empleado;
    }

    public function getProveedor() {
        return $this->proveedor;
    }

    public function getBeneficiario() {
        return $this->beneficiario;
    }

    public function getObservaciones() {
        return $this->observaciones;
    }

    public function getEstatus() {
        return $this->estatus;
    }

    public function setIdComprobacion($idComprobacion) {
        $this->idComprobacion = $idComprobacion;
    }

    public function setIdPeriodo($idPeriodo) {
        $this->idPeriodo = $idPeriodo;
    }

    public function setFolio($folio) {
        $this->folio = $folio;
    }

    public function setFecha($fecha) {
        $this->fecha = $fecha;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function setMontoComprobacion($montoComprobacion) {
        $this->montoComprobacion = $montoComprobacion;
    }

    public function setDestino($destino) {
        $this->destino = $destino;
    }

    public function setIdProyecto($idProyecto) {
        $this->idProyecto = $idProyecto;
    }

    public function setIdCheque($idCheque) {
        $this->idCheque = $idCheque;
    }

    public function setFolioCheque($folioCheque) {
        $this->folioCheque = $folioCheque;
    }

    public function setEmpleado($empleado) {
        $this->empleado = $empleado;
    }

    public function setProveedor($proveedor) {
        $this->proveedor = $proveedor;
    }

    public function setBeneficiario($beneficiario) {
        $this->beneficiario = $beneficiario;
    }

    public function setObservaciones($observaciones) {
        $this->observaciones = $observaciones;
    }

    public function setEstatus($estatus) {
        $this->estatus = $estatus;
    }


}
