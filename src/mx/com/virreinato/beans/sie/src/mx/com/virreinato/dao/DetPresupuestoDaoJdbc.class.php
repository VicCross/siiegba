<?php
if(session_id() == '') {
    session_start();
}

include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/DetPresupuesto.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class DetPresupuestoDaoJdbc {
    
    public function obtieneListado($idPresupuesto) {
		
        $lista= array();
        $query="SELECT p.spr_detallesprod, p.spr_productosobtenidos,p.dsp_id_det_solpresup, p.spr_id_solicitud, p.spr_monto, a.cag_descripcion, pa.cpa_descripcion,pa.CPA_PARTIDA, proy.cpr_descripcion, m.proyecto_desc_meta FROM sie_solicitud_presup pre, sie_det_sol_presup p, cat_log_activgral a , sie_cat_partidas pa, sie_cat_proyectos proy, sie_proyecto_metas m WHERE p.cpa_id_partida = pa.cpa_id_partida AND p.cag_id_log_activgral = a.cag_id_log_activgral AND p.spr_estatus = 1 AND p.spr_id_solicitud = ".(int)($idPresupuesto)." AND p.spr_id_solicitud = pre.spr_id_solicitud AND  pre.proyecto_id_meta = m.proyecto_id_meta AND m.proyecto_estatus_meta = 1 AND m.cpr_id_proyecto = proy.cpr_id_proyecto";

        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[("dsp_id_det_solpresup")];
            $idPresup= $rs[("spr_id_solicitud")];
            $actividad = $rs[("cag_descripcion")];
            $monto = $rs[("spr_monto")];
            $partida = $rs[("CPA_PARTIDA")]." ".$rs[("cpa_descripcion")];
            $productos = $rs[("spr_productosobtenidos")];
            $detalles = $rs[("spr_detallesprod")];
            $proyecto = $rs[("cpr_descripcion")];
            $meta = $rs[("proyecto_desc_meta")];

            $elemento = new DetPresupuesto();
            $elemento->constructor3($id,$idPresup,$monto,$productos,$detalles,$actividad,$partida, $proyecto, $meta);
            array_push($lista, $elemento);
        }	
        return $lista;
    }
    
    public function obtieneListadoGb($idPresupuesto) {
		
        $lista= array();
        $query="SELECT p.spr_detallesprod, p.spr_productosobtenidos,p.dsp_id_det_solpresup, p.spr_id_solicitud, p.spr_monto, a.cag_descripcion, pa.cpa_descripcion, pa.CPA_PARTIDA FROM sie_solicitud_presup pre, sie_det_sol_presup p, cat_log_activgral a , sie_cat_partidas pa WHERE p.cpa_id_partida = pa.cpa_id_partida AND p.cag_id_log_activgral = a.cag_id_log_activgral AND p.spr_estatus = 1 AND p.spr_id_solicitud = ".(int)($idPresupuesto)." AND p.spr_id_solicitud = pre.spr_id_solicitud ";

        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs["dsp_id_det_solpresup"];
            $idPresup= $rs["spr_id_solicitud"];
            $actividad = $rs["cag_descripcion"];
            $monto = $rs["spr_monto"];
            $partida = $rs[("CPA_PARTIDA")]." ".$rs["cpa_descripcion"];
            $productos = $rs["spr_productosobtenidos"];
            $detalles = $rs["spr_detallesprod"];
            $proyecto = "";
            $meta = "";

            $elemento = new DetPresupuesto();
            $elemento->constructor3($id,$idPresup,$monto,$productos,$detalles,$actividad,$partida, $proyecto, $meta);
            array_push($lista, $elemento);
        }	

        return $lista;
    }
    
    public function obtieneListadoProgramas($idPresupuesto) {
		
	$lista= array();
	$query=strtolower("SELECT p.spr_detallesprod, p.spr_productosobtenidos,p.dsp_id_det_solpresup, " .
            "p.spr_id_solicitud, p.spr_monto, a.cag_descripcion, pa.cpa_descripcion, pa.CPA_PARTIDA, proy.cpr_descripcion, m.proyecto_desc_meta, PI.cpi_programa, PO.cpo_programa " .
            " FROM sie_solicitud_presup pre, sie_det_sol_presup p, cat_log_activgral a ," .
            " sie_cat_partidas pa, sie_cat_proyectos proy, sie_proyecto_metas m, SIE_CAT_PROGRAMA_INSTITUCIONAL PI, SIE_CAT_PROGRAMA_OPERATIVO PO " .
            " WHERE p.cpa_id_partida = pa.cpa_id_partida AND p.cag_id_log_activgral = a.cag_id_log_activgral " .
            " AND p.spr_estatus = 1 AND p.spr_id_solicitud = ".(int)($idPresupuesto)." AND " .
            " p.spr_id_solicitud = pre.spr_id_solicitud AND  pre.proyecto_id_meta = m.proyecto_id_meta AND m.CPI_ID_PROGRAMA=PI.CPI_ID_PROGRAMA AND m.CPO_ID_PROGRAMA=PO.CPO_ID_PROGRAMA AND " .
            " m.proyecto_estatus_meta = 1 AND m.cpr_id_proyecto = proy.cpr_id_proyecto");
		
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[("dsp_id_det_solpresup")];
            $idPresup= $rs[("spr_id_solicitud")];
            $actividad = $rs[("cag_descripcion")];
            $monto = $rs[("spr_monto")];
            $partida = $rs[("CPA_PARTIDA")]." ".$rs[("cpa_descripcion")];
            $productos = $rs[("spr_productosobtenidos")];
            $detalles = $rs[("spr_detallesprod")];
            $proyecto = $rs[("cpr_descripcion")];
            $meta = $rs[("proyecto_desc_meta")];
            $poperativo = $id= $rs[("cpo_programa")];
            $pinstitucional = $id= $rs[("cpi_programa")];

            $elemento = new DetPresupuesto();
            $elemento->constructor4($id,$idPresup,$monto,$productos,$detalles, $actividad,$partida, $proyecto, $meta,$pinstitucional,$poperativo);
            array_push($lista, $elemento);
        }	
        return $lista;
    }
    
    public function obtieneListadoProgramasGb($idPresupuesto) {
		
	$lista= array();
        $query="SELECT p.spr_detallesprod, p.spr_productosobtenidos,p.dsp_id_det_solpresup, " .
            "p.spr_id_solicitud, p.spr_monto, a.cag_descripcion, pa.cpa_descripcion, pa.CPA_PARTIDA " .
            " FROM sie_solicitud_presup pre, sie_det_sol_presup p, cat_log_activgral a ," .
            " sie_cat_partidas pa  " .
            " WHERE p.cpa_id_partida = pa.cpa_id_partida AND p.cag_id_log_activgral = a.cag_id_log_activgral " .
            " AND p.spr_estatus = 1 AND p.spr_id_solicitud = ".(int)($idPresupuesto)." AND " .
            " p.spr_id_solicitud = pre.spr_id_solicitud   " ;

        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs["dsp_id_det_solpresup"];
            $idPresup= $rs["spr_id_solicitud"];
            $actividad = $rs["cag_descripcion"];
            $monto = $rs["spr_monto"];
            $partida = $rs["CPA_PARTIDA"]." ".$rs["cpa_descripcion"];
            $productos = $rs["spr_productosobtenidos"];
            $detalles = $rs["spr_detallesprod"];
            $proyecto = "";
            $meta = "";
            $poperativo = "";
            $pinstitucional = "";

            $elemento = new DetPresupuesto();
            $elemento->constructor4($id,$idPresup,$monto,$productos,$detalles, $actividad,$partida, $proyecto, $meta,$pinstitucional,$poperativo);
            array_push($lista, $elemento);
        }	

        return $lista;
    }
    
    public function obtieneElemento($idElemento) {
		
		
        $elemento = new DetPresupuesto();
        $query="SELECT * FROM sie_det_sol_presup WHERE  spr_estatus = 1 AND dsp_id_det_solpresup = ".$idElemento;

        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){

            $id= $rs[strtoupper("dsp_id_det_solpresup")];
            $idPresup= $rs[strtoupper("spr_id_solicitud")];
            $idActividad= $rs[strtoupper("cag_id_log_activgral")];
            $idPartida= $rs[strtoupper("cpa_id_partida")];
            $monto = $rs[strtoupper("spr_monto")];
            $productos= $rs[strtoupper("spr_productosobtenidos")];
            $det = $rs[strtoupper("spr_detallesprod")];

            $elemento->constructor1($id,$idPresup,$idActividad,$idPartida,$monto,$productos,$det);

        }		
		
        return $elemento;
		
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $query="INSERT INTO sie_det_sol_presup (SPR_ID_SOLICITUD,CAG_ID_LOG_ACTIVGRAL,CPA_ID_PARTIDA,SPR_MONTO,SPR_PRODUCTOSOBTENIDOS,SPR_DETALLESPROD,SPR_ESTATUS) VALUES (".$elemento->getIdPresupuesto().", ". $elemento->getIdActiviad() .", ".
		$elemento->getIdPartida().",". $elemento->getMonto() .",". $elemento->getProductos_obtenidos() .", '". $elemento->getDetalle_productos() ."', 1)";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
	$query="UPDATE sie_det_sol_presup  SET  spr_id_solicitud = ".$elemento->getIdPresupuesto().", cag_id_log_activgral = ".$elemento->getIdActiviad(). ", " .
				"cpa_id_partida = ". $elemento->getIdPartida() ." , spr_monto = ". $elemento->getMonto() ."," .
				"spr_productosobtenidos = ". $elemento->getProductos_obtenidos() ." ,spr_detallesprod ='".$elemento->getDetalle_productos()."' WHERE dsp_id_det_solpresup = ". $elemento->getId() ."";
        $res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_det_sol_presup  set  spr_estatus = 0 WHERE dsp_id_det_solpresup = ".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function obtieneDetalle($idPresupuesto){
        $resultado  = null;
        $query = "SELECT p.cpr_descripcion, m.proyecto_desc_meta FROM sie_cat_proyectos p, sie_proyecto_metas m, sie_solicitud_presup pr WHERE pr.spr_id_solicitud = ".$idPresupuesto." AND pr.proyecto_id_meta = m.proyecto_id_meta AND m.cpr_id_proyecto = p.cpr_id_proyecto";
        $con=new Catalogo();

        $result=$con->obtenerLista($query);
        if($rs = mysql_fetch_array($result)){
                $resultado = $rs[("cpr_descripcion")]."/".$rs[("proyecto_desc_meta")];
        }

        return $resultado;
    }
}
