<?php

class ComprobaINAHDet {
    private $id;
    private $idComprobacionINAH;
    private $idPartida;
    private $desPartida;
    private $partida;
    private $monto;
    private $num_notas;
    
    function comprobaINAHDet($id, $idComprobacionINAH, $idPartida, $monto, $num_notas) {
        $this->id = $id;
        $this->idComprobacionINAH = $idComprobacionINAH;
        $this->idPartida = $idPartida;
        $this->monto = $monto;
        $this->num_notas = $num_notas;
    }
    
    function setAll($id, $idComprobacionINAH, $idPartida, $desPartida, $partida, $monto, $num_notas) {
        $this->id = $id;
        $this->idComprobacionINAH = $idComprobacionINAH;
        $this->idPartida = $idPartida;
        $this->desPartida = $desPartida;
        $this->partida = $partida;
        $this->monto = $monto;
        $this->num_notas = $num_notas;
    }

    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getIdComprobacionINAH() {
        return $this->idComprobacionINAH;
    }

    public function getIdPartida() {
        return $this->idPartida;
    }

    public function getDesPartida() {
        return $this->desPartida;
    }

    public function getPartida() {
        return $this->partida;
    }

    public function getMonto() {
        return $this->monto;
    }

    public function getNum_notas() {
        return $this->num_notas;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setIdComprobacionINAH($idComprobacionINAH) {
        $this->idComprobacionINAH = $idComprobacionINAH;
    }

    public function setIdPartida($idPartida) {
        $this->idPartida = $idPartida;
    }

    public function setDesPartida($desPartida) {
        $this->desPartida = $desPartida;
    }

    public function setPartida($partida) {
        $this->partida = $partida;
    }

    public function setMonto($monto) {
        $this->monto = $monto;
    }

    public function setNum_notas($num_notas) {
        $this->num_notas = $num_notas;
    }


}
