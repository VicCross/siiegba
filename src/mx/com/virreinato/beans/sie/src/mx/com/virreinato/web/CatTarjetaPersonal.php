<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/TarjetaPersonalDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/TarjetaPersonal.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$respuesta = "";
if (isset($_POST['id_Tarjeta'])){
    $elemento=new TarjetaPersonal();
    $elemento->setAsignado($_POST["Asignado"]);
    $elemento->setGrado($_POST["grado_personal"]);
    $elemento->setIdTarjeta((int)($_POST["id_Tarjeta"]));
    $elemento->setInstituciones($_POST["instituciones_personal"]);
    $elemento->setNombre($_POST["nombre_personal"]);
    $elemento->setPuesto($_POST["puesto_personal"]);
    $elemento->setNum_Personal((int)($_POST["numero_personal"]));
    if ($_POST["id"] != null){
        $elemento->setIdPersonal((int)($_POST["id"]));
        $dao=new TarjetaPersonalDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new TarjetaPersonalDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
    header("Location: ../../../../../Proyectos/lista_Personal.php?respuesta=" . $respuesta."&idTarjeta=".$_POST['id_Tarjeta']);
}else if (isset($_GET['id'])){
    $dao=new TarjetaPersonalDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
    header("Location: ../../../../../Proyectos/lista_Personal.php?respuesta=" . $respuesta."&idTarjeta=".$_GET['id_Tarjeta']);
} else {
    $respuesta = "No se detecto la acción a realizar.";
    header("Location: ../../../../../Proyectos/lista_Personal.php?respuesta=" . $respuesta);
}
?>

