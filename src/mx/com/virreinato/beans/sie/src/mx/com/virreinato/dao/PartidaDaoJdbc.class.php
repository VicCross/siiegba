<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Partida.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Capitulo.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class PartidaDaoJdbc {

    public function obtieneListado() {
		
	$lista= array();
		
	$query="SELECT * FROM sie_cat_partidas WHERE cpa_estatus=1 ORDER BY cpa_capitulo,cpa_partida";
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);
			
	while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cpa_id_partida")];
            $id_capitulo= $rs[strtoupper("cca_id_capitulo")];
            $capitulo= $rs[strtoupper("cpa_capitulo")];
            $cpa_partida= $rs[strtoupper("cpa_partida")];
            $descripcion= $rs[strtoupper("cpa_descripcion")];

            $cap = new Capitulo($id_capitulo,"",$capitulo);

            $elemento= new Partida();
            $elemento->setAll($id,$cap,$cpa_partida,$descripcion);
            array_push($lista, $elemento);
        }	
	return $lista;
    }
    
    public function obtieneElemento($idElemento) {
		
		
	$elemento=new Partida();
		
	$query="SELECT * FROM sie_cat_partidas WHERE cpa_id_partida=".$idElemento;
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);
			
	while ($rs = mysql_fetch_array($result)){
								
            $id= $rs[strtoupper("cpa_id_partida")];
            $id_capitulo= $rs[strtoupper("cca_id_capitulo")];
            $capitulo= $rs[strtoupper("cpa_capitulo")];
            $cpa_partida= $rs[strtoupper("cpa_partida")];
            $descripcion= $rs[strtoupper("cpa_descripcion")];
				
            $cap = new Capitulo($id_capitulo,"",$capitulo);
				
            $elemento= new Partida();
            $elemento->setAll($id,$cap,$cpa_partida,$descripcion);
	}

	return $elemento;
		
	}
        
    public function guardaElemento($elemento) {
		
	$catalogo = new Catalogo();
	$query="INSERT INTO sie_cat_partidas(cca_id_capitulo,cpa_capitulo,cpa_partida,cpa_descripcion) VALUES ('".$elemento->getCapitulo()->getId()."' , '".$elemento->getCapitulo()->getCapitulo()."' , '".$elemento->getPartida()."' , '".$elemento->getDescripcion()."')";
	$result = $catalogo->obtenerLista($query);
		
        if($result == "1"){return true;}
        else{ return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$catalogo = new Catalogo();
	$query="UPDATE sie_cat_partidas set  cca_id_capitulo='".$elemento->getCapitulo()->getId()."', cpa_capitulo='".$elemento->getCapitulo()->getCapitulo()."' , cpa_partida='".$elemento->getPartida()."' , cpa_descripcion='".$elemento->getDescripcion()."' WHERE cpa_id_partida=".$elemento->getId();
	$result = $catalogo->obtenerLista($query);
		
        if($result == "1"){return true;}
        else{ return false; }	
    }
    
    public function eliminaElemento($idElemento){
		
	$catalogo = new Catalogo();
	$query="UPDATE sie_cat_partidas set  cpa_estatus=0 WHERE cpa_id_partida=".$idElemento;
	$result = $catalogo->obtenerLista($query);
		
        if($result == "1"){return true;}
        else{ return false; }
		
    }
}
