<?php

class Empleado {

    private $id;
    private $nombre;
    private $apPaterno;
    private $apMaterno;
    private $sueldoNeto;
    private $sueldoLetra;
    private $puesto;
    private $rfc;
    private $tipoNomina;
    private $numero;

    function construct_default($id, $nombre, $apPaterno, $apMaterno) {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->apMaterno = $apMaterno;
        $this->apPaterho = $apPaterno;
    }

    function setAll($id, $nombre, $apPaterno, $apMaterno, $sueldoNeto, $sueldoLetra, $puesto, $rfc, $tipoNomina, $numero) {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->apPaterno = $apPaterno;
        $this->apMaterno = $apMaterno;
        $this->sueldoNeto = $sueldoNeto;
        $this->sueldoLetra = $sueldoLetra;
        $this->puesto = $puesto;
        $this->rfc = $rfc;
        $this->tipoNomina = $tipoNomina;
        $this->numero = $numero;
    }
    
    function __construct() {
        
    }

        public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function getApMaterno() {
        return $this->apMaterno;
    }

    public function setApMaterno($apMaterno) {
        $this->apMaterno = $apMaterno;
    }

    public function getApPaterno() {
        return $this->apPaterno;
    }

    public function setApPaterno($apPaterno) {
        $this->apPaterno = $apPaterno;
    }

    public function getSueldoNeto() {
        return $this->sueldoNeto;
    }

    public function setSueldoNeto($sueldoNeto) {
        $this->sueldoNeto = $sueldoNeto;
    }

    public function getPuesto() {
        return $this->puesto;
    }

    public function setPuesto($puesto) {
        $this->puesto = $puesto;
    }

    public function getRfc() {
        return $this->rfc;
    }

    public function setRfc($rfc) {
        $this->rfc = $rfc;
    }

    public function getTipoNomina() {
        return $this->tipoNomina;
    }

    public function setTipoNomina($tipoNomina) {
        $this->tipoNomina = $tipoNomina;
    }

    public function getSueldoLetra() {
        return $this->sueldoLetra;
    }

    public function setSueldoLetra($sueldoLetra) {
        $this->sueldoLetra = $sueldoLetra;
    }

    public function getNumero() {
        return $this->numero;
    }

    public function setNumero($numero) {
        $this->numero = $numero;
    }

}

?>
