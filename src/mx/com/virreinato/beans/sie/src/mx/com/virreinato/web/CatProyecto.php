<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/CatProyectoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatProyecto.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Area.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/AreaNormativa.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CentroCostos.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Empleado.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/LineaAccion.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$respuesta = "";
if (isset($_POST['guardar'])){
    $elemento=new CatProyecto();
			
    $elemento->setNumero($_POST["numero"]);

    $periodo=new Periodo();
    $periodo->setId((int)($_POST["id_periodo"]));
    $elemento->setPeriodo($periodo);

    $areaNormativa=new AreaNormativa();
    $areaNormativa->setId((int)($_POST["id_arean"]));
    $elemento->setAreaNormativa($areaNormativa);

    $centroCostos=new CentroCostos();
    $centroCostos->setId((int)($_POST["id_ccosto"]));
    $elemento->setCentroCostos($centroCostos);

    $empleado=new Empleado();
    $empleado->setId((int)($_POST["id_lider"]));
    $elemento->setLider($empleado);

    $lineaAccion=new LineaAccion();
    $lineaAccion->setId((int)($_POST["id_linea"]));
    $elemento->setLineaAccion($lineaAccion);


    $area=new Area();
    $area->setId((int)($_POST["id_area"]));
    $elemento->setArea($area);


    $elemento->setDescripcion($_POST["descripcion"]);
    $elemento->setOrigen($_POST["origen"]);
    $elemento->setMonto((double)($_POST["costo"]));

    $elemento->setAntecedentes($_POST["antecedentes"]);
    $elemento->setObjetivoGral($_POST["objetivo"]);
    $elemento->setImpactoSocial($_POST["impacto_soc"]);
    $elemento->setImpactoOrganizacional($_POST["impacto_org"]);

    $elemento->setImpactoEstrategico($_POST["impacto_est"]);
    $elemento->setBeneficiarios($_POST["beneficiarios"]);
    if(isset($_POST['estatus'])){
        $elemento->setEstatus($_POST["estatus"]);
    }else{
        $elemento->setEstatus(null);
    }
    $elemento->setTipoproy($_POST["tipo"]);
    $elemento->setLimites($_POST["limites"]);


    if($_POST["finicio_tot"]!=null && $_POST["finicio_tot"]!="" ){
            $finicio_tot= date("d-m-Y",strtotime($_POST["finicio_tot"]));
            $elemento->setFechaIniProyecto($finicio_tot);
    }	
    if($_POST["ffin_tot"]!=null && $_POST["ffin_tot"]!=""){				
            $ffin_tot= date("d-m-Y",strtotime($_POST["ffin_tot"]));
            $elemento->setFechaFinProyecto($ffin_tot);
    }	
    if($_POST["finicio_eje"]!=null && $_POST["finicio_eje"]!="" ){
            $finicio_eje= date("d-m-Y",strtotime($_POST["finicio_eje"]));
            $elemento->setFechaIniEjercicio($finicio_eje);				
    }
    if($_POST["ffin_eje"]!=null && $_POST["ffin_eje"]!="" ){
            $ffin_eje= date("d-m-Y",strtotime($_POST["finicio_eje"]));
            $elemento->setFechaFinEjercicio($ffin_eje);
    }
    if ($_POST["id"] != null){
        $elemento->setId((int)($_POST["id"]));
        $dao=new CatProyectoDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new CatProyectoDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
}else if (isset($_GET['id'])){
    $dao=new CatProyectoDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
} else {
    $respuesta = "No se detecto la acción a realizar.";
}
header("Location: ../../../../../catalogos/lista_proyecto.php?respuesta=" . $respuesta);
?>