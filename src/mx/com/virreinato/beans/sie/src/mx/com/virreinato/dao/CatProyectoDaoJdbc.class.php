<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatProyecto.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Area.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/AreaNormativa.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CentroCostos.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Eje.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Empleado.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/LineaAccion.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class CatProyectoDaoJdbc {
    
    public function periodoEstaAbierto($idElemento) {
		
	$resultado=false;
		
		
	$query=strtolower("SELECT PE.CPE_CERRADO FROM sie_cat_proyectos P, SIE_CAT_PERIODOS PE  WHERE".
				" P.cpe_id_periodo=PE.cpe_id_periodo AND P.cpr_id_proyecto=".$idElemento);
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
				
            $id= $rs["cpe_cerrado"];

            if($id == 0){
                    $resultado=true;
            }
            else if($id ==1){
                    $resultado=false;
            }	
	}
		
	return $resultado;
		
    }
    
    public function obtieneListado( $idPerfil , $idUsuario ) {
		
        $lista= array();
        $query = "";

        if( $idPerfil == 7 ){

            $query=strtolower("SELECT * FROM sie_usuario_x_meta um, sie_proyecto_metas pm, sie_cat_proyectos P, sie_cat_areanormativa A, sie_cat_centrocosto CC, cat_empleado E, sie_cat_linea_accion LA, sie_cat_ejes EJ,  cat_areas AR, SIE_CAT_PERIODOS PE ".
                    " WHERE um.cus_id_usuario = ".$idUsuario." AND um.proyecto_id_meta = pm.proyecto_id_meta AND pm.cpr_id_proyecto = P.cpr_id_proyecto AND  P.can_id_areanormativa=A.can_id_areanormativa AND P.ccc_id_ccosto=CC.ccc_id_ccosto AND cpr_id_lider=E.cem_id_empleado AND P.cla_id_lineaaccion=LA.cla_id_lineaaccion AND " .
                    " LA.cej_id_eje=EJ.cej_id_eje AND P.car_id_area=AR.car_id_area  AND P.cpe_id_periodo=PE.cpe_id_periodo AND  cpr_estatus='1' ORDER BY PE.CPE_PERIODO,P.cpr_descripcion");

        }else{

            $query=strtolower("SELECT * FROM sie_cat_proyectos P, sie_cat_areanormativa A, sie_cat_centrocosto CC, cat_empleado E, sie_cat_linea_accion LA, sie_cat_ejes EJ,  cat_areas AR, SIE_CAT_PERIODOS PE ".
                    " WHERE P.can_id_areanormativa=A.can_id_areanormativa AND P.ccc_id_ccosto=CC.ccc_id_ccosto AND cpr_id_lider=E.cem_id_empleado AND P.cla_id_lineaaccion=LA.cla_id_lineaaccion AND " .
                    " LA.cej_id_eje=EJ.cej_id_eje AND P.car_id_area=AR.car_id_area  AND P.cpe_id_periodo=PE.cpe_id_periodo AND  cpr_estatus='1' ORDER BY PE.CPE_PERIODO,P.cpr_descripcion");
        }

        $catalogo = new Catalogo();

        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cpr_id_proyecto")];

            $can_id_areanormativa= $rs[strtoupper("can_id_areanormativa")];
            $numero= $rs[strtoupper("cpr_numeroproyecto")];

            $periodo = new Periodo();
            $periodo->setAll($rs[strtoupper("CPE_ID_PERIODO")],$rs[strtoupper("CPE_PERIODO")],$rs[strtoupper("CPE_CERRADO")],$rs[strtoupper("CPE_FECHA_INI")],$rs[strtoupper("CPE_FECHA_FIN")]); 

            $can_descripcion= $rs[strtoupper("can_descripcion")];
            $arean = new AreaNormativa();
            $arean->setAll($can_id_areanormativa,$can_descripcion);

            $ccc_id_ccosto= $rs[strtoupper("ccc_id_ccosto")];
            $ccc_clave_cc= $rs[strtoupper("ccc_clave_cc")];
            $ccc_descripcion= $rs[strtoupper("ccc_descripcion")];
            $ccostos = new CentroCostos();
            $ccostos->setAll($ccc_id_ccosto,$ccc_clave_cc,$ccc_descripcion);

            $cem_id_empleado= $rs["cem_id_empleado"];				
            $cem_nombre= $rs[strtoupper("cem_nombre")];
            $cem_appaterno= $rs[strtoupper("cem_appaterno")];
            $cem_apmaterno= $rs[strtoupper("cem_apmaterno")];
            $empleado = new Empleado();
            $empleado->construct_default($cem_id_empleado,$cem_nombre,$cem_appaterno,$cem_apmaterno);


            $cej_id_eje= $rs[strtoupper("cej_id_eje")];
            $cej_eje= $rs[strtoupper("cej_eje")];
            $cej_descripcion= $rs[strtoupper("cej_descripcion")];
            $eje = new Eje();
            $eje->setAll($cej_id_eje,$cej_eje,$cej_descripcion);


            $cla_id_lineaaccion= $rs[strtoupper("cla_id_lineaaccion")];
            $cla_lineaaccion= $rs[strtoupper("cla_lineaaccion")];
            $cla_descripcion= $rs[strtoupper("cla_descripcion")];
            $linea = new LineaAccion();
            $linea->setAll($cla_id_lineaaccion,$eje,$cla_lineaaccion,$cla_descripcion);

            $cpr_descripcion= $rs[strtoupper("cpr_descripcion")];
            $cpr_origpresup= $rs[strtoupper("cpr_origpresup")];
            $cpr_costo= $rs[strtoupper("cpr_costo")];
            $cpr_antecedentes= $rs[strtoupper("cpr_antecedentes")];
            $cpr_objetivogral= $rs[strtoupper("cpr_objetivogral")];
            $cpr_limites= $rs[strtoupper("cpr_limites")];
            $cpr_imp_soc= $rs[strtoupper("cpr_imp_soc")];
            $cpr_imp_organiacional= $rs[strtoupper("cpr_imp_organiacional")];
            $cpr_imp_estrategico= $rs[strtoupper("cpr_imp_estrategico")];
            $cpr_beneficiarios= $rs[strtoupper("cpr_beneficiarios")];
            $cpr_estatus= $rs[strtoupper("cpr_estatus")];
            $cpr_tipoproy= $rs[strtoupper("cpr_tipoproy")];
            $cpr_fecha_ini_total= $rs[strtoupper("cpr_fecha_ini_total")];
            $cpr_fecha_fin_total= $rs[strtoupper("cpr_fecha_fin_total")];
            $cpr_fecha_ini_ejer= $rs[strtoupper("cpr_fecha_ini_ejer")];
            $cpr_fecha_fin_ejer= $rs[strtoupper("cpr_fecha_fin_ejer")];

            $car_id_area= $rs["car_id_area"];
            $car_area= $rs[strtoupper("car_area")];
            $area = new Area();
            $area->setAll($car_id_area,$car_area);	

            $elemento = new CatProyecto();				
            $elemento->setAll($id,$arean,$ccostos,$cpr_descripcion,$cpr_origpresup,$empleado,$cpr_costo,$linea,$numero, $periodo,$cpr_antecedentes,$cpr_objetivogral,$cpr_limites,$cpr_imp_soc,$cpr_imp_organiacional,$cpr_imp_estrategico,$cpr_beneficiarios,$cpr_estatus,$cpr_tipoproy,$cpr_fecha_ini_total,$cpr_fecha_fin_total,$cpr_fecha_ini_ejer,$cpr_fecha_fin_ejer,$area);
            array_push($lista, $elemento);
                }	
        return $lista;
    }
    
    public function obtieneListadoAbierto( $idPerfil , $idUsuario ) {
		
        $lista= array();
        $query = "";

        if( $idPerfil == 7 ){

            $query=strtolower("SELECT * FROM sie_usuario_x_meta um, sie_proyecto_metas pm, sie_cat_proyectos P, sie_cat_areanormativa A, sie_cat_centrocosto CC, cat_empleado E, sie_cat_linea_accion LA, sie_cat_ejes EJ,  cat_areas AR, SIE_CAT_PERIODOS PE ".
                                " WHERE um.cus_id_usuario = ".$idUsuario." AND um.proyecto_id_meta = pm.proyecto_id_meta AND pm.cpr_id_proyecto = P.cpr_id_proyecto AND  P.can_id_areanormativa=A.can_id_areanormativa AND P.ccc_id_ccosto=CC.ccc_id_ccosto AND cpr_id_lider=E.cem_id_empleado AND P.cla_id_lineaaccion=LA.cla_id_lineaaccion AND " .
                                " LA.cej_id_eje=EJ.cej_id_eje AND P.car_id_area=AR.car_id_area  AND P.cpe_id_periodo=PE.cpe_id_periodo AND  cpr_estatus='1' AND  PE.cpe_cerrado=0  ORDER BY PE.CPE_PERIODO,P.cpr_descripcion");

        }else{

            $query=strtolower("SELECT * FROM sie_cat_proyectos P, sie_cat_areanormativa A, sie_cat_centrocosto CC, cat_empleado E, sie_cat_linea_accion LA, sie_cat_ejes EJ,  cat_areas AR, SIE_CAT_PERIODOS PE ".
                                " WHERE P.can_id_areanormativa=A.can_id_areanormativa AND P.ccc_id_ccosto=CC.ccc_id_ccosto AND cpr_id_lider=E.cem_id_empleado AND P.cla_id_lineaaccion=LA.cla_id_lineaaccion AND " .
                                " LA.cej_id_eje=EJ.cej_id_eje AND P.car_id_area=AR.car_id_area  AND P.cpe_id_periodo=PE.cpe_id_periodo AND  cpr_estatus='1'  AND  PE.cpe_cerrado=0 ORDER BY PE.CPE_PERIODO,P.cpr_descripcion");
        }
        
        $catalogo = new Catalogo();

        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cpr_id_proyecto")];

            $can_id_areanormativa= $rs[strtoupper("can_id_areanormativa")];
            $numero= $rs[strtoupper("cpr_numeroproyecto")];

            $periodo = new Periodo();
            $periodo->setAll($rs[strtoupper("CPE_ID_PERIODO")],$rs[strtoupper("CPE_PERIODO")],$rs[strtoupper("CPE_CERRADO")],$rs[strtoupper("CPE_FECHA_INI")],$rs[strtoupper("CPE_FECHA_FIN")]); 

            $can_descripcion= $rs[strtoupper("can_descripcion")];
            $arean = new AreaNormativa();
            $arean->setAll($can_id_areanormativa,$can_descripcion);

            $ccc_id_ccosto= $rs[strtoupper("ccc_id_ccosto")];
            $ccc_clave_cc= $rs[strtoupper("ccc_clave_cc")];
            $ccc_descripcion= $rs[strtoupper("ccc_descripcion")];
            $ccostos = new CentroCostos();
            $ccostos->setAll($ccc_id_ccosto,$ccc_clave_cc,$ccc_descripcion);

            $cem_id_empleado= $rs["cem_id_empleado"];				
            $cem_nombre= $rs[strtoupper("cem_nombre")];
            $cem_appaterno= $rs[strtoupper("cem_appaterno")];
            $cem_apmaterno= $rs[strtoupper("cem_apmaterno")];
            $empleado = new Empleado();
            $empleado->construct_default($cem_id_empleado,$cem_nombre,$cem_appaterno,$cem_apmaterno);


            $cej_id_eje= $rs[strtoupper("cej_id_eje")];
            $cej_eje= $rs[strtoupper("cej_eje")];
            $cej_descripcion= $rs[strtoupper("cej_descripcion")];
            $eje = new Eje();
            $eje->setAll($cej_id_eje,$cej_eje,$cej_descripcion);


            $cla_id_lineaaccion= $rs[strtoupper("cla_id_lineaaccion")];
            $cla_lineaaccion= $rs[strtoupper("cla_lineaaccion")];
            $cla_descripcion= $rs[strtoupper("cla_descripcion")];
            $linea = new LineaAccion();
            $linea->setAll($cla_id_lineaaccion,$eje,$cla_lineaaccion,$cla_descripcion);

            $cpr_descripcion= $rs[strtoupper("cpr_descripcion")];
            $cpr_origpresup= $rs[strtoupper("cpr_origpresup")];
            $cpr_costo= $rs[strtoupper("cpr_costo")];
            $cpr_antecedentes= $rs[strtoupper("cpr_antecedentes")];
            $cpr_objetivogral= $rs[strtoupper("cpr_objetivogral")];
            $cpr_limites= $rs[strtoupper("cpr_limites")];
            $cpr_imp_soc= $rs[strtoupper("cpr_imp_soc")];
            $cpr_imp_organiacional= $rs[strtoupper("cpr_imp_organiacional")];
            $cpr_imp_estrategico= $rs[strtoupper("cpr_imp_estrategico")];
            $cpr_beneficiarios= $rs[strtoupper("cpr_beneficiarios")];
            $cpr_estatus= $rs[strtoupper("cpr_estatus")];
            $cpr_tipoproy= $rs[strtoupper("cpr_tipoproy")];
            $cpr_fecha_ini_total= $rs[strtoupper("cpr_fecha_ini_total")];
            $cpr_fecha_fin_total= $rs[strtoupper("cpr_fecha_fin_total")];
            $cpr_fecha_ini_ejer= $rs[strtoupper("cpr_fecha_ini_ejer")];
            $cpr_fecha_fin_ejer= $rs[strtoupper("cpr_fecha_fin_ejer")];

            $car_id_area= $rs["car_id_area"];
            $car_area= $rs[strtoupper("car_area")];
            $area = new Area();
            $area->setAll($car_id_area,$car_area);	

            $elemento = new CatProyecto();				
            $elemento->setAll($id,$arean,$ccostos,$cpr_descripcion,$cpr_origpresup,$empleado,$cpr_costo,$linea,$numero, $periodo,$cpr_antecedentes,$cpr_objetivogral,$cpr_limites,$cpr_imp_soc,$cpr_imp_organiacional,$cpr_imp_estrategico,$cpr_beneficiarios,$cpr_estatus,$cpr_tipoproy,$cpr_fecha_ini_total,$cpr_fecha_fin_total,$cpr_fecha_ini_ejer,$cpr_fecha_fin_ejer,$area);
            array_push($lista, $elemento);
                }	
        return $lista;
    }
    
    public function obtieneListadoAbiertoIdProy( $idPerfil , $idUsuario, $idProyecto ) {
		
	$lista= array();
	$query = "";
		
        if( $idPerfil == 7 ){
			
            if($this->periodoEstaAbierto((String)($idProyecto))==true){
				
                $query=strtolower("SELECT * FROM sie_usuario_x_meta um, sie_proyecto_metas pm, sie_cat_proyectos P, sie_cat_areanormativa A, sie_cat_centrocosto CC, cat_empleado E, sie_cat_linea_accion LA, sie_cat_ejes EJ,  cat_areas AR, SIE_CAT_PERIODOS PE ".
                " WHERE um.cus_id_usuario = ".$idUsuario." AND um.proyecto_id_meta = pm.proyecto_id_meta AND pm.cpr_id_proyecto = P.cpr_id_proyecto AND  P.can_id_areanormativa=A.can_id_areanormativa AND P.ccc_id_ccosto=CC.ccc_id_ccosto AND cpr_id_lider=E.cem_id_empleado AND P.cla_id_lineaaccion=LA.cla_id_lineaaccion AND " .
                " LA.cej_id_eje=EJ.cej_id_eje AND P.car_id_area=AR.car_id_area  AND P.cpe_id_periodo=PE.cpe_id_periodo AND  cpr_estatus='1' AND PE.cpe_cerrado=0 ORDER BY PE.CPE_PERIODO,P.cpr_descripcion");

            }
            else{

                $query=strtolower("SELECT * FROM sie_usuario_x_meta um, sie_proyecto_metas pm, sie_cat_proyectos P, sie_cat_areanormativa A, sie_cat_centrocosto CC, cat_empleado E, sie_cat_linea_accion LA, sie_cat_ejes EJ,  cat_areas AR, SIE_CAT_PERIODOS PE ".
                " WHERE um.cus_id_usuario = ".$idUsuario." AND um.proyecto_id_meta = pm.proyecto_id_meta AND pm.cpr_id_proyecto = P.cpr_id_proyecto AND  P.can_id_areanormativa=A.can_id_areanormativa AND P.ccc_id_ccosto=CC.ccc_id_ccosto AND cpr_id_lider=E.cem_id_empleado AND P.cla_id_lineaaccion=LA.cla_id_lineaaccion AND " .
                " LA.cej_id_eje=EJ.cej_id_eje AND P.car_id_area=AR.car_id_area  AND P.cpe_id_periodo=PE.cpe_id_periodo AND  cpr_estatus='1' AND ( PE.cpe_cerrado=0 OR P.CPR_ID_PROYECTO=".$idProyecto.") ORDER BY PE.CPE_PERIODO,P.cpr_descripcion");

            }			
			
	}else{
			
            if($this->periodoEstaAbierto((String)($idProyecto))==true){

                $query=strtolower("SELECT * FROM sie_cat_proyectos P, sie_cat_areanormativa A, sie_cat_centrocosto CC, cat_empleado E, sie_cat_linea_accion LA, sie_cat_ejes EJ,  cat_areas AR, SIE_CAT_PERIODOS PE ".
                " WHERE P.can_id_areanormativa=A.can_id_areanormativa AND P.ccc_id_ccosto=CC.ccc_id_ccosto AND cpr_id_lider=E.cem_id_empleado AND P.cla_id_lineaaccion=LA.cla_id_lineaaccion AND " .
                " LA.cej_id_eje=EJ.cej_id_eje AND P.car_id_area=AR.car_id_area  AND P.cpe_id_periodo=PE.cpe_id_periodo AND  cpr_estatus='1' AND PE.cpe_cerrado=0 ORDER BY PE.CPE_PERIODO,P.cpr_descripcion");	
            }
            else{

                $query=strtolower("SELECT * FROM sie_cat_proyectos P, sie_cat_areanormativa A, sie_cat_centrocosto CC, cat_empleado E, sie_cat_linea_accion LA, sie_cat_ejes EJ,  cat_areas AR, SIE_CAT_PERIODOS PE ".
                " WHERE P.can_id_areanormativa=A.can_id_areanormativa AND P.ccc_id_ccosto=CC.ccc_id_ccosto AND cpr_id_lider=E.cem_id_empleado AND P.cla_id_lineaaccion=LA.cla_id_lineaaccion AND " .
                " LA.cej_id_eje=EJ.cej_id_eje AND P.car_id_area=AR.car_id_area  AND P.cpe_id_periodo=PE.cpe_id_periodo AND  cpr_estatus='1' AND ( PE.cpe_cerrado=0 OR P.CPR_ID_PROYECTO=".$idProyecto.") ORDER BY PE.CPE_PERIODO,P.cpr_descripcion");

            }												
	}
        $catalogo = new Catalogo();

        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cpr_id_proyecto")];

            $can_id_areanormativa= $rs[strtoupper("can_id_areanormativa")];
            $numero= $rs[strtoupper("cpr_numeroproyecto")];

            $periodo = new Periodo();
            $periodo->setAll($rs[strtoupper("CPE_ID_PERIODO")],$rs[strtoupper("CPE_PERIODO")],$rs[strtoupper("CPE_CERRADO")],$rs[strtoupper("CPE_FECHA_INI")],$rs[strtoupper("CPE_FECHA_FIN")]); 

            $can_descripcion= $rs[strtoupper("can_descripcion")];
            $arean = new AreaNormativa();
            $arean->setAll($can_id_areanormativa,$can_descripcion);

            $ccc_id_ccosto= $rs[strtoupper("ccc_id_ccosto")];
            $ccc_clave_cc= $rs[strtoupper("ccc_clave_cc")];
            $ccc_descripcion= $rs[strtoupper("ccc_descripcion")];
            $ccostos = new CentroCostos();
            $ccostos->setAll($ccc_id_ccosto,$ccc_clave_cc,$ccc_descripcion);

            $cem_id_empleado= $rs["cem_id_empleado"];				
            $cem_nombre= $rs[strtoupper("cem_nombre")];
            $cem_appaterno= $rs[strtoupper("cem_appaterno")];
            $cem_apmaterno= $rs[strtoupper("cem_apmaterno")];
            $empleado = new Empleado();
            $empleado->construct_default($cem_id_empleado,$cem_nombre,$cem_appaterno,$cem_apmaterno);


            $cej_id_eje= $rs[strtoupper("cej_id_eje")];
            $cej_eje= $rs[strtoupper("cej_eje")];
            $cej_descripcion= $rs[strtoupper("cej_descripcion")];
            $eje = new Eje();
            $eje->setAll($cej_id_eje,$cej_eje,$cej_descripcion);


            $cla_id_lineaaccion= $rs[strtoupper("cla_id_lineaaccion")];
            $cla_lineaaccion= $rs[strtoupper("cla_lineaaccion")];
            $cla_descripcion= $rs[strtoupper("cla_descripcion")];
            $linea = new LineaAccion();
            $linea->setAll($cla_id_lineaaccion,$eje,$cla_lineaaccion,$cla_descripcion);

            $cpr_descripcion= $rs[strtoupper("cpr_descripcion")];
            $cpr_origpresup= $rs[strtoupper("cpr_origpresup")];
            $cpr_costo= $rs[strtoupper("cpr_costo")];
            $cpr_antecedentes= $rs[strtoupper("cpr_antecedentes")];
            $cpr_objetivogral= $rs[strtoupper("cpr_objetivogral")];
            $cpr_limites= $rs[strtoupper("cpr_limites")];
            $cpr_imp_soc= $rs[strtoupper("cpr_imp_soc")];
            $cpr_imp_organiacional= $rs[strtoupper("cpr_imp_organiacional")];
            $cpr_imp_estrategico= $rs[strtoupper("cpr_imp_estrategico")];
            $cpr_beneficiarios= $rs[strtoupper("cpr_beneficiarios")];
            $cpr_estatus= $rs[strtoupper("cpr_estatus")];
            $cpr_tipoproy= $rs[strtoupper("cpr_tipoproy")];
            $cpr_fecha_ini_total= $rs[strtoupper("cpr_fecha_ini_total")];
            $cpr_fecha_fin_total= $rs[strtoupper("cpr_fecha_fin_total")];
            $cpr_fecha_ini_ejer= $rs[strtoupper("cpr_fecha_ini_ejer")];
            $cpr_fecha_fin_ejer= $rs[strtoupper("cpr_fecha_fin_ejer")];

            $car_id_area= $rs["car_id_area"];
            $car_area= $rs[strtoupper("car_area")];
            $area = new Area();
            $area->setAll($car_id_area,$car_area);	

            $elemento = new CatProyecto();				
            $elemento->setAll($id,$arean,$ccostos,$cpr_descripcion,$cpr_origpresup,$empleado,$cpr_costo,$linea,$numero, $periodo,$cpr_antecedentes,$cpr_objetivogral,$cpr_limites,$cpr_imp_soc,$cpr_imp_organiacional,$cpr_imp_estrategico,$cpr_beneficiarios,$cpr_estatus,$cpr_tipoproy,$cpr_fecha_ini_total,$cpr_fecha_fin_total,$cpr_fecha_ini_ejer,$cpr_fecha_fin_ejer,$area);
            array_push($lista, $elemento);
                }	
        return $lista;
    }
    
    public function obtieneListado2( ) {

        $lista= array();

        $query=strtolower("SELECT * FROM sie_cat_proyectos P, sie_cat_areanormativa A, sie_cat_centrocosto CC, cat_empleado E, sie_cat_linea_accion LA, sie_cat_ejes EJ,  cat_areas AR, SIE_CAT_PERIODOS PE ".
                        " WHERE P.can_id_areanormativa=A.can_id_areanormativa AND P.ccc_id_ccosto=CC.ccc_id_ccosto AND cpr_id_lider=E.cem_id_empleado AND P.cla_id_lineaaccion=LA.cla_id_lineaaccion AND " .
                        " LA.cej_id_eje=EJ.cej_id_eje AND P.car_id_area=AR.car_id_area  AND P.cpe_id_periodo=PE.cpe_id_periodo AND  cpr_estatus='1' ORDER BY PE.CPE_PERIODO,P.cpr_descripcion");
        $catalogo = new Catalogo();

        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cpr_id_proyecto")];

            $can_id_areanormativa= $rs[strtoupper("can_id_areanormativa")];
            $numero= $rs[strtoupper("cpr_numeroproyecto")];

            $periodo = new Periodo();
            $periodo->setAll($rs[strtoupper("CPE_ID_PERIODO")],$rs[strtoupper("CPE_PERIODO")],$rs[strtoupper("CPE_CERRADO")],$rs[strtoupper("CPE_FECHA_INI")],$rs[strtoupper("CPE_FECHA_FIN")]); 

            $can_descripcion= $rs[strtoupper("can_descripcion")];
            $arean = new AreaNormativa();
            $arean->setAll($can_id_areanormativa,$can_descripcion);

            $ccc_id_ccosto= $rs[strtoupper("ccc_id_ccosto")];
            $ccc_clave_cc= $rs[strtoupper("ccc_clave_cc")];
            $ccc_descripcion= $rs[strtoupper("ccc_descripcion")];
            $ccostos = new CentroCostos();
            $ccostos->setAll($ccc_id_ccosto,$ccc_clave_cc,$ccc_descripcion);

            $cem_id_empleado= $rs["cem_id_empleado"];				
            $cem_nombre= $rs[strtoupper("cem_nombre")];
            $cem_appaterno= $rs[strtoupper("cem_appaterno")];
            $cem_apmaterno= $rs[strtoupper("cem_apmaterno")];
            $empleado = new Empleado();
            $empleado->construct_default($cem_id_empleado,$cem_nombre,$cem_appaterno,$cem_apmaterno);


            $cej_id_eje= $rs[strtoupper("cej_id_eje")];
            $cej_eje= $rs[strtoupper("cej_eje")];
            $cej_descripcion= $rs[strtoupper("cej_descripcion")];
            $eje = new Eje();
            $eje->setAll($cej_id_eje,$cej_eje,$cej_descripcion);


            $cla_id_lineaaccion= $rs[strtoupper("cla_id_lineaaccion")];
            $cla_lineaaccion= $rs[strtoupper("cla_lineaaccion")];
            $cla_descripcion= $rs[strtoupper("cla_descripcion")];
            $linea = new LineaAccion();
            $linea->setAll($cla_id_lineaaccion,$eje,$cla_lineaaccion,$cla_descripcion);

            $cpr_descripcion= $rs[strtoupper("cpr_descripcion")];
            $cpr_origpresup= $rs[strtoupper("cpr_origpresup")];
            $cpr_costo= $rs[strtoupper("cpr_costo")];
            $cpr_antecedentes= $rs[strtoupper("cpr_antecedentes")];
            $cpr_objetivogral= $rs[strtoupper("cpr_objetivogral")];
            $cpr_limites= $rs[strtoupper("cpr_limites")];
            $cpr_imp_soc= $rs[strtoupper("cpr_imp_soc")];
            $cpr_imp_organiacional= $rs[strtoupper("cpr_imp_organiacional")];
            $cpr_imp_estrategico= $rs[strtoupper("cpr_imp_estrategico")];
            $cpr_beneficiarios= $rs[strtoupper("cpr_beneficiarios")];
            $cpr_estatus= $rs[strtoupper("cpr_estatus")];
            $cpr_tipoproy= $rs[strtoupper("cpr_tipoproy")];
            $cpr_fecha_ini_total= $rs[strtoupper("cpr_fecha_ini_total")];
            $cpr_fecha_fin_total= $rs[strtoupper("cpr_fecha_fin_total")];
            $cpr_fecha_ini_ejer= $rs[strtoupper("cpr_fecha_ini_ejer")];
            $cpr_fecha_fin_ejer= $rs[strtoupper("cpr_fecha_fin_ejer")];

            $car_id_area= $rs["car_id_area"];
            $car_area= $rs[strtoupper("car_area")];
            $area = new Area();
            $area->setAll($car_id_area,$car_area);	

            $elemento = new CatProyecto();				
            $elemento->setAll($id,$arean,$ccostos,$cpr_descripcion,$cpr_origpresup,$empleado,$cpr_costo,$linea,$numero, $periodo,$cpr_antecedentes,$cpr_objetivogral,$cpr_limites,$cpr_imp_soc,$cpr_imp_organiacional,$cpr_imp_estrategico,$cpr_beneficiarios,$cpr_estatus,$cpr_tipoproy,$cpr_fecha_ini_total,$cpr_fecha_fin_total,$cpr_fecha_ini_ejer,$cpr_fecha_fin_ejer,$area);
            array_push($lista, $elemento);
                }	
        return $lista;
    }
    
    public function obtieneListadoAbierto2( ) {
		
	$lista=array();
		
	$query=strtolower("SELECT * FROM sie_cat_proyectos P, sie_cat_areanormativa A, sie_cat_centrocosto CC, cat_empleado E, sie_cat_linea_accion LA, sie_cat_ejes EJ,  cat_areas AR, SIE_CAT_PERIODOS PE ".
                " WHERE P.can_id_areanormativa=A.can_id_areanormativa AND P.ccc_id_ccosto=CC.ccc_id_ccosto AND cpr_id_lider=E.cem_id_empleado AND P.cla_id_lineaaccion=LA.cla_id_lineaaccion AND " .
                " LA.cej_id_eje=EJ.cej_id_eje AND P.car_id_area=AR.car_id_area  AND P.cpe_id_periodo=PE.cpe_id_periodo AND  cpr_estatus='1' AND PE.cpe_cerrado=0 ORDER BY PE.CPE_PERIODO,P.cpr_descripcion");
        $catalogo = new Catalogo();

        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cpr_id_proyecto")];

            $can_id_areanormativa= $rs[strtoupper("can_id_areanormativa")];
            $numero= $rs[strtoupper("cpr_numeroproyecto")];

            $periodo = new Periodo();
            $periodo->setAll($rs[strtoupper("CPE_ID_PERIODO")],$rs[strtoupper("CPE_PERIODO")],$rs[strtoupper("CPE_CERRADO")],$rs[strtoupper("CPE_FECHA_INI")],$rs[strtoupper("CPE_FECHA_FIN")]); 

            $can_descripcion= $rs[strtoupper("can_descripcion")];
            $arean = new AreaNormativa();
            $arean->setAll($can_id_areanormativa,$can_descripcion);

            $ccc_id_ccosto= $rs[strtoupper("ccc_id_ccosto")];
            $ccc_clave_cc= $rs[strtoupper("ccc_clave_cc")];
            $ccc_descripcion= $rs[strtoupper("ccc_descripcion")];
            $ccostos = new CentroCostos();
            $ccostos->setAll($ccc_id_ccosto,$ccc_clave_cc,$ccc_descripcion);

            $cem_id_empleado= $rs["cem_id_empleado"];				
            $cem_nombre= $rs[strtoupper("cem_nombre")];
            $cem_appaterno= $rs[strtoupper("cem_appaterno")];
            $cem_apmaterno= $rs[strtoupper("cem_apmaterno")];
            $empleado = new Empleado();
            $empleado->construct_default($cem_id_empleado,$cem_nombre,$cem_appaterno,$cem_apmaterno);


            $cej_id_eje= $rs[strtoupper("cej_id_eje")];
            $cej_eje= $rs[strtoupper("cej_eje")];
            $cej_descripcion= $rs[strtoupper("cej_descripcion")];
            $eje = new Eje();
            $eje->setAll($cej_id_eje,$cej_eje,$cej_descripcion);


            $cla_id_lineaaccion= $rs[strtoupper("cla_id_lineaaccion")];
            $cla_lineaaccion= $rs[strtoupper("cla_lineaaccion")];
            $cla_descripcion= $rs[strtoupper("cla_descripcion")];
            $linea = new LineaAccion();
            $linea->setAll($cla_id_lineaaccion,$eje,$cla_lineaaccion,$cla_descripcion);

            $cpr_descripcion= $rs[strtoupper("cpr_descripcion")];
            $cpr_origpresup= $rs[strtoupper("cpr_origpresup")];
            $cpr_costo= $rs[strtoupper("cpr_costo")];
            $cpr_antecedentes= $rs[strtoupper("cpr_antecedentes")];
            $cpr_objetivogral= $rs[strtoupper("cpr_objetivogral")];
            $cpr_limites= $rs[strtoupper("cpr_limites")];
            $cpr_imp_soc= $rs[strtoupper("cpr_imp_soc")];
            $cpr_imp_organiacional= $rs[strtoupper("cpr_imp_organiacional")];
            $cpr_imp_estrategico= $rs[strtoupper("cpr_imp_estrategico")];
            $cpr_beneficiarios= $rs[strtoupper("cpr_beneficiarios")];
            $cpr_estatus= $rs[strtoupper("cpr_estatus")];
            $cpr_tipoproy= $rs[strtoupper("cpr_tipoproy")];
            $cpr_fecha_ini_total= $rs[strtoupper("cpr_fecha_ini_total")];
            $cpr_fecha_fin_total= $rs[strtoupper("cpr_fecha_fin_total")];
            $cpr_fecha_ini_ejer= $rs[strtoupper("cpr_fecha_ini_ejer")];
            $cpr_fecha_fin_ejer= $rs[strtoupper("cpr_fecha_fin_ejer")];

            $car_id_area= $rs["car_id_area"];
            $car_area= $rs[strtoupper("car_area")];
            $area = new Area();
            $area->setAll($car_id_area,$car_area);	

            $elemento = new CatProyecto();				
            $elemento->setAll($id,$arean,$ccostos,$cpr_descripcion,$cpr_origpresup,$empleado,$cpr_costo,$linea,$numero, $periodo,$cpr_antecedentes,$cpr_objetivogral,$cpr_limites,$cpr_imp_soc,$cpr_imp_organiacional,$cpr_imp_estrategico,$cpr_beneficiarios,$cpr_estatus,$cpr_tipoproy,$cpr_fecha_ini_total,$cpr_fecha_fin_total,$cpr_fecha_ini_ejer,$cpr_fecha_fin_ejer,$area);
            array_push($lista, $elemento);
            }	
        return $lista;
    }
     
    public function obtieneListadoAbiertoIdProy2($idProyecto ) {
		
	$lista= array();
	$query=strtolower("");
        //verifico si el periodo del proyecto esta abierto, si es asi solo obtengo los proyectos de periodos abiertos,
        //de lo contrario hago el union con el id proyecto;
        if($this->periodoEstaAbierto((String)($idProyecto))==true){
            $query=strtolower("SELECT * FROM sie_cat_proyectos P, sie_cat_areanormativa A, sie_cat_centrocosto CC, cat_empleado E, sie_cat_linea_accion LA, sie_cat_ejes EJ,  cat_areas AR, SIE_CAT_PERIODOS PE ".
                    " WHERE P.can_id_areanormativa=A.can_id_areanormativa AND P.ccc_id_ccosto=CC.ccc_id_ccosto AND cpr_id_lider=E.cem_id_empleado AND P.cla_id_lineaaccion=LA.cla_id_lineaaccion AND " .
                    " LA.cej_id_eje=EJ.cej_id_eje AND P.car_id_area=AR.car_id_area  AND P.cpe_id_periodo=PE.cpe_id_periodo AND  cpr_estatus='1' AND PE.cpe_cerrado=0 ORDER BY PE.CPE_PERIODO,P.cpr_descripcion");

        }
        else{

            $query=strtolower("SELECT * FROM sie_cat_proyectos P, sie_cat_areanormativa A, sie_cat_centrocosto CC, cat_empleado E, sie_cat_linea_accion LA, sie_cat_ejes EJ,  cat_areas AR, SIE_CAT_PERIODOS PE ".
                " WHERE P.can_id_areanormativa=A.can_id_areanormativa AND P.ccc_id_ccosto=CC.ccc_id_ccosto AND cpr_id_lider=E.cem_id_empleado AND P.cla_id_lineaaccion=LA.cla_id_lineaaccion AND " .
                " LA.cej_id_eje=EJ.cej_id_eje AND P.car_id_area=AR.car_id_area  AND P.cpe_id_periodo=PE.cpe_id_periodo AND  cpr_estatus='1' AND ( PE.cpe_cerrado=0 OR P.CPR_ID_PROYECTO=".$idProyecto.") ".
                " ORDER BY PE.CPE_PERIODO,P.cpr_descripcion");
        }
        $catalogo = new Catalogo();

        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cpr_id_proyecto")];

            $can_id_areanormativa= $rs[strtoupper("can_id_areanormativa")];
            $numero= $rs[strtoupper("cpr_numeroproyecto")];

            $periodo = new Periodo();
            $periodo->setAll($rs[strtoupper("CPE_ID_PERIODO")],$rs[strtoupper("CPE_PERIODO")],$rs[strtoupper("CPE_CERRADO")],$rs[strtoupper("CPE_FECHA_INI")],$rs[strtoupper("CPE_FECHA_FIN")]); 

            $can_descripcion= $rs[strtoupper("can_descripcion")];
            $arean = new AreaNormativa();
            $arean->setAll($can_id_areanormativa,$can_descripcion);

            $ccc_id_ccosto= $rs[strtoupper("ccc_id_ccosto")];
            $ccc_clave_cc= $rs[strtoupper("ccc_clave_cc")];
            $ccc_descripcion= $rs[strtoupper("ccc_descripcion")];
            $ccostos = new CentroCostos();
            $ccostos->setAll($ccc_id_ccosto,$ccc_clave_cc,$ccc_descripcion);

            $cem_id_empleado= $rs["cem_id_empleado"];				
            $cem_nombre= $rs[strtoupper("cem_nombre")];
            $cem_appaterno= $rs[strtoupper("cem_appaterno")];
            $cem_apmaterno= $rs[strtoupper("cem_apmaterno")];
            $empleado = new Empleado();
            $empleado->construct_default($cem_id_empleado,$cem_nombre,$cem_appaterno,$cem_apmaterno);


            $cej_id_eje= $rs[strtoupper("cej_id_eje")];
            $cej_eje= $rs[strtoupper("cej_eje")];
            $cej_descripcion= $rs[strtoupper("cej_descripcion")];
            $eje = new Eje();
            $eje->setAll($cej_id_eje,$cej_eje,$cej_descripcion);


            $cla_id_lineaaccion= $rs[strtoupper("cla_id_lineaaccion")];
            $cla_lineaaccion= $rs[strtoupper("cla_lineaaccion")];
            $cla_descripcion= $rs[strtoupper("cla_descripcion")];
            $linea = new LineaAccion();
            $linea->setAll($cla_id_lineaaccion,$eje,$cla_lineaaccion,$cla_descripcion);

            $cpr_descripcion= $rs[strtoupper("cpr_descripcion")];
            $cpr_origpresup= $rs[strtoupper("cpr_origpresup")];
            $cpr_costo= $rs[strtoupper("cpr_costo")];
            $cpr_antecedentes= $rs[strtoupper("cpr_antecedentes")];
            $cpr_objetivogral= $rs[strtoupper("cpr_objetivogral")];
            $cpr_limites= $rs[strtoupper("cpr_limites")];
            $cpr_imp_soc= $rs[strtoupper("cpr_imp_soc")];
            $cpr_imp_organiacional= $rs[strtoupper("cpr_imp_organiacional")];
            $cpr_imp_estrategico= $rs[strtoupper("cpr_imp_estrategico")];
            $cpr_beneficiarios= $rs[strtoupper("cpr_beneficiarios")];
            $cpr_estatus= $rs[strtoupper("cpr_estatus")];
            $cpr_tipoproy= $rs[strtoupper("cpr_tipoproy")];
            $cpr_fecha_ini_total= $rs[strtoupper("cpr_fecha_ini_total")];
            $cpr_fecha_fin_total= $rs[strtoupper("cpr_fecha_fin_total")];
            $cpr_fecha_ini_ejer= $rs[strtoupper("cpr_fecha_ini_ejer")];
            $cpr_fecha_fin_ejer= $rs[strtoupper("cpr_fecha_fin_ejer")];

            $car_id_area= $rs["car_id_area"];
            $car_area= $rs[strtoupper("car_area")];
            $area = new Area();
            $area->setAll($car_id_area,$car_area);	

            $elemento = new CatProyecto();				
            $elemento->setAll($id,$arean,$ccostos,$cpr_descripcion,$cpr_origpresup,$empleado,$cpr_costo,$linea,$numero, $periodo,$cpr_antecedentes,$cpr_objetivogral,$cpr_limites,$cpr_imp_soc,$cpr_imp_organiacional,$cpr_imp_estrategico,$cpr_beneficiarios,$cpr_estatus,$cpr_tipoproy,$cpr_fecha_ini_total,$cpr_fecha_fin_total,$cpr_fecha_ini_ejer,$cpr_fecha_fin_ejer,$area);
            array_push($lista, $elemento);
            }	
        return $lista;
    }
    
    public function obtieneListadoByPeriodo($idPeriodo ){
		
        $lista= array();

        $query=strtolower("SELECT cpr_id_proyecto, cpr_numeroproyecto, cpr_descripcion FROM sie_cat_proyectos   WHERE  cpr_estatus='1' AND cpe_id_periodo = ".$idPeriodo." ORDER BY cpr_descripcion");

        $catalogo = new Catalogo();

        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $idProyecto = $rs["cpr_id_proyecto"];
            $numero = $rs["cpr_numeroproyecto"];
            $descripcion = $rs["cpr_descripcion"];

            $elemento = new CatProyecto();
            $elemento->catProy( $idProyecto, $descripcion, $numero );
            array_push($lista, $elemento);
        }

        return $lista;
    }
    
    public function obtieneListadoByAnio($anio ){
		
	$lista = array();
		
	$query=strtolower("SELECT * FROM sie_cat_proyectos P, sie_cat_areanormativa A, sie_cat_centrocosto CC, cat_empleado E, sie_cat_linea_accion LA, sie_cat_ejes EJ,  cat_areas AR, SIE_CAT_PERIODOS PE ".
                " WHERE P.can_id_areanormativa=A.can_id_areanormativa AND P.ccc_id_ccosto=CC.ccc_id_ccosto AND cpr_id_lider=E.cem_id_empleado AND P.cla_id_lineaaccion=LA.cla_id_lineaaccion AND " .
                " LA.cej_id_eje=EJ.cej_id_eje AND P.car_id_area=AR.car_id_area  AND P.cpe_id_periodo=PE.cpe_id_periodo AND  cpr_estatus='1' AND PE.cpe_periodo = ".$anio." ORDER BY PE.CPE_PERIODO,P.cpr_descripcion");
        $catalogo = new Catalogo();

        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cpr_id_proyecto")];

            $can_id_areanormativa= $rs[strtoupper("can_id_areanormativa")];
            $numero= $rs[strtoupper("cpr_numeroproyecto")];

            $periodo = new Periodo();
            $periodo->setAll($rs[strtoupper("CPE_ID_PERIODO")],$rs[strtoupper("CPE_PERIODO")],$rs[strtoupper("CPE_CERRADO")],$rs[strtoupper("CPE_FECHA_INI")],$rs[strtoupper("CPE_FECHA_FIN")]); 

            $can_descripcion= $rs[strtoupper("can_descripcion")];
            $arean = new AreaNormativa();
            $arean->setAll($can_id_areanormativa,$can_descripcion);

            $ccc_id_ccosto= $rs[strtoupper("ccc_id_ccosto")];
            $ccc_clave_cc= $rs[strtoupper("ccc_clave_cc")];
            $ccc_descripcion= $rs[strtoupper("ccc_descripcion")];
            $ccostos = new CentroCostos();
            $ccostos->setAll($ccc_id_ccosto,$ccc_clave_cc,$ccc_descripcion);

            $cem_id_empleado= $rs["cem_id_empleado"];				
            $cem_nombre= $rs[strtoupper("cem_nombre")];
            $cem_appaterno= $rs[strtoupper("cem_appaterno")];
            $cem_apmaterno= $rs[strtoupper("cem_apmaterno")];
            $empleado = new Empleado();
            $empleado->construct_default($cem_id_empleado,$cem_nombre,$cem_appaterno,$cem_apmaterno);


            $cej_id_eje= $rs[strtoupper("cej_id_eje")];
            $cej_eje= $rs[strtoupper("cej_eje")];
            $cej_descripcion= $rs[strtoupper("cej_descripcion")];
            $eje = new Eje();
            $eje->setAll($cej_id_eje,$cej_eje,$cej_descripcion);


            $cla_id_lineaaccion= $rs[strtoupper("cla_id_lineaaccion")];
            $cla_lineaaccion= $rs[strtoupper("cla_lineaaccion")];
            $cla_descripcion= $rs[strtoupper("cla_descripcion")];
            $linea = new LineaAccion();
            $linea->setAll($cla_id_lineaaccion,$eje,$cla_lineaaccion,$cla_descripcion);

            $cpr_descripcion= $rs[strtoupper("cpr_descripcion")];
            $cpr_origpresup= $rs[strtoupper("cpr_origpresup")];
            $cpr_costo= $rs[strtoupper("cpr_costo")];
            $cpr_antecedentes= $rs[strtoupper("cpr_antecedentes")];
            $cpr_objetivogral= $rs[strtoupper("cpr_objetivogral")];
            $cpr_limites= $rs[strtoupper("cpr_limites")];
            $cpr_imp_soc= $rs[strtoupper("cpr_imp_soc")];
            $cpr_imp_organiacional= $rs[strtoupper("cpr_imp_organiacional")];
            $cpr_imp_estrategico= $rs[strtoupper("cpr_imp_estrategico")];
            $cpr_beneficiarios= $rs[strtoupper("cpr_beneficiarios")];
            $cpr_estatus= $rs[strtoupper("cpr_estatus")];
            $cpr_tipoproy= $rs[strtoupper("cpr_tipoproy")];
            $cpr_fecha_ini_total= $rs[strtoupper("cpr_fecha_ini_total")];
            $cpr_fecha_fin_total= $rs[strtoupper("cpr_fecha_fin_total")];
            $cpr_fecha_ini_ejer= $rs[strtoupper("cpr_fecha_ini_ejer")];
            $cpr_fecha_fin_ejer= $rs[strtoupper("cpr_fecha_fin_ejer")];

            $car_id_area= $rs["car_id_area"];
            $car_area= $rs[strtoupper("car_area")];
            $area = new Area();
            $area->setAll($car_id_area,$car_area);	

            $elemento = new CatProyecto();				
            $elemento->setAll($id,$arean,$ccostos,$cpr_descripcion,$cpr_origpresup,$empleado,$cpr_costo,$linea,$numero, $periodo,$cpr_antecedentes,$cpr_objetivogral,$cpr_limites,$cpr_imp_soc,$cpr_imp_organiacional,$cpr_imp_estrategico,$cpr_beneficiarios,$cpr_estatus,$cpr_tipoproy,$cpr_fecha_ini_total,$cpr_fecha_fin_total,$cpr_fecha_ini_ejer,$cpr_fecha_fin_ejer,$area);
            array_push($lista, $elemento);
            }	
        return $lista;
    }
     
    public function obtieneElemento($idElemento) {
				
        if($idElemento == null){
            $idElemento = "null";
        }
        
	$elemento=new CatProyecto();
		
	$query=strtolower("SELECT * FROM sie_cat_proyectos P, sie_cat_areanormativa A, sie_cat_centrocosto CC, cat_empleado E, sie_cat_linea_accion LA, sie_cat_ejes EJ,  cat_areas AR, sie_cat_periodos PE  WHERE P.can_id_areanormativa=A.can_id_areanormativa AND P.ccc_id_ccosto=CC.ccc_id_ccosto AND cpr_id_lider=E.cem_id_empleado AND P.cla_id_lineaaccion=LA.cla_id_lineaaccion AND LA.cej_id_eje=EJ.cej_id_eje AND P.car_id_area=AR.car_id_area  AND P.cpe_id_periodo=PE.cpe_id_periodo AND P.cpr_id_proyecto=".$idElemento);
        $catalogo = new Catalogo();

        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cpr_id_proyecto")];

            $can_id_areanormativa= $rs[strtoupper("can_id_areanormativa")];
            $numero= $rs[strtoupper("cpr_numeroproyecto")];

            $periodo = new Periodo();
            $periodo->setAll($rs[strtoupper("CPE_ID_PERIODO")],$rs[strtoupper("CPE_PERIODO")],$rs[strtoupper("CPE_CERRADO")],$rs[strtoupper("CPE_FECHA_INI")],$rs[strtoupper("CPE_FECHA_FIN")]); 

            $can_descripcion= $rs[strtoupper("can_descripcion")];
            $arean = new AreaNormativa();
            $arean->setAll($can_id_areanormativa,$can_descripcion);

            $ccc_id_ccosto= $rs[strtoupper("ccc_id_ccosto")];
            $ccc_clave_cc= $rs[strtoupper("ccc_clave_cc")];
            $ccc_descripcion= $rs[strtoupper("ccc_descripcion")];
            $ccostos = new CentroCostos();
            $ccostos->setAll($ccc_id_ccosto,$ccc_clave_cc,$ccc_descripcion);

            $cem_id_empleado= $rs["cem_id_empleado"];				
            $cem_nombre= $rs[strtoupper("cem_nombre")];
            $cem_appaterno= $rs[strtoupper("cem_appaterno")];
            $cem_apmaterno= $rs[strtoupper("cem_apmaterno")];
            $empleado = new Empleado();
            $empleado->construct_default($cem_id_empleado,$cem_nombre,$cem_appaterno,$cem_apmaterno);


            $cej_id_eje= $rs[strtoupper("cej_id_eje")];
            $cej_eje= $rs[strtoupper("cej_eje")];
            $cej_descripcion= $rs[strtoupper("cej_descripcion")];
            $eje = new Eje();
            $eje->setAll($cej_id_eje,$cej_eje,$cej_descripcion);


            $cla_id_lineaaccion= $rs[strtoupper("cla_id_lineaaccion")];
            $cla_lineaaccion= $rs[strtoupper("cla_lineaaccion")];
            $cla_descripcion= $rs[strtoupper("cla_descripcion")];
            $linea = new LineaAccion();
            $linea->setAll($cla_id_lineaaccion,$eje,$cla_lineaaccion,$cla_descripcion);

            $cpr_descripcion= $rs[strtoupper("cpr_descripcion")];
            $cpr_origpresup= $rs[strtoupper("cpr_origpresup")];
            $cpr_costo= $rs[strtoupper("cpr_costo")];
            $cpr_antecedentes= $rs[strtoupper("cpr_antecedentes")];
            $cpr_objetivogral= $rs[strtoupper("cpr_objetivogral")];
            $cpr_limites= $rs[strtoupper("cpr_limites")];
            $cpr_imp_soc= $rs[strtoupper("cpr_imp_soc")];
            $cpr_imp_organiacional= $rs[strtoupper("cpr_imp_organiacional")];
            $cpr_imp_estrategico= $rs[strtoupper("cpr_imp_estrategico")];
            $cpr_beneficiarios= $rs[strtoupper("cpr_beneficiarios")];
            $cpr_estatus= $rs[strtoupper("cpr_estatus")];
            $cpr_tipoproy= $rs[strtoupper("cpr_tipoproy")];
            $cpr_fecha_ini_total= $rs[strtoupper("cpr_fecha_ini_total")];
            $cpr_fecha_fin_total= $rs[strtoupper("cpr_fecha_fin_total")];
            $cpr_fecha_ini_ejer= $rs[strtoupper("cpr_fecha_ini_ejer")];
            $cpr_fecha_fin_ejer= $rs[strtoupper("cpr_fecha_fin_ejer")];

            $car_id_area= $rs["car_id_area"];
            $car_area= $rs[strtoupper("car_area")];
            $area = new Area();
            $area->setAll($car_id_area,$car_area);	

            $elemento = new CatProyecto();				
            $elemento->setAll($id,$arean,$ccostos,$cpr_descripcion,$cpr_origpresup,$empleado,$cpr_costo,$linea,$numero, $periodo,$cpr_antecedentes,$cpr_objetivogral,$cpr_limites,$cpr_imp_soc,$cpr_imp_organiacional,$cpr_imp_estrategico,$cpr_beneficiarios,$cpr_estatus,$cpr_tipoproy,$cpr_fecha_ini_total,$cpr_fecha_fin_total,$cpr_fecha_ini_ejer,$cpr_fecha_fin_ejer,$area);
            
            }	
        return $elemento;
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $fechainiproy="";
        $fechafinproy="";
        $fechainieje="";
        $fechafineje="";
		
        if($elemento->getFechaIniProyecto()!=null)
        {	$fechainiproy= "'".date("Y-m-d",strtotime($elemento->getFechaIniProyecto()))."'";}
        else
        {	$fechainiproy = "null";}

        if($elemento->getFechaFinProyecto()!=null)
        {	$fechafinproy= "'".date("Y-m-d",strtotime($elemento->getFechaFinProyecto()))."'";}
        else
        {	$fechafinproy = "null";}

        if($elemento->getFechaIniEjercicio()!=null)
        {	$fechainieje= "'".date("Y-m-d",strtotime($elemento->getFechaIniEjercicio()))."'";}
        else
        {	$fechainieje = "null";}

        if($elemento->getFechaFinEjercicio()!=null)
        {	$fechafineje= "'".date("Y-m-d",strtotime($elemento->getFechaFinEjercicio()))."'"; }
        else
        {	$fechafineje = "null"; }

        $periodo = $elemento->getPeriodo();
        $arean = $elemento->getAreaNormativa();
        $ccostos = $elemento->getCentroCostos();
        $lider = $elemento->getLider();
        $lineaAccion = $elemento->getLineaAccion();
        $area = $elemento->getArea();
        
        $query=strtolower("INSERT INTO sie_cat_proyectos(cpr_numeroproyecto,cpe_id_periodo, can_id_areanormativa, ccc_id_ccosto, cpr_descripcion, cpr_origpresup, cpr_id_lider, cpr_costo, cla_id_lineaaccion, cpr_antecedentes, cpr_objetivogral,cpr_limites,cpr_imp_soc,cpr_imp_organiacional,cpr_imp_estrategico,cpr_beneficiarios,cpr_estatus, cpr_tipoproy, cpr_fecha_ini_total, cpr_fecha_fin_total, cpr_fecha_ini_ejer, cpr_fecha_fin_ejer, car_id_area )".
        " VALUES (".$elemento->getNumero().", ".$periodo->getId().", ".$arean->getId().", ".
        $ccostos->getId().", '".$elemento->getDescripcion()."', '".$elemento->getOrigen().
        "', ".$lider->getId()." , ".$elemento->getMonto()." , ".$lineaAccion->getId().
        ", '".$elemento->getAntecedentes()."', '".$elemento->getObjetivoGral()."' , '".$elemento->getLimites().
        "' , '".$elemento->getImpactoSocial()."' , '".$elemento->getImpactoOrganizacional().
        "' , '".$elemento->getImpactoEstrategico()."' , '".$elemento->getBeneficiarios()."' , '1', ".
        "'".$elemento->getTipoproy()."', ".$fechainiproy.", ".$fechafinproy.", ".$fechainieje.", ".$fechafineje.", ".$area->getId().")");


        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
		
    }
    
    public function actualizaElemento($elemento){
        
        $con=new Catalogo();
        $fechainiproy="";
        $fechafinproy="";
        $fechainieje="";
        $fechafineje="";
		
        if($elemento->getFechaIniProyecto()!=null)
        {       $fechainiproy= "'".date("Y-m-d",strtotime($elemento->getFechaIniProyecto()))."'";}
        else
        {	$fechainiproy = "null";}

        if($elemento->getFechaFinProyecto()!=null)
        {	$fechafinproy= "'".date("Y-m-d",strtotime($elemento->getFechaFinProyecto()))."'";}
        else
        {	$fechafinproy = "null";}

        if($elemento->getFechaIniEjercicio()!=null)
        {	$fechainieje= "'".date("Y-m-d",strtotime($elemento->getFechaIniEjercicio()))."'";}
        else
        {	$fechainieje = "null";}

        if($elemento->getFechaFinEjercicio()!=null)
        {	$fechafineje= "'".date("Y-m-d",strtotime($elemento->getFechaFinEjercicio()))."'"; }
        else
        {	$fechafineje = "null"; }
        
        $periodo = $elemento->getPeriodo();
        $arean = $elemento->getAreaNormativa();
        $ccostos = $elemento->getCentroCostos();
        $lider = $elemento->getLider();
        $lineaAccion = $elemento->getLineaAccion();
        $area = $elemento->getArea();
        
        $query=strtolower("UPDATE sie_cat_proyectos set cpr_numeroproyecto='".$elemento->getNumero()."', cpe_id_periodo='".$periodo->getId()."', can_id_areanormativa=".$arean->getId().", ccc_id_ccosto=".$ccostos->getId().", cpr_descripcion='".$elemento->getDescripcion()."' , cpr_origpresup='".$elemento->getOrigen()."' , cpr_id_lider=".$lider->getId()." , cpr_costo=".$elemento->getMonto()." , cla_id_lineaaccion=".$lineaAccion->getId().", cpr_antecedentes='".$elemento->getAntecedentes()."', cpr_objetivogral='".$elemento->getObjetivoGral()."', cpr_limites='".$elemento->getLimites()."', cpr_imp_soc='".$elemento->getImpactoSocial()."', cpr_imp_organiacional='".$elemento->getImpactoOrganizacional()."', cpr_imp_estrategico='".$elemento->getImpactoEstrategico()."', cpr_beneficiarios='".$elemento->getBeneficiarios()."',  cpr_tipoproy='".$elemento->getTipoproy()."', cpr_fecha_ini_total=".$fechainiproy.", cpr_fecha_fin_total=".$fechafinproy.", cpr_fecha_ini_ejer=".$fechainieje.", cpr_fecha_fin_ejer=".$fechafineje.", car_id_area=".$area->getId()."  WHERE cpr_id_proyecto=".$elemento->getId());
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query=strtolower("UPDATE sie_cat_proyectos set  cpr_estatus=0 WHERE cpr_id_proyecto=".$idElemento);
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}
