<?php

class PersonalProy {
    private $id;
    private $rfc;
    private $curp;
    private $nombre;
    private $app;
    private $apm;
    private $estatus;
    
    function setAll($id, $rfc, $curp, $nombre, $app, $apm, $estatus) {
        $this->id = $id;
        $this->rfc = $rfc;
        $this->curp = $curp;
        $this->nombre = $nombre;
        $this->app = $app;
        $this->apm = $apm;
        $this->estatus = $estatus;
    }

    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getRfc() {
        return $this->rfc;
    }

    public function getCurp() {
        return $this->curp;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function getApp() {
        return $this->app;
    }

    public function getApm() {
        return $this->apm;
    }

    public function getEstatus() {
        return $this->estatus;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setRfc($rfc) {
        $this->rfc = $rfc;
    }

    public function setCurp($curp) {
        $this->curp = $curp;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function setApp($app) {
        $this->app = $app;
    }

    public function setApm($apm) {
        $this->apm = $apm;
    }

    public function setEstatus($estatus) {
        $this->estatus = $estatus;
    }


}
