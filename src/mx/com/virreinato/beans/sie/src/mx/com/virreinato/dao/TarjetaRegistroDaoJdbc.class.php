<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/TarjetaRegistro.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class TarjetaRegistroDaoJdbc {

    public function obtieneListado($anio) {

        $lista = array();
        $query = "SELECT t.tre_id_tarjeta, t.tre_responsable, p.cpr_descripcion " .
                " FROM sie_cat_proyectos p, sie_tarjeta_registro t WHERE p.cpr_id_proyecto=t.cpr_id_proyecto AND" .
                " t.tre_fechaautorizacion BETWEEN '" . $anio . "-01-01' AND '" . $anio . "-12-31' AND t.tre_estatus = 1 ORDER BY p.cpr_id_proyecto ";

        $catalogo = new Catalogo();

        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)) {

            $idTarjeta = $rs["tre_id_tarjeta"];
            $Responsable = $rs["tre_responsable"];
            $proyecto = $rs["cpr_descripcion"];
            $meta = "";

            $elemento = new TarjetaRegistro();
            $elemento->constructor1($idTarjeta, $Responsable, $proyecto, $meta);
            array_push($lista, $elemento);
        }
        return $lista;
    }

    public function Imprimir($idProyecto) {

        $elemento = null;
        $query = "SELECT a.can_descripcion , l.cla_descripcion, p.cpr_numeroproyecto, p.cpr_costo, p.cpr_fecha_ini_total, p.cpr_fecha_fin_total, p.cpr_fecha_ini_ejer, p.cpr_fecha_fin_ejer FROM sie_cat_areanormativa a, sie_cat_proyectos p, sie_cat_linea_accion l WHERE p.can_id_areanormativa = a.can_id_areanormativa AND p.cla_id_lineaaccion = l.cla_id_lineaaccion AND p.cpr_id_proyecto =  " . $idProyecto;

        $catalogo = new Catalogo();

        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)) {
            $area = $rs["can_descripcion"];
            $lineaAccion = $rs["cla_descripcion"];
            $numeroProyecto = $rs["cpr_numeroproyecto"];
            $costo = $rs["cpr_costo"];
            $fechaInicio = $rs["cpr_fecha_ini_total"];
            $fechaFin = $rs["cpr_fecha_fin_total"];
            $Inicio = $rs["cpr_fecha_ini_ejer"];
            $Fin = $rs["cpr_fecha_fin_ejer"];

            $elemento = new TarjetaRegistro();
            $elemento->constructor2($area, $lineaAccion, $numeroProyecto, $costo, $fechaInicio, $fechaFin, $Inicio, $Fin);
        }

        return $elemento;
    }

    public function obtieneElemento($idElemento) {

        $elemento = new TarjetaRegistro();
        $query = "SELECT t.*, t.cpr_id_proyecto FROM sie_tarjeta_registro t WHERE  t.tre_estatus = 1 AND t.tre_id_tarjeta = " . $idElemento;

        $catalogo = new Catalogo();

        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)) {

            $idTarjeta = $rs[strtoupper("tre_id_tarjeta")];
            $idProyecto = $rs[strtoupper("cpr_id_proyecto")];
            $idMeta = 1;
            $unidad = $rs[strtoupper("tre_unidadadministrativa")];
            $responsble = $rs[strtoupper("tre_responsable")];
            $clave = $rs[strtoupper("tre_clavesecretecnica")];
            $claveAccion = $rs[strtoupper("tre_clavelineaaccion")];
            $financiamiento = $rs[strtoupper("tre_autorizadosinfin")];
            $fechaElaboracion = $rs[strtoupper("tre_fechaelaboracion")];
            $fechaAutorizacion = $rs[strtoupper("tre_fechaautorizacion")];
            $funcion = $rs[strtoupper("tre_funcion")];
            $subFuncion = $rs[strtoupper("tre_subfuncion")];
            $programaGeneral = $rs[strtoupper("tre_programageneral")];
            $institucional = $rs[strtoupper("tre_actividadinstitucional")];
            $prioritaria = $rs[strtoupper("tre_actividadprioritaria")];
            $tipo = $rs[strtoupper("tre_tipoproyecto")];
            $concluir = $rs[strtoupper("tre_programadoaconcluir")];
            $reactivado = $rs[strtoupper("tre_reactivado")];
            $producto = $rs[strtoupper("tre_productoprincipal")];
            $acumulado = $rs[strtoupper("tre_acumuladoanterior")];
            $programado = $rs[strtoupper("tre_programadoparax")];
            $terceros = $rs[strtoupper("tre_encasorecursosaportados")];
            $objetivo = $rs[strtoupper("tre_objetivoproyecto")];
            $del1 = $rs[strtoupper("tre_f1delanio")];
            $del2 = $rs[strtoupper("tre_f2delanio")];
            $del3 = $rs[strtoupper("tre_f3delanio")];
            $del4 = $rs[strtoupper("tre_f4delanio")];
            $del5 = $rs[strtoupper("tre_f5delanio")];
            $del6 = $rs[strtoupper("tre_f6delanio")];
            $al1 = $rs[strtoupper("tre_f1alanio")];
            $al2 = $rs[strtoupper("tre_f2alanio")];
            $al3 = $rs[strtoupper("tre_f3alanio")];
            $al4 = $rs[strtoupper("tre_f4alanio")];
            $al5 = $rs[strtoupper("tre_f5alanio")];
            $al6 = $rs[strtoupper("tre_f6alanio")];
            $Total_mod = $rs[strtoupper("tre_costototalmodifica")];
            $prod_mod = $rs[strtoupper("tre_productoprincipalmodifica")];
            $linea_mod = $rs[strtoupper("tre_lineaaccionmodifica")];
            $nombre_mod = $rs[strtoupper("tre_nombreproymodifica")];
            $permanente_mod = $rs[strtoupper("tre_apermanentemodifica")];
            $concluir_mod = $rs[strtoupper("tre_programadoconcluirmodifica")];
            $anio_mod = $rs[strtoupper("tre_aniodemodifica")];
            $al_mod = $rs[strtoupper("tre_almodifica")];
            $justificacion = $rs[strtoupper("tre_justificacionpresupporcap")];
            $elabora = $rs[strtoupper("tre_elabora")];
            $propone = $rs[strtoupper("tre_propone")];
            $autorizaT = $rs[strtoupper("tre_autorizast")];
            $autorizaA = $rs[strtoupper("tre_autorizasa")];

            $elemento->constructor3($idTarjeta, $responsble, $unidad, 0, $idProyecto, $clave, $claveAccion, $financiamiento, $funcion, $subFuncion, $programaGeneral, $institucional, $prioritaria, $tipo, $concluir, $reactivado, $objetivo, $producto, $acumulado, $programado, $terceros, $del1, $del2, $del3, $del4, $del5, $del6, $al1, $al2, $al3, $al4, $al5, $al6, $Total_mod, $prod_mod, $linea_mod, $nombre_mod, $permanente_mod, $concluir_mod, $anio_mod, $al_mod, $fechaElaboracion, $fechaAutorizacion, $elabora, $propone, $autorizaT, $autorizaA, $justificacion);
        }


        return $elemento;
    }

    public function guardaElemento($elemento) {

        $fechaElaboracion = "";
        $Autorizacion = "";

        if ($elemento->getFechaElaboracion() != null) {
            $fechaElaboracion = "'" . date("Y-m-d", strtotime($elemento->getFechaElaboracion())) . "'";
        } else {
            $fechaElaboracion = "null";
        }

        if ($elemento->getFechaAutorizacion() != null) {
            $Autorizacion = "'" . date("Y-m-d", strtotime($elemento->getFechaAutorizacion())) . "'";
        } else {
            $Autorizacion = "null";
        }
        
        if(($elemento->getDel_Fase1() == null) || $elemento->getDel_Fase1() == ""){
            $elemento->setDel_Fase1("null");
        }
        
        if(($elemento->getAl_Fase1()  == null) || $elemento->getAl_Fase1() == ""){
            $elemento->setAl_Fase1("null");
        }                
        
        if(($elemento->getDel_Fase2()  == null) || $elemento->getDel_Fase2() == ""){
            $elemento->setDel_Fase2("null");
        }
        
        if(($elemento->getAl_Fase2() == null) || $elemento->getAl_Fase2() == ""){
            $elemento->setAl_Fase2("null");
        }
        
        if(($elemento->getDel_Fase3() == null) || $elemento->getDel_Fase3() == ""){
            $elemento->setDel_Fase3("null");
        }
        
        if(($elemento->getAl_Fase3() == null) || $elemento->getAl_Fase3() == ""){
            $elemento->setAl_Fase3("null");
        }
        
        if(($elemento->getDel_Fase4() == null) || $elemento->getDel_Fase4() == ""){
            $elemento->setDel_Fase4("null");
        }
        
        if(($elemento->getAl_Fase4() == null) || $elemento->getAl_Fase4() == ""){
            $elemento->setAl_Fase4("null");
        }
        
        if(($elemento->getDel_Fase5() == null) || $elemento->getDel_Fase5() == ""){
            $elemento->setDel_Fase5("null");
        }
        
        if(($elemento->getAl_Fase5() == null) || $elemento->getAl_Fase5() == ""){
            $elemento->setAl_Fase5("null");
        }
        
        if(($elemento->getDel_Fase6() == null) || $elemento->getDel_Fase6() == ""){
            $elemento->setDel_Fase6("null");
        }
        
        if(($elemento->getAl_Fase6() == null) || $elemento->getAl_Fase6() == ""){
            $elemento->setAl_Fase6("null");
        }

        if(($elemento->getCostoTotal_Modificado() == null) || $elemento->getCostoTotal_Modificado() == ""){
            $elemento->setCostoTotal_Modificado("null");
        }
        
        $con = new Catalogo();
        $query = "INSERT INTO sie_tarjeta_registro(CPR_ID_PROYECTO,TRE_FECHAELABORACION,TRE_FECHAAUTORIZACION, " .
                "TRE_UNIDADADMINISTRATIVA, TRE_RESPONSABLE, TRE_CLAVELINEAACCION, TRE_CLAVESECRETECNICA,TRE_AUTORIZADOSINFIN,TRE_FUNCION, " .
                "TRE_SUBFUNCION, TRE_PROGRAMAGENERAL, TRE_ACTIVIDADINSTITUCIONAL, TRE_ACTIVIDADPRIORITARIA, TRE_TIPOPROYECTO, " .
                "TRE_PROGRAMADOACONCLUIR, TRE_REACTIVADO, TRE_PRODUCTOPRINCIPAL, TRE_ACUMULADOANTERIOR, TRE_PROGRAMADOPARAX, " .
                "TRE_ENCASORECURSOSAPORTADOS, TRE_OBJETIVOPROYECTO, TRE_F1DELANIO, TRE_F1ALANIO, TRE_F2DELANIO, TRE_F2ALANIO, " .
                "TRE_F3DELANIO, TRE_F3ALANIO, TRE_F4DELANIO, TRE_F4ALANIO, TRE_F5DELANIO, TRE_F5ALANIO, TRE_F6DELANIO, " .
                "TRE_F6ALANIO, TRE_COSTOTOTALMODIFICA, TRE_PRODUCTOPRINCIPALMODIFICA, TRE_LINEAACCIONMODIFICA, " .
                "TRE_NOMBREPROYMODIFICA, TRE_APERMANENTEMODIFICA, TRE_PROGRAMADOCONCLUIRMODIFICA, TRE_ANIODEMODIFICA, TRE_ALMODIFICA, " .
                "TRE_JUSTIFICACIONPRESUPPORCAP, TRE_ELABORA, TRE_PROPONE, TRE_AUTORIZAST, TRE_AUTORIZASA, TRE_ESTATUS) " .
                "VALUES ('" . $elemento->getIdProyecto() . "', " . $fechaElaboracion . ", " . $Autorizacion . ",'" .
                $elemento->getUnidadAdministrativa() . "' , '" . $elemento->getResponsable() . "','" . $elemento->getClaveAccion() . "','" . $elemento->getClaveTecnica() . "', " .
                " '" . $elemento->getAutorizadoSinFinanciamiento() . "', '" . $elemento->getFuncion() . "' , '" . $elemento->getSubFuncion() . "', '" . $elemento->getProgramaGeneral() . "' ," .
                " '" . $elemento->getActividadInstitucional() . "', '" . $elemento->getActividadPrioritaria() . "', '" . $elemento->getTipoProyecto() . "', '" . $elemento->getConcluir() . "', '" . $elemento->getReactivado() . "'," .
                " '" . $elemento->getProductoPrincipal() . "', '" . $elemento->getAcumulado() . "', '" . $elemento->getProgramado() . "'," .
                " '" . $elemento->getRecursos_Terceros() . "','" . $elemento->getObjetivo() . "'," . $elemento->getDel_Fase1() . ", " . $elemento->getAl_Fase1() . ", " . $elemento->getDel_Fase2() . ", " . $elemento->getAl_Fase2() . ", " . $elemento->getDel_Fase3() . "," .
                " " . $elemento->getAl_Fase3() . "," . $elemento->getDel_Fase4() . ", " . $elemento->getAl_Fase4() . ", " . $elemento->getDel_Fase5() . "," . $elemento->getAl_Fase5() . ", " . $elemento->getDel_Fase6() . ", " . $elemento->getAl_Fase6() . "," .
                " " . $elemento->getCostoTotal_Modificado() . ", '" . $elemento->getProductoPrincipal_Modificado() . "','" . $elemento->getLineaAccion_Modificado() . "', '" . $elemento->getNombreProyecto_Modificado() . "'," .
                " '" . $elemento->getPermanente_Modificado() . "', '" . $elemento->getConcluir_Modificado() . "', '" . $elemento->getAnio_Modificacion() . "', '" . $elemento->getAl_Modificacion() . "','" . $elemento->getJustificacion() . "','" . $elemento->getElabora() . "'," .
                " '" . $elemento->getPropone() . "', '" . $elemento->getAutoriza_Tecnico() . "', '" . $elemento->getAutoriza_Adminstrativo() . "',1)";
        
        $res = $con->obtenerLista($query);

        if ($res == "1") {
            return true;
        } else {
            return false;
        }
    }

    public function actualizaElemento($elemento) {
        $con = new Catalogo();
        $fechaElaboracion = "";
        $Autorizacion = "";

        if ($elemento->getFechaElaboracion() != null) {
            $fechaElaboracion = "'" . date("Y-m-d", strtotime($elemento->getFechaElaboracion())) . "'";
        } else {
            $fechaElaboracion = "null";
        }

        if ($elemento->getFechaAutorizacion() != null) {
            $Autorizacion = "'" . date("Y-m-d", strtotime($elemento->getFechaAutorizacion())) . "'";
        } else {
            $Autorizacion = "null";
        }
        
        if(($elemento->getDel_Fase1() == null) || $elemento->getDel_Fase1() == ""){
            $elemento->setDel_Fase1("null");
        }
        
        if(($elemento->getAl_Fase1()  == null) || $elemento->getAl_Fase1() == ""){
            $elemento->setAl_Fase1("null");
        }                
        
        if(($elemento->getDel_Fase2()  == null) || $elemento->getDel_Fase2() == ""){
            $elemento->setDel_Fase2("null");
        }
        
        if(($elemento->getAl_Fase2() == null) || $elemento->getAl_Fase2() == ""){
            $elemento->setAl_Fase2("null");
        }
        
        if(($elemento->getDel_Fase3() == null) || $elemento->getDel_Fase3() == ""){
            $elemento->setDel_Fase3("null");
        }
        
        if(($elemento->getAl_Fase3() == null) || $elemento->getAl_Fase3() == ""){
            $elemento->setAl_Fase3("null");
        }
        
        if(($elemento->getDel_Fase4() == null) || $elemento->getDel_Fase4() == ""){
            $elemento->setDel_Fase4("null");
        }
        
        if(($elemento->getAl_Fase4() == null) || $elemento->getAl_Fase4() == ""){
            $elemento->setAl_Fase4("null");
        }
        
        if(($elemento->getDel_Fase5() == null) || $elemento->getDel_Fase5() == ""){
            $elemento->setDel_Fase5("null");
        }
        
        if(($elemento->getAl_Fase5() == null) || $elemento->getAl_Fase5() == ""){
            $elemento->setAl_Fase5("null");
        }
        
        if(($elemento->getDel_Fase6() == null) || $elemento->getDel_Fase6() == ""){
            $elemento->setDel_Fase6("null");
        }
        
        if(($elemento->getAl_Fase6() == null) || $elemento->getAl_Fase6() == ""){
            $elemento->setAl_Fase6("null");
        }

        if(($elemento->getCostoTotal_Modificado() == null) || $elemento->getCostoTotal_Modificado() == ""){
            $elemento->setCostoTotal_Modificado("null");
        }

        $query = "UPDATE sie_tarjeta_registro  SET CPR_ID_PROYECTO='" . $elemento->getIdProyecto() . "',  tre_fechaelaboracion = " . $fechaElaboracion . ", tre_fechaautorizacion = " . $Autorizacion . ", tre_unidadadministrativa = '" . $elemento->getUnidadAdministrativa() . "', tre_responsable = '" . $elemento->getResponsable() . "', tre_clavelineaaccion = '" . $elemento->getClaveAccion() . "',  tre_clavesecretecnica='" . $elemento->getClaveTecnica() . "', tre_autorizadosinfin = '" . $elemento->getAutorizadoSinFinanciamiento() . "'" .
                ",tre_funcion = '" . $elemento->getFuncion() . "', tre_subfuncion = '" . $elemento->getSubFuncion() . "', tre_programageneral = '" . $elemento->getProgramaGeneral() . "' , tre_actividadinstitucional = '" . $elemento->getActividadInstitucional() . "', tre_actividadprioritaria = '" . $elemento->getActividadPrioritaria() . "', tre_tipoproyecto = '" . $elemento->getTipoProyecto() . "', tre_programadoaconcluir = '" . $elemento->getConcluir() . "' , tre_reactivado = '" . $elemento->getReactivado() . "'" .
                ",tre_productoprincipal = '" . $elemento->getProductoPrincipal() . "' , tre_acumuladoanterior = '" . $elemento->getAcumulado() . "' , tre_programadoparaX = '" . $elemento->getProgramado() . "' , tre_encasorecursosaportados = '" . $elemento->getRecursos_Terceros() . "', tre_objetivoproyecto='" . $elemento->getObjetivo() . "', tre_f1delanio = " . $elemento->getDel_Fase1() . " ,tre_f2delanio = " . $elemento->getDel_Fase2() . " , tre_f3delanio = " . $elemento->getDel_Fase3() . " , tre_f4delanio = " . $elemento->getDel_Fase4() . "" .
                " , tre_f5delanio = " . $elemento->getDel_Fase5() . " , tre_f6delanio = " . $elemento->getDel_Fase6() . ", tre_f1alanio = " . $elemento->getAl_Fase1() . " , tre_f2alanio = " . $elemento->getAl_Fase2() . " , tre_f3alanio = " . $elemento->getAl_Fase3() . " , tre_f4alanio = " . $elemento->getAl_Fase4() . " , tre_f5alanio = " . $elemento->getAl_Fase5() . " , tre_f6alanio = " . $elemento->getAl_Fase6() . " , tre_costototalmodifica = " . $elemento->getCostoTotal_Modificado() . " , tre_productoprincipalmodifica = '" . $elemento->getProductoPrincipal_Modificado() . "'" .
                " , tre_lineaaccionmodifica = '" . $elemento->getLineaAccion_Modificado() . "', tre_nombreproymodifica = '" . $elemento->getNombreProyecto_Modificado() . "' , tre_apermanentemodifica = '" . $elemento->getPermanente_Modificado() . "' , tre_programadoconcluirmodifica = '" . $elemento->getConcluir_Modificado() . "' , tre_aniodemodifica = '" . $elemento->getAnio_Modificacion() . "', tre_almodifica = '" . $elemento->getAl_Modificacion() . "' , tre_elabora = '" . $elemento->getElabora() . "' , tre_propone = '" . $elemento->getPropone() . "'" .
                " , tre_autorizast = '" . $elemento->getAutoriza_Tecnico() . "' , tre_autorizasa = '" . $elemento->getAutoriza_Adminstrativo() . "' , tre_justificacionpresupporcap = '" . $elemento->getJustificacion() . "'   WHERE tre_id_tarjeta = " . $elemento->getId() . " ";
        $res = $con->obtenerLista($query);

        if ($res == "1") {
            return true;
        } else {
            return false;
        }
    }

    public function eliminaElemento($idElemento) {

        $con = new Catalogo();
        $query = "UPDATE sie_tarjeta_registro  set  tre_estatus = 0 WHERE tre_id_tarjeta = " . $idElemento;
        $res = $con->obtenerLista($query);

        if ($res == "1") {
            return true;
        } else {
            return false;
        }
    }

}
