<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Persona.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class PersonaDaoJdbc {
    
    public function obtieneListado() {
		
        $lista= array();

        $query="SELECT * FROM cat_personas WHERE CPE_ESTATUS=1 ORDER BY cpe_nombre,cpe_appaterno,cpe_apmaterno";
		
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs["cpe_id_persona"];
            $nombre= $rs[strtoupper("cpe_nombre")];
            $apaterno= $rs[strtoupper("cpe_appaterno")];
            $amaterno= $rs[strtoupper("cpe_apmaterno")];
            $correo= $rs[strtoupper("cpe_correo")];
            $comentario= $rs[strtoupper("cpe_comentario")];

            $elemento = new Persona();
            $elemento->setAll($id,$nombre,$apaterno,$amaterno,$correo,$comentario);
            array_push($lista, $elemento);
        }	
			//iterar el resultado y llenar el objeto, construir la lista
	return $lista;
    }
    
    public function obtieneListadoSinUsuario() {
		
        $lista= array();

        $query=strtolower("SELECT * FROM cat_personas WHERE CPE_ESTATUS=1 and cpe_id_persona not in (select cpe_id_persona from SIE_CAT_USUARIOS WHERE CUS_ESTATUS=1 )  ORDER BY cpe_nombre,cpe_appaterno,cpe_apmaterno");
		
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs["cpe_id_persona"];
            $nombre= $rs[strtoupper("cpe_nombre")];
            $apaterno= $rs[strtoupper("cpe_appaterno")];
            $amaterno= $rs[strtoupper("cpe_apmaterno")];
            $correo= $rs[strtoupper("cpe_correo")];
            $comentario= $rs[strtoupper("cpe_comentario")];

            $elemento = new Persona();
            $elemento->setAll($id,$nombre,$apaterno,$amaterno,$correo,$comentario);
            array_push($lista, $elemento);
        }	
			//iterar el resultado y llenar el objeto, construir la lista
	return $lista;
    }
    
    public function obtieneElemento($idElemento){
        
        $elemento=new Persona();
        $query = "SELECT * FROM cat_personas WHERE cpe_id_persona=".$idElemento;
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs["cpe_id_persona"];
            $nombre= $rs[strtoupper("cpe_nombre")];
            $apaterno= $rs[strtoupper("cpe_appaterno")];
            $amaterno= $rs[strtoupper("cpe_apmaterno")];
            $correo= $rs[strtoupper("cpe_correo")];
            $comentario= $rs[strtoupper("cpe_comentario")];

            $elemento = new Persona();
            $elemento->setAll($id,$nombre,$apaterno,$amaterno,$correo,$comentario);
        }	
			//iterar el resultado y llenar el objeto, construir la lista
	return $elemento;
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $query="INSERT INTO cat_personas(cpe_nombre,cpe_appaterno,cpe_apmaterno,cpe_correo,cpe_comentario) VALUES ( '".$elemento->getNombre()."', '".$elemento->getApPaterno()."', '".$elemento->getApMaterno()."', '".$elemento->getCorreo()."', '".$elemento->getComentario()."')";
        $res = $con->obtenerLista($query);
		
        if($res == "1"){return true;}
        else{ return false; }		
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
        $query="UPDATE cat_personas set  cpe_nombre='".$elemento->getNombre()."', cpe_appaterno='".$elemento->getApPaterno()."', cpe_apmaterno='".$elemento->getApMaterno()."' , cpe_correo='".$elemento->getCorreo()."' , cpe_comentario='".$elemento->getComentario()."' WHERE cpe_id_persona=".$elemento->getId();
        $res = $con->obtenerLista($query);
		
        if($res == "1"){return true;}
        else{ return false; }
		
	}
        
    public function eliminaElemento($idElemento){
		
	$catalogo=new Catalogo();
	$query="UPDATE cat_personas set  cpe_estatus=0 WHERE cpe_id_persona=".$idElemento;
        $res = $catalogo->obtenerLista($query);
		
        if($res == "1"){return true;}
        else{ return false; }
		
    }
}
