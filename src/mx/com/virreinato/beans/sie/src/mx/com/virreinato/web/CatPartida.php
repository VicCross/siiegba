<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/CapituloDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Capitulo.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/PartidaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Partida.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$respuesta = "";
if (isset($_POST['guardar'])){
    
    $elemento=new Partida();			
			
    $cdao=new CapituloDaoJdbc();
    $capitulo=$cdao->obtieneElemento($_POST["id_capitulo"]);
			
    $elemento->setCapitulo($capitulo);
    $elemento->setPartida($_POST['partida']);
	$elemento->setGenerica($_POST['generica']);
	$elemento->setEspecifica($_POST['especifica']);
    $elemento->setDescripcion($_POST['descripcion']);
    
    if ($_POST["id"] != null){
        $elemento->setId((int)($_POST["id"]));
        $dao=new PartidaDaoJdbc();
        $res=$dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new PartidaDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
}else if(isset($_GET['id'])){
    $dao=new PartidaDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));
    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
}else{
    $respuesta = "No se detecto la acción a realizar.";
}
header("Location: ../../../../../catalogos/lista_partida.php?respuesta=" . $respuesta);
?>
