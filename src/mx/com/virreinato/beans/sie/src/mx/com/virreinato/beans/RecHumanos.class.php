<?php

class RecHumanos {
    
    private $id;
    private $idCarta;
    private $num_orden;
    private $rec_humano;
    private $nomina;
    private $honorarios;
    private $factura;
    private $partida;
    private $fechaEjercicio;
    private $Fiscal;
    private $cheque;
    private $retencion;
    
    function setAll($id, $idCarta, $num_orden, $rec_humano, $nomina, $honorarios, $factura, $partida, $fechaEjercicio, $Fiscal, $cheque, $retencion) {
        $this->id = $id;
        $this->idCarta = $idCarta;
        $this->num_orden = $num_orden;
        $this->rec_humano = $rec_humano;
        $this->nomina = $nomina;
        $this->honorarios = $honorarios;
        $this->factura = $factura;
        $this->partida = $partida;
        $this->fechaEjercicio = $fechaEjercicio;
        $this->Fiscal = $Fiscal;
        $this->cheque = $cheque;
        $this->retencion = $retencion;
    }

    function constructor2($id, $idCarta, $num_orden, $rec_humano) {
        $this->id = $id;
        $this->idCarta = $idCarta;
        $this->num_orden = $num_orden;
        $this->rec_humano = $rec_humano;
    }
    
    function __construct() {
        
    }
    
    public function getId() {
        return $this->id;
    }

    public function getIdCarta() {
        return $this->idCarta;
    }

    public function getNum_orden() {
        return $this->num_orden;
    }

    public function getRec_humano() {
        return $this->rec_humano;
    }

    public function getNomina() {
        return $this->nomina;
    }

    public function getHonorarios() {
        return $this->honorarios;
    }

    public function getFactura() {
        return $this->factura;
    }

    public function getPartida() {
        return $this->partida;
    }

    public function getFechaEjercicio() {
        return $this->fechaEjercicio;
    }

    public function getFiscal() {
        return $this->Fiscal;
    }

    public function getCheque() {
        return $this->cheque;
    }

    public function getRetencion() {
        return $this->retencion;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setIdCarta($idCarta) {
        $this->idCarta = $idCarta;
    }

    public function setNum_orden($num_orden) {
        $this->num_orden = $num_orden;
    }

    public function setRec_humano($rec_humano) {
        $this->rec_humano = $rec_humano;
    }

    public function setNomina($nomina) {
        $this->nomina = $nomina;
    }

    public function setHonorarios($honorarios) {
        $this->honorarios = $honorarios;
    }

    public function setFactura($factura) {
        $this->factura = $factura;
    }

    public function setPartida($partida) {
        $this->partida = $partida;
    }

    public function setFechaEjercicio($fechaEjercicio) {
        $this->fechaEjercicio = $fechaEjercicio;
    }

    public function setFiscal($Fiscal) {
        $this->Fiscal = $Fiscal;
    }

    public function setCheque($cheque) {
        $this->cheque = $cheque;
    }

    public function setRetencion($retencion) {
        $this->retencion = $retencion;
    }

}
