<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/RecHumanos.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class RecHumanoDaoJdbc {
    
    public function obtieneListado($id_carta) {
		
        $lista= array();
        $query="SELECT * FROM sie_carta_cons_det_rhum WHERE ccdh_estatus = 1 AND  cco_id_carta = ".(int)($id_carta)." ";

        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $idCarta= $rs[strtoupper("cco_id_carta")];
            $idRecurso = $rs[strtoupper("ccdh_id_recurso")];
            $orden = $rs[strtoupper("ccdh_ordenrecursohumano")];
            $recurso = $rs[strtoupper("ccdh_recursohumano")];
            $nomina = $rs[strtoupper("ccdh_ppto_nomina")];
            $honorario = $rs[strtoupper("ccdh_honorarios")];
            $factura  = $rs[strtoupper("ccdh_factura")];
            $partida = $rs[strtoupper("ccda_partida")];
            $fecha = $rs[strtoupper("ccda_fechaejercicio")];
            $fiscal = $rs[strtoupper("ccda_doctofiscal")];
            $cheque = $rs[strtoupper("ccda_nocheque")];
            $retencion = $rs[strtoupper("ccda_hojaretencion")];

            $elemento = new RecHumanos();
            $elemento->setAll($idRecurso,$idCarta,$orden,$recurso,$nomina,$honorario, $factura, $partida, $fecha, $fiscal, $cheque, $retencion);
            array_push($lista, $elemento);
        }	
        return $lista;
    }
    
    public function obtieneElemento($idElemento) {
			
        $elemento = new RecHumanos();
        $query="SELECT * FROM sie_carta_cons_det_rhum WHERE  ccdh_estatus = 1 AND ccdh_id_recurso = ".$idElemento;
        
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $idCarta= $rs[strtoupper("cco_id_carta")];
            $idRecurso = $rs[strtoupper("ccdh_id_recurso")];
            $orden = $rs[strtoupper("ccdh_ordenrecursohumano")];
            $recurso = $rs[strtoupper("ccdh_recursohumano")];
            $nomina = $rs[strtoupper("ccdh_ppto_nomina")];
            $honorario = $rs[strtoupper("ccdh_honorarios")];
            $factura  = $rs[strtoupper("ccdh_factura")];
            $partida = $rs[strtoupper("ccda_partida")];
            $fecha = $rs[strtoupper("ccda_fechaejercicio")];
            $fiscal = $rs[strtoupper("ccda_doctofiscal")];
            $cheque = $rs[strtoupper("ccda_nocheque")];
            $retencion = $rs[strtoupper("ccda_hojaretencion")];

            $elemento = new RecHumanos();
            $elemento->setAll($idRecurso,$idCarta,$orden,$recurso,$nomina,$honorario, $factura, $partida, $fecha, $fiscal, $cheque, $retencion);
        }	
        return $elemento;
    }
    
    public function guardaElemento($elemento) {
		
        $fecha = "";
		
        if($elemento->getFechaEjercicio()!=null)
        {        $fecha= "'".date("Y-m-d",strtotime($elemento->getFechaEjercicio()))."'";   }
        else
        {        $fecha = "null";   }
        $con=new Catalogo();
        $query="INSERT INTO sie_carta_cons_det_rhum(CCO_ID_CARTA,CCDH_ORDENRECURSOHUMANO,CCDH_RECURSOHUMANO,CCDH_PPTO_NOMINA,CCDH_HONORARIOS,CCDH_FACTURA,CCDA_PARTIDA,CCDA_FECHAEJERCICIO,CCDA_DOCTOFISCAL,CCDA_NOCHEQUE,CCDA_HOJARETENCION,CCDH_ESTATUS) VALUES (".$elemento->getIdCarta().", ". $elemento->getNum_orden() .", '".  $elemento->getRec_humano() ."',".
		$elemento->getNomina().",". $elemento->getHonorarios() .",'". $elemento->getFactura() ."', '". $elemento->getPartida() ."', ". $fecha ." , '". $elemento->getFiscal() ."'," .
                " '". $elemento->getCheque() ."' , '". $elemento->getRetencion() ."' , 1)";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
        $fecha = "";
		
        if($elemento->getFechaEjercicio()!=null)
        {        $fecha= "'".date("Y-m-d",strtotime($elemento->getFechaEjercicio()))."'";   }
        else
        {        $fecha = "null";   }
        
	$con=new Catalogo();
	$query="UPDATE sie_carta_cons_det_rhum  SET  ccdh_ordenrecursohumano = ".$elemento->getNum_orden().", ccdh_recursohumano = '".$elemento->getRec_humano(). "', " .
				"ccdh_ppto_nomina = ".$elemento->getNomina() .", ccdh_honorarios = ".$elemento->getHonorarios() ." , ccdh_factura = '".$elemento->getFactura() ."'," .
				"ccda_partida = '".$elemento->getPartida() ."' ,ccda_fechaejercicio =".$fecha.", ccda_doctofiscal = '".$elemento->getFiscal() ."'," .
				" ccda_nocheque = '".$elemento->getCheque() ."', ccda_hojaretencion = '".$elemento->getRetencion() ."'  WHERE ccdh_id_recurso = ".$elemento->getId() ."";
        $res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_carta_cons_det_rhum set  ccdh_estatus = 0 WHERE ccdh_id_recurso = ".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}
