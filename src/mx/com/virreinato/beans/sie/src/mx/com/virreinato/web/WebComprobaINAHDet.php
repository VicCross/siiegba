<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/ComprobaINAHDetDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ComprobaINAHDet.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$respuesta = "";
if(isset($_POST['guardar'])){
    $elemento=new ComprobaINAHDet();
			
    $elemento->setIdComprobacionINAH((int)($_POST["idMaestro"]));
    $elemento->setIdPartida((int)($_POST["idPartida"]));
    $elemento->setMonto((double)($_POST["monto"]));
    $elemento->setNum_notas((int)($_POST["num_notas"]));
    
    if (isset($_POST['id'])){
        $elemento->setId((int)($_POST["id"]));
        $dao=new ComprobaINAHDetDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new ComprobaINAHDetDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
    header("Location: ../../../../../Proyectos/AgregarComprobaINAH.php?respuesta=".$respuesta."&id=".$_POST['idMaestro']);
}else if (isset($_GET['id'])){
    $dao=new ComprobaINAHDetDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
    header("Location: ../../../../../Proyectos/AgregarComprobaINAH.php?respuesta=".$respuesta."&id=".$_GET['idMaestro']);
} else {
    $respuesta = "No se detecto la acción a realizar.";
    header("Location: ../../../../../Proyectos/AgregarComprobaINAH.php?respuesta=" . $respuesta);
}
?>

