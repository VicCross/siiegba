<?php
if(session_id() == '') {
    session_start();
}

include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Directivo.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CentroCostos.class.php");

class DirectivoDaoJdbc {

    public function obtieneElementoCargo($puesto) {

        $elemento = new Directivo();

        $query = "SELECT * FROM sie_cat_directivos CD, sie_cat_centrocosto CC WHERE CC.ccc_id_ccosto=CD.ccc_id_ccosto AND CDR_tipo='" . $puesto . "' and CDR_ESTATUS=1 ";
        $catalogo = new Catalogo();
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)) {
            $id = $rs[strtoupper("cdr_id_directivo")];
            $nombre = $rs[strtoupper("cdr_nombre")];
            $apaterno = $rs[strtoupper("cdr_appaterno")];
            $amaterno = $rs[strtoupper("cdr_apmaterno")];
            $cargo = $rs[strtoupper("cdr_cargo")];
            $tipo = $rs[strtoupper("cdr_tipo")];
            $iniciales = $rs[strtoupper("cdr_iniciales")];

            $idCcostos = $rs[strtoupper("ccc_id_ccosto")];
            $cccClave = $rs[strtoupper("ccc_clave_cc")];
            $cccDescripcion = $rs[strtoupper("ccc_descripcion")];

            $cCosto = new CentroCostos();
            $cCosto->setAll($idCcostos, $cccClave, $cccDescripcion);

            $elemento = new Directivo();
            $elemento->setAll($id, $nombre, $apaterno, $amaterno, $cargo, $tipo, $cCosto, $iniciales);
        }
        return $elemento;
    }
    
    public function obtieneListado(){
        $lista = array();
        $query = "SELECT * FROM sie_cat_directivos CD, sie_cat_centrocosto CC WHERE CC.ccc_id_ccosto=CD.ccc_id_ccosto AND cdr_estatus=1 ORDER BY cdr_nombre,cdr_appaterno,cdr_apmaterno";
        
        $con = new Catalogo();
        $result = $con->obtenerLista($query);
        
        while ($rs = mysql_fetch_array($result)) {
            $id= $rs[strtoupper("cdr_id_directivo")];
            $nombre= $rs[strtoupper("cdr_nombre")];
            $apaterno= $rs[strtoupper("cdr_appaterno")];
            $amaterno= $rs[strtoupper("cdr_apmaterno")];
            $cargo= $rs[strtoupper("cdr_cargo")];
            $tipo= $rs[strtoupper("cdr_tipo")];
            $iniciales= $rs[strtoupper("cdr_iniciales")];

            $idCcostos= $rs[strtoupper("ccc_id_ccosto")];
            $cccClave= $rs[strtoupper("ccc_clave_cc")];
            $cccDescripcion= $rs[strtoupper("ccc_descripcion")];

            $cCosto= new CentroCostos();
            $cCosto->setAll($idCcostos, $cccClave, $cccDescripcion);

            $elemento=new Directivo();
            $elemento->setAll($id,$nombre,$apaterno,$amaterno,$cargo,$tipo,$cCosto,$iniciales);
            array_push($lista, $elemento);
        }

        return $lista;
    }
    
    public function obtieneElemento($idElemento) {


            $elemento=new Directivo();

            $query="SELECT * FROM sie_cat_directivos CD, sie_cat_centrocosto CC WHERE CC.ccc_id_ccosto=CD.ccc_id_ccosto AND cdr_id_directivo=".$idElemento;

            $con=new Catalogo();
            $result = $con->obtenerLista($query);

            while ($rs = mysql_fetch_array($result)){
                    $id= $rs[strtoupper("cdr_id_directivo")];
                    $nombre= $rs[strtoupper("cdr_nombre")];
                    $apaterno= $rs[strtoupper("cdr_appaterno")];
                    $amaterno= $rs[strtoupper("cdr_apmaterno")];
                    $cargo= $rs[strtoupper("cdr_cargo")];
                    $tipo= $rs[strtoupper("cdr_tipo")];
                    $iniciales= $rs[strtoupper("cdr_iniciales")];

                    $idCcostos= $rs[strtoupper("ccc_id_ccosto")];
                    $cccClave= $rs[strtoupper("ccc_clave_cc")];
                    $cccDescripcion= $rs[strtoupper("ccc_descripcion")];

                    $cCosto= new CentroCostos();
                    $cCosto->setAll($idCcostos, $cccClave, $cccDescripcion);

                    $elemento=new Directivo();
                    $elemento->setAll($id,$nombre,$apaterno,$amaterno,$cargo,$tipo,$cCosto,$iniciales);

            }

    return $elemento;
    }

    public function guardaElemento($elemento) {
		
            $con=new Catalogo();
            $query="INSERT INTO sie_cat_directivos(cdr_nombre,cdr_appaterno,cdr_apmaterno,cdr_cargo,cdr_tipo, ccc_id_ccosto,cdr_iniciales) VALUES ('".$elemento->getNombre()."', '".$elemento->getApPaterno()."', '".$elemento->getApMaterno()."', '".$elemento->getCargo()."', '".$elemento->getTipoCargo()."', '".$elemento->getCcostos()->getId()."', '".$elemento->getIniciales()."' )";
            $res=$con->obtenerLista($query);

            if($res==1)
            {	return true; }
            else
            {	return false; }

    }
        
    public function actualizaElemento($elemento) {
		
		$con=new Catalogo();
		$query="UPDATE sie_cat_directivos set  cdr_nombre='".$elemento->getNombre()."', cdr_appaterno='".$elemento->getApPaterno()."', cdr_apmaterno='".$elemento->getApMaterno()."' , cdr_cargo='".$elemento->getCargo()."' , cdr_tipo='".$elemento->getTipoCargo()."' , ccc_id_ccosto='".$elemento->getCcostos()->getId()."', cdr_iniciales='".$elemento->getIniciales()."' WHERE cdr_id_directivo=".$elemento->getId();
		$res= $con->obtenerLista($query);
		
		if($res==1)
                {	return true; }
		else
                {	return false; }
		
	}
	
	public function eliminaElemento($idElemento){
		
		$con=new Catalogo();
		$query="UPDATE sie_cat_directivos set  cdr_estatus=0 WHERE cdr_id_directivo=".$idElemento;
		$res= $con->obtenerLista($query);
		
		if($res!=0)
                {	return true; }
		else
                {	return false; }
		
	}
}
