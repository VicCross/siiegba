<?php

class RecMateriales {
    private $id;
    private $idCarta;
    private $num_orden;
    private $recurso;
    private $partida;
    private $unitario;
    private $costo;
    private $fecha;
    private $facturaEntregada;
    private $nombreCheque;
    private $no_cheque;
    
    function setAll($id, $idCarta, $num_orden, $recurso, $partida, $unitario, $costo, $fecha, $facturaEntregada, $nombreCheque, $no_cheque) {
        $this->id = $id;
        $this->idCarta = $idCarta;
        $this->num_orden = $num_orden;
        $this->recurso = $recurso;
        $this->partida = $partida;
        $this->unitario = $unitario;
        $this->costo = $costo;
        $this->fecha = $fecha;
        $this->facturaEntregada = $facturaEntregada;
        $this->nombreCheque = $nombreCheque;
        $this->no_cheque = $no_cheque;
    }

    function constructor2($id, $idCarta, $recurso) {
        $this->id = $id;
        $this->idCarta = $idCarta;
        $this->recurso = $recurso;
    }

    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getIdCarta() {
        return $this->idCarta;
    }

    public function getNum_orden() {
        return $this->num_orden;
    }

    public function getRecurso() {
        return $this->recurso;
    }

    public function getPartida() {
        return $this->partida;
    }

    public function getUnitario() {
        return $this->unitario;
    }

    public function getCosto() {
        return $this->costo;
    }

    public function getFecha() {
        return $this->fecha;
    }

    public function getFacturaEntregada() {
        return $this->facturaEntregada;
    }

    public function getNombreCheque() {
        return $this->nombreCheque;
    }

    public function getNo_cheque() {
        return $this->no_cheque;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setIdCarta($idCarta) {
        $this->idCarta = $idCarta;
    }

    public function setNum_orden($num_orden) {
        $this->num_orden = $num_orden;
    }

    public function setRecurso($recurso) {
        $this->recurso = $recurso;
    }

    public function setPartida($partida) {
        $this->partida = $partida;
    }

    public function setUnitario($unitario) {
        $this->unitario = $unitario;
    }

    public function setCosto($costo) {
        $this->costo = $costo;
    }

    public function setFecha($fecha) {
        $this->fecha = $fecha;
    }

    public function setFacturaEntregada($facturaEntregada) {
        $this->facturaEntregada = $facturaEntregada;
    }

    public function setNombreCheque($nombreCheque) {
        $this->nombreCheque = $nombreCheque;
    }

    public function setNo_cheque($no_cheque) {
        $this->no_cheque = $no_cheque;
    }

}
