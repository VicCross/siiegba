<?php

class Pasaje {
    private $id;
    private $idCCosto;
    private $fecha;
    private $mes;
    private $tarifaDiaria;
    private $numeroDias;
    private $montoTotal;
    private $letraMonto;
    private $concepto;
    private $estatus;
    private $empleado;
    
    function setAll($id, $idCCosto, $fecha, $mes, $tarifaDiaria, $numeroDias, $montoTotal, $letraMonto, $concepto, $estatus, $empleado) {
        $this->id = $id;
        $this->idCCosto = $idCCosto;
        $this->fecha = $fecha;
        $this->mes = $mes;
        $this->tarifaDiaria = $tarifaDiaria;
        $this->numeroDias = $numeroDias;
        $this->montoTotal = $montoTotal;
        $this->letraMonto = $letraMonto;
        $this->concepto = $concepto;
        $this->estatus = $estatus;
        $this->empleado = $empleado;
    }

    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getIdCCosto() {
        return $this->idCCosto;
    }

    public function getFecha() {
        return $this->fecha;
    }

    public function getMes() {
        return $this->mes;
    }

    public function getTarifaDiaria() {
        return $this->tarifaDiaria;
    }

    public function getNumeroDias() {
        return $this->numeroDias;
    }

    public function getMontoTotal() {
        return $this->montoTotal;
    }

    public function getLetraMonto() {
        return $this->letraMonto;
    }

    public function getConcepto() {
        return $this->concepto;
    }

    public function getEstatus() {
        return $this->estatus;
    }

    public function getEmpleado() {
        return $this->empleado;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setIdCCosto($idCCosto) {
        $this->idCCosto = $idCCosto;
    }

    public function setFecha($fecha) {
        $this->fecha = $fecha;
    }

    public function setMes($mes) {
        $this->mes = $mes;
    }

    public function setTarifaDiaria($tarifaDiaria) {
        $this->tarifaDiaria = $tarifaDiaria;
    }

    public function setNumeroDias($numeroDias) {
        $this->numeroDias = $numeroDias;
    }

    public function setMontoTotal($montoTotal) {
        $this->montoTotal = $montoTotal;
    }

    public function setLetraMonto($letraMonto) {
        $this->letraMonto = $letraMonto;
    }

    public function setConcepto($concepto) {
        $this->concepto = $concepto;
    }

    public function setEstatus($estatus) {
        $this->estatus = $estatus;
    }

    public function setEmpleado($empleado) {
        $this->empleado = $empleado;
    }


}
