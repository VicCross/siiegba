<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Capitulo.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class CatChequeDaoJdbc {
    
    public function obtieneListado() {
		
        $lista = array();
	$query="select * from sie_cheques where che_estatusborrado= 1";
		
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id = $rs[strtoupper("che_id_cheque")];
            $folio = $rs[strtoupper("che_folio")];
            $destino = $rs[strtoupper("che_destino")];
            $idCuenta = $rs[strtoupper("ccb_id_cuentabancaria")];
            $monto = $rs[strtoupper("che_monto")];
            $letra = $rs[strtoupper("che_montoletra")];
            $fecha = $rs[strtoupper("che_fecha_emision")];
            $estatus = $rs[strtoupper("che_estatus")];
            $obs = $rs[strtoupper("che_observaciones")];
            $idSolicitud = $rs[strtoupper("spr_id_solicitud")];
            $poliza = $rs[strtoupper("che_polizacheque")];
            $periodo = $rs[strtoupper("cpe_id_periodo")];
            $area = $rs[strtoupper("che_area")];
            $idProyecto = $rs[strtoupper("cpr_id_proyecto")];
            $idEmpleado=$rs[strtoupper("cem_id_empleado")];

            $elemento = new CatCheque();
            $elemento->setAll($id,$folio,$destino,$idCuenta,$monto,$letra,$fecha,$estatus,$obs,$poliza,$idSolicitud,$periodo,$idProyecto,$area, $idEmpleado);

            array_push($lista, $elemento);

	}

        return $lista;	
    }
    
    public  function obtieneListadoEmitido(){
        $lista = array();
        $query="select che_id_cheque,che_folio,che_monto, cpr_id_proyecto " .
            " from sie_cheques CH, SIE_CAT_PERIODOS P  " .
            " where CH.CPE_ID_PERIODO=P.CPE_ID_PERIODO AND P.CPE_CERRADO=0 AND P.CPE_ESTATUS=1 AND CH.che_estatusborrado= 1 AND CH.che_estatus IN ('EM', 'CO') ";
			
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id = $rs[strtoupper("che_id_cheque")];
            $folio = $rs[strtoupper("che_folio")];
            $monto = $rs[strtoupper("che_monto")];
            $idProyecto = $rs[strtoupper("cpr_id_proyecto")];

            $elemento = new CatCheque();
            $elemento->setCheq($id,$folio,$monto,$idProyecto);

            array_push($lista, $elemento);

        }

        return $lista;
    }

    public  function obtieneListadoEmitidoSinCmpDestChe($destiny,$idCheque){
        
	$lista = array();
		
	$query=strtolower("select che_id_cheque,che_folio,che_monto, cpr_id_proyecto, che_destino  " .
            " from sie_cheques CH, SIE_CAT_PERIODOS P  " .
            " where CH.CPE_ID_PERIODO=P.CPE_ID_PERIODO AND P.CPE_CERRADO=0 AND P.CPE_ESTATUS=1 " .
            " AND CH.che_estatusborrado= 1 AND CH.che_estatus IN ('EM', 'CO') AND CH.che_destino='".$destiny."' " .
            " AND CH.che_id_cheque not in (SELECT CHE_ID_CHEQUE FROM SIE_COMPROBACIONES WHERE COM_PRE_ESTATUS=1 AND COM_DESTINO='".$destiny."' ) " .
            " UNION select che_id_cheque,che_folio,che_monto, cpr_id_proyecto, che_destino   from sie_cheques ".    
            " where che_id_cheque=".$idCheque);

	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id = $rs["che_id_cheque"];
            $folio = $rs["che_folio"];
            $monto = $rs["che_monto"];
            $idProyecto =null;
            if($destiny == "PR"){
                    $idProyecto = $rs["cpr_id_proyecto"];
            }
            else{
                    $idProyecto =0;
            }
                    
            $destino = $rs["che_destino"];
            $elemento = new CatCheque();
            $elemento->catCheqDesti($id,$folio,$destino,$monto,$idProyecto);

            array_push($lista, $elemento);

        }

        return $lista;
    }
    
    public  function obtieneListadoEmitidoSinCmpDest($destiny){
        
	$lista = array();
		
	$query=strtolower("select che_id_cheque,che_folio,che_monto, cpr_id_proyecto, che_destino  " .
            " from sie_cheques CH, SIE_CAT_PERIODOS P  " .
            " where CH.CPE_ID_PERIODO=P.CPE_ID_PERIODO AND P.CPE_CERRADO=0 AND P.CPE_ESTATUS=1 " .
            "AND CH.che_estatusborrado= 1 AND CH.che_estatus IN ('EM', 'CO') AND CH.che_destino='".$destiny."' " .
            " AND CH.che_id_cheque not in (SELECT CHE_ID_CHEQUE FROM SIE_COMPROBACIONES WHERE COM_PRE_ESTATUS=1 AND COM_DESTINO='".$destiny."' ) order by che_folio*1 ");
		
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id = $rs["che_id_cheque"];
            $folio = $rs["che_folio"];
            $monto = $rs["che_monto"];
            $idProyecto =null;
            if($destiny == "PR"){
                    $idProyecto = $rs[strtoupper("cpr_id_proyecto")];
            }
            else{
                    $idProyecto =0;
            }
            
            $destino = $rs["che_destino"];
            $elemento = new CatCheque();
            $elemento->catCheqDesti($id,$folio,$destino,$monto,$idProyecto);

            array_push($lista, $elemento);

        }

        return $lista;
    }
    
    public function obtieneVoBo($idSolicitud ){
        
	$vobo = null;
	$query = "SELECT spr_vobo_sa, spr_vobo_st, spr_vobo_director FROM sie_solicitud_presup WHERE spr_id_solicitud = ".$idSolicitud."";

        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
           $vobo = $rs["spr_vobo_sa"]."/".$rs["spr_vobo_st"]."/".$rs["spr_vobo_director"];	
        }

        return $vobo;
    }
    
    public  function obtieneEstatus($idSolicitud){
        
	$estatus = null;
	$query =  "SELECT che_estatus FROM sie_cheques WHERE spr_id_solicitud = ".$idSolicitud." AND che_estatusborrado = 1 ";

	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);
        while ($rs = mysql_fetch_array($result)){
            $estatus = $rs["che_estatus"];
        }
			
        return $estatus;
    }
    
    public function obtieneCheque($idSolicitud){
        $id = 0;
        $query="SELECT che_id_cheque FROM sie_cheques WHERE spr_id_solicitud = ".$idSolicitud." AND che_estatusborrado = 1 "; 
                
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id = $rs["che_id_cheque"];
        }

        return $id;
    }
    
    public function obtieneElemento($idElemento) {
        
        $elemento=new CatCheque();
        $query="SELECT * FROM sie_cheques WHERE che_id_cheque = ".$idElemento;

        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id = $rs[strtoupper("che_id_cheque")];
            $folio = $rs[strtoupper("che_folio")];
            $destino = $rs[strtoupper("che_destino")];
            $idCuenta = $rs[strtoupper("ccb_id_cuentabancaria")];
            $monto = $rs[strtoupper("che_monto")];
            $letra = $rs[strtoupper("che_montoletra")];
            $fecha = $rs[strtoupper("che_fecha_emision")];
            $estatus = $rs[strtoupper("che_estatus")];
            $obs = $rs[strtoupper("che_observaciones")];
            $idSolicitud = $rs[strtoupper("spr_id_solicitud")];
            $poliza = $rs[strtoupper("che_polizacheque")];
            $periodo = $rs[strtoupper("cpe_id_periodo")];
            $area = $rs[strtoupper("che_area")];
            $idProyecto = $rs[strtoupper("cpr_id_proyecto")];
            $idEmpleado=$rs[strtoupper("cem_id_empleado")];

            $elemento = new CatCheque();
            $elemento->setAll($id,$folio,$destino,$idCuenta,$monto,$letra,$fecha,$estatus,$obs,$poliza,$idSolicitud,$periodo,$idProyecto,$area, $idEmpleado);
            }

        return $elemento;
    }
    
    public function obtieneChequesGB($idSueldos) {
        
	$elemento=new CatCheque();
	$listaCheques= array();
	$query="SELECT * FROM sie_cheques WHERE che_destino='SL' and spr_id_solicitud = ".$idSueldos;

	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
		
            $id = $rs[strtoupper("che_id_cheque")];
            $folio = $rs[strtoupper("che_folio")];
            $destino = $rs[strtoupper("che_destino")];
            $idCuenta = $rs[strtoupper("ccb_id_cuentabancaria")];
            $monto = $rs[strtoupper("che_monto")];
            $letra = $rs[strtoupper("che_montoletra")];
            $fecha = $rs[strtoupper("che_fecha_emision")];
            $estatus = $rs[strtoupper("che_estatus")];
            $obs = $rs[strtoupper("che_observaciones")];
            $idSolicitud = $rs[strtoupper("spr_id_solicitud")];
            $poliza = $rs[strtoupper("che_polizacheque")];
            $periodo = $rs[strtoupper("cpe_id_periodo")];
            $area = $rs[strtoupper("che_area")];
            $idProyecto = $rs[strtoupper("cpr_id_proyecto")];
            $idEmpleado=$rs[strtoupper("cem_id_empleado")];
				
            $elemento = new CatCheque();
            $elemento->setAll($id,$folio,$destino,$idCuenta,$monto,$letra,$fecha,$estatus,$obs,$poliza,$idSolicitud,$periodo,$idProyecto,$area, $idEmpleado);
            array_push($listaCheques, $elemento);
            }
		
        return $listaCheques;
		
    }
    
    public function guardaElemento($elemento) {
		
	$con=new Catalogo();
	$fecha = "";
        $proyecto = 'null';
		
        if($elemento->getFechaEmision()!=null)
        {	$fecha= "'".date("Y-m-d",strtotime($elemento->getFechaEmision()))."'"; }
        else
        {	$fecha = "null";}
        if($elemento->getIdPeriodo()){
            
        }

        $idEmpleado=0;
        if($elemento->getIdEmpleado()!=null){
            $idEmpleado=$elemento->getIdEmpleado(); 	
        }
        if($elemento->getIdProyecto()!=null){
            $proyecto = $elemento->getIdProyecto();
        }

        $query="INSERT INTO sie_cheques(che_folio,che_destino,ccb_id_cuentabancaria,che_monto,che_montoletra,che_fecha_emision,che_estatus,che_tipobeneficiario,cem_id_empleado,che_observaciones,cpr_id_proyecto,cpe_id_periodo,cpr_id_proveedor,spr_id_solicitud,che_polizacheque,che_beneficiario,che_area,che_estatusborrado) VALUES (".$elemento->getFolio().", '".$elemento->getDestino()."', ".$elemento->getIdCuenta().", ".$elemento->getMonto().", '".$elemento->getMontoLetra()."'," .
            " ".$fecha." , '".$elemento->getEstatus()."' ,null,".$idEmpleado.",'".$elemento->getObservaciones()."', ".$proyecto.", ".$elemento->getIdPeriodo().", null, ".$elemento->getIdSolicitud()." , ".$elemento->getNum_Poliza().", null,".$elemento->getArea().", 1 )";
	$res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
		
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
	$fecha = "";
		
	if($elemento->getFechaEmision()!=null)
        {	$fecha= "'".date("Y-m-d",strtotime($elemento->getFechaEmision()))."'"; }
        else
        {	$fecha = "null";}

        $idEmpleado=0;
        if($elemento->getIdEmpleado()!=null){
            $idEmpleado=$elemento->getIdEmpleado(); 	
        }
		
	$query="UPDATE sie_cheques set che_folio = ".$elemento->getFolio()." ,che_destino ='".$elemento->getDestino()."', ccb_id_cuentabancaria = ".$elemento->getIdCuenta()." , che_monto = ".$elemento->getMonto()." ," .
				"che_montoletra ='".$elemento->getMontoLetra()."' , che_fecha_emision = ".$fecha." , che_estatus = '".$elemento->getEstatus()."' ,cem_id_empleado=".$idEmpleado.", che_observaciones = '".$elemento->getObservaciones()."' , cpe_id_periodo = ".$elemento->getIdPeriodo()."," .
				"che_polizacheque = ".$elemento->getNum_Poliza().", che_area=".$elemento->getArea()." WHERE che_id_cheque = ".$elemento->getId();
		
	$res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_cheques set che_estatusborrado = 0 WHERE che_id_cheque = ".$idElemento." AND che_estatus = 'RE' ";
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function ImprimirElemento($idElemento){
        
	$res = 0;
	$query = "SELECT p.spr_vobo_st, p.spr_vobo_sa, p.spr_vobo_director, p.spr_id_solicitud, p.spr_estatus FROM sie_solicitud_presup p, sie_cheques ch WHERE ch.che_id_cheque =".$idElemento." AND ch.spr_id_solicitud = p.spr_id_solicitud ";

	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $st = $rs["spr_vobo_st"];
            $sa = $rs["spr_vobo_sa"];
            $dr = $rs["spr_vobo_director"];
            $estatus = $rs["spr_estatus"];
            $id = $rs["spr_id_solicitud"];
            if( $st == "Y" || $sa == "Y" || $dr == "Y" || $estatus == "IP" ){
                $query = "UPDATE sie_solicitud_presup SET spr_estatus = 'IP' WHERE spr_id_solicitud =".$id."";
                $res = $catalogo->obtenerLista($query);
            }
        }
		
	return $res;
    }
}
