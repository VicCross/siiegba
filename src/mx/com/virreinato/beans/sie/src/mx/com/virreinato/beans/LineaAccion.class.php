<?php

class LineaAccion {
    private $Id;
    private $eje;
    private $lineaAccion;
    private $descripcion;
    
    function setAll($Id, $eje, $lineaAccion, $descripcion) {
        $this->Id = $Id;
        $this->eje = $eje;
        $this->lineaAccion = $lineaAccion;
        $this->descripcion = $descripcion;
    }

    function __construct() {
        
    }

    public function getId() {
        return $this->Id;
    }

    public function getEje() {
        return $this->eje;
    }

    public function getLineaAccion() {
        return $this->lineaAccion;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setEje($eje) {
        $this->eje = $eje;
    }

    public function setLineaAccion($lineaAccion) {
        $this->lineaAccion = $lineaAccion;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }


}
