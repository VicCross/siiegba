<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/DetComprobacionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/DetComprobacion.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$respuesta = "";
if (isset($_POST['guardar'])){
    
    $elemento=new DetComprobacion();

    $elemento->setIdComprobacion((int)($_POST["Comprobacion"]));
    $elemento->setTipo($_POST["Tipo"]);
    $elemento->setFolio( $_POST["Folio"] );
    $elemento->setFecha(date("Y-m-d",strtotime( $_POST["fecha"] )));
    $elemento->setIdPartida((int)($_POST["Partida"]));
    $elemento->setMonto((double)($_POST["Monto"]));
    $elemento->setDescripcion($_POST["Descripcion"]);
    $elemento->setUrl(null);
    $elemento->setIdProveedor((int)($_POST["proveedor"]));
    
    if (isset($_POST['id']) && $_POST["id"] != null){
        $elemento->setIdComprobante((int)($_POST["id"]));
        $dao=new DetComprobacionDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new DetComprobacionDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
    $id = $elemento->getIdComprobacion();
}else if (isset($_GET['id'])){
    $dao=new DetComprobacionDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
    $id = $_GET['idC'];
} else {
    $respuesta = "No se detecto la acción a realizar.";
}
header("Location: ../../../../../Terceros/Agregar_ComprobacionTr.php?respuesta=" . $respuesta."&id=".$id);
?>

