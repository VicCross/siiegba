<?php

class Usuario {
    private $Id;
    private $perfil;
    private $persona;
    private $usuario;
    private $password;
    private $idMetas;
    
    function setAll($Id, $perfil, $persona, $usuario, $password, $idMetas) {
        $this->Id = $Id;
        $this->perfil = $perfil;
        $this->persona = $persona;
        $this->usuario = $usuario;
        $this->password = $password;
        $this->idMetas = $idMetas;
    }

    function __construct() {
        
    }

    public function getId() {
        return $this->Id;
    }

    public function getPerfil() {
        return $this->perfil;
    }

    public function getPersona() {
        return $this->persona;
    }

    public function getUsuario() {
        return $this->usuario;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getIdMetas() {
        return $this->idMetas;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setPerfil($perfil) {
        $this->perfil = $perfil;
    }

    public function setPersona($persona) {
        $this->persona = $persona;
    }

    public function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function setIdMetas($idMetas) {
        $this->idMetas = $idMetas;
    }


}
