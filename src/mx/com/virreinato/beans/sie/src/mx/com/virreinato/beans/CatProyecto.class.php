<?php

class CatProyecto {
    private $Id;
    private $areaNormativa; 
    private $centroCostos;
    private $descripcion;
    private $origen;
    private $lider;
    private $monto;
    private $lineaAccion;
    private $numero;
    private $periodo;
    private $antecedentes;
    private $objetivoGral;
    private $limites;
    private $impactoSocial;
    private $impactoOrganizacional;
    private $impactoEstrategico;
    private $beneficiarios;
    private $estatus;
    private $tipoproy;

    private $fechaIniProyecto;
    private $fechaFinProyecto;
    private $fechaIniEjercicio;
    private $fechaFinEjercicio;
    private $area;
    
    function catProy($Id, $descripcion, $numero) {
        $this->Id = $Id;
        $this->descripcion = $descripcion;
        $this->numero = $numero;
    }
    
    function setAll($Id, $areaNormativa, $centroCostos, $descripcion, $origen, $lider, $monto, $lineaAccion, $numero, $periodo, $antecedentes, $objetivoGral, $limites, $impactoSocial, $impactoOrganizacional, $impactoEstrategico, $beneficiarios, $estatus, $tipoproy, $fechaIniProyecto, $fechaFinProyecto, $fechaIniEjercicio, $fechaFinEjercicio, $area) {
        $this->Id = $Id;
        $this->areaNormativa = $areaNormativa;
        $this->centroCostos = $centroCostos;
        $this->descripcion = $descripcion;
        $this->origen = $origen;
        $this->lider = $lider;
        $this->monto = $monto;
        $this->lineaAccion = $lineaAccion;
        $this->numero = $numero;
        $this->periodo = $periodo;
        $this->antecedentes = $antecedentes;
        $this->objetivoGral = $objetivoGral;
        $this->limites = $limites;
        $this->impactoSocial = $impactoSocial;
        $this->impactoOrganizacional = $impactoOrganizacional;
        $this->impactoEstrategico = $impactoEstrategico;
        $this->beneficiarios = $beneficiarios;
        $this->estatus = $estatus;
        $this->tipoproy = $tipoproy;
        $this->fechaIniProyecto = $fechaIniProyecto;
        $this->fechaFinProyecto = $fechaFinProyecto;
        $this->fechaIniEjercicio = $fechaIniEjercicio;
        $this->fechaFinEjercicio = $fechaFinEjercicio;
        $this->area = $area;
    }

    function __construct() {
        
    }

    public function getId() {
        return $this->Id;
    }

    public function getAreaNormativa() {
        return $this->areaNormativa;
    }

    public function getCentroCostos() {
        return $this->centroCostos;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function getOrigen() {
        return $this->origen;
    }

    public function getLider() {
        return $this->lider;
    }

    public function getMonto() {
        return $this->monto;
    }

    public function getLineaAccion() {
        return $this->lineaAccion;
    }

    public function getNumero() {
        return $this->numero;
    }

    public function getPeriodo() {
        return $this->periodo;
    }

    public function getAntecedentes() {
        return $this->antecedentes;
    }

    public function getObjetivoGral() {
        return $this->objetivoGral;
    }

    public function getLimites() {
        return $this->limites;
    }

    public function getImpactoSocial() {
        return $this->impactoSocial;
    }

    public function getImpactoOrganizacional() {
        return $this->impactoOrganizacional;
    }

    public function getImpactoEstrategico() {
        return $this->impactoEstrategico;
    }

    public function getBeneficiarios() {
        return $this->beneficiarios;
    }

    public function getEstatus() {
        return $this->estatus;
    }

    public function getTipoproy() {
        return $this->tipoproy;
    }

    public function getFechaIniProyecto() {
        return $this->fechaIniProyecto;
    }

    public function getFechaFinProyecto() {
        return $this->fechaFinProyecto;
    }

    public function getFechaIniEjercicio() {
        return $this->fechaIniEjercicio;
    }

    public function getFechaFinEjercicio() {
        return $this->fechaFinEjercicio;
    }

    public function getArea() {
        return $this->area;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setAreaNormativa($areaNormativa) {
        $this->areaNormativa = $areaNormativa;
    }

    public function setCentroCostos($centroCostos) {
        $this->centroCostos = $centroCostos;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function setOrigen($origen) {
        $this->origen = $origen;
    }

    public function setLider($lider) {
        $this->lider = $lider;
    }

    public function setMonto($monto) {
        $this->monto = $monto;
    }

    public function setLineaAccion($lineaAccion) {
        $this->lineaAccion = $lineaAccion;
    }

    public function setNumero($numero) {
        $this->numero = $numero;
    }

    public function setPeriodo($periodo) {
        $this->periodo = $periodo;
    }

    public function setAntecedentes($antecedentes) {
        $this->antecedentes = $antecedentes;
    }

    public function setObjetivoGral($objetivoGral) {
        $this->objetivoGral = $objetivoGral;
    }

    public function setLimites($limites) {
        $this->limites = $limites;
    }

    public function setImpactoSocial($impactoSocial) {
        $this->impactoSocial = $impactoSocial;
    }

    public function setImpactoOrganizacional($impactoOrganizacional) {
        $this->impactoOrganizacional = $impactoOrganizacional;
    }

    public function setImpactoEstrategico($impactoEstrategico) {
        $this->impactoEstrategico = $impactoEstrategico;
    }

    public function setBeneficiarios($beneficiarios) {
        $this->beneficiarios = $beneficiarios;
    }

    public function setEstatus($estatus) {
        $this->estatus = $estatus;
    }

    public function setTipoproy($tipoproy) {
        $this->tipoproy = $tipoproy;
    }

    public function setFechaIniProyecto($fechaIniProyecto) {
        $this->fechaIniProyecto = $fechaIniProyecto;
    }

    public function setFechaFinProyecto($fechaFinProyecto) {
        $this->fechaFinProyecto = $fechaFinProyecto;
    }

    public function setFechaIniEjercicio($fechaIniEjercicio) {
        $this->fechaIniEjercicio = $fechaIniEjercicio;
    }

    public function setFechaFinEjercicio($fechaFinEjercicio) {
        $this->fechaFinEjercicio = $fechaFinEjercicio;
    }

    public function setArea($area) {
        $this->area = $area;
    }

}
