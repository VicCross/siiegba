<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/CatChequeDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatCheque.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$respuesta = "";
if(isset($_POST['Actualizar'])){
    $monto = $_POST["monto"];
    $letra =  $_POST["letra"];
    $idCheque = $_POST["Cheque"];
    $idProyecto = $_POST["idProyecto"];

    if( !$idCheque == "0"){
        $query = "UPDATE sie_cheques SET che_monto = ".$monto." , che_montoletra = '".$letra."', cpr_id_proyecto = ".$idProyecto." WHERE che_id_cheque = ".$idCheque."";
            $con = new Catalogo();
            $con->obtenerLista($query);
    }
}else if(isset($_POST['guardar'])){
    
    $elemento = new CatCheque();
    $idCheque = 0;

    $elemento->setIdCuenta((int)($_POST["cuenta_cheque"]));
    $elemento->setIdPeriodo((int)($_POST["periodo"]));
    $elemento->setIdSolicitud((int)($_POST["idSolicitud"]));
    $elemento->setDestino($_POST["destino_cheque"]);
    $elemento->setEstatus($_POST["estatus_cheque"]);
    $elemento->setObservaciones($_POST["obs_cheque"]);
    $elemento->setFechaEmision(date("Y-m-d",strtotime($_POST["fecha"])));
    $elemento->setFolio((int)($_POST["folio"]));
    $elemento->setNum_Poliza((int)($_POST["poliza"]));
    $elemento->setMonto((float)($_POST["monto"]));
    $elemento->setMontoLetra($_POST["monto_letra"]);
    $elemento->setArea((int)($_POST["areaEmpleado"]));
    $elemento->setIdProyecto((int)$_POST["idProyecto"]);
    
    if ($_POST["id"] != null){
        $elemento->setId((int)($_POST["id"]));
        $dao=new CatChequeDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente";
            $idCheque = $dao->obtieneCheque(($elemento->getIdSolicitud()));
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new CatChequeDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
    $idCheque = $dao->obtieneCheque((String)($elemento->getIdSolicitud())); 
    $solicitud = "";
    if(isset($_POST['idSolicitud']) && $_POST['idSolicitud'] != ""){
        $solicitud = "&idSolicitud=".$_POST['idSolicitud'];
    }
    header("Location: ../../../../../Proyectos/Imprimir_Cheque.php?respuesta=" . $respuesta."&idCheque=".(String)$idCheque.$solicitud);
}else if (isset($_GET['id'])){
    if(isset($_GET['estatus']) && $_GET['estatus'] == "RE"){
        $dao=new CatChequeDaoJdbc();
        $res=$dao->eliminaElemento((int)($_GET['id']));

        if ($res) {
            $respuesta = "El registro se eliminó exitosamente.";
        } else {
            $respuesta = "No fue posible eliminar el registro de la base de datos.";
        }
    }else{
        $respuesta = "No fue posible eliminar el cheque porque tiene no tiene el estatus de Registrado.";
    }
    header("Location: ../../../../../Proyectos/Solicitud.php?respuesta=" . $respuesta);
} else {
    $respuesta = "No se detecto la acción a realizar.";
    header("Location: ../../../../../Proyectos/Solicitud.php?respuesta=" . $respuesta);
}
?>

