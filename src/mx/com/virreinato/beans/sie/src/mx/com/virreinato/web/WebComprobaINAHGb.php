<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/ComprobaINAHDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ComprobaINAH.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
if(isset($_POST['inicio'])){
    $inicio = $_POST["inicio"];
    $fin = $_POST["fin"];
    $periodo = $_POST["periodo"];
    
    header("Location: ../../../../../gasto_basico/lista_ComprobaINAHGb.php?inicio=".$inicio."&fin=".$fin."&periodo=".$periodo);
}else if(isset($_POST['idPeriodo'])){
    $elemento = new ComprobaINAH();

    $elemento->setIdPeriodo((int)($_POST['idPeriodo']));
    $value = $_POST['idMinistracion'];
    $aux = explode("/", $value);
    $elemento->setIdMinistracion((int)($aux[0]));
    $elemento->setUrl($_POST['url']);

    if (isset($_POST['fecha']) && $_POST['fecha']!= "") {
        $fechaConFormato = date("Y-m-d",strtotime($_POST['fecha']));
        $elemento->setFecha($fechaConFormato);
    }

    $elemento->setIdCCosto((int)($_POST['idCCosto']));
    if (isset($_POST['id'])){
        $elemento->setId((int)($_POST["id"]));
        $dao=new ComprobaINAHDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new ComprobaINAHDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
    header("Location: ../../../../../gasto_basico/lista_ComprobaINAHGb.php?respuesta=" . $respuesta);
}else if (isset($_GET['id'])){
    $dao=new ComprobaINAHDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
    header("Location: ../../../../../gasto_basico/lista_ComprobaINAHGb.php?respuesta=" . $respuesta);
} else {
    $respuesta = "No se detecto la acción a realizar.";
    header("Location: ../../../../../gasto_basico/lista_ComprobaINAHGb.php?respuesta=" . $respuesta);
}
?>

