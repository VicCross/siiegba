<?php

class SueldoGB {
    private $id;
    private $idPeriodo;
    private $periodo;
    private $fecha;
    private $idMinistracion;
    private $tipoNomina;
    private $monto;
    private $quincena;
    private $estatus; 
    private $detalleSueldos;
    private $id_DetalleSueldos;
    
    function setAll($id, $idPeriodo, $periodo, $fecha, $idMinistracion, $tipoNomina, $monto, $quincena, $estatus, $detalleSueldos, $id_DetalleSueldos) {
        $this->id = $id;
        $this->idPeriodo = $idPeriodo;
        $this->periodo = $periodo;
        $this->fecha = $fecha;
        $this->idMinistracion = $idMinistracion;
        $this->tipoNomina = $tipoNomina;
        $this->monto = $monto;
        $this->quincena = $quincena;
        $this->estatus = $estatus;
        $this->detalleSueldos = $detalleSueldos;
        $this->id_DetalleSueldos = $id_DetalleSueldos;
    }

    function constructor2($id, $idPeriodo, $periodo, $fecha, $idMinistracion, $tipoNomina, $monto, $quincena, $estatus, $detalleSueldos) {
        $this->id = $id;
        $this->idPeriodo = $idPeriodo;
        $this->periodo = $periodo;
        $this->fecha = $fecha;
        $this->idMinistracion = $idMinistracion;
        $this->tipoNomina = $tipoNomina;
        $this->monto = $monto;
        $this->quincena = $quincena;
        $this->estatus = $estatus;
        $this->detalleSueldos = $detalleSueldos;
    }

    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getIdPeriodo() {
        return $this->idPeriodo;
    }

    public function getPeriodo() {
        return $this->periodo;
    }

    public function getFecha() {
        return $this->fecha;
    }

    public function getIdMinistracion() {
        return $this->idMinistracion;
    }

    public function getTipoNomina() {
        return $this->tipoNomina;
    }

    public function getMonto() {
        return $this->monto;
    }

    public function getQuincena() {
        return $this->quincena;
    }

    public function getEstatus() {
        return $this->estatus;
    }

    public function getDetalleSueldos() {
        return $this->detalleSueldos;
    }

    public function getId_DetalleSueldos() {
        return $this->id_DetalleSueldos;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setIdPeriodo($idPeriodo) {
        $this->idPeriodo = $idPeriodo;
    }

    public function setPeriodo($periodo) {
        $this->periodo = $periodo;
    }

    public function setFecha($fecha) {
        $this->fecha = $fecha;
    }

    public function setIdMinistracion($idMinistracion) {
        $this->idMinistracion = $idMinistracion;
    }

    public function setTipoNomina($tipoNomina) {
        $this->tipoNomina = $tipoNomina;
    }

    public function setMonto($monto) {
        $this->monto = $monto;
    }

    public function setQuincena($quincena) {
        $this->quincena = $quincena;
    }

    public function setEstatus($estatus) {
        $this->estatus = $estatus;
    }

    public function setDetalleSueldos($detalleSueldos) {
        $this->detalleSueldos = $detalleSueldos;
    }

    public function setId_DetalleSueldos($id_DetalleSueldos) {
        $this->id_DetalleSueldos = $id_DetalleSueldos;
    }


}
