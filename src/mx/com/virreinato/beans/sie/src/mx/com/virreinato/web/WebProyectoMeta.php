<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/ProyectoMetaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProyectoMeta.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
if(isset($_POST['guardar']) && $_POST['guardar']!=null){
     
    $elemento = new ProyectoMeta();
    $elemento->setDescripcion($_POST["meta"]);
    $elemento->setIdProyecto((int)($_POST["idProyecto"]));
    $elemento->setIdProgramaInstitucional((int)($_POST["programa_inst"]));
    $elemento->setIdProgramaOperativo((int)($_POST["programa_op"]));

    $dao= new ProyectoMetaDaoJdbc();
    $res = "";
    if ($_POST["id"] != null){
        $elemento->setIdMeta((int)($_POST["id"]));
        $res = $dao->actualizaElemento($elemento);
        if ($res!=null) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
	$res=$dao->guardaElemento($elemento);
        
        if ($res!=null) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
    header("Location: ../../../../../catalogos/lista_metas_proyecto.php?respuesta=" . $respuesta."&idProyecto=".$elemento->getIdProyecto()."&desc=".$res);
}else if (isset($_GET['id'])){
    $dao= new ProyectoMetaDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
header("Location: ../../../../../catalogos/lista_metas_proyecto.php?idProyecto=".$_GET['idProyecto']."&desc".$_GET['desc']);
} else {
    $respuesta = "No se detecto la acción a realizar.";
}
?>

