<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/CatChequeDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatCheque.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/ComprobacionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Comprobacion.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/CatProyectoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatProyecto.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
if(isset($_POST['inicio'])){
    $inicio = $_POST["inicio"];
    $fin = $_POST["fin"];
    $periodo = $_POST["periodo"];
    
    header("Location: ../../../../../Terceros/lista_ComprobacionTr.php?inicio=".$inicio."&fin=".$fin."&periodo=".$periodo);
}else if (isset($_POST['Folio']) && $_POST['Folio']!=null){
    $elemento=new Comprobacion();

    $elemento->setFolio( $_POST["Folio"] );
    $elemento->setFecha(date("Y-m-d",strtotime($_POST["Fecha"])));
    $elemento->setIdPeriodo((int)($_POST["Periodo"]));
    $elemento->setIdProyecto(0);
    $elemento->setMontoComprobacion((double)($_POST["Monto"]));
    $elemento->setDescripcion($_POST["Descripcion"]);
    $aux = explode("/",$_POST["Cheque"]);
    $elemento->setIdCheque((int)( $aux[1] ));
    $elemento->setEstatus($_POST["Estatus"]);
    $elemento->setDestino($_POST["Destino"]);
    $elemento->setObservaciones($_POST["Observaciones"]);
    
     if (isset($_POST['id']) && $_POST["id"] != null){
        $elemento->setIdComprobacion((int)($_POST["id"]));
        $dao=new ComprobacionDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
            $id = (String)$dao->obtieneId();
            header("Location: ../../../../../Terceros/lista_ComprobacionTr.php?respuesta=" . $respuesta."&idComprobacion=".$id);
        } else {
            $respuesta = "No fue posible actualizar su información.";
              header("Location: ../../../../../Terceros/lista_ComprobacionTr.php?respuesta=" . $respuesta);
        }
    }else{
        $dao=new ComprobacionDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
            header("Location: ../../../../../Terceros/lista_ComprobacionTr.php?respuesta=" . $respuesta."&id=".$POST['id']);
        } else {
            $respuesta = "No fue posible almacenar su información.";
              header("Location: ../../../../../Terceros/lista_ComprobacionTr.php?respuesta=" . $respuesta);
        }
    }
}else if (isset($_GET['id'])){
    $dao=new ComprobacionDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
    header("Location: ../../../../../Terceros/lista_ComprobacionTr.php?respuesta=" . $respuesta);
} else {
    $respuesta = "No se detecto la acción a realizar.";
      header("Location: ../../../../../Terceros/lista_ComprobacionTr.php?respuesta=" . $respuesta);
}
?>
