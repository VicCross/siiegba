<?php

class CartaEquipo {
    
    private $id;
    private $idCarta;
    private $nombre;
    
    function setAll($id, $idCarta, $nombre) {
        $this->id = $id;
        $this->idCarta = $idCarta;
        $this->nombre = $nombre;
    }
    
    function cartaEqui($id, $idCarta) {
        $this->id = $id;
        $this->idCarta = $idCarta;
    }
    
    public function __construct() {

    }

    public function getId() {
        return $this->id;
    }

    public function getIdCarta() {
        return $this->idCarta;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setIdCarta($idCarta) {
        $this->idCarta = $idCarta;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

}
