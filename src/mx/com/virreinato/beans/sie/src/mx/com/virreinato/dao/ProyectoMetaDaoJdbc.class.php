<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProyectoMeta.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class ProyectoMetaDaoJdbc {
    
    public function obtenerListado($idProyecto){
        $lista =  array();
        $query = "SELECT * FROM sie_proyecto_metas m, sie_cat_programa_institucional pi, sie_cat_programa_operativo po " .
                " WHERE m.cpi_id_programa=pi.cpi_id_programa AND m.cpo_id_programa=po.cpo_id_programa AND " .
                " proyecto_estatus_meta =  1 AND cpr_id_proyecto = ".$idProyecto." ORDER BY( proyecto_desc_meta )";
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $idMeta = $rs[strtoupper("proyecto_id_meta")];
            $idProy = $rs[strtoupper("cpr_id_proyecto")];
            $desc = $rs[strtoupper("proyecto_desc_meta")];
            $idProyectoInst=$rs[strtoupper("cpi_id_programa")];
            $idProyectoOp=$rs[strtoupper("cpo_id_programa")];
            $programaInstitucional = $rs[strtoupper("cpi_programa")];
            $programaOperativo = $rs[strtoupper("cpo_programa")];

            $elemento = new ProyectoMeta();
            $elemento ->constructor2($idMeta,$idProy,$desc,$idProyectoInst,$idProyectoOp,$programaInstitucional, $programaOperativo);
            array_push($lista, $elemento);
				 
        }
        return $lista;
    }
        
    public function obtenerListado2(){
	$lista =  array();
        $query = "SELECT m.*, p.cpr_descripcion FROM sie_proyecto_metas m, sie_cat_proyectos p, sie_cat_programa_institucional pi, sie_cat_programa_operativo po " .
                "WHERE m.cpr_id_proyecto = p.cpr_id_proyecto AND m.cpi_id_programa=pi.cpi_id_programa AND m.cpo_id_programa=po.cpo_id_programa AND m.proyecto_estatus_meta =  1 AND p.cpr_estatus='1'   " .
                "ORDER BY( proyecto_desc_meta ) ";
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $idMeta = $rs[strtoupper("proyecto_id_meta")];
            $idProy = $rs[strtoupper("cpr_id_proyecto")];
            $desc = $rs[strtoupper("proyecto_desc_meta")];
            $idProyectoInst=$rs[strtoupper("cpi_id_programa")];
            $idProyectoOp=$rs[strtoupper("cpo_id_programa")];
            $programaInstitucional = $rs[strtoupper("proyecto_estatus_meta")];
            $programaOperativo = $rs["cpr_descripcion"];

            $elemento = new ProyectoMeta();
            $elemento->constructor2($idMeta,$idProy,$desc,$idProyectoInst,$idProyectoOp,$programaInstitucional, $programaOperativo);
            array_push($lista, $elemento);
				 
        }
        return $lista;
    }   
    
    public function obtieneElemento($idElemento){
        $elemento  =  null;
        $query = "SELECT * FROM sie_proyecto_metas m,sie_cat_programa_institucional pi, sie_cat_programa_operativo po " .
                        " WHERE m.cpi_id_programa=pi.cpi_id_programa AND m.cpo_id_programa=po.cpo_id_programa AND proyecto_id_meta = ".$idElemento;
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $idMeta = $rs[strtoupper("proyecto_id_meta")];
            $idProy = $rs[strtoupper("cpr_id_proyecto")];
            $desc = $rs[strtoupper("proyecto_desc_meta")];
            $idProyectoInst=$rs[strtoupper("cpi_id_programa")];
            $idProyectoOp=$rs[strtoupper("cpo_id_programa")];
            $programaInstitucional = $rs[strtoupper("cpi_programa")];
            $programaOperativo = $rs[strtoupper("cpo_programa")];

            $elemento = new ProyectoMeta();
            $elemento ->constructor2($idMeta,$idProy,$desc,$idProyectoInst,$idProyectoOp,$programaInstitucional, $programaOperativo);
        }
        return $elemento;
    }
    
    public function guardaElemento($elemento){
	$con = new Catalogo();
	$des =  null;
	$query = "INSERT INTO sie_proyecto_metas (PROYECTO_DESC_META, CPR_ID_PROYECTO, PROYECTO_ESTATUS_META, CPI_ID_PROGRAMA, CPO_ID_PROGRAMA) VALUES('".$elemento->getDescripcion()."',".$elemento->getIdProyecto().",1,".$elemento->getIdProgramaInstitucional().",".$elemento->getIdProgramaOperativo().")";
	$res = $con->obtenerLista($query);
	        
	if($res=="1"){	
	    $query = "SELECT cpr_descripcion FROM sie_cat_proyectos WHERE cpr_id_proyecto = ".$elemento->getIdProyecto();
	    $result  = $con->obtenerLista($query);
            if($rs = mysql_fetch_array($result)){
                $des = $rs["cpr_descripcion"];
            }
	}

        return $des;
    }
    
    public function actualizaElemento($elemento){
	$con = new Catalogo();
	$des =  null;
	$query = "UPDATE sie_proyecto_metas SET proyecto_desc_meta = '".$elemento->getDescripcion()."', CPI_ID_PROGRAMA=".$elemento->getIdProgramaInstitucional().", CPO_ID_PROGRAMA=".$elemento->getIdProgramaOperativo()."   WHERE proyecto_id_meta = ".$elemento->getIdMeta()."";
	$res = $con->obtenerLista($query);
	    
	if($res=="1"){	
	    $query = "SELECT cpr_descripcion FROM sie_cat_proyectos WHERE cpr_id_proyecto = ".$elemento->getIdProyecto();
	    $result  = $con->obtenerLista($query);
            if($rs = mysql_fetch_array($result)){
                $des = $rs["cpr_descripcion"];
            }
	}

        return $des;
    }
    
    public function eliminaElemento($idElemento){
	$con = new Catalogo();
	$query = "UPDATE sie_proyecto_metas SET proyecto_estatus_meta = 0 WHERE proyecto_id_meta =  ".$idElemento;
	$res = $con->obtenerLista($query);
	    
	if($res == "1"){return true;}
        else{ return false; }
    }
}
