<?php

class DetComprobacion {
    private $idComprobante;
    private $idComprobacion;
    private $tipo;
    private $folio;
    private $fecha;
    private $idPartida;
    private $monto;
    private $descripcion;
    private $url;
    private $idProveedor;
    
    function setAll($idComprobante, $idComprobacion, $tipo, $folio, $fecha, $idPartida, $monto, $descripcion, $url, $idProveedor) {
        $this->idComprobante = $idComprobante;
        $this->idComprobacion = $idComprobacion;
        $this->tipo = $tipo;
        $this->folio = $folio;
        $this->fecha = $fecha;
        $this->idPartida = $idPartida;
        $this->monto = $monto;
        $this->descripcion = $descripcion;
        $this->url = $url;
        $this->idProveedor = $idProveedor;
    }

    function __construct() {
        
    }
    
    public function getIdComprobante() {
        return $this->idComprobante;
    }

    public function getIdComprobacion() {
        return $this->idComprobacion;
    }

    public function getTipo() {
        return $this->tipo;
    }

    public function getFolio() {
        return $this->folio;
    }

    public function getFecha() {
        return $this->fecha;
    }

    public function getIdPartida() {
        return $this->idPartida;
    }

    public function getMonto() {
        return $this->monto;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function getUrl() {
        return $this->url;
    }

    public function getIdProveedor() {
        return $this->idProveedor;
    }

    public function setIdComprobante($idComprobante) {
        $this->idComprobante = $idComprobante;
    }

    public function setIdComprobacion($idComprobacion) {
        $this->idComprobacion = $idComprobacion;
    }

    public function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    public function setFolio($folio) {
        $this->folio = $folio;
    }

    public function setFecha($fecha) {
        $this->fecha = $fecha;
    }

    public function setIdPartida($idPartida) {
        $this->idPartida = $idPartida;
    }

    public function setMonto($monto) {
        $this->monto = $monto;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function setUrl($url) {
        $this->url = $url;
    }

    public function setIdProveedor($idProveedor) {
        $this->idProveedor = $idProveedor;
    }


}
