<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Empleado.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class EmpleadoDaoJdbc {
    
    public function obtieneListado() {
		
	$lista= array();
		
	$query="SELECT * FROM cat_empleado WHERE CEM_ESTATUS=1 order by cem_appaterno,cem_apmaterno,cem_nombre ";
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs["cem_id_empleado"];
            $nombre= $rs[strtoupper("cem_nombre")];
            $apaterno= $rs[strtoupper("cem_appaterno")];
            $amaterno= $rs[strtoupper("cem_apmaterno")];
            $sueldoNeto= $rs[strtoupper("cem_sueldo_neto")];
            $sueldoLetra= $rs[strtoupper("cem_sueldo_letra")];
            $puesto= $rs[strtoupper("cem_puesto")];
            $rfc= $rs[strtoupper("cem_rfc")];
            $tipoNomina= $rs[strtoupper("cem_tipo_nomina")];
            $numero= $rs[strtoupper("cem_numero_emp")];

            $elemento = new Empleado();
            $elemento->setAll($id,$nombre,$apaterno,$amaterno,$sueldoNeto,$sueldoLetra,$puesto,$rfc,$tipoNomina,$numero);
            array_push($lista, $elemento);
        }	
        return $lista;
    }
    
    public function obtieneListadoTipoNomina($tipoNominaEmp){
        
        $lista= array();
		
	$query="SELECT * FROM cat_empleado WHERE CEM_ESTATUS=1 and cem_tipo_nomina='".$tipoNominaEmp."' order by cem_appaterno,cem_apmaterno,cem_nombre ";
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs["cem_id_empleado"];
            $nombre= $rs[strtoupper("cem_nombre")];
            $apaterno= $rs[strtoupper("cem_appaterno")];
            $amaterno= $rs[strtoupper("cem_apmaterno")];
            $sueldoNeto= $rs[strtoupper("cem_sueldo_neto")];
            $sueldoLetra= $rs[strtoupper("cem_sueldo_letra")];
            $puesto= $rs[strtoupper("cem_puesto")];
            $rfc= $rs[strtoupper("cem_rfc")];
            $tipoNomina= $rs[strtoupper("cem_tipo_nomina")];
            $numero= $rs[strtoupper("cem_numero_emp")];

            $elemento = new Empleado();
            $elemento->setAll($id,$nombre,$apaterno,$amaterno,$sueldoNeto,$sueldoLetra,$puesto,$rfc,$tipoNomina,$numero);
            array_push($lista, $elemento);
        }	
        return $lista;
    }
    
    public function obtieneElemento($idElemento){
        
        $elemento = new Empleado();
        
        $query="SELECT * FROM cat_empleado WHERE cem_id_empleado=".$idElemento." ";
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs["cem_id_empleado"];
            $nombre= $rs[strtoupper("cem_nombre")];
            $apaterno= $rs[strtoupper("cem_appaterno")];
            $amaterno= $rs[strtoupper("cem_apmaterno")];
            $sueldoNeto= $rs[strtoupper("cem_sueldo_neto")];
            $sueldoLetra= $rs[strtoupper("cem_sueldo_letra")];
            $puesto= $rs[strtoupper("cem_puesto")];
            $rfc= $rs[strtoupper("cem_rfc")];
            $tipoNomina= $rs[strtoupper("cem_tipo_nomina")];
            $numero= $rs[strtoupper("cem_numero_emp")];

            $elemento->setAll($id,$nombre,$apaterno,$amaterno,$sueldoNeto,$sueldoLetra,$puesto,$rfc,$tipoNomina,$numero);
        }	
        return $elemento;
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        if($elemento->getNumero() == null){
            $mensaje = "null";
        }else{
            $mensaje = $elemento->getNumero();
        }
        $query="INSERT INTO cat_empleado(cem_nombre,cem_appaterno,cem_apmaterno,cem_sueldo_neto,cem_sueldo_letra,cem_puesto,cem_rfc,cem_tipo_nomina,cem_numero_emp) " .
	       "VALUES ('".$elemento->getNombre()."', '".$elemento->getApPaterno()."', '".$elemento->getApMaterno()."', ".$elemento->getSueldoNeto().",'".$elemento->getSueldoLetra()."', '".$elemento->getPuesto()."', '".$elemento->getRfc()."', '".$elemento->getTipoNomina()."', ".$mensaje.")";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
        if($elemento->getNumero() == null){
            $mensaje = "null";
        }else{
            $mensaje = $elemento->getNumero();
        }
	$query="UPDATE cat_empleado set cem_nombre='".$elemento->getNombre()."', cem_appaterno='".$elemento->getApPaterno()."', cem_apmaterno='".$elemento->getApMaterno()."', cem_sueldo_neto=".$elemento->getSueldoNeto().",cem_sueldo_letra='".$elemento->getSueldoLetra()."', cem_puesto='".$elemento->getPuesto()."',cem_rfc='".$elemento->getRfc()."',cem_tipo_nomina='".$elemento->getTipoNomina()."' ,cem_numero_emp=".$mensaje."  " .
				"WHERE cem_id_empleado=".$elemento->getId();
	$res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE cat_empleado set  cem_estatus=0 WHERE cem_id_empleado=".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}
