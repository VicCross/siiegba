<?php

class MovimientoBanco {
    private $id;
    private $cuenta;
    private $numeroOperacion;
    private $cargo;
    private $abono;
    private $fecha;
    private $destino;
    private $referencia;
    private $proyecto;
    private $formaCarga;
    private $periodo;
    private $idCheque;
    
    function setAll($id, $cuenta, $numeroOperacion, $cargo, $abono, $fecha, $destino, $referencia, $proyecto, $formaCarga, $periodo, $idCheque) {
        $this->id = $id;
        $this->cuenta = $cuenta;
        $this->numeroOperacion = $numeroOperacion;
        $this->cargo = $cargo;
        $this->abono = $abono;
        $this->fecha = $fecha;
        $this->destino = $destino;
        $this->referencia = $referencia;
        $this->proyecto = $proyecto;
        $this->formaCarga = $formaCarga;
        $this->periodo = $periodo;
        $this->idCheque = $idCheque;
    }

    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getCuenta() {
        return $this->cuenta;
    }

    public function getNumeroOperacion() {
        return $this->numeroOperacion;
    }

    public function getCargo() {
        return $this->cargo;
    }

    public function getAbono() {
        return $this->abono;
    }

    public function getFecha() {
        return $this->fecha;
    }

    public function getDestino() {
        return $this->destino;
    }

    public function getReferencia() {
        return $this->referencia;
    }

    public function getProyecto() {
        return $this->proyecto;
    }

    public function getFormaCarga() {
        return $this->formaCarga;
    }

    public function getPeriodo() {
        return $this->periodo;
    }

    public function getIdCheque() {
        return $this->idCheque;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setCuenta($cuenta) {
        $this->cuenta = $cuenta;
    }

    public function setNumeroOperacion($numeroOperacion) {
        $this->numeroOperacion = $numeroOperacion;
    }

    public function setCargo($cargo) {
        $this->cargo = $cargo;
    }

    public function setAbono($abono) {
        $this->abono = $abono;
    }

    public function setFecha($fecha) {
        $this->fecha = $fecha;
    }

    public function setDestino($destino) {
        $this->destino = $destino;
    }

    public function setReferencia($referencia) {
        $this->referencia = $referencia;
    }

    public function setProyecto($proyecto) {
        $this->proyecto = $proyecto;
    }

    public function setFormaCarga($formaCarga) {
        $this->formaCarga = $formaCarga;
    }

    public function setPeriodo($periodo) {
        $this->periodo = $periodo;
    }

    public function setIdCheque($idCheque) {
        $this->idCheque = $idCheque;
    }


}
