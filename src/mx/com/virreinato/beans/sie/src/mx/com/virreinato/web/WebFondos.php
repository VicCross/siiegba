<?php

session_start();
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/FondosDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Fondos.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
if(isset($_POST['inicio'])){
    $inicio = $_POST["inicio"];
    $fin = $_POST["fin"];
    $proyecto = $_POST["proyecto"];
    $periodo = $_POST["periodo"];
    
    header("Location: ../../../../../Proyectos/lista_Fondos.php?inicio=".$inicio."&fin=".$fin."&proyecto=".$proyecto."&periodo=".$periodo);
}else if (isset($_POST['txtNumSolicitud'])){
    $elemento = new Fondos();
    $elemento->setNumsol($_POST["txtNumSolicitud"]);
    $elemento->setIdCCosto((int)($_POST["Centro"]));
    $elemento->setTipo((int)($_POST["Tipo"]));
    $elemento->setDescripcion($_POST["txtDescripcion"]);
    $elemento->setTitular($_POST["txtTitular"]);
    $elemento->setMesCalendario($_POST["mesFondos"]);

    if ($_POST["txtFecha"] != null && $_POST["txtFecha"] != "") {
        $fechaConFormato = date("Y-m-d",strtotime($_POST["txtFecha"]));
        $elemento->setFecha($fechaConFormato);
    }


    if( $_POST["periodo"] != null )  $elemento->setIdPeriodo((int)($_POST["periodo"]));
    if( $_POST["Proyecto"]!= null ) $elemento->setIdProyecto((int)($_POST["Proyecto"]));
    
    if ($_POST["id"] != null){
        $elemento->setId((int)($_POST["id"]));
        $dao=new FondosDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new FondosDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
    header("Location: ../../../../../Proyectos/lista_Fondos.php?respuesta=" . $respuesta."&periodo=".$elemento->getIdPeriodo());
}else if (isset($_GET['id'])){
    $dao=new FondosDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
    header("Location: ../../../../../Proyectos/lista_Fondos.php?respuesta=" . $respuesta);
} else {
    $respuesta = "No se detecto la acción a realizar.";
    header("Location: ../../../../../Proyectos/lista_Fondos.php?respuesta=" . $respuesta);
}
?>

