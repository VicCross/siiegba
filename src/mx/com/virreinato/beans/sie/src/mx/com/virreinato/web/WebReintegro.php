<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/ReintegroDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Reintegro.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
if(isset($_POST['inicio'])){
    $inicio = $_POST["inicio"];
    $fin = $_POST["fin"];
    $periodo = $_POST["periodo"];
    
    header("Location: ../../../../../Proyectos/lista_Reintegro.php?inicio=".$inicio."&fin=".$fin."&periodo=".$periodo);
}else if(isset($_POST['refNumerica'])){
    $elemento = new Reintegro();
    $elemento->setRefNumerica($_POST["refNumerica"]);

    if (isset($_POST['fecha']) && $_POST['fecha']!="") {
        $fechaConFormato = date("Y-m-d",strtotime($_POST["fecha"]));
        $elemento->setFecha($fechaConFormato);
    }// if

    $elemento->setFolio($_POST["folio"]);
    $elemento->setHonorarios((double)($_POST["honorarios"]));
    $elemento->setIdPersonal((int)($_POST["idPersonal"]));
    $elemento->setPeriodo((int)($_POST["periodo"]));
    if (isset($_POST['id'])){
        $elemento->setId((int)($_POST["id"]));
        $dao=new ReintegroDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new ReintegroDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
    header("Location: ../../../../../Proyectos/lista_Reintegro.php?respuesta=" . $respuesta);
}else if (isset($_GET['id'])){
    $dao=new ReintegroDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
    header("Location: ../../../../../Proyectos/lista_Reintegro.php?respuesta=" . $respuesta);
} else {
    $respuesta = "No se detecto la acción a realizar.";
    header("Location: ../../../../../Proyectos/lista_Reintegro.php?respuesta=" . $respuesta);
}
?>
