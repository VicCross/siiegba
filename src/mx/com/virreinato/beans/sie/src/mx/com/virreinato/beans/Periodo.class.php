<?php

class Periodo {
    private $id;
    private $periodo;
    private $cerrado;
    private $fechaInicial;
    private $fechaFinal;
    
    function setAll($id, $periodo, $cerrado, $fechaInicial, $fechaFinal) {
        $this->id = $id;
        $this->periodo = $periodo;
        $this->cerrado = $cerrado;
        $this->fechaInicial = $fechaInicial;
        $this->fechaFinal = $fechaFinal;
    }
    
    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getPeriodo() {
        return $this->periodo;
    }

    public function getCerrado() {
        return $this->cerrado;
    }

    public function getFechaInicial() {
        return $this->fechaInicial;
    }

    public function getFechaFinal() {
        return $this->fechaFinal;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setPeriodo($periodo) {
        $this->periodo = $periodo;
    }

    public function setCerrado($cerrado) {
        $this->cerrado = $cerrado;
    }

    public function setFechaInicial($fechaInicial) {
        $this->fechaInicial = $fechaInicial;
    }

    public function setFechaFinal($fechaFinal) {
        $this->fechaFinal = $fechaFinal;
    }

}
