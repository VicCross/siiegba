<?php

class PresupuestoAsignadoGb {
    private $id;
    private $idPeriodo;
    private $montoEne;
    private $montoFeb;
    private $montoMar;
    private $montoAbr;
    private $montoMay;
    private $montoJun;
    private $montoJul;
    private $montoAgo;
    private $montoSep;
    private $montoOct;
    private $montoNov;
    private $montoDic;
    private $destino;
    
    function setAll($id, $idPeriodo, $montoEne, $montoFeb, $montoMar, $montoAbr, $montoMay, $montoJun, $montoJul, $montoAgo, $montoSep, $montoOct, $montoNov, $montoDic, $destino) {
        $this->id = $id;
        $this->idPeriodo = $idPeriodo;
        $this->montoEne = $montoEne;
        $this->montoFeb = $montoFeb;
        $this->montoMar = $montoMar;
        $this->montoAbr = $montoAbr;
        $this->montoMay = $montoMay;
        $this->montoJun = $montoJun;
        $this->montoJul = $montoJul;
        $this->montoAgo = $montoAgo;
        $this->montoSep = $montoSep;
        $this->montoOct = $montoOct;
        $this->montoNov = $montoNov;
        $this->montoDic = $montoDic;
        $this->destino = $destino;
    }

    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getIdPeriodo() {
        return $this->idPeriodo;
    }

    public function getMontoEne() {
        return $this->montoEne;
    }

    public function getMontoFeb() {
        return $this->montoFeb;
    }

    public function getMontoMar() {
        return $this->montoMar;
    }

    public function getMontoAbr() {
        return $this->montoAbr;
    }

    public function getMontoMay() {
        return $this->montoMay;
    }

    public function getMontoJun() {
        return $this->montoJun;
    }

    public function getMontoJul() {
        return $this->montoJul;
    }

    public function getMontoAgo() {
        return $this->montoAgo;
    }

    public function getMontoSep() {
        return $this->montoSep;
    }

    public function getMontoOct() {
        return $this->montoOct;
    }

    public function getMontoNov() {
        return $this->montoNov;
    }

    public function getMontoDic() {
        return $this->montoDic;
    }

    public function getDestino() {
        return $this->destino;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setIdPeriodo($idPeriodo) {
        $this->idPeriodo = $idPeriodo;
    }

    public function setMontoEne($montoEne) {
        $this->montoEne = $montoEne;
    }

    public function setMontoFeb($montoFeb) {
        $this->montoFeb = $montoFeb;
    }

    public function setMontoMar($montoMar) {
        $this->montoMar = $montoMar;
    }

    public function setMontoAbr($montoAbr) {
        $this->montoAbr = $montoAbr;
    }

    public function setMontoMay($montoMay) {
        $this->montoMay = $montoMay;
    }

    public function setMontoJun($montoJun) {
        $this->montoJun = $montoJun;
    }

    public function setMontoJul($montoJul) {
        $this->montoJul = $montoJul;
    }

    public function setMontoAgo($montoAgo) {
        $this->montoAgo = $montoAgo;
    }

    public function setMontoSep($montoSep) {
        $this->montoSep = $montoSep;
    }

    public function setMontoOct($montoOct) {
        $this->montoOct = $montoOct;
    }

    public function setMontoNov($montoNov) {
        $this->montoNov = $montoNov;
    }

    public function setMontoDic($montoDic) {
        $this->montoDic = $montoDic;
    }

    public function setDestino($destino) {
        $this->destino = $destino;
    }


}
