<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Parametro.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class ParametroDaoJdbc {
    
    private $valor;
    
    public function obtieneListado() {

        $lista= array();

        $query="SELECT * FROM sie_parametros WHERE cpa_estatus=1 ORDER BY cpa_parametro";
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cpa_id_parametro")];
            $parametro= $rs[strtoupper("cpa_parametro")];
            $valor = $rs[strtoupper("cpa_valor")];
            $this->valor = $valor;
            $elemento= new Parametro();
            $elemento->setAll($id, $parametro, $valor);
            array_push($lista,$elemento);
        }	
        return $lista;
    }
    
    public function obtieneElemento($idElemento) {				
	$elemento=new Parametro();
		
	$query="SELECT * FROM sie_parametros WHERE cpa_id_parametro=".$idElemento;
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cpa_id_parametro")];
            $parametro= $rs[strtoupper("cpa_parametro")];
            $valor= $rs[strtoupper("cpa_valor")];
            $this->valor = $valor;
            $elemento= new Parametro();
            $elemento->setAll($id, $parametro, $valor);
        }	
        return $elemento;		
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $query="INSERT INTO sie_parametros(cpa_parametro,cpa_valor) VALUES ('".$elemento->getParametro()."', '".$elemento->getValor()."')";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
	$query="UPDATE sie_parametros set cpa_valor='".$elemento->getValor()."', cpa_parametro='".$elemento->getParametro()."' WHERE cpa_id_parametro=".$elemento->getId();
	$res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_parametros set  cpa_estatus=0 WHERE cpa_id_parametro=".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function getValor() {
        return $this->valor;
    }


}
