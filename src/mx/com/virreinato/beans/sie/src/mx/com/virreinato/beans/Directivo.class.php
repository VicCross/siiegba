<?php

class Directivo {
    private $id;
    private $nombre;
    private $apPaterno;
    private $apMaterno;
    private $cargo;
    private $tipoCargo;
    private $ccostos;
    private $iniciales;
    
    function __construct() {
        
    }
    
    public function setAll($id, $nombre, $apPaterno, $apMaterno, $cargo, $tipoCargo, $ccostos, $iniciales){
        $this->id = $id;
        $this->nombre = $nombre;
        $this->apPaterno = $apPaterno;
        $this->apMaterno = $apMaterno;
        $this->cargo = $cargo;
        $this->tipoCargo = $tipoCargo;
        $this->ccostos = $ccostos;
        $this->iniciales = $iniciales;
    }

    
    public function getId() {
        return $this->id;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function getApPaterno() {
        return $this->apPaterno;
    }

    public function getApMaterno() {
        return $this->apMaterno;
    }

    public function getCargo() {
        return $this->cargo;
    }

    public function getTipoCargo() {
        return $this->tipoCargo;
    }

    public function getCcostos() {
        return $this->ccostos;
    }

    public function getIniciales() {
        return $this->iniciales;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function setApPaterno($apPaterno) {
        $this->apPaterno = $apPaterno;
    }

    public function setApMaterno($apMaterno) {
        $this->apMaterno = $apMaterno;
    }

    public function setCargo($cargo) {
        $this->cargo = $cargo;
    }

    public function setTipoCargo($tipoCargo) {
        $this->tipoCargo = $tipoCargo;
    }

    public function setCcostos($ccostos) {
        $this->ccostos = $ccostos;
    }

    public function setIniciales($iniciales) {
        $this->iniciales = $iniciales;
    }


}
