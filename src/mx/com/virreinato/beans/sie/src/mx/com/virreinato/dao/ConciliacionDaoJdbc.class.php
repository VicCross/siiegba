<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Parametro.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class ConciliacionDaoJdbc {
    
    public function obtieneElemento($mes, $id_periodo) {
		
	$elemento=new Conciliacion();
		
	$query="SELECT * FROM sie_conciliacion_bancaria WHERE cob_mes=".$mes." and cpe_id_periodo=".$id_periodo." and cob_estatus=1";
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
										
            $elemento->setId($rs[strtoupper("cob_id_conciliacion")]);
            $elemento->setMes($rs[strtoupper("cob_mes")]);
            $elemento->setIdPeriodo($rs[strtoupper("cpe_id_periodo")]);
            $elemento->setSaldoInicial($rs[strtoupper("cob_saldo_ini")]);

            $elemento->setIngresosMuseo($rs[strtoupper("cob_total_ingresos_m")]);
            $elemento->setEgresosMuseo($rs[strtoupper("cob_total_egresos_m")]);
            $elemento->setSaldoFinalMuseo($rs[strtoupper("cob_saldo_fin_m")]);

            $elemento->setIngresosBanco($rs[strtoupper("cob_total_ingresos_b")]);
            $elemento->setEgresosBanco($rs[strtoupper("cob_total_egresos_b")]);
            $elemento->setSaldoFinalBanco($rs[strtoupper("cob_saldo_fin_b")]);

            $elemento->setDiferencia($rs[strtoupper("cob_diferencia_conciliacion")]);
            $elemento->setFechaConciliacion($rs[strtoupper("cob_fecha_conciliacion")]);
            $elemento->setObservaciones($rs[strtoupper("cob_observaciones")]);
            $elemento->setStatus($rs[strtoupper("cob_estatus")]);		
				
        }
		
        return $elemento;
		
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $query="INSERT INTO sie_conciliacion_bancaria(cob_mes,cpe_id_periodo,cob_saldo_ini,cob_total_ingresos_m, cob_total_egresos_m, cob_saldo_fin_m, cob_total_ingresos_b, cob_total_egresos_b, cob_saldo_fin_b, cob_diferencia_conciliacion, cob_fecha_conciliacion, cob_observaciones, cob_estatus) VALUES (".$elemento->getMes().", ".$elemento->getIdPeriodo().", ".$elemento->getSaldoInicial()." , ".$elemento->getIngresosMuseo()." , ".$elemento->getEgresosMuseo()." , ".$elemento->getSaldoFinalMuseo()." , ".$elemento->getIngresosBanco()." , ".$elemento->getEgresosBanco()." , ".$elemento->getSaldoFinalBanco()." , ".$elemento->getDiferencia()." , '".date("Y-m-d")."' , '".$elemento->getObservaciones()."', '".$elemento->getStatus()."')";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
}
