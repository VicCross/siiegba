<?php

class Fondos {
    private $id;
    private $numSol;
    private $fecha;
    private $idCCosto;
    private $tipo;
    private $idProyecto;
    private $idPeriodo;
    private $descripcion;
    private $titular;
    private $estatus;
    private $desCCosto;
    private $desProyecto;
    private $desPeriodo;
    private $mesCalendario;
    
    function constructor($id, $numSol, $descripcion) {
        $this->id = $id;
        $this->numSol = $numSol;
        $this->descripcion = $descripcion;
    }
    
    function setAll($id, $numSol, $fecha, $idCCosto, $tipo, $idProyecto, $idPeriodo, $descripcion, $titular, $estatus, $desCCosto, $desProyecto, $desPeriodo, $mesCalendario) {
        $this->id = $id;
        $this->numSol = $numSol;
        $this->fecha = $fecha;
        $this->idCCosto = $idCCosto;
        $this->tipo = $tipo;
        $this->idProyecto = $idProyecto;
        $this->idPeriodo = $idPeriodo;
        $this->descripcion = $descripcion;
        $this->titular = $titular;
        $this->estatus = $estatus;
        $this->desCCosto = $desCCosto;
        $this->desProyecto = $desProyecto;
        $this->desPeriodo = $desPeriodo;
        $this->mesCalendario = $mesCalendario;
    }
    
    function __construct() {
        
    }

        public function getId() {
        return $this->id;
    }

    public function getNumSol() {
        return $this->numSol;
    }

    public function getFecha() {
        return $this->fecha;
    }

    public function getIdCCosto() {
        return $this->idCCosto;
    }

    public function getTipo() {
        return $this->tipo;
    }

    public function getIdProyecto() {
        return $this->idProyecto;
    }

    public function getIdPeriodo() {
        return $this->idPeriodo;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function getTitular() {
        return $this->titular;
    }

    public function getEstatus() {
        return $this->estatus;
    }

    public function getDesCCosto() {
        return $this->desCCosto;
    }

    public function getDesProyecto() {
        return $this->desProyecto;
    }

    public function getDesPeriodo() {
        return $this->desPeriodo;
    }

    public function getMesCalendario() {
        return $this->mesCalendario;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setNumSol($numSol) {
        $this->numSol = $numSol;
    }

    public function setFecha($fecha) {
        $this->fecha = $fecha;
    }

    public function setIdCCosto($idCCosto) {
        $this->idCCosto = $idCCosto;
    }

    public function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    public function setIdProyecto($idProyecto) {
        $this->idProyecto = $idProyecto;
    }

    public function setIdPeriodo($idPeriodo) {
        $this->idPeriodo = $idPeriodo;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function setTitular($titular) {
        $this->titular = $titular;
    }

    public function setEstatus($estatus) {
        $this->estatus = $estatus;
    }

    public function setDesCCosto($desCCosto) {
        $this->desCCosto = $desCCosto;
    }

    public function setDesProyecto($desProyecto) {
        $this->desProyecto = $desProyecto;
    }

    public function setDesPeriodo($desPeriodo) {
        $this->desPeriodo = $desPeriodo;
    }

    public function setMesCalendario($mesCalendario) {
        $this->mesCalendario = $mesCalendario;
    }

    public function getNumMesCalendario(){
            $resul = 0;

            if( strcmp($this->mesCalendario,"trdc_ene" )) { $resul = 1; }
            if( strcmp($this->mesCalendario,"trdc_feb")) { $resul = 2;}
            if( strcmp($this->mesCalendario,"trdc_mar")) { $resul = 3;}
            if( strcmp($this->mesCalendario,"trdc_abr")) { $resul = 4;}
            if( strcmp($this->mesCalendario,"trdc_may")) { $resul = 5;}
            if( strcmp($this->mesCalendario,"trdc_jun")) { $resul = 6;}
            if( strcmp($this->mesCalendario,"trdc_jul")) { $resul = 7;}
            if( strcmp($this->mesCalendario,"trdc_ago")) { $resul = 8;}
            if( strcmp($this->mesCalendario,"trdc_sep")) { $resul = 9;}
            if( strcmp($this->mesCalendario,"trdc_oct")) { $resul = 10;}
            if( strcmp($this->mesCalendario,"trdc_nov")) { $resul = 11;}
            if( strcmp($this->mesCalendario,"trdc_dic")) { $resul = 12;}

            return $resul;
    }
}
