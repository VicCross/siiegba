<?php

class FondosDet {
    private $idFondo;
    private $idPartida;
    private $desPartida;
    private $monto;
    
    function setAll($idFondo, $idPartida, $desPartida, $monto) {
        $this->idFondo = $idFondo;
        $this->idPartida = $idPartida;
        $this->desPartida = $desPartida;
        $this->monto = $monto;
    }
    
    function __construct() {
        
    }

    public function getIdFondo() {
        return $this->idFondo;
    }

    public function getIdPartida() {
        return $this->idPartida;
    }

    public function getDesPartida() {
        return $this->desPartida;
    }

    public function getMonto() {
        return $this->monto;
    }

    public function setIdFondo($idFondo) {
        $this->idFondo = $idFondo;
    }

    public function setIdPartida($idPartida) {
        $this->idPartida = $idPartida;
    }

    public function setDesPartida($desPartida) {
        $this->desPartida = $desPartida;
    }

    public function setMonto($monto) {
        $this->monto = $monto;
    }

}
