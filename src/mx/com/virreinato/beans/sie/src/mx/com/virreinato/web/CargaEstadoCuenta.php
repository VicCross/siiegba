<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/ConciliacionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Conciliacion.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/CuentaBancariaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CuentaBancaria.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/MovimientoBancoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/MovimientoBanco.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/PHPExcel.php");

if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
if(isset($_POST['id_cuenta'])){
    
    //Declaramos un array de meses, ya que del documento de Excel vienen con este formato y lo necesitamos númerico para la BD
    $meses = array("ene","feb","mar","abr","may","jun","jul","ago","sep","oct","nov","dic");
    $nmeses = array("01","02","03","04","05","06","07","08","09","10","11","12");
    $i = 0;
    $nfecha = "";
    //Con esto se mueve el archivo temporal a la carpeta especificada, con el nombre dado por el usuario a través
    //de $_FILES['file']['name']
    move_uploaded_file($_FILES['file']['tmp_name'],$_FILES['file']['name']);
    
    $aux = 0;
    $fechafin = $fechaini = null;
    
    $cuenta=new CuentaBancaria();
    $cuentadao=new CuentaBancariaDaoJdbc();
    $cuenta=$cuentadao->obtieneElemento($_POST['id_cuenta']);

    $periodo=new Periodo();
    $periododao=new PeriodoDaoJdbc();
    $periodo=$periododao->obtieneElemento($_POST['id_periodo']);
    $conciliaciondao=new ConciliacionDaoJdbc();
    
    //Abrimos el archivo que recien ingreso el usuario
    $inputFileName = $_FILES['file']['name'];
    $inputFileType = 'CSV';
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
    
    //Obtenemos en un array todas las celdas activas del excel
    $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true); 
    $result = false;
    
    foreach($sheetData as $columna){
        if($columna['A'] == "FECHA" || $columna['B'] == "Total:"){
            //En la primera fila solo se encuentran los títulos de la columnas, por lo tanto la ignoramos al igual que la última
            continue;
        }else{
            //Si no es la primera fila entonces entramos y obtenemos los datos de las columnas que nos interesan
            $fecha = $columna['A'];
            $fecha = strtolower($fecha);
            $fecha = explode("-",$fecha);
            for($i = 0; $i<12; $i++){
                if($meses[$i] == $fecha[1]){
                    $nfecha = $fecha[0]."-".$nmeses[$i]."-20".$fecha[2];
                    continue;
                }
            }
            $descripcion = $columna['B'];
            $depositos = $columna['C'];
            $retiros = $columna['D'];
            
            if($fecha!=null && $fecha!=""){
                //creo el elemento
                $elemento=new MovimientoBanco();
                
                if($depositos!=null && $depositos!=""){
                    //le quito las comas y singos de pesos
                    $depositos = str_replace(",", "", $depositos);
                    $depositos = str_replace("\\$", "", $depositos);
                    $elemento->setAbono((double)($depositos));
								
                }
                else
                {        $elemento->setAbono(0.0);    }
                
                if( $retiros!=null && $retiros!=""){
                    //le quito las comas y signos de pesos
                    $retiros = str_replace(",", "", $retiros);
                    $retiros = str_replace("\\$", "", $retiros);
                    $elemento->setCargo((double)($retiros));
                }
                else
                {    $elemento->setCargo(0.0);    }
                
                /*$auxiliar = explode("-", $nfecha);
                $nfecha= $auxiliar[2]."-".$auxiliar[1]."-".$auxiliar[0];*/
                $elemento->setFecha($nfecha);
                
                if($aux == 0){
                    $fechafin = $nfecha;
                }
                
                $fechaini = $nfecha;
                $elemento->setReferencia($descripcion);
                $elemento->setFormaCarga("Archivo");

                $elemento->setDestino(null);
                $elemento->setNumeroOperacion(null);

                $elemento->setCuenta($cuenta);
                $elemento->setPeriodo($periodo);

                $dao=new MovimientoBancoDaoJdbc();
                $res = false;
                $conciliacion=$conciliaciondao->obtieneElemento((int)(date("m",strtotime($nfecha))),$periodo->getId());
							
                if($conciliacion!=null && $conciliacion->getId()!=null)
                {        /*echo("mes conciliado"); */ }
                else
                {    $res=$dao->guardaElemento($elemento);  }
                
                if($res == false)
                {    $result=true;  }
            }
        }   
        $aux++;
    }
    
    if($result)
    {    $respuesta = "Ocurrio un error al almacenar la información, es posible que no se hayan importado todos los movimientos.";  }
    else
    {    $respuesta = "Su información se almacenó exitosamente.";   }
    
    $continuar = "Continuar";
    $finicio = date("d-m-Y",strtotime($fechaini));
    $ffin = date("d-m-Y",strtotime($fechafin));
    $mes = date("m",strtotime($fechaini));
    $id_periodo = (String)($periodo->getId());
    
    //verifico si esta conciliado
    $conciliacion=$conciliaciondao->obtieneElemento((int)(date("m",strtotime($fechaini))),$periodo->getId());

    if($conciliacion!=null && $conciliacion->getId()!=null)
    {    $conciliado = "1"; }
    else
    {    $conciliado= "0";  }

    header("Location: ../../../../../bancos/lista_movimiento_banco.php?finicio=" . $ffin."&ffin=".$finicio."&mes=".$mes."&id_periodo=".$id_periodo."&conciliado=".$conciliado."&respuesta=".$respuesta);
}