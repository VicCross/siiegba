<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");

class PeriodoDaoJdbc {
    
    public function obtieneListado() {
		
        $lista= array();

        $query="SELECT * FROM sie_cat_periodos WHERE cpe_estatus=1 ORDER BY cpe_periodo";
        $catalogo = new Catalogo();
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
                $id = $rs[strtoupper("cpe_id_periodo")];
                $periodo=$rs[strtoupper("cpe_periodo")];
                $cerrado= $rs[strtoupper("cpe_cerrado")];
                $fecha_ini= $rs[strtoupper("cpe_fecha_ini")];
                $fecha_fin= $rs[strtoupper("cpe_fecha_fin")];

                $elemento=new Periodo();
                $elemento->setAll($id, $periodo, $cerrado, $fecha_ini, $fecha_fin);
                array_push($lista,$elemento);
        }	
        return $lista;
    }
        
    public function obtieneListadoAbierto() {
		
        $lista= array();

        $query="SELECT * FROM sie_cat_periodos WHERE cpe_estatus=1 and cpe_cerrado=0 ORDER BY cpe_periodo";

        $catalogo = new Catalogo();
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cpe_id_periodo")];
            $periodo=$rs[strtoupper("cpe_periodo")];
            $cerrado= $rs[strtoupper("cpe_cerrado")];
            $fecha_ini= $rs[strtoupper("cpe_fecha_ini")];
            $fecha_fin= $rs[strtoupper("cpe_fecha_fin")];

            $elemento=new Periodo();
            $elemento->setAll($id, $periodo, $cerrado, $fecha_ini, $fecha_fin);
            array_push($lista,$elemento);
        }	

        return $lista;
    }
    
    public function obtieneListadoAbiertoSinInfoGb() {
		
            $lista= array();

            $query="SELECT * FROM sie_cat_periodos WHERE cpe_estatus=1 and cpe_cerrado=0 and cpe_id_periodo not in (Select CPE_ID_PERIODO from SIE_PRESUP_ASIGNADO WHERE PAS_ESTATUS=1 AND DESTINO='GB' )ORDER BY cpe_periodo";

            $catalogo = new Catalogo();
            $result = $catalogo->obtenerLista($query);

            while ($rs = mysql_fetch_array($result)){
                $id= $rs[strtoupper("cpe_id_periodo")];
                $periodo=$rs[strtoupper("cpe_periodo")];
                $cerrado= $rs[strtoupper("cpe_cerrado")];
                $fecha_ini= $rs[strtoupper("cpe_fecha_ini")];
                $fecha_fin= $rs[strtoupper("cpe_fecha_fin")];

                $elemento=new Periodo();
                $elemento->setAll($id, $periodo, $cerrado, $fecha_ini, $fecha_fin);
                array_push($lista,$elemento);
            }	

        return $lista;
	}
        
    public function obtieneListadoAbiertoIdPeriodo($idPeriodo){
		
        $lista= array();

            $query="SELECT * FROM sie_cat_periodos WHERE cpe_estatus=1 and (cpe_cerrado=0 OR cpe_id_periodo=".$idPeriodo." ) ORDER BY cpe_periodo";

            $catalogo = new Catalogo();
            $result = $catalogo->obtenerLista($query);

            while ($rs = mysql_fetch_array($result)){
                $id= $rs[strtoupper("cpe_id_periodo")];
                $periodo=$rs[strtoupper("cpe_periodo")];
                $cerrado= $rs[strtoupper("cpe_cerrado")];
                $fecha_ini= $rs[strtoupper("cpe_fecha_ini")];
                $fecha_fin= $rs[strtoupper("cpe_fecha_fin")];

                $elemento=new Periodo();
                $elemento->setAll($id, $periodo, $cerrado, $fecha_ini, $fecha_fin);
                array_push($lista,$elemento);
            }	

        return $lista;		
    }
    
    public function obtieneElemento($idElemento) {
		
		
        $elemento=new Periodo();

        $query="SELECT * FROM sie_cat_periodos WHERE cpe_id_periodo=".$idElemento;

        $catalogo = new Catalogo();
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cpe_id_periodo")];
            $periodo = $rs[strtoupper("cpe_periodo")];
            $cerrado= $rs[strtoupper("cpe_cerrado")];
            $fecha_ini= $rs[strtoupper("cpe_fecha_ini")];
            $fecha_fin= $rs[strtoupper("cpe_fecha_fin")];

            $elemento=new Periodo();
            $elemento->setAll($id, $periodo, $cerrado, $fecha_ini, $fecha_fin);

                }

        return $elemento;
		
    }
    
    public function obtieneidElemento($anio) {	
		
        $id=null;
        $query="SELECT cpe_id_periodo FROM sie_cat_periodos WHERE cpe_estatus=1 and cpe_periodo=".$anio;
        
        $catalogo = new Catalogo();
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cpe_id_periodo")];

        }
	return $id;	
    }
    
    public function obtieneElementoDesc($descripcion) {
		
		
        $elemento=new Periodo();
        $query="SELECT * FROM sie_cat_periodos WHERE cpe_periodo=".$descripcion;

        $catalogo = new Catalogo();
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cpe_id_periodo")];
            $periodo = $rs[strtoupper("cpe_periodo")];
            $cerrado= $rs[strtoupper("cpe_cerrado")];
            $fecha_ini= $rs[strtoupper("cpe_fecha_ini")];
            $fecha_fin= $rs[strtoupper("cpe_fecha_fin")];

            $elemento=new Periodo();
            $elemento->setAll($id, $periodo, $cerrado, $fecha_ini, $fecha_fin);
								
	}
		
	return $elemento;
		
    }
		
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();

        $ffin = $elemento->getFechaFinal();
        $ffin = explode("/", $ffin);
        $query="INSERT INTO sie_cat_periodos(cpe_periodo,cpe_cerrado,cpe_fecha_ini,cpe_fecha_fin) VALUES ('".$elemento->getPeriodo()."', ".$elemento->getCerrado().", '".date("Y-m-d", strtotime($elemento->getFechaInicial()))."', '".$ffin[2]."-".$ffin[1]."-".$ffin[0]."')";
        $res = $con->obtenerLista($query);

        if($res==1)
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function actualizaElemento($elemento) {
		
        $con=new Catalogo();
        $ffin = $elemento->getFechaFinal();
        $ffin = explode("/", $ffin);
        $query="UPDATE sie_cat_periodos set cpe_periodo='".$elemento->getPeriodo()."', cpe_cerrado='".$elemento->getCerrado()."', cpe_fecha_ini='".date("Y-m-d", strtotime($elemento->getFechaInicial()))."', cpe_fecha_fin='".$ffin[2]."-".$ffin[1]."-".$ffin[0]."' WHERE cpe_id_periodo=".$elemento->getId();
        $res = $con->obtenerLista($query);

        if($res==1)
        {	return true; }
        else
        {	return false; }
		
    }

    public function eliminaElemento($idElemento){
		
	$con=new Catalogo();
	$query="UPDATE sie_cat_periodos set  cpe_estatus=0 WHERE cpe_id_periodo=".$idElemento;
	$res = $con->obtenerLista($query);
		
        if($res==1)
        {	return true; }
        else
        {	return false; }
		
    }
}
