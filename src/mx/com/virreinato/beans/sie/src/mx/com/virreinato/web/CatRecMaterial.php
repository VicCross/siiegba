<?php

session_start();
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/RecMaterialDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/RecMateriales.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$respuesta = "";
if (isset($_POST['id_Carta'])){
    $elemento = new RecMateriales();

    $elemento->setIdCarta( (int)( $_POST["id_Carta"] ));
    if( $_POST["orden_material"]!= "" )$elemento->setNum_orden( (int)( $_POST["orden_material"] ) );
    $elemento->setRecurso( $_POST["descripcion_material"]);
    $elemento->setPartida( $_POST["partida"] );
    $elemento->setUnitario((float)( $_POST["unitario"] ) );
    $elemento->setCosto( (float)( $_POST["total"] ));
    if( $_POST["ejercico_Material"] != "" )$elemento->setFecha(date("Y-m-d",strtotime( $_POST["ejercico_Material"])) );
    $elemento->setFacturaEntregada( $_POST["fac_entregada"] );
    $elemento->setNombreCheque($_POST["cheque_nombre"] );
    $elemento->setNo_cheque( $_POST["noCheque"] );
    if (isset($_POST['id'])){
        $elemento->setId((int)($_POST["id"]));
        $dao=new RecMaterialDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new RecMaterialDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
    header("Location: ../../../../../Proyectos/lista_RecMateriales.php?respuesta=" . $respuesta."&idCarta=".$_POST['id_Carta']);
}else if (isset($_GET['id'])){
    $dao=new RecMaterialDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
    header("Location: ../../../../../Proyectos/lista_RecMateriales.php?respuesta=" . $respuesta."&idCarta=".$_GET['idCarta']);
} else {
    $respuesta = "No se detecto la acción a realizar.";
    header("Location: ../../../../../Proyectos/lista_RecMateriales.php?respuesta=" . $respuesta);
}
?>
