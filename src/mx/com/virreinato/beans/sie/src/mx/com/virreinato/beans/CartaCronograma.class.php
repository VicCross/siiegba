<?php

class CartaCronograma {
    private $id;
    private $idCarta;
    private $orden;
    private $Actividad;
    private $fechaInicio;
    private $fechaFin;
    private $fase;
    
    function setAll($id, $idCarta, $orden, $Actividad, $fechaInicio, $fechaFin, $fase) {
        $this->id = $id;
        $this->idCarta = $idCarta;
        $this->orden = $orden;
        $this->Actividad = $Actividad;
        $this->fechaInicio = $fechaInicio;
        $this->fechaFin = $fechaFin;
        $this->fase = $fase;
    }
    
    function cartaCrono($id, $idCarta, $Actividad, $fechaInicio, $fechaFin, $fase) {
        $this->id = $id;
        $this->idCarta = $idCarta;
        $this->Actividad = $Actividad;
        $this->fechaInicio = $fechaInicio;
        $this->fechaFin = $fechaFin;
        $this->fase = $fase;
    }

    function __construct() {
        
    }
    
    public function getId() {
        return $this->id;
    }

    public function getIdCarta() {
        return $this->idCarta;
    }

    public function getOrden() {
        return $this->orden;
    }

    public function getActividad() {
        return $this->Actividad;
    }

    public function getFechaInicio() {
        return $this->fechaInicio;
    }

    public function getFechaFin() {
        return $this->fechaFin;
    }

    public function getFase() {
        return $this->fase;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setIdCarta($idCarta) {
        $this->idCarta = $idCarta;
    }

    public function setOrden($orden) {
        $this->orden = $orden;
    }

    public function setActividad($Actividad) {
        $this->Actividad = $Actividad;
    }

    public function setFechaInicio($fechaInicio) {
        $this->fechaInicio = $fechaInicio;
    }

    public function setFechaFin($fechaFin) {
        $this->fechaFin = $fechaFin;
    }

    public function setFase($fase) {
        $this->fase = $fase;
    }



}
