<?php

class Entregables {
    
    private $id;
    private $idCarta;
    private $num_orden;
    private $descripcion;
    private $caracteristicas;
    private $solicitado;
    private $entregado;
    private $valido;
    
    function setAll($id, $idCarta, $num_orden, $descripcion, $caracteristicas, $solicitado, $entregado, $valido) {
        $this->id = $id;
        $this->idCarta = $idCarta;
        $this->num_orden = $num_orden;
        $this->descripcion = $descripcion;
        $this->caracteristicas = $caracteristicas;
        $this->solicitado = $solicitado;
        $this->entregado = $entregado;
        $this->valido = $valido;
    }

    function constructor1($id, $idCarta, $num_orden, $descripcion) {
        $this->id = $id;
        $this->idCarta = $idCarta;
        $this->num_orden = $num_orden;
        $this->descripcion = $descripcion;
    }
    
    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getIdCarta() {
        return $this->idCarta;
    }

    public function getNum_orden() {
        return $this->num_orden;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function getCaracteristicas() {
        return $this->caracteristicas;
    }

    public function getSolicitado() {
        return $this->solicitado;
    }

    public function getEntregado() {
        return $this->entregado;
    }

    public function getValido() {
        return $this->valido;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setIdCarta($idCarta) {
        $this->idCarta = $idCarta;
    }

    public function setNum_orden($num_orden) {
        $this->num_orden = $num_orden;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function setCaracteristicas($caracteristicas) {
        $this->caracteristicas = $caracteristicas;
    }

    public function setSolicitado($solicitado) {
        $this->solicitado = $solicitado;
    }

    public function setEntregado($entregado) {
        $this->entregado = $entregado;
    }

    public function setValido($valido) {
        $this->valido = $valido;
    }

}
