<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProyectoMeta.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProgramaInstitucional.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class ProgramaInstitucionalDaoJdbc {
    
    public function obtieneListado(){
        
	$lista =  array();
	$query =strtolower("SELECT * FROM sie_cat_programa_institucional  " .
		 " WHERE cpi_estatus =  1  ORDER BY cpi_programa ");
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while($rs = mysql_fetch_array($result)){
            $idProyectoInst=$rs[strtoupper("cpi_id_programa")];
            $programaInstitucional = $rs[strtoupper("cpi_programa")];
            $estatus= $rs[strtoupper("cpi_estatus")];

            $elemento = new ProgramaInstitucional();
            $elemento->setAll($idProyectoInst,$programaInstitucional,$estatus);
            array_push($lista,$elemento);

        }

        return $lista;
    }
    
    public function obtieneElemento($idPrograma){
        
	$elemento = null;
	$query = strtolower("SELECT * FROM sie_cat_programa_institucional WHERE cpi_id_programa = ".$idPrograma);
	$catalogo = new Catalogo();
        $result = $catalogo->obtenerLista($query);
        
        while ($rs = mysql_fetch_array($result)){
            $idProyectoInst=$rs[strtoupper("cpi_id_programa")];
            $programaInstitucional = $rs[strtoupper("cpi_programa")];
            $estatus= $rs[strtoupper("cpi_estatus")];

            $elemento = new ProgramaInstitucional();
            $elemento->setAll($idProyectoInst,$programaInstitucional,$estatus);

        }

        return $elemento;	
    }
}
