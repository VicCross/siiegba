<?php

class Partida {
    private $Id;
    private $capitulo;
    private $partida;
	private $generica;
	private $especifica;
    private $descripcion;
    
    function setAll($Id, $capitulo, $partida,$generica,$especifica, $descripcion) {
        $this->Id = $Id;
        $this->capitulo = $capitulo;
        $this->partida = $partida;
		$this->generica = $generica;
		$this->especifica = $especifica;
        $this->descripcion = $descripcion;
    }
    
    function __construct() {
        
    }

    public function getId() {
        return $this->Id;
    }

    public function getCapitulo() {
        return $this->capitulo;
    }

    public function getPartida() {
        return $this->partida;
    }
	
	public function getGenerica() {
        return $this->generica;
    }
	
	public function getEspecifica() {
        return $this->especifica;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setCapitulo($capitulo) {
        $this->capitulo = $capitulo;
    }

    public function setPartida($partida) {
        $this->partida = $partida;
    }
	
	 public function setGenerica($generica) {
        $this->generica = $generica;
    }
	
	 public function setEspecifica($especifica) {
        $this->especifica = $especifica;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }


}
