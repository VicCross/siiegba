<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/FondosDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/MinistraDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Fondos.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Ministra.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
if(isset($_POST['inicio'])){
    $inicio = $_POST["inicio"];
    $fin = $_POST["fin"];
    $periodo = $_POST["periodo"];
    $tipo_nomina = $_POST["tipo_nomina"];
    
    header("Location: ../../../../../Proyectos/lista_Ministracion.php?inicio=".$inicio."&fin=".$fin."&periodo=".$periodo."&tipo_nomina=".$tipo_nomina);
}else if($_POST['PSolicitud']){
    $dao = new FondosDaoJdbc();
    $fondos =  $dao->obtieneListado($_POST['PSolicitud']);
    $elemento = new Fondos();
    echo("<option value='0' >Selecciona</option>");
    foreach($fondos as $elemento){
        $sel="";
        if( (int)($_POST['Seleccionado']) == $elemento->getId() ) $sel="selected='selected'";
        echo("<option value='".$elemento->getId()."' ".$sel." >".$elemento->getNumSol()." ".$elemento->getDescripcion()."</option>");
    }
}else if($_POST['numOpera']){
    $elemento = new Ministra();
    $elemento->setNumOperacion($_POST["numOpera"]);
    $elemento->setDescripcion($_POST["descripcion"]);
    $elemento->setMonto((double)($_POST["monto"]));

    if (isset($_POST['fecha']) && $_POST["fecha"] != "") {
            $fechaConFormato = date("d-m-Y",strtotime($_POST["fecha"]));
            $elemento->setFecha($fechaConFormato);
    }// if

    $elemento->setDestino($_POST["destino"]);
    $elemento->setObservaciones($_POST["observa"]);
    $elemento->setFormaCarga($_POST["formaCarga"]);
    $elemento->setIdCuentaBancaria((int)($_POST["cuentaBancaria"]));
    $elemento->setIdPeriodo((int)($_POST["periodo"]));


    if($_POST["destino"] == "GB" || $_POST["destino"] == "TR" || $_POST["destino"] == "DN" || $_POST["destino"] == "SL" ){
        $elemento->setIdProyecto(0);
        $elemento->setIdSolicitud(0);

    }
    else{
        $elemento->setIdProyecto((int)($_POST["proyecto"]));
        $elemento->setIdSolicitud((int)($_POST["solicitud"]));
    }
    
    if ($_POST["id"] != null){
        $elemento->setId((int)($_POST["id"]));
        $dao=new MinistraDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new MinistraDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
    header("Location: ../../../../../Proyectos/lista_Ministracion.php?respuesta=" . $respuesta);
}else if (isset($_GET['id'])){
    $dao=new MinistraDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
    header("Location: ../../../../../Proyectos/lista_Ministracion.php?respuesta=" . $respuesta);
} else {
    $respuesta = "No se detecto la acción a realizar.";
    header("Location: ../../../../../Proyectos/lista_Ministracion.php?respuesta=" . $respuesta);
}

?>

