<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Fondos.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class FondosDaoJdbc {
    
    public function obtieneListado($inicio, $fin, $periodo, $proyecto) {
        
        $lista = array();
        $query = null;
        
        $inicio = date("Y-m-d",strtotime($inicio));
        $fin = date("Y",strtotime($fin))."-".date("m",strtotime($fin))."-".date("d",strtotime($fin));
        
        if( $proyecto == "TD"){
            $query = "select f.sfo_mes, f.sfo_id_solfondos as idsolfon, f.sfo_numerosol as numsol, f.sfo_fecha as fecha," .
                " c.ccc_id_ccosto as idccosto, c.ccc_descripcion as descc, f.sfo_tipo as tipo, p.cpr_id_proyecto as idproy," .
                " p.cpr_descripcion as desproy, p.cpr_numeroproyecto,  pr.cpe_id_periodo as idper, pr.cpe_periodo as desper," .
                " f.sfo_descripcion as dessol, f.sfo_titular as tit, f.sfo_num_mes  " .
                "FROM sie_solicitud_fondos f, sie_cat_proyectos p,sie_cat_periodos pr, sie_cat_centrocosto c  " .
                "WHERE p.cpr_id_proyecto=f.cpr_id_proyecto AND f.ccc_id_ccosto = c.ccc_id_ccosto  " .
                "AND f.cpe_id_periodo = pr.cpe_id_periodo AND pr.cpe_id_periodo=p.cpe_id_periodo " .
                "AND p.cpr_estatus=1 AND pr.cpe_estatus=1 AND f.sfo_estatus = 1 " .
                "AND f.sfo_fecha BETWEEN '".$inicio."' AND '".$fin."' AND pr.cpe_id_periodo = '".$periodo."' " .
                " ORDER BY  p.cpr_descripcion, f.sfo_numerosol,  f.sfo_num_mes ";

        }else{
            $query = "select sfo_mes, f.sfo_id_solfondos as idsolfon, f.sfo_numerosol as numsol, f.sfo_fecha as fecha, " .
                "c.ccc_id_ccosto as idccosto, c.ccc_descripcion as descc, f.sfo_tipo as tipo, p.cpr_id_proyecto as idproy, " .
                "p.cpr_descripcion as desproy, p.cpr_numeroproyecto,  pr.cpe_id_periodo as idper, pr.cpe_periodo as desper," .
                " f.sfo_descripcion as dessol, f.sfo_titular as tit, f.sfo_num_mes " .
                "FROM sie_solicitud_fondos f, sie_cat_centrocosto c, sie_cat_proyectos p, sie_cat_periodos pr " .
                "WHERE p.cpr_id_proyecto=f.cpr_id_proyecto AND f.ccc_id_ccosto = c.ccc_id_ccosto  " .
                "AND f.cpe_id_periodo = pr.cpe_id_periodo AND pr.cpe_id_periodo=p.cpe_id_periodo " .
                "AND p.cpr_estatus=1 AND pr.cpe_estatus=1 AND f.sfo_estatus = 1 " .
                "AND f.sfo_fecha BETWEEN '".$inicio."' AND '".$fin."' AND pr.cpe_id_periodo = ".$periodo."  " .
                " AND p.cpr_id_proyecto = ".$proyecto." " .
                " ORDER BY p.cpr_descripcion, f.sfo_numerosol,  f.sfo_num_mes ";
        }

        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)) {
            $id = $rs["idsolfon"];
            $numSol = $rs["numsol"];
            $fecha = $rs["fecha"];
            $idCCosto = $rs["idccosto"];
            $desCCosto = $rs["descc"];
            $tipo = $rs["tipo"];
            $idProyecto = $rs["idproy"];
            $desProyecto = $rs["desproy"];
            $idPeriodo = $rs["idper"];
            $desPeriodo = $rs["desper"];
            $descripcion = $rs["dessol"];
            $titular = $rs["tit"];
            $mes = $rs["sfo_mes"];
            $estatus=1;

            $elemento = new Fondos();
            $elemento->setAll($id, $numSol, $fecha, $idCCosto, $tipo, $idProyecto, $idPeriodo, $descripcion, $titular, $estatus, $desCCosto, $desProyecto, $desPeriodo,$mes);

            array_push($lista, $elemento);

        }

        return $lista;
    }
    
    public function obtieneListado2($proyecto) {
        
	$lista = array();

	$query = "select sfo_id_solfondos,sfo_numerosol , sfo_descripcion FROM sie_solicitud_fondos WHERE cpr_id_proyecto = ".$proyecto." and sfo_estatus = 1 ORDER BY(sfo_id_solfondos) ";

	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)) {
            $id = $rs["sfo_id_solfondos"];
            $numSol = $rs["sfo_numerosol"];
            $descripcion = $rs["sfo_descripcion"];

            $elemento = new Fondos();
            $elemento->constructor($id, $numSol, $descripcion);

            array_push($lista, $elemento);

        }

        return $lista;
    }
    
    public function obtieneElemento($idElemento) {

	$elemento=new Fondos();

	$query = "select f.sfo_id_solfondos as idsolfon, f.sfo_numerosol as numsol, "
            ."f.sfo_fecha as fecha, c.ccc_id_ccosto as idccosto, c.ccc_descripcion  "
            ."as descc, f.sfo_tipo as tipo, p.cpr_id_proyecto as idproy,  "
            ."p.cpr_descripcion as desproy, pr.cpe_id_periodo as idper,  "
            ."pr.cpe_periodo as desper, f.sfo_descripcion as dessol,  "
            ."f.sfo_titular as tit, f.sfo_mes  FROM sie_solicitud_fondos f,  "
            ."sie_cat_centrocosto c, sie_cat_proyectos p, sie_cat_periodos pr "
            ."where f.ccc_id_ccosto = c.ccc_id_ccosto  "
            ."and f.cpr_id_proyecto = p.cpr_id_proyecto  "
            ."and f.cpe_id_periodo = pr.cpe_id_periodo  "
            ."and f.sfo_estatus = 1 and f.sfo_id_solfondos = "
            .$idElemento;

	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)) {
            $id = $rs["idsolfon"];
            $numSol = $rs["numsol"];
            $fecha = $rs["fecha"];
            $idCCosto = $rs["idccosto"];
            $desCCosto = $rs["descc"];
            $tipo = $rs["tipo"];
            $idProyecto = $rs["idproy"];
            $desProyecto = $rs["desproy"];
            $idPeriodo = $rs["idper"];
            $desPeriodo = $rs["desper"];
            $descripcion = $rs["dessol"];
            $titular = $rs["tit"];
            $mes = $rs["sfo_mes"];
            $estatus=1;

            $elemento = new Fondos();
            $elemento->setAll($id, $numSol, $fecha, $idCCosto, $tipo, $idProyecto, $idPeriodo, $descripcion, $titular, $estatus, $desCCosto, $desProyecto, $desPeriodo,$mes);
        }

        return $elemento;
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $query="INSERT INTO sie_solicitud_fondos(sfo_numerosol,sfo_fecha,ccc_id_ccosto,sfo_tipo,cpr_id_proyecto,cpe_id_periodo,sfo_descripcion,sfo_titular,sfo_estatus,sfo_mes,sfo_num_mes) VALUES ('".$elemento->getNumSol(). "', '"
            . date("Y-m-d",strtotime($elemento->getFecha())). "', "
            . $elemento->getIdCCosto(). ","
            . $elemento->getTipo()	. ","
            . $elemento->getIdProyecto()	. ","
            . $elemento->getIdPeriodo()	. ", '"
            . $elemento->getDescripcion()	. "', '"
            . $elemento->getTitular() . "', 1, '" .$elemento->getMesCalendario()."', ".$elemento->getNumMesCalendario()." )";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
	$query="UPDATE sie_solicitud_fondos  SET  sfo_numerosol = '". $elemento->getNumSol() . "', sfo_fecha = '". date("Y-m-d",strtotime($elemento->getFecha())). "', ccc_id_ccosto=" . $elemento->getIdCCosto() . ", sfo_tipo = " 
				. $elemento->getTipo() . ", sfo_descripcion = '". $elemento->getDescripcion() . "', sfo_titular = '". $elemento->getTitular() . "' where sfo_id_solfondos = " . $elemento->getId();
        $res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_solicitud_fondos set sfo_estatus = 0 WHERE sfo_id_solfondos =".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}
