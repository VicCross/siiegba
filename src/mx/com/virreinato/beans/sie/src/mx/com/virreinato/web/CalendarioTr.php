<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/PresupuestoAsignadoGbDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/PresupuestoAsignadoGb.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$respuesta = "";
if(isset($_POST['destino'])){
    $elemento = new PresupuestoAsignadoGb(); 
			
    $elemento->setIdPeriodo((int)($_POST["periodo"]));

    if( $_POST["enero_Calendario"] != "" ){ $elemento->setMontoEne((float)($_POST["enero_Calendario"])); }else{ $elemento->setMontoEne(0);}
    if( $_POST["febrero_Calendario"] != "" ){ $elemento->setMontoFeb((float)($_POST["febrero_Calendario"])); }else{ $elemento->setMontoFeb(0);}
    if( $_POST["marzo_Calendario"] != "" ){ $elemento->setMontoMar((float)($_POST["marzo_Calendario"])); }else{ $elemento->setMontoMar(0); }
    if( $_POST["abril_Calendario"] != "" ) {$elemento->setMontoAbr((float)($_POST["abril_Calendario"])); }else {$elemento->setMontoAbr(0); }
    if( $_POST["mayo_Calendario"] != "" ){ $elemento->setMontoMay((float)($_POST["mayo_Calendario"])); }else{ $elemento->setMontoMay(0); }
    if( $_POST["junio_Calendario"] != "" ){ $elemento->setMontoJun((float)($_POST["junio_Calendario"])); }else{ $elemento->setMontoJun(0); }
    if( $_POST["julio_Calendario"] != "" ){ $elemento->setMontoJul((float)($_POST["julio_Calendario"])); }else{ $elemento->setMontoJul(0); }
    if( $_POST["agosto_Calendario"] != "" ){ $elemento->setMontoAgo((float)($_POST["agosto_Calendario"])); }else{ $elemento->setMontoAgo(0); }
    if( $_POST["septiembre_Calendario"] != "" ){ $elemento->setMontoSep((float)($_POST["septiembre_Calendario"]));} else{ $elemento->setMontoSep(0); }
    if( $_POST["octubre_Calendario"] != "" ) {$elemento->setMontoOct((float)($_POST["octubre_Calendario"])); }else{ $elemento->setMontoOct(0); }
    if( $_POST["noviembre_Calendario"] != "" ){ $elemento->setMontoNov((float)($_POST["noviembre_Calendario"])); }else {$elemento->setMontoNov(0); }
    if( $_POST["diciembre_Calendario"] != "" ){ $elemento->setMontoDic((float)($_POST["diciembre_Calendario"])); }else {$elemento->setMontoDic(0); }
    $elemento->setDestino($_POST["destino"]);
    
    if ($_POST["id"] != null){
        $elemento->setId((int)($_POST["id"]));
        $dao=new PresupuestoAsignadoGbDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new PresupuestoAsignadoGbDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
}else if (isset($_GET['id'])){
    $dao=new PresupuestoAsignadoGbDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
} else {
    $respuesta = "No se detecto la acción a realizar.";
}
header("Location: ../../../../../Terceros/lista_CalendarioTr.php?respuesta=" . $respuesta);
?>
}

