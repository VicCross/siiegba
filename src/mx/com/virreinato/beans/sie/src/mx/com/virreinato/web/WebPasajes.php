<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/PasajeDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Pasaje.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Empleado.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$respuesta = "";
if(isset($_POST['inicioFecha']) && $_POST['inicioFecha']!=null){
    $inicio = $_POST["inicioFecha"];
    $fin= $_POST["finFecha"];
    header("Location: ../../../../../Formatos/lista_Pasaje.php?inicio=".$inicio."&fin=".$fin);
}else if(isset($_POST['ccosto'])){
    $elemento=new Pasaje();

    $elemento->setIdCCosto((int)($_POST["ccosto"]));
    $fechaof= date("Y-m-d",strtotime($_POST["Fecha"] ));

    $elemento->setFecha($fechaof);
    $elemento->setMes(date("Y-m-d",strtotime($_POST["mes"] )));
    $elemento->setTarifaDiaria((float)($_POST["tarifaDiaria"]));
    $elemento->setNumeroDias((int)($_POST["numDias"]));
    $elemento->setMontoTotal((float)($_POST["montoTotal"]));
    $elemento->setLetraMonto($_POST["letra"] );
    $elemento->setConcepto($_POST["concepto"] );
    $elemento->setEstatus(1);

    $emp=new Empleado();
    $emp->setId((int)($_POST["empleado"]));
    $elemento->setEmpleado($emp);
    if ($_POST["id"] != null){
        $elemento->setId((int)($_POST["id"]));
        $dao=new PasajeDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new PasajeDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
    header("Location: ../../../../../Formatos/lista_Pasaje.php?respuesta=" . $respuesta);
}else if (isset($_GET['id'])){
    $dao=new PasajeDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
    header("Location: ../../../../../Formatos/lista_Pasaje.php?respuesta=" . $respuesta);
} else {
    $respuesta = "No se detecto la acción a realizar.";
    header("Location: ../../../../../Formatos/lista_Pasaje.php?respuesta=" . $respuesta);
}
?>

