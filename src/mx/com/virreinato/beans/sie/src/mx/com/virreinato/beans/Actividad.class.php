<?php


class Actividad {
    private $id;
    private $idArea;
    private $descripcion;
    private $orden;
    
function setAll($id, $idArea, $descripcion, $orden) {
    $this->id = $id;
    $this->idArea = $idArea;
    $this->descripcion = $descripcion;
    $this->orden = $orden;
}

function __construct() {
    
}

public function getId() {
    return $this->id;
}

public function getIdArea() {
    return $this->idArea;
}

public function getDescripcion() {
    return $this->descripcion;
}

public function getOrden() {
    return $this->orden;
}

public function setId($id) {
    $this->id = $id;
}

public function setIdArea($idArea) {
    $this->idArea = $idArea;
}

public function setDescripcion($descripcion) {
    $this->descripcion = $descripcion;
}

public function setOrden($orden) {
    $this->orden = $orden;
}


}