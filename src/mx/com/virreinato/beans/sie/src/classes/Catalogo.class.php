<?php

include_once("Conexion.class.php");

/**
 * Description of Catalogo
 *
 * @author MAGG
 */
class Catalogo {

    public function obtenerLista($query) {
        $this->conn = new Conexion();
        $query = mysql_query($query);
        $this->conn->Desconectar();
        return $query;
    }

    public function insertarRegistro($consulta){
        $this->conn = new Conexion();
        $query = mysql_query($consulta);        
        $id = mysql_insert_id();
        $this->conn->Desconectar();
        return $id;
    }


    public function getListaAlta($tabla, $order_by) {
        $this->conn = new Conexion();
        $order = "";
        if ($order_by != "") {
            $order = "ORDER BY `" . $order_by . "`";
        }
        
        $consulta = "SELECT * FROM `" . $tabla . "` Where Activo = 1 " . $order . ";";        
        $query = mysql_query($consulta);
        
        $this->conn->Desconectar();
        return $query;
    }
    
    public function getListaAltaTodo($tabla, $order_by) {
        $this->conn = new Conexion();
        $order = "";
        if ($order_by != "") {
            $order = "ORDER BY `" . $order_by . "`";
        }             
        $query = mysql_query("SELECT * FROM `" . $tabla . "` " . $order . ";");
        $this->conn->Desconectar();
        return $query;
    }        
}

?>
