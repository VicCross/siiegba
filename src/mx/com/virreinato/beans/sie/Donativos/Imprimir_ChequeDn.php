<?php
$id = null;

session_start();
include_once("../src/mx/com/virreinato/dao/CuentaBancariaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CuentaBancaria.class.php");
include_once("../src/mx/com/virreinato/dao/CatChequeDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatCheque.class.php");
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/PresupuestoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Presupuesto.class.php");
include_once("../src/mx/com/virreinato/dao/AreaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Area.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}else{
    if(isset($_SESSION['id'])){ $id = $_SESSION['id'];}
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<link href="../css/calendario.css" type="text/css" rel="stylesheet">
<script src="../js/calendar.js" type="text/javascript"></script>
<script src="../js/calendar-es.js" type="text/javascript"></script>
<script src="../js/calendar-setup.js" type="text/javascript"></script>
<script src="../js/gen_validatorv4.js" ></script>
<script>
	function numero_poliza(){
		
		folio=document.getElementById('folio').value;
		document.getElementById('poliza').value=folio;
		//alert('folio'+folio);
	}
	
	function Imprimir(pagina,folio,alto) {
		
		var dir = pagina + "?folio=" + folio;
		var left = Math.floor((screen.width - 980) / 2);
		var opciones = 'titlebar=0, menubar=0, toolbar=0, location=0, directories=0, status=0, scrollbars=0, resizable=0, width=980, height='+alto+',top=50,left='
				+ left + '';
		window.open(dir, "", opciones);
	}
	
	function OnlyNumber(value, elemnt){
			if( isNaN(value) ){
				elemnt.value = ""
			}
	}
	
	$().ready(function(){
		 var monto = $("#MontoCheque").val()
		 covertirNumLetras(monto);
	});
	
	// Función modulo, regresa el residuo de una división 
   function mod(dividendo , divisor) 
   { 
  			resDiv = dividendo / divisor ;  
  			parteEnt = Math.floor(resDiv);            // Obtiene la parte Entera de resDiv 
  			parteFrac = resDiv - parteEnt ;      // Obtiene la parte Fraccionaria de la división
  			modulo = Math.round(parteFrac * divisor);  // Regresa la parte fraccionaria * la división (modulo) 
  			return modulo; 
	} // Fin de función mod


	// Función ObtenerParteEntDiv, regresa la parte entera de una división
	function ObtenerParteEntDiv(dividendo , divisor) 
	{ 
  			resDiv = dividendo / divisor ;  
  			parteEntDiv = Math.floor(resDiv); 
  			return parteEntDiv; 
	} // Fin de función ObtenerParteEntDiv

	
	// function fraction_part, regresa la parte Fraccionaria de una cantidad
	function fraction_part(dividendo , divisor) 
	{ 
  			resDiv = dividendo / divisor ;  
  			f_part = Math.floor(resDiv); 
  			return f_part; 
	} // Fin de función fraction_part


	// function string_literal convierte el monto en letra 
	function string_literal_conversion(number) 
	{   
   		centenas = ObtenerParteEntDiv(number, 100); 
   		number = mod(number, 100); 

     		decenas = ObtenerParteEntDiv(number, 10); 
   		number = mod(number, 10); 

   		unidades = ObtenerParteEntDiv(number, 1); 
   		number = mod(number, 1);  
   		string_hundreds="";
   		string_tens="";
   		string_units="";
   
		   if(centenas == 1) string_hundreds = "ciento ";
   		if(centenas == 2) string_hundreds = "doscientos ";
   		if(centenas == 3) string_hundreds = "trescientos ";
   		if(centenas == 4) string_hundreds = "cuatrocientos ";
   		if(centenas == 5) string_hundreds = "quinientos ";
   		if(centenas == 6)	string_hundreds = "seiscientos ";
   		if(centenas == 7) string_hundreds = "setecientos ";
   		if(centenas == 8)	string_hundreds = "ochocientos ";
   		if(centenas == 9) string_hundreds = "novecientos ";

		   if(decenas == 1){
      		//Special case, depends on units for each conversion
     			if(unidades == 1) string_tens = "once";
      		if(unidades == 2) string_tens = "doce";
      		if(unidades == 3) string_tens = "trece";
      		if(unidades == 4) string_tens = "catorce";
      		if(unidades == 5) string_tens = "quince";
      		if(unidades == 6) string_tens = "dieciseis";
      		if(unidades == 7) string_tens = "diecisiete";
      		if(unidades == 8) string_tens = "dieciocho";
      		if(unidades == 9) string_tens = "diecinueve";
      
   		} 
   		
   		if(decenas == 2) string_tens = "veinti";
			if(decenas == 3) string_tens = "treinta";
   		if(decenas == 4) string_tens = "cuarenta";
   		if(decenas == 5) string_tens = "cincuenta";
   		if(decenas == 6) string_tens = "sesenta";
   		if(decenas == 7) string_tens = "setenta";
   		if(decenas == 8) string_tens = "ochenta";
   		if(decenas == 9) string_tens = "noventa";
   
   
	   if (decenas == 1) string_units="";   
   	else { 
      	if(unidades == 1) string_units = "un";
      	if(unidades == 2) string_units = "dos";
      	if(unidades == 3) string_units = "tres";
      	if(unidades == 4) string_units = "cuatro";
      	if(unidades == 5) string_units = "cinco";
      	if(unidades == 6) string_units = "seis";
      	if(unidades == 7) string_units = "siete";
      	if(unidades == 8) string_units = "ocho";
      	if(unidades == 9) string_units = "nueve";
  
  		  } // end if-then-else 
   	
   	  if (centenas == 1 && decenas == 0 && unidades == 0) string_hundreds = "cien " ; 
		  if (decenas == 1 && unidades ==0) string_tens = "diez " ; 
		  if (decenas == 2 && unidades ==0) string_tens = "veinte " ; 
		  if (decenas >=3 && unidades >=1)  string_tens = string_tens+" y "; 
	
		  final_string = string_hundreds+string_tens+string_units;
		  return final_string ; 

	} //end of function string_literal_conversion()================================ 


	function covertirNumLetras(number)
	{
  
   	cent = number.split(".");   
   	centavos = cent[1];
   	number = cent[0];
   
   
   	if (centavos == 0 || centavos == undefined){
   	centavos = "00";}

   	if (number == 0 || number == "") 
   	{ // if amount = 0, then forget all about conversions, 
      	centenas_final_string=" cero "; // amount is zero (cero). handle it externally, to 
      // function breakdown 
  		} 
   	else 
   	{ 
   
     		millions  = ObtenerParteEntDiv(number, 1000000); // first, send the millions to the string 
      	number = mod(number, 1000000);           // conversion function 
      
     		if (millions != 0)
      	{
      	                      
      		// This condition handles the plural case 
         	if (millions == 1) descriptor= " millon ";  // > than 1, use 'millones' (millions) as 
         	else descriptor = " millones "; 
            
      	} 
      	else  descriptor = " ";                 // if 0 million then use no descriptor. 
      
      
	      millions_final_string = string_literal_conversion(millions)+descriptor; 
      	thousands = ObtenerParteEntDiv(number, 1000);  // now, send the thousands to the string 
        	number = mod(number, 1000);            // conversion function. 
      
      
     		if (thousands != 1) 
      	{                   // This condition eliminates the descriptor 
         		thousands_final_string =string_literal_conversion(thousands) + " mil "; 
       		//  descriptor = " mil ";          // if there are no thousands on the amount 
      	} 
      	if (thousands == 1)
      	{
         	thousands_final_string = " mil "; 
     		}
      	if (thousands < 1) 
      	{ 
         	thousands_final_string = " "; 
      	} 
  
      	// this will handle numbers between 1 and 999 which 
      	// need no descriptor whatsoever. 

     		centenas  = number;                     
      	centenas_final_string = string_literal_conversion(centenas) ; 
      
   	} //end if (number ==0) 

   	/*if (ereg("un",centenas_final_string))
   	{
     		centenas_final_string = ereg_replace("","o",centenas_final_string); 
   	}*/
   	//finally, print the output. 

   	/* Concatena los millones, miles y cientos*/
   	cad = millions_final_string+thousands_final_string+centenas_final_string; 
   
   	/* Convierte la cadena a Mayúsculas*/
   	cad = cad.toUpperCase();       

   	if (centavos.length>2)
  		 {   
      		if(centavos.substring(2,3)>= 5){
         		centavos = centavos.substring(0,1)+(parseInt(centavos.substring(1,2))+1).toString();
      		}   else{
        			centavos = centavos.substring(0,2);
       		}
   	}

   	/* Concatena a los centavos la cadena "/100" */
   	centavos=centavos.replace(/\s*[\r\n][\r\n \t]*/g , "");
   	if (centavos.length==1)
   	{
      		centavos = centavos+"0";
   	}
   		centavos = centavos+ "/100"; 


   	/* Asigna el tipo de moneda, para 1 = PESO, para distinto de 1 = PESOS*/
   	if (number == 1) moneda = " PESO ";  
   	else moneda = " PESOS ";  
   
   
   	/* Regresa el número en cadena entre paréntesis y con tipo de moneda y la fase M.N.*/
   	$("#monto_letra").text("");
   	$("#monto_letra").text(cad+moneda+centavos+" M.N. ");
   	
   	$.ajax({
   		url: "ChequeDn.do",
   		type: "POST",
   		data: "Actualizar=actualizar&Cheque="+$("#idCheque").val()+"&monto="+$("#MontoCheque").val()+"&letra="+$("#monto_letra").val()+"&idProyecto="+ $("#idProyecto").val()
   	});
}


</script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<title>Imprimir Cheque</title>
<body>
    <?php 
    $respuesta = null;
    $error = null;
    $idSolicitud = null;
    
   if(isset($_GET["respuesta"]))
        {$respuesta= (String) $_GET["respuesta"];}
   if(isset($_GET["error"]))
        {$error= (String) $_GET["error"];}
    if(isset($_GET["idSolicitud"]))
        {$idSolicitud= (String) $_GET["idSolicitud"];}
?>
<div class="contenido">
<br/>
<br/>
<p class="titulo_cat1">Donativos > <a class="linkTitulo_cat1" href="SolicitudDn.jsp" >Solicitud de Presupuesto</a> > Impresión de Cheque </p>
<h2 class="titulo_cat2">
<?php
    $daoCheque=new CatChequeDaoJdbc();
    $cheque=new CatCheque();	

    $dia = (String)( date('d')); 
    $mes = (String)( date('m'));
    $anio = (String)( date('Y'));   
		
	if(isset($_GET['idCheque']) && !$_GET['idCheque'] == "0" ){
            $cheque = $daoCheque->obtieneElemento($_GET['idCheque']);
            echo("Imprimir Cheque");

        }else{
		    echo("Nuevo Cheque");
	}		
?>
</h2>
<?php 
     if($respuesta!=null){ echo("<div align='center' class='msj'>".$respuesta."</div>"); }
     if($error!=null){ echo("<div align='center' class='msj'>".$error."</div>"); }
     if(isset($_GET['idCheque']) && !$_GET['idCheque'] == "0" ) { 
?>
<div align="center" >
               <?php if( $id<3 || $id>4 ){ ?>
    &nbsp;&nbsp;&nbsp; <button style="cursor:pointer" class='btn'><a class="link" href="../src/mx/com/virreinato/web/CatChequesDn.php?id=<?php echo($cheque->getId());?>&estatus=<?php echo($cheque->getEstatus());?>" onclick='return confirm("¿Esta seguro que desea eliminar este registro?")' >Eliminar</a></button>
		        <?php }?>
		        
		         <?php $estatus = $daoCheque->obtieneVoBo((String)( $cheque->getIdSolicitud() ));
		            $aux = explode("/",$estatus);  //voboSA/voboST/voboDirector
		            if( ($aux[0] == "Y" && $aux[1] == "Y") || ($aux[0] == "Y" && $aux[2] == "Y") || ($aux[1] == "Y" && $aux[2] == "Y") ){
		           		?>  
		          		&nbsp;&nbsp;&nbsp; <input type="button" style="cursor:pointer" class='btn' onclick="Imprimir('../Formatos/Cheque.php','<?php echo($_GET['idCheque']);?>',250)" value="Imprimir Cheque" >
		           		&nbsp;&nbsp;&nbsp; <input type="button" style="cursor:pointer" class='btn' onclick="Imprimir('../Formatos/PolizaCheque.php','<?php echo($_GET['idCheque']);?>',630)"  value="Imprimir Póliza">
		           <?php } ?>
		     </div>
<?php }?>
<br/>
<form name="frmImprimirCheque" id="frmImprimirCheque" method="POST" action="../src/mx/com/virreinato/web/CatChequesDn.php">
	<table width="50%" border="0" cellspacing="0" cellpadding="5" class='tb_add_cat' align='center'>
 	<tr>
            <td style="display:none" > <input type="text" value="<?php echo($idSolicitud);?>" name="idSolicitud" id="idSolicitud" /> </td>

                <td >

                    <br>&nbsp;&nbsp;&nbsp;No. de Cheque* <input type="text" maxlength="10" size="15" onkeyup="OnlyNumber(this.value,this);numero_poliza();" name="folio" id="folio" value="<?php if($cheque!=null && $cheque->getFolio()!= null) echo($cheque->getFolio());?>"   > 
                    &nbsp;&nbsp;&nbsp; No. de Póliza* <input type="text" maxlength="10" size="15" onkeyup="OnlyNumber(this.value,this)" name="poliza" id="poliza" value="<?php if($cheque!=null && $cheque->getNum_Poliza()!= null) echo($cheque->getNum_Poliza());?>"  >

                    <br/><br/>&nbsp; &nbsp;Aplicación* 
                    <select name="destino_cheque" id="destino_cheque">
                    <option value="DN" <?php if($cheque!=null && $cheque->getDestino()!= null && $cheque->getDestino() == "DN" ) echo( "selected='selected'");?>> Donativos</option>

                     </select>
                         &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Cuenta Bancaria* <select name="cuenta_cheque" id="cuenta_cheque" > 
                             <?php
                                $dao=new CuentaBancariaDaoJdbc();
                                $lista=$dao->obtieneListado();
                                $elemento=new CuentaBancaria();

                                foreach($lista as $elemento){
                                    $sel = "";
                                      if($cheque!= null && $cheque->getIdCuenta()!= null ){
                                             if( $elemento->getId() == $cheque->getIdCuenta() ){ $sel = "selected='selected'"; }
                                        }
                                    echo("<option value=".$elemento->getId()." ".$sel." >" .$elemento->getNumeroCuenta()."</option>");
                                } 
                      ?> 
               </select>
               <br/><br/>&nbsp; &nbsp;Fecha* <input type="text" name="fecha" id="fecha" size="20" value="<?php if($cheque!=null && $cheque->getFechaEmision()!= null){ echo( date("d-m-Y",strtotime($cheque->getFechaEmision()))); }else{ echo($dia."-".$mes."-".$anio); }?>"  />
	  	         
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;Estatus*: 

                <select name="estatus_cheque" id="estatus_cheque" > 
                <?php   if($cheque!= null && $cheque->getEstatus()!= null ){
                        if( $cheque->getEstatus() == "RE")
                         {  
                            echo("  <option value='RE' selected='selected'  > Registrado  </option>");	
                            echo("  <option value='EM'  > Emitido </option>  ");
                            echo("  <option value='CO'   > Cobrado</option>  ");
                            echo("  <option value='CA'  > Cancelado </option>  ");  
                         }else if( $cheque->getEstatus() == "EM")
                         {  
                                 echo("  <option value='RE' > Registrado  </option>");	
                            echo("  <option value='EM' selected='selected'  > Emitido </option>  ");
                            echo("  <option value='CO'   > Cobrado</option>  ");
                            echo("  <option value='CA'  > Cancelado </option>  ");  
                         }
                         else if( $cheque->getEstatus() == "CO" )
                         {
                             echo("  <option value='RE' > Registrado  </option>");		  		 						 	 
                            echo("  <option value='EM'   > Emitido </option>  ");
                            echo("  <option value='CO' selected='selected'   > Cobrado</option>  ");
                            echo("  <option value='CA'  > Cancelado </option>  ");   
                        }
                        else if( $cheque->getEstatus() == "CA" )
                        {
                                echo("  <option value='RE' > Registrado  </option>");	
                                echo("  <option value='EM'   > Emitido </option>  ");
                           echo("  <option value='CO'   > Cobrado</option>  ");
                           echo("  <option value='CA' selected='selected'  > Cancelado </option>  ");     
                        }
                        }else{
                    ?> 
                        <option value="RE" > Registrado  </option> 
                        <option value="EM" > Emitido  </option> 
                        <option value="CO" > Cobrado  </option> 
                        <option value="CA" > Cancelado  </option> 
                <?php  }?>
                </select>

                <br> <br> &nbsp;&nbsp;&nbsp;Observaciones*: 
                <br/>&nbsp; &nbsp;  &nbsp; &nbsp;<textarea name="obs_cheque" id="obs_cheque" rows="3"  maxlength="255" cols="64"><?php if($cheque!=null && $cheque->getObservaciones()!= null){	echo($cheque->getObservaciones());  }else{ $daoPre = new PresupuestoDaoJdbc(); $p = new Presupuesto(); $p = $daoPre->obtieneElemento($idSolicitud);if($p!= null && $p->getObservaciones() != null){ echo($p->getObservaciones());	}}?></textarea>

               <br/><br/>&nbsp;&nbsp;&nbsp;Período* <select name="periodo" id="periodo"> 
                <option>Selecciona</option> 
                <?php
                    $daoPer=new PeriodoDaoJdbc();
                    $listaPer = $daoPer->obtieneListado();
                    $elementoPer = new Periodo();

                    foreach($listaPer as $elementoPer){

                        $sel2 = "";
                        if($cheque!= null && $cheque->getIdPeriodo()!= null ){
                            if( $cheque->getIdPeriodo() == $elementoPer->getId() ){ $sel2 = "selected='selected'"; }
                        }
                        else if( (String)(date('Y')) == $elementoPer->getPeriodo()){
                         $sel2 = "selected='selected'";

                        }
                        echo("<option value=".$elementoPer->getId()."  ".$sel2." >".$elementoPer->getPeriodo()."</option>");
                    }
                ?> 
                </select>
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monto :  <?php if(isset($_GET["Fmonto"]) && $_GET["Fmonto"]!= null ) { echo("$".$_GET["Fmonto"]); } else { echo("$".number_format($cheque->getMonto(),2)); } ?>
              <input type="hidden" name="monto" id="MontoCheque" value="<?php /*if(cheque!=null  && cheque.getMonto()!=null) out.println( cheque.getMonto()); else*/ if(isset($_GET["monto"])){ echo( $_GET["monto"]); } ?>" />            
					
                <br/><br/>&nbsp;&nbsp;&nbsp;Monto  con Letra*:
                <br/>&nbsp; &nbsp;  &nbsp; &nbsp;
                <textarea name="monto_letra" id="monto_letra" rows="3"  maxlength="255" cols="64" readonly='readonly'> <?php if($cheque!=null && $cheque->getMontoLetra()!= null) echo($cheque->getMontoLetra()); ?> </textarea>   
					
                <br/><br/>&nbsp;&nbsp;&nbsp;Área de quién Solicita*: <select style="width:190px" name="areaEmpleado" id="areaEmpleado" >
                <option value="0" >Selecciona</option> 
                <?php
                    $daoArea = new AreaDaoJdbc();
                    $listaArea =$daoArea->obtieneAreas();
                    $Area=new Area();

                    foreach($listaArea as $Area){
                        $sel3 = "";
                        if($cheque!=null && $cheque->getArea()!=null  ){
                            if( $cheque->getArea() == $Area->getId()){ $sel3 = "selected='selected'"; }
                        }
                     echo("<option value=".$Area->getId()."  ".$sel3." >".$Area->getDescripcion()."</option>");
                    }
                ?>
                <br/>					
            </td>
	</tr>
	
	<tr>
	    <td align="center" colspan="6" >
	    <?php if( $id<3 || $id>4 ){ ?>
	         <input name="guardar" style="cursor:pointer" type="submit" value="Guardar"  class='btn'> &nbsp; &nbsp; &nbsp;
		 <?php }?>
		   <input name="cancelar" style="cursor:pointer" type="button" value="Regresar"  onclick="Regresar()"  class='btn'>
		 </td>
	</tr>
	</table>
    	
	  <?php
              if($cheque!=null && $cheque->getId()!=null){ echo("<input type='hidden' name='id' id='idCheque' value='".$cheque->getId()."' />"); }
	     
	 ?>
   </form>    
</div>
<br/><br/>
</body>
</html>
<script>
Calendar.setup({ inputField : "fecha", ifFormat : "%d-%m-%Y", button:"fecha" }); 

var frmvalidator  = new Validator("frmImprimirCheque");

frmvalidator.addValidation("folio","req","Por favor capture el folio del cheque.");
frmvalidator.addValidation("poliza","req","Por favor capture el número de póliza.");
frmvalidator.addValidation("fecha","req","Por favor capture la fecha.");
frmvalidator.addValidation("periodo","dontselect=0","Por favor seleccione el periodo.");
frmvalidator.addValidation("monto_letra","req","Por favor capture el monto con letra.");
frmvalidator.addValidation("areaEmpleado","dontselect=0","Por favor seleccione el área de quién solicita.");


function Regresar(){
	window.location="SolicitudDn.php";
}
</script>
