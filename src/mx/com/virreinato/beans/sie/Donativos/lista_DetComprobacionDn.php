<?php

include_once("../src/mx/com/virreinato/dao/DetPresupuestoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/DetPresupuesto.class.php");
include_once("../src/mx/com/virreinato/dao/CatChequeDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatCheque.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html>

<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
<title>Detalle de Solicitud de Comprobacion</title>
</head>
<body>
<div class="contenido">
    <table width="60%" border="0" cellspacing="0" cellpadding="5" class='tb_cat' align='center' >

    <tr bgcolor="#9FB1CB">
                    <th width="20%" align="center">Fecha</th>
                    <th width="40%" align="center">Descripcion</th>
                    <th width="10%" align="center">Monto</th>
                    <th width="2%"></th>
                    <th width="2%"></th>
    </tr>

        <?php	  	
            $monto = $elemento->getMontoComprobacion();
            $dao=new DetComprobacionDaoJdbc();
            $lista=$dao->obtieneListado($id);
            $elemento = new DetComprobacion();

            $montoComprobaciones=0;
            $aux= $monto;
            $montoTotal= (double)($aux);

            foreach($lista as $elemento)
            {
                $montoComprobaciones+=(double)$elemento->getMonto();
            }

            foreach($lista as $elemento)
            {
	 ?>
    
         <tr class="SizeText">
            <td align="center"><?php echo( $elemento->getFecha()); ?>	</td>
            <td align="center"><?php echo( $elemento->getDescripcion() );?></td>
            <td align="center"><?php echo( "$".number_format( $elemento->getMonto(),2) ); ?></td>
            <td><a href='Agregar_DetComprobacionDn.php?idC=<?php echo($id); ?>&id=<?php echo($elemento->getIdComprobante());?>&monto=<?php echo($montoTotal-$montoComprobaciones+$elemento->getMonto()); ?>&folio=<?php echo$folio; ?>&cheque=<?php echo$montoTotal; ?>' class='liga_cat'><acronym title="Editar"><img src="../img/Pencil3.png" width="16" height="16" alt="Editar" style="border:0;" /></acronym></a></td>
            <td><a href='../src/mx/com/virreinato/web/CatDetComprobacionDn.php?id=<?php echo($elemento->getIdComprobante());?>&idC=<?php echo($elemento->getIdComprobacion()); ?>' class='liga_cat' onclick='return confirm("¿Esta seguro que desea eliminar este registro?")'><acronym title="Borrar"><img src="../img/DeleteRed.png" width="16" height="16" alt="Borrar" style="border:0;" /></acronym></a></td>
         </tr>
        <?php
        }
        ?>
        <input type="hidden" value="<?php echo($montoTotal-$montoComprobaciones); ?>" id="total" name="total"/>
    </table>
<br/>
<?php  if($montoTotal>$montoComprobaciones) 
    {
?>
        <div align="center"><a href='Agregar_DetComprobacionDn.php?idC=<?php echo($id); ?>&monto=<?php echo($montoTotal-$montoComprobaciones); ?>&folio=<?php echo$folio; ?>' class='liga_btn'>Agregar Comprobante</a></div>
<?php 
    }
?>	
</div>
<br/><br/>	
</body>
</html>
</body>

