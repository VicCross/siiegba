<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once("../src/mx/com/virreinato/dao/PersonalProyDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/PersonalProy.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<body>
<?php $respuesta = NULL;
    if(isset($_GET['respuesta'])){ $respuesta = $_GET['respuesta']; }?>  
<div class="contenido">
   <br/>
	<p class="titulo_cat1"> Personal de Proyectos</p>
	<h2 class="titulo_cat2">Catálogo de Personal de Proyectos</h2>
    <?php 
    if($respuesta!=null){
        echo("<div align='center' class='msj'>".$respuesta."</div>");
    }
    ?>
    <table width="90%" border="0" cellspacing="0" cellpadding="5" class='tb_cat' align='center' >
        <tr>
          <th width="10%">RFC</th>
          <th width="8%">CURP</th>
          <th width="15%" >Nombre</th>
          <th width="15%">Apellido Paterno</th>
          <th width="15%">Apellido Materno</th>
          <th width="2%" ></th>
          <th width="2%" ></th>
        </tr>
        <?php
        $dao = new PersonalProyDaoJdbc();
        $lista=$dao->obtieneListado();
        $elemento=new PersonalProy();
 
        foreach($lista as $elemento){
                ?>
        <tr>
            <td align="center" ><?php echo($elemento->getRfc());?></td>
            <td align="center"><?php echo($elemento->getCurp());?></td>
           <td align="center"><?php echo($elemento->getNombre());?></td>
           <td align="center"><?php echo($elemento->getApp());?></td>
           <td align="center"><?php echo($elemento->getApm());?></td>
           <td align="center"><a href='agrega_personalProy.php?id=<?php echo($elemento->getId());?>' class='liga_cat'><acronym title="Editar"><img src="../img/Pencil3.png" width="" height="" alt="Editar" style="border:0;" /></acronym> </a></td>
           <td align="center"><a href='../src/mx/com/virreinato/web/CatPersonalProy.php?id=<?php echo($elemento->getId());?>' class='liga_cat' onclick='return confirm("¿Esta seguro que desea eliminar este registro?")'><acronym title="Borrar"><img src="../img/DeleteRed.png" width="" height="" alt="Borrar" style="border:0;" /></acronym></a></td>
     </tr>	
   <?php			
		}
	  ?>
        </table>
	<br>
	<div align="center"><a href='agrega_personalProy.php' class='liga_btn'> Agrega Personal de Proyectos</a></div>
</div>
<?php include 'footer.php' ?>
</body>
</html>