<?php

include_once("../src/mx/com/virreinato/dao/LineaAccionDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/dao/EjeDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/LineaAccion.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Eje.class.php");
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
</head>
<body>
 <?php
    $error = NULL;
    if (isset($_REQUEST['error'])) {
        $error = $_REQUEST['error'];
    }
    ?>
<div class="contenido">
    <p class="titulo_cat1">Catálogos > Catálogos de Proyectos >
    <?php
	$dao=new LineaAccionDaoJdbc();
	$elemento=new LineaAccion();

	if(isset($_GET['id'])){
		echo("Modificar Línea de Acción");
		$elemento=$dao->obtieneElemento($_GET['id']);	
	}	
	else{
		echo("Alta de Nueva Línea de Acción");
	}	
    ?>
    </p>
    <br/>
    <?php if ($error != null) echo("<div align='center' class='msj'>" . $error . "</div>"); ?>
    <form id="flinea" name="flinea" method="post" action="../src/mx/com/virreinato/web/CatLineaAccion.php">
        <table width="60%%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
          <tr>
            <td width="19%">Eje*:</td>
            <td width="81%">
              <select name="id_eje" id="id_eje">
              <option value='0'>Selecciona</option>
              <?php
                $eje=new EjeDaoJdbc();
                $ejes=$eje->obtieneListado(); 
                $b = new Eje();
                
                foreach ($ejes as $b){
                  $sel="";
                  if($elemento!= null && $elemento->getEje()!= null ){
                          $aux = $elemento->getEje();
                          if($b->getId()==$aux->getId())
                                  $sel="selected='selected'";
                  }  
                  echo("<option value='".$b->getId()."' ".$sel." >".$b->getEje()."</option>");
                }
             ?>
            </select>
                </td>
          </tr>
          <tr>
            <td>Línea de Acción*:</td>
            <td><input type="text" name="linea" id="linea" value='<?php if($elemento!=null && $elemento->getLineaAccion()!= null) echo($elemento->getLineaAccion());?>' /></td>
          </tr>
          <tr>
            <td>Descripción*:</td>
            <td><input name="descripcion" id='descripcion' type="text" size="50" value='<?php if($elemento!=null && $elemento->getDescripcion()!= null) echo($elemento->getDescripcion());?>'/></td>
          </tr>
          <tr>
            <td align="center" colspan="2"><input name="guardar" type="submit" value="Guardar"  class='btn' />
            &nbsp;&nbsp;&nbsp;&nbsp;<a href='lista_linea_accion.php' class='liga_btn'> Cancelar </a>
            </td>
          </tr>
        </table>
        <?php if($elemento!=null && $elemento->getId()!=null) echo("<input type='hidden' name='id' value='".$elemento->getId()."' />");?>
    </form>
</div>
<?php include 'footer.php' ?>
<script type="text/javascript">
 var frmvalidator  = new Validator("flinea");
 frmvalidator.addValidation("id_eje","dontselect=0","Por favor seleccione el eje.");
 frmvalidator.addValidation("linea","req","Por favor capture la línea de acción.");
 frmvalidator.addValidation("descripcion","req","Por favor capture la línea de acción.");
 </script>

</body>
</html>