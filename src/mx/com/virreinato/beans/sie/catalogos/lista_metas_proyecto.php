<?php
session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once("../src/mx/com/virreinato/dao/ProyectoMetaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProyectoMeta.class.php");
include_once("../src/mx/com/virreinato/dao/ProgramaInstitucionalDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProgramaOperativo.class.php");
include_once("../src/mx/com/virreinato/dao/ProgramaOperativoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProgramaInstitucional.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<body>
<?php 
   $respuesta = NULL;
    if(isset($_GET['respuesta'])){ $respuesta = $_GET['respuesta']; }
   $idProyecto = "";
    if(isset($_GET['idProyecto'])){ $idProyecto = $_GET['idProyecto']; }
    $desc = NULL;
    if(isset($_GET['desc'])){ $desc = $_GET['desc']; }
?>
<div class="contenido">
	<br/>
	<p class="titulo_cat1">Cat&aacute;logos > Cat&aacute;logos de Proyectos > <a class="linkTitulo_cat1" href="agrega_proyecto.php?id=<?php $idProyecto ?>" > Definición de  Proyecto</a> > Metas de Proyectos </p>
	<p class="titulo_cat2" align="left" > Proyecto: <?php echo $desc; ?> </p>
    <?php 
        if($respuesta!=null){
            echo("<div align='center' class='msj'>".$respuesta."</div>");
        }
    ?>
	<br/>
	<form name="frmMetas" id="frmMetas" method="post" action="../src/mx/com/virreinato/web/WebProyectoMeta.php"> 
        <table width="40%" border="0" cellspacing="0" cellpadding="5" class='tb_cat' align='center' id="metasProy" >
            <tr>
            <th align="center" width="40%">Metas</th>
            <th width="20%">Programa Institucional</th>
            <th width="20%">Programa Operativo</th>
            <th width="10%"></th>
            <th width="10%"></th>
        </tr>
        <?php
        $dao = new ProyectoMetaDaoJdbc();
        $lista=$dao->obtenerListado($idProyecto);
        $elemento=new ProyectoMeta();
        $indice = 0;
 
        foreach($lista as $elemento){
           ?>
         <tr class="SizeText">
            <td align="center" id="<?php echo $indice; ?>" ><?php echo$elemento->getDescripcion();?></td>
            <td align="center" id="programaInst<?php echo $indice; ?>"><?php echo$elemento->getProgramaInstitucional();?></td>
            <td align="center" id="programaOp<?php echo $indice; ?>"><?php echo$elemento->getProgramaOperativo();?></td>
            <td align="center" id="editMeta<?php echo $indice; ?>"><a style="cursor:pointer" class='liga_cat' onclick="Modificar('<?php echo$indice; ?>','<?php echo$elemento->getIdMeta(); ?>','<?php echo$elemento->getIdProgramaInstitucional(); ?>','<?php echo$elemento->getIdProgramaOperativo(); ?>')"  ><acronym title="Editar"><img src="../img/Pencil3.png" width="16" height="16" alt="Editar" style="border:0;" /></acronym> </a></td>
            <td align="center" id="deleteMeta<?php echo $indice; ?>"><a href='../src/mx/com/virreinato/web/WebProyectoMeta.php?id=<?php echo($elemento->getIdMeta());?>&idProyecto=<?php echo($elemento->getIdProyecto()); ?>&desc=<?php echo($desc); ?>' class='liga_cat' onclick='return confirm("¿Esta seguro que desea eliminar este registro?")'><acronym title="Borrar"><img src="../img/DeleteRed.png" width="16" height="16" alt="Borrar" style="border:0;" /></acronym></a></td>
            <td colspan="2" id="modificarMetaProyecto<?php echo $indice; ?>" style="display:none"><input name="guardar" type="submit" value="Guardar"  class='btn' /></td>			    
        </tr>	
        <?php  $indice++;
        } ?>  
        </table>
	</form>
	<br>
	<div align="center">
	   <a href='agrega_proyecto.php?id=<?php echo$idProyecto; ?>' class='liga_btn'> Regresar </a>
	   &nbsp; &nbsp; &nbsp;
	   <a style="cursor:pointer" onclick="Nueva()"  class='liga_btn'> Agregar Nueva Meta</a>
	</div>
</div>
</body>
<script>
   function Modificar(value,id,idProgramaInst,idProgramaOp ){
           //alert("Hola "+value+" "+id+" "+idProgramaInst+" "+idProgramaOp);
	   var idProyecto = <?php echo $idProyecto.";" ?>
	   var texto = $("#"+value+" ").text();
	   var programaInst = $("#programaInst"+value).text();
	   var programaOp = $("#programaOp"+value).text();	   	   	   
	   var tds1 = ' <input type="text" id="meta" name="meta" size="40" value="'+texto+'"  /> ';
           
	   var tds2 = '<select name="programa_inst" id="programa_inst"> <option>Seleccione</option>';            
	 <?php
  			$daoInst = new ProgramaInstitucionalDaoJdbc();
 			$listaInst = $daoInst->obtieneListado();
 			$elementoInst=new ProgramaInstitucional();
 	  		
 	  		foreach($listaInst as $elementoInst){
 		  		?>
		  		if(<?php echo$elementoInst->getIdProgramaInstitucional()?> == idProgramaInst)
		  			tds2 +='<option value="<?php echo$elementoInst->getIdProgramaInstitucional() ?>" selected="selected" ><?php echo$elementoInst->getProgramaInstitucional() ?></option>';
				else
					tds2 +='<option value="<?php echo$elementoInst->getIdProgramaInstitucional() ?>" ><?php echo$elementoInst->getProgramaInstitucional() ?></option>';
				<?php
 	  		}	
 		?>
	   tds2 += '</select>';
	   
	   var tds3 = ' <select name="programa_op" id="programa_op"> <option>Seleccione</option>';
	   <?php
	   		$daoOpe = new ProgramaOperativoDaoJdbc();
 			$listaOpe = $daoOpe->obtieneListado();
 			$elementoOpe=new ProgramaOperativo();
 	  		
                         foreach ($listaOpe as $elementoOpe) {
  
 		  		?>
 		  		if(<?php echo$elementoOpe->getIdProgramaOperativo()?>== idProgramaOp)
		  			tds3 +='<option value="<?php echo$elementoOpe->getIdProgramaOperativo()?>" selected="selected" ><?php echo$elementoOpe->getProgramaOperativo(); ?></option>';
		  		else
		  			tds3 +='<option value="<?php echo$elementoOpe->getIdProgramaOperativo()?>" ><?php echo$elementoOpe->getProgramaOperativo(); ?></option>';	
				<?php
 	  		}	
 		?>
	   
	   
	   tds3 += '</select><input type="hidden" name="id" value="'+id+'" /> <input type="hidden" name="idProyecto" value="'+idProyecto+'"/>';
	   
	   $("#"+value+" ").text("");
	   $("#"+value+" ").html(tds1);
	   
	   $("#programaInst"+value).text("");
	   $("#programaInst"+value).html(tds2);
	   
	   $("#programaOp"+value).text("");
           
	   $("#programaOp"+value).html(tds3);
	   
	   $("#editMeta"+value).hide();
	   $("#deleteMeta"+value).hide();
	   $("#modificarMetaProyecto"+value).show();
	   	   
   }
   
   function Nueva(){
	   var n = $('tr:last td', $("#metasProy")).length;
	   var idProyecto = <?php echo$idProyecto ?>
	   
	   var tds = '<tr class="SizeText"><td align="center"><input type="text" id="meta" name="meta" size="40" maxlenght="10"  /></td> <td> <select name="programa_inst" id="programa_inst"> <option>Seleccione</option>';
	   
	   <?php
  			$daoIns = new ProgramaInstitucionalDaoJdbc();
 			$listaIns = $daoIns->obtieneListado();
 			$elementoIns=new ProgramaInstitucional();
 	  		
 	  		foreach($listaIns as $elementoIns){
 		  		?>
		  		tds +='<option value="<?php echo$elementoIns->getIdProgramaInstitucional()?>" ><?php echo$elementoIns->getProgramaInstitucional()?></option>';
				<?php
 	  		}	
 		?>
	   tds += '</select></td><td> <select name="programa_op" id="programa_op"> <option>Seleccione</option>';
	   <?php
	   		$daoOp = new ProgramaOperativoDaoJdbc();
 			$listaOp = $daoOp->obtieneListado();
 			$elementoOp=new ProgramaOperativo();
 	  		
 	  		foreach($listaOp as $elementoOp){
 		  		?>
		  		tds +='<option value="<?php echo$elementoOp->getIdProgramaOperativo()?>" ><?php echo$elementoOp->getProgramaOperativo()?></option>';
				<?php
 	  		}	
 		?>
	   
	   tds +='</select></td><td colspan="2" ><input type="submit" id="guardar" name="guardar" value="Guardar" class="btn" /> <input type="hidden" name="idProyecto" value="'+idProyecto+'"/> </td></tr>';
	   $("#metasProy").append(tds);
	}
   
   function Add(key){
	    var unicode
	    if (key.charCode){ unicode=key.charCode;}
	    else {unicode=key.keyCode;}
	    //alert(unicode); // Para saber que codigo de tecla presiono , descomentar
	 
	    if (unicode == 13){
	    	//Enter 
	    }
	    
   }
   
  
</script>
</html>