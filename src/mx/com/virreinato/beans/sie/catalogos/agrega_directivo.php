<?php
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/CentroCostosDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CentroCostos.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Directivo.class.php");
include_once("../src/mx/com/virreinato/dao/DirectivoDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<body>
<?php
    $error = NULL;
    if (isset($_REQUEST['error'])) {
        $error = $_REQUEST['error'];
    }
    ?> 
<div class="contenido"> 
	<p class="titulo_cat1">Catálogos > Catálogos de Estructura >
<?php
    $dao=new DirectivoDaoJdbc();
    $elemento=new Directivo();

    if (isset($_GET['id'])){
            echo("Modificar Directivo");
            $elemento=$dao->obtieneElemento($_GET['id']);	
    }	
    else{
            echo("Alta de Nueva Directivo");
    }	
    ?>
    </p>
    <br/>       
    <?php if ($error != null) echo("<div align='center' class='msj'>" . $error . "</div>"); ?>
    <form id="fdirectivo" name="fdirectivo" method="post" action="../src/mx/com/virreinato/web/CatDirectivo.php">
		<table width="90%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
		  <tr>
		    <td width='14%'>Nombre(s)*:</td>
		    <td width='20%'><input type="text" name="nombre" id="nombre" size='30' value='<?php if($elemento!=null && $elemento->getNombre()!= null) echo($elemento->getNombre());?>'/></td>
		    <td width='13%'>Apellido Paterno*:</td>
		    <td width='20%'><input type="text" name="apaterno" id="apaterno" size='30' value='<?php if($elemento!=null && $elemento->getApPaterno()!= null) echo($elemento->getApPaterno());?>'/></td>
		    <td width='13%'>Apellido Materno*:</td>
		    <td width='20%'><input type="text" name="amaterno" id="amaterno" size='30' value='<?php if($elemento!=null && $elemento->getApMaterno()!= null) echo($elemento->getApMaterno());?>'/></td>
		  </tr>
		  		
		  <tr>
		    <td>Cargo*:</td>
		    <td><input type="text" name="cargo" id="cargo" size='30' value='<?php if($elemento!=null && $elemento->getCargo()!= null) echo($elemento->getCargo());?>' /></td>
		    <td>Tipo Cargo*:</td>
		    <td>
			   <select name="tipo_cargo" id="tipo_cargo">
			      <option value='0'>Selecciona</option>
			      <option value='1' <?php if($elemento!=null && $elemento->getTipoCargo()!= null &&  $elemento->getTipoCargo()==1) echo("selected='selected'");?> >Dirección</option>
			      <option value='2' <?php if($elemento!=null && $elemento->getTipoCargo()!= null &&  $elemento->getTipoCargo()==2) echo("selected='selected'");?> >Subdirección Administrativa</option>
			      <option value='3' <?php if($elemento!=null && $elemento->getTipoCargo()!= null &&  $elemento->getTipoCargo()==3) echo("selected='selected'");?> >Subdirección Técnica</option>
			      <option value='4' <?php if($elemento!=null && $elemento->getTipoCargo()!= null &&  $elemento->getTipoCargo()==4) echo("selected='selected'");?> >Jefe de Recursos Financieros</option>
			      <option value='5' <?php if($elemento!=null && $elemento->getTipoCargo()!= null &&  $elemento->getTipoCargo()==5) echo("selected='selected'");?> >Asistente de Recursos Financieros</option>
			   </select>
		    </td>
		    <td>Iniciales*:</td>
		    <td><input type="text" name="iniciales" id="iniciales" size='30' value='<?php if($elemento!=null && $elemento->getIniciales()!= null) echo($elemento->getIniciales());?>'/></td>
		  </tr>
		 
		  <tr>
		    <td>Centro de Costos*:</td>
		    <td colspan='5'>
			 <select name="id_ccosto" id="id_ccosto">
		      <option value='0'>Selecciona</option>
		      <?php
	      		$ccosto=new CentroCostosDaoJdbc();
                        $ccostos=$ccosto->obtieneListado(); 
                        
                        foreach($ccostos as $c) {
                          $sel="";
                          if($elemento!= null && $elemento->getCcostos()!= null ){
                                  if($c->getId()==$elemento->getCcostos()->getId())
                                          $sel="selected='selected'";
                          }
                          echo("<option value='".$c->getId()."' ".$sel." >".$c->getClave()." ".$c->getDescripcion()."</option>");
                        }
                        ?>
		      </select>
			 </td>
		  </tr>
		 
		 		 		  
		  <tr>
		    <td align="center" colspan="6" >
		    	<input name="guardar" type="submit" value="Guardar"  class='btn' />
		    	&nbsp;&nbsp;&nbsp;&nbsp;<a href='lista_directivo.php' class='liga_btn'> Cancelar </a>
		    </td>
		  </tr>
		  
		</table>
		<?php if($elemento!=null && $elemento->getId()!=null) echo("<input type='hidden' name='id' value='".$elemento->getId()."' />");?>
	</form>
</div>
<?php include 'footer.php' ?>
<script type="text/javascript">
 var frmvalidator  = new Validator("fdirectivo");
 frmvalidator.addValidation("nombre","req","Por favor capture el nombre del directivo.");
 frmvalidator.addValidation("apaterno","req","Por favor capture el apellido paterno del directivo.");
 frmvalidator.addValidation("amaterno","req","Por favor capture el apellido materno del directivo.");
 frmvalidator.addValidation("cargo","req","Por favor capture el cargo del directivo.");
 frmvalidator.addValidation("tipo_cargo","dontselect=0","Por favor seleccione el tipo de cargo.");
 frmvalidator.addValidation("iniciales","req","Por favor capture las iniciales del directivo.");
 frmvalidator.addValidation("id_ccosto","dontselect=0","Por favor seleccione el centro de costos.");
 </script>
</body>
</html>


