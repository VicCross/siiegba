<?php
session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once("../src/mx/com/virreinato/dao/UsuarioDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Usuario.class.php");
include_once("../src/mx/com/virreinato/dao/PerfilDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Perfil.class.php");
include_once("../src/mx/com/virreinato/dao/PersonaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Persona.class.php");
include_once("../src/mx/com/virreinato/dao/ProyectoMetaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProyectoMeta.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sistema de Ingresos y Egresos</title>
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
        <script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
        <script>
		$(document).ready(function() {                    
			$("#Perfil").css({
				background: "<?php echo $parametro->getValor(); ?>"
			});
			$(".div_menu").css({
				background: "<?php echo $parametro->getValor(); ?>"
			});
			$(".tb_presupuesto th").css({
				background: "<?php echo $parametro->getValor(); ?>"
			});
			$(".tb_presupuestoResumen th").css({
				background: "<?php echo $parametro->getValor(); ?>"
			});
			$(".tb_cat th").css({
				background: "<?php echo $parametro->getValor(); ?>"
			});
			$(".tb_add_cat").css({
				background: "<?php echo $parametro->getValor(); ?>"
			});
		});
        </script>
    </head>
    <body>
        <?php
        $error = NULL;
        $respuesta = NULL;
        if (isset($_REQUEST['error'])) {
            $error = $_REQUEST['error'];
        }
        if (isset($_REQUEST['respuesta'])) {
            $error = $_REQUEST['respuesta'];
        }
        $perfilUsuario = 0;
        ?>
        <div class="contenido"> 
            <br/>
            <p class="titulo_cat1">Catálogos > Catálogos de Estructura >
                <?php
                $dao = new UsuarioDaoJdbc();
                $elemento = new Usuario();

                if (isset($_GET['id'])) {
                    echo("Modificar Usuario");

                    $elemento = $dao->obtieneElemento($_GET['id']);
                    $perfil = $elemento->getPerfil();
                    $perfilUsuario = $perfil->getId();                   
                } else {
                    echo("Alta de Nuevo Usuario");
                }
                ?>
            </p>
            <br/>
            <?php if ($error != null) echo("<div align='center' class='msj'>" . $error . "</div>"); ?>
            <?php if ($respuesta != null) echo("<div align='center' class='msj'>" . $respuesta . "</div>"); ?>
            <form id="fusuario" name="fusuario" method="post" action="../src/mx/com/virreinato/web/CatUsuario.php">
                <table width="65%" border="0" cellspacing="0" cellpadding="5" class='tb_add_cat' align='center' >
                    <tr>
                        <td class="SizeText" width="20%" id="tbPersona" ><br/>&nbsp; &nbsp;Persona*: </td>
                        <td><br/>
                            <?php
                                $persona = new PersonaDaoJdbc();
                                $personas = array();
                                if (isset($_GET['id'])) {
                                    $personas = $persona->obtieneListado();
                                } else {
                                    $personas = $persona->obtieneListadoSinUsuario();
                                }
                                
                            ?>
                            <select name="id_persona" id="id_persona" style="width:170px">
                                <option value='0'>Selecciona</option>
                                <?php                                
                                $p = new Persona();
                                foreach ($personas as $p) {
                                    $sel = "";
                                    if ($elemento != null && $elemento->getPersona() != null) {
                                        $aux = $elemento->getPersona();
                                        if ($p->getId() == $aux->getId()) {
                                            $sel = "selected='selected'";
                                        }
                                    } else if (isset($_REQUEST['id_persona'])) {

                                        if ($p->getId() == ((int) ((String) $_REQUEST['id_persona']))) {
                                            $sel = "selected='selected'";
                                        }
                                    }
                                    echo("<option value='" . $p->getId() . "' " . $sel . " >" . $p->getNombre() . " " . $p->getApPaterno() . " " . $p->getApMaterno() . "</option>");
                                }
                                ?>
                            </select>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="SizeText"  >&nbsp; &nbsp;Perfil*:</td>
                        <td width="20%" >
                            <select name="id_perfil" id="id_perfil" onchange='muestra_metas()' style="width:170px">
                                <option value='0'>Selecciona</option>
                                <?php
                                $perfil = new PerfilDaoJdbc();
                                $perfiles = $perfil->obtieneListado();
                                $p = new Perfil();
                                foreach ($perfiles as $p) {
                                    $sel = "";
                                    if ($elemento != null && $elemento->getPerfil() != null) {
                                        $aux2 = $elemento->getPerfil();
                                        if ($p->getId() == $aux2->getId()) {
                                            $sel = "selected='selected'";
                                        }
                                    } else if (isset($_REQUEST['id_perfil'])) {
                                        if ($p->getId() == ((int) ((String) $_REQUEST['id_perfil']))) {
                                            $sel = "selected='selected'";
                                        }
                                    }
                                    echo("<option value='" . $p->getId() . "' " . $sel . " >" . $p->getDescripcion() . "</option>");
                                }
                                ?>
                            </select>
                        </td>

                        <td class="SizeText" id="texto" style="display:none" > &nbsp; &nbsp; Selecciona los proyectos a los que tendrá acceso el usuario: </td>

                    </tr>

                    <tr>
                        <?php
                        $daoMeta = new ProyectoMetaDaoJdbc();
                        $listaMeta = $daoMeta->obtenerListado2();
                        ?>
                        <td class="SizeText"  >&nbsp; &nbsp;Usuario*:</td>
                        <td>
                            <input type="text" name="usr" id="usr" value='<?php if ($elemento != null && $elemento->getUsuario() != null) echo($elemento->getUsuario()); ?>' >
                        </td>

                        <td rowspan="3" valign="top"  id="selMetas" style="display:none" >
                            &nbsp; &nbsp; &nbsp; 
                            <select name="id_metas" id="id_proyectos" size='6' multiple='multiple' style="width:370px">
                                <?php
                                $daoMeta = new ProyectoMetaDaoJdbc();
                                $listaMeta = $daoMeta->obtenerListado2();
                                print_r($listaMeta);
                                $Meta = new ProyectoMeta();
                                foreach ($listaMeta as $Meta) {
                                    $sel = "";
                                    if ($elemento != null && $elemento->getIdMetas() != null) {
                                        if ($elemento->getIdMetas() == ( $Meta->getIdMeta())) {
                                            $sel = "selected='selected'";
                                        }
                                    }
                                    echo("<option value='" . $Meta->getIdMeta() . "' " . $sel . " title='" . $Meta->getNombreProyecto() . "'  >   " . $Meta->getDescripcion() . " </option> ");
                                }
                                ?>
                            </select> 
                        </td>
                    </tr>

                    <tr>
                        <td class="SizeText" >&nbsp; &nbsp;Password*:</td>
                        <td >
                            <input type="password" name="pwd" id="pwd" value='<?php if ($elemento != null && $elemento->getPassword() != null) echo($elemento->getPassword()); ?>'>
                        </td>
                    </tr>

                    <tr>
                        <td class="SizeText" > &nbsp; &nbsp;Confirma password*:</td>
                        <td>
                            <input type="password" name="pwd_cnf" id="pwd_cnf" value='<?php if ($elemento != null && $elemento->getPassword() != null) echo($elemento->getPassword()); ?>'>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="3"  align="center">
                            <input name="guardar" type="submit" value="Guardar" style="cursor:pointer"  class='btn' />
                            &nbsp;&nbsp;&nbsp;&nbsp;<a href='lista_usuario.php' class='liga_btn'> Cancelar </a>
                        </td>
                    </tr>
                </table>
<?php if ($elemento != null && $elemento->getId() != null) echo("<input type='hidden' name='id' value='" . $elemento->getId() . "' />"); ?>
            </form>

        </div>
<?php include 'footer.php' ?>
        <script type="text/javascript">

            $().ready(function() {
                var perfil = '<?php echo$perfilUsuario ?>'
                muestra_metas(perfil);

            });


            function Regresar() {
                window.location = "lista_usuario.php";
            }


            function muestra_metas(perfil) {
                if (perfil == undefined) {
                    perfil = $("#id_perfil").val();
                }

                if (perfil == '7') {
                    $("#selMetas").show();
                    $("#texto").show();
                }
                else {
                    $("#selMetas").hide();
                    $("#texto").hide();
                }


            }

            function Ayuda() {
                alert("Ayuda de lupe!");
            }


            var frmvalidator = new Validator("fusuario");
            frmvalidator.addValidation("id_persona", "dontselect=0", "Por favor seleccione la persona.");
            frmvalidator.addValidation("id_perfil", "dontselect=0", "Por favor seleccione el perfil de usuario.");
            frmvalidator.addValidation("usr", "req", "Por favor capture su usuario");
            frmvalidator.addValidation("pwd", "req", "Por favor capture su contraseña");
            frmvalidator.addValidation("pwd_cnf", "req", "Por favor confirme su contraseña");
            frmvalidator.addValidation("pwd_cnf", "eqelmnt=pwd", "Las contraseñas no coinciden");

        </script>
    </body>
</html>
