<?php
include_once("../src/mx/com/virreinato/dao/DirectivoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Directivo.class.php");
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<body>
 <?php $respuesta = NULL;
        if(isset($_GET['respuesta'])){ $respuesta = $_GET['respuesta']; }?>
 <div class="contenido">
	<p class="titulo_cat1">Catálogos > Catálogos de Estructura > Catálogo de Directivos</p><br/>
<?php 
            if($respuesta!=null){
                echo("<div align='center' class='msj'>".$respuesta."</div>");
            }
        ?>
    <table width="90%" border="0" cellspacing="0" cellpadding="5" class='tb_cat' align='center' >
      <tr>
        <th>Nombre</th>
        <th>Apellido Paterno</th>
        <th>Apellido Materno</th>
        <th>Iniciales</th>
        <th>Cargo</th>
        <th>Tipo de Cargo</th>
        <th>Centro de Costos</th>
        <th></th>
        <th></th>
      </tr>
     <?php
        $dao=new DirectivoDaoJdbc();
        $lista = $dao->obtieneListado();
        $elemento = new Directivo();
            
        foreach($lista as $elemento){
         ?>  
         <tr>
            <td><?php echo($elemento->getNombre()); ?></td>
            <td><?php echo($elemento->getApPaterno()); ?></td>
            <td><?php echo($elemento->getApMaterno()); ?></td>
            <td><?php if($elemento->getIniciales()!=null) {echo($elemento->getIniciales());}?></td>
            <td><?php echo($elemento->getCargo());?></td>
            <td><?php if($elemento->getTipoCargo()!=null && $elemento->getTipoCargo() == 1) {echo("Dirección");} else if($elemento->getTipoCargo()!=null && $elemento->getTipoCargo() == 2){ echo("Subdirección Administrativa");} else if($elemento->getTipoCargo()!=null && $elemento->getTipoCargo() == 3){ echo("Subdirección Técnica");}else if($elemento->getTipoCargo()!=null && $elemento->getTipoCargo() == 4) {echo("Jefe de Recursos Financieros");} else if($elemento->getTipoCargo()!=null && $elemento->getTipoCargo() == 5){ echo("Asistente de Recursos Financieros"); }?></td>
            <td><?php if($elemento->getCcostos()!=null){echo($elemento->getCcostos()->getDescripcion());}?></td>			    
            <td><a href='agrega_directivo.php?id=<?php echo($elemento->getId());?>' class='liga_cat'><img src="../img/Pencil3.png" alt="Editar" style="border: 0px none;" ></a></td>
            <td><a href='../src/mx/com/virreinato/web/CatDirectivo.php?id=<?php echo($elemento->getId());?>' onclick='return confirm("¿Esta seguro que desea eliminar este registro?")' class='liga_cat'><img src="../img/DeleteRed.png" alt="Borrar" style="border: 0px none;"></a></td>
          </tr>	
			<?php			
		}
	  ?>
        </table>
	<br>
	<div align="center"><a href='agrega_directivo.php' class='liga_btn'> Agrega Directivo</a></div>
</div>
<?php include 'footer.php' ?>
</body>
</html>