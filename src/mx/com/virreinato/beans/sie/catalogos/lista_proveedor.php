<?php
session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once("../src/mx/com/virreinato/dao/ProveedorDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Proveedor.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<body>
<?php $respuesta = NULL;
    if(isset($_GET['respuesta'])){ $respuesta = $_GET['respuesta']; }?> 
<div class="contenido">
	<p class="titulo_cat1">Catálogos > Catálogos de Operación  > Catálogo de Proveedores</p><br/>
    <?php 
        if($respuesta!=null){
            echo("<div align='center' class='msj'>".$respuesta."</div>");
        }
    ?>
    <table width="90%" border="0" cellspacing="0" cellpadding="5" class='tb_cat' align='center' >
        <tr>
          <th width="19%">Proveedor</th>
          <th>Domicilio</th>
          <th>Código postal</th>
          <th>Representante</th>
          <th>Teléfono</th>
          <th>Otro Teléfono</th>
          <th>Correo electrónico</th>
          <th>Estatus</th>
          <th></th>
          <th></th>
        </tr>
    <?php
        $dao = new ProveedorDaoJdbc();
        $lista=$dao->obtieneListado();
        $elemento=new Proveedor();
 
        foreach($lista as $elemento){
                ?>
        <tr>
            <td><?php echo($elemento->getProveedor());?></td>
            <td><?php echo($elemento->getDomicilio());?></td>
            <td><?php echo($elemento->getCp());?></td>
            <td><?php if($elemento->getRepresentante()!=null){echo($elemento->getRepresentante());}?></td>
            <td><?php if($elemento->getTelefono()!=null){echo($elemento->getTelefono());}?></td>
            <td><?php if($elemento->getOtroTelefono()!=null ){echo($elemento->getOtroTelefono());}?></td>
            <td><?php if($elemento->getCorreo()!=null){echo($elemento->getCorreo());}?></td>
            <td><?php if($elemento->getEstatus()!=null){
                if($elemento->getEstatus()==1)
                        echo("Activo");
                else if($elemento->getEstatus()==0)
                                echo("Inactivo");
                }?></td>
            <td><a href='agrega_proveedor.php?id=<?php echo($elemento->getId());?>' class='liga_cat'><img src="../img/Pencil3.png" alt="Editar" style="border: 0px none;" ></a></td>
            <td><a href='../src/mx/com/virreinato/web/CatProveedor.php?id=<?php echo($elemento->getId());?>' onclick='return confirm("¿Esta seguro que desea eliminar este registro?")' class='liga_cat'><img src="../img/DeleteRed.png" alt="Borrar" style="border: 0px none;"></a></td>
          </tr>	
    <?php			
    	}
    ?>
    </table>
	<br>
	<div align="center"><a href='agrega_proveedor.php' class='liga_btn'> Agregar Proveedor</a></div>
</div>
<?php include 'footer.php' ?>
</body>
</html>
