<?php
session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once("../src/mx/com/virreinato/dao/LineaAccionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/LineaAccion.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Eje.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<body>
<?php $respuesta = NULL;
    if(isset($_GET['respuesta'])){ $respuesta = $_GET['respuesta']; }?>  
<div class="contenido">
    <p class="titulo_cat1">Catálogos > Catálogos de Proyectos >Catálogo de Líneas de Acción</p><br/>
    <?php 
        if($respuesta!=null){
            echo("<div align='center' class='msj'>".$respuesta."</div>");
        }
    ?>
    <table width="60%" border="0" cellspacing="0" cellpadding="5" class='tb_cat' align='center' >
	  <tr>
	    <th>Eje</th>
	    <th>Línea de Acción</th>
	    <th>Descripción</th>
	    <th></th>
	    <th></th>
	  </tr>
    <?php
        $dao = new LineaAccionDaoJdbc();
        $lista=$dao->obtieneListado();
        $elemento=new LineaAccion();
 
        foreach($lista as $elemento){
                ?>
    <tr>
        <td><?php 
        $aux = $elemento->getEje();
        echo($aux->getEje());?></td>
        <td><?php echo($elemento->getLineaAccion());?></td>
        <td><?php if($elemento->getDescripcion()!=null){echo($elemento->getDescripcion());}?></td>
        <td><a href='agrega_linea_accion.php?id=<?php echo($elemento->getId());?>' class='liga_cat'><img src="../img/Pencil3.png" alt="Editar" style="border: 0px none;" ></a></td>
        <td><a href='../src/mx/com/virreinato/web/CatLineaAccion.php?id=<?php echo($elemento->getId());?>' onclick='return confirm("¿Esta seguro que desea eliminar este registro?")' class='liga_cat'><img src="../img/DeleteRed.png" alt="Borrar" style="border: 0px none;"></a></td>
      </tr>	
            <?php			
		}
	  ?>
        </table>
	<br>
	<div align="center"><a href='agrega_linea_accion.php' class='liga_btn'> Agregar Línea de Acción</a></div>
</div>
<?php include 'footer.php' ?>
</body>
</html>
