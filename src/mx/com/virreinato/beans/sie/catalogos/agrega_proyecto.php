<?php

include_once("../src/mx/com/virreinato/dao/CatProyectoDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/dao/AreaDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/dao/AreaNormativaDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/dao/CentroCostosDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/dao/EmpleadoDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/dao/LineaAccionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatProyecto.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Area.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/AreaNormativa.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CentroCostos.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Eje.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Empleado.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/LineaAccion.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<link href="../css/calendario.css" type="text/css" rel="stylesheet">
<script src="../js/calendar.js" type="text/javascript"></script>
<script src="../js/calendar-es.js" type="text/javascript"></script>
<script src="../js/calendar-setup.js" type="text/javascript"></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<body>
 <?php
    $error = NULL;
    if (isset($_REQUEST['error'])) {
        $error = $_REQUEST['error'];
    }
    ?>
<div class="contenido"> 
    <br/>
	<p class="titulo_cat1"> Catálogos > Catálogos de Proyectos >
    <?php
	$dao=new CatProyectoDaoJdbc();
	$elemento=new CatProyecto();

	if(isset($_GET['id'])){
		echo("Modificar Proyecto");
		$elemento=$dao->obtieneElemento($_GET['id']);	
	}	
	else{
		echo("Alta de Nuevo Proyecto");
	}	
    ?>
    </p>
    <br/>
    <?php if ($error != null) echo("<div align='center' class='msj'>" . $error . "</div>"); ?>
    <form id="fproyecto" name="fproyecto" method="post" action="../src/mx/com/virreinato/web/CatProyecto.php">
        <table width="60%%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
          <tr>
                <td width="%">Número de Proyecto*:</td>
            <td width="%">
              <input type="text" name="numero" id="numero" size="20" maxlength='40' value='<?php if($elemento!=null && $elemento->getNumero()!= null) echo($elemento->getNumero());?>' />
                </td>

          <td width="%">Descripción*:</td>
            <td width="%" colspan='3'>
              <input type="text" name="descripcion" id="descripcion" size="100" maxlength='255' value='<?php if($elemento!=null && $elemento->getDescripcion()!= null) echo($elemento->getDescripcion());?>' />
                </td>

        </tr>


         <tr>
            <td>Periodo*:</td>
            <td>
            
             <select name="id_periodo" id="id_periodo" style="width:260px">
                <option value='0'>Selecciona</option>
                 <?php
                    $periodo=new PeriodoDaoJdbc();
                    $periodos=$periodo->obtieneListadoAbierto(); 
                    $b = new Periodo();

                    foreach($periodos as $b){
                       $sel="";
                        if($elemento!= null && $elemento->getPeriodo()!= null ){
                              $aux = $elemento->getPeriodo();
                              if($b->getId() == $aux->getId())
                              {  $sel="selected='selected'"; }
                      }
                      echo("<option value='".$b->getId()."' ".$sel." >".$b->getPeriodo()."</option>");
                    }
                ?>
                </select>
		    </td>
		    
		  	<td>Fecha de inicio total*:</td>
		    <td>
                        <input name="finicio_tot" id="finicio_tot" type="text" size="20" maxlength='10' value='<?php if($elemento!=null && $elemento->getFechaIniProyecto()!= null) {echo(date("d-m-Y",strtotime($elemento->getFechaIniProyecto()))); }?>'/>
		    </td>
		    
		    <td>Fecha final total*:</td>
		    <td>
		    <input name="ffin_tot" id="ffin_tot" type="text" size="20" maxlength='10' value='<?php if($elemento!=null && $elemento->getFechaFinProyecto()!= null) { echo(date("d-m-Y",strtotime($elemento->getFechaFinProyecto()))); }?>'/>
		    </td>
		
		  </tr>
		    
		  <tr>
		  	<td>Tipo de proyecto*:</td>
		    <td>
		      <select name="tipo" id="tipo" style="width:260px">
		      <option value='0'>Selecciona</option>
                      <option value='PE' <?php if($elemento!=null && $elemento->getTipoproy()!= null && strtoupper($elemento->getTipoproy()) =="PE") echo("selected='selected'");?> >Permanente</option>
		      <option value='NC' <?php if($elemento!=null && $elemento->getTipoproy()!= null &&  strtoupper($elemento->getTipoproy()) =="NC") echo("selected='selected'");?>>Nueva Creación</option>
		      <option value='FT' <?php if($elemento!=null && $elemento->getTipoproy()!= null &&  strtoupper($elemento->getTipoproy()) =="FT") echo("selected='selected'");?>>Con Fecha de Término</option>
		      </select>
		    </td>
		
		    <td>Fecha de inicio en el ejercicio*:</td>
		    <td>
		    <input name="finicio_eje" id="finicio_eje" type="text" size="20" maxlength='10' value='<?php if($elemento!=null && $elemento->getFechaIniEjercicio()!= null) { echo(date("d-m-Y",strtotime($elemento->getFechaIniEjercicio()))); }?>'/>
		    </td>
		    <td>Fecha final en el ejercicio*:</td>
		    <td>
		    <input name="ffin_eje" id="ffin_eje" type="text" size="20" maxlength='10' value='<?php if($elemento!=null && $elemento->getFechaFinEjercicio()!= null) { echo(date("d-m-Y",strtotime($elemento->getFechaFinEjercicio()))); }?>'/>
		    </td>	    
		    
		  </tr>
				  
		  <tr>
		  	<td width="%">Área*:</td>
		    <td width="%">
		      <select name="id_area" id="id_area" style="width:260px">
		      <option value='0'>Selecciona</option>    
                    <?php
                        $area=new AreaDaoJdbc();
                        $areas=$area->obtieneAreas();
                        $a = new Area();
                            foreach($areas as $a){
                              $sel="";
                              if($elemento!= null && $elemento->getArea()!= null ){
                                      $aux3 = $elemento->getArea();
                                      if($a->getId() == $aux3->getId())
                                      {        $sel="selected='selected'"; }
                              }
                              echo("<option value='".$a->getId()."' ".$sel." >".$a->getDescripcion()."</option>");
                            }
                        ?>
                    </select>
			</td>
		  	 <td width="%">Área Normativa*:</td>
		    <td width="%">
		      <select name="id_arean" id="id_arean" style="width:260px" >
		      <option value='0'>Selecciona</option>
            <?php
                $arean=new AreaNormativaDaoJdbc();
                $areans=$arean->obtieneListado(); 
                $a = new AreaNormativa();
                foreach($areans as $a) {
                    $sel="";
                    if($elemento!= null && $elemento->getAreaNormativa()!= null ){
                            $aux4 = $elemento->getAreaNormativa();
                            if($a->getId() == $aux4->getId())
                            {    $sel="selected='selected'"; }
                    }
                        echo("<option value='".$a->getId()."' ".$sel." >".$a->getDescripcion()."</option>");
                  }
           ?>
          </select>
            </td>


            <td width="%">Centro de Costos*:</td>
        <td width="%">
          <select name="id_ccosto" id="id_ccosto" style="width:260px">
          <option value='0'>Selecciona</option>
          <?php
            $ccosto=new CentroCostosDaoJdbc();
            $ccostos=$ccosto->obtieneListado(); 
            $c = new CentroCostos();
            
            foreach($ccostos as $c){
              $sel="";
              if($elemento!= null && $elemento->getCentroCostos()!= null ){
                    $aux5 = $elemento->getCentroCostos();
                      if($c->getId() == $aux5->getId())
                      {        $sel="selected='selected'"; }
              }
              echo("<option value='".$c->getId()."' ".$sel." >".$c->getClave()." ".$c->getDescripcion()."</option>");
            }
            ?>
            </select>
			</td>
		  </tr>
		  
		  <tr>
			
			
			<td>Origen del Presupuesto*:</td>
		    <td>
		    <select name="origen" id="origen" style="width:260px">
		      <option value='0'>Selecciona</option>
		      <option value='I' <?php if($elemento!=null && $elemento->getOrigen()!= null &&  strtoupper($elemento->getOrigen()) == "I") echo("selected='selected'");?> >INAH</option>
		      <option value='T' <?php if($elemento!=null && $elemento->getOrigen()!= null &&  strtoupper($elemento->getOrigen()) == "T") echo("selected='selected'");?>>Terceros</option>
	      	</select>
		    </td>
		    		    
		    <td width="%">Lider de proyecto*:</td>
		    <td width="%">
		      <select name="id_lider" id="id_lider" style="width:260px">
		      <option value='0'>Selecciona</option>
                      <?php
                        $empleado=new EmpleadoDaoJdbc();
                        $empleados=$empleado->obtieneListado(); 
                        $e = new Empleado();
                        
                        foreach($empleados as $e){
                          $sel="";
                          if($elemento!= null && $elemento->getLider()!= null ){
                                $aux6 = $elemento->getLider();
                                  if($e->getId() == $aux6->getId())
                                          $sel="selected='selected'";
                          }
                          echo("<option value='".$e->getId()."' ".$sel." >".$e->getNombre()." ".$e->getApPaterno()." ".$e->getApMaterno()."</option>");
                        }
                        ?>
                        </select>
			</td>
		  
			<td>Costo*:</td>
		    <td><input name="costo" id='costo' type="text" size="40" value='<?php if($elemento!=null ) echo($elemento->getMonto());?>'/></td>
		  </tr>
		  
		  <tr>
		
		  	<td width="%">Línea de Acción*:</td>
		    <td width="%" colspan='3'>
		      <select name="id_linea" id="id_linea" style="width:630px">
		      <option value='0'>Selecciona</option>
                      <?php
                        $linea=new LineaAccionDaoJdbc();
                        $lineas=$linea->obtieneListado(); 
                        $l = new LineaAccion();
                        
                        foreach($lineas as $l){
                          $sel="";
                          if($elemento!= null && $elemento->getLineaAccion()!= null ){
                                  $aux7 = $elemento->getLineaAccion();
                                  if($l->getId() == $aux7->getId())
                                          $sel="selected='selected'";
                          }
                          echo("<option value='".$l->getId()."' ".$sel." >".$l->getLineaAccion()."</option>");
                        }
                        ?>
                    </select>
			</td>
		  	<td>Límites:</td>
		    <td><input name="limites" id='limites' type="text" size="40" maxlength='254' value='<?php if($elemento!=null && $elemento->getLimites()!= null) echo($elemento->getLimites());?>'/></td>
		  </tr>
		  
		  <tr>
 			<td>Antecedentes:</td>
		    <td><textarea name="antecedentes" id="antecedentes" rows='3' cols='30' ><?php if($elemento!=null && $elemento->getAntecedentes()!= null) echo($elemento->getAntecedentes());?></textarea></td>		
		    <td>Objetivo general:</td>
		    <td><textarea name="objetivo" id="objetivo" rows='3' cols='30' ><?php if($elemento!=null && $elemento->getObjetivoGral()!= null) echo($elemento->getObjetivoGral());?></textarea></td>
		    <td>Impacto social:</td>
		    <td><textarea name="impacto_soc" id="impacto_soc" rows='3' cols='30' ><?php if($elemento!=null && $elemento->getImpactoSocial()!= null) echo($elemento->getImpactoSocial());?></textarea></td>		
		    
		  </tr>
		  
		  <tr>
			<td>Impacto organizacional:</td>
		    <td><textarea name="impacto_org" id="impacto_org" rows='3' cols='30' ><?php if($elemento!=null && $elemento->getImpactoOrganizacional()!= null) echo($elemento->getImpactoOrganizacional());?></textarea></td>
		    <td>Impacto estratégico:</td>
		    <td><textarea name="impacto_est" id="impacto_est" rows='3' cols='30' ><?php if($elemento!=null && $elemento->getImpactoEstrategico()!= null) echo($elemento->getImpactoEstrategico());?></textarea></td>
		  	<td>Beneficiarios del proyecto:</td>
		    <td colspan='5'><textarea name="beneficiarios" id="beneficiarios" rows='3' cols='30'><?php if($elemento!=null && $elemento->getBeneficiarios()!= null) echo($elemento->getBeneficiarios());?></textarea></td>
		  </tr>
		   
		  <tr>
		    <td align="center" colspan="6"><input name="guardar" type="submit" value="Guardar"  class='btn' style="cursor:pointer" />
                        &nbsp;&nbsp;&nbsp;&nbsp;<a href='lista_proyecto.php' class='liga_btn'> Cancelar </a>
		    </td>
		  </tr>
		  
		  <?php if($elemento != null && $elemento->getId()!=null){  ?>
	     	<tr class="SizeText">
	        	<td colspan="6">
	            	<br/>
	            	<hr width="95%"  >
	            	<div align="center">  
	                     <a href="lista_metas_proyecto.php?idProyecto=<?php echo $elemento->getId();?>&num=<?php echo $elemento->getNumero();?>&desc=<?php echo $elemento->getDescripcion(); ?>" class="link" >Metas del Proyecto</a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;   
	                </div> 
	        	</td>
	        </tr>
	      <?php  } ?>  
        </table>
        <?php if($elemento!=null && $elemento->getId()!=null) echo("<input type='hidden' name='id' value='".$elemento->getId()."' />");?>
    </form>
</div>
<?php include 'footer.php' ?>
<script type="text/javascript">

function DoCustomValidation(){
 	
 	var numero=document.getElementById('numero').value;
 	var costo=document.getElementById('costo').value;
 	
 	if(isNaN(numero)){
 	 	alert('El número del proyecto debe contener sólo números, por favor verifíquelo.');
 	 	document.getElementById('numero').focus();
 	 	return false;
 	}
 	else if(isNaN(costo)){
 	 	alert('El costo no debe contener signo de pesos ni comas, por favor verifíquelo.');
 	 	document.getElementById('costo').focus();
 	 	return false;
 	}
 	else{
 		return true;
 	}
 }

 var frmvalidator  = new Validator("fproyecto");
 frmvalidator.addValidation("numero","req","Por favor capture el número del proyecto.");
 frmvalidator.addValidation("descripcion","req","Por favor capture la descripción del proyecto.");
 frmvalidator.addValidation("tipo","dontselect=0","Por favor seleccione el tipo de proyecto.");
 frmvalidator.addValidation("id_periodo","dontselect=0","Por favor seleccione el periodo.");
 frmvalidator.addValidation("finicio_tot","req","Por favor capture la fecha de inicio total del proyecto.");
 frmvalidator.addValidation("ffin_tot","req","Por favor capture la fecha final total del proyecto.");
 frmvalidator.addValidation("finicio_eje","req","Por favor capture la fecha de inicio en el ejercicio.");
 frmvalidator.addValidation("ffin_eje","req","Por favor capture la fecha final en el ejercicio.");
 frmvalidator.addValidation("id_area","dontselect=0","Por favor seleccione el área.");
 frmvalidator.addValidation("id_arean","dontselect=0","Por favor seleccione el área normativa.");
 frmvalidator.addValidation("id_ccosto","dontselect=0","Por favor seleccione el Centro de Costos.");
 frmvalidator.addValidation("origen","dontselect=0","Por favor seleccione el origen del proyecto.");
 frmvalidator.addValidation("id_lider","dontselect=0","Por favor seleccione el lider de Proyecto.");
 frmvalidator.addValidation("costo","req","Por favor capture el costo del proyecto.");
 frmvalidator.addValidation("id_linea","dontselect=0","Por favor seleccione la línea de acción.");
 frmvalidator.setAddnlValidationFunction(DoCustomValidation);
 frmvalidator.DoCustomValidation;
 
 
 Calendar.setup({ inputField : "finicio_tot", ifFormat : "%d-%m-%Y", button: "finicio_tot" });
 Calendar.setup({ inputField : "ffin_tot", ifFormat : "%d-%m-%Y", button: "ffin_tot" });
 Calendar.setup({ inputField : "finicio_eje", ifFormat : "%d-%m-%Y", button: "finicio_eje" });
 Calendar.setup({ inputField : "ffin_eje", ifFormat : "%d-%m-%Y", button: "ffin_eje" });
 
 function Regresar(){
 	window.location="lista_proyecto.php";
 }

 
</script>

</body>
</html>