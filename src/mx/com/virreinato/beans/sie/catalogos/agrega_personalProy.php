<?php

include_once("../src/mx/com/virreinato/dao/PersonalProyDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/PersonalProy.class.php");
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<script src="../js/jquery-1.7.2.js"></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
<script src="../js/jquery.maskedinput.js"></script>
</head>
<body>
 <?php
    $error = NULL;
    if (isset($_REQUEST['error'])) {
        $error = $_REQUEST['error'];
    }
    ?>
    <div class="contenido">
   <br/>
	<p class="titulo_cat1">Proyectos > Información de Proyectos > <a class="linkTitulo_cat1" href="lista_personalProy.php" > Personal de Proyectos </a></p>
	<h2 class="titulo_cat2">
            <?php
	$dao=new PersonalProyDaoJdbc();
	$elemento=new PersonalProy();

	if(isset($_GET['id'])){
		echo("Modificar Personal de Proyectos");
		$elemento=$dao->obtieneElemento($_GET['id']);	
	}	
	else{
		echo("Alta de Nuevo Personal de Proyectos");
	}	
    ?>
        </h2>
        <?php if ($error != null) echo("<div align='center' class='msj'>" . $error . "</div>"); ?>
        <form id="fpersona" name="fpersona" method="post" action="../src/mx/com/virreinato/web/CatPersonalProy.php">
            <table width="50%" border="0" cellspacing="0" cellpadding="5" class='tb_add_cat' align='center'>
            <tr>
               <td>
                           <br/>&nbsp; &nbsp; &nbsp; Curp*: <input type="text" name="curp" id="curp" size='25' value='<?php if($elemento!=null && $elemento->getCurp()!= null) echo($elemento->getCurp());?>'/>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; RFC*: <input type="text" name="rfc" id="rfc" size='25' value='<?php if($elemento!=null && $elemento->getRfc()!= null) echo($elemento->getRfc());?>'/>
               </td> 
             </tr>
             <tr>		
               <td>
                   &nbsp; &nbsp; &nbsp; Nombre(s)*: &nbsp; &nbsp; &nbsp; &nbsp;<input type="text" name="nombre" id="nombre" size='45' value='<?php if($elemento!=null && $elemento->getNombre()!= null) echo($elemento->getNombre());?>'/>

                   <br/><br/>&nbsp; &nbsp; &nbsp; Apellido Paterno*: <input type="text" name="app" id="app" size='45' value='<?php if($elemento!=null && $elemento->getApp()!= null) echo($elemento->getApp());?>'/>

                   <br/><br/>&nbsp; &nbsp; &nbsp; Apellido Materno*: <input type="text" name="apm" id="apm" size='45' value='<?php if($elemento!=null && $elemento->getApm()!= null) echo($elemento->getApm());?>'/>
              </td>
            </tr>

                <tr>
                        <td align="center" colspan="6" ><input name="guardar" type="submit" value="Guardar"  class='btn' />
                            &nbsp;&nbsp;&nbsp;&nbsp;<a href='lista_personalProy.php' class='liga_btn'> Cancelar </a>
                        </td>
               </tr>

           </table>
            <?php if($elemento!=null && $elemento->getId()!=null) echo("<input type='hidden' name='id' value='".$elemento->getId()."' />");?>
    </form>
</div>
<?php include 'footer.php' ?>
<script type="text/javascript">
  
  jQuery(function(){
                $("#curp").mask("aaa*999999aaaaaa99");
  });

 var frmvalidator  = new Validator("fpersona");
 frmvalidator.addValidation("curp","req","Por favor capture la CURP de la persona.");
 frmvalidator.addValidation("rfc","req","Por favor capture el RFC de la persona.");
 frmvalidator.addValidation("nombre","req","Por favor capture el nombre de la persona.");
 frmvalidator.addValidation("app","req","Por favor capture el apellido paterno de la persona.");
 frmvalidator.addValidation("apm","req","Por favor capture el apellido materno de la persona.");
 </script>
</body>
</html>
