<?php

$id = null;

session_start();
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/ComprobaINAHDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ComprobaINAH.class.php");
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
else{
    if(isset($_SESSION['id'])){
        $id = $_SESSION['id'];
    }
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link href="../css/calendario.css" type="text/css" rel="stylesheet">
<script src="../js/calendar.js" type="text/javascript"></script>
<script src="../js/calendar-es.js" type="text/javascript"></script>
<script src="../js/calendar-setup.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
<title>ComprobaINAH</title>
</head>
<body>
<?php 
    $respuesta = null;
    
   if(isset($_GET["respuesta"]))
        {$respuesta= (String) $_GET["respuesta"];}
        
   $dia = (String)(date('d'));
   $mes = (String)(date('m'));
   $mesInicio=$mes;
    $diaInicio=$dia;
    if(date('n')>1){
            $mesInicio= (String)( date('n') - 1 ); if(strlen($mesInicio) == 1){ $mesInicio = "0".$mesInicio; }

    }
    else{
            $diaInicio="01";
    } 
?>
    <div class="contenido">

    <br/>
    <p class="titulo_cat1">Gasto Básico > Comprobación INAH</p>
    <h2 class="titulo_cat2">Comprobación INAH</h2>

    <p class="titulo_cat1">
    <form name="frmFiltroComprobacion" id="frmFiltroComprobacion" method="POST" action="../src/mx/com/virreinato/web/WebComprobaINAHGb.php" >
    &nbsp; &nbsp; Fecha de Inicio: <input type="text" size="10" name="inicio" id="inicioFecha" maxlength="20" value=" <?php  if(isset($_GET["inicio"]) && $_GET["inicio"] !=null ){ echo( $_GET["inicio"]); } ?>"  >
    Fecha de Fin: <input type="text" size="10" name="fin" id="finFecha" maxlength="20" value=" <?php  if(isset($_GET["fin"]) && $_GET["fin"]!=null ){ echo( $_GET["fin"] ); }  else { echo($dia."-".$mes."-".date('Y'));} ?>"  >
    <?php 

        $aux = $diaInicio;

        $inicio =  $aux."-".$mesInicio."-".date('Y');
        $fin = $dia."-".$mes."-".date('Y');
        $periodo = "";
    ?> 
    &nbsp; Período: 
    <select name="periodo" id="periodo" style="width:110px" > 
    <?php
        $daoPer=new PeriodoDaoJdbc();
        $listaPer = $daoPer->obtieneListado();
        $elementoPer = new Periodo();

        foreach($listaPer as $elementoPer){

            $sel = "";
            if(isset($_GET["periodo"]) && $_GET['periodo']!=null && (int)( $_GET["periodo" ] ) == $elementoPer->getId()){ $sel = "selected='selected'"; }
            else if( $_GET["periodo"] == null && (int)($elementoPer->getPeriodo()) == date('Y')  ){ $sel = "selected='selected'";  $periodo = (String)($elementoPer->getId() ); }
            echo("<option value=".$elementoPer->getId()." ".$sel." >".$elementoPer->getPeriodo()."</option>");
        }
    ?>
    </select>
    &nbsp; <input name="filtro" style="cursor:pointer" type="submit" value="Filtrar"  class='btn' />
    </form>
    <br>
    
<?php
    if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>");
    if(isset($_GET['inicio']) && $_GET["inicio"] !=null ){
       $inicio = ( String )$_GET["inicio"];
       $fin = (String)$_GET["fin"];
    }
    if(isset($_GET['periodo']) && $_GET['periodo'] != null ) { $periodo = (String)$_GET['periodo'];  }
        else {
	      	$aniosel = (String)(date('Y'));
	      	$per=new PeriodoDaoJdbc();
	      	
                $periodo=$per->obtieneidElemento($aniosel);
	      }
?>
  <table width="80%" border="0" cellspacing="0" cellpadding="5" class='tb_cat' align='center' >

    <tr bgcolor="#9FB1CB">
        <th width="15%" align="center">Fecha</th>
        <th width="6%" align="center">Periodo</th>
        <th width="30%" align="center">Ministración</th>
        <th width="11%" align="center">Monto</th>  
        
        <?php if( $id<3 || $id>4 ){ ?>	
            <th width="2%"></th>
            <th width="2%"></th>
        <?php }?>
        <th width="2%"></th>
    </tr>
    
    <?php			
        $dao=new ComprobaINAHDaoJdbc();
        $lista=$dao->obtieneListadoDestino($inicio,$fin,$periodo,"GB");
        $elemento=new ComprobaINAH();

        foreach($lista as $elemento){
    ?>	
    <tr class="SizeText">
        <td align="center"><?php echo(date("d-m-Y",strtotime( $elemento->getFecha())));?></td>
        <td align="center"><?php echo($elemento->getDesPeriodo());?></td>
        <td align="center"><?php echo($elemento->getDesMinistracion());?></td>
        <td align="center"><?php echo("$".number_format($elemento->getMonto(),2));?></td>
        <?php if( $id<3 || $id>4 ){ ?>		 		
                <td align="center"><a href='AgregarComprobaINAHGb.php?id=<?php echo($elemento->getId());?>&periodo=<?php echo($elemento->getIdPeriodo());?>' class='liga_cat'><acronym title="Editar"><img src="../img/Pencil3.png" width="16" height="16" alt="Editar" style="border:0;" /></acronym></a></td>
                <td align="center"><a href='../src/mx/com/virreinato/web/WebComprobaINAHGb.php?id=<?php echo($elemento->getId());?>' class='liga_cat' onclick='return confirm("¿Esta seguro que desea eliminar este registro?")' ><acronym title="Borrar"><img src="../img/DeleteRed.png" width="16" height="16" alt="Borrar" style="border:0;" /></acronym></a></td>
        <?php }?>
        <td><a href="javascript:Imprimir('../Formatos/ComprobacionGastos.php','<?php echo($elemento->getId()); ?>')" class='liga_cat' ><acronym title="Imprimir"><img src="../img/printer.png" width="18" height="18" alt="Imprimir" style="border:0;" /></acronym></a></td>
    </tr>
    <?php			
        }//while
    ?>
    </table>
    
    <br>
    <?php if( $id<3 || $id>4 ){ ?>	
            <div align="center"><a href='AgregarComprobaINAHGb.php' class='liga_btn'>Agregar Comprobación INAH</a></div>
    <?php }?>	
</div>
<br/><br/>	
</body>
<script language="JavaScript">
        Calendar.setup({ inputField :"finFecha", ifFormat : "%d-%m-%Y", button:"finFecha" });

        Calendar.setup({ inputField : "inicioFecha", ifFormat : "%d-%m-%Y", button:"inicioFecha" });

        var ini = '<?php echo$inicio?>';
        var fin = '<?php echo$fin?>';
        $("#inicioFecha").val(ini);
        $("#finFecha").val(fin); 

        function Imprimir(pagina,folio) {
                var dir = pagina + "?folio=" + folio;
                var left = Math.floor((screen.width - 980) / 2);
                var opciones = 'titlebar=0, menubar=0, toolbar=0, location=0, directories=0, status=0, scrollbars=0, resizable=0, width=980, height=750,top=50,left='
                        + left + '';
                window.open(dir, "", opciones);
        }
</script>	
</html>
</body>

