<?php

$perfil = null;
$user = null;

session_start();
include_once("../src/mx/com/virreinato/dao/SueldoGbDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/SueldoGB.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/DetalleSueldoGB.class.php");
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/MinistraDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Ministra.class.php");
include_once("../src/mx/com/virreinato/dao/EmpleadoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Empleado.class.php");
include_once("../src/mx/com/virreinato/dao/AreaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Area.class.php");
include_once("../src/mx/com/virreinato/dao/CatChequeDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatCheque.class.php");
include_once("../src/mx/com/virreinato/dao/CuentaBancariaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CuentaBancaria.class.php");
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}else{
    if(isset($_SESSION['id'])){ $perfil = $_SESSION['id'];}
    if(isset($_SESSION['user'])){ $user = $_SESSION['user'];}
}
?>
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" type="text/css" href="../css/style.css" />
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<link href="../css/calendario.css" type="text/css" rel="stylesheet">
<script src="../js/calendar.js" type="text/javascript"></script>
<script src="../js/calendar-es.js" type="text/javascript"></script>
<script src="../js/calendar-setup.js" type="text/javascript"></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
<script language="JavaScript">
$(function(){
	
	$("#checkAll").click( function(){
	
 	
 	$(".checked").attr("checked",this.checked); 
 	});
 	
 	// if all checkbox are selected, check the selectall checkbox
    // and viceversa
    $(".checked").click(function(){
 
        if($(".checked").length == $(".checked:checked").length) {
            $("#checkAll").attr("checked", "checked");
        } else {
            $("#checkAll").removeAttr("checked");
        }
 
    });

});				
	function Regresar(){
		window.location="Sueldos.php";
	}
	
	function replaceAll( text, busca, reemplaza ){
	  while (text.toString().indexOf(busca) != -1)
	      text = text.toString().replace(busca,reemplaza);
	  return text;
	}
	
	function Validar(){
		enviar=1;
		hay_sel=0;
		monto_cheques=0;
		monto_ministracion=document.getElementById('montoministracion').value;
		
		if( $("#cuenta").val() == "0"){
				 alert("Por favor seleccione la cuenta bancaria.");
				 $("#idMinistracion").focus();
				 enviar=0;
				 return false;
				 
		}
		
		$(".checked").each(function(){
			if(this.checked){
				
				
				hay_sel=1;
				//verifico que no este vacio el sueldo, monto y cheque
				 idcheck=$(this).attr("id");
				 partes=idcheck.split("_");
				 
				 var montoCheque=document.getElementById('monto_'+partes[1]).value;
				 montoCheque= replaceAll(montoCheque,'$','');
				 montoCheque=replaceAll(montoCheque,',','');
				 
				 monto_cheques= (monto_cheques*1)+(montoCheque*1);
				 
				 if(document.getElementById('monto_'+partes[1]).value == ""){
					alert("Por favor captura el sueldo neto del empleado.");
				 	document.getElementById('monto_'+partes[1]).focus();
				 	enviar=0;
				 	return false;
				 }
				 if(document.getElementById('letra_'+partes[1]).value == ""){
					alert("Por favor captura el monto con letra.");
				 	document.getElementById('letra_'+partes[1]).focus();
				 	enviar=0;
				 	return false;
				 }
				 if(document.getElementById('cheque_'+partes[1]).value == ""){
					alert("Por favor captura el número de cheque.");
				 	document.getElementById('cheque_'+partes[1]).focus();
				 	enviar=0;
				 	return false;
				 }
				 
			}
			
		});
		
		
		if( hay_sel == 0){
				 alert("Por favor seleccione al menos a un empleado para generar el cheque.");
				 enviar=0;
				 return false;
		}
		
				
		if(enviar==1){
			if(monto_cheques!= monto_ministracion){
					 if( confirm("El monto de la ministración ($ "+monto_ministracion+") no coincide con el total de los cheques a imprimir ($ "+monto_cheques+"), ¿Deseas continuar con la impresión de los cheques? ")){
					 	document.frmSueldos.submit();
						return true;
					 }
					 
			}
			else{
				document.frmSueldos.submit();
				return true;
			}	
		}	
	}
	

	function Imprimir(pagina,folio,alto) {
		
		var dir = pagina + "?idSueldos=" + folio;
		var left = Math.floor((screen.width - 980) / 2);
		var opciones = 'titlebar=0, menubar=0, toolbar=0, location=0, directories=0, status=0, scrollbars=0, resizable=0, width=980, height='+alto+',top=50,left='
				+ left + '';
		window.open(dir, "", opciones);
	}
	
	
	
	function numerotoletra(id){
		
		 var monto = $("#monto_"+id).val();
		 
		 if(monto!= ''){
		 	if(isNaN(monto)){
		 		alert('El sueldo neto debe contener sólo números, no debe de contener comas ni segno de pesos, por fafor verifiquelo.')
		 	}
		 	else{
		 		covertirNumLetras(monto, id);
		 	}	
		 }	
	}
	
	// Función modulo, regresa el residuo de una división 
   function mod(dividendo , divisor) 
   { 
  			resDiv = dividendo / divisor ;  
  			parteEnt = Math.floor(resDiv);            // Obtiene la parte Entera de resDiv 
  			parteFrac = resDiv - parteEnt ;      // Obtiene la parte Fraccionaria de la división
  			modulo = Math.round(parteFrac * divisor);  // Regresa la parte fraccionaria * la división (modulo) 
  			return modulo; 
	} // Fin de función mod


	// Función ObtenerParteEntDiv, regresa la parte entera de una división
	function ObtenerParteEntDiv(dividendo , divisor) 
	{ 
  			resDiv = dividendo / divisor ;  
  			parteEntDiv = Math.floor(resDiv); 
  			return parteEntDiv; 
	} // Fin de función ObtenerParteEntDiv

	
	// function fraction_part, regresa la parte Fraccionaria de una cantidad
	function fraction_part(dividendo , divisor) 
	{ 
  			resDiv = dividendo / divisor ;  
  			f_part = Math.floor(resDiv); 
  			return f_part; 
	} // Fin de función fraction_part

	
	// function string_literal convierte el monto en letra 
	function string_literal_conversion(number) 
	{   
   		centenas = ObtenerParteEntDiv(number, 100); 
   		number = mod(number, 100); 

     		decenas = ObtenerParteEntDiv(number, 10); 
   		number = mod(number, 10); 

   		unidades = ObtenerParteEntDiv(number, 1); 
   		number = mod(number, 1);  
   		string_hundreds="";
   		string_tens="";
   		string_units="";
   
		   if(centenas == 1) string_hundreds = "ciento ";
   		if(centenas == 2) string_hundreds = "doscientos ";
   		if(centenas == 3) string_hundreds = "trescientos ";
   		if(centenas == 4) string_hundreds = "cuatrocientos ";
   		if(centenas == 5) string_hundreds = "quinientos ";
   		if(centenas == 6)	string_hundreds = "seiscientos ";
   		if(centenas == 7) string_hundreds = "setecientos ";
   		if(centenas == 8)	string_hundreds = "ochocientos ";
   		if(centenas == 9) string_hundreds = "novecientos ";

		   if(decenas == 1){
      		//Special case, depends on units for each conversion
     			if(unidades == 1) string_tens = "once";
      		if(unidades == 2) string_tens = "doce";
      		if(unidades == 3) string_tens = "trece";
      		if(unidades == 4) string_tens = "catorce";
      		if(unidades == 5) string_tens = "quince";
      		if(unidades == 6) string_tens = "dieciseis";
      		if(unidades == 7) string_tens = "diecisiete";
      		if(unidades == 8) string_tens = "dieciocho";
      		if(unidades == 9) string_tens = "diecinueve";
      
   		} 
   		
   		if(decenas == 2) string_tens = "veinti";
			if(decenas == 3) string_tens = "treinta";
   		if(decenas == 4) string_tens = "cuarenta";
   		if(decenas == 5) string_tens = "cincuenta";
   		if(decenas == 6) string_tens = "sesenta";
   		if(decenas == 7) string_tens = "setenta";
   		if(decenas == 8) string_tens = "ochenta";
   		if(decenas == 9) string_tens = "noventa";
   
   
	   if (decenas == 1) string_units="";   
   	else { 
      	if(unidades == 1) string_units = "un";
      	if(unidades == 2) string_units = "dos";
      	if(unidades == 3) string_units = "tres";
      	if(unidades == 4) string_units = "cuatro";
      	if(unidades == 5) string_units = "cinco";
      	if(unidades == 6) string_units = "seis";
      	if(unidades == 7) string_units = "siete";
      	if(unidades == 8) string_units = "ocho";
      	if(unidades == 9) string_units = "nueve";
  
  		  } // end if-then-else 
   	
   	  if (centenas == 1 && decenas == 0 && unidades == 0) string_hundreds = "cien " ; 
		  if (decenas == 1 && unidades ==0) string_tens = "diez " ; 
		  if (decenas == 2 && unidades ==0) string_tens = "veinte " ; 
		  if (decenas >=3 && unidades >=1)  string_tens = string_tens+" y "; 
	
		  final_string = string_hundreds+string_tens+string_units;
		  return final_string ; 

	} //end of function string_literal_conversion()================================ 


	function covertirNumLetras(number, id)
	{
  
   	cent = number.split(".");   
   	centavos = cent[1];
   	number = cent[0];
   
   
   	if (centavos == 0 || centavos == undefined){
   	centavos = "00";}

   	if (number == 0 || number == "") 
   	{ // if amount = 0, then forget all about conversions, 
      	centenas_final_string=" cero "; // amount is zero (cero). handle it externally, to 
      // function breakdown 
  		} 
   	else 
   	{ 
   
     		millions  = ObtenerParteEntDiv(number, 1000000); // first, send the millions to the string 
      	number = mod(number, 1000000);           // conversion function 
      
     		if (millions != 0)
      	{
      	                      
      		// This condition handles the plural case 
         	if (millions == 1) descriptor= " millon ";  // > than 1, use 'millones' (millions) as 
         	else descriptor = " millones "; 
            
      	} 
      	else  descriptor = "";                 // if 0 million then use no descriptor. 
      
      
	      millions_final_string = string_literal_conversion(millions)+descriptor; 
      	thousands = ObtenerParteEntDiv(number, 1000);  // now, send the thousands to the string 
        	number = mod(number, 1000);            // conversion function. 
      
      
     		if (thousands != 1) 
      	{                   // This condition eliminates the descriptor 
         		thousands_final_string =string_literal_conversion(thousands) + " mil "; 
       		//  descriptor = " mil ";          // if there are no thousands on the amount 
      	} 
      	if (thousands == 1)
      	{
         	thousands_final_string = "mil "; 
     		}
      	if (thousands < 1) 
      	{ 
         	thousands_final_string = ""; 
      	} 
  
      	// this will handle numbers between 1 and 999 which 
      	// need no descriptor whatsoever. 

     		centenas  = number;                     
      	centenas_final_string = string_literal_conversion(centenas) ; 
      
   	} //end if (number ==0) 

   	/*if (ereg("un",centenas_final_string))
   	{
     		centenas_final_string = ereg_replace("","o",centenas_final_string); 
   	}*/
   	//finally, print the output. 

   	/* Concatena los millones, miles y cientos*/
   	cad = millions_final_string+thousands_final_string+centenas_final_string; 
   
   	/* Convierte la cadena a Mayúsculas*/
   	cad = cad.toUpperCase();       
	
	cad=cad.replace(/\s*[\r\n][\r\n \t]*/g , "");
	
	
   	if (centavos.length>2)
  		 {   
      		if(centavos.substring(2,3)>= 5){
         		centavos = centavos.substring(0,1)+(parseInt(centavos.substring(1,2))+1).toString();
      		}   else{
        			centavos = centavos.substring(0,2);
       		}
   	}

   	/* Concatena a los centavos la cadena "/100" */
   	centavos=centavos.replace(/\s*[\r\n][\r\n \t]*/g , ""); 
   	if (centavos.length==1)
   	{
      		centavos = centavos+"0";
   	}
   	centavos = centavos+ "/100"; 


   	/* Asigna el tipo de moneda, para 1 = PESO, para distinto de 1 = PESOS*/
   	if (number == 1) moneda = " PESO ";  
   	else moneda = " PESOS ";  
   
   
   	/* Regresa el número en cadena entre paréntesis y con tipo de moneda y la fase M.N.*/
   	
   	document.getElementById('letra_'+id).value=cad+moneda+centavos+" M.N.";
   	   	
}
	
function genera_consecutivo(id){
	var no_cheque=document.getElementById('cheque_'+id).value;
	no_cheque=no_cheque*1;
	
	$(".checked").each(function(){
			if(this.checked){
				
				
				//verifico que no este vacio el sueldo, monto y cheque
				 idcheck=$(this).attr("id");
				 partes=idcheck.split("_");
				 
				 document.getElementById('cheque_'+partes[1]).value =no_cheque;
				 no_cheque++;	 
			}
		});
}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Editar Sueldo</title>
</head>
<body>
<?php 
    $respuesta = null;
    $error = null;
    $id = null;
    
   if(isset($_GET["respuesta"]))
        {$respuesta= (String) $_GET["respuesta"];}
   if(isset($_GET["error"]))
        {$error = (String) $_GET["error"];}
?>
<div class="contenido">
    <p class="titulo_cat1">Gasto Básico > <a class="linkTitulo_cat1" href="Sueldos.php" >Sueldos</a> </p>
    <h2 class="titulo_cat2">
<?php
    $dao = new SueldoGbDaoJdbc();
    $p = new SueldoGb();
    $cheque=null;

    if( isset($_GET['id'])){
        $id = (String)$_GET['id'];
        echo("Sueldos");
        $p = $dao->obtieneElemento($id);
        
        $cdao = new CatChequeDaoJdbc();
        $idCheque=$cdao->obtieneCheque($_GET['id']);

        $cheque= $cdao->obtieneElemento((String)($idCheque));
    }else if(isset($_GET['idSueldo'])){
        $id = (String)$_GET['idSueldo'];
        echo("Sueldos");
        $p = $dao->obtieneElemento($id);
        
        $cdao = new CatChequeDaoJdbc();
        $idCheque=$cdao->obtieneCheque($id);

        $cheque= $cdao->obtieneElemento((String)($idCheque));

    }else{
            echo("Agregar Sueldos");
    }	
	   		   
?>
</h2>
<?php 
     if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>");
     if($error!=null) echo("<div align='center' class='msj'>".$error."</div>");
?>

    <form name="frmSueldos" id="frmSueldos" method="POST" action="../src/mx/com/virreinato/web/WebSueldosChe.php">
	<table width="75%" border="0" cellspacing="0" cellpadding="5" class="tb_add_cat" align="center" style='background-color:transparent;'>
	<tr>
	<th width='20%' align='left'>Período:</th>
        <td><?php echo($p->getPeriodo());?> </td>
  	<th width='15%' align='left'>Fecha:</th>
	<td><?php echo(date("d-m-Y",strtotime( $p->getFecha())));?></td>
	<th width='15%' align='left'>Ministración:</th>
	<td>
        <?php
		
            if($p != null && $p->getIdMinistracion()!= null ){
                $daoMin=new MinistraDaoJdbc();
                $min=$daoMin->obtieneElemento((String)($p->getIdMinistracion()));
                echo($min->getDescripcion());
            }
        ?> 
        </td>
	</tr>
	<tr>
            <th align='left'>Tipo de Nómina:</th>
            <td>
                <?php echo($p->getTipoNomina());?>
            </td>
            <th align='left'>Quincena:</th>
            <td>
                <?php echo($p->getQuincena());?>
            </td>
            <th align='left'>Monto Total:</th>
            <td>
                <?php echo("$".number_format($p->getMonto(),2));?>
                <input type='hidden' name='montoministracion' id='montoministracion' value='<?php echo($p->getMonto());?>'>
            </td>
	</tr>
	<tr>
            <th colspan='6'><hr></th>
	</tr>
	<?php
            $cdao=new CuentaBancariaDaoJdbc();
            $lista=$cdao->obtieneListado();
            $elemento=new CuentaBancaria();
        ?>
	<tr>
            <th align='left'>Cuenta Bancaria:</th>
            <td>
                <select name="cuenta" id="cuenta" > 
                <?php

                    foreach($lista as $elemento){
                        $sel = "";
                        if($cheque!=null && $cheque->getIdCuenta()!=null && $cheque->getIdCuenta() == $elemento->getId() )
                        {    $sel = "selected"; }
                        echo("<option value=".$elemento->getId()." ".$sel." >".$elemento->getNumeroCuenta()."</option>");
	            } 
                ?>
                </select> 					
				
            </td>
            <th align='left'>Aplicación:</th>
            <td colspan='3'> Gasto Básico
            <input type="hidden" name="area" id="area" value='9'>
            </td>
	</tr>
	</table>
	<br>
        
        <table width="90%" border="0" cellspacing="0" cellpadding="5" class="tb_add_cat" align="center" style='background-color:transparent;'>
	<tr>
	<th width='20%' ><input type='checkbox' id='checkAll' >  Nombre Completo</th>
	<th width='20%' >Sueldo Neto</th>
	<th width='20%' >Monto con Letra</th>
	<th width='20%' >Número de Cheque</th>
	</tr>
        
<?php	

    $daoe=new EmpleadoDaoJdbc();
    $listae=$daoe->obtieneListadoTipoNomina( $p->getTipoNomina());
    $element = new Empleado();

    $contador=0;
    $l = 1;
    foreach($listae as $element){
        $det=null;
        $id_emp_tmp=null;
        $monto_tmp=null;
        $letra_tmp=null;
        $cheque_tmp=null;

        if($p !=null && $p->getId_DetalleSueldos()!=null  ){
            $id_DetalleSueldos=$p->getId_DetalleSueldos();

            if(array_key_exists($element->getId(), $id_DetalleSueldos)){
                $det=$id_DetalleSueldos[$element->getId()];
                $id_emp_tmp=$det->getIdEmpleado();
                $letra_tmp=$det->getMontoLetra();
                $monto_tmp=$det->getSueldo();
                $cheque_tmp=$det->getFolioCheque();
            }	
        }
        if( $p->getId_DetalleSueldos()==null || count($p->getId_DetalleSueldos())==0 || ( $id_emp_tmp!=null && $id_emp_tmp == $element->getId() ) ){

?>

        <tr class="SizeText">
            <td align="center"><input type='hidden' name='id_empleado<?php echo($l); ?>' value='<?php echo( $element->getId()); ?>'> <input type='checkbox' name='nombre' id='nombre_<?php echo( $element->getId()); ?>' class="checked"  value='<?php echo( $element->getId()); ?>' <?php if($id_emp_tmp!=null && $id_emp_tmp == $element->getId()) echo("checked='checked'  disabled='disabled' ");?>  >  <?php  echo( $element->getApPaterno()." ".$element->getApMaterno()." ".$element->getNombre() );?></td>
            <td align="center"><input type='text' name='monto<?php echo($l); ?>' id='monto_<?php echo( $element->getId()); ?>' onblur='numerotoletra(<?php echo( $element->getId());?>)' value='<?php if($monto_tmp!=null) echo($monto_tmp);?>' size='30' <?php if($id_emp_tmp!=null && $id_emp_tmp == $element->getId()) echo("readonly='readonly'");?> >  </td>
            <td align="center"><input type='text' name='letra<?php echo($l); ?>' id='letra_<?php echo( $element->getId()); ?>' value='<?php if($letra_tmp!=null) echo($letra_tmp); ?>' size='50'  readonly='readonly' > </td>
            <td align="center"><input type='text' name='cheque<?php echo($l); ?>' id='cheque_<?php echo( $element->getId()); ?>' value='<?php if($cheque_tmp!=null) echo($cheque_tmp);?>'  <?php if($id_emp_tmp!=null && $id_emp_tmp == $element->getId()) echo("readonly='readonly'"); if($contador==0){ echo("onblur='genera_consecutivo(".$element->getId().")'"); } ?> size='10'> </td>
        </tr>	
<?php      
        $l++;
        }	
}
?>
    <tr>
        <td align="center" colspan="3">
            <?php 
            if($p ==null || $p->getId_DetalleSueldos()==null || count($p->getId_DetalleSueldos())==0 ){
                      ?>    	  
            <input name="guardar" style="cursor:pointer" type="button" onClick = "Validar()" value="Continuar"  class='btn' />
                <?php
            }
            else{
                ?> 
                 &nbsp;&nbsp;&nbsp; <input type="button" style="cursor:pointer" class='btn' onclick="Imprimir('../Formatos/ChequesNominaGB.php','<?php echo($id);?>',250)" value="Imprimir Cheque" >   	  

                <?php

            }
            ?>
            &nbsp; &nbsp; &nbsp;
            <input name="cancelar" type="button" style="cursor:pointer"  onclick="Regresar()" value="Regresar"  class='btn' />
        </td>
    </tr>
    
    <?php if($id!= null ) echo("<input type='hidden' name='idSueldo' id='idSueldo' value='".$id."' />"); ?>
    <?php if($p->getIdPeriodo()!= null ) echo("<input type='hidden' name='idPeriodo'  value='".$p->getIdPeriodo()."' />"); ?>
    <?php if($p->getFecha()!= null ) echo("<input type='hidden' name='fecha' value='".date("Y-m-d",strtotime( $p->getFecha()))."' />"); ?>
    <?php if($p->getIdMinistracion()!= null ) echo("<input type='hidden' name='idMinistracion'  value='".$p->getIdMinistracion()."' />"); ?>
    <input type="hidden" name="numero" value='<?php echo$l; ?>'
    </table>
    </form>
    <br>
	
 </div>
 <br/><br/>
</body>
</html>
<script type='javascript' language='javascript'>
 Calendar.setup({ 
       inputField : "fecha", ifFormat :"%d-%m-%Y", button :"fecha"      
    });
</script>

        