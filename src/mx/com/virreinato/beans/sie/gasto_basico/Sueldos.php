<?php

$id = null;
$user = null;

session_start();
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/SueldoGbDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/SueldoGB.class.php");
include_once("../src/mx/com/virreinato/dao/ConsultaResumenDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ConsultaResumen.class.php");
include_once("../src/mx/com/virreinato/dao/MinistraDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Ministra.class.php");
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}else{
    if(isset($_SESSION['id'])){ $id = $_SESSION['id'];}
    if(isset($_SESSION['user'])){ $user = $_SESSION['user'];}
}
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sueldos </title>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link href="../css/calendario.css" type="text/css" rel="stylesheet">
<script src="../js/calendar.js" type="text/javascript"></script>
<script src="../js/calendar-es.js" type="text/javascript"></script>
<script src="../js/calendar-setup.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
<script>
 
  function Imprimir(pagina,folio) {
		var dir = pagina + "?idSueldos=" + folio;
		var left = Math.floor((screen.width - 1200) / 2);
		var opciones = 'titlebar=0, menubar=0, toolbar=0, location=0, directories=0, status=0, scrollbars=0, resizable=0, width=1200, height=650,top=50,left='
				+ left + '';
		window.open(dir, "", opciones);
	}
</script>
</head>
<body>
<?php 
    $respuesta = null;
    $error = null;
    
   if(isset($_GET["respuesta"]))
        {$respuesta= (String) $_GET["respuesta"];}
   if(isset($_GET["error"]))
        {$error= (String) $_GET["error"];}
        
   $dia = (String)(date('d'));
   $mes = (String)(date('m'));
   $mesInicio=$mes;
    $diaInicio=$dia;
    if(date('n')>1){
            $mesInicio= (String)( date('n') - 1 ); if(strlen($mesInicio) == 1){ $mesInicio = "0".$mesInicio; }

    }
    else{
            $diaInicio="01";
    } 
 
?>

<div class="contenido">
    <br/>
    <p class="titulo_cat1">Gasto Básico > Sueldos</p>
    <h2 class="titulo_cat2">Sueldos</h2>

    <p class="titulo_cat1">
       <form name="frmFiltroSueldos" id="frmFiltroSueldos" method="POST" action="../src/mx/com/virreinato/web/WebSueldos.php" >
           &nbsp; &nbsp; Fecha de Inicio: <input type="text" size="10" name="inicio" id="inicioFecha" maxlength="20" value=" <?php  if(isset($_GET["inicio"]) && $_GET["inicio"] !=null ){ echo( $_GET["inicio"]); } ?>"  >
            Fecha de Fin: <input type="text" size="10" name="fin" id="finFecha" maxlength="20" value=" <?php  if(isset($_GET["fin"]) && $_GET["fin"]!=null ){ echo( $_GET["fin"] ); }  else { echo($dia."-".$mes."-".date('Y'));} ?>"  >
            <?php 

                $aux = $diaInicio;
                if( ($mesInicio == "04" || $mesInicio == "06" || $mesInicio == "09" || $mesInicio == "11") && $aux == "31" )
                {        $aux = "30"; }

                if( $mesInicio == "02" && (  $dia == "29" || $dia == "30" || $dia == "31" ) )
                {        $aux = "28"; }

                $inicio =  $aux."-".$mesInicio."-".date('Y');
                    $fin = $dia."-".$mes."-".date('Y');
                    $periodo = "";
                    $estatus = "TD";
                    if( $id < 7 ){       
            ?>
            &nbsp; Período: <select name="periodo" id="periodo" style="width: 110px">
            <?php
                $daoPer=new PeriodoDaoJdbc();
                $listaPer = $daoPer->obtieneListado();
                $elementoPer = new Periodo();

                foreach($listaPer as $elementoPer){

                $sel = "";
                if(isset($_GET["periodo"]) && $_GET['periodo']!=null && (int)( $_GET["periodo" ] ) == $elementoPer->getId()){ $sel = "selected='selected'"; }
                else if( $_GET["periodo"] == null && (int)($elementoPer->getPeriodo()) == date('Y')  ){ $sel = "selected='selected'";  $periodo = (String)($elementoPer->getId() ); }
                echo("<option value=".$elementoPer->getId()." ".$sel." >".$elementoPer->getPeriodo()."</option>");
                }
            ?>
            </select>  
            &nbsp; &nbsp; Tipo de Nómina:
            <select style="width:120px" name="tipo_nomina" id="tipo_nomina">
                <?php if( $_POST['tipo_nomina']!= null && $_POST['tipo_nomina'] == "TD"){ ?>
                            <option value="TD" selected="selected" >  Todos </option>
                    <?php }else{ ?>
                    <option value="TD" selected="selected" >  Todos </option>

                    <?php }if( $_POST['tipo_nomina']!= null && $_POST['tipo_nomina'] == "Proyectos" ){?>
            <option value="Proyectos" selected="selected">Proyectos</option>
            <?php }else{?>
            <option value="Proyectos">Proyectos</option>

                    <?php }if( $_POST['tipo_nomina']!= null && $_POST['tipo_nomina'] == "ATM" ){?>
                            <option value="ATM" selected="selected">  ATM </option>
                    <?php }else{?>
                    <option value="ATM">  ATM </option>
                    <?php }if( $_POST['tipo_nomina']!= null && $_POST['tipo_nomina'] == "Apoyo a confianza" ){?>
            <option value="Apoyo a confianza" selected="selected">Apoyo a confianza</option>
            <?php }else{?>
            <option value="Apoyo a confianza">Apoyo a confianza</option>
            <?php }if( $_POST['tipo_nomina']!= null && $_POST['tipo_nomina'] == "Mandos medios" ){?>
                    <option value="Mandos medios" selected="selected">Mandos medios</option>
            <?php }else{?>
            <option value="Mandos medios">Mandos medios</option>
            <?php }if( $_POST['tipo_nomina']!= null && $_POST['tipo_nomina'] == "Compactados" ){?>
            <option value="Compactados" selected="selected">Compactados</option>
            <?php }else{?>
            <option value="Compactados">Compactados</option>
            <?php }if( $_POST['tipo_nomina']!= null && $_POST['tipo_nomina'] == "Pensión alimenticia" ){?>
                    <option value="Pensión alimenticia" selected="selected">Pensión alimenticia</option>
            <?php }else{?>
            <option value="Pensión alimenticia">Pensión alimenticia</option>
                    <?php } ?>
            </select>
            <?php     } ?>	
            &nbsp; <input name="Filtro" style="cursor:pointer" type="submit" value="Filtrar"  class='btn' />
        </form>
    <br>
<?php
    if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>");
    if($error!=null) echo("<div align='center' class='msj'>".$error."</div>");
?>
 <div id="Descripcion" >
    <?php
        $tipo_nomina = null;
        if(isset($_GET['inicio']) && $_GET["inicio"] !=null ){
 	       $inicio = ( String )$_GET["inicio"];
 	       $fin = (String)$_GET["fin"];
  	 }
 
        if(isset($_GET['tipo_nomina']) ){
            $tipo_nomina = $_GET['tipo_nomina']; 
        }else if(isset($_GET['estatus']) && $_GET['estatus']!=null) {
            $estatus = "TD";
        }

        if(isset($_GET['periodo']) && $_GET['periodo'] != null ){  $periodo = (String)$_GET['periodo'];  }
        else {
	      	$aniosel = (String)(date('Y'));
	      	$per=new PeriodoDaoJdbc();
	      	
                $periodo=$per->obtieneidElemento($aniosel);	        
	      }
    ?>
    <table width="85%" border="0" cellspacing="0" cellpadding="5" class='tb_cat' align='center' >
        <tr>
        <th width="10%">Fecha</th>
        <th width="20%">Tipo de Nómina</th>
        <th width="20%">Ministración</th>
        <th width="10%">Monto Total</th>
        <th width="2%"></th>
        <th width="2%"></th>
        <?php  if( $id == 1 || $id == 2 || $id== 5 || $id==6 ){ ?>
            <th width="2%"></th>
        <?php } ?>
        <th width="2%"></th>
        </tr> 

        <?php	
            $dao=new SueldoGbDaoJdbc();
            $lista=$dao->obtieneListado(  $inicio, $fin , $periodo, $tipo_nomina);
            $elemento = new SueldoGb();

            foreach($lista as $elemento){
               $ministra=new MinistraDaoJdbc();
        ?>
        
        <tr class="SizeText">
            <td align="center"><?php echo( date("d-m-Y",strtotime($elemento->getFecha())) ); ?></td>
            <td align="center"><?php  echo($elemento->getTipoNomina()); ?></td>
            <td align="center"><?php if($elemento->getIdMinistracion()!=null){
                    $min=$ministra->obtieneElemento((String)($elemento->getIdMinistracion()));
                    echo( $min->getDescripcion()); 
            } ?></td>
            <td align="center"><?php echo( "$".number_format($elemento->getMonto(),2));?></td>

            <td align="right"><a href='SueldosEdit.php?id=<?php echo($elemento->getId());?>' class='liga_cat'><acronym title="Editar"><img src="../img/Pencil3.png" width="16" height="16" alt="Editar" style="border:0;" /></acronym> </a></td>
            <td><a href='../src/mx/com/virreinato/web/WebSueldos.php?id=<?php echo($elemento->getId());?>' class='liga_cat' onclick='return confirm("¿Esta seguro que desea eliminar este registro?")' ><acronym title="Borrar"><img src="../img/DeleteRed.png" width="16" height="16" alt="Borrar" style="border:0;" /></acronym></a></td>
            <td><a href='SueldosGeneraCheques.php?id=<?php  echo($elemento->getId());?>' class='liga_cat'><acronym title="Imprimir Cheque"><img src="../img/dollar_coin.png" width="18" height="18" alt="Imprimir Cheques" style="border:0;" /></acronym></a></td>
            <td><a style="cursor:pointer" onclick="Imprimir('../Formatos/ChequesNominaGB.php',<?php echo($elemento->getId()); ?>)" class='liga_cat'><acronym title="Imprimir"><img src="../img/printer.png" width="18" height="18" alt="Imprimir" style="border:0;" /></acronym></a></td>  
        </tr>	
        <?php  } ?>
    </table>
    </div>
    <br>
        <div align="center"><a href='SueldosEdit.php' class='liga_btn'> Agregar Nueva Nómina</a></div>
    <br>
    <br>	
</div>
<br/><br/>
</body>
</html>	
<script language="JavaScript">
   Calendar.setup({ inputField :"finFecha", ifFormat : "%d-%m-%Y", button:"finFecha" });
    
   Calendar.setup({ inputField : "inicioFecha", ifFormat : "%d-%m-%Y", button:"inicioFecha" });
   
   var ini = '<?php echo$inicio ?>';
   var fin = '<?php echo$fin ?>';
   $("#inicioFecha").val(ini);
   $("#finFecha").val(fin); 

   
</script>
        