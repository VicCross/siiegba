<?php

session_start();
include_once("../src/mx/com/virreinato/dao/EntregablesDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Entregables.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Calendario</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
     <script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
     <script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
    <script>
    function OnlyNumber(value, elemnt){
                    if( isNaN(value) ){
                            elemnt.value = ""
                    }
    }
    </script>
	<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<?php 
    $idCarta = null;

   if(isset($_GET["idCarta"]))
        {$idCarta= (String) $_GET["idCarta"];}
?>
<body>
<div class="contenido">
<br/>
<p class="titulo_cat1">Proyectos > Información de Proyectos > <a class="linkTitulo_cat1" href="CartaConsAdd.php?idCarta=<?php echo(idCarta);?>" >Carta Constitutiva de Proyectos</a> > <a class="linkTitulo_cat1" href="lista_Entregables.jsp?idCarta=<?php echo(idCarta);?>" > Entregables Parciales </a> </p>
<h2 class="titulo_cat2">
<?php 
    $daoEntregable=new EntregablesDaoJdbc();
    $elemento=new Entregables();		

    if(isset($_GET['id'])){
        echo("Modificar Entregable");
        $elemento = $daoEntregable->obtieneElemento($_GET['id']);
    }	
    else{
        echo("Alta de Nuevo Entregable");
    }	
?>
</h2>

    <form id="frmEntregables" name="frmEntregables" method="post" action="../src/mx/com/virreinato/web/CatEntregables.php">
        <table width="50%" border="0" cellspacing="0" cellpadding="5" class='tb_add_cat' align='center'>

        <tr>

            <td class="SizeText">
                <br/>&nbsp; &nbsp; &nbsp; No. Orden*: <input type="text" name="orden_entregable" id="orden_entregable" onkeyup="OnlyNumber(this.value,this)" size="3" maxlength="2" value="<?php if($elemento!=null && $elemento->getNum_orden()!= null) echo($elemento->getNum_orden());?>" />

                <br/><br/>&nbsp; &nbsp; &nbsp; Descripción*: <input type="text" name="descripcion" id="descripcion" size="40" maxlength="255" value="<?php if($elemento!=null && $elemento->getDescripcion() != null) echo($elemento->getDescripcion() );?>" />

                <br/><br/>&nbsp; &nbsp; &nbsp; Características*:
                <br/>&nbsp; &nbsp; &nbsp; &nbsp;<textarea  name="carac" id="carac" cols="43" rows="5" maxlength="255"><?php if($elemento!=null && $elemento->getCaracteristicas()!= null) echo($elemento->getCaracteristicas());?></textarea>

                <br/><br/>&nbsp; &nbsp; &nbsp; Solicitado Por*: <input type="text" name="solicitado" id="solicitado" size="30" maxlength="255" value="<?php if($elemento!=null && $elemento->getSolicitado()!= null) echo($elemento->getSolicitado());?>" />

                <br/><br/>&nbsp; &nbsp; &nbsp; Entregado Por*: <input type="text" name="entregado" id="entregado" size="30" maxlength="255" value="<?php if($elemento!=null && $elemento->getEntregado()!= null) echo($elemento->getEntregado());?>" />

                <br/><br/>&nbsp; &nbsp; &nbsp; Validado por*: &nbsp;  <input type="text" name="validado" id="validado" size="30" maxlength="255" value="<?php if($elemento!=null && $elemento->getValido()!= null) echo($elemento->getValido());?>" />
            </td>
        </tr>

          <tr>
            <td align="center" colspan="6"><br/>
              <input name="guardar" style="cursor:pointer" type="submit" value="Guardar"  class='btn' />
              &nbsp; &nbsp; &nbsp;
              <input name="cancelar" style="cursor:pointer" type="button" value="Cancelar" onclick="Regresar()"  class='btn' />

            </td>
         </tr>
        </table>

        <?php 
           if($elemento!=null && $elemento->getId()!=null) echo("<input type='hidden' name='id' value='".$elemento->getId()."' />");
           echo("<input type='hidden' name='id_Carta' value='".$idCarta."'>");
        ?>
    </form>
</div>
<script>
 function Regresar(){
	 var carta = '<?php echo$idCarta ?>'
	 window.location="lista_Entregables.php?idCarta="+carta;
 }

 var frmvalidator  = new Validator("frmEntregables");
 frmvalidator.addValidation("descripcion","req","Por favor capture la descripción.");
 </script>
<br/><br/>
</body>
</html>