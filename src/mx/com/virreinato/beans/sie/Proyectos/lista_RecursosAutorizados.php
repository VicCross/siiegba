<?php

session_start();
include_once("../src/mx/com/virreinato/dao/TarjetaRecAutorizadoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/TarjetaRecAutorizados.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
<?php 
    $respuesta = null;
    $idTarjeta = null;
    
   if(isset($_GET["respuesta"]))
        {$respuesta= (String) $_GET["respuesta"];}
   if(isset($_GET["idTarjeta"]))
        {$idTarjeta= (String) $_GET["idTarjeta"];}
?>
<script>
	function Eliminar(idRecurso){
		alert("Registro Eliminado!");
	}	
</script>
</head>
<body>
<div class="contenido">
    <br/>
    <p class="titulo_cat1">Proyectos > Información de Proyectos  >  <a class="linkTitulo_cat1" href="TarjetaRegistroMod.php?idTarjeta=<?php echo($idTarjeta);?>"> Tarjeta de Registro </a> > Recursos Autorizados</p>
    <h2 class="titulo_cat2"><?php if(isset($_GET['proyecto'])) echo($_GET['proyecto']); ?></h2>
    <h2 class="titulo_cat2">Catálogo de Recursos Autorizados</h2>
    <?php
        if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>");
    ?>
    <table width="50%" border="0" cellspacing="0" cellpadding="5" class='tb_cat' align='center' >
        <tr>
          <th width="10%">Año de Autorización</th>
          <th width="10%">Ingreso Autogenerado</th>
          <th width="10%">Aportaciones</th>
          <th width="2%"></th>
          <th width="2%"></th>
        </tr>
        <?php 

            $dao=new TarjetaRecAutorizadoDaoJdbc();
            $lista=$dao->obtieneListado($idTarjeta);
            $elemento = new TarjetaRecAutorizados();
            foreach($lista as $elemento){
        ?>
        <tr>
            <td align="center"><?php echo($elemento->getAnio() );?></td>
            <td align="center"><?php echo( "$".number_format( $elemento->getIngresoAutogenerado(),2) );?></td>
            <td align="center"><?php echo( "$".number_format( $elemento->getAportacionesTerceros(),2) );?></td>
            <td><a href="RecursosAutorizados.php?id=<?php echo($elemento->getIdRecurso());?>&idTarjeta=<?php echo($idTarjeta);?>" class='liga_cat'><acronym title="Editar"><img src="../img/Pencil3.png" width="16" height="16" alt="Editar" style="border:0;" /></acronym></a></td>
            <td><a href="../src/mx/com/virreinato/web/CatTarjetaRecAutorizados.php?id=<?php echo($elemento->getIdRecurso());?>&id_Tarjeta=<?php echo($idTarjeta);?>" class='liga_cat' ><acronym title="Borrar"><img src="../img/DeleteRed.png" width="16" height="16" alt="Borrar" style="border:0;" /></acronym> </a></td>

        </tr>	
		<?php  }?>			
			
    </table>
    <br>
    <div align="center">
       <a href='TarjetaRegistroMod.php?idTarjeta=<?php echo($idTarjeta);?>' class='liga_btn'> Regresar </a>
       &nbsp; &nbsp; &nbsp;
       <a href="RecursosAutorizados.php?idTarjeta=<?php echo($idTarjeta);?>" class='liga_btn'> Agregar Nuevo Recurso Autorizado</a>
    </div>
</div>
<br/><br/>
</body>
</html>