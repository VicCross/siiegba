<?php

session_start();
include_once("../src/mx/com/virreinato/dao/CartaCronogramaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CartaCronograma.class.php");
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<link rel="stylesheet" type="text/css" href="../css/pro_dop_1.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
	function Eliminar(idMetas){
		alert("Registro Eliminado!");
	}
</script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
<script>
	$(document).ready(function() {
		$(".menu ul").css({
			background: "<?php echo $parametro->getValor(); ?>"
		});
		$("#.menu a").css({
			background: "<?php echo $parametro->getValor(); ?>"
		});
	});
</script>

</head>
<body>
<?php 
    $respuesta = null;
    $idCarta = null;
    
   if(isset($_GET["respuesta"]))
        {$respuesta= (String) $_GET["respuesta"];}
   if(isset($_GET["idCarta"]))
        {$idCarta= (String) $_GET["idCarta"];}
?>
<div class="contenido">
    <br/>
    <p class="titulo_cat1">Proyectos > Información de Proyectos > <a class="linkTitulo_cat1" href="CartaConsAdd.php?idCarta=<?php echo($idCarta);?>" >Carta Constitutiva de Proyectos</a> > Cronograma </p>
    <h2 class="titulo_cat2">Cronograma</h2>
    <?php
        if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>");
    ?>
    <table width="85%" border="0" cellspacing="0" cellpadding="5" class='tb_cat' align='center' >
        <tr>
          <th width="10%">Fase</th>
          <th width="50%">Actividades del Proyecto</th>
          <th width="10%"> Fecha de Inicio </th>
          <th width="2%"></th>
          <th width="2%"></th>
        </tr>
        <?php
           $dao=new CartaCronogramaDaoJdbc();
           $lista=$dao->obtieneListado($idCarta );
           $elemento = new CartaCronograma();

            foreach($lista as $elemento){
        ?>
        <tr class="SizeText">
            <td align="center"><?php if( $elemento->getFase() != null ) echo($elemento->getFase());?></td>
            <td align="center"><?php echo($elemento->getActividad());?></td>
            <td align="center"><?php echo(date("d-m-Y",strtotime($elemento->getFechaInicio())));?> </td>
          <td align="center"><a href='Cronograma_Carta.php?id=<?php echo($elemento->getId());?>&idCarta=<?php echo($idCarta);?>' class='liga_cat'><acronym title="Editar"><img src="../img/Pencil3.png" width="16" height="16" alt="Editar" style="border:0;" /></acronym></a></td>
            <td align="center"><a href='../src/mx/com/virreinato/web/Cronograma.php?id=<?php echo($elemento->getId());?>&idCarta=<?php echo($idCarta);?>' class='liga_cat'><acronym title="Borrar"><img src="../img/DeleteRed.png" width="16" height="16" alt="Borrar" style="border:0;" /></acronym></a></td>			    
        </tr>	
			  
        <?php			
            }
	  ?>						
			
	</table>
	<br>
	<div align="center">
	   <a href='CartaConsAdd.php?idCarta=<?php echo($idCarta); ?>' class='liga_btn'> Regresar </a>
	   &nbsp; &nbsp; &nbsp;
	   <a href='Cronograma_Carta.php?idCarta=<?php echo($idCarta); ?>' class='liga_btn'> Agregar Nueva Actividad</a>
	</div>
</div>
<br/><br/>
</body>
</html>