<?php

session_start();
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/PartidaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Partida.class.php");
include_once("../src/mx/com/virreinato/dao/ComprobaINAHDetDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ComprobaINAHDet.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<link rel="stylesheet" type="text/css" href="../css/pro_dop_1.css">	
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
	$(document).ready(function() {
		$(".menu ul").css({
			background: "<?php echo $parametro->getValor(); ?>"
		});
		$("#.menu a").css({
			background: "<?php echo $parametro->getValor(); ?>"
		});
	});
</script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<body>
<?php
    $respuesta = null;
    $comprobacion = null;
    $diferencia = null;
    $cheque = null;
    if(isset($_GET["respuesta"])){ $respuesta= $_GET["respuesta"]; }
    if(isset($_GET["idMaestro"]))$comprobacion = $_GET["idMaestro"];
    if(isset($_GET["diferencia"]))$diferencia = $_GET["diferencia"];
				
?>

<div class="contenido"> 
<br/>
<p class="titulo_cat1">Proyectos > <a class="linkTitulo_cat1" href="lista_ComprobaINAH.php" >Comprobación INAH</a> > <a class="linkTitulo_cat1" href="AgregarComprobaINAH.php?id=<?php echo($_GET['idMaestro']); ?>" > Detalle de Comprobación INAH </a></p>
<h2 class="titulo_cat2">
<?php
    $dao=new ComprobaINAHDetDaoJdbc();
    $elemento=new ComprobaINAHDet();

    if(isset($_GET['id'])){
        echo("Modificar Detalle de Comprobación INAH");
        $elemento=$dao->obtieneElemento($_GET['id']);
        $diferencia = (String)( (double)( $diferencia ) + $elemento->getMonto() );
    }	
    else{
        echo("Nuevo Detalle de Comprobación INAH");
    }	
?>
</h2>    
<?php if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>"); ?>
    <br/>
    <form id="fComprobaINAHDet" name="fComprobaINAHDet" method="post" action="../src/mx/com/virreinato/web/WebComprobaINAHDet.php">
        <table width="40%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
          <tr>
                <td>
                <br/> &nbsp; &nbsp; &nbsp; Partida*:
                <select style="width:350px" name="idPartida" id="idPartida">
                <option value="0" >  Selecciona </option>
                    <?php
                        $daoPartida = new PartidaDaoJdbc();
                        $listaPartida = $daoPartida->obtieneListado();
                        $partida = new Partida();

                        foreach($listaPartida as $partida){
                            $sel = "";
                            if($elemento!= null && $elemento->getIdPartida()!= null ){
                                if( $elemento->getIdPartida() == $partida->getId() ){ $sel = "selected='selected'"; }
                            }
                            echo("<option value=".$partida->getId()." ".$sel." >".$partida->getPartida()." ".$partida->getDescripcion()."</option>");
                        }
                    ?>
                </select> 
				
                <br/><br/> &nbsp;  &nbsp; &nbsp; Monto: <input type="text" name="monto" id="monto" onblur="Verificar()"  size="20" value='<?php if($elemento!=null  && $elemento->getMonto()!= null ) echo($elemento->getMonto());?>'/>

                <br/><br/> &nbsp;  &nbsp; &nbsp; Número de Notas: <input type="text" name="num_notas" id="num_notas" onkeyup="OnlyNumber(this.value, this)" size="15" value='<?php if($elemento!=null  && $elemento->getNum_notas()!= null ) echo($elemento->getNum_notas());?>'/>

                </td>
            </tr>
		  
            <tr>
              <td align="center" colspan="2">
                  <input name="guardar" type="submit" value="Guardar"  class='btn' />
                  &nbsp; &nbsp; &nbsp;
                  <input name="cancelar" style="cursor:pointer" type="button" onclick="Regresar()"  value="Cancelar"  class='btn' />
              </td>
            </tr>
        </table>
        <?php
            echo("<input type='hidden' name='idMaestro' value='".$_GET['idMaestro']."' />");
            if($elemento!=null && $elemento->getId()!=null) echo("<input type='hidden' name='id' value='".$elemento->getId()."' />");
        ?>
</div>
<br/><br/>	
</body>
<script type="text/javascript">

function Regresar(){
	var idComprobacion = '<?php echo$comprobacion ?>'
	window.location="AgregarComprobaINAH.php?id="+idComprobacion;
}

function Verificar(){
	var monto = parseInt( $("#monto").val() );
	var dif = '<?php echo$diferencia ?>'
	if( monto > dif ){
		$("#monto").val(dif);
	}
}

function OnlyNumber(value, elemnt){
	if( isNaN(value) ){
		elemnt.value = ""
	}
}

 var frmvalidator  = new Validator("fComprobaINAHDet");
 frmvalidator.addValidation("monto","req","Por favor capture el monto.");
</script>
</html>