<?php

session_start();
include_once("../src/mx/com/virreinato/dao/NominaProyDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/NominaProy.class.php");
include_once("../src/mx/com/virreinato/dao/PersonalProyDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/PersonalProy.class.php");
include_once("../src/mx/com/virreinato/dao/CatProyectoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatProyecto.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<link rel="stylesheet" type="text/css" href="../css/pro_dop_1.css">	
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
	$(document).ready(function() {
		$(".menu ul").css({
			background: "<?php echo $parametro->getValor(); ?>"
		});
		$("#.menu a").css({
			background: "<?php echo $parametro->getValor(); ?>"
		});
	});
</script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<body>
<?php 
    $respuesta = null;
    
   if(isset($_GET["respuesta"]))
        {$respuesta= (String) $_GET["respuesta"];}
?>
<div class="contenido"> 
    <br/>
    <p class="titulo_cat1">Proyectos > Información de Proyectos > <a class="linkTitulo_cat1" href="lista_NominaProy.php" > Nómina de Proyectos </a> </p>
    <h2 class="titulo_cat2">
    <?php
        $dao=new NominaProyDaoJdbc();
        $elemento=new NominaProy();

        if(isset($_GET['id'])){
            echo("Modificar Personal de proyectos");
            $elemento=$dao->obtieneElemento($_GET['id']);	
        }	
        else{
            echo("Nuevo Personal de proyectos");
            $elemento = null;
        }	
    ?>
    </h2> 
    <?php
        if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>");
    ?>
    <form id="fNominaProy" name="fNominaProy" method="post" action="../src/mx/com/virreinato/web/WebNominaProy.php">
        <table width="50%" border="0" cellspacing="0" cellpadding="1" class='tb_add_cat' align='center' >
            <tr>
                <td>
                    <br/>&nbsp; &nbsp; &nbsp; Periodo de pago*: <input type="text" name="periodoPago" id="periodoPago" value='<?php if($elemento!=null && $elemento->getPeriodoPago()!= null) echo($elemento->getPeriodoPago());?>'>

                    <br/><br/>&nbsp; &nbsp; &nbsp; Proyecto*: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                    <select name="proyecto" id="proyecto">
                        <option value='0'>Selecciona</option>		      
		      	<?php
                            if(isset($_GET['id'])){
                                $catProyecto=new CatProyectoDaoJdbc();
				$catProyectos=$catProyecto->obtieneListadoAbiertoIdProy2($elemento->getIdProy()); 
                                $cp = new CatProyecto();
                                
                                foreach($catProyectos as $cp){
                                  $sel="";
                                  if($elemento!= null && $elemento->getIdProy()!= null ){
                                          if($cp->getId()==$elemento->getIdProy()){
                                            $sel="selected='selected'";
                                          }	
                                  }	  
                                  echo("<option value='".$cp->getId()."' ".$sel." >".$cp->getDescripcion()."</option>");
                                }
                            }
                            else{
                                $catProyecto=new CatProyectoDaoJdbc();
                                $catProyectos=$catProyecto->obtieneListadoAbierto2(); 
                                $cp = new CatProyecto();
					
				foreach($catProyectos as $cp){
                                  $sel="";
                                  if($elemento!= null && $elemento->getIdProy()!= null ){
                                          if($cp->getId()==$elemento->getIdProy()){
                                            $sel="selected='selected'";
                                          }	
                                  }	  
                                  echo("<option value='".$cp->getId()."' ".$sel." >".$cp->getDescripcion()."</option>");
                                }
                            }
                        ?>
                    </select>

                    <br/><br/>&nbsp; &nbsp; &nbsp;Nombre*: &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; 
                    <select name="idPersonal" id="idPersonal">
                    <option value='0'>Selecciona</option>		      
                    <?php
                        $personalProy=new PersonalProyDaoJdbc();
                        $personalProys=$personalProy->obtieneListado(); 
                        $c = new PersonalProy();

                        foreach($personalProys as $c){
                            $sel="";
                            if($elemento!= null && $elemento->getIdPersonal()!= null ){
                                if($c->getId()==$elemento->getIdPersonal()){
                                      $sel="selected='selected'";
                                }	
                            }	  
                            echo("<option value='".$c->getId()."' ".$sel." >".$c->getApp().' '.$c->getApm().' '.$c->getNombre()."</option>");
                        }
                    ?>
		    </select>
		    
                <br/><br/>&nbsp; &nbsp; &nbsp;Responsable: &nbsp; &nbsp; &nbsp;  <input type="text" name="responsable" id="responsable" size="50" value='<?php if($elemento!=null && $elemento->getResponsable()!= null) echo($elemento->getResponsable());?>'/>
		   
                <br/><br/>&nbsp; &nbsp; &nbsp;Dias*: &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;  <input type="text" name="dias" id="dias" size="15" value='<?php if($elemento!=null && $elemento->getDias()!= null) echo($elemento->getDias());?>'/>
                 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Salario diario*: &nbsp; <input type="text" name="salarioDiario" onblur="CalcularImporte()" id="salarioDiario" size="15" value="<?php if( $elemento!=null )  echo($elemento->getSalarioDiario()); ?>" />

                <br/><br/>&nbsp; &nbsp; &nbsp;Importe*: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <input type="text" name="importe" id="importe" size="15" value="<?php if($elemento!=null) echo($elemento->getImporte()); ?>" />
                 &nbsp; &nbsp; &nbsp; &nbsp; Crédito al Salario*: <input type="text" name="creditoAlSalario" id="creditoAlSalario" size="15" value="<?php if($elemento!=null) echo($elemento->getCreditoAlSalario()); ?>" />

                <br/><br/>&nbsp; &nbsp; &nbsp;Bonificación*: &nbsp; &nbsp;&nbsp; &nbsp; <input type="text" name="bonificacion" id="bonificacion" size="15" value="<?php if($elemento!=null) echo($elemento->getBonificacion()); ?>"/> 
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Neto*: &nbsp;	<input type="text" name="neto" id="neto" size="15" value="<?php if($elemento!=null) echo($elemento->getNeto()); ?>" />

                </td>
            </tr>
	  
            <tr>
               <td align="center" colspan="2">
                   <br/>
                   <input name="guardar" type="submit" value="Guardar"  class='btn' />
                   &nbsp; &nbsp; &nbsp;
                   <input name="cancelar" style="cursor:pointer" type="button" onclick="Regresar()" value="Cancelar"  class='btn' />
                   <br/><br/>
               </td>
            </tr>
        </table>
		<?php if($elemento!=null && $elemento->getId()!=null) echo("<input type='hidden' name='id' value='".$elemento->getId()."' />");?>
    </form>
</div>
<script type="text/javascript">

function Regresar(){
	window.location="lista_NominaProy.php";
}

 function CalcularImporte(){
	 var dias = parseFloat($("#dias").val());
	 var salario = parseFloat( $("#salarioDiario").val() );
	 
	 $("#importe").val( Math.round( (dias*salario)*100 )/100 );
	 $("#neto").val( Math.round( (dias*salario)*100 )/100 );
 }

 var frmvalidator  = new Validator("fNominaProy");
 frmvalidator.addValidation("idPersonal","req","Por favor capture la persona.");
 frmvalidator.addValidation("periodoPago","req","Por favor capture el Periodo de Pago.");
 frmvalidator.addValidation("proyecto","req","Por favor seleccione un Proyecto.");
 frmvalidator.addValidation("dias","req","Por favor capture los días.");
 frmvalidator.addValidation("salarioDiario","req","Por favor capture el salario diario.");
 frmvalidator.addValidation("importe","req","Por favor capture el importe.");
 frmvalidator.addValidation("creditoAlSalario","req","Por favor capture el crédito al Salario.");
 frmvalidator.addValidation("bonificacion","req","Por favor capture la bonificación.");
 frmvalidator.addValidation("neto","req","Por favor capture el importe Neto."); 

 </script>
 <br/><br/>
</body>
</html>
