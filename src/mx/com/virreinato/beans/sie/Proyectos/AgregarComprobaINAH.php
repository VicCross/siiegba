<?php

session_start();
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/MinistraDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Ministra.class.php");
include_once("../src/mx/com/virreinato/dao/ComprobaINAHDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ComprobaINAH.class.php");
include_once("../src/mx/com/virreinato/dao/CentroCostosDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CentroCostos.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Sistema de Ingresos y Egresos</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link href="../css/calendario.css" type="text/css" rel="stylesheet">
    <script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
    <script src="../js/calendar.js" type="text/javascript"></script>
    <script src="../js/calendar-es.js" type="text/javascript"></script>
    <script src="../js/calendar-setup.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
	<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<body>
<?php
  $respuesta = null; 
  if(isset($_GET['error'])){ (String)$_GET["error"]; }
  $idComprobacion = null;
?>
    <div class="contenido"> 
    <br/>
    <p class="titulo_cat1">Proyectos > <a class="linkTitulo_cat1" href="lista_ComprobaINAH.php" >Comprobación INAH</a> </p>
    <h2 class="titulo_cat2">
        <?php
            $dao=new ComprobaINAHDaoJdbc();
            $elemento=new ComprobaINAH();

            if(isset($_GET['id'])){
                echo("Modificar Comprobación INAH");
                $elemento=$dao->obtieneElemento($_GET['id']);
                $idComprobacion = (String)$_GET['id'];
            }else{
                echo("Nueva Comprobación INAH");
            }	
        ?>
    </h2> 

    <?php if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>");?>
    <br/>
    
    <form id="fComprobaINAH" name="fComprobaINAH" method="post" action="../src/mx/com/virreinato/web/WebComprobaINAH.php">
        <table width="50%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
         <tr>
            <td>Periodo*:</td>
            <td><select name="idPeriodo" id="idPeriodo" style="width:110px">
                <option value='0'>Selecciona</option>	
                <?php
                    if(!isset($_GET['id'])){
                        $daoPer=new PeriodoDaoJdbc();
                        $listaPer = $daoPer->obtieneListadoAbierto();
                        $elementoPer = new Periodo();

                        foreach($listaPer as $elementoPer){

                        $sel = "";
                        if($elemento!= null && $elemento->getIdPeriodo()!= null ){
                            if($elementoPer->getId()==$elemento->getIdPeriodo()){
                                $sel="selected='selected'";
                            }	
                        }	  
                        echo("<option value=".$elementoPer->getId()." ".$sel." >".$elementoPer->getPeriodo()."</option>");
                        }
                    }else{
                        $daoPer=new PeriodoDaoJdbc();
                        $listaPer = $daoPer->obtieneListadoAbiertoIdPeriodo($elemento->getIdPeriodo());
                        $elementoPer = new Periodo();

                        foreach($listaPer as $elementoPer){

                        $sel = "";
                        if($elemento!= null && $elemento->getIdPeriodo()!= null ){
                            if($elementoPer->getId()==$elemento->getIdPeriodo()){
                                $sel="selected='selected'";
                            }	
                        }	  
                        echo("<option value=".$elementoPer->getId()." ".$sel." >".$elementoPer->getPeriodo()."</option>");
                        }
                    }
                ?>
                </select>
            </td>
        </tr>
		  
        <tr>
            <td>Ministración*:</td>
            <td>
                <select name="idMinistracion" id="idMinistracion" style="width:180px" onchange="ObtenerMonto()" >  
                    <option value='0'>Selecciona</option>
                    <?php
                        $ministracion=new MinistraDaoJdbc();
                        $ministracions=$ministracion->obtieneListadoDestino("PR"); 
                        $c = new Ministra();


                        foreach ($ministracions as $c) {

                            $sel="";
                            if($elemento!= null && $elemento->getIdMinistracion()!= null ){
                                if( $elemento->getIdMinistracion() == ($c->getId()) ){ $sel=" selected='selected' "; }	
                            }	  

                          echo("<option value='".$c->getId()."/"."$".number_format($c->getMonto(),2)."' ".$sel." >".$c->getDescripcion()."</option>");
                        }
                    ?>  		      
                </select>
                <span class="SizeText">&nbsp; &nbsp;Monto: <label id="MontoMinistracion" >$ 0.0</label></span>
            </td>
        </tr>
        
        <tr>
            <td>URL:</td>
            <td><input type="text" name="url" id="url" size="50" value='<?php if($elemento!=null && $elemento->getUrl()!= null) echo($elemento->getUrl()); ?>'/></td>
        </tr>
		  
        <tr>
            <td>Fecha*:</td>
            <td><input type="text" name="fecha" id="fecha" size="27" value='<?php if($elemento!=null && $elemento->getFecha()!= null) echo(date("d-m-Y",strtotime($elemento->getFecha()))); ?>'/></td>
        </tr>
		  
        <tr>
            <td>Centro de Costo*:</td>
            <td><select name="idCCosto" id="idCCosto">
                <option value='0'>Selecciona</option>		      
                <?php 
                    $ccosto=new CentroCostosDaoJdbc();
                    $ccostos=$ccosto->obtieneListado(); 
                    $cc = new CentroCostos();

                    foreach($ccostos as $cc){

                        $sel="";
                        if($elemento!= null && $elemento->getIdCCosto()!= null ){
                            if($cc->getId()==$elemento->getIdCCosto()){
                                $sel="selected='selected'";
                            }	
                        }	  
                        echo("<option value='".$cc->getId()."' ".$sel." >".$cc->getDescripcion()."</option>");
                    }
                ?>
            </select>
            </td>
        </tr>
		  
        <tr>
            <td align="center" colspan="2">
                <input name="guardar" style="cursor:pointer" type="submit" value="Guardar"  class='btn' />
                &nbsp; &nbsp; &nbsp;
                <input name="cancelar" style="cursor:pointer" type="button" onclick="Regresar()"   value="Cancelar"  class='btn' />
            </td>
        </tr>
		  
        </table>
        <?php if($elemento!=null && $elemento->getId()!=null) echo("<input type='hidden' name='id' value='".$elemento->getId()."' />");?>
    </form>
	
    <?php  if(isset($_GET['id'])){ ?>
        <br/><br/><br/><br/>
        <?php include "lista_DetComprobaINAH.php"; ?>
    <?php   } ?>
    </div>
<script type="text/javascript">

function Regresar(){
	window.location="lista_ComprobaINAH.php";
}

$().ready(function(){
	if( $("#idMinistracion").val() != 0 ){
		ObtenerMonto();
	}
});

function ObtenerMonto(){
	var value = $("#idMinistracion").val();
	var aux = value.split('/');
	$("#MontoMinistracion").text( aux[1] );
	
}

Calendar.setup({ inputField :"fecha", ifFormat : "%d-%m-%Y", button:"fecha" }); 

 var frmvalidator  = new Validator("fComprobaINAH");
 frmvalidator.addValidation("fecha","req","Por favor capture la fecha.");
 frmvalidator.addValidation("idPeriodo","dontselect=0","Por favor selecciona el período");
 frmvalidator.addValidation("idMinistracion","dontselect=0","Por favor selecciona la ministración");
 frmvalidator.addValidation("idCCosto","dontselect=0","Por favor selecciona el centro de costo");
</script>
 <br/><br/>
</body>
</html>