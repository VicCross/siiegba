<?php

$id = null;

session_start();
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/ReintegroDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Reintegro.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}else{
    if(isset($_SESSION['id'])){ $id = $_SESSION['id'];}
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html>

<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link href="../css/calendario.css" type="text/css" rel="stylesheet">
<script src="../js/calendar.js" type="text/javascript"></script>
<script src="../js/calendar-es.js" type="text/javascript"></script>
<script src="../js/calendar-setup.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Reintegro de Impuestos</title>
</head>

<body>
<?php 
    $respuesta = null;
    
   if(isset($_GET["respuesta"]))
        {$respuesta= (String) $_GET["respuesta"];}
        
   $dia = (String)(date('d'));
   $mes = (String)(date('m'));
   $mesInicio=$mes;
    $diaInicio=$dia;
    if(date('n')>1){
            $mesInicio= (String)( date('n') - 1 ); if(strlen($mesInicio) == 1){ $mesInicio = "0".$mesInicio; }

    }
    else{
            $diaInicio="01";
    } 
?>
<div class="contenido">

    <br/>
    <p class="titulo_cat1">Proyectos > Reintegros de Impuestos</p>
    <h2 class="titulo_cat2">Reintegros de Impuestos</h2>

    <p class="titulo_cat1">
        <form name="frmFiltroReintegro" id="frmFiltroReintegro" method="POST" action="../src/mx/com/virreinato/web/WebReintegro.php" >
        &nbsp; &nbsp; Fecha de Inicio: <input type="text" size="10" name="inicio" id="inicioFecha" maxlength="20" value=" <?php  if(isset($_GET["inicio"]) && $_GET["inicio"] !=null ){ echo( $_GET["inicio"]); } ?>"  >
        Fecha de Fin: <input type="text" size="10" name="fin" id="finFecha" maxlength="20" value=" <?php  if(isset($_GET["fin"]) && $_GET["fin"]!=null ){ echo( $_GET["fin"] ); }  else { echo($dia."-".$mes."-".date('Y'));} ?>"  >
        <?php 

            $aux = $diaInicio;
            if( ($mesInicio == "04" || $mesInicio == "06" || $mesInicio == "09" || $mesInicio == "11") && $aux == "31" )
            {        $aux = "30"; }

            if( $mesInicio == "02" && (  $dia == "29" || $dia == "30" || $dia == "31" ) )
            {        $aux = "28"; }

            $inicio =  $aux."-".$mesInicio."-".date('Y');
            $fin = $dia."-".$mes."-".date('Y');
            $periodo = "";    
        ?>
        &nbsp; Período: <select name="periodo" id="periodo" style="width: 110px">
        <?php
            $daoPer=new PeriodoDaoJdbc();
            $listaPer = $daoPer->obtieneListado();
            $elementoPer = new Periodo();

            foreach($listaPer as $elementoPer){

            $sel = "";
            if(isset($_GET["periodo"]) && $_GET['periodo']!=null && (int)( $_GET["periodo" ] ) == $elementoPer->getId()){ $sel = "selected='selected'"; }
            else if( $_GET["periodo"] == null && (int)($elementoPer->getPeriodo()) == date('Y')  ){ $sel = "selected='selected'";  $periodo = (String)($elementoPer->getId() ); }
            echo("<option value=".$elementoPer->getId()." ".$sel." >".$elementoPer->getPeriodo()."</option>");
            }
        ?>
        </select>
        &nbsp; <input name="Filtro" style="cursor: pointer" type="submit" value="Filtrar" class='btn' />
    </form>
    <br>
    
    <?php
        if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>");
        
        if(isset($_GET['inicio']) && $_GET["inicio"] !=null ){
            $inicio = ( String )$_GET["inicio"];
            $fin = (String)$_GET["fin"];
        }
         
        if(isset($_GET['periodo']) && $_GET['periodo'] != null ){  $periodo = (String)$_GET['periodo'];  }
        else {
            $aniosel = (String)(date('Y'));
            $per=new PeriodoDaoJdbc();

            $periodo=$per->obtieneidElemento($aniosel);	
            if($periodo == null){ $periodo = "null"; }
        }
    ?>
    <table width="90%" border="0" cellspacing="0" cellpadding="5" class='tb_cat' align='center'>
        <tr bgcolor="#9FB1CB">
            <th align="center">Fecha</th>
            <th align="center">Folio</th>
            <th align="center">Proveedor</th>
            <th align="center">R.F.C</th>
            <th align="center">CURP</th>
            <th align="center">Honorarios</th>
            <th width="2%"></th>
            <th width="2%"></th>
            <th width="2%"></th>
        </tr>
        
        <?php	
            $dao=new ReintegroDaoJdbc();
            $lista=$dao->obtieneListado($inicio,$fin,$periodo);
            $elemento=new Reintegro();

            foreach($lista as $elemento){
        ?>				
        <tr class="SizeText">
            <td align="center"><?php echo(date("d-m-Y",strtotime( $elemento->getFecha() )));?></td>
            <td align="center"><?php echo($elemento->getFolio());?></td>
            <td align="center"><?php echo($elemento->getApp().' '.$elemento->getApm().' '.$elemento->getNombre()); ?></td>
            <td align="center"><?php echo($elemento->getRfc()); ?></td>
            <td align="center"><?php echo($elemento->getCurp()); ?></td>
            <td align="center"><?php echo(number_format($elemento->getHonorarios(),2)); ?></td>
            <td align="center"><a href='AgregarReintegro.php?id=<?php echo($elemento->getId()); ?>' class='liga_cat'><acronym title="Editar"><img src="../img/Pencil3.png" width="16" height="16" alt="Editar" style="border:0;" /></acronym></a></td>
            <td align="center"><a href='../src/mx/com/virreinato/web/WebReintegro.php?id=<?php echo($elemento->getId()); ?>' class='liga_cat' onclick='return confirm("¿Esta seguro que desea eliminar este registro?")' ><acronym title="Borrar"><img src="../img/DeleteRed.png" width="16" height="16" alt="Borrar" style="border:0;" /></acronym></a></td>
            <td><a style="cursor:pointer" class='liga_cat' onclick="Imprimir('../Formatos/Reintegro.php','<?php echo$elemento->getId()?>')"><acronym title="Imprimir Reintegro"><img src="../img/printer.png" width="18" height="18" alt="Imprimir Reintegro" style="border:0;" /></acronym></a></td>
        </tr>
        <?php			
            }//while
        ?>		
    </table>
    <br>
        <div align="center"><a href='AgregarReintegro.php' class='liga_btn'>Agregar Reintegro</a></div>
    </div>
<script>
    function Imprimir(pagina,folio) 
    {
        var dir = pagina + "?folio=" + folio;
        var dir2 = "../Formatos/ConstanciaPagos.php?folio="+ folio;
        var left = Math.floor((screen.width - 980) / 2);
        var opciones = 'titlebar=0, menubar=0, toolbar=0, location=0, directories=0, status=0, scrollbars=0, resizable=0, width=980, height=750,top=50,left='
                + left + '';
        window.open(dir,"", opciones);

        dir = "../Formatos/ConstanciaPagos.php?folio="+ folio;
        opciones = 'titlebar=0, menubar=0, toolbar=0, location=0, directories=0, status=0, scrollbars=0, resizable=0, width=880, height=650,top=50,left='+ left + '';
        window.open(dir, "", opciones);
    }

    Calendar.setup({ inputField :"finFecha", ifFormat : "%d-%m-%Y", button:"finFecha" });

    Calendar.setup({ inputField : "inicioFecha", ifFormat : "%d-%m-%Y", button:"inicioFecha" });

       var ini = '<?php echo$inicio ?>';
       var fin = '<?php echo$fin ?>';
       $("#inicioFecha").val(ini);
       $("#finFecha").val(fin); 
</script>
<br/><br/>
</body>
</html>