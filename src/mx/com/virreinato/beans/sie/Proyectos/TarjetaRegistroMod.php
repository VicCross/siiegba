<?php

session_start();
include_once("../src/mx/com/virreinato/dao/TarjetaRegistroDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/TarjetaRegistro.class.php");
include_once("../src/mx/com/virreinato/dao/CatProyectoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatProyecto.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" type="text/css" href="../css/style.css" />
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<link href="../css/calendario.css" type="text/css" rel="stylesheet">
<script src="../js/calendar.js" type="text/javascript"></script>
<script src="../js/calendar-es.js" type="text/javascript"></script>
<script src="../js/calendar-setup.js" type="text/javascript"></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Editar Proyecto</title>
</head>
<body>
<?php 
    $respuesta = null;
    $idTarjeta = null;
    $error = null;
    $idProy = null;
    $nombreProy = null;
    
   if(isset($_GET["respuesta"]))
        {$respuesta= (String) $_GET["respuesta"];}
    if(isset($_GET["error"]))
        {$error = (String) $_GET["error"];}
?>
<div class="contenido">
<br/>
<p class="titulo_cat1">Proyectos > Informaci&oacute;n de Proyectos> <a class="linkTitulo_cat1" href="lista_TarjetaRegistro.php" >Tarjeta de Registro</a> </p>
<h2 class="titulo_cat2">
    
<?php
   
     $daoTarjeta = new TarjetaRegistroDaoJdbc();
     $elemento = new TarjetaRegistro();
   
    if(isset($_GET['idTarjeta'])){
        echo("Tarjeta de Registro");
        $idTarjeta =(String)$_GET['idTarjeta'];
        $elemento = $daoTarjeta->obtieneElemento($idTarjeta);
        $idProy=$elemento->getIdProyecto();
        $daop = new CatProyectoDaoJdbc();
        $proy=$daop->obtieneElemento((String)($idProy));
        $nombreProy=$proy->getDescripcion();
    }else{
        echo("Crear Tarjeta de Registro");

    }
?>
</h2>
<?php
    if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>");
    if($error!=null) echo("<div align='center' class='msj'>".$error."</div>");
    ?>
<form id="frmTarjetaRegistro" name="frmTarjetaRegistro" method="post" action="../src/mx/com/virreinato/web/CatTarjetaRegistro.php">
   <table width="90%" border="0" cellspacing="0"  class='tb_add_cat' align='center' >
		
        <tr class="SizeText">
           <td align="center" > <br/>&nbsp; Nombre del Proyecto*: </td>
            <td colspan='3'> <br/><select id="nombre_Proyecto" name="nombre_Proyecto" style="width:510px" > <!-- onchange="obtenerMetas(0)"--> 

            <option value="0"> Selecciona </option>
            <?php
            if( $idTarjeta == null){
                $dao=new CatProyectoDaoJdbc();
                $lista=$dao->obtieneListadoAbierto();
                $elementoLista=new CatProyecto();
 
                foreach($lista as $elementoLista){
                    $sel = "";
                    if($elemento!= null && $elemento->getIdProyecto()!= null ){
                             if( $elemento->getIdProyecto() == $elementoLista->getId() ){ $sel = "selected='selected'"; }
                    }
                    $periodo = $elementoLista->getPeriodo();
                    echo("<option value=".$elementoLista->getId()." ".$sel." >" .$periodo->getPeriodo()."-" .$elementoLista->getDescripcion()."</option>");
                }	
            }
            else{
 
                $dao=new CatProyectoDaoJdbc();
                $lista= $dao->obtieneListadoAbiertoIdProy2($elemento->getIdProyecto());
                $elementoLista=new CatProyecto();
                
                foreach($lista as $elementoLista){
                    $sel = "";
                    if($elemento!= null && $elemento->getIdProyecto()!= null ){
                        if( $elemento->getIdProyecto() == $elementoLista->getId() ){ $sel = "selected='selected'"; }
                    }
                    $periodo = $elementoLista->getPeriodo();
                    echo("<option value=".$elementoLista->getId()." ".$sel." >" .$periodo->getPeriodo()."-" .$elementoLista->getDescripcion()."</option>");
                }	

            }
            ?>
            </select>

           </td>

           <!-- td aling="center"><br/>Meta del Proyecto*:</td>
           <td> <br/><select id="meta_Proyecto" name="meta_Proyecto" style="width:160px"></select></td -->

           <td align="center"><br/>Unidad Administrativa*:</td>

           <td>
              <br/><input type="text" name="unidadAdministrativa" id="unidadAdministrativa" size="20" maxlength="250" value="<?php if($elemento!=null && $elemento->getUnidadAdministrativa()!= null) echo($elemento->getUnidadAdministrativa());?>"  />
           </td>



        </tr>
	    
	    <tr class="SizeText">
	       
	       <td align="center"> &nbsp; Responsable*:</td>
	       <td>
		   	  <br/><input type="text" name="id_Responsable" id="id_Responsable" size="20" maxlength="250" value="<?php if($elemento!=null && $elemento->getResponsable()!= null) echo($elemento->getResponsable()); else echo("Cecilia Genel Velasco"); ?>"  />
	       </td>
	        
	       <td align="center"> Clave L&iacute;nea Acci&oacute;n:</td>
	       <td>
		      <input type="text" name="claveLineaAccion" id="claveLineaAccion" size="20" maxlength="250" value="<?php if($elemento!=null && $elemento-> getClaveAccion()!= null) echo($elemento-> getClaveAccion());?>"  />
	       </td>
	       
	       <td align="center">Clave T&eacute;cnica:</td>
	       <td>
		      <input type="text" name="claveTecnica" id="claveTecnica" size="20" maxlength="250" value="<?php if($elemento!=null && $elemento->getClaveTecnica()!= null) echo($elemento->getClaveTecnica());?>"  />
	       </td>
	       
	    </tr>
	    
	    <tr class="SizeText">
		     <td align="center">&nbsp; Autorizado sin Financiamiento:</td>
	         <td> <input type="text" name="autorizadoSinFinanciamiento" id="autorizadoSinFinanciamiento" size="20" maxlength="250" value="<?php if($elemento!=null && $elemento->getAutorizadoSinFinanciamiento()!= null) echo($elemento->getAutorizadoSinFinanciamiento());?>"  /> </td>
		      
		      <td align="center">Fecha Elaboraci&oacute;n*:</td>
		      <td>
		        <input type="text" name="elaboracion_Tarjeta" id="elaboracion_Tarjeta" size="20" maxlength="250" value="<?php if($elemento!=null && $elemento->getFechaElaboracion()!= null) echo(date("d-m-Y",strtotime( $elemento->getFechaElaboracion())));?>"  />
		      </td>
		    
		       <td align="center">Fecha Autorizaci&oacute;n*:</td>
		       <td>
		        <input type="text" name="autorizacion_Tarjeta" id="autorizacion_Tarjeta" size="20" maxlength="250" value="<?php if($elemento!=null && $elemento->getFechaAutorizacion()!= null) echo( date("d-m-Y",strtotime($elemento->getFechaAutorizacion())));?>"  />
		      </td>
		      
		  </tr>
		  
		  <tr class="SizeText">
		     <td colspan="2" align="center" ><br/>Clave Programatica</td>
		     <td colspan="4" align="center" ><br/></td>
		  </tr>
		  
		  <tr class="SizeText">
				<td align="center">Funci&oacute;n</td>		     
                <td> <input type="text" name="funcion" id="funcion" size="20" maxlength="250" value="<?php if($elemento!=null && $elemento->getFuncion()!= null) echo($elemento->getFuncion());?>"  />  </td>
                <td align="center" > Tipo de Proyecto</td>
                <td align="left">
                      <select id="tipoProyecto" name="tipoProyecto">
		    	  		<?php if($elemento!=null && $elemento->getTipoProyecto()!=null){ 
		    	        	if($elemento->getTipoProyecto() == "2"){ ?>
                		 		<option value="1"> Permanente </option>
		    	    	 	 	<option value="2" selected="selected" > Con Fecha de Termino </option> 
		    	    		 	<option value="3"> Nueva Creaci&oacute;n </option>   
               			<?php   }else if($elemento->getTipoProyecto() == "3"){?>
                				 <option value="1"> Permanente </option>
		    	    		 	 <option value="2"> Con Fecha de Termino </option> 
		    	    		 	 <option value="3" selected="selected" > Nueva Creaci&oacute;n </option>   
               			<?php   }else {?>
                  	 			 <option value="1"  > Permanente </option>
		    	    		 	 <option value="2"> Con Fecha de Termino </option> 
		    	    		 	 <option value="3"> Nueva Creaci&oacute;n </option>
                
               			<?php  
                    		}
                  		   }else{
               			?>
                   			<option value="1"  > Permanente </option>
		    	    		<option value="2"> Con Fecha de Termino </option> 
		    	    		<option value="3"> Nueva Creaci&oacute;n </option>
               			<?php }?>
               
               		  </select>
                </td>
                <td align="center"> Producto Principal*:  </td>
                <td rowspan="2" > <textarea rows="2" maxlength="250"  cols="17" name="producto" id="producto" ><?php if($elemento!=null && $elemento->getProductoPrincipal()!=null) echo($elemento->getProductoPrincipal());?></textarea> </td>
		  </tr>
		  
		  <tr class="SizeText">
				<td align="center">SubFunci&oacute;n</td>		     
                <td> <input type="text" name="Subfuncion" id="Subfuncion" size="20" maxlength="250" value="<?php if($elemento!=null && $elemento->getSubFuncion()!= null) echo($elemento->getSubFuncion());?>"  />  </td>
                <td colspan="2">&nbsp;</td>
                <td colspan="2">&nbsp;</td>
                 
		  </tr>
		  
		  <tr class="SizeText">
				<td align="center">Programa General</td>		     
                <td> <input type="text" name="programaGeneral" id="programaGeneral" size="20" maxlength="250" value="<?php if($elemento!=null && $elemento->getProgramaGeneral()!= null) echo($elemento->getProgramaGeneral());?>"  />  </td>
                <td colspan="2" align="center" > Seguimiento del Proyecto </td>
                <td align="center" > Acumulado Anterior </td>
                <td> <input type="text" size="20" maxlength="255" name="acumuladoAnterior" id="acumuladoAnterior" value="<?php if($elemento!=null && $elemento->getAcumulado()!=null) echo($elemento->getAcumulado()); ?>" />  </td>
		  </tr>
		  
		  <tr class="SizeText">
				<td align="center">Act. Institucional</td>		     
                <td> <input type="text" name="act_Institucional" id="act_Institucional" size="20" maxlength="250" value="<?php if($elemento!=null && $elemento->getActividadInstitucional()!= null) echo($elemento->getActividadInstitucional());?>"  />  </td>
                <td align="center"> A Concluir <input type="text" size="5" maxlength="1" name="seguimiento_Programado" id="seguimiento_Programado" value="<?php if($elemento!=null && $elemento->getConcluir()!=null) echo($elemento->getConcluir()); ?>" />     </td>
                <td align="center" > Reactivado <input type="text" size="5" maxlength="1" name="seguimiento_Reactivado" id="seguimiento_Reactivado" value="<?php if($elemento!=null && $elemento->getReactivado()!=null) echo($elemento->getReactivado()); ?>" /> </td>
                <td align="center" > Programado para: </td>
                <td> <input type="text" size="20" maxlength="255" name="Programado_X" id="Programado_X" value="<?php if($elemento!=null && $elemento->getProgramado()!=null) echo($elemento->getProgramado()); ?>" />  </td>
		  </tr>
		  
		  <tr class="SizeText">
				<td align="center">Act. Prioritaria </td>		     
                <td> <input type="text" name="act_Prioritaria" id="act_Prioritaria" size="20" maxlength="250" value="<?php if($elemento!=null && $elemento->getActividadPrioritaria()!= null) echo($elemento->getActividadPrioritaria());?>"  /> </td>
                <td colspan="4"> &nbsp; </td> 
		  </tr>
		  
		  <tr class="SizeText">
		     <td align="center"> Objetivo del Proyecto </td>
		     <td colspan="5" ><br><textarea rows="4" cols="40" name="objetivo" maxlength="250" id="objetivo" ><?php if($elemento!=null && $elemento->getObjetivo()!=null) echo($elemento->getObjetivo());?></textarea>  </td>
		  </tr>  
		  
		  <tr class="SizeText">
		     <td rowspan="2" align="center"> Justificaci&oacute;n Presupuestaria </td>
		     <td colspan="5"><br><textarea rows="10" cols="105" name="justificacion" id="justificacion" ><?php if($elemento!=null && $elemento->getJustificacion()!=null) echo($elemento->getJustificacion());?></textarea>  </td>
		  </tr>
		  
		  <tr class="SizeText">
		     <td align="left"  colspan="3" ><br/>Fases del Proyecto Concluidas </td>
		     <td align="left" colspan="2" ><br/> No. de Oficio de Autorizaci&oacute;n Tecnica </td>
		     <td></td>
		  </tr>
		  
		<tr class="SizeText" >
		    <td align="center"> Fases </td>
		    <td align="center"> Del a&ntilde;o </td>
		    <td align="center"> Al a&ntilde;o </td>
		    <td align="center"> Costo Total </td>
		    <td colspan="2"> <input type="text" name="total_Modificado" id="total_Modificado" size="20" maxlength="12" onkeyup="OnlyNumber(this.value,this)" value="<?php if($elemento!=null && $elemento->getCostoTotal_Modificado()!= null) echo($elemento->getCostoTotal_Modificado());?>"  /> </td>
		</tr>
		
		<tr class="SizeText" >
		    <td align="center"> I </td>
		    <td align="center"> <input type="text" name="del1" id="del1" size="5" maxlength="4" onkeyup="OnlyNumber(this.value,this)" value="<?php if($elemento!=null && $elemento->getDel_Fase1()!= null) echo($elemento->getDel_Fase1());?>"  /></td>
		    <td align="center"> <input type="text" name="al1" id="al1" size="5" maxlength="4" onkeyup="OnlyNumber(this.value,this)" value="<?php if($elemento!=null && $elemento->getAl_Fase1()!= null) echo($elemento->getAl_Fase1());?>" /> </td>
		    <td align="center"> Producto Principal </td>
		    <td colspan="2"> <input type="text" name="prod_PrincipalMod" id="prod_PrincipalMod" size="20" maxlength="255" value="<?php if($elemento!=null && $elemento->getProductoPrincipal_Modificado()!= null) echo($elemento->getProductoPrincipal_Modificado());?>"  /> </td>
		</tr>
		
		<tr class="SizeText" >
		    <td align="center"> II </td>
		    <td align="center"> <input type="text" name="del2" id="del2" size="5" maxlength="4" onkeyup="OnlyNumber(this.value,this)" value="<?php if($elemento!=null && $elemento->getDel_Fase2()!= null) echo($elemento->getDel_Fase2());?>"  /> </td>
		    <td align="center"> <input type="text" name="al2" id="al2" size="5" maxlength="4" onkeyup="OnlyNumber(this.value,this)" value="<?php if($elemento!=null && $elemento->getAl_Fase2()!= null) echo($elemento->getAl_Fase2());?>"  /> </td>
		    <td align="center"> Linea de Acci&oacute;n </td>
		    <td colspan="2"> <input type="text" name="linea_Modificado" id="linea_Modificado" size="20" maxlength="255" onkeyup="OnlyNumber(this.value,this)" value="<?php if($elemento!=null && $elemento->getLineaAccion_Modificado()!= null) echo($elemento->getLineaAccion_Modificado());?>"  /> </td>
		</tr>
		
		
		<tr class="SizeText" >
		    <td align="center"> III </td>
		    <td align="center"> <input type="text" name="del3" id="del3" size="5" maxlength="4" onkeyup="OnlyNumber(this.value,this)" value="<?php if($elemento!=null && $elemento->getDel_Fase3()!= null) echo($elemento->getDel_Fase3());?>"  /> </td>
		    <td align="center"> <input type="text" name="al3" id="al3" size="5" maxlength="4" onkeyup="OnlyNumber(this.value,this)"  value="<?php if($elemento!=null && $elemento->getAl_Fase3()!= null) echo($elemento->getAl_Fase3());?>" /> </td>
		    <td align="center"> Nombre del Proyecto </td>
		    <td colspan="2"> <input type="text" name="nombre_Modificado" id="nombre_Modificado" size="20" maxlength="255"value="<?php if($elemento!=null && $elemento->getNombreProyecto_Modificado()!= null) echo($elemento->getNombreProyecto_Modificado());?>"   /> </td>
		</tr>
		
		<tr class="SizeText" >
		    <td align="center"> IV </td>
		    <td align="center"> <input type="text" name="del4" id="del4" size="5" maxlength="4" onkeyup="OnlyNumber(this.value,this)" value="<?php if($elemento!=null && $elemento->getDel_Fase4()!= null) echo($elemento->getDel_Fase4());?>"  /> </td>
		    <td align="center"> <input type="text" name="al4" id="al4" size="5" maxlength="4" onkeyup="OnlyNumber(this.value,this)" value="<?php if($elemento!=null && $elemento->getAl_Fase4()!= null) echo($elemento->getAl_Fase4());?>"  /> </td>
		    <td align="center"> A Permanente </td>
		    <td colspan="2"> <input type="text" name="Permanente" id="Permanente" size="20" maxlength="250" value="<?php if($elemento!=null && $elemento->getPermanente_Modificado()!= null) echo($elemento->getPermanente_Modificado());?>"  /> </td>
		</tr>
		
		
		<tr class="SizeText" >
		    <td align="center"> V </td>
		    <td align="center"> <input type="text" name="del5" id="del5" size="5" maxlength="4" onkeyup="OnlyNumber(this.value,this)" value="<?php if($elemento!=null && $elemento->getDel_Fase5()!= null) echo($elemento->getDel_Fase5());?>"  /> </td>
		    <td align="center"> <input type="text" name="al5" id="al5" size="5" maxlength="4" onkeyup="OnlyNumber(this.value,this)" value="<?php if($elemento!=null && $elemento->getAl_Fase5()!= null) echo($elemento->getAl_Fase5());?>"  /> </td>
		    <td align="center"> Programado a Concluir </td>
		    <td colspan="2"> <input type="text" name="concluir_Modificado" id="concluir_Modificado" size="20" maxlength="250" onkeyup="OnlyNumber(this.value,this)" value="<?php if($elemento!=null && $elemento->getConcluir_Modificado()!= null) echo($elemento->getConcluir_Modificado());?>" />  </td>
		</tr>
		
		<tr class="SizeText" >
		    <td align="center"> VI </td>
		    <td align="center"> <input type="text" name="del6" id="del6" size="5" maxlength="4" onkeyup="OnlyNumber(this.value,this)" value="<?php if($elemento!=null && $elemento->getDel_Fase6()!= null) echo($elemento->getDel_Fase6());?>"  /> </td>
		    <td align="center"> <input type="text" name="al6" id="al6" size="5" maxlength="4" onkeyup="OnlyNumber(this.value,this)" value="<?php if($elemento!=null && $elemento->getAl_Fase6()!= null) echo($elemento->getAl_Fase6());?>"  /> </td>
		    <td align="center"> A&ntilde;o de <input type="text" name="anio_Modificado" id="anio_Modificado" size="5" maxlength="100" value="<?php if($elemento!=null && $elemento->getAnio_Modificacion() != null) echo($elemento->getAnio_Modificacion() );?>"  /> </td>
		    <td align="left"> Al <input type="text" name="al_Modificado" id="al_Modificado" size="5" maxlength="100"value="<?php if($elemento!=null && $elemento->getAl_Modificacion()!= null) echo($elemento->getAl_Modificacion());?>"  /> </td>
		    <td></td>
		</tr>
		
		<tr class="SizeText" >
		    <td align="center"><br/><br/> Elabora*: </td>
		    <td><br/><br/><input type="text" name="elabora_Tarjeta" id="elabora_Tarjeta" size="20" maxlength="250" value="<?php if($elemento!=null && $elemento->getElabora()!= null) echo($elemento->getElabora());?>"  /> </td>
		    <td align="center"><br/><br/>Propone*: </td>
		    <td><br/><br/> <input type="text" name="propone_Tarjeta" id="propone_Tarjeta" size="20" maxlength="250" value="<?php if($elemento!=null && $elemento->getPropone()!= null) echo($elemento->getPropone());?>"  /> </td>
		    <td align="center"><br/><br/> Secretario T&eacute;cnico*: </td>
		    <td><br/><br/> <input type="text" name="autorizaTecnico_Tarjeta" id="autorizaTecnico_Tarjeta" size="20" maxlength="250" value="<?php if($elemento!=null && $elemento->getAutoriza_Tecnico()!= null) echo($elemento->getAutoriza_Tecnico());?>"  /> </td>
		</tr>
		
			<tr class="SizeText" >
		    <td align="center">Secretario Administrativo*: </td>
		    <td><input type="text" name="adminsitrativo_Tarjeta" id="administrativo_Tarjeta" size="20" maxlength="250" value="<?php if($elemento!=null && $elemento->getAutoriza_Adminstrativo()!= null) echo($elemento->getAutoriza_Adminstrativo());?>"  /> </td>
		    <td align="center" colspan="2" > En caso de Recursos Aportados, nombre de la persona </td>
		    <td colspan="2"> <input type="text" name="recursos_Nombre" id="recursos_Nombre" size="60" maxlength="250" value="<?php if($elemento!=null && $elemento->getRecursos_Terceros()!=null) echo($elemento->getRecursos_Terceros()); ?>" />  </td>
		</tr>
		
		<tr>
		     <td colspan="6" align="center"  >
	                <br/><input name="guardar" style="cursor:pointer" type="submit"  value="Guardar"  class='btn' />
		    		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;   
		    		<input name="cancelar" sytle="cursor:pointer"  type="button" value="Cancelar" class="btn" onclick="Regresar()"  />
		    		<br/><br/> 
	         </td>
		</tr>
		
		<?php if( $idTarjeta!= null ){ ?>
		 <tr class="SizeText">
	        	<td colspan="6">
	            	<hr width="98%"  >
	            	<div align="center">  
	                     <a href="lista_RecursosAutorizados.php?idTarjeta=<?php echo($idTarjeta);?>&proyecto= <?php echo($nombreProy);?>" class="link"  >Recursos Autorizados</a> &nbsp; &nbsp; &nbsp; &nbsp; 
	                     <a href="lista_Metas.php?idTarjeta=<?php echo($idTarjeta);?>&proyecto= <?php echo($nombreProy);?>"               class="link"  >Metas</a> &nbsp; &nbsp; &nbsp; &nbsp;
	                     <a href="lista_Calendario.php?idTarjeta=<?php echo($idTarjeta);?>&proyecto= <?php echo($nombreProy);?>"          class="link"  >Calendario</a> &nbsp; &nbsp; &nbsp; &nbsp;
	                     <a href="lista_Personal.php?idTarjeta=<?php echo($idTarjeta);?>&proyecto= <?php echo($nombreProy);?>"            class="link"  >Personal que Participa</a> 
	                </div>
	                <br/> 
	        	</td>
	     </tr>
	    <?php } ?>
	  
		  
		  
  </table>
    <?php if($elemento!=null && $elemento->getId()!=null )  echo("<input type='hidden' name='id' value='".$elemento->getId()."' />");?>
 </form>
 </div>
 <script>
 /*
 $().ready(function(){
      var idMeta = '<?php echo$elemento->getIdMeta() ?>'
      obtenerMetas(idMeta);
      
 });
 
 function obtenerMetas( sel ){
	 var idProyecto = $("#nombre_Proyecto").val();
	 $.ajax({
		 url: "Tarjeta.do",
		 type: "POST",
		 data: "obtenerMetas="+idProyecto+"&sel="+sel,
		 success: function(data){ $("#meta_Proyecto").html(data); }
	 });
	 
 }
 */
 function Regresar(){
		window.location="lista_TarjetaRegistro.php";
 }
 
 function OnlyNumber(value, elemnt){
			if( isNaN(value) ){
				elemnt.value = ""
			}
	}
 
  Calendar.setup({ inputField : "elaboracion_Tarjeta", ifFormat : "%d-%m-%Y", button:"elaboracion_Tarjeta" });
  Calendar.setup({ inputField : "autorizacion_Tarjeta", ifFormat : "%d-%m-%Y", button:"autorizacion_Tarjeta" });
  
  var frmvalidator  = new Validator("frmTarjetaRegistro");
  frmvalidator.addValidation("nombre_Proyecto","dontselect=0","Por favor seleccione un proyecto.");
  frmvalidator.addValidation("unidadAdministrativa","req","Por favor capture la Unidad Administrativa.");
  frmvalidator.addValidation("id_Responsable","req","Por favor capture el Responsable del Proyecto.");
  frmvalidator.addValidation("elabora_Tarjeta","req","Por favor capture quien elabor� la Tarjeta.");
  frmvalidator.addValidation("propone_Tarjeta","req","Por favor capture quien propone la Tarjeta.");
  frmvalidator.addValidation("autorizaTecnico_Tarjeta","req","Por favor capture la autorizaci�n T�cnica.");
  frmvalidator.addValidation("adminsitrativo_Tarjeta","req","Por favor capture la autorizaci�n Administrativa.");
 </script>
 <br/><br/>
 </body>
 </html>