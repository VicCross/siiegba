<?php

$perfil = null;

session_start();
include_once("../src/mx/com/virreinato/dao/NominaProyDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/NominaProy.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}else{
    if(isset($_SESSION['id'])){ $perfil = $_SESSION['id'];}
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html>

<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<title>NominaProy</title>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
<script>
 
  function Imprimir(pagina,folio) {
        var dir = pagina + "?folio=" + folio;
        var left = Math.floor((screen.width - 980) / 2);
        var opciones = 'titlebar=0, menubar=0, toolbar=0, location=0, directories=0, status=0, scrollbars=0, resizable=0, width=980, height=750,top=50,left='
                        + left + '';
        window.open(dir, "", opciones);
    }
</script>
</head>
<?php 
    $respuesta = null;
  
   if(isset($_GET["respuesta"]))
        {$respuesta= (String) $_GET["respuesta"];}
?>
<div class="contenido">

<br/>
    <p class="titulo_cat1">Proyectos > Información de Proyectos >  Nómina de Proyectos</p>
    <h2 class="titulo_cat2">Nómina de Proyectos</h2>
    <?php 
        if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>");
    ?>
    <table width="90%" border="0" cellspacing="0" cellpadding="5" class='tb_cat' align='center' >

	<tr bgcolor="#9FB1CB">
            <th align="center">Proyecto</th>
            <th align="center">Nombre</th>
            <th align="center">Periodo de pago</th>
            <th align="center">Neto</th>
            <th width="2%" align="center"></th>
            <th width="2%" align="center"></th>
            <th width="2%" align="center"></th>
	</tr>
		
    <?php			
        $dao=new NominaProyDaoJdbc();
	$lista=$dao->obtieneListado();
	$elemento=new NominaProy();

	foreach($lista as $elemento){
    ?>		
        <tr class="SizeText">
            <td align="center"><?php echo($elemento->getDesProy());?></td>
            <td align="center"><?php echo($elemento->getDesPersonal());?></td>
            <td align="center"><?php echo($elemento->getPeriodoPago());?></td>
            <td align="center"><?php echo("$".number_format($elemento->getNeto(),2));?></td>
            <td align="center"><a href='AgregarNominaProy.php?id=<?php echo($elemento->getId());?>' class='liga_cat'><acronym title="Editar"><img src="../img/Pencil3.png" width="" height="" alt="Editar" style="border:0;" /></acronym></a></td>
            <td align="center"><a href='../src/mx/com/virreinato/web/WebNominaProy.php?id=<?php echo($elemento->getId());?>' class='liga_cat'><acronym title="Borrar" onclick='return confirm("¿Esta seguro que desea eliminar este registro?")' ><img src="../img/DeleteRed.png" width="" height="" alt="Borrar" style="border:0;" /></acronym></a></td>
            <td><a style="cursor:pointer" onclick="Imprimir('../Formatos/Nomina.php',<?php echo($elemento->getId());?>)" class='liga_cat'><acronym title="Imprimir"><img src="../img/printer.png" width="18" height="18" alt="Imprimir" style="border:0;" /></acronym></a></td>
        </tr>
    <?php			
        }//while
    ?>
    </table>
    <br>
    <div align="center"><a href='AgregarNominaProy.php' class='liga_btn'>Agregar Personas a proyectos</a></div>	
</div>	
</body>
<br/><br/>
</html>
       