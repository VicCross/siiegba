<?php

session_start();
include_once("../src/mx/com/virreinato/dao/TarjetaRecAutorizadoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/TarjetaRecAutorizados.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Recursos Autorizados</title>
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
	<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
	<script>
	
	function OnlyNumber(value, elemnt){
			if( isNaN(value) ){
				elemnt.value = ""
			}
	}
	</script>
</head>
<body>
<?php 
    $idTarjeta = null;

   if(isset($_GET["idTarjeta"]))
        {$idTarjeta= (String) $_GET["idTarjeta"];}
?>
<div class="contenido">
<br/>
    <p class="titulo_cat1">Proyectos > Información de Proyectos >  <a class="linkTitulo_cat1" href="TarjetaRegistroMod.php?idTarjeta=<?php echo($idTarjeta);?>"> Tarjeta de Registro </a> > <a class="linkTitulo_cat1" href="lista_RecursosAutorizados.php?idTarjeta=<?php echo($idTarjeta);?>"> Recursos Autorizados </a></p>
    <h2 class="titulo_cat2"> 
    <?php
	$dao=new TarjetaRecAutorizadoDaoJdbc();
	$elemento = new TarjetaRecAutorizados();
	
	if(isset($_GET['id'])){
            echo("Modificar Recursos Autorizados");
            $elemento = $dao->obtieneElemento($_GET['id']);
	}	
	else{
            echo("Alta de Recursos Autorizados");
	}	
	?>
    </h2>
    <br/>
	<form id="frmRecAuto" name="frmRecAuto" method="post" action="../src/mx/com/virreinato/web/CatTarjetaRecAutorizados.php">
            <table width="30%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
              <tr>

                 <td class="SizeText" width="%">
                  <br/> &nbsp; &nbsp; Año de Autorización*: &nbsp; &nbsp; 
                  <input type="text" name="anio_Autorizacion" id="anio_Autorizacion" onkeyup="OnlyNumber(this.value,this)" size="15" maxlength="4" value="<?php if($elemento!= null && $elemento->getAnio()!= null) echo($elemento->getAnio());  ?>"  /></td>
            </tr>	 

            <tr>
                     <td class="SizeText" width="%">
                     &nbsp; &nbsp;Ingreso Autogenerado*: &nbsp;
                <input type="text" name="ingreso_Autogenerado" id="ingreso_Autogenerado" onkeyup="OnlyNumber(this.value,this)" size="15" maxlength="13" value="<?php if($elemento!= null && $elemento->getIngresoAutogenerado() != null) echo($elemento->getIngresoAutogenerado());?>" /></td>
             </tr>
             <tr>   
                <td class="SizeText" width="%">
                &nbsp; &nbsp;Aportaciones Terceros*: &nbsp; 
                <input type="text" name="recursos_Terceros" id="recursos_Terceros" onkeyup="OnlyNumber(this.value,this)" size="15" maxlength="13" value="<?php if($elemento!=null && $elemento->getAportacionesTerceros() != null ) echo($elemento->getAportacionesTerceros()); ?>" /></td>

              </tr>

              <tr>
                <td align="center" colspan="6"><br/>
                   <input name="guardar" style="cursor:pointer" type="submit" value="Guardar"  class='btn' />
                   &nbsp; &nbsp; &nbsp;
                   <input name="cancelar" sytle="cursor:pointer"  type="button" value="Cancelar" class="btn" onclick="Regresar()"  />

                </td>
              </tr>
            </table>
            <?php
                echo("<input type='hidden' name='id_Tarjeta' value='".$idTarjeta."' />");
                if( $elemento!=null && $elemento->getIdRecurso()!= null )  echo( "<input type='hidden' name='id' value='".$elemento->getIdRecurso()."'  /> " ); 
            ?>
	</form>
</div>
<br/><br/>
</body>
<script>

function Regresar(){
    var tarjeta = '<?php echo$idTarjeta ?>'
    window.location="lista_RecursosAutorizados.php?idTarjeta="+tarjeta;
}

  var frmvalidator  = new Validator("frmRecAuto");
 frmvalidator.addValidation("anio_Autorizacion","req","Por favor capture en año de Autorizacion.");
 frmvalidator.addValidation("ingreso_Autogenerado","req","Por favor capture el ingreso Autogenerado.");
 frmvalidator.addValidation("recursos_Terceros","req","Por favor capture los recursos de Terceros.");
</script>
</html>
</body>