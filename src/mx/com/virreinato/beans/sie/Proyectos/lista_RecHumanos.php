<?php

session_start();
include_once("../src/mx/com/virreinato/dao/RecHumanoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/RecHumanos.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<link rel="stylesheet" type="text/css" href="../css/pro_dop_1.css">
<script>
	function Eliminar(idRecurso){
		alert("Registro Eliminado!");
	}
</script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
<script>
	$(document).ready(function() {
		$(".menu ul").css({
			background: "<?php echo $parametro->getValor(); ?>"
		});
		$("#.menu a").css({
			background: "<?php echo $parametro->getValor(); ?>"
		});
	});
</script>

</head>
<body>
<?php 
    $respuesta = null;
    $idCarta = null;
    
   if(isset($_GET["respuesta"]))
        {$respuesta= (String) $_GET["respuesta"];}
   if(isset($_GET["idCarta"]))
        {$idCarta= (String) $_GET["idCarta"];}
?>
<div class="contenido">
    <br/>
    <p class="titulo_cat1">Proyectos > Información de Proyectos > <a class="linkTitulo_cat1" href="CartaConsAdd.php?idCarta=<?php echo($idCarta); ?>" >Carta Constitutiva de Proyectos</a> > Recursos Humanos </p>
    <h2 class="titulo_cat2">Recursos Humanos</h2>
    <?php
        if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>");
    ?>  
    <table width="60%" border="0" cellspacing="0" cellpadding="5" class='tb_cat' align='center' >
        <tr>
          <th width="5%">Orden</th>
          <th width="50%">Recurso</th>
          <th width="2%"></th>
          <th width="2%"></th>
        </tr>
	    
        <?php 
            $dao=new RecHumanoDaoJdbc();
            $lista=$dao->obtieneListado($idCarta );
            $elemento = new RecHumanos();

            foreach($lista as $elemento){

        ?>
        <tr>
            <td align='center'><?php echo($elemento->getNum_orden());?></td>
            <td align='center'><?php echo($elemento->getRec_humano());?></td>
            <td><a href='RecHumanos_Carta.php?id=<?php echo($elemento->getId());?>&idCarta=<?php echo($idCarta);?>' class='liga_cat'><acronym title="Editar"><img src="../img/Pencil3.png" width=16"" height="16" alt="Editar" style="border:0;" /></acronym> </a></td>
            <td><a href='../src/mx/com/virreinato/web/CatRecHumano.php?id=<?php echo($elemento->getId());?>&idCarta=<?php echo($idCarta);?>' class='liga_cat' ><acronym title="Borrar"><img src="../img/DeleteRed.png" width="16" height="16" alt="Borrar" style="border:0;" /></acronym> </a></td>
          </tr>	
					
        <?php  } ?>
			
	</table>
	<br>
	<div align="center">
	    <a href='CartaConsAdd.php?idCarta=<?php echo($idCarta);?>' class='liga_btn'> Regresar </a>
	    &nbsp; &nbsp; &nbsp;
            <a href='RecHumanos_Carta.php?idCarta=<?php echo($idCarta);?>' class='liga_btn'> Agregar Nuevo Recurso</a>
	</div>
</div>
</body>
<br/><br/>
</html>