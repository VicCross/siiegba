<?php

session_start();
include_once("../src/mx/com/virreinato/dao/TarjetaCalendarioDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/TarjetaCalendario.class.php");
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<link rel="stylesheet" type="text/css" href="../css/pro_dop_1.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
	function Eliminar(idCalendario){
		alert("Registro Eliminado!");
	}
</script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<body>
<?php 
    $respuesta = null;
    $idTarjeta = null;
    
   if(isset($_GET["respuesta"]))
        {$respuesta= (String) $_GET["respuesta"];}
   if(isset($_GET["idTarjeta"]))
        {$idTarjeta= (String) $_GET["idTarjeta"];}
?>
    <div class="contenido">
	<br/>
	<p class="titulo_cat1">Proyectos > Información de Proyectos >  <a class="linkTitulo_cat1" href="TarjetaRegistroMod.php?idTarjeta=<?php echo($idTarjeta);?>"> Tarjeta de Registro </a> > Metas del Proyecto</p>
	<h2 class="titulo_cat2"><?php if(isset($_GET['proyecto'])) echo($_GET['proyecto']); ?></h2>
        <h2 class="titulo_cat2">Catálogo de Calendario Financiero</h2>
        <?php
            if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>");
        ?>
        <table width="65%" border="0" cellspacing="0" cellpadding="5" class='tb_cat' align='center' >
	  <tr>
	    <th width="10%">Partida</th>
	    <th width="50%">Descripción</th>
	    <th width="50%">Meta</th>
	    <th width="2%"></th>
	    <th width="2%"></th>
	  </tr>
	  
	  <?php 
            $dao=new TarjetaCalendarioDaoJdbc();
            $lista=$dao->obtieneListado($idTarjeta);
            $elemento = new TarjetaCalendario();
            foreach($lista as $elemento){
	  ?>
        <tr class="SizeText">
            <td align="center" ><?php echo($elemento->getNum_Partida());?></td>
            <td align="center" ><?php echo($elemento->getDesc_Partida());?></td>
            <td align="center" ><?php echo($elemento->getDescMeta());?></td>
            <td><a href="AgregarCalendario.php?id=<?php echo($elemento->getIdCalendario());?>&idTarjeta=<?php echo($idTarjeta);?>" class='liga_cat'><acronym title="Editar"><img src="../img/Pencil3.png" width="16" height="16" alt="Editar" style="border:0;" /></acronym></a></td>
            <td><a href="../src/mx/com/virreinato/web/CatTarjetaCalendario.php?id=<?php echo($elemento->getIdCalendario());?>&id_Tarjeta=<?php echo($idTarjeta);?>" class='liga_cat' ><acronym title="Borrar"><img src="../img/DeleteRed.png" width="16" height="16" alt="Borrar" style="border:0;" /></acronym> </a></td>
        </tr>	
					
		<?php  }?>	
	</table>
	<br>
	<div align="center">
		<a href='TarjetaRegistroMod.php?idTarjeta=<?php echo($idTarjeta);?>' class='liga_btn'> Regresar </a>
	    &nbsp; &nbsp; &nbsp;
		<a href='AgregarCalendario.php?idTarjeta=<?php echo($idTarjeta);?>' class='liga_btn'> Agregar Nuevo Calendario</a>
    </div>
</div>
<br/><br/>
</body>
</html>
</body>

