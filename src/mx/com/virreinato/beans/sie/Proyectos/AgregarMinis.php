<?php

session_start();
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/MinistraDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Ministra.class.php");
include_once("../src/mx/com/virreinato/dao/CuentaBancariaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CuentaBancaria.class.php");
include_once("../src/mx/com/virreinato/dao/CatProyectoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatProyecto.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<link href="../css/calendario.css" type="text/css" rel="stylesheet">
<script src="../js/calendar.js" type="text/javascript"></script>
<script src="../js/calendar-es.js" type="text/javascript"></script>
<script src="../js/calendar-setup.js" type="text/javascript"></script>
<script src="../js/jquery-1.7.2.js"></script>
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
<script language="JavaScript">
function verifica_campos(){

	if( $("#destino").val() == "PR"){

			$("#tr_sol_fondos").show();
			$("#tr_proyecto").show();

	}
	else if( $("#destino").val() == "GB" || $("#destino").val() == "TR" || $("#destino").val() == "DN" || $("#destino").val() == "SL" ){

			$("#tr_sol_fondos").hide();
			$("#tr_proyecto").hide();
			  								
	}
}	
</script>
</head>
<body>
<?php
$error = null;
if(isset($_GET['error']))
{    $error= (String)$_GET['error']; }
?>
<div class="contenido">
  <br/> 
  <p class="titulo_cat1">Proyectos > <a class="linkTitulo_cat1" href="lista_Ministracion.php" > Ministración </a>  </p>
  <h2 class="titulo_cat2">
      <?php
	
	$dao=new MinistraDaoJdbc();
	$elemento=new Ministra();

	if(isset($_GET['id'])){
            echo("Modificar Ministración");
            $elemento=$dao->obtieneElemento($_GET['id']);
	}	
	else{
		echo("Nueva Ministración");
	}	
      ?>
      </h2> 
  <?php if($error!=null) echo("<div align='center' class='msj'>".$error."</div>"); ?>
<form id="fministra" name="fministra" method="post" action="../src/mx/com/virreinato/web/WebMinistra.php">
    <table width="60%" border="0" cellspacing="2" cellpadding="1" class='tb_add_cat' align='center' >
      <tr>
        <td width="30%">Número de operación*:</td>
        <td width="70%"><input type="text" name="numOpera" id="numOpera" value='<?php if($elemento!=null && $elemento->getNumOperacion()!= null) echo($elemento->getNumOperacion());?>'></td>
      </tr>
      <tr>
        <td>Descripción*:</td>
        <td><input type="text" name="descripcion" id="descripcion" size="50" value='<?php if($elemento!=null && $elemento->getDescripcion()!= null) echo($elemento->getDescripcion());?>'/></td>
      </tr>
      <tr>
        <td>Monto*:</td>
        <td><input type="text" name="monto" id="monto" size="50" value='<?php if($elemento!=null ) echo($elemento->getMonto());?>'/></td>
      </tr>
      <tr>
        <td>Fecha (dd-MM-yyyy)*:</td>
        <td><input type="text" name="fecha" id="fecha" size="50" value='<?php if($elemento!=null && $elemento->getFecha()!= null) echo(date("d-m-Y",strtotime($elemento->getFecha())));?>'/></td>
      </tr>
      <tr>
        <td>Destino:</td>
        <td>
            <select name="destino" id="destino" style="width: 150px" onchange='verifica_campos()'>
               <option value='0'>Selecciona</option>
                <option value="DN" <?php if($elemento!=null && $elemento->getDestino()!= null && $elemento->getDestino() == "DN") echo( "selected='selected'");?>> Donativos</option>		    	   
               <option value="GB" <?php if($elemento!=null && $elemento->getDestino()!= null && $elemento->getDestino() == "GB") echo( "selected='selected'");?>> Gasto Basico</option>
               <option value="PR" <?php if($elemento!=null && $elemento->getDestino()!= null && $elemento->getDestino() == "PR") echo( "selected='selected'");?>> Proyectos </option>
               <option value="SL" <?php if($elemento!=null && $elemento->getDestino()!= null && $elemento->getDestino() == "SL") echo( "selected='selected'");?>> Logística sueldos </option>
               <option value="TR" <?php if($elemento!=null && $elemento->getDestino()!= null && $elemento->getDestino() == "TR") echo( "selected='selected'");?>> Terceros</option>
            </select>
        </td>
      </tr>
      <tr>
        <td>Observaciones:</td>
        <td><input type="text" name="observa" id="observa" size="50" value='<?php if($elemento!=null && $elemento->getObservaciones()!= null) echo($elemento->getObservaciones());?>'/></td>
      </tr>
      <tr>
        <td>Cuenta bancaria*:</td>
          <td>
          <select name="cuentaBancaria" id="cuentaBancaria">
            <option value='0'>Selecciona</option>		      
            <?php
                $cuentaBancaria=new CuentaBancariaDaoJdbc();
                $cuentaBancarias=$cuentaBancaria->obtieneListado(); 
                $c = new CuentaBancaria();

                foreach($cuentaBancarias as $c){
                  $sel="";
                  if($elemento!= null && $elemento->getIdCuentaBancaria()!= null ){
                          if($c->getId() == $elemento->getIdCuentaBancaria()){
                                $sel="selected='selected'";
                          }	
                  }	  
                  echo("<option value='".$c->getId()."' ".$sel." >".$c->getNumeroCuenta()."</option>");
                }
            ?>
          </select>
          </td>
        </tr>
        <tr>
            <td>Periodo*:</td>
            <td>
            <select name="periodo" id="periodo">
            <option value='0'>Selecciona</option>		      
            <?php
            if(!isset($_GET['id'])){
                $periodo=new PeriodoDaoJdbc();
                $periodos=$periodo->obtieneListadoAbierto(); 
                $p = new Periodo();

                foreach($periodos as $p){
                  $sel="";
                  if($elemento!= null && $elemento->getIdPeriodo()!= null ){
                    if($p->getId() == $elemento->getIdPeriodo()){
                        $sel="selected='selected'";
                    }	
                  }	  
                  echo("<option value='".$p->getId()."' ".$sel." >".$p->getPeriodo()."</option>");
                }
            }
            else{
                $periodo=new PeriodoDaoJdbc();
                $periodos=$periodo->obtieneListadoAbiertoIdPeriodo($elemento->getIdPeriodo()); 
                $p = new Periodo();

                foreach($periodos as $p){
                  $sel="";
                  if($elemento!= null && $elemento->getIdPeriodo()!= null ){
                    if($p->getId() == $elemento->getIdPeriodo()){
                        $sel="selected='selected'";
                    }	
                  }	  
                  echo("<option value='".$p->getId()."' ".$sel." >".$p->getPeriodo()."</option>");
                }
            }
        ?>
        </select>
        </td>
          </tr>
          <tr id='tr_proyecto' <?php if($elemento!=null && $elemento->getDestino()!= null && ($elemento->getDestino() == "DN" || $elemento->getDestino() == "TR" || $elemento->getDestino() == "GB" || $elemento->getDestino() == "SL" ) ) echo( "style='display:none'"); ?>>
            <td>Proyecto*:</td>
            <td>
            <select name="proyecto" id="proyecto" onChange="obtenerFondos(0)" style="width:340px" >
                        <option value='0'>Selecciona</option>		      
                <?php 
                    $catProyecto=new CatProyectoDaoJdbc();
                    $catProyectos=$catProyecto->obtieneListadoAbierto(); 
                    $cp = new CatProyecto();

                    foreach($catProyectos as $cp){
                      $sel="";
                      if($elemento!= null && $elemento->getIdProyecto()!= null ){
                            if($cp->getId() == $elemento->getIdProyecto()){
                                  $sel="selected='selected'";
                            }	
                      }	  
                      $periodol = $cp->getPeriodo();
                      echo("<option value='".$cp->getId()."' ".$sel." >".$periodol->getPeriodo()."-" .$cp->getDescripcion()."</option>");
                    }
                ?>
            </select>
            </td>
          </tr>

          <tr  id='tr_sol_fondos'  <?php if($elemento!=null && $elemento->getDestino()!= null && ($elemento->getDestino() == "DN" || $elemento->getDestino() == "TR" || $elemento->getDestino() == "GB" || $elemento->getDestino() == "SL" ) ) echo( "style='display:none'");?>>
             <td>Solicitud de Fondos*:</td>
             <td><select id="solicitud" name="solicitud" style="width:340px" ></select></td>
          </tr>
          <tr>
            <td align="center" colspan="2">
                <input name="guardar" type="submit" value="Guardar"  class='btn' />
                &nbsp; &nbsp; &nbsp;
                <input name="cancelar" type="button" onclick="Regresar()" value="Cancelar"  class='btn' />
            </td>
          </tr>
        </table>
        <?php if($elemento!=null && $elemento->getId()!=null ) echo("<input type='hidden' name='id' value='".$elemento->getId()."' />");?>
        <input type='hidden' name='formaCarga' id='formaCarga' value='1' />
		
	</form>
</div>
<script type="text/javascript">

$().ready(function(){
	  var idSol = '<?php echo($elemento->getIdSolicitud()) ?>'
	  if( idSol != null ){
		  obtenerFondos(idSol);
	  }
});

function obtenerFondos(sel){
	var PSolicitud = $("#proyecto").val();
	$.ajax({
		url: "WebMinistra.do",
		type: "POST",
		data: "PSolicitud="+PSolicitud+"&Selecionado="+sel,
		success: function(data){ $("#solicitud").html(data); }
	});
}

function Regresar(){
	window.location="lista_Ministracion.php";
}

 var frmvalidator  = new Validator("fministra");
 frmvalidator.addValidation("numOpera","req","Por favor capture el número de operación.");
 frmvalidator.addValidation("descripcion","req","Por favor capture la Descripción.");
 frmvalidator.addValidation("fecha","req","Por favor capture la Fecha.");
 frmvalidator.addValidation("proyecto","req","Por favor seleccione un Proyecto.");
 frmvalidator.addValidation("periodo","req","Por favor seleccione un Periodo.");
 frmvalidator.addValidation("cuentaBancaria","req","Por favor seleccione una Cuenta Bancaria.");
 
 Calendar.setup({ inputField : "fecha", ifFormat : "%d-%m-%Y", button:"fecha" });
 </script>
<br/><br/>
</body>
</html>