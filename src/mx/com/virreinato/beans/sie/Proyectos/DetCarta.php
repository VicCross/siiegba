<?php

session_start();
include_once("../src/mx/com/virreinato/dao/CatCartaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatCarta.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
if(isset($_POST['Modificado'])){
?>
<table width="80%" border="0" cellspacing="0" cellpadding="5" class='tb_cat' align='center' >
    <tr >
        <th width="33%" align="center">Nombre<br>del proyecto</th>
        <th width="33%" align="center">Meta</th>
        <th width="11%" align="center">Costo total</th>
        <th width="20%" align="center">L&iacute;der<br>del proyecto</th>
        <th width="11%" align="center">Versión</th>	
        <th width="1%"></th>
        <th width="1%"></th>	
        <th width="1%"></th>	
    </tr>
    <?php
        $dao=new CatCartaDaoJdbc();
        $lista=$dao->obtieneListado($_POST["Anio"] );
        $elemento = new CatCarta();

        foreach($lista as $elemento){
    ?>
    <tr class="SizeText">
      <td align="center"><?php echo($elemento->getNombreProyecto());?></td>
      <td align="center"><?php echo($elemento->getMeta());?></td>
      <td align="center"><?php echo($elemento->getCosto());?></td>
      <td align="center"><?php echo($elemento->getNombreLider());?></td>
        <td align="center"><?php if( $elemento->getVersion() != null ) echo($elemento->getVersion());?></td> 
            <td ><a href='CartaConsAdd.php?idCarta=<?php echo($elemento->getId());?>' class='liga_cat'><acronym title="Editar"><img src="../img/Pencil3.png" width="16" height="16" alt="Editar" style="border:0;" /></acronym></a></td>
       <td ><a href='../src/mx/com/virreinato/web/CatCartaConst.php?id=<?php echo($elemento->getId());?>' class='liga_cat' onclick='return confirm("¿Esta seguro que desea eliminar este registro?")' ><acronym title="Borrar"><img src="../img/DeleteRed.png" width="16" height="16" alt="Borrar" style="border:0;" /></acronym></a></td>			    
            <td ><a  style="cursor:pointer" onclick="Imprimir('../Formatos/ImprimirCarConst.php',<?php echo($elemento->getId());?>)" class='liga_cat' ><acronym title="Imprimir"><img src="../img/printer.png" width="18" height="18" alt="Imprimir" style="border:0;" /></acronym></a></td>
    </tr>	
			  
    <?php			
        }
    ?>
	  	  	  
</table>
<?php } ?>