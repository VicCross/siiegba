<?php

session_start();
include_once("../src/mx/com/virreinato/dao/CatCartaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatCarta.class.php");
include_once("../src/mx/com/virreinato/dao/CatProyectoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatProyecto.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html>
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<meta http-equiv="X-UA-Compatible" content="IE=7" />

<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
<script language="JavaScript">
	
	function setFecha(){
		Calendar.setup({
		      dateField      : 'fechaIni',
		      triggerElement : 'botonFechaIni'      
		   	 });
		Calendar.setup({
		      dateField      : 'fechaFin',
		      triggerElement : 'botonFechaFin'      
		   	 });
		Calendar.setup({
		      dateField      : 'fechaVersion',
		      triggerElement : 'botonFechaVersion'      
		   	 });
	}
	
	function OnlyNumber(value, elemnt){
			if( isNaN(value) ){
				elemnt.value = ""
			}
	}
	
</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Editar Proyecto</title>
</head>
<body>
<?php 
    $respuesta = null;
    $idCarta = null;
    $error = null;
    
   if(isset($_GET["respuesta"]))
        {$respuesta= (String) $_GET["respuesta"];}
    if(isset($_GET["error"]))
        {$error = (String) $_GET["error"];}
?>
<div class="contenido">
<br/>
<p class="titulo_cat1">Proyectos > Información de Proyectos > <a class="linkTitulo_cat1" href="CartaCons.php" >Carta Constitutiva de Proyectos</a> </p>
<h2 class="titulo_cat2">
 <?php
    $daoCarta=new CatCartaDaoJdbc();
    $elemento=new CatCarta();	
	
    if(isset($_GET['idCarta'])){
        $idCarta = (String)$_GET['idCarta'];
        echo("Carta Constitutiva del Proyecto");
        $elemento = $daoCarta->obtieneElemento($idCarta); 
    }else{
	echo("Crear Carta Constitutiva del Proyecto");
    }	
?>
</h2>
<br/>
<?php
    if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>");
    if($error!=null) echo("<div align='center' class='msj'>".$error."</div>");
    ?>
<form id="frmNuevaCartaCons" name="frmNuevaCartaCons" method="post" action="../src/mx/com/virreinato/web/CatCartaConst.php">
   <table width="80%" border="0" cellspacing="3" cellpadding="3" class='tb_add_cat' align='center' >
        <tr class="SizeText">
           <td width='12%'> Nombre del Proyecto*:</td>
           <td width='38%'><select id="proyectoCC" name="proyectoCC" style="width:350px"  onchange="obtenerMetas(0)"  >
                <option value="0"> Selecciona </option>
                <?php
                    if( $idCarta == null){
                        //obtengo el listado de los proyectos sólo de los periodos abiertos
                        $dao=new CatProyectoDaoJdbc();
                        $lista=$dao->obtieneListadoAbierto2();
                        $elementoLista=new CatProyecto();

                        foreach($lista as $elementoLista){
                            $sel = "";
                            if($elemento!= null && $elemento->getIdProyecto()!= null ){
                                     if($elemento->getIdProyecto() == $elementoLista->getId() ){ $sel = "selected='selected'"; }
                            }	
                            echo("<option value=".$elementoLista->getId()." ".$sel." >" .$elementoLista->getDescripcion()."</option>");
                        }
                    }
                    else{
                        //obtengo el listado de los proyectos de los periodos abiertos más el proyecto actual
                        $dao=new CatProyectoDaoJdbc();
                        $lista=$dao->obtieneListadoAbiertoIdProy2($elemento->getIdProyecto()); 
                        $elementoLista=new CatProyecto();

                        foreach($lista as $elementoLista){
                            $sel = "";
                            if($elemento!= null && $elemento->getIdProyecto()!= null ){
                                if( $elemento->getIdProyecto() == $elementoLista->getId() ){ $sel = "selected='selected'"; }
                            }	
                            echo("<option value=".$elementoLista->getId()." ".$sel." >" .$elementoLista->getDescripcion()."</option>");
                        }
                    }	      	
                ?>
            </select></td>
            
            <td  width='15%' aling="center">Meta del Proyecto*:</td>
	        <td  width='35%'><select id="meta_Proyecto" name="meta_Proyecto" style="width:300px"></select></td>
	     </tr>
	     
	     <tr>   
	        <td>Dependencia* </td>
	        <td><input type="text" name="dependenciaCC" value="<?php if($elemento!=null && $elemento->getDependencia()!= null) echo($elemento->getDependencia());?>"  id="dependenciaCC" size="55" maxlength="254" /></td>
	        <td>Versi&oacute;n del Documento*</td>
	        <td><input type="text" name="fechaVersionCC" id="fechaVersionCC" size="20" maxlength="254" value="<?php if($elemento!=null && $elemento->getVersion()!= null) echo($elemento->getVersion());?>"  /></td>
	      </tr>
	      
	      <tr>   
	        <td>Costo total* </td>
	        <td colspan='3'><input type="text" name="costoCC" value="<?php if($elemento!=null && $elemento->getCosto()!= null) echo($elemento->getCosto());?>"  id="costoCC" size="55" maxlength="254" /></td>
	      </tr>  

	   <tr class="SizeText">
	         <td>Objetivos espec&iacute;ficos*:</td>
	         <td colspan='3'>
	            <textarea name="objEspCC" id="objEspCC" rows="7"  cols="107"><?php if($elemento!=null && $elemento->getObjetivoEsp()!= null) echo($elemento->getObjetivoEsp());?></textarea>
	         </td>
	    </tr>     
	    <tr class="SizeText">     
           <td>Descripción del Entregable Final*: </td> 
           <td><textarea name="desc_Entregable" id="desc_Entregable" cols="35" rows="5"  maxlength="254"><?php if($elemento!= null && $elemento->getDescEntregable()!=null ) echo($elemento->getDescEntregable());?></textarea></td> 
	       <td>Características Entregable Final*:</td>
	       <td><textarea name="carac_Entregable" id="carac_Entregable" rows="5"  maxlength="254" cols="34"><?php if($elemento!= null && $elemento->getCaracteristicas_Entregable()!=null ) echo($elemento->getCaracteristicas_Entregable());?></textarea></td>
	     </tr>
	     
	     
	     <tr class="SizeText">
	        <td>Entregado Por*</td>
	        <td> <input type="text" maxlength="254" size="48" name="entregado" id="entregado" value="<?php  if($elemento!= null && $elemento->getEntregado()!=null) echo($elemento->getEntregado());?>"/></td>
	        <td> Validado Por*: </td>
	        <td> <input type="text" name="validado" id="validado" size="46" maxlength="254" value="<?php  if($elemento!= null && $elemento->getValidado()!=null ) echo($elemento->getValidado());?>"/></td>
	     </tr>
	     
	     <tr>
	        <td colspan="4" align="center"  >
	                <br/><input name="guardar" style="cursor:pointer" type="submit" onclick="GuardarCarta()" value="Guardar"  class='btn' />
		    		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;   
		    		<input name="cancelar" sytle="cursor:pointer"  type="button" value="Cancelar" class="btn" onclick="Regresar()"  />
		    		 
	        </td>
	        
	     </tr>
             
        <?php if($idCarta != null){  ?>
           <tr class="SizeText">
               <td colspan="4">
               <br/>
               <hr width="95%"  >
               <div align="center">  
                    <a href="lista_Cronograma.php?idCarta=<?php echo($idCarta);?>"    class="link"  >Cronograma</a> &nbsp; &nbsp; &nbsp; 
                    <a href="lista_Equipo.php?idCarta=<?php echo($idCarta);?>"        class="link"  >Miembros del Equipo</a> &nbsp; &nbsp; &nbsp; 
                    <a href="lista_Entregables.php?idCarta=<?php echo($idCarta);?>"   class="link"  >Entregables Parciales</a> &nbsp; &nbsp; &nbsp; 
                    <a href="lista_RecHumanos.php?idCarta=<?php echo($idCarta);?>"    class="link"  >Recursos Humanos</a> &nbsp; &nbsp; &nbsp; 
                    <a href="lista_RecMateriales.php?idCarta=<?php echo($idCarta);?>" class="link"  >Recursos Materiales</a> &nbsp; &nbsp; &nbsp; 
               </div> 
               </td>
           </tr>
         <?php  } ?>  	      
    </table>
    
<?php if($elemento!=null && $elemento->getId()!=null) echo("<input type='hidden' name='id' value='".$elemento->getId()."' />");?>
</form>
</div>
<script>

 $().ready(function(){
      var idMeta = '<?php echo$elemento->getIdMeta()?>'
      obtenerMetas(idMeta);
      
 });
 
 function obtenerMetas( sel ){
	 var idProyecto = $("#proyectoCC").val();
	 $.ajax({
		 url: "../src/mx/com/virreinato/web/CatCartaConst.php",
		 type: "POST",
		 data: "obtenerMetas="+idProyecto+"&sel="+sel,
		 success: function(data){ $("#meta_Proyecto").html(data); }
	 });
	 
 }


function Regresar(){
	window.location="CartaCons.php";
}

var frmvalidator  = new Validator("frmNuevaCartaCons");
 frmvalidator.addValidation("proyectoCC","dontselect=0","Por favor selecciona el proyecto");
 frmvalidator.addValidation("meta_Proyecto","dontselect=0","Por favor selecciona la meta del proyecto");
 frmvalidator.addValidation("dependenciaCC","req","Por favor capture la dependencia.");
 frmvalidator.addValidation("fechaVersionCC","req","Por favor capture la versión del documento.");
 frmvalidator.addValidation("costoCC","req","Por favor capture el costo total.");
 frmvalidator.addValidation("objEspCC","req","Por favor capture los objetivos específicos.");
 frmvalidator.addValidation("desc_Entregable","req","Por favor capture la descripción del entregable final.");
 frmvalidator.addValidation("carac_Entregable","req","Por favor capture las características del entregable final.");
 frmvalidator.addValidation("entregado","req","Por favor capture el nombre de quién entregó.");
 frmvalidator.addValidation("validado","req","Por favor capture el nombre de quién validó.");
 </script>
<br/><br/>
</body>
</html>

