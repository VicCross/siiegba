<?php

session_start();
include_once("../src/mx/com/virreinato/dao/ActividadDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Actividad.class.php");
include_once("../src/mx/com/virreinato/dao/PartidaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Partida.class.php");
include_once("../src/mx/com/virreinato/dao/DetPresupuestoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/DetPresupuesto.class.php");
include_once("../src/mx/com/virreinato/dao/PresupuestoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Presupuesto.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html>
<html>
<?php
  $idSol = null;
  $idMeta = null;
  if(isset($_GET['idSol'])){$idSol = (String) $_GET['idSol'];}
  if(isset($_GET['idMeta'])){$idMeta = (String) $_GET['idMeta'];}
  $montoPartida = 0; 
  
  $daoP=new DetPresupuestoDaoJdbc();
  $detP = new DetPresupuesto();
  
  if(isset($_GET['id'])){
    $detP = $daoP->obtieneElemento($_GET['id']);
    $montoPartida = $detP->getMonto();
  }
  
  $descripcion = $daoP->obtieneDetalle($idSol);
  
  $daoPre = new PresupuestoDaoJdbc();
  $p = new Presupuesto();
  $p = $daoPre->obtieneElemento($idSol);	
?>
<head>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
<script language="JavaScript">

   var totalPartida = 0;
   var totalSolicitud = 0;
   var idSol = 0;
   
   $().ready(function(){
	   idSol = '<?php echo$idSol?>';
   });

	function Mostrar(value,name){
		$("#idArea").hide();
		$("#idProyecto").hide();
		$("#"+value).show();
	}
	
	function validarPartida(){
		var partida = $("#partida_sol").val();
		var idMeta = '<?php echo$idMeta ?>';

		$.ajax({
			url: "PresupuestoDet.do",
			type: "POST",
			data: "validarPartida="+partida+"&idMeta="+idMeta+"&idSolicitud="+idSol,
			success: function(data){  
				   var aux = data.split("/");
				   totalPartida = aux[0];
				   totalSolicitud = aux[1];
				}
		});
		
	}
	
	function validarMonto(){
		var compara = totalPartida - totalSolicitud;
		var partidaActual = '<?php echo$montoPartida ?>';
		var monto = parseFloat( $("#monto_det").val() );
		var falta = 0;
		
		if( partidaActual != 0 ){
			compara = compara + partidaActual;
		}
	
		if( monto > compara ){
			if( partidaActual != 0 ){
				falta = totalPartida - totalSolicitud + partidaActual; 
			}else{
				falta = totalPartida - totalSolicitud;
			}
		      
		    alert("No puedes superar el monto del Capítulo \n que es de: $"+totalPartida+"\n solo puedes Solicitar: $"+falta);
			$("#monto_det").val(falta);
	   }  
		
		
		
	 }
	
	function OnlyNumber(value, elemnt){
			if( isNaN(value) ){
				elemnt.value = ""
			}
	}
	
	function Regresar(){
		var idSolicitud = '<?php echo$idSol ?>';
		window.location="SolicitudEdit.php?id="+idSolicitud;
	}
</script>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" >
<title>Editar Presupuesto</title>
</head>
<body>
<div class="contenido">
<br/>
<p class="titulo_cat1">Proyectos > <a class="linkTitulo_cat1" href="Solicitud.php" >Solicitud de Presupuesto</a> > <a class="linkTitulo_cat1" href="SolicitudEdit.php?id=<?php echo($idSol); ?>" > Detalle de Solicitud de Presupuesto </a> </p>
<h2 class="titulo_cat2">
<?php  
   if(isset($_GET['id']))
   {	echo("Modificar Partida"); }
    else
    {	echo("Alta de Nueva Partida"); }
?>
</h2>
<form action="../src/mx/com/virreinato/web/CatDetPresupuesto.php" method="POST" name="frmDetSolicitud" id="frmDetSolicitud">
  <table width="40%" border="0" cellspacing="0" cellpadding="5" class='tb_add_cat' align='center'>
      <tr class="SizeText">
          <td>
            <?php $aux = explode("/", $descripcion); ?>
            <br/>&nbsp;&nbsp;&nbsp;Proyecto: <?php echo$aux[0] ?>

            <br/><br/>&nbsp;&nbsp;&nbsp;Meta: <?php echo$aux[1] ?>

            <?php
                $dao=new ActividadDaoJdbc();
                $lista = $dao->obtieneActividades();
                $elemento=new Actividad();
            ?>
            <br/><br> &nbsp;&nbsp;&nbsp;Actividad*: <select name="actividades_sol" id="actividades_sol" style="width:320px" >
            <option value="0" > Selecciona </option>
            <?php

                foreach($lista as $elemento){
                    $sel = "";
                    if($detP!= null && $detP->getIdActiviad()!= null ){
                        if( $detP->getIdActiviad() == $elemento->getId() ){ $sel = "selected='selected'"; }
                        }
                    echo("<option value=".$elemento->getId()." ".$sel." >" .$elemento->getDescripcion()."</option>");
	             }
            ?> 
            </select>
            
            <br> <br> &nbsp;&nbsp;&nbsp;Partida*: &nbsp; &nbsp;<select style="width:320px" name="partida_sol" id="partida_sol" onchange="" >
                <option value="0" >  Selecciona </option>
                <?php
                    $daoPartida = new PartidaDaoJdbc();
                    $listaPartida = $daoPartida->obtieneListado();
                    $partida = new Partida();

                    foreach($listaPartida as $partida){
                        $sel = "";
                        if($detP!= null && $detP->getIdPartida()!= null ){
                            if( $detP->getIdPartida() == $partida->getId() ){ $sel = "selected='selected'"; }
                        }
                            echo("<option value=".$partida->getId()." ".$sel." >" .$partida->getPartida()." ".$partida->getDescripcion()."</option>");
                    }
                ?>
            </select>
            
            <br> <br> &nbsp;&nbsp;&nbsp;Monto $*:&nbsp; <input name="monto_det" id="monto_det"  type="text" size="15" maxlength="13" onblur="" onkeyup="OnlyNumber(this.value,this)" value="<?php if($detP!= null && $detP->getMonto()!= null ){ echo($detP->getMonto()); }?>" />


            <br> <br> &nbsp;&nbsp;&nbsp;Productos a Obtener*: <input type="text" size="15" name="productos" id="productos" maxlength="10" onkeyup="OnlyNumber(this.value,this)" value="<?php if($detP!= null && $detP->getProductos_obtenidos()!= null ){ echo($detP->getProductos_obtenidos()); }?>" />


            <br> <br> &nbsp;&nbsp;&nbsp;Detalles de los materiales o servicios*:
            <br/>&nbsp; &nbsp;  &nbsp; &nbsp;<textarea name="materiales_sol" id="materiales_sol" rows="3"  maxlength="150" cols="43"><?php  if($detP!= null && $detP->getDetalle_productos() != null ){ echo($detP->getDetalle_productos()); } else if($p != null && $p->getObservaciones()!= null){echo($p->getObservaciones()); }?></textarea>
            
            </td>
      </tr>
      <tr>
        <td align="center" colspan="3">
            <input name="guardar" style="cursor:pointer" type="submit" value="Guardar"  class='btn' />
            &nbsp; &nbsp; &nbsp;
            <input name="cancelar" style="cursor:pointer" type="button" onclick="Regresar()"  value="Cancelar"  class='btn' />
        </td>
      </tr>
  </table>
  <?php echo("<input type='hidden' name='idSolicitud' value='".$idSol."' />"); 
      if($detP!= null && $detP->getId()!= null ) echo( "<input type='hidden' name='id' value='".$detP->getId()."' >"); ?>
</form>
</div>
<script type="text/javascript">

var frmvalidator  = new Validator("frmDetSolicitud");
 frmvalidator.addValidation("monto_det","req","Por favor capture el Monto.");
 frmvalidator.addValidation("actividades_sol","dontselect=0","Por favor seleecione la Actividad.");
 frmvalidator.addValidation("idMeta","dontselect=0,"Por favor seleccione la Meta.");
 frmvalidator.addValidation("partida_sol","dontselect=0","Por favor seleccione la Partida.");
 frmvalidator.addValidation("productos","req","Por favor capture el No. de Productos obtenidos.");
 frmvalidator.addValidation("materiales_sol","req","Por favor capture los Detalles del material o servicio.");
 </script>
<br/><br/>
</body>
</html>    
            