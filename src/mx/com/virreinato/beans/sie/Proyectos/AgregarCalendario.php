<?php

session_start();
include_once("../src/mx/com/virreinato/dao/TarjetaCalendarioDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/TarjetaCalendario.class.php");
include_once("../src/mx/com/virreinato/dao/PartidaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Partida.class.php");
include_once("../src/mx/com/virreinato/dao/ProyectoMetaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProyectoMeta.class.php");
include_once("../src/mx/com/virreinato/dao/TarjetaRegistroDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/TarjetaRegistro.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Calendario</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
    <script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
	<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
    <script>
    function OnlyNumber(value, elemnt){
                    if( isNaN(value) ){
                            elemnt.value = ""
                    }
    }

    </script>
</head>
<?php 
    $idTarjeta = null;

   if(isset($_GET["idTarjeta"]))
        {$idTarjeta= (String) $_GET["idTarjeta"];}
?>
<body>
<div class="contenido">
<br/>
<p class="titulo_cat1">Proyectos >  Información de Proyectos >  <a class="linkTitulo_cat1" href="TarjetaRegistroMod.php?idTarjeta=<?php echo($idTarjeta)?>"> Tarjeta de Registro </a> > <a class="linkTitulo_cat1" href="lista_Calendario.php?idTarjeta=<?php echo($idTarjeta)?>"> Calendario Financiero </a> </p>
<h2 class="titulo_cat2">
<?php
    $dao=new TarjetaCalendarioDaoJdbc();
    $elemento = new TarjetaCalendario();

    if(isset($_GET['id'])){
        echo("Modificar Calendario Financiero");
        $elemento = $dao->obtieneElemento($_GET['id']);
    }	
    else{
        echo("Alta del Calendario Financiero");
    }	
?>
</h2>

    <form id="frmCalendario" name="frmCalendario" method="post" action="../src/mx/com/virreinato/web/CatTarjetaCalendario.php">
        <table width="65%" border="0" cellspacing="0" cellpadding="0" class='tb_add_cat' align='center'>
          <tr>

             <td class="SizeText" >
                <br/>&nbsp;&nbsp;&nbsp;Partida*: <select style="width:150px" name="partida_Calendario" id="partida_Calendario">
                    <option value="0" >  Selecciona </option>
                    <?php
                        $daoPartida = new PartidaDaoJdbc();
                        $listaPartida = $daoPartida->obtieneListado();
                        $partida = new Partida();
                        
                        foreach($listaPartida as $partida){
                            $sel = "";
                            if($elemento!= null && $elemento->getNum_Partida()!= null ){

                                     if($elemento->getNum_Partida() == $partida->getId()){ $sel = "selected='selected'"; }
                            }
                                    echo("<option value=".$partida->getId()." ".$sel." >" .$partida->getPartida()." ".$partida->getDescripcion()."</option>");
                            }
                    ?>
                </select>
                </td>
                <td  class="SizeText">Meta del Proyecto*:</td>
                <td colspan='3'>
                <select id="idMeta" name="idMeta" style="width:300px">
                <option value="0">Selecciona</option>
                <?php

                    //si existe el id meta obtengo el id proyecto
                    if($idTarjeta!=null)
                    {
                        $daoTarjeta=new TarjetaRegistroDaoJdbc();
                        $tarjeta= $daoTarjeta->obtieneElemento($idTarjeta);

                        $daoMeta=new ProyectoMetaDaoJdbc();

                        $listaMetas=$daoMeta->obtenerListado((String)($tarjeta->getIdProyecto()));

                        $meta = new ProyectoMeta();
                        foreach($listaMetas as $meta){
                            $sel = "";
                            if($elemento!= null && $elemento->getIdMeta()!= null ){
                                if($elemento->getIdMeta() == $meta->getIdMeta() ){ $sel = "selected='selected'"; }
                            }
                            echo("<option value=".$meta->getIdMeta()." ".$sel." >" .$meta->getDescripcion()."</option>");
                        }

                    }
                ?>
                </select>
	        </td>  
            </tr>
		  
            <tr>
               <td class="SizeText" width="30%" >&nbsp;&nbsp;&nbsp;Enero*:&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="enero_Calendario" id="enero_Calendario" onkeyup="OnlyNumber(this.value,this)" size="15" maxlength="13" value="<?php if($elemento!=null && $elemento->getEnero()!=null) echo($elemento->getEnero());?>" /></td>
                <td class="SizeText" width="30%" >Febrero*:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="febrero_Calendario" id="febrero_Calendario" onkeyup="OnlyNumber(this.value,this)" size="15" maxlength="13" value="<?php if($elemento!=null && $elemento->getFebrero()!=null) echo($elemento->getFebrero());?>" /></td>
               <td class="SizeText" >Marzo*:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="marzo_Calendario" id="marzo_Calendario" onkeyup="OnlyNumber(this.value,this)" size="15" maxlength="13" value="<?php if($elemento!=null && $elemento->getMarzo()!=null) echo($elemento->getMarzo());?>" /></td>
            </tr>
		  
            <tr>

               <td class="SizeText">&nbsp;&nbsp;&nbsp;Abril*:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="abril_Calendario" id="abril_Calendario" onkeyup="OnlyNumber(this.value,this)" size="15" maxlength="13" value="<?php if($elemento!=null && $elemento->getAbril()!=null) echo($elemento->getAbril());?>" /></td>

               <td class="SizeText" >Mayo*:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="mayo_Calendario" id="mayo_Calendario" onkeyup="OnlyNumber(this.value,this)" size="15" maxlength="13" value="<?php if($elemento!=null && $elemento->getMayo()!=null) echo($elemento->getMayo());?>" /></td>

               <td class="SizeText" >Junio*:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="junio_Calendario" id="junio_Calendario" onkeyup="OnlyNumber(this.value,this)" size="15" maxlength="13" value="<?php if($elemento!=null && $elemento->getJunio()!=null) echo($elemento->getJunio());?>" /></td>
            </tr>
		  
            <tr>

               <td class="SizeText" >&nbsp;&nbsp;&nbsp;Julio*:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="julio_Calendario" id="julio_Calendario" onkeyup="OnlyNumber(this.value,this)" size="15" maxlength="13" value="<?php if($elemento!=null && $elemento->getJulio()!=null) echo($elemento->getJulio());?>" /></td>

                   <td class="SizeText" >Agosto*:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="agosto_Calendario" id="agosto_Calendario" onkeyup="OnlyNumber(this.value,this)" size="15" maxlength="13" value="<?php if($elemento!=null && $elemento->getAgosto()!=null) echo($elemento->getAgosto());?>" /></td>

              <td class="SizeText" >Septiembre*: <input type="text" name="septiembre_Calendario" id="septiembre_Calendario" onkeyup="OnlyNumber(this.value,this)" size="15" maxlength="13" value="<?php if($elemento!=null && $elemento->getSeptiembre()!=null) echo($elemento->getSeptiembre());?>" /></td>
            </tr>

            <tr>

               <td class="SizeText" >&nbsp;&nbsp;&nbsp;Octubre*: <input type="text" name="octubre_Calendario" id="octubre_Calendario" onkeyup="OnlyNumber(this.value,this)" size="15" maxlength="13" value="<?php if($elemento!=null && $elemento->getOctubre()!=null) echo($elemento->getOctubre());?>" /></td>

                   <td class="SizeText" >Noviembre*: <input type="text" name="noviembre_Calendario" id="noviembre_Calendario" onkeyup="OnlyNumber(this.value,this)" size="15" maxlength="13" value="<?php if($elemento!=null && $elemento->getNoviembre()!=null) echo($elemento->getNoviembre());?>" /></td>

              <td class="SizeText" >Diciembre*: &nbsp;&nbsp;<input type="text" name="diciembre_Calendario" id="diciembre_Calendario" onkeyup="OnlyNumber(this.value,this)" size="15" maxlength="13" value="<?php if($elemento!=null && $elemento->getDiciembre()!=null) echo($elemento->getDiciembre());?>" /></td>
            </tr>
		  
		    
            <tr>
               <td align="center" colspan="6"><br/>
                 <input name="guardar" style="cursor:pointer" type="button" value="Guardar" onclick="Validar()"  class='btn' />
                 &nbsp; &nbsp; &nbsp;
                 <input name="cancelar" style="cursor:pointer" type="button" value="Cancelar" onclick="Regresar()"  class='btn' />
              </td>
            </tr>

            <tr>
              <td><br/></td>
            </tr>
        </table>
        <?php
            echo("<input type='hidden' name='id_Tarjeta' value='".$idTarjeta."' />");
            if($elemento!=null && $elemento->getIdCalendario()!=null) echo("<input type='hidden' name='id' value='".$elemento->getIdCalendario()."' />");
        ?>
    </form>
</div>
<br/><br/>
</body>
<script>

function Regresar(){
	var tarjeta = '<?php echo$idTarjeta ?>'
	window.location="lista_Calendario.php?idTarjeta="+tarjeta;
}

function Validar(){
	var msj = "";
	if(  $("#partida_Calendario").val() == 0 ){
		msj = "Por favor seleccione la Partida correspondiente.";
	}
	else if(  $("#idMeta").val() == 0 ){
		msj = "Por favor seleccione la Meta correspondiente.";
	}
	else if(  $("#enero_Calendario").val() == "" && $("#febrero_Calendario").val() == "" && $("#marzo_Calendario").val() == "" && $("#abril_Calendario").val() == "" && $("#mayo_Calendario").val() == "" && $("#junio_Calendario").val() == "" && $("#julio_Calendario").val() == "" && $("#agosto_Calendario").val() == "" && $("#septiembre_Calendario").val() == "" && $("#octubre_Calendario").val() == "" && $("#noviembre_Calendario").val() == "" && $("#diciembre_Calendario").val() == ""  ){
	   msj = "Por favor capture al menos un Mes del Calendario.";	
	}
	
	if(msj != ""){
	  alert(msj);
	}else{
		document.forms['frmCalendario'].submit();
	}
}
 
</script>
</html>