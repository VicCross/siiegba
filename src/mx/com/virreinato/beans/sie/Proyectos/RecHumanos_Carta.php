<?php

session_start();
include_once("../src/mx/com/virreinato/dao/RecHumanoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/RecHumanos.class.php");
include_once("../src/mx/com/virreinato/dao/PartidaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Partida.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE HTML>

<html>
<head>
    <title>Recursos Humanos</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
    <link href="../css/calendario.css" type="text/css" rel="stylesheet">
    <script src="../js/calendar.js" type="text/javascript"></script>
    <script src="../js/calendar-es.js" type="text/javascript"></script>
    <script src="../js/calendar-setup.js" type="text/javascript"></script>
	<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
	<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
    <script language="JavaScript">


    function OnlyNumber(value, elemnt){
                    if( isNaN(value) ){
                            elemnt.value = ""
                    }
    }
    </script>
</head>
<body>
<?php 
    $idCarta = null;

   if(isset($_GET["idCarta"]))
        {$idCarta= (String) $_GET["idCarta"];}
?>
<div class="contenido">
<br/>
<p class="titulo_cat1">Proyectos > Información de Proyectos > <a class="linkTitulo_cat1" href="CartaConsAdd.php?idCarta=<?php echo($idCarta); ?>" >Carta Constitutiva</a> > <a class="linkTitulo_cat1" href="lista_RecHumanos.php?idCarta=<?php echo($idCarta); ?>" > Recursos Humanos </a> </p>
<h2 class="titulo_cat2">
<?php 
    $daoHumano=new RecHumanoDaoJdbc();
    $elemento=new RecHumanos();

    if(isset($_GET['id'])){
        echo("Modificar Recurso Humano");
        $elemento = $daoHumano->obtieneElemento($_GET['id']);
    }	
    else{
        echo("Alta de Nuevo Recurso Humano");
    }	
?>
</h2>
<br/>

<form id="frmRecHumanos" name="frmRecHumanos" method="post" action="../src/mx/com/virreinato/web/CatRecHumano.php">
    <table width="65%" border="0" cellspacing="1" cellpadding="6" class='tb_add_cat' align='center'>
        <tr>
            <td style="display:none"><input type="text" name="id_Carta" id="id_Carta" value="<?php echo($idCarta); ?>" />

        <tr>

            <td class="SizeText" width="%">
                <br/> &nbsp; &nbsp; &nbsp; No. Orden:  <input type="text" name="orden_humano" id="orden_humano" onkeyup="OnlyNumber(this.value,this)" size="5" maxlength="2" value="<?php if($elemento!=null && $elemento->getNum_orden()!= null) echo($elemento->getNum_orden());?>" />
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                Nómina Extraordinario: <input type="text" name="nomina" id="nomina" onkeyup="OnlyNumber(this.value,this)" size="17" maxlength="13" value="<?php if($elemento!=null && $elemento->getNomina()!= 0) echo($elemento->getNomina());?>" />
            </td>
        </tr>
        
        <tr>
            <td class="SizeText" width="%">
                &nbsp; &nbsp; &nbsp; Honorarios:  <input type="text" name="honorarios" id="honorarios"  onkeyup="OnlyNumber(this.value,this)" size="20" maxlength="13" value="<?php if($elemento!=null && $elemento->getHonorarios()!= 0) echo($elemento->getHonorarios());?>" />
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                Factura:  &nbsp;  &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; <input type="text" name="factura_humano" id="factura_humano" size="20" maxlength="255" value="<?php if($elemento!=null && $elemento->getFactura()!= null) echo($elemento->getFactura());?>" />
            </td>
        </tr>
        
        <tr>
		  	       
            <td class="SizeText" width="%">
               &nbsp; &nbsp; &nbsp; Fecha Ejercicio: <input type="text" name="fecha_Ejercicio" id="fecha_Ejercicio"  size="15" maxlength="255" value="<?php if($elemento!=null && $elemento->getFechaEjercicio()!= null) echo(date("d-m-Y",strtotime(  $elemento->getFechaEjercicio())));?>" />
               &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; 
                Documento Fiscal: &nbsp; <input type="text" name="fiscal_humano" id="fiscalhumano" size="20" maxlength="255" value="<?php if($elemento!=null && $elemento->getFiscal()!= null) echo($elemento->getFiscal());?>" />
            </td>
        </tr>
		  
        <tr>  
          <td class="SizeText" width="%">
            &nbsp; &nbsp; &nbsp; No. Cheque: <input type="text" name="cheque_humano" id="cheque_humano" size="19" maxlength="255" value="<?php if($elemento!=null && $elemento->getCheque()!= null) echo($elemento->getCheque());?>" />
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            Hoja de Retención: <input type="text" name="retencion" id="retencion"  size="20" maxlength="255" value="<?php if($elemento!=null && $elemento->getRetencion()!= null) echo($elemento->getRetencion());?>" />

            </td>
        </tr>
    		  
        <tr>      		     
           <td class="SizeText">
             &nbsp; &nbsp; &nbsp; Partida: 
             <select style="width:150px" name="partida_humano" id="partida_humano">
                <option value="0" >  Selecciona </option>
                <?php 
                    $daoPartida = new PartidaDaoJdbc();
                    $listaPartida = $daoPartida->obtieneListado();
                    $partida = new Partida();

                    foreach($listaPartida as $partida){
                        $sel = "";
                        if($elemento!= null && $elemento->getPartida()!= null ){
                         if( $elemento->getPartida() == (String)($partida->getPartida() ) ){ $sel = "selected='selected'"; }
                            }
                                    echo("<option value=".$partida->getPartida()." ".$sel."  >".$partida->getPartida()." ".$partida->getDescripcion()."</option>");
                            }
                ?>
             </select>
             &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
             Recurso Humano*: 
              <br/>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
              &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
              <textarea name="descripcion_humano" id="descripcion_humano" rows="3" cols="31" maxlength="255"><?php if($elemento!=null && $elemento->getRec_humano()!= null) echo($elemento->getRec_humano()); ?></textarea>


          </td>
        </tr>
		  
		    
        <tr>
          <td align="center" colspan="6"><br/>
             <input name="guardar" style="cursor:pointer" type="submit" value="Guardar"  class='btn' />
             &nbsp; &nbsp; &nbsp; 
             <input name="cancelar" style="cursor:pointer" type="button" value="Cancelar"  onclick="Regresar()" class='btn' />
          </td>
        </tr>
    </table>
    <?php if($elemento!=null && $elemento->getId()!=null) echo("<input type='hidden' name='id' value='".$elemento->getId()."' />");?>
</form>
</div>
<script>

 function Regresar(){
	 var carta = '<?php echo$idCarta ?>'
	 window.location="lista_RecHumanos.php?idCarta="+carta;
 }

 var frmvalidator  = new Validator("frmRecHumanos");
 frmvalidator.addValidation("descripcion_humano","req","Por favor capture el Recurso Humano.");

 
 Calendar.setup({ inputField : "fecha_Ejercicio", ifFormat : "%d-%m-%Y", button:"fecha_Ejercicio" });
 </script>
<br/><br/>
</body>

</html>