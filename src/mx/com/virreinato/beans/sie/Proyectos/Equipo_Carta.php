<?php

include_once("../src/mx/com/virreinato/dao/CartaEquipoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CartaEquipo.class.php");
include_once("../src/mx/com/virreinato/dao/EmpleadoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Empleado.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<html>
<head>

<link rel="stylesheet" type="text/css" href="../css/style.css" />
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
<title>Miembros del Equipo</title>
</head>
<body>
<div class="contenido">
  <br/><br/>
  <form name="equipoCarta" id="equipoCarta" action="../src/mx/com/virreinato/web/CatCartaEquipo.php" method="POST" >
   <table width="40%" border="0" cellspacing="0" cellpadding="5" class='tb_add_cat' align='center'>
      <tr>
          <td style="display:none" ><input type="text" id="idCarta" name="idCarta" value='<?php echo($idCarta); ?>' /></td>
      </tr>		
        <tr>
            <?php
                    
            ?>
            <td align="center"><br/>Miembro del Equipo* &nbsp; &nbsp; 
              <select name="id_equipo_carta" id="id_equipo_carta">
                <option value="0" >Selecciona</option>
                <?php
                    $dao=new EmpleadoDaoJdbc();
                    $lista=$dao->obtieneListado();
                    $elementoLista=new Empleado();
                
                    foreach($lista as $elementoLista){
                        $paint = 0;
                        $daoCarta = new CartaEquipoDaoJdbc();
                        $listaCarta = $daoCarta->obtieneListado($idCarta);
                        $Equipo = new CartaEquipo();

                        foreach($listaCarta as $Equipo){
                            if($Equipo->getId() == $elementoLista->getId() ){  $paint = 1; }
                        }

                        if( $paint == 0 )
                        echo("<option value=".$elementoLista->getId().">" .$elementoLista->getNombre()." ".$elementoLista->getApPaterno()." ".$elementoLista->getApMaterno()."</option>");
}
                    ?>
              </select>
            </td>
        </tr>
        <tr>
            <td align="center">
                <input type="submit" style="cursor:pointer" value="Guardar"  class='btn' />
                &nbsp; &nbsp; &nbsp;
                <input name="cancelar" style="cursor:pointer" type="button" value="Cancelar" onclick="Regresar()"  class='btn' />

            </td>
        </tr>
	</table>
	</form>
</div>
<br/><br/>
</body>
<script>
function Regresar(){
	 var carta = '<?php echo$idCarta ?>'
	 window.location="lista_Equipo.php?idCarta="+carta;
}

 var frmvalidator  = new Validator("equipoCarta");
 frmvalidator.addValidation("id_equipo_carta","dontselect=0","Por favor seleccione el miembro del Equipo.");

</script>
</html>

