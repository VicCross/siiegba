<?php

$id_perfil = null;

session_start();
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/CuentaBancariaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CuentaBancaria.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}else{
    $id_perfil = $_SESSION['id'];
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<body>
<?php 

$respuesta = null;
    if(isset($_GET['respuesta'])){ $respuesta = $_GET['respuesta']; }
if(($id_perfil==null || $id_perfil == 3 || $id_perfil == 4))
    echo("<div align='center' class='msj'>Usted no tiene los permisos suficientes para ingresar a este módulo del sistema.</div>");
else{
?>   
<div class="contenido"> 
    <br/><p class="titulo_cat1">Bancos > Cargar Estado de Cuenta</p> 
    <?php if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>");?>	
    <form id="parametros" name="parametros" method="post" action="../src/mx/com/virreinato/web/CargaEstadoCuenta.php" ENCTYPE="multipart/form-data">
        <table width="60%%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
          <tr>
            <td colspan='2'>Seleccione los parámetros para realizar la carga del estado de cuenta:</td>
          </tr>

          <tr>
            <td>Período*:</td>
            <td>
              <select name="id_periodo" id="id_periodo">
                <option value='0'>Selecciona</option>		      
                <?php
                    $periodo=new PeriodoDaoJdbc();
                    $periodos=$periodo->obtieneListadoAbierto(); 
                    $b = new Periodo();

                    foreach($periodos as $b){
                        $sel="";
                        if( $b->getPeriodo() == (String)date('Y'))
                        {    $sel = "selected='selected'";   }

                      echo("<option value='".$b->getId()."' ".$sel." >".$b->getPeriodo()."</option>");
                    }
                ?>
              </select>
            </td>
          </tr>
		  
          <tr>
            <td width="">Número de Cuenta*:</td>
            <td width="">
                <select name="id_cuenta" id="id_cuenta">
                    <option value='0'>Selecciona</option>		      
                    <?php
                        $cuenta=new CuentaBancariaDaoJdbc();
                        $cuentas=$cuenta->obtieneListado(); 
                        $b = new CuentaBancaria();

                        foreach($cuentas as $b){
                            $banco = $b->getBanco();
                          echo("<option value='".$b->getId()."' >".$b->getNumeroCuenta()." (".$banco->getDescripcion().")</option>");
                        }
                    ?>
                </select>
            </td>
          </tr>
		  
          <tr>
            <td width="">Archivo con el  estado de cuenta*:</td>
            <td width="">
                <input type='file' name='file' id='file'>
            </td>
          </tr>
		  
          <tr>
            <td align="center" colspan="4"><input name="cargar" type="submit" value="Cargar Estado de Cuenta"  class='btn' /></td>
          </tr>
        </table>
    </form>
</div>
<script type="text/javascript">
 var frmvalidator  = new Validator("parametros");
  frmvalidator.addValidation("id_periodo","dontselect=0","Por favor seleccione el período.");
  frmvalidator.addValidation("id_cuenta","dontselect=0","Por favor seleccione el número de cuenta.");
  frmvalidator.addValidation("file","req","Por favor seleccione el archivo con el estado de cuenta.");
</script>
<?php
}
?>
</body>
</html>    

