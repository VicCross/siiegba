<?php

$id_perfil = null;

session_start();
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}else{
    $id_perfil = $_SESSION['id'];
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<script language="JavaScript" type="text/javascript" src="../js/CalendarPopup.js"></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script language="JavaScript" type="text/javascript">document.write(getCalendarStyles());</script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<body>
<?php
if($id_perfil==null || $id_perfil == 3 || $id_perfil == 4)
    echo("<div align='center' class='msj'>Usted no tiene los permisos suficientes para ingresar a este módulo del sistema.</div>");
else{
?>
    <div class="contenido"> 
        <br/><p class="titulo_cat1">Bancos > Conciliación Bancaria</p> 

        <form id="fechas" name="fechas" method="post" action="../src/mx/com/virreinato/web/ConciliacionBancaria.php">

            <table width="60%%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
                <tr>
                  <td colspan='2'>Seleccione el período y el mes para consultar o capturar sus movimientos:</td>
                </tr>

                <tr>
                  <td>Mes*:</td>
                  <td>
                    <select name="mes" id="mes" >
                    <option value='0'>Selecciona</option>
                        <option value='1'>Enero</option>
                        <option value='2'>Febrero</option>
                        <option value='3'>Marzo</option>
                        <option value='4'>Abril</option>
                        <option value='5'>Mayo</option>
                        <option value='6'>Junio</option>
                        <option value='7'>Julio</option>
                        <option value='8'>Agosto</option>
                        <option value='9'>Septiembre</option>
                        <option value='10'>Octubre</option>
                        <option value='11'>Noviembre</option>
                        <option value='12'>Diciembre</option>		   		
                    </select>
                  </td>
                  <td>Período*:</td>
                  <td>
                     <select name="id_periodo" id="id_periodo" style="width:110px" > 
                        <?php 
                            $daoPer=new PeriodoDaoJdbc();
                            $listaPer = $daoPer->obtieneListado();
                            $elementoPer = new Periodo();
                            foreach($listaPer as $elementoPer){
                                $sel = "";
                                if( isset($_GET['periodo']) && (int)( $_GET['periodo'] ) == $elementoPer->getId()) $sel = "selected='selected'"; 
                                else if( $_GET['periodo'] == null && (int)($elementoPer->getPeriodo()) == date('Y')  ){ $sel = "selected='selected'";  $periodo = (String)($elementoPer->getId() ); }
                                echo("<option value=".$elementoPer->getId()." ".$sel." >" . $elementoPer->getPeriodo() ."</option>");
                            }
                        ?>  
                     </select> 
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="4"><input name="continuar" type="submit" value="Continuar"  class='btn' /></td>
                </tr>
            </table>
        </form>
    </div>
<script type="text/javascript">
 var frmvalidator  = new Validator("fechas");
  frmvalidator.addValidation("mes","dontselect=0","Por favor seleccione el mes.");
  frmvalidator.addValidation("id_periodo","dontselect=0","Por favor seleccione el periodo.");
</script>
<?php
}
?>
</body>
</html>
                     

