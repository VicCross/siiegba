<?php
include_once("src/classes/Configuracion.class.php");
$config = new Configuracion();
session_start();
$_SESSION['RAIZ'] = $config->getRoot();

if (isset($_SESSION['id']) && $_SESSION['id'] != NULL) {
    include_once("src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");   
	$parametro = new ParametroDaoJdbc();
	$parametro = $parametro->obtieneElemento(5);
    ?>
    <!DOCTYPE html>
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <meta http-equiv="X-UA-Compatible" content="IE=7" />
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>Sistema de Ingresos y Egresos</title>
            <script language="JavaScript" type="text/javascript" src="js/jquery-1.7.2.js" ></script>
            <link rel="stylesheet" type="text/css" href="css/style.css">
            <link rel="stylesheet" type="text/css" href="css/pro_dop_1.css">
            <script>
                $(document).ready(function() {
                    $(".menu ul").css({
                        background: "<?php echo $parametro->getValor(); ?>"
                    });
                    $("#.menu a").css({
                        background: "<?php echo $parametro->getValor(); ?>"
                    });
                    $("#Perfil").css({
                        background: "<?php echo $parametro->getValor(); ?>"
                    });
                    $(".div_menu").css({
                        background: "<?php echo $parametro->getValor(); ?>"
                    });
                    $(".tb_presupuesto th").css({
                        background: "<?php echo $parametro->getValor(); ?>"
                    });
                    $(".tb_presupuestoResumen th").css({
                        background: "<?php echo $parametro->getValor(); ?>"
                    });
                    $(".tb_cat th").css({
                        background: "<?php echo $parametro->getValor(); ?>"
                    });
                    $(".tb_add_cat").css({
                        background: "<?php echo $parametro->getValor(); ?>"
                    });
                });
            </script>

        </head>
        <body>
            <div class="encabezado" style='padding:0px;margin:0px;'> 
                <img src='img/4.png' border='0' style='padding:5px 20px;'>
                <span class='titulo_encabezado'> Sistema de Ingresos y Egresos </span>
            </div>
            <?php include_once "BodyAplication.php"; ?>
            <div>
                <script>
                    var espacio_iframe = 700;
                    if (window.innerHeight) {
                        //navegadores basados en mozilla 
                        espacio_iframe = window.innerHeight - 100;
                    } else {
                        if (screen.availHeight) {
                            //Navegadores basados en IExplorer, es que no tengo innerheight

                            espacio_iframe = parseInt(screen.availHeight) - 250;
                            //alert(espacio_iframe);
                        }
                    }
                    document.write('<iframe frameborder="0" src="about:blank" width="100%" height="' + espacio_iframe + '" id="destino" name="destino" scrolling="auto" style="overflow:visible" >');
                    document.write('</iframe>');
                </script>
            </div>
        </body>
    </html>
<?php
} else {
    header("Location: index.php");
}
?>

