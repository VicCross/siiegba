<?php
header('Content-Type: text/html; charset=UTF-8');
include_once("src/classes/Configuracion.class.php");

$config = new Configuracion();
session_start();
$_SESSION['RAIZ'] = $config->getRoot();

include_once("src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
//Obtenemos el valor del color, su id es el número 5
$parametro = new ParametroDaoJdbc();
$parametro->obtieneElemento(5);

session_destroy();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script language="JavaScript" type="text/javascript" src="js/jquery-1.7.2.js" ></script>
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <!-- Este Script nos ayuda a usar el valor del color que se encuentra como parámetro -->
        <script>
            $(document).ready(function() {
                $("#Perfil").css({
                    background: "<?php echo $parametro->getValor(); ?>"
                });
                $(".div_menu").css({
                    background: "<?php echo $parametro->getValor(); ?>"
                });
                $(".tb_presupuesto th").css({
                    background: "<?php echo $parametro->getValor(); ?>"
                });
                $(".tb_presupuestoResumen th").css({
                    background: "<?php echo $parametro->getValor(); ?>"
                });
                $(".tb_cat th").css({
                    background: "<?php echo $parametro->getValor(); ?>"
                });
                $(".tb_add_cat").css({
                    background: "<?php echo $parametro->getValor(); ?>"
                });
            });
        </script>
        <title>Login SIE-Museo</title>
        <style>
            div {
                position: absolute;
                left: 20%;
                top: 10%;
                width: 900px;
                height: 434px;
                //margin-top: -100px;
                //margin-left: -150px;
                overflow: auto;
                background-image: url(img/Login.png); 

            }
        </style>
    </head>
    <body>  
        <?php
            //Se obtiene el parámetro del nombre del museo
            $parametro = $parametro->obtieneElemento(4);
        ?>
        <span style="margin: 15% 0% 0% 50%; font-size: 42px; font-family: Times New Roman; color: #005697;">
        <?php echo $parametro->getValor(); ?>
        </span>
        <div>            
            <form action="Verifica.php" method="post">                
                <table align="center" border="0"  >
                    <tr height="150px"></tr>
                    <tr>
                        <td>Usuario:</td> <td><input type="Text" name="user" ></td>
                    </tr>
                    <tr>
                        <td>Password:</td> <td> <input type="Password" name="pwd"></td>
                    </tr>
                    <tr>
                        <td colspan='2' align='center' style='font-size:9px;color:red;'>Versión 3.0 <br><br></td>
                    </tr>

                    <tr>
                        <td></td><td><input type="submit" name="envio" value="&nbsp; &nbsp;  Ingresar &nbsp; "></td>
                    </tr>
                </table>
                <br/>
                <center>
                    <label class="Error">        	    	
                        <?php
                        if (isset($_GET['err']) && $_GET['err'] == "1") {
                            echo "Error en usuario y/o contraseña";
                        }
                        ?>
                    </label></center>

            </form>
        </div>


    </body>
</html>