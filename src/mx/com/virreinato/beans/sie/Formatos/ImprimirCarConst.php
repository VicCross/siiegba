<?php

session_start();
include_once("../src/mx/com/virreinato/dao/CatCartaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatCarta.class.php");
include_once("../src/mx/com/virreinato/dao/CatProyectoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatProyecto.class.php");
include_once("../src/mx/com/virreinato/dao/EmpleadoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Empleado.class.php");
include_once("../src/mx/com/virreinato/dao/LineaAccionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/LineaAccion.class.php");
include_once("../src/mx/com/virreinato/dao/CartaEquipoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CartaEquipo.class.php");
include_once("../src/mx/com/virreinato/dao/CartaCronogramaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CartaCronograma.class.php");
include_once("../src/mx/com/virreinato/dao/EntregablesDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Entregables.class.php");
include_once("../src/mx/com/virreinato/dao/RecHumanoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/RecHumanos.class.php");
include_once("../src/mx/com/virreinato/dao/RecMaterialDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/RecMateriales.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/style_Impresion.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
   function Mes(num){
   	 if(num == "01") return "Enero";
   	 if(num == "02") return "Febrero";
   	 if(num == "03") return "Marzo";
   	 if(num == "04") return "Abril";
   	 if(num == "05") return "Mayo";
   	 if(num == "06") return "Junio";
   	 if(num == "07") return "Julio";
   	 if(num == "08") return "Agosto";
   	 if(num == "09") return "Septiembre";
		 if(num == "10") return "Octubre";
		 if(num == "11") return "Novimbre";
		 if(num == "12") return "Diciembre";
   }

   $().ready(function(){
   	 var fechaInicio =  $("#fechaInicio").val();
   	 if( fechaInicio != "" ){
   	 	var auxInicio = fechaInicio.split("-");
   	 	$("#FechaInicio").text("Fecha de Inicio: "+Mes(auxInicio[1])+" "+auxInicio[2] );
   	 }
   	 
   	 var fechaFin =  $("#fechaFin").val();
   	 if(fechaFin != ""){
   	 	var auxFin = fechaFin.split("-");
   	 	$("#FechaFin").text("Fecha de Término: "+Mes(auxFin[1])+" "+auxFin[2] );
   	 }
   	 
   });
</script>
<title>Impresion</title>
</head>
<?php
if(isset($_GET['folio']))
{  $folio = (String)$_GET['folio']; }
  $daoCarta=new CatCartaDaoJdbc();
  $Carta = new CatCarta();
  $Carta = $daoCarta->obtieneElemento($folio); 
  
  $daoProyecto = new CatProyectoDaoJdbc();
  $DescProy = new CatProyecto();
  $DescProy = $daoProyecto->obtieneElemento( (String)( $Carta->getIdProyecto() ));

?>
<body>
<div id="cartaConst"><center><h4>CARTA CONSTITUTIVA DE PROYECTOS</h4></center></div>
<div id="solicitud">
    <table align="center" BORDER WIDTH="100%" HEIGHT="5%" border="1" cellspacing="0" cellpadding="2">
        <tr class="ContenidoCarta">
            <td >Nombre del Proyecto:</td>
            <td  WIDTH="60%" colspan="2" class="Resaltar">
              <?php

                $listaProyecto = $daoProyecto->obtieneListado2();
                $Proyecto = new CatProyecto();
                foreach($listaProyecto as $Proyecto){
                    if($Carta->getIdProyecto() == $Proyecto->getId() ){ echo( $Proyecto->getDescripcion() ); }	
                }
             ?>
            </td>
            <td>Origen del Presupuesto* <?php echo($DescProy->getOrigen()); ?> </td>
        </tr>

        <tr class="ContenidoCarta">
            <td>Dependencia:</td>
            <td colspan="2"><center><?php if($Carta->getDependencia()!=null) echo($Carta->getDependencia()); ?></center></td>
            <td id="FechaInicio"> <input type="hidden" id="fechaInicio" value="<?php if($DescProy->getFechaIniProyecto()!=null) echo(date("d-m-Y",strtotime( $DescProy->getFechaIniProyecto() )) );?>" />  </td>
        </tr>

        <tr class="ContenidoCarta">
            <td>Líder del Proyecto:</td>
            <td colspan="2" align="center"> 
                    <?php
                       $empleado=new EmpleadoDaoJdbc();
                       $empleados=$empleado->obtieneListado(); 
                       $e = new Empleado();
                       
                       foreach($empleados as $e){
                            $lider = $DescProy->getLider();
                            if($e->getId() == $lider->getId())
                            {    echo($e->getNombre()." ".$e->getApPaterno()." ".$e->getApMaterno()); }
                        }
            ?> </td>
            <td id="FechaFin"> <input type="hidden" id="fechaFin" value="<?php if($DescProy->getFechaFinProyecto()!=null) echo(date("d-m-Y",strtotime( $DescProy->getFechaFinProyecto()))); ?>" /> </td>
        </tr>
			
        <tr class="ContenidoCarta">
            <td>Miembros del Equipo:</td>
            <td align="center" colspan="3">
               <?php
                    $daoEquipo = new CartaEquipoDaoJdbc();
                    $listaEquipo = $daoEquipo->obtieneListado($folio);
                    $Equipo = new CartaEquipo();
                    $auxi = count($listaEquipo);
                    $contaux = 0;

                    foreach($listaEquipo as $Equipo){
                        echo($Equipo->getNombre());
                        $contaux++;
                        if($auxi != $contaux){ echo(",&nbsp;");  }
                    }
                ?>
          </td>
        </tr>
			
        <tr class="ContenidoCarta">
            <td>Versión del Documento:</td>
            <td colspan="2"><center><?php if( $Carta->getVersion()!= null ) echo($Carta->getVersion()); ?></center></td>
            <td class="Resaltar">Costo Total: &nbsp; <?php if($DescProy!= null) echo("$".number_format( $DescProy->getMonto(),2)); ?></td>
        </tr>
			
        <tr class="ContenidoCarta">
            <td><br/><br/>Línea de Acción*:</td>
            <td colspan="2">
             <?php
                $linea=new LineaAccionDaoJdbc();
                $lineas=$linea->obtieneListado(); 
                $l = new LineaAccion();
                
                foreach($lineas as $l){
                    $lineaAcc = $DescProy->getLineaAccion();
                     if($l->getId() == $lineaAcc->getId())
                            echo($l->getLineaAccion());
                    }
            ?><br/>
            </td>
            <td ></td>
        </tr>
			
        <tr class="ContenidoCarta">
            <td><b>Eje*:</b></td>
            <td colspan="2" ><b></b><?php echo($Carta->getEje()); ?></td>
            <td></td>
        </tr>
			
        <tr class="ContenidoCarta">
            <td WIDTH="17%"><b><center>Antecendetes</center></b></td>
            <td WIDTH="17%"><b><center>Objetivo General del Proyecto</center></b></td>
            <td WIDTH="17%" ><b><center>Límites del Proyecto (Definir el % de avance en el ejercicio pptal. Inicio y final)</center></b></td>
            <td WIDTH="17%" ><b><center>Objetivos específicos o de Desempeño (cantidad, calidad, costo, tiempo)*</center></b></td>
        </tr>
			
        <tr class="ContenidoCarta">
            <td ><?php if( $DescProy->getAntecedentes()!=null ) echo( $DescProy->getAntecedentes()); ?><br><br><br><br><br> </td>
            <td ><?php if( $DescProy->getObjetivoGral()!=null) echo( $DescProy->getObjetivoGral()); ?></td>
            <td ><?php if( $DescProy->getLimites()!=null) echo( $DescProy->getLimites()); ?></td>
            <td ><?php if( $Carta->getObjetivoEsp()!=null) echo($Carta->getObjetivoEsp()); ?></td>
        </tr>
			
        <tr class="ContenidoCarta">
            <td rowspan="2" ><b><center>Beneficios Esperados:</center></b></td>
            <td><b><center>Impacto Social</center></b></td>
            <td><b><center>Impacto Organizacional</center></b></td>
            <td><b><center>Impacto Estratégico</center></b></td>
        </tr>

        <tr class="ContenidoCarta">
            <td><?php if($DescProy->getImpactoSocial()!=null) echo($DescProy->getImpactoSocial());?><br/></td>
            <td><?php if($DescProy->getImpactoOrganizacional()!=null) echo($DescProy->getImpactoOrganizacional());?><br/></td>
            <td><?php if($DescProy->getImpactoEstrategico()!=null) echo($DescProy->getImpactoEstrategico());?><br /></td>
        </tr>

        <tr class="ContenidoCarta">
            <td><b>Beneficiarios:</b></td>
            <td colspan="3"><?php if($DescProy->getBeneficiarios()!=null) echo($DescProy->getBeneficiarios());?></td>
        </tr>
			
			
    </table>
		
    <br/><br/>
		
    <center><h4>CRONOGRAMA</h4></center>
    <div id="cronograma">
    <table align="center" BORDER WIDTH="100%" HEIGHT="5%" border="1" cellspacing="0" cellpadding="2">
            <tr >
                <td WIDTH="30%" ></td>
                <?php 
                    $actual = (int)date('Y');
                ?>

                <td colspan="24"><center><?php echo($actual); ?></center></td>
	
            </tr>
			
            <tr class="ContenidoCarta">
                <td ><b>Actividades de Planeación</b></td>
                <td WIDTH="8%"  align="center">Enero</td>
                <td WIDTH="8%"  align="center">Febrero</td>
                <td WIDTH="8%"  align="center">Marzo</td>
                <td WIDTH="8%" colspan="3"  align="center">Abril</td>
                <td WIDTH="8%" colspan="3"  align="center">Mayo</td>
                <td WIDTH="8%" colspan="2"  align="center">Junio</td>
                <td WIDTH="8%" colspan="2"  align="center">Julio</td>
                <td WIDTH="10%" colspan="3"  align="center">Agosto</td>
                <td WIDTH="12%"  align="center">Septiembre</td>
                <td WIDTH="16%" colspan="5" align="center">Octubre</td>
                <td WIDTH="10%"  align="center">Noviembre</td>
                <td WIDTH="10%"  align="center">Diciembre</td>

            </tr>
            
            <tr class='ContenidoCarta'>
                <?php
                    $daoCronograma = new CartaCronogramaDaoJdbc();
                    $listaCronograma = $daoCronograma->Imprimir($folio , (String)( $actual-1 ), (String)( $actual +1 ) );
                    $Cronograma = new CartaCronograma();
                    
                    foreach($listaCronograma as $Cronograma){

                         echo("<td>".$Cronograma->getActividad()."</td>"); 
                         $year = date("Y",strtotime($Cronograma->getFechaInicio())) - 100;
                         $fin = date("Y",strtotime($Cronograma->getFechaFin())) - 100;
	  	         	  	             
	  	     ?>
	  	      
                <td align="center" > <?php if( ( date("n",strtotime($Cronograma->getFechaInicio())) == 0 && $year ==  $actual - 2000  ) || ( date("n",strtotime($Cronograma->getFechaFin())) == 0 && $fin == $actual - 2000 )  ) echo("<b>X</b>");?> </td>
                <td align="center" > <?php if( ( date("n",strtotime($Cronograma->getFechaInicio())) == 1 && $year == $actual - 2000 ) || ( date("n",strtotime($Cronograma->getFechaFin())) == 1 && $fin == $actual - 2000 ) ) echo("<b>X</b>");?> </td>
                <td align="center" > <?php if( ( date("n",strtotime($Cronograma->getFechaInicio())) == 2 && $year == $actual - 2000 ) || ( date("n",strtotime($Cronograma->getFechaFin())) == 2 && $fin == $actual - 2000 ) ) echo("<b>X</b>");?> </td>
                <td align="center" colspan="3"  > <?php if( ( date("n",strtotime($Cronograma->getFechaInicio())) == 3 && $year == $actual - 2000 ) || ( date("n",strtotime($Cronograma->getFechaFin())) == 3 && $fin == $actual - 2000 ) ) echo("<b>X</b>");?></td>
                <td align="center" colspan="3"  > <?php if( ( date("n",strtotime($Cronograma->getFechaInicio())) == 4 && $year == $actual - 2000 ) || ( date("n",strtotime($Cronograma->getFechaFin())) == 4 && $fin == $actual - 2000 ) ) echo("<b>X</b>");?> </td>
                <td align="center" colspan="2"  > <?php if( ( date("n",strtotime($Cronograma->getFechaInicio())) == 5 && $year == $actual - 2000 ) || ( date("n",strtotime($Cronograma->getFechaFin())) == 5 && $fin == $actual - 2000 ) ) echo("<b>X</b>");?></td>
                <td align="center" colspan="2"  > <?php if( ( date("n",strtotime($Cronograma->getFechaInicio())) == 6 && $year == $actual - 2000 ) || ( date("n",strtotime($Cronograma->getFechaFin())) == 6 && $fin == $actual - 2000 ) ) echo("<b>X</b>");?> </td>
                <td align="center" colspan="3"  > <?php if( ( date("n",strtotime($Cronograma->getFechaInicio())) == 7 && $year == $actual - 2000 ) || ( date("n",strtotime($Cronograma->getFechaFin())) == 7 && $fin == $actual - 2000 ) ) echo("<b>X</b>");?></td>
                <td align="center" > <?php if( ( date("n",strtotime($Cronograma->getFechaInicio())) == 8 && $year == $actual - 2000 ) || ( date("n",strtotime($Cronograma->getFechaFin())) == 8 && $fin == $actual - 2000 ) ) echo("<b>X</b>");?></td>
                <td align="center" colspan="5"  > <?php if( ( date("n",strtotime($Cronograma->getFechaInicio())) == 9 && $year == $actual - 2000 ) || ( date("n",strtotime($Cronograma->getFechaFin())) == 9 && $fin == $actual - 2000 ) ) echo("<b>X</b>");?></td>
                <td align="center" > <?php if( ( date("n",strtotime($Cronograma->getFechaInicio())) == 10 && $year == $actual - 2000 ) || ( date("n",strtotime($Cronograma->getFechaFin())) == 10 && $fin == $actual - 2000 ) ) echo("<b>X</b>");?></td>
                <td align="center" > <?php if( ( date("n",strtotime($Cronograma->getFechaInicio())) == 11 && $year == $actual - 2000 ) ||  ( date("n",strtotime($Cronograma->getFechaFin())) == 11 && $fin == $actual - 2000 )  ) echo("<b>X</b>");?></td>
	  	       
            </tr>
            <?php }?>
			
            <tr class="ContenidoCarta" style="font-size: 10pt">
                <td align="center" colspan="25"><b>ENTREGABLES PARCIALES</b></td>

            </tr>
			
            <tr class="ContenidoCarta">
                <td colspan="3" ><b>Descripción del entregable parcial*</b></td>
                <td align="center" colspan="6"><b>Características*</b></td>
                <td align="center" colspan="6"><b>Solicitado por:</b></td>
                <td align="center" colspan="5" ><b>Entregado por:</b></td>
                <td align="center" colspan="5"><b>Validado por:*</b></td>
            </tr>
			
            <?php
                $daoEntregables = new EntregablesDaoJdbc();
                $listaEntregables = $daoEntregables->obtieneListado( $folio );
                $Entregables = new Entregables();

                foreach($listaEntregables as $Entregables){

            ?>
            <tr class="ContenidoCarta" >
                <td colspan="3" ><?php echo( $Entregables->getDescripcion());?></td>
                <td  align="center" WIDTH="15%"  colspan="6"><?php if( $Entregables->getCaracteristicas()!= null ) echo(  $Entregables->getCaracteristicas());?></td>
                <td align="center"  WIDTH="15%"  colspan="6"><b><?php if( $Entregables->getSolicitado()!= null ) echo(  $Entregables->getSolicitado());?></b></td>
                <td align="center"  WIDTH="15%"  colspan="5" ><b><?php if( $Entregables->getEntregado() != null ) echo(  $Entregables->getEntregado());?></b></td>
                <td align="center"  WIDTH="15%"  colspan="5"><b><?php if( $Entregables->getValido()!=  null ) echo(  $Entregables->getValido());?></b></td>
            </tr>
            <?php }?>

            <tr class="ContenidoCarta" style="font-size: 10pt">
                <td align="center" colspan="25"><b>ENTREGABLE FINAL</b></td>

            </tr>
		
            <tr class="ContenidoCarta">
                <td  align="center" colspan="3" ><b>Descripción</b></td>
                <td align="center" colspan="9"><b>Características</b></td>
                <td align="center" colspan="7" ><b>Entregado por:</b></td>
                <td align="center" colspan="6"><b>Validado por:*</b></td>
            </tr> 
			
            <tr class="ContenidoCarta" >
                <td colspan="3" ><b><?php if($Carta->getDescEntregable()!=null)  echo($Carta->getDescEntregable()); ?></b></td>
                <td  colspan="9"><?php if($Carta->getCaracteristicas_Entregable()!=null)  echo($Carta->getCaracteristicas_Entregable()); ?></td>
                <td align="center" colspan="7" ><?php if($Carta->getEntregado()!=null)  echo($Carta->getEntregado()); ?></td>
                <td align="center" colspan="6"><?php if($Carta->getValidado()!=null)  echo($Carta->getValidado()); ?></td>
            </tr>
			
    </table>
			
    <table  align="center" BORDER WIDTH="100%" HEIGHT="5%" border="1" cellspacing="0" cellpadding="2"  >
			
        <tr class="ContenidoCarta" style="font-size: 10pt">
            <td align="center" colspan="9" ><b>PROGRAMA DE RECURSOS</b></td>
        </tr>
			
        <tr class="ContenidoCarta">
            <td  align="center" rowspan="2"  ><b>Recursos Humanos</b></td>
            <td align="center"  rowspan="2" ><b>Ppto. de Nómina Extraordinario</b></td>
            <td align="center"  rowspan="2"  ><b>Honorarios</b></td>
            <td align="center"  rowspan="2" ><b>Factura</b></td>
            <td align="center"  rowspan="2" ><b>Pda. Pptal</b></td>
            <td align="center"  rowspan="2" ><b>Fecha del Ejercicio del <br/> Ppto.</b></td>
            <td align="center"  colspan="3"><b>Comprobación</b></td>
        </tr> 
			
        <tr class="ContenidoCarta">
            <td  align="center" style="font-size:9px" WIDTH="5%" > <b> Docto Fiscal<br> entregado <br> y pagado </b> </td>
            <td  align="center" style="font-size:9px" WIDTH="5%"  > <b>No. De <br> Cheque</b></td>
            <td  align="center" style="font-size:9px" WIDTH="5%"  ><b> Hoja de<br> retención<br> entregada  </b></td>

        </tr> 
			
        <?php
            $daoHumanos = new RecHumanoDaoJdbc();
            $listaHumanos = $daoHumanos->obtieneListado($folio );
            $Humanos = new RecHumanos();
            
            foreach($listaHumanos as $Humanos){

        ?>
        <tr class="ContenidoCarta">
            <td  align="center" WIDTH="34%"><?php echo( $Humanos->getRec_humano() ); ?></td>
            <td align="center"  WIDTH="9%"><?php if( $Humanos->getNomina() != 0 )  echo( "$".number_format( $Humanos->getNomina(),2) ); ?></td>
            <td align="center"  WIDTH="9%"><?php if( $Humanos->getHonorarios()!= 0 ) echo( "$".number_format( $Humanos->getHonorarios(),2)); ?></td>
            <td align="center"  WIDTH="9%"><?php if( $Humanos->getFactura() != null ) echo($Humanos->getFactura()); ?></td>
            <td align="center"  WIDTH="9%"><?php if(  $Humanos->getPartida() != null ) echo($Humanos->getPartida()); ?></td>
            <td align="center"  WIDTH="9%"><?php if( $Humanos->getFechaEjercicio()!= null )echo(date("d-m-Y",strtotime( $Humanos->getFechaEjercicio())) ); ?></td>
            <td align="center"  ><?php if( $Humanos->getFiscal()!= null ) echo(  $Humanos->getFiscal()); ?></td>
            <td align="center"  ><?php if( $Humanos->getCheque()!= null )echo(  $Humanos->getCheque()); ?></td>
            <td align="center"  ><?php if( $Humanos->getRetencion()!= null )echo( $Humanos->getRetencion()); ?></td>
        </tr> 
        <?php }?>
	     
	     
        <tr class="ContenidoCarta">
            <td  align="center"  ><b>Descripción de Recursos Materiales y/o Técnicos</b></td>
            <td align="center"  ><b>Pda. Pptal</b></td>
            <td align="center"  ><b>Precio Unitario</b></td>
            <td align="center"  ><b>CostoTotal</b></td>
            <td align="center"  ><b>Fecha del Ejercicio del <br/> Ppto.</b></td>
            <td align="center"  ><b>Factura Entregada</b></td>
            <td align="center" colspan="2" ><b>Cheque a nombre de: </b></td>
            <td align="center"  ><b>No. de <br/> cheque </b></td>
        </tr>
			
        <?php
            $daoMateriales = new RecMaterialDaoJdbc();
            $listaMateriales = $daoMateriales->obtieneListado( $folio );
            $Materiales = new RecMateriales();
            
            foreach($listaMateriales as $Materiales){
        ?>
        <tr class="ContenidoCarta">
            <td  align="center"  ><?php echo( $Materiales->getRecurso() ); ?></td>
            <td align="center"  ><?php echo( $Materiales->getPartida() ); ?></td>
            <td align="center"  ><?php echo(  "$".number_format( $Materiales->getUnitario(),2)); ?></td>
            <td align="center"  ><?php echo(  "$".number_format( $Materiales->getCosto(),2)); ?></td>
            <td align="center"  ><?php if( $Materiales->getFecha()!= null )echo( date("d-m-Y",strtotime($Materiales->getFecha()))); ?></td>
            <td align="center"  ><?php if( $Materiales->getFacturaEntregada()!=null )  echo( $Materiales->getFacturaEntregada() ); ?></td>
            <td align="center"  colspan="2"><?php if( $Materiales->getNombreCheque()!=null )  echo( $Materiales->getNombreCheque() ); ?></td>
            <td align="center"  ><?php if( $Materiales->getNo_cheque()!=null ) echo( $Materiales->getNo_cheque() ); ?></td>
        </tr>
			
        <?php }?>	  
			
    </table>
    </div>
</body>

</html>