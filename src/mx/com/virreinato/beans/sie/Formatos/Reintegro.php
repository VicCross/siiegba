<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/ReintegroDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Reintegro.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="/css/style_Impresion.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>

        <title> REINTEGRO DE IMPUESTOS </title>
    </head>
    <body>
    <?php
	$dao=new ReintegroDaoJdbc();
	$elemento=$dao->obtieneElemento($_GET['folio']);
	  	
	?>

        <table align="center" width="90%" border="1">
            <caption align="top"> 
                <br />
                REINTEGRO DE IMPUESTOS A LA CUENTA CONCENTRADORA <br><br> 
                REFERENCIA NUMERICA: <?php echo($elemento->getRefNumerica()); ?> <br><br /> <!--Colocar la referencia numerica-->
            </caption>
            <tr>
                <th align="center" width="70%" colspan="5"> DATOS DEL PROVEEDOR</th>
                <th align="center" rowspan="3" width="30%" colspan="2"> DESGLOCE DE IMPUESTOS</th>
            </tr>

            <tr>
                <th align="center" width="12%"> FECHA </th>
                <th align="center"> FOLIO </th>
                <th align="center" width="20%"> PROVEEDOR </th>
                <th align="center" width="20%"> R.F.C </th>
                <th align="center" width="20%"> CURP </th>
            </tr>

            <tr> <!--Colocar los datos del proveedor -->
                <td align="center"> <?php echo( date("d-m-Y",strtotime( $elemento->getFecha())) ); ?> </td>
                <td align="center"> <?php echo$elemento->getFolio(); ?> </td>
                <td align="center"> <?php echo($elemento->getApp().' '.$elemento->getApm().' '.$elemento->getNombre()); ?> </td>
                <td align="center"> <?php echo$elemento->getRfc(); ?> </td>
                <td align="center"> <?php echo$elemento->getCurp(); ?> </td>
            </tr>
        </table>
		
        <?php
            $honorarios=$elemento->getHonorarios();
            $iva=$honorarios*0.16;
            $iva23=$iva*(2.0/3.0);
            $isr= $honorarios*.10;
        ?>

        <font size="2">
        <table width="22.8%" border="1" cellspacing="0" style="position:absolute;right:5.7%;"> <!-- Colocar los impuestos correspondientes -->
            <tr>
                <td width="45%" align="right"> HONORARIOS </td>
                <td width="55%" align="center"> <?php echo "$".number_format($honorarios,2) ?> </td>
            </tr>
            <tr>
                <td align="right"> IVA (16%) </td>
                <td align="center"> <?php echo "$".number_format($iva,2) ?> </td>
            </tr>
            <tr>
                <td align="right"> SUBTOTAL </td>
                <td align="center"> <?php echo"$".number_format(($honorarios+$iva),2) ?> </td>
            </tr>
            <tr>
                <td align="right"> IVA (2/3) </td>
                <td align="center"> <?php echo "$".number_format($iva23,2) ?> </td>
            </tr>
            <tr>
                <td align="right"> ISR </td>
                <td align="center"> <?php echo "$".number_format($isr,2) ?> </td>
            </tr>
            <tr>
                <td align="right"> TOTAL </td>
                <td align="center"> <?php echo "$".number_format(($honorarios+$iva-$iva23-$isr),2) ?> </td>
            </tr>
        </table>
        </font>
    </body>
</html>