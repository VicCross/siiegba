<?php

include_once("../src/mx/com/virreinato/dao/PartidaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Partida.class.php");
include_once("../src/mx/com/virreinato/dao/ComprobaINAHDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ComprobaINAH.class.php");
include_once("../src/mx/com/virreinato/dao/MinistraDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Ministra.class.php");
include_once("../src/mx/com/virreinato/dao/ComprobaINAHDetDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ComprobaINAHDet.class.php");
include_once("../src/mx/com/virreinato/dao/CentroCostosDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CentroCostos.class.php");
include_once("../src/mx/com/virreinato/dao/DirectivoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Directivo.class.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="../css/style_Impresion.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>

        <title> COMPROBACION DE GASTOS </title>
    </head>
    <?php
        $dao=new ComprobaINAHDaoJdbc();
        $elemento=new ComprobaINAH();

        if(isset($_GET['folio']))
        {   $elemento=$dao->obtieneElemento($_GET['folio']);  }

        $dirDao=new DirectivoDaoJdbc();
        $sub=$dirDao->obtieneElementoCargo("2");
        $dir=$dirDao->obtieneElementoCargo("1");
		
    ?>
<body>
    <table width="90%" align="center" >
        <tr>
            <th width="20%"><img src="../img/Conaculta_INAH.png" align="left" width="120%"></th>
            <th width="60%"> COMPROBACIÓN DE GASTOS</th>
            <th width="20%"></th>
        </tr>
    </table>	
    <br>

    <table align="center" width="90%" border="1" cellspacing="0" class="Contenido"> <!-- Museo nacional -->
        <tr>
            <?php
                $ccosto=new CentroCostosDaoJdbc();
                $ccostos=$ccosto->obtieneListado(); 
                $cc = new CentroCostos();

                foreach($ccostos as $cc){
                  $sel="";
                  if($elemento!= null && $elemento->getIdCCosto()!= null ){
                    if($cc->getId()==$elemento->getIdCCosto()){
                        echo(" <td> Dependencia <br> <center> <b> ".$cc->getDescripcion()."</b> </center> </td>");
                        echo(" <td align='center'> Clave <br> <b>".$cc->getClave()."</b> </td>  ");
                    }	
                  }
                }
            ?>
				
            <td align="center" width="20%"> Clave Centro de Trabajo <br> <b> 094MI23700 </b> </td>
        </tr>
    </table>
    
    <br>
		
    <table align="center" width="90%" border="1" cellspacing="0" class="Contenido"> <!-- Gastos de admon. -->
        <tr> 
            <td width="30%"> <input type="checkbox"> Gastos de Administraci&oacute;n</td>
            <td width="20%"> <input type="checkbox"> Inversi&oacute;n</td>
            <td width="30%"> <input type="checkbox"> Terceros</td>
            <td width="20%"> <input type="checkbox"> Otros</td>
        </tr>
        <tr>
            <td colspan="2"> <input type="checkbox"> Comprobaci&oacute;n parcial</td>
            <td colspan="2"> <input type="checkbox"> Comprobaci&oacute;n total</td>
        </tr>
    </table>
		
    <br>

    <table align="center" border="1" cellspacing="0" width="90%" class="Contenido"> <!-- Poliza de egresos -->
        <tr>
            <td align="center" width="30%" valign="top" style="font-size: 8pt"> P&oacute;liza de egresos No. </td>
            <td align="center" width="15%" style="font-size: 8pt" > Fecha de expedici&oacute;n <br> <b> <?php echo(date("d-m-Y",strtotime( $elemento->getFecha() )) ); ?> </b></td>
            <?php
                $ministracion=new MinistraDaoJdbc();
                $ministracions=$ministracion->obtieneListado2(); 
                $c = new Ministra();
                
                $proyecto = "";
                $descMin = "";
                $monto = 0.0;

                foreach($ministracions as $c){
                   if($elemento!= null && $elemento->getIdMinistracion()!= null ){
                          if($c->getId()==$elemento->getIdMinistracion()){
                                $proyecto = $c->getDesProyecto();  
                                $descMin = $c->getDescripcion();
                                $monto = $c->getMonto();
                                echo("<td width='35%' style='font-size: 8pt'> <center> N&uacute;m. de cheque</center> &nbsp, &nbsp; &nbsp;  &nbsp;   <b>".$c->getDescripcion()."</b> </td>");
                          }	
                  }	  
                }
            ?>

            <td align="center" width="20%" style="font-size: 8pt"> Importe del cheque <br> <b> <?php echo("$".number_format($monto,2)); ?> </b></td>
        </tr>

        <tr>
            <td colspan="3"> <label style="font-size: 8pt" > A favor de </label>  <br> <blockquote><blockquote> <b> <?php echo($dir->getNombre()." ".$dir->getApPaterno()." ".$dir->getApMaterno() );?> </b> </blockquote></blockquote></td>
            <td valign="top" >  <label style="font-size: 8pt" > Clave </label>  <br><br> <?php echo($proyecto); ?> </td>
        </tr>
    </table>

    <br>
		
		
    <table border="1" cellspacing="0" align="center" width="90%" class="Contenido"> <!-- Registro -->
        <thead>
            <tr>
                <th colspan="2" align="center" style="font-size: 9pt"> REGISTRO</th>
                <th colspan="3" align="center" style="font-size: 9pt"> OBJETO DEL GASTO</th>
                <th rowspan="2" align="center" style="font-size: 9pt"> NUMERO DE NOTAS</th>
                <th rowspan="2" align="center" style="font-size: 9pt"> DEBE</th>
                <th rowspan="2" align="center" style="font-size: 9pt"> HABER</th>
            </tr>
            <tr>
                <th style="font-size: 7pt"> Cuenta</th>
                <th style="font-size: 7pt"> Sub-Cta</th>
                <th style="font-size: 7pt"> SS-Cta</th>
                <th style="font-size: 7pt"> N&uacute;mero de Partida</th>
                <th style="font-size: 7pt"> Objeto del gasto</th>
            </tr>
        </thead>
        <tbody>

        <?php
            $dao2=new ComprobaINAHDetDaoJdbc();
            if(isset($_GET['folio']))
            { $lista=$dao2->obtieneListado($_GET['folio']); }
            $elemento2=new ComprobaINAHDet();

            $total = 0.0;
            $totalNotas = 0;

            foreach($lista as $elemento2){

                echo("<tr style='font-size: 8pt' >");
                echo("<td></td>");
                echo("<td></td>");
                echo("<td></td>");
                echo("<td align='center' >".$elemento2->getPartida()."</td>");
                echo("<td>".$elemento2->getDesPartida()."</td>");
                echo("<td align='center'>".$elemento2->getNum_notas()."</td>");
                echo("<td align='right'>"."$".number_format($elemento2->getMonto(),2)."</td>");
                echo("<td></td>");
                echo("</tr>");
                $total = $total+$elemento2->getMonto();
                $totalNotas  = $totalNotas+$elemento2->getNum_notas();
            }

            $i = 0;
            for($i=0; $i<3; $i++){
                echo("<tr>");
                echo("<td>&nbsp;</td>");
                echo("<td>&nbsp;</td>");
                echo("<td>&nbsp;</td>");
                echo("<td>&nbsp;</td>");
                echo("<td>&nbsp;</td>");
                echo("<td>&nbsp;</td>");
                echo("<td>&nbsp;</td>");
                echo("<td>&nbsp;</td>");
                echo("</tr>");
            }

        echo("<tr>");
        echo("<td>&nbsp;</td>");
        echo("<td>&nbsp;</td>");
        echo("<td>&nbsp;</td>");
        echo("<td>&nbsp;</td>");
        echo("<td>".$descMin."</td>");
        echo("<td>&nbsp;</td>");
        echo("<td>&nbsp;</td>");
        echo("<td style='font-size: 8pt' align='right' >"."$".number_format($total,2)."</td>");
        echo("</tr>");

    ?>

    <tr>
        <td colspan="5" rowspan="6">
            "Declaro bajo protesta de decir la verdad, que la relaci&oacute;n de los documentos comprobatorios que se 
            detallan, reflejan fielmente los datos e importes de los comprobantes originales, y que por ser 
            soporte de gastos de (Operaci&oacute;n,Proyectos,Terceros o Inversi&oacute;n) correspondiente al mes de
            (___________), han sido cancelados con sello de fecha de pago, quedando bajo la custodia y resguardo de 
            este Centro INAH"
        </td>
        <td> &nbsp;</td>
        <td> &nbsp;</td>
        <td> &nbsp;</td>
    </tr>
    
    <?php
        $a = 1;
        for($a=0;$a<5;$a++)
        {
    ?>
    <tr>
        <td> &nbsp;</td>
        <td> &nbsp;</td>
        <td> &nbsp;</td>
    </tr>
    <?php
        }
    ?>
    </tbody>
       <tr style="font-size: 8pt">
            <td width="7%"></td>
            <td width="7%"></td>
            <td width="7%"></td>
            <td align="right" width="12%"> Sumas</td>
            <td width="35%"></td>
            <td align="right" width="10%"><?php echo($totalNotas); ?></td>
            <td align="right" width="10%"><?php echo("$".number_format($total,2)); ?></td>
            <td align="right" width="10%"><?php echo("$".number_format($total,2)); ?></td>
        </tr>
    </table>
    
    <br>
		
    <table border="1" cellspacing="0" align="center" width="90%"> <!-- Elaboracion -->
        <tr>
            <td align="center" width="20%" valign="top"> <label style="font-size: 8pt" > Fecha de elaboraci&oacute;n </label> <br><br> <?php echo(date("d-m-Y",strtotime($elemento->getFecha()))); ?> </td>
            <td align="center" width="35%" valign="top"> <label style="font-size: 8pt"> Firma del titular de la dependencia </label> <br><br> <?php echo($dir->getNombre()." ".$dir->getApPaterno()." ".$dir->getApMaterno());?> </td>
            <td align="center" width="25%" valign="top" style="font-size: 8pt"> Recibi&oacute;</td>
            <td align="center" width="20%" valign="top" style="font-size: 8pt"> Fecha</td>
        </tr>
    </table>

    <br>

    <table border="1" cellspacing="0" align="center" width="90%"> <!-- Poliza -->
        <tr>
            <th colspan="3"> PARA USO EXCLUSIVO DE LA SUBDIRECCION DE FISCALIZACIÓN</th>
        </tr>
        <tr>
            <td height="60" colspan="2" ><b>PÓLIZA DE COMPROBACIÓN</b> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; N&uacute;mero  </td>
            <td valign="top" width="55%">Fecha </td>
        </tr>
    </table>
</body>
</html>