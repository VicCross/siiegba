<?php

session_start();
include_once("../src/mx/com/virreinato/dao/CatProyectoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatProyecto.class.php");
include_once("../src/mx/com/virreinato/dao/ProveedorDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Proveedor.class.php");
include_once("../src/mx/com/virreinato/dao/EmpleadoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Empleado.class.php");
include_once("../src/mx/com/virreinato/dao/AreaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Area.class.php");
include_once("../src/mx/com/virreinato/dao/PresupuestoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Presupuesto.class.php");
include_once("../src/mx/com/virreinato/dao/DirectivoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Directivo.class.php");
include_once("../src/mx/com/virreinato/dao/CatChequeDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatCheque.class.php");
include_once("../src/mx/com/virreinato/dao/DetPresupuestoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/DetPresupuesto.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="../css/estilo_impresion.css" media="print"  />
<link rel="stylesheet" type="text/css" href="../css/style_Impresion.css"  />
<title>Solicitud Presupuesto</title>
</head>
<?php 
$idSol = null;
    if(isset($_GET['folio']))
    {$idSol= (String)$_GET['folio'];}

   $daoPre = new PresupuestoDaoJdbc();
   $p = new Presupuesto();
   $p = $daoPre->obtieneElementoGb($idSol);
 
   $daoCheque=new CatChequeDaoJdbc();
   $cheque=new CatCheque();	
   $idCheque=$daoCheque->obtieneCheque($idSol);
   $cheque=$daoCheque->obtieneElemento((String)($idCheque));
   
    $daoArea=new AreaDaoJdbc();
    $area=new Area();
	
    if($cheque->getArea()!=null)
    {	$area=$daoArea->obtieneArea((String)($cheque->getArea()));  }
	$dirDao=new DirectivoDaoJdbc();
	$sub=$dirDao->obtieneElementoCargo("2");
	$dir=$dirDao->obtieneElementoCargo("1");
	$subt=$dirDao->obtieneElementoCargo("3");
?>
<body class="Contenido">
    <div id="solicitud">

        <table align="center" width="100%" boder="1" >
            <tr>
                <td align="center"><img src="../img/5.png" /></td>
                <td align="center"><img src="../img/INAH.PNG" width="122" height="40"   /></td>
                <td align="center"><img src="../img/LogotipoCNCA.jpg" width="120" height="40" /></td>
                <td align="center"><img src="../img/EscudoNacional.jpg" width="50" height="50" /></td>
            </tr>
        </table>
        <br> <br>
        <table align="center" width="100%">
            <tr>
                    <td></td>
                    <td align="left">
                            <?php echo( "Fecha: ".date("d-m-Y",strtotime( $p->getFecha())) );?>
                    </td>
            </tr>

            <tr>
                    <td align="left" width="70%"><?php echo($sub->getNombre()." ".$sub->getApPaterno()." ".$sub->getApMaterno()."<BR>".$sub->getCargo() );?>
                            <BR>PRESENTE</td>
                    <td align="left" valign="top">Area: <?php echo( $area->getDescripcion()); ?> </td>
                    <td></td>
            </tr>
        </table>
		
        <br>
        <p align="left">Por este medio le env&iacute;o la solicitud de
                presupuesto, para adquirir:</p>

        <table align="center" width="100%" border="1"  cellspacing="0">
            <tr id="descPresupuesto" >
               <th WIDTH="8"  align="center">Proyecto/&Aacuterea</th>
                <th WIDTH="8" align="center">Programa Institucional</th>
                <th WIDTH="8" align="center">Programa Operativo PAT</th>
                <th WIDTH="6" align="center">Monto</th>
                <th WIDTH="10" align="center">Meta</th>
                <th WIDTH="25" align="center">Partida</th>
                <th WIDTH="5"  align="center">Productos <br/> a obtener:</th>
                <th WIDTH="20" align="center">Detalle de los <br/> materiales o <br/> servicios:</th>
            </tr>
            <?php	  	  	 		
	 		
                $daoDet=new DetPresupuestoDaoJdbc();

               $listaDet =null;

               if($p!= null && $p->getDestino()!=null && $p->getDestino() == "PR"){
                   $listaDet =$daoDet->obtieneListadoProgramas($idSol);
                }
                else{
                     $listaDet =$daoDet->obtieneListadoProgramasGb($idSol);

                }

                $Det = new DetPresupuesto();
                foreach($listaDet as $Det){

            ?>
                <tr>
                  <td WIDTH="8" align="center">
            <?php
                if($p!= null && $p->getDestino()!=null && $p->getDestino() == "PR"){
                    echo( $Det->getProyecto());
                }
                else{
                    echo($area->getDescripcion());

                }
                ?>
                </td>
                <td WIDTH="8" align="center"><?php echo( $Det->getProgramaInstitucional()); ?></td>
                <td WIDTH="8" align="center"><?php echo( $Det->getProgramaOperativo()); ?></td>
                <td WIDTH="6" align="center"><?php echo( "$".number_format( $Det->getMonto(),2));?></td>
                <td WIDTH="10" align="center"><?php echo $Det->getMeta(); ?></td>
                <td WIDTH="25" align="center"><?php echo( $Det->getPartida() );?></td>
                <td WIDTH="5" align="center"><?php echo( $Det->getProductos_obtenidos() );?></td>
                <td WIDTH="20" align="center"><?php echo( $Det->getDetalle_productos() );?></td>
              </tr>
			
            <?php }?>

            </table>
             <br/><br/>
            <br>Elaborar cheque a nombre de:
            <u> &nbsp; &nbsp; &nbsp; 

            <?php
                if( $p!= null && $p->getIdProveedor() != null && !(String)($p->getIdProveedor()) == ("0")){

                    $daoProv=new ProveedorDaoJdbc();
                    $elementoProv = $daoProv->obtieneElemento((String)($p->getIdProveedor()));
                    echo( $elementoProv->getProveedor() );

                }else if( $p!= null && $p->getIdEmpleado()!= null &&  !(String)($p->getIdEmpleado()) == ("0")){
                    $daoEm=new EmpleadoDaoJdbc();
                    $elementoEm = $daoEm->obtieneElemento((String)($p->getIdEmpleado()));
                    echo( $elementoEm->getNombre()." ".$elementoEm->getApPaterno(). " ".$elementoEm->getApMaterno() );
                }else if($p!= null && $p->getOtro() != null ){
                    echo($p->getOtro());
            }
          ?> &nbsp; &nbsp; &nbsp;
		</u>
		<br> <br>
		<table align="center" width="100%">
                    <tr>
                        <th align="center">ATENTAMENTE</th>
                        <th align="center">VISTO BUENO</th>
                        <th align="center">VISTO BUENO</th>
                        <th align="center">VISTO BUENO</th>
                    </tr>
                    <tr>
                        <th align="center"><br> <br><br>_____________________________________<br>
                        &nbsp;<?php echo( strtoupper($p->getElabora()) ); ?> &nbsp;</th>
                        <th align="center"><br> <br> <br>&nbsp;&nbsp;&nbsp;_____________________________________<br><?php echo($subt->getNombre()." ".$subt->getApPaterno()." ".$subt->getApMaterno() );?></th>
                        <th align="center"><br> <br> <br>&nbsp;&nbsp;&nbsp;___________________________________<br><?php echo($sub->getNombre()." ".$sub->getApPaterno()." ".$sub->getApMaterno() );?></th>
                        <th align="center"><br> <br> <br>&nbsp;&nbsp;&nbsp;___________________________________<br><?php echo($dir->getNombre()." ".$dir->getApPaterno()." ".$dir->getApMaterno() );?></th>
                    </tr>
		</table>
		
		<br> <br>
	</div>
</body>
</html>