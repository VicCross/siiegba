<?php

session_start();
include_once("../src/mx/com/virreinato/dao/CentroCostosDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CentroCostos.class.php");
include_once("../src/mx/com/virreinato/dao/CatProyectoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatProyecto.class.php");
include_once("../src/mx/com/virreinato/dao/PartidaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Partida.class.php");
include_once("../src/mx/com/virreinato/dao/FondosDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Fondos.class.php");
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/FondosDetDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/FondosDet.class.php");
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(4);
?>

<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="/css/style_Impresion.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
        <script>
		  // Función modulo, regresa el residuo de una división 
   function mod(dividendo , divisor) 
   { 
  			resDiv = dividendo / divisor ;  
  			parteEnt = Math.floor(resDiv);            // Obtiene la parte Entera de resDiv 
  			parteFrac = resDiv - parteEnt ;      // Obtiene la parte Fraccionaria de la división
  			modulo = Math.round(parteFrac * divisor);  // Regresa la parte fraccionaria * la división (modulo) 
  			return modulo; 
	} // Fin de función mod


	// Función ObtenerParteEntDiv, regresa la parte entera de una división
	function ObtenerParteEntDiv(dividendo , divisor) 
	{ 
  			resDiv = dividendo / divisor ;  
  			parteEntDiv = Math.floor(resDiv); 
  			return parteEntDiv; 
	} // Fin de función ObtenerParteEntDiv

	
	// function fraction_part, regresa la parte Fraccionaria de una cantidad
	function fraction_part(dividendo , divisor) 
	{ 
  			resDiv = dividendo / divisor ;  
  			f_part = Math.floor(resDiv); 
  			return f_part; 
	} // Fin de función fraction_part


	// function string_literal convierte el monto en letra 
	function string_literal_conversion(number) 
	{   
   		centenas = ObtenerParteEntDiv(number, 100); 
   		number = mod(number, 100); 

     		decenas = ObtenerParteEntDiv(number, 10); 
   		number = mod(number, 10); 

   		unidades = ObtenerParteEntDiv(number, 1); 
   		number = mod(number, 1);  
   		string_hundreds="";
   		string_tens="";
   		string_units="";
   
		   if(centenas == 1) string_hundreds = "ciento ";
   		if(centenas == 2) string_hundreds = "doscientos ";
   		if(centenas == 3) string_hundreds = "trescientos ";
   		if(centenas == 4) string_hundreds = "cuatrocientos ";
   		if(centenas == 5) string_hundreds = "quinientos ";
   		if(centenas == 6)	string_hundreds = "seiscientos ";
   		if(centenas == 7) string_hundreds = "setecientos ";
   		if(centenas == 8)	string_hundreds = "ochocientos ";
   		if(centenas == 9) string_hundreds = "novecientos ";

		   if(decenas == 1){
      		//Special case, depends on units for each conversion
     			if(unidades == 1) string_tens = "once";
      		if(unidades == 2) string_tens = "doce";
      		if(unidades == 3) string_tens = "trece";
      		if(unidades == 4) string_tens = "catorce";
      		if(unidades == 5) string_tens = "quince";
      		if(unidades == 6) string_tens = "dieciseis";
      		if(unidades == 7) string_tens = "diecisiete";
      		if(unidades == 8) string_tens = "dieciocho";
      		if(unidades == 9) string_tens = "diecinueve";
      
   		} 
   		
   		if(decenas == 2) string_tens = "veinti";
			if(decenas == 3) string_tens = "treinta";
   		if(decenas == 4) string_tens = "cuarenta";
   		if(decenas == 5) string_tens = "cincuenta";
   		if(decenas == 6) string_tens = "sesenta";
   		if(decenas == 7) string_tens = "setenta";
   		if(decenas == 8) string_tens = "ochenta";
   		if(decenas == 9) string_tens = "noventa";
   
   
	   if (decenas == 1) string_units="";   
   	else { 
      	if(unidades == 1) string_units = "un";
      	if(unidades == 2) string_units = "dos";
      	if(unidades == 3) string_units = "tres";
      	if(unidades == 4) string_units = "cuatro";
      	if(unidades == 5) string_units = "cinco";
      	if(unidades == 6) string_units = "seis";
      	if(unidades == 7) string_units = "siete";
      	if(unidades == 8) string_units = "ocho";
      	if(unidades == 9) string_units = "nueve";
  
  		  } // end if-then-else 
   	
   	  if (centenas == 1 && decenas == 0 && unidades == 0) string_hundreds = "cien " ; 
		  if (decenas == 1 && unidades ==0) string_tens = "diez " ; 
		  if (decenas == 2 && unidades ==0) string_tens = "veinte " ; 
		  if (decenas >=3 && unidades >=1)  string_tens = string_tens+" y "; 
	
		  final_string = string_hundreds+string_tens+string_units;
		  return final_string ; 

	} //end of function string_literal_conversion()================================ 


	function covertirNumLetras(number)
	{
  
   	cent = number.split(".");   
   	centavos = cent[1];
   	number = cent[0];
   
   
   	if (centavos == 0 || centavos == undefined){
   	centavos = "00";}

   	if (number == 0 || number == "") 
   	{ // if amount = 0, then forget all about conversions, 
      	centenas_final_string=" cero "; // amount is zero (cero). handle it externally, to 
      // function breakdown 
  		} 
   	else 
   	{ 
   
     		millions  = ObtenerParteEntDiv(number, 1000000); // first, send the millions to the string 
      	number = mod(number, 1000000);           // conversion function 
      
     		if (millions != 0)
      	{
      	                      
      		// This condition handles the plural case 
         	if (millions == 1) descriptor= " millon ";  // > than 1, use 'millones' (millions) as 
         	else descriptor = " millones "; 
            
      	} 
      	else  descriptor = " ";                 // if 0 million then use no descriptor. 
      
      
	      millions_final_string = string_literal_conversion(millions)+descriptor; 
      	thousands = ObtenerParteEntDiv(number, 1000);  // now, send the thousands to the string 
        	number = mod(number, 1000);            // conversion function. 
      
      
     		if (thousands != 1) 
      	{                   // This condition eliminates the descriptor 
         		thousands_final_string =string_literal_conversion(thousands) + " mil "; 
       		//  descriptor = " mil ";          // if there are no thousands on the amount 
      	} 
      	if (thousands == 1)
      	{
         	thousands_final_string = " mil "; 
     		}
      	if (thousands < 1) 
      	{ 
         	thousands_final_string = " "; 
      	} 
  
      	// this will handle numbers between 1 and 999 which 
      	// need no descriptor whatsoever. 

     		centenas  = number;                     
      	centenas_final_string = string_literal_conversion(centenas) ; 
      
   	} //end if (number ==0) 

   	/*if (ereg("un",centenas_final_string))
   	{
     		centenas_final_string = ereg_replace("","o",centenas_final_string); 
   	}*/
   	//finally, print the output. 

   	/* Concatena los millones, miles y cientos*/
   	cad = millions_final_string+thousands_final_string+centenas_final_string; 
   
   	/* Convierte la cadena a Mayúsculas*/
   	cad = cad.toUpperCase();       

   	if (centavos.length>2)
  		 {   
      		if(centavos.substring(2,3)>= 5){
         		centavos = centavos.substring(0,1)+(parseInt(centavos.substring(1,2))+1).toString();
      		}   else{
        			centavos = centavos.substring(0,2);
       		}
   	}

   		/* Concatena a los centavos la cadena "/100" */
     		if (centavos.length == 1)
   		{
      	 	centavos = centavos+"0";
      		
   		}
   		centavos = centavos+ "/100"; 


   		/* Asigna el tipo de moneda, para 1 = PESO, para distinto de 1 = PESOS*/
   		if (number == 1) moneda = " PESO ";  
   		else moneda = " PESOS ";  
   
   
   		/* Regresa el número en cadena entre paréntesis y con tipo de moneda y la fase M.N.*/
   		$("#TotalLetra").text("");
   		$("#TotalLetra").text("( "+cad+moneda+centavos+" M.N. )");
		}

		  $().ready(function(){
		  	  var total = $("#TotalSinFormato").val();
		  	  covertirNumLetras(total);
		  	});
		</script>
		
    <title> SOLICITUD DE FONDOS </title>
    </head>
    <?php
        $folio = null;
    
        if(isset($_GET['folio'])){ $folio = (String)$_GET['folio']; }
        $dao=new FondosDaoJdbc();
        $fondo = new Fondos();
        $fondo = $dao->obtieneElemento($folio);

    ?>
    <body>
        <font size=2>		
            <table width="90%" border="1" align="center" cellspacing="0" style="font-size: 7pt">
                <caption align="top">
                <label style="font-size: 9pt" ><br> INSTITUTO NACIONAL DE ANTROPOLOGIA E HISTORIA <br></label>
                <label style="font-size: 9pt"><br> SOLICITUD DE FONDOS <br><br></label>

                <tr>
                    <th style="font-size: 7pt" width="10%"> N&UacuteMERO SOLICITUD</th>
                    <th style="font-size: 7pt" width="15%"> FECHA </th>
                    <th style="font-size: 7pt" width="55%"> NOMBRE DEL CENTRO DE TRABAJO </th>
                    <th style="font-size: 7pt" width="20%"> CLAVE DEL CENTRO </th>
                </tr> 
                <tr>
                    <td align="center"><?php if($fondo->getNumSol()!= null) echo($fondo->getNumSol()); ?> </td>
                    <td align="center"><?php if($fondo->getFecha()!= null) echo(date("d-m-Y",strtotime($fondo->getFecha())) ); ?> </td>
                    <?php
                        $centro=new CentroCostosDaoJdbc();
                        $centros=$centro->obtieneListado(); 
                        $c = new CentroCostos();

                        foreach($centros as $c){
                            if($fondo!= null && $fondo->getIdCCosto()!= null ){
                                if($c->getId()==$fondo->getIdCCosto()){
                                    echo(" <td align='center'>".$c->getDescripcion()."</td> ");
                                    echo(" <td align='center'>".$c->getId()."</td>");  
                                }	
                            }
                        }
                    ?> 
                </tr>
					
                <!-- /caption --> 
												
            </table>
            <br>
            <table width="90%" border="2" align="center" cellspacing="0" rules="cols">	
                <tr align="left">	
                    <th ></th>
                    <th width="15%" bgcolor="silver" align="center"> I.D </th>
                    <th width="60%" align="center"> NOMBRE DEL PROYECTO </th>
                </tr>	
                <tr>
                    <td align="center">
                        OPERACION &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (<?php if($fondo!= null && $fondo->getTipo() != null && $fondo->getTipo() == 1) echo(" X "); else echo("&nbsp;&nbsp;&nbsp;");  ?>) <br> 
                        INVERSION &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (<?php if($fondo!= null && $fondo->getTipo() != null && $fondo->getTipo() == 2) echo(" X "); else echo("&nbsp;&nbsp;&nbsp;"); ?>) <br> 
                        PROYECTOS &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ( <?php if($fondo!= null && $fondo->getTipo() != null && $fondo->getTipo() == 3) echo(" X "); else echo("&nbsp;&nbsp;&nbsp;");  ?> ) <br> 
                        TERCEROS &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (<?php if($fondo!= null && $fondo->getTipo() != null && $fondo->getTipo() == 4) echo(" X ");  else echo("&nbsp;&nbsp;&nbsp;&nbsp;");  ?> )
                    </td>
                    <?php
                        $catProyecto=new CatProyectoDaoJdbc();
                        $catProyectos=$catProyecto->obtieneListado2(); 
                        $cp = new CatProyecto();

                        foreach($catProyectos as $cp){
                            if($fondo!= null && $fondo->getIdProyecto()!= null ){
                                if($cp->getId()==$fondo->getIdProyecto()){ 
                                    echo(" <td bgcolor='silver' align='center'>".$cp->getId()."</td>");
                                    echo(" <td align='center' >".$cp->getDescripcion()."</td> ");				        	  			
                                 }	
                            }
                        }
                    ?> 
                </tr>			
            </table>
            <br>
            <table width="90%" border="1" align="center" cellspacing="0">
                <tr>
                    <td>
                        A FAVOR DE: <br>
                        <center><b><?php echo $parametro->getValor(); ?></b><br/><br/></center> 
                    </td>
                </tr>
            </table>
            <br>
            <table width="90%" border="2" align="center" cellspacing="0" rules="groups">
                <caption>
                    <br> APLICACION CONTABLE Y PRESUPUESTAL <br><br>
                </caption>
				
                <colgroup width="">	
                <colgroup width="">
                <colgroup width="">
                <colgroup width="">
                <colgroup width="">		

                <THEAD>
                    <tr>
                        <th align="center" style="font-size: 6.5pt" width="10%"> CTA. CONTABLE </th>
                        <th align="center" style="font-size: 6.5pt" width="15%"> DEBE </th>
                        <th align="center" style="font-size: 6.5pt" width="15%"> HABER </th>
                        <th style="font-size: 6.5pt" align="center" width="45%"> PARTIDA PRESUPUESTAL </th>
                        <th style="font-size: 6.5pt" align="center" width="15%"> IMPORTE PARCIAL </th>
                    </tr>
                </THEAD>
                <TBODY>
                <?php 
                    $daoDet=new FondosDetDaoJdbc();
                    $listaDet=$daoDet->obtieneListado($folio,$fondo->getMesCalendario());
                    $fondoDet=new FondosDet();

                    $total = 0.0; 

                    foreach($listaDet as $fondoDet){
                        $total = $fondoDet->getMonto() + $total;
                ?>
                <tr>
                    <td height="40"></td>
                    <td></td>
                    <td></td>
                    <td><?php echo($fondoDet->getDesPartida());?> </td>
                    <td>$ &nbsp; &nbsp; &nbsp; &nbsp; <?php echo(number_format($fondoDet->getMonto(),2));?></td>
                </tr>
                <?php
                    }
                ?>
                </TBODY>
				
                <tfoot>
                    <tr>
                       <td colspan="7">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        TOTAL SOLICITADO &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        <input type="hidden" id="TotalSinFormato" name="TotalSinFormato" value=" <?php echo( $total ); ?> "/>
                        <b>$ <label id="TotalSolicitado"><?php echo(number_format( $total,2) ); ?></label></b> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                        <label id="TotalLetra" ></label>
                        </td>
                    </tr>
                <tfoot>
            </table>
            <br>
            <table width="90%" border="1" align="center" cellspacing="0">
                <tr>			
                    <td><?php if( $fondo->getDescripcion()!=  null ) echo($fondo->getDescripcion()); ?></td>
                </tr>	
            </table>
            <br>
            <table width="90%" border="1" align="center" cellspacing="0">
                <tr>
                    <th style="font-size: 6.5pt" width="35%"> NOMBRE Y FIRMA DEL TITULAR DEL AREA </th>
                    <th style="font-size: 6.5pt" width="15%"> PRESUPUESTO </th>
                    <th style="font-size: 6.5pt" width="30%"> FISCALIZACION </th>
                    <th style="font-size: 6.5pt" width="20%"> CONTABILIDAD </th>
                </tr> 
                <tr>
                    <td height="100" valign="bottom"> <center> <b><?php if( $fondo->getTitular()!= null ) echo($fondo->getTitular()); ?> </b></center> </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>	
            </table>
            <br>
            <table border="0" align="center" width="50%">		
                <tr>
                    <td width="50%" align="center">
                        <img src="../img/INAH.PNG" width="45%" height="15%" alt="" style="border:0;" />
                    </td>
                    <td width="50%" align="center">
                        <img src="../img/LogotipoCNCA.jpg" width="45%" height="7%" alt="" style="border:0;" />
                    </td>
                </tr>
            </table>
        </font>
    </body>
</html>


