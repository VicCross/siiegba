<?php

include_once("../src/mx/com/virreinato/dao/ProveedorDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Proveedor.class.php");
include_once("../src/mx/com/virreinato/dao/CatChequeDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatCheque.class.php");
include_once("../src/mx/com/virreinato/dao/AreaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Area.class.php");
include_once("../src/mx/com/virreinato/dao/EmpleadoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Empleado.class.php");
include_once("../src/mx/com/virreinato/dao/PresupuestoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Presupuesto.class.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<link href="../css/style_Impresion.css" type="text/css" rel="stylesheet">
<title>Impresión de Cheques</title>
</head>
<?php
   if(isset($_GET['folio'])){$id = (String)$_GET["folio"];}
  $daoCheque=new CatChequeDaoJdbc();
  $cheque=new CatCheque();	
	
   if( $daoCheque->ImprimirElemento($id) == 0 ){
	   echo(" <div align='center' class='msj' style='font-size: 15pt'>El cheque no tiene un VoBo</div> "); 
	}else{
	
	$cheque = $daoCheque->obtieneElemento($id);
	//verifico si el estatus es Registrado para cambiarlo a Emitido
	if($cheque->getEstatus() == "RE"){
		$cheque->setEstatus("EM");
		$daoCheque->actualizaElemento($cheque);
		
	}
	
	$daoPre = new PresupuestoDaoJdbc();
        $p = new Presupuesto();
        $p = $daoPre->obtieneElementoGb((String)($cheque->getIdSolicitud()));
   
   $aux = 0;
   
   $meses = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
?>
<body >
    <div id="solicitud" style='paddinq:0px; margin:0px'>

        <table align="center" width="100%" style='font-family:arial;font-size:8pt;text-transform:uppercase'>
                <tr>

         <td align="right" id="fechaEmision" colspan='2' style='padding-right:45px;padding-top:5pt' > <?php echo( date("d",strtotime($cheque->getFechaEmision()))."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$meses[date("n",strtotime($cheque->getFechaEmision()))]."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".date("Y",strtotime($cheque->getFechaEmision())) ); ?></td>			
            </tr>

            <tr >
                <td width="70%" align='left' style='font-family:arial;font-size:9pt;text-transform:uppercase;padding-top:0pt'>
                <br>
                <br>
                <br>
        <?php
                if(  $p!= null && $p->getIdProveedor() != null && !(String)($p->getIdProveedor()) == "0"){
                   $aux = 1;
                    $daoProv=new ProveedorDaoJdbc();
                    $elementoProv = $daoProv->obtieneElemento((String)($p->getIdProveedor()));
                                echo( $elementoProv->getProveedor() );
               }

               else if( $p!= null && $p->getIdEmpleado()!= null && !(String)($p->getIdEmpleado()) == "0"){
                   $aux = 1;
                    $daoEm=new EmpleadoDaoJdbc();
                    $elementoEm = $daoEm->obtieneElemento((String)($p->getIdEmpleado()));
                    echo( $elementoEm->getNombre()." ".$elementoEm->getApPaterno(). " ".$elementoEm->getApMaterno() ); 

               }
               else if( $p!= null && $p->getOtro() != null &&  $aux == 0){
                    echo($p->getOtro());
               }
            ?>
            </td>
                <td  width="30%" align="right" style='font-family:arial;font-size:9pt;text-transform:uppercase;padding-top:0pt' >				   	<br>
                     <br>
                     <br><?php echo(number_format($cheque->getMonto(),2)); ?></td>
             </tr>
             <tr>
                <td style='font-family:arial;font-size:9pt;text-transform:uppercase;padding-top:2ex' width="20%" >( &nbsp; <?php echo( $cheque->getMontoLetra() );?> &nbsp; ) </td>
             </tr>
        </table>
	</div>
</body>
</html>
<?php }?>