<?php

session_start();
include_once("../src/mx/com/virreinato/dao/ComisionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Comision.class.php");
include_once("../src/mx/com/virreinato/dao/CentroCostosDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CentroCostos.class.php");
include_once("../src/mx/com/virreinato/dao/CatChequeDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatCheque.class.php");
include_once("../src/mx/com/virreinato/dao/EmpleadoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Empleado.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
header('Content-Type: text/html; charset=UTF-8');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link href="../css/calendario.css" type="text/css" rel="stylesheet">
<script src="../js/calendar.js" type="text/javascript"></script>
<script src="../js/calendar-es.js" type="text/javascript"></script>
<script src="../js/calendar-setup.js" type="text/javascript"></script>
<script src="../js/gen_validatorv4.js" ></script>
<script src="../js/jquery-1.7.2.js"></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>

<title>Consultar Comisión</title>
</head>
<body>
<div class="contenido">
<br/>
<p class="titulo_cat1">Formatos > <a class="linkTitulo_cat1" href="lista_Comision.php" > Comisi&oacute;n </a></p>
<h2 class="titulo_cat2">
    <?php
	$id = "";
        $folio = "";
        if(isset($_REQUEST["respuesta"]))
        {$respuesta= (String) $_REQUEST["respuesta"];}
	
	if (isset($_REQUEST["id"]) && $_REQUEST["id"]!= null)
        {	$id = (String)$_REQUEST["id"]; }
	else
        {	$id=null; }
		
	$dao= new ComisionDaoJdbc();
	$elemento = new Comision();

	if($id!= null){
		echo("Modificar Comisión");
		$elemento = $dao->obtieneElemento($id);
		
	}	
	else{
		echo("Alta de Comisión");
	}	
	
    ?>
</h2>
<?php $respuesta = NULL;
    if(isset($_GET['respuesta'])){ $respuesta = $_GET['respuesta']; }?> 
  <form id="frmComprobacion" id="frmComprobacion" method="POST" action="../src/mx/com/virreinato/web/WebComission.php">
  <table width="90%" border="0" cellspacing="0" cellpadding="5" class='tb_add_cat' align='center'>
    <tr>
        <td>U. Administrativa o Adscripción*:</td>
        <td>
            <select name="ccosto" id="ccosto" SIZE=1 >
                <?php
                 $dao2=new CentroCostosDaoJdbc();
                 $lista2=$dao2->obtieneListado();
                 $c = new CentroCostos();

                foreach($lista2 as $c) {
                  $sel="";
                  if($elemento!= null && $elemento->getIdCCosto()!= null ){
                          if($c->getId() == $elemento->getIdCCosto()){
                                $sel="selected='selected'";
                          }	
                  }	  
                  echo("<option value='".$c->getId()."' ".$sel." >".$c->getDescripcion()."</option>");
                }
                ?> 
            </select>
        </td>
        <td>Fecha del Oficio de Comisión*:</td>
            <td>
                <input onblur="validar('Fecha','fecha')" type="text" maxlength="20" size="20" name="Fecha" id="Fecha" value="<?php if($elemento!=null && $elemento->getFechaOficio()!= null) echo(date("d-m-Y",strtotime($elemento->getFechaOficio())));?>" >
            </td>
 			
            <td>No. del Oficio de Comisión*:</td>
            <td>
                <input type="text"  size="20" name="numeroOficio" id="numeroOficio" value="<?php if($elemento!=null && $elemento->getNumeroOficio()!= null) echo($elemento->getNumeroOficio());?>">			
            </td>

                </tr>

           <tr>
           <td>Nombre *:</td>
           <td>
                <select name="empleado" id="empleado">
                    <option value="0"> Selecciona  </option>
                    <?php
                        $daoEm=new EmpleadoDaoJdbc();
                        $listaEm = $daoEm->obtieneListado();
                        $elementoEm=new Empleado();
                        foreach($listaEm as $elementoEm){
                            $sel = "";
                            if($elemento!= null && $elemento->getEmpleado()!= null ){
                                   $empleado = $elemento->getEmpleado();
                                    if( $empleado->getId() == $elementoEm->getId()){ $sel = "selected='selected'"; }
                                }
                                    echo("<option value=".$elementoEm->getId()." ".$sel." >" .$elementoEm->getNombre()." ".$elementoEm->getApPaterno()." ".$elementoEm->getApMaterno()."</option>");
                            }
                        ?>
                </select>
            </td>

            <td>Fecha de inicio de la comisión*:</td>
            <td>
                <input onblur="validar('FechaInicio','fecha')" type="text" maxlength="20" size="20" name="FechaInicio" id="FechaInicio" value="<?php if($elemento!=null && $elemento->getPeriodoInicial()!= null) echo(date("d-m-Y",strtotime($elemento->getPeriodoInicial()))); ?>" >
            </td>
 			
            <td>Fecha final de la comisión*:</td>
            <td>
                <input onblur="validar('FechaFinal','fecha');" type="text" maxlength="20" size="20" name="FechaFinal" id="FechaFinal" value="<?php if($elemento!=null && $elemento->getPeriodoFinal()!= null) echo(date("d-m-Y",strtotime($elemento->getPeriodoFinal())));?>" >
            </td>
            </tr>
		
            <tr>
  	         
                <td>Objetivo:</td> 
                    <td colspan='3'>
                        <textarea name="objetivo" id="objetivo" rows="4"  maxlength="255" cols="50" /><?php if($elemento!=null && $elemento->getObjetivo()!= null) echo($elemento->getObjetivo());?></textarea>
                    </td>
			 
                    <td>Destino*:</td>
                    <td colspan='5'>	
                           <select name="destino" id="destino" SIZE=1>
                            <option value='0' >Selecciona</option>
                            <option value='1' <?php if($elemento!=null && $elemento->getNacional()!= null && $elemento->getNacional() == 1 ) echo("selected='selected'");?>>Nacional</option>
                            <option value='2' <?php if($elemento!=null && $elemento->getNacional()!= null && $elemento->getNacional() == 2 ) echo("selected='selected'");?>>Internacional</option>
                           </select>
                   </td>	
				 
			</tr>
		
			<tr>
			 <td>Resultados:</td> 
			 <td colspan='3'>
			 	<textarea name="resultados" id="resultados" rows="4"  maxlength="255" cols="50" /><?php if($elemento!=null && $elemento->getResultados()!= null) echo($elemento->getResultados());?></textarea>
			 </td>
			 <td>Lugar de la comisión(Zona)*:</td>
			 <td>
			 	<input type="text" size="20" name="lugar" id="lugar" value="<?php if($elemento!=null && $elemento->getLugar()!= null) echo($elemento->getLugar());?>">			
			 </td>
				 
			</tr>
		
			
			<tr>
			
			<td>Tarifa diaria de la comisión*:</td>
			 <td>
			 	<input type="text" size="20" name="tarifaDiaria" id="tarifaDiaria"  value="<?php if($elemento!=null && $elemento->getTarifaDiaria()!= null) echo($elemento->getTarifaDiaria());?>">			
			 </td>
			
			<td>Número de días de la comisión*:</td>
			 <td>
			 	<input type="text" size="20" name="numDias" onblur="totalViaticos();" id="numDias" value="<?php if($elemento!=null && $elemento->getNumeroDias()!= null) echo($elemento->getNumeroDias());?>">			
			 </td>
			 <td>Total de Viáticos*:</td>
			<td>
			 	<input type="text" size="20" name="montoTotal" id="montoTotal" onfocus='totalViaticos();' value="<?php if($elemento!=null && $elemento->getMontoTotal()!= null) echo($elemento->getMontoTotal());?>">			
			</td>
			
 			</tr>
		
			<tr>			
			
			<td>Monto con documentos:</td>
			 <td>
			 	<input type="text" size="20" name="montoDocumentos" id="montoDocumentos" onblur='saldoPend();' value="<?php if($elemento!=null && $elemento->getMontoDoctos()!= null && $elemento->getMontoDoctos()!= 0.0) echo($elemento->getMontoDoctos());?>">			
			 </td>
			
			<td>Monto sin documentos:</td>
			 <td>
			 	<input type="text" size="20" name="montoSinDocumentos" id="montoSinDocumentos" onblur='saldoPend();' value="<?php if($elemento!=null && $elemento->getMontoSinDoctos()!= null  && $elemento->getMontoSinDoctos()!= 0.0 ) echo($elemento->getMontoSinDoctos());?>">			
			 </td>
				 <td>Saldo pendiente:</td>
			 <td>
			 	<input type="text" size="20" name="saldoPendiente" id="saldoPendiente" onfocus='saldoPend();' value="<?php if($elemento!=null && $elemento->getSaldoPendiente()!= null ) echo($elemento->getSaldoPendiente());?>">			
			 </td>
		</tr>
		
		
		<tr>			
			
			<td colspan='6'>Monto Sin documentos con letra: &nbsp;&nbsp;
			 	<input type="text" size="100" name="montoSinDocumentosLetra" id="montoSinDocumentosLetra"  value="<?php if($elemento!=null && $elemento->getLetraSinDoctos()!= null) echo($elemento->getLetraSinDoctos());?>">			
			 </td>
		</tr>		
		
		
		<tr>
   	  <?php  if($id == null){?>
	       <td align="center" colspan="6">
		      <input name="guardar" style="cursor:pointer" type="submit" value="Guardar"  class='btn' />
		       &nbsp; &nbsp; &nbsp;
	       	  &nbsp;&nbsp;&nbsp;<a href='lista_Comision.php' class='liga_btn'> Cancelar </a>
          </td>
       <?php  } ?>
     
        <?php  if($id != null){?>
	       <td align="center" colspan="6">
                    <input name="guardar" style="cursor:pointer" type="button" onClick = "Validar()" value="Guardar"  class='btn' />
                    &nbsp; &nbsp; &nbsp;
                    &nbsp;&nbsp;&nbsp;<a href='lista_Comision.php' class='liga_btn'> Cancelar </a>
          </td>
      <?php  } ?>
	</tr>
        </table>
        <?php if($elemento!=null && $elemento->getId()!=null) echo("<input type='hidden' name='id' value='".$elemento->getId()."' />");?>
        </form>
	</div> 
    <br><br>
</body>
<script>
    Calendar.setup({ inputField : "Fecha", ifFormat : "%d-%m-%Y", button: "Fecha" });
    Calendar.setup({ inputField : "FechaInicio", ifFormat : "%d-%m-%Y", button: "FechaInicio" });
    Calendar.setup({ inputField : "FechaFinal", ifFormat : "%d-%m-%Y", button: "FechaFinal" });
    
    
    
    var frmvalidator  = new Validator("frmComprobacion");
   

	frmvalidator.addValidation("Fecha","req","Por favor capture la fecha del oficio de comisión.");
	frmvalidator.addValidation("numeroOficio","req","Por favor capture el número de oficio de la comisión.");
	frmvalidator.addValidation("empleado","dontselect=0","Por favor seleccione el nombre del empleado.");
	frmvalidator.addValidation("FechaInicio","req","Por favor capture la fecha de inicio de la comisión.");
	frmvalidator.addValidation("FechaFinal","req","Por favor capture la fecha final de la comisión.");
	//frmvalidator.addValidation("objetivo","req","Por favor capture el objetivo de la comisión.");
	frmvalidator.addValidation("destino","dontselect=0","Por favor seleccione el destino de la comisión.");
	//frmvalidator.addValidation("resultados","req","Por favor capture los resultados de la comisión.");
	frmvalidator.addValidation("lugar","req","Por favor capture el lugar de la comisión.");
	frmvalidator.addValidation("tarifaDiaria","req","Por favor capture la tarifa diaria de la comisión.");
	frmvalidator.addValidation("numDias","req","Por favor capture el número de días de la comisión.");
	frmvalidator.addValidation("montoTotal","req","Por favor capture el monto total de la comisión.");
	
    function Regresar(){
    	window.location="lista_Comision.php";
    }
	
	function totalViaticos(){
		var tarifaDiaria=document.getElementById('tarifaDiaria').value;
		var numDias=document.getElementById('numDias').value;
		
		if(tarifaDiaria!='' && numDias!='')
			document.getElementById('montoTotal').value=tarifaDiaria*numDias; 
		
	}
	function dias(){
		var fini=document.getElementById('FechaInicio').value;
		var ffin=document.getElementById('FechaFinal').value;
		
		if(fini!='' && ffin!='')
			document.getElementById('numDias').value=ffin - fini; 
		
	}
	function saldoPend(){
		var montoTotal=document.getElementById('montoTotal').value;
		var montoDocumentos=document.getElementById('montoDocumentos').value;
		var montoSinDocumentos=document.getElementById('montoSinDocumentos').value;
		
		document.getElementById('saldoPendiente').value=montoTotal-montoDocumentos-montoSinDocumentos; 
		
	}
	function Validar(){
		var frmvalidator  = new Validator("frmComprobacion");
	
		frmvalidator.addValidation("Fecha","req","Por favor capture la fecha del oficio de comisión.");
		frmvalidator.addValidation("numeroOficio","req","Por favor capture el número de oficio de la comisión.");
		frmvalidator.addValidation("empleado","dontselect=0","Por favor seleccione el nombre del empleado.");
		frmvalidator.addValidation("FechaInicio","req","Por favor capture la fecha de inicio de la comisión.");
		frmvalidator.addValidation("FechaFinal","req","Por favor capture la fecha final de la comisión.");
		//frmvalidator.addValidation("objetivo","req","Por favor capture el objetivo de la comisión.");
		frmvalidator.addValidation("destino","dontselect=0","Por favor seleccione el destino de la comisión.");
		//frmvalidator.addValidation("resultados","req","Por favor capture los resultados de la comisión.");
		frmvalidator.addValidation("lugar","req","Por favor capture el lugar de la comisión.");
		frmvalidator.addValidation("tarifaDiaria","req","Por favor capture la tarifa diaria de la comisión.");
		frmvalidator.addValidation("numDias","req","Por favor capture el número de días de la comisión.");
		frmvalidator.addValidation("montoTotal","req","Por favor capture el monto total de la comisión.");
		frmvalidator.addValidation("montoDocumentos","req","Por favor capture el monto con documentos de la comisión.");
		frmvalidator.addValidation("montoSinDocumentos","req","Por favor capture el monto sin documentos de la comisión.");
		frmvalidator.addValidation("saldoPendiente","req","Por favor capture el saldo pendiente de la comisión.");
			    
    	 
    	 document.forms["frmComprobacion"].submit(); 
    }
	function validar(elemento,tipo)
	{
		var campo=document.getElementById(elemento);
		var texto=document.getElementById(elemento).value;
		
		switch(tipo)
		{
			case 'numerico':
				if(texto.match(/[a-z]+/gi).length!=null)
				{
					alert('El campo no puede contener letras');
					campo.value="0000";
				}
				break;
			case 'fecha':
				if(texto.match(/[a-z]+/gi).length!=null)
				{
					alert('El campo no puede contener letras');
					var date = new Date();
					var dia = date.getDate();      if( dia.toString().length == 1 ) dia = "0"+dia; 
					var mes = (date.getMonth()+1); if( mes.toString().length == 1 )  mes = "0"+mes;
					campo.value= dia+"-"+mes+"-"+date.getFullYear() ;
				}
				break;
			default:
				break;
		}
	}
	
	
</script>
</html>