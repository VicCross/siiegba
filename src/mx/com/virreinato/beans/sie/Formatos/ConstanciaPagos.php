<?php

$id = null;

session_start();
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/ReintegroDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Reintegro.class.php");
include_once("../src/mx/com/virreinato/dao/DirectivoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Directivo.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}else{
    if(isset($_SESSION['id'])){ $id = $_SESSION['id'];}
}
?>
<!DOCTYPE html>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<html>
    <head>
      <link rel="stylesheet" type="text/css" href="/css/style_Impresion.css">
      <script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
    </head>
    <body>
        
    <?php 
        $dao=new ReintegroDaoJdbc();
        $elemento=$dao->obtieneElemento($_GET['folio']);

        $dirDao=new DirectivoDaoJdbc();
        $sub=$dirDao->obtieneElementoCargo("2");
        $dir=$dirDao->obtieneElementoCargo("1");

    ?>
    
    <div class="Contenido">
	<font size="1px">
            <table width="95%" align="center" cellspacing="0" border="1"  style="font-size:7.4px">
                <tr>
                    <td colspan="7">
                        <table align="center" width="100%"> <!-- Encabezado -->
                            <tr>
                                <td colspan="5"></td>
                                <td align="right" width="15%">37A <br></br>37AP1A04 </td>
                            </tr>

                            <tr>
                                <td colspan="6" width="100%" align="center"><b> CONSTANCIA DE PAGOS Y RETENCIONES ISR, IVA E IEPS </b></td>
                            </tr>

                            <tr>
                                <td width="40%"></td>
                                <td align="center"><input type="hidden" id="fecha" value="<?php echo(date("d-m-Y",strtotime( $elemento->getFecha())) );?>"/></td>
                                <td align="center" width="15%"><b>MES INICIAL</b></td>
                                <td align="center" width="15%"><b>MES FINAL</b></td>
                                <td align="center" width="15%"><b>EJERCICIO</b></td>
                                <td width="15%"></td>
                            </tr>
						
                            <tr>
                                <td></td>
                                <td align="right" width="35%">PERIODO QUE AMPARA LA CONSTANCIA:</td>
                                <td align="center"> <input style="font-size:11px" type="text" id="mes" size="3" readonly="readonly"> </td>
                                <td align="center"> <input style="font-size:11px" type="text" id="mes2" size="3" readonly="readonly"> </td>
                                <td align="center"> <input style="font-size:11px" type="text" id="anio" size="3" readonly="readonly"> </td>
                                <td width="15%"> </td>
                            </tr>
                        </table>
                    </td>
                </tr>
			
			<!-- Identificacion del tercero -->
			
                <tr>
                    <td colspan="7">
                        <table width="100%">
                            <tr>
                                <th width="20%" align="left"> 1 </th>
                                <th align="center" > DATOS DE IDENTIFICACIÓN DEL TERCERO </th>
                            </tr>
                        </table>
                    </td>
                </tr>
	
                <tr>
                    <td colspan="2" width="20%"> REGISTRO FEDERAL DE CONTRIBUYENTES</td>
                    <td colspan="5" width="70%"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <?php echo$elemento->getRfc(); ?> </td>
                </tr>
                <tr>
                    <td colspan="2"> CLAVE ÚNICA DE REGISTRO DE REGISTRO DE POBLACIÓN (*)</td>
                    <td colspan="5"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  <?php echo$elemento->getCurp(); ?> </td>
                </tr>
                <tr>
                    <td colspan="2"> APELLIDO PATERNO,MATERNO Y NOMBRE(S) O DENOMINACIÓN O RAZÓN SOCIAL </td>
                    <td colspan="5"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  <?php  echo($elemento->getApp().' '.$elemento->getApm().' '.$elemento->getNombre()); ?> </td>
                </tr>
			
			<!-- Dividendos o utilidades distribuidos -->
			
                <tr>
                    <td colspan="7">
                        <table width="100%">
                            <tr>
                                <th width="20%" align="left"> 2 </th>
                                <th align="center" > DIVIDENDOS O UTILIDADES DISTRIBUIDOS </th>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table width="100%">
                            <tr>
                                <td width="20%" align="center"> a. </td>
                                <td width="80%"> TIPO DE DIVIDENDO O UTILIDAD DISTRIBUIDO </td>
                            </tr>
                        </table>
                    </td>
                    <td width="5%"> </td>
                    <td width="15%"> </td>
                    <td colspan="2">
                        <table width="100%">
                            <tr>
                                <td width="10%" align="center"> c. </td>
                                <td width="90%"> MONTO DEL DIVIDENDO O UTILIDAD ACUMULABLE </td>
                            </tr>
                        </table>
                    </td>
                    <td width="20%"></td>
                </tr>
                    <tr>
                        <td colspan="2">
                            <table width="100%">
                                <tr>
                                    <td width="20%" align="center"> b. </td>
                                    <td width="80%"> MONTO DEL DIVIDENDO O UTILIDAD DISTRIBUIDO </td>
                                </tr>
                            </table>
                        </td>
                        <td width="20%" colspan="2"> </td>
                        <td colspan="2">
                            <table width="100%">
                                <tr>
                                    <td width="10%" align="center"> d. </td>
                                    <td width="90%"> MONTO DEL ISR ACREDITABLE </td>
                                </tr>
                            </table>
                        </td>
                        <td width="20%"></td>
                    </tr>
                    <tr>
                        <td colspan="2"> DOMICILIO DEL SOCIO O ACCIONISTA (Calle,número.código postal,entidad federativa) </td>
                        <td colspan="5"></td>
                    </tr>
			
			<!-- Remanente distribuible -->
			
                    <tr>
                        <td colspan="7">
                            <table width="100%">
                                <tr>
                                    <th width="20%" align="left"> 3 </th>
                                    <th align="center" > REMANENTE DISTRIBUIBLE </th>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="100%">
                                <tr>
                                    <td width="20%" align="center"> e. </td>
                                    <td width="80%"> PORCENTAJE DE PARTICIPACIÓN </td>
                                </tr>
                            </table>
                        </td>
                        <td width="20%" colspan="2" align="right"> %</td>
                        <td colspan="2">
                            <table width="100%">
                                <tr>
                                    <td width="10%" align="center"> g. </td>
                                    <td width="90%">IMPUESTO RETENIDO (En su caso) </td>
                                </tr>
                            </table>
                        </td>
                        <td width="20%"></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="100%">
                                <tr>
                                    <td width="20%" align="center"> f. </td>
                                    <td width="80%"> MONTO DEL REMANENTE QUE LE CORRESPONDE </td>
                                </tr>
                            </table>
                        </td>
                        <td width="20%" colspan="2"> </td>
                        <td width="20%" colspan="2"></td>
                        <td width="20%"></td>
                    </tr>
			
			<!-- Otros pagos y retenciones -->
			
                    <tr>
                        <td colspan="7">
                            <table width="100%">
                                <tr>
                                    <th width="20%" align="left"> 4 </th>
                                    <th align="center" > OTROS PAGOS Y RETENCIONES </th>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="100%">
                                <tr>
                                    <td width="20%" align="center"> h. </td>
                                    <td width="80%"> CLAVE DEL PAGO (2) </td>
                                </tr>
                            </table>
                        </td>
                        <td width="5%" align="center"> A1</td>
                        <td width="15%" align="center"> SERVICIOS PROFESIONALES </td>
                        <td colspan="2">
                            <table width="100%">
                                <tr>
                                    <td width="10%" align="center"> j. </td>
                                    <td width="90%">PAGOS PROVISIONALES EFECTUADOS POR LA FIDUCIAIA (Tratandose de arrendamiento en fideicomiso) </td>
                                </tr>
                            </table>
                        </td>
                        <td width="20%"></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="100%">
                                <tr>
                                    <td width="20%" align="center"> i. </td>
                                    <td width="80%"> MONTO DEL INTERÉS NOMINAL (3) </td>
                                </tr>
                            </table>
                        </td>
                        <td width="20%" colspan="2"> </td>
                        <td colspan="2">
                            <table width="100%">
                                <tr>
                                    <td width="10%" align="center"> k. </td>
                                    <td width="90%">DEDUCCIONES CORRESPONDIENTES (Tratandose de arrendamiento en fideicomiso)</td>
                                </tr>
                            </table>
                        </td>
                        <td width="20%"></td>
                    </tr>
                    <tr>
                        <td colspan="2"> ESPECIFIQUE TIPO DE PAGO (Sólo si seleccionó la clave de pago G1. Otros) </td>
                        <td colspan="5"></td>
                    </tr>
			
			<!-- ISR, IVA, IEPS -->
			
                    <tr>
                        <td colspan="2"></td>
                        <td colspan="2" align="center"><b>ISR</b></td>
                        <td colspan="2" align="center"><b>IVA</b></td>
                        <td align="center"><b>IEPS</b></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="100%">
                                <tr>
                                    <td width="20%" align="center"> l. </td>
                                    <td width="80%"> MONTO DE LA OPERACIÓN O ACTIVIDAD GRAVADA (3) </td>
                                </tr>
                            </table>
                        </td>
                        <td width="20%" colspan="2" align="center"> $ </td> 
                        <td width="20%" colspan="2" align="center"> $ </td>
                        <td width="20%"></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="100%">
                                <tr>
                                    <td width="20%" align="center"> m. </td>
                                    <td width="80%"> MONTO DE LA OPERACIÓN O ACTIVIDAD EXENTA </td>
                                </tr>
                            </table>
                        </td>
                        <td width="20%" colspan="2" align="center"> $ </td> 
                        <td width="20%" colspan="2" align="center"> $ </td>
                        <td width="20%"></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="100%">
                                <tr>
                                    <td width="20%" align="center"> n. </td>
                                    <td width="80%"> IMPUESTO RETENIDO </td>
                                </tr>
                            </table>
                        </td>
                        <td width="20%" colspan="2" align="center"> $ </td> 
                        <td width="20%" colspan="2" align="center"> $ </td>
                        <td width="20%"></td>
                    </tr>
			
			<!-- DATOS DEL RETENEDOR -->
			
                    <tr>
                        <td colspan="7">
                            <table width="100%">
                                <tr>
                                    <th width="20%" align="left"> 5 </th>
                                    <th align="center" > DATOS DEL RETENEDOR </th>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" colspan="2"> REGISTRO FEDERAL DE CONTRIBUYENTES </td>
                        <td width="20%" colspan="2" align="center">  NA-460815-GC1 </td> 
                        <td width="20%" colspan="2"> CLAVE ÚNICA DE REGISTRO DE POBLACIÓN (*) </td>
                        <td width="20%"></td>
                    </tr>
                    <tr>
                        <td colspan="2"> APELLIDO PATERNO,MATERNO Y NOMBRE(S) O DENOMINACIÓN O RAZÓN SOCIAL </td>
                        <td colspan="5" align="center">   INSTITUTO NACIONAL DE ANTROPOLOGIA E HISTORIA  </td>
                    </tr>
			<tr>
                            <td rowspan="2" width="5%"><img alt="" src="../img/datosRepresentante.PNG" width="30" height="50"></img></td>
                            <td width="15%"> APELLIDO PATERNO,MATERNO Y NOMBRE(S) O DENOMINACIÓN O RAZÓN SOCIAL </td>
                            <td colspan="5" align="center"> <?php  echo($sub->getNombre()." ".$sub->getApPaterno()." ".$sub->getApMaterno());?> </td>
			</tr>
			<tr>
                            <td width="15%"> REGISTRO FEDERAL DE CONTRIBUYENTES </td>
                            <td width="20%" colspan="2" align="center"> MOPB-650329-SF8  </td> 
                            <td width="20%" colspan="2" > CLAVE ÚNICA DE REGISTRO DE POBLACIÓN (*) </td>
                            <td width="20%" align="center"> MOPB650329HHGRRN03 </td>
			</tr>
			
			<!-- Especificaciones -->
			
			<tr>
                            <td colspan="7" width="100%">
                                <table>
                                    <tr>
                                        <td width="1%"></td>
                                        <td width="1%"> (*)</td>
                                        <td width="48%"> Sólo personas fisicas </td>
                                        <td width="1%"> C1.</td>
                                        <td width="49%"> Enajenación de acciones</td>
                                    </tr>
                                    <tr>
                                        <td width="1%"></td>
                                        <td width="1%"></td>
                                        <td width="48%"></td>
                                        <td width="1%"> C2.</td>
                                        <td width="49%"> Enajenación de bienes objeto de la LIEPS, a través de mediadores, agentes, representantes, corredores, consignatarios o distribuibles</td>
                                    </tr>
                                    <tr>
                                        <td width="1%">(1)</td>
                                        <td width="1%">A.</td>
                                        <td width="48%"> Proviene de CUFN </td>
                                        <td width="1%"> C3.</td>
                                        <td width="49%"> Enajenación de otros bienes, no consignada en escritura pública</td>
                                    </tr>
                                    <tr>
                                        <td width="1%"></td>
                                        <td width="1%">B.</td>
                                        <td width="48%"> No proviene de CUFN</td>
                                        <td width="1%"> D1.</td>
                                        <td width="49%"> Adquisición de desperdicios industriales</td>
                                    </tr>
                                    <tr>
                                        <td width="1%"></td>
                                        <td width="1%"> C.</td>
                                        <td width="48%"> Reembolso o reducción de capital </td>
                                        <td width="1%"> D2.</td>
                                        <td width="49%"> Adquisición de otros bienes, no consignada en escritura publica</td>
                                    </tr>
                                    <tr>
                                        <td width="1%"></td>
                                        <td width="1%"> D.</td>
                                        <td width="48%"> Liquidación de la persona moral </td>
                                        <td width="1%"> E1.</td>
                                        <td width="49%"> Intereses</td>
                                    </tr>
                                    <tr>
                                        <td width="1%"></td>
                                        <td width="1%">E.</td>
                                        <td width="48%"> CUFINRE (fracción XLV del Art. Segundo de las disposiciones Transitorias para 2002). </td>
                                        <td width="1%"> E2.</td>
                                        <td width="49%"> Operaciones financieras derivadas</td>
                                    </tr>
                                    <tr>
                                        <td width="1%"></td>
                                        <td width="1%"></td>
                                        <td width="48%"></td>
                                        <td width="1%"> F1.</td>
                                        <td width="49%"> Premios</td>
                                    </tr>
                                    <tr>
                                        <td width="1%">(2)</td>
                                        <td width="1%">A1.</td>
                                        <td width="48%"> Servicios profesionales </td>
                                        <td width="1%"> G1.</td>
                                        <td width="49%"> Otros</td>
                                    </tr>
                                    <tr>
                                        <td width="1%"></td>
                                        <td width="1%">A2.</td>
                                        <td width="48%"> Regalias por derechos de autor</td>
                                        <td width="1%"> </td>
                                        <td width="49%"></td>
                                    </tr>
                                    <tr>
                                        <td width="1%"></td>
                                        <td width="1%">A3.</td>
                                        <td width="48%">Autotransporte terrestre de carga</td>
                                        <td width="1%"> </td>
                                        <td width="49%"></td>
                                    </tr>
                                    <tr>
                                        <td width="1%"></td>
                                        <td width="1%">A4.</td>
                                        <td width="48%"> Servicios prestados por comisionistas </td>
                                        <td width="1%"> (3).</td>
                                        <td width="49%"> Deberá anotar el monto del interés nominal en el campo i y el monto del interés real en el campo l, tratándose de intereses.</td>
                                    </tr>
                                    <tr>
                                        <td width="1%"></td>
                                        <td width="1%">B1.</td>
                                        <td width="48%"> Arrendamiento </td>
                                        <td width="1%"></td>
                                        <td width="49%"></td>
                                    </tr>
                                    <tr>
                                        <td width="1%"></td>
                                        <td width="1%">B2.</td>
                                        <td width="48%"> Arrendamiento en fideicomiso </td>
                                        <td width="1%"> </td>
                                        <td width="49%"></td>
                                    </tr>
                                </table>
                            </td>
			</tr>
			
			<!-- Sellos y firmas -->
			
			<tr>
                            <td height="100" valign="bottom" colspan="3" align="center" ><?php  echo($sub->getNombre()." ".$sub->getApPaterno()." ".$sub->getApMaterno() );?> <br> FIRMA DEL RETENEDOR O REPRESENTANTE LEGAL</td>
                            <td height="100" valign="bottom" align="center" colspan="3" > SELLO DEL RETENEDOR (EN CASO DE TENERLO)</td>
                            <td height="100" valign="bottom" align="center">CRUZ FUENTES CARINA <br> FIRMA DE RECIBIDO POR EL CONTRIBUYENTE</td>
			</tr>
			<tr>
                            <td colspan="7" align="center">SE EXPIDE POR DUPLICADO</td>
			</tr>
		</table>
		<center>Original-Contribuyente &nbsp; &nbsp; &nbsp;  Duplicado-Retenedor</center>
	</font>
    </div>
</body>
<script>
    $().ready(function(){
       var fecha = $("#fecha").val();
       var aux = fecha.split("-");
       $("#anio").val(aux[2]);
       $("#mes").val(Mes(aux[1]));
       $("#mes2").val(Mes(aux[1]));
    });

    function Mes(num){
        if(num == "01") return "Enero";
        if(num == "02") return "Febrero";
        if(num == "03") return "Marzo";
        if(num == "04") return "Abril";
        if(num == "05") return "Mayo";
        if(num == "06") return "Junio";
        if(num == "07") return "Julio";
        if(num == "08") return "Agosto";
        if(num == "09") return "Septiembre";
        if(num == "10") return "Octubre";
        if(num == "11") return "Novimbre";
        if(num == "12") return "Diciembre";
    }
</script>
</html>