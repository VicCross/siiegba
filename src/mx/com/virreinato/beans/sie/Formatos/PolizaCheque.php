<?php

include_once("../src/mx/com/virreinato/dao/CuentaBancariaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CuentaBancaria.class.php");
include_once("../src/mx/com/virreinato/dao/CatChequeDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatCheque.class.php");
include_once("../src/mx/com/virreinato/dao/ProveedorDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Proveedor.class.php");
include_once("../src/mx/com/virreinato/dao/EmpleadoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Empleado.class.php");
include_once("../src/mx/com/virreinato/dao/PresupuestoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Presupuesto.class.php");
include_once("../src/mx/com/virreinato/dao/DetPresupuestoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/DetPresupuesto.class.php");
include_once("../src/mx/com/virreinato/dao/AreaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Area.class.php");
include_once("../src/mx/com/virreinato/dao/DirectivoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Directivo.class.php");
include_once("../src/mx/com/virreinato/dao/ProgramaInstitucionalDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProgramaInstitucional.class.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="/css/style_Impresion.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
        <script>
   	function Mes(num){
   		 if(num == "01") return "ENERO";
   	 	 if(num == "02") return "FEBRERO";
   	 	 if(num == "03") return "MARZO";
   	 	 if(num == "04") return "ABRIL";
   	 	 if(num == "05") return "MAYO";
   	 	 if(num == "06") return "JUNIO";
   	 	 if(num == "07") return "JULIO";
   	 	 if(num == "08") return "AGOSTO";
   	 	 if(num == "09") return "SEPTIEMBRE";
		 	 if(num == "10") return "OCTUBRE";
		 	 if(num == "11") return "NOVIEMBRE";
		 	 if(num == "12") return "DICIEMBRE";
      }

   	$().ready(function(){
   		 var fecha =  $("#fecha").val();
   	 	if( fecha != "" ){
   	 		var aux = fecha.split("-");
   	 		$("#fechaPoliza").text("TEPOTZOTLAN EDO. DE MEX. A: "+aux[0]+" "+Mes(aux[1])+" "+aux[2] );
   	 	}
   	 
   	});
     </script>
    <title> POLIZA DE CHEQUE </title>
	</head>
	<?php
        $id = null;
    if(isset($_GET['folio'])){ $id = (String)$_GET['folio']; } 
    $daoCheque=new CatChequeDaoJdbc();
    $cheque=new CatCheque();	
    
    $dirDao=new DirectivoDaoJdbc();
    $sub=$dirDao->obtieneElementoCargo("2");
    $dir=$dirDao->obtieneElementoCargo("1");
    $asi=$dirDao->obtieneElementoCargo("5");
    $au=$dirDao->obtieneElementoCargo("4");
	
		
    $cheque = $daoCheque->obtieneElemento($id);
	
    $daoPre = new PresupuestoDaoJdbc();
    $p = new Presupuesto();
    $p = $daoPre->obtieneElementoGb((String)($cheque->getIdSolicitud()));
    
    $meta="";
    
    if($p->getDestino() == "PR"){
    	$p= $daoPre->obtieneElemento((String)($cheque->getIdSolicitud()));
    	$meta=$p->getNombreMeta();
    }
    
    $progInst = null;
    if($p->getIdPrograma()!= null)
    {
        $daoInst = new ProgramaInstitucionalDaoJdbc();
        $progInst=$daoInst->obtieneElemento($p->getIdPrograma());
    }
    	
    $dao=new DetPresupuestoDaoJdbc();
	 
    $lista=null;
    if($p->getDestino() == "PR"){
        $lista=$dao->obtieneListado((String)($cheque->getIdSolicitud())); 
    }
    else{
        $lista=$dao->obtieneListadoGb((String)($cheque->getIdSolicitud()));
    }
      
      $elemPart=new DetPresupuesto();
      $aux=0;
 ?>
<body>
    <font size="2">
        <table style="border: solid 2px "  rules="groups" frame="box" align="center" cellspacing="0" width="96%"> <!-- Tabla de la poliza -->
            <tbody>	
                <tr>
                   <td style=" background-color: #424242; color: white; " height="30" align="left"> <b> &nbsp; &nbsp; &nbsp; POLIZA DE CHEQUE </b> </td>
                   <td></td>
                   <td align="right"> No. De Cheque: </td>
                   <td align="left"> &nbsp; &nbsp; &nbsp;  <b> <?php echo($cheque->getFolio()); ?> </b> </td>
                   <td></td>
                   <td align="right"> COPIA DEL CHEQUE </td>
                </tr>


                <tr>
                    <td colspan="4" > <input type="hidden" id="fecha" value="<?php echo(date("d-m-Y",strtotime($cheque->getFechaEmision()))); ?>"/> </td>
                    <td colspan="2" id="fechaPoliza"></td>
                </tr>

                <tr>
                    <td colspan="2" align="right" > <b>
                    <?php
                        if($p!= null && $p->getIdProveedor() != null && !(String)($p->getIdProveedor()) == "0"){
                        $aux = 1;

                        $daoProv=new ProveedorDaoJdbc();
                        $elementoProv = $daoProv->obtieneElemento((String)($p->getIdProveedor()));
                            echo( $elementoProv->getProveedor() );
                        }

                        else if($p!= null && $p->getIdEmpleado()!= null && !(String)($p->getIdEmpleado()) == "0"){
                        $aux = 1;
                        $daoEm=new EmpleadoDaoJdbc();
                            $elementoEm = $daoEm->obtieneElemento((String)($p->getIdEmpleado()));
                                echo( $elementoEm->getNombre()." ".$elementoEm->getApPaterno()." ".$elementoEm->getApMaterno() );
                        }	

                        else if($p!= null && $p->getOtro() != null && $aux == 0){
                            echo($p->getOtro());
                        }
                    ?></b>   
            </td>
            <td colspan="4"  align="center"> <b> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  <?php  echo("$".number_format( $cheque->getMonto(),2 )); ?>  </b></td>
                </tr>

                <tr>
                      <td colspan="4" height="30" align="center" style="font-size:10px"> (<?php echo($cheque->getMontoLetra());?>)</td>
                      <td colspan="2" ></td>
                </tr>

                <tr>
                        <td  align="left" width="2%"> &nbsp; &nbsp; CTA. &nbsp; &nbsp;
                           <b><?php 
                                $daoCuenta=new CuentaBancariaDaoJdbc();
                                $Cuenta=$daoCuenta->obtieneElemento((String)($cheque->getIdCuenta()));
                                echo($Cuenta->getNumeroCuenta());
                   ?></b>
                        </td>
                        <td colspan="5" width="10%"> </td>
                </tr>
			
            </tbody>
        </table>
	
         <br>
         
         <table align="center" width="96.5%"> <!-- tabla de concepto y firma -->
            <tr>
        <td width="67%">
            <table style="border: solid 2px "  rules="groups" frame="box" cellspacing="0" width="100%"> <!-- Tabla del concepto -->
                    <thead>
                        <tr> <th colspan="6"> CONCEPTO </th>  </tr>
                    </thead>

                    <tbody style="border: solid 2px " >
                        <tr> 
                               <td colspan="6" height="50"> &nbsp; &nbsp; <?php  if($cheque->getObservaciones()!=null ){ echo($cheque->getObservaciones()); } ?> </td> 
                       </tr>

                       <tr>
                           <td width="15%" height="30"> <b> &nbsp; &nbsp; APLICACION: </b> </td>
                            <td colspan="2" width="25%"> 
                            <?php 
                            if($cheque->getDestino()!=null ){

                                    if($cheque->getDestino() == "PR"){
                                            echo("PROYECTO");	
                                    }
                                    else if($cheque->getDestino() == "GB"){
                                            echo("GASTO BÁSICO");	
                                    }
                                    else if($cheque->getDestino() == "TR"){
                                            echo("TERCEROS");	
                                    }
                                    else if($cheque->getDestino() == "DN"){
                                            echo("DONATIVOS");	
                                    }

                            }  
                            ?> </td>
                        <td width="10%"> <b> AREA: </b> </td>
                        <td colspan="2" width="50%">
                            <?php
                                $daoArea = new AreaDaoJdbc();
                                $listaArea =$daoArea->obtieneAreas();
                                $Area=new Area();
                                foreach($listaArea as $Area){
                                    if( $cheque!=null && $cheque->getArea()!=null  ){
                                        if( $cheque->getArea() == $Area->getId() ){ echo($Area->getDescripcion()); }
                                    }

                                }
                            ?> 
                            </td>
                                </tr>

                                <tr>
                                        <td colspan="2" height="30"> <b> &nbsp; &nbsp; ACTIVIDAD/OBJETIVO: </b> </td>
                                        <td colspan="4"> <?php if( $progInst!=null ) { echo($progInst->getProgramaInstitucional()); }?> </td>			
                                </tr>


                                <?php if( $meta!=null && !$meta == "" ) { ?>
                                <tr>
                                        <td height="30"> <b> &nbsp; &nbsp;  META: </b> </td>
                                <td colspan="5"> <?php if( $meta!=null ){ echo($meta); }?></td> 
                                </tr>
                                <?php } ?>
                                <tr>
                                        <td height="30"> <b>  &nbsp; &nbsp; PARTIDA: </b> </td>
                                        <td  align="center" colspan="4">
                                    <?php 
                                        foreach($lista as $elemPart){
                                                echo((String)$elemPart->getPartida().","); 
                                        }
                                    ?>
                                        </td>

                                        <td width="20%"> <b> POR COMPROBAR </b>  </td>
                                </tr>
				
                        </tbody>
                </table>
                </td>
                <td width="2%"></td>
                <td width="26%">
                    <table style="border: solid 2px "  rules="groups" frame="box" cellspacing="0" width="100%">	<!-- Firma -->													
                            <tbody style="border: solid 2px " >
                                    <tr>
                                            <td align="center"> FIRMA </td>
                                    </tr>
                                    <tr>
                                            <td height="186"> </td>					
                                    </tr>
                            </tbody>
                    </table>
                </td>
            </tr>
            </table>

            <br>

            <table border="1" cellspacing="0" align="center" width="96%"> <!-- Cuenta -->
                <tr>
                        <th colspan="3"> CUENTA </th>
                        <th width="12%"></th>
                        <th width="13.6%"> DEBE </th>
                        <th width="13.6%"> HABER </th>
                </tr>

                <tr>
                        <td height="70" width="5%">Banamex</td>
                        <td width="10%"></td>
                        <td><?php 
                        foreach($lista as $elemPart){
                            echo($elemPart->getPartida().", "); 
                        }
                        ?>
                        </td>
                        <td></td>
                        <td></td>
                        <td><?php echo("$".number_format($cheque->getMonto(),2)); ?></td>
                </tr>

                <tr>
                        <td height="70" width="5%"></td>
                        <td width="10%"></td>
                        <td></td>
                        <td></td>
                        <td><?php echo("$".number_format($cheque->getMonto(),2)); ?></td>
                        <td></td>
                </tr>
            </table>			
                <table width="96.5%" align="center"> <!-- Sumas del debe y haber en cuenta -->
                    <tr>
                        <td width="0%"></td>
                        <td width="39.2%"> 
                            <table border="1" cellspacing="0" width="100%">								
                                <tr>
                                    <th> SUMAS IGUALES</th>
                                    <td width="34.9%"><?php echo("$".number_format( $cheque->getMonto(),2)); ?></td>
                                    <td width="34.9%"><?php echo("$".number_format( $cheque->getMonto(),2)); ?></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>			
                <br><br><br>

                <table align="center" border="1" cellspacing="0" align="center" width="96%"> <!-- Ultima tabla -->
                        <tr>
                                <th> HECHO POR </th>
                                <th></th>
                                <th> AUTORIZADO </th>
                                <th> AUXILIARES </th>
                                <th> DIARIO </th>
                                <th> POLIZA No. </th>				
                        </tr>			
                        <tr>
                                <td width="15%" align="center" height="30"> <?php echo($asi->getIniciales());?> </td>
                                <td width="15%" align="center"> <?php echo($sub->getIniciales());?> </td>
                                <td width="15%" align="center"> <?php echo($dir->getIniciales());?> </td>
                                <td width="15%" align="center"> <?php echo($au->getIniciales());?> </td>
                                <td width="25%" align="center"></td>
                                <td width="15%" align="center"><?php echo( $cheque->getNum_Poliza()); ?></td>
                        </tr>
                </table>
			
            </font>
	</body>
</html>