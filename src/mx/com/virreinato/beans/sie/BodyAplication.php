<?php
if (isset($_SESSION['id'])) {
    include_once("src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
    //Obtenemos el valor del color, su id es el número 5
    $parametro = new ParametroDaoJdbc();
    $parametro = $parametro->obtieneElemento(5);
    $id = $_SESSION['id'];    
    ?>
<!DOCTYPE HTML>
    <html>
        <head>
            <title>MENU</title>
            <meta http-equiv="X-UA-Compatible" content="IE=8" />
            <meta http-equiv="X-UA-Compatible" content="IE=7" />
            <link rel="stylesheet" type="text/css" href="css/pro_dop_1.css">
            <script language="JavaScript" type="text/javascript" src="js/jquery-1.7.2.js" ></script>
            <!-- Este Script nos ayuda a usar el valor del color que se encuentra como parámetro -->
            <script>
                $(document).ready(function() {
                    $(".menu ul").css({
                        background: "<?php echo $parametro->getValor(); ?>"
                    });
                    $("#.menu a").css({
                        background: "<?php echo $parametro->getValor(); ?>"
                    });
                    
                    $(".menu ul ul").css({
                        position: "absolute",
                        display: "none"
                    });

                    $(".menu li").hover(function() {
                        $(this).find(' > ul').stop(true, true).slideDown('fast');
                    }, function() {
                        $(this).find(' > ul').stop(true, true).slideUp('fast');
                    });
                });
            </script>
        </head>
        <!-- El manejo de los usuario se hace a código duro a través de esta página 
        El id con más prioridad es el número 1 y el de menor prioridad es de 7 -->
        <body>
            <div class="menu" style='padding:0px;margin:0px;'  >
                <ul style='background-color:<?php echo $parametro->getValor(); ?> ;height:40px' style='padding:0px;margin:0px;'> 

                    <li style='width:120px' ><a href="" onclick="return false;" style='width:120px' >Consulta<br> Presupuesto </a>
                        <ul>
                            <?php if ($id == 7) { ?>
                                <li><a href="ConsultaPresupuesto/Resumen_Usuario.php" target="destino"> Resumen</a></li>
                                <li><a href="ConsultaPresupuesto/Detalle_Usuario.php" target="destino"> Detalle</a></li>
                            <?php } else { ?>
                                <li><a href="ConsultaPresupuesto/Resumen.php" target="destino"> Resumen</a></li>
                                <li><a href="ConsultaPresupuesto/Detalle.php" target="destino"> Detalle</a></li>
                                <li><a href="ConsultaPresupuesto/PlaneacionGb.php" target="destino">Planeación Gasto Básico</a></li>
                            <?php } ?>
                        </ul>
                    </li>
                    <li style='width:110px'><a style='width:110px' onclick="return false;" href="">Proyectos </a>
                        <ul>
                            <?php if ($id < 7) { ?>
                                <li><a href="">&nbsp; Información de Proyectos</a>
                                    <ul>
                                        <li><a href="Proyectos/CartaCons.php" target="destino">&nbsp; Carta Constitutiva</a></li>
                                        <li><a href="Proyectos/lista_TarjetaRegistro.php" target="destino">&nbsp; Tarjeta de Registro</a></li>
                                        <li><a href="Proyectos/lista_NominaProy.php" target="destino">&nbsp; Nómina</a></li>
                                    </ul>
                                </li>
                                <li><a href="Proyectos/lista_Fondos.php" target="destino">&nbsp; Solicitud de Fondos</a></li>
                            <?php } ?>
                            <li><a href="Proyectos/Solicitud.php" target="destino">&nbsp; Solicitud de Presupuesto</a></li>
                            <?php if ($id < 7) { ?> <li><a href="Proyectos/lista_Comprobacion.php" target="destino">&nbsp; Comprobación Museo</a></li> <?php } ?>
                            <?php if ($id < 7) { ?> <li><a href="Proyectos/lista_ComprobaINAH.php" target="destino">&nbsp; Comprobación INAH</a></li> <?php } ?>
                            <?php if ($id < 7) { ?> <li><a href="Proyectos/lista_Reintegro.php" target="destino">&nbsp; Reintegro de Impuestos</a></li> <?php } ?>
                        </ul>
                    </li>
                    <?php if ($id < 7) { ?>
                        <li style='width:110px'> <a style='width:110px' onclick="return false;" href="" style='width:110px'>Gasto <br> Básico</a>
                            <ul style='width:110px'>
                                <li><a href="gasto_basico/lista_CalendarioGb.php" target="destino">&nbsp; Presupuesto Asignado</a></li>
                                <li><a href="gasto_basico/SolicitudGb.php" target="destino">&nbsp; Solicitud de Presupuesto</a></li>
                                <?php if ($id < 7) { ?> <li><a href="gasto_basico/lista_ComprobacionGb.php"  target="destino">&nbsp; Comprobación Museo</a></li> <?php } ?>
                                <?php if ($id < 7) { ?> <li><a  href="gasto_basico/lista_ComprobaINAHGb.php"  target="destino">&nbsp; Comprobación INAH</a></li> <?php } ?>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($id == 1 || $id == 2 || $id == 5 || $id == 6) { ?>
                        <li style='width:120px'><a style='width:120px' target="destino" href="Proyectos/lista_Ministracion.php">&nbsp;Ministraciones</a></li>
                    <?php } ?>
                    <?php if ($id < 7) { ?>
                        <li style='width:100px'> <a href="" onclick="return false;" style='width:100px'>Bancos</a>
                            <ul>
                                <li><a target="destino" href="bancos/selecciona_periodo.php">&nbsp; Movimientos Bancarios</a></li>
                                <?php if ($id != 3 && $id != 4) { ?>
                                    <li><a target="destino" href="bancos/carga_archivo_banco.php" >&nbsp; Cargar Estado de Cuenta</a></li>
                                    <li><a target="destino" href="bancos/selecciona_mes_periodo.php">&nbsp; Conciliación Bancaria</a></li>
                                <?php } ?>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($id == 1 || $id == 5 || $id == 6) { ?>
                        <li  style='width:100px'> <a style='width:100px' target="destino"  href="gasto_basico/Sueldos.php">&nbsp; Sueldos</a></li>
                    <?php } ?>


                    <?php if ($id < 7) { ?>
                        <li style='width:100px'> <a href="" onclick="return false;" style='width:100px'>Terceros</a>
                            <ul style='width:120px'>
                                <li><a href="Terceros/lista_CalendarioTr.php" target="destino">&nbsp; Presupuesto Asignado</a></li>
                                <li><a href="Terceros/SolicitudTr.php" target="destino">&nbsp; Solicitud de Presupuesto</a></li>
                                <?php if ($id < 7) { ?> <li><a href="Terceros/lista_ComprobacionTr.php"  target="destino">&nbsp; Comprobación Museo</a></li> <?php } ?>
                            </ul>
                        </li>
                    <?php } ?>


                    <?php if ($id < 7) { ?>
                        <li style='width:100px'> <a href="" onclick="return false;" style='width:100px'>Donativos</a>
                            <ul style='width:120px'>
                                <li><a href="Donativos/SolicitudDn.php" target="destino">&nbsp; Solicitud de Presupuesto</a></li>
                                <?php if ($id < 7) { ?> <li><a href="Donativos/lista_ComprobacionDn.php"  target="destino">&nbsp; Comprobación  Museo</a></li> <?php } ?>
                            </ul>
                        </li>
                    <?php } ?>



                    <?php if ($id == 1) { ?>
                        <li style='width:100px'><a style='width:100px'  href="catalogos/menu_catalogos.php" target="destino">Catálogos</a>
                            <ul>
                                <li><a href="">&nbsp; Catálogos de Estructura</a>
                                    <ul>
                                        <li><a href="catalogos/lista_ccostos.php" target="destino">&nbsp; Centro de Costos</a></li>
                                        <li><a href="catalogos/lista_directivo.php" target="destino">&nbsp; Directivos</a></li>
                                        <li><a href="catalogos/lista_area.php" target="destino">&nbsp; Áreas</a></li>
                                        <li><a href="catalogos/lista_periodo.php" target="destino">&nbsp; Períodos</a></li>
                                        <li><a href="catalogos/lista_capitulo.php" target="destino">&nbsp; Capítulos Presupuestales</a></li>
                                        <li><a href="catalogos/lista_partida.php" target="destino">&nbsp; Partidas Presupuestales</a></li>
                                        <li><a href="catalogos/lista_usuario.php" target="destino">&nbsp; Usuarios</a></li>
                                        <li><a href="catalogos/lista_parametro.php" target="destino">&nbsp; Parámetros del Sistema</a></li>
                                    </ul>
                                </li>
                                <li><a href="">&nbsp; Catálogos de Operación</a>
                                    <ul id="operacion">
                                        <li><a href="catalogos/lista_banco.php" target="destino">&nbsp; Bancos</a></li>
                                        <li><a href="catalogos/lista_cuenta.php" target="destino">&nbsp; Cuentas Bancarias</a></li>
                                        <li><a href="catalogos/lista_proveedor.php" target="destino">&nbsp; Proveedores</a></li>
                                        <li><a href="catalogos/lista_empleado.php" target="destino">&nbsp; Empleados</a></li>
                                        <li><a href="catalogos/lista_persona.php" target="destino">&nbsp; Personas</a></li>
                                    </ul>
                                </li>
                                <li  style='width:200px'><a href="">&nbsp; Catálogos de Proyectos</a>
                                    <ul id="proyectos">
                                        <li><a href="catalogos/lista_eje.php" target="destino">&nbsp; Ejes</a></li>
                                        <li><a href="catalogos/lista_linea_accion.php" target="destino">&nbsp; Líneas de Acción</a></li>
                                        <li><a href="catalogos/lista_area_normativa.php" target="destino">&nbsp; Áreas Normativas</a></li>
                                        <li><a href="catalogos/lista_proyecto.php" target="destino">&nbsp; Proyectos</a></li>
                                        <li><a href="catalogos/lista_personalProy.php" target="destino">&nbsp; Personal de Proyectos</a></li>

                                    </ul>
                                </li>

                            </ul>
                        </li>
                    <?php } ?>

                    <?php if ($id < 7) { ?>
                        <li style='width:100px'> <a href="" onclick="return false;" style='width:100px'>Formatos</a>
                            <ul style='width:120px'>
                                <li><a href="Formatos/lista_Comision.php" target="destino">&nbsp; Comisiones</a></li>
                                <li> <a href="Formatos/lista_Pasaje.php"  target="destino">&nbsp; Pasajes</a></li> 
                            </ul>
                        </li>
                    <?php } ?>

                    <li style='width:50px'> <a href="Cerrar.php"  target="_parent" style='width:100px'>Salir</a></li>
                        <?php
                        $perfil = null;
                        if ($id == 1)
                            $perfil = "Administrador del Sistema";
                        if ($id == 2)
                            $perfil = "Subdirector Adminstrativo";
                        if ($id == 3)
                            $perfil = "Subdirector Técnico";
                        if ($id == 4)
                            $perfil = "Director";
                        if ($id == 5)
                            $perfil = "Jefe de Departamento";
                        if ($id == 6)
                            $perfil = "Operador del Sistema";
                        if ($id == 7)
                            $perfil = "Usuario de Proyectos";
                        ?>
                </ul>
                <?php
                    echo("<div  style='width:100%;padding-left:5px;padding-bottom:5px; font-size:9px;text-align:right;pading-top:0px;background-color: ".$parametro->getValor()." margin-top:0px;'> <img src='img/iconUser.jpg' width='15' height='15' style='' align='bottom' />  <b>Usuario: </b>" . $_SESSION["nombre"] . " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Perfil: </b>" . $perfil . " &nbsp;&nbsp;</div> ");
                ?>
            </div>
        </body>
    </html>
    <?php
} else {
    header("Location: index.php?err=1&error=2");
}
?>