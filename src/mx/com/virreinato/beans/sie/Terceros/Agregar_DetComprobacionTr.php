<?php

session_start();
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/PartidaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Partida.class.php");
include_once("../src/mx/com/virreinato/dao/AreaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Area.class.php");
include_once("../src/mx/com/virreinato/dao/DetComprobacionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/DetComprobacion.class.php");
include_once("../src/mx/com/virreinato/dao/CatChequeDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatCheque.class.php");
include_once("../src/mx/com/virreinato/dao/ProveedorDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Proveedor.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html>
<html>
<script>
function Cambiar( a1, a2){
	document.getElementById(a1).innerHTML = document.getElementById(a2).value;		
	
}

	function validar(elemento,tipo)
	{
		var campo=document.getElementById(elemento);
		var texto=document.getElementById(elemento).value;
		
		switch(tipo)
		{
			case 'numerico':
				if(texto.match(/[a-z]+/gi).length!=null)
				{
					alert('El campo no puede contener letras');
					campo.value="0000";
				}
				break;
			case 'fecha':
				if(texto.match(/[a-z]+/gi).length!=null)
				{
					alert('El campo no puede contener letras');
					campo.value="01-01-2012";
				}
				break;
			default:
				break;
		}
	}
	
	function Regresar(){
    	window.location="lista_ComprobacionTr.jsp";
    }

</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../css/calendario.css" type="text/css" rel="stylesheet">
<script src="../js/calendar.js" type="text/javascript"></script>
<script src="../js/calendar-es.js" type="text/javascript"></script>
<script src="../js/calendar-setup.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<script src="../js/jquery-1.7.2.js"></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
<script>
	function OnlyNumber(value, elemnt){
			if( isNaN(value) ){
				elemnt.value = ""
			}
	}
</script>
<title>Consultar Comprobacion</title>
</head>
<body>
<div class="contenido">
<?php
    $id = "";
    $idC = null;
    $folio = null;
    $cheque = null;
    if(isset($_GET["idC"])){ $idC= $_GET["idC"]; }
    if(isset($_GET["folio"]))$folio = $_GET["folio"];
    if(isset($_GET["cheque"]))$cheque = $_GET["cheque"];

    if (isset($_GET['id']))
    {    $id = $_GET['id']; }

    $dao= new DetComprobacionDaoJdbc();
    $elemento = new DetComprobacion();
	
    if($id!=null)
    {   $elemento = $dao->obtieneElemento($id); }
		
		
?>
<br/>
<p class="titulo_cat1">Donativos > <a class="linkTitulo_cat1" href="lista_ComprobacionTr.php" > Comprobaci&oacute;n Museo </a> > <a class="linkTitulo_cat1" href="Agregar_ComprobacionDn.php?id=<?php echo$idC; ?>" > Detalle de Comprobación </a></p>
<h2 class="titulo_cat2">
<?php	

    if($id!=null)
    {
            echo("Modificar Comprobante");
    }	
    else
    {
            echo("Alta de Comprobante");
    }

?>
</h2>

  <form id="frmDetComprobacion" id="frmDetComprobacion" method="POST" action="../src/mx/com/virreinato/web/CatDetComprobacionTr.php">
  <table width="40%" border="0" cellspacing="0" cellpadding="5" class='tb_add_cat' align='center'>
  	<tr>
  	    <?php echo(" <input type='hidden' name='Comprobacion' id='Comprobacion' value='".$idC."'/>"); ?> 
    	<td ><br/>&nbsp; &nbsp; Número de Comprobaci&oacute;n:</td> 
		<td ><br/><?php echo$folio; ?></td>
    </tr>
    <tr>
        <td width="40%">&nbsp; &nbsp;Tipo de Comprobante*:</td>
        <td width="81%">
        <?php 
            $seleccionFa="";
            $seleccionOt="";
            $tipoBase= $elemento->getTipo();

            if($tipoBase!=null)
            {
                if($tipoBase == "fa")
                {        $seleccionFa="selected='selected'"; }
                else
                {        $seleccionOt="selected='selected'"; }
            }
        ?>
        <select name="Tipo" id="Tipo" value="<?php if($elemento!=null && $elemento->getTipo()!= null){ echo($elemento->getTipo()); }?> " >
            <option value="fa" <?php echo$seleccionFa; ?> > Factura </option>
            <option value="ot" <?php echo$seleccionOt; ?> > Otro </option>
        </select>
        </td>
        </tr>
	<tr>
		<td>&nbsp; &nbsp;Folio del Comprobante*:</td>
                <td><input type="text" name="Folio" id="Folio" size="20" value='<?php if($elemento!=null && $elemento->getFolio()!= null){ echo($elemento->getFolio()); } ?>'/></td>
	</tr>
	<tr>
            <td>&nbsp; &nbsp;Proveedor*:</td>
            <td>
                <select style="width:157px" name="proveedor" id="proveedor">
                <option value="0" >  Selecciona </option>
                <?php
                    $proveedoresDao = new ProveedorDaoJdbc();
                    $proveedoresJdbc = $proveedoresDao->obtieneListado();
                    $proveedor = new Proveedor();

                    foreach ($proveedoresJdbc as $proveedor) {

                        $sel = "";
                        if($elemento!= null && $elemento->getIdPartida()!=null )
                        {
                            if( $elemento->getIdProveedor() == $proveedor->getId() )
                            {        $sel = "selected='selected'";  }
                        echo("elemento.idpartida " . $elemento->getIdPartida());
                        }
                        echo("<option value=".$proveedor->getId()." ".$sel.">".$proveedor->getProveedor()."</option>");
                    }
                ?>
	    	</select>
            </td>
        </tr>
        <tr>
            <td>&nbsp; &nbsp; Fecha*:</td>
		<td>
                <input onblur="validar('fecha','fecha')" type="text" name="fecha" id="fecha" size=20" value='<?php if($elemento!=null && $elemento->getFecha()!= null){ echo(date("d-m-Y",strtotime($elemento->getFecha()))); }?>'/>
		</td>
	</tr>
	<tr> 
		<td>&nbsp; &nbsp; Partida*: </td>
		<td><select style="width:157px" name="Partida" id="Partida">
                <option value="0" >  Selecciona </option>
                <?php 
                    $daoPartida = new PartidaDaoJdbc();
                    $listaPartida = $daoPartida->obtieneListado();
                    $partida = new Partida();

                    foreach($listaPartida as $partida){
                        $sel = "";
                        if($elemento!= null && $elemento->getIdPartida()!= null ){
                            if( $elemento->getIdPartida() == $partida->getId() ){ $sel = "selected='selected'"; }
                    echo("elemento.idpartida " .$elemento->getIdPartida());
                            }
                            echo("<option value=".$partida->getId()." ".$sel." >" .$partida->getPartida()." ".$partida->getDescripcion()."</option>");
                    }
                    ?>
                </select>
				
		</td>
	</tr>  
	<tr>
		<td>&nbsp; &nbsp; Monto*: $</td>
                <td><input type="text" onblur="verificarMonto(<?php echo($_GET['monto']); ?>)" maxlength="20" size="20" name="Monto" id="Monto" value="<?php if($elemento!=null && $elemento->getMonto()!= null){ echo($elemento->getMonto()); } ?>" ></td>
	</tr>
	<tr>
	   <td align="left" colspan="4">&nbsp; &nbsp; Descripci&oacute;n* </td>
	</tr>
	<tr><td colspan="4">
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <textarea name="Descripcion" id="Descripcion" rows="4"  maxlength="255" cols="40" /><?php if($elemento!=null && $elemento->getDescripcion()!= null){ echo($elemento->getDescripcion()); }?></textarea>
	</td></tr>
	
	<tr>
  
	       <td align="center" colspan="3">
                <input name="guardar" style="cursor:pointer" type="submit"  value="Guardar"  class='btn' />
                &nbsp; &nbsp; &nbsp;
                <input name="cancelar" style="cursor:pointer" type="button" onclick="Regresar()"   value="Cancelar"  class='btn' />
		      
          </td>
     
	
		  
		</table>
	<?php if($elemento!=null && $id!=null) echo("<input type='hidden' name='id' value='".$id."' />");?>
	<?php if($elemento!=null && $idC!=null) echo("<input type='hidden' name='idC' value='".$idC."' />");?>
	</form>
	</div>
<br/><br/>	
</body>
<script>

	function verificarMonto(montoTotal)
    {
		var montoComprobante=document.getElementById("Monto").value;
		var che = "<?php echo$cheque ?>"

		if(montoComprobante>montoTotal)
		{
			alert("La suma de los comprobantes no puede exceder al monto del cheque que es de $ "+che+". \n\nSolo puedes comprobar $ " + montoTotal);
			document.getElementById("Monto").value=montoTotal;
		}
 	}


   function Regresar(){
	  var idC= "<?php echo$idC ?>"
	  window.location="Agregar_ComprobaINAH.php?id="+idC;
   }
   
   Calendar.setup({ inputField : "fecha", ifFormat : "%d-%m-%Y", button: "fecha" });
</script>
</html>