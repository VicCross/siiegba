<?php

class TarjetaCalendario {
    private $idCalendario;
    private $idTarjeta;
    private $num_Partida;
    private $desc_Partida;
    private $enero;
    private $febrero;
    private $marzo;
    private $abril;
    private $mayo;
    private $junio;
    private $julio;
    private $agosto;
    private $septiembre;
    private $octubre;
    private $noviembre;
    private $diciembre;
    private $idMeta;
    private $descMeta;
    
    function constructor2($idCalendario, $idTarjeta, $num_Partida, $enero, $febrero, $marzo, $abril, $mayo, $junio, $julio, $agosto, $septiembre, $octubre, $noviembre, $diciembre, $idMeta, $descMeta) {
        $this->idCalendario = $idCalendario;
        $this->idTarjeta = $idTarjeta;
        $this->num_Partida = $num_Partida;
        $this->enero = $enero;
        $this->febrero = $febrero;
        $this->marzo = $marzo;
        $this->abril = $abril;
        $this->mayo = $mayo;
        $this->junio = $junio;
        $this->julio = $julio;
        $this->agosto = $agosto;
        $this->septiembre = $septiembre;
        $this->octubre = $octubre;
        $this->noviembre = $noviembre;
        $this->diciembre = $diciembre;
        $this->idMeta = $idMeta;
        $this->descMeta = $descMeta;
    }

    function constructor3($idCalendario, $num_Partida, $desc_Partida, $enero, $febrero, $marzo, $abril, $mayo, $junio, $julio, $agosto, $septiembre, $octubre, $noviembre, $diciembre, $idMeta, $descMeta) {
        $this->idCalendario = $idCalendario;
        $this->num_Partida = $num_Partida;
        $this->desc_Partida = $desc_Partida;
        $this->enero = $enero;
        $this->febrero = $febrero;
        $this->marzo = $marzo;
        $this->abril = $abril;
        $this->mayo = $mayo;
        $this->junio = $junio;
        $this->julio = $julio;
        $this->agosto = $agosto;
        $this->septiembre = $septiembre;
        $this->octubre = $octubre;
        $this->noviembre = $noviembre;
        $this->diciembre = $diciembre;
        $this->idMeta = $idMeta;
        $this->descMeta = $descMeta;
    }

    function __construct() {
        
    }

    public function getIdCalendario() {
        return $this->idCalendario;
    }

    public function getIdTarjeta() {
        return $this->idTarjeta;
    }

    public function getNum_Partida() {
        return $this->num_Partida;
    }

    public function getDesc_Partida() {
        return $this->desc_Partida;
    }

    public function getEnero() {
        return $this->enero;
    }

    public function getFebrero() {
        return $this->febrero;
    }

    public function getMarzo() {
        return $this->marzo;
    }

    public function getAbril() {
        return $this->abril;
    }

    public function getMayo() {
        return $this->mayo;
    }

    public function getJunio() {
        return $this->junio;
    }

    public function getJulio() {
        return $this->julio;
    }

    public function getAgosto() {
        return $this->agosto;
    }

    public function getSeptiembre() {
        return $this->septiembre;
    }

    public function getOctubre() {
        return $this->octubre;
    }

    public function getNoviembre() {
        return $this->noviembre;
    }

    public function getDiciembre() {
        return $this->diciembre;
    }

    public function getIdMeta() {
        return $this->idMeta;
    }

    public function getDescMeta() {
        return $this->descMeta;
    }

    public function setIdCalendario($idCalendario) {
        $this->idCalendario = $idCalendario;
    }

    public function setIdTarjeta($idTarjeta) {
        $this->idTarjeta = $idTarjeta;
    }

    public function setNum_Partida($num_Partida) {
        $this->num_Partida = $num_Partida;
    }

    public function setDesc_Partida($desc_Partida) {
        $this->desc_Partida = $desc_Partida;
    }

    public function setEnero($enero) {
        $this->enero = $enero;
    }

    public function setFebrero($febrero) {
        $this->febrero = $febrero;
    }

    public function setMarzo($marzo) {
        $this->marzo = $marzo;
    }

    public function setAbril($abril) {
        $this->abril = $abril;
    }

    public function setMayo($mayo) {
        $this->mayo = $mayo;
    }

    public function setJunio($junio) {
        $this->junio = $junio;
    }

    public function setJulio($julio) {
        $this->julio = $julio;
    }

    public function setAgosto($agosto) {
        $this->agosto = $agosto;
    }

    public function setSeptiembre($septiembre) {
        $this->septiembre = $septiembre;
    }

    public function setOctubre($octubre) {
        $this->octubre = $octubre;
    }

    public function setNoviembre($noviembre) {
        $this->noviembre = $noviembre;
    }

    public function setDiciembre($diciembre) {
        $this->diciembre = $diciembre;
    }

    public function setIdMeta($idMeta) {
        $this->idMeta = $idMeta;
    }

    public function setDescMeta($descMeta) {
        $this->descMeta = $descMeta;
    }

}
