<?php

class NominaProy {
    private $id;
    private $idProy;
    private $desProy;
    private $responsable;
    private $periodoPago;
    private $dias;
    private $salarioDiario;
    private $importe;
    private $creditoAlSalario;
    private $bonificacion;
    private $neto;
    private $idPersonal;
    private $desPersonal;
    
    function setAll($id, $idProy, $desProy, $responsable, $periodoPago, $dias, $salarioDiario, $importe, $creditoAlSalario, $bonificacion, $neto, $idPersonal, $desPersonal) {
        $this->id = $id;
        $this->idProy = $idProy;
        $this->desProy = $desProy;
        $this->responsable = $responsable;
        $this->periodoPago = $periodoPago;
        $this->dias = $dias;
        $this->salarioDiario = $salarioDiario;
        $this->importe = $importe;
        $this->creditoAlSalario = $creditoAlSalario;
        $this->bonificacion = $bonificacion;
        $this->neto = $neto;
        $this->idPersonal = $idPersonal;
        $this->desPersonal = $desPersonal;
    }
    
    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getIdProy() {
        return $this->idProy;
    }

    public function getDesProy() {
        return $this->desProy;
    }

    public function getResponsable() {
        return $this->responsable;
    }

    public function getPeriodoPago() {
        return $this->periodoPago;
    }

    public function getDias() {
        return $this->dias;
    }

    public function getSalarioDiario() {
        return $this->salarioDiario;
    }

    public function getImporte() {
        return $this->importe;
    }

    public function getCreditoAlSalario() {
        return $this->creditoAlSalario;
    }

    public function getBonificacion() {
        return $this->bonificacion;
    }

    public function getNeto() {
        return $this->neto;
    }

    public function getIdPersonal() {
        return $this->idPersonal;
    }

    public function getDesPersonal() {
        return $this->desPersonal;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setIdProy($idProy) {
        $this->idProy = $idProy;
    }

    public function setDesProy($desProy) {
        $this->desProy = $desProy;
    }

    public function setResponsable($responsable) {
        $this->responsable = $responsable;
    }

    public function setPeriodoPago($periodoPago) {
        $this->periodoPago = $periodoPago;
    }

    public function setDias($dias) {
        $this->dias = $dias;
    }

    public function setSalarioDiario($salarioDiario) {
        $this->salarioDiario = $salarioDiario;
    }

    public function setImporte($importe) {
        $this->importe = $importe;
    }

    public function setCreditoAlSalario($creditoAlSalario) {
        $this->creditoAlSalario = $creditoAlSalario;
    }

    public function setBonificacion($bonificacion) {
        $this->bonificacion = $bonificacion;
    }

    public function setNeto($neto) {
        $this->neto = $neto;
    }

    public function setIdPersonal($idPersonal) {
        $this->idPersonal = $idPersonal;
    }

    public function setDesPersonal($desPersonal) {
        $this->desPersonal = $desPersonal;
    }

}
