<?php

class DetalleSueldoGB {
    private $id;
    private $idSueldo;
    private $folioCheque;
    private $sueldo;
    private $montoLetra;
    private $idEmpleado;
    private $estatus;
    
    function setAll($id, $idSueldo, $folioCheque, $sueldo, $montoLetra, $idEmpleado, $estatus) {
        $this->id = $id;
        $this->idSueldo = $idSueldo;
        $this->folioCheque = $folioCheque;
        $this->sueldo = $sueldo;
        $this->montoLetra = $montoLetra;
        $this->idEmpleado = $idEmpleado;
        $this->estatus = $estatus;
    }

    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getIdSueldo() {
        return $this->idSueldo;
    }

    public function getFolioCheque() {
        return $this->folioCheque;
    }

    public function getSueldo() {
        return $this->sueldo;
    }

    public function getMontoLetra() {
        return $this->montoLetra;
    }

    public function getIdEmpleado() {
        return $this->idEmpleado;
    }

    public function getEstatus() {
        return $this->estatus;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setIdSueldo($idSueldo) {
        $this->idSueldo = $idSueldo;
    }

    public function setFolioCheque($folioCheque) {
        $this->folioCheque = $folioCheque;
    }

    public function setSueldo($sueldo) {
        $this->sueldo = $sueldo;
    }

    public function setMontoLetra($montoLetra) {
        $this->montoLetra = $montoLetra;
    }

    public function setIdEmpleado($idEmpleado) {
        $this->idEmpleado = $idEmpleado;
    }

    public function setEstatus($estatus) {
        $this->estatus = $estatus;
    }


}
