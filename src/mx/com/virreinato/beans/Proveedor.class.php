<?php

class Proveedor {
    
    private $id;
	private $clave;
	private $beneficiario;
    private $cuenta;
    private $unidadCTA;
    private $activoBEN;
    private $estatusCTA;
    private $curp;
    private $claveBanco;
	private $banco;
	private $sicop;
    
    
    function setAll($id, $clave, $beneficiario, $cuenta, $unidadCTA, $activoBEN, $estatusCTA, $curp, $claveBanco, $banco, $sicop) {
        $this->id = $id;
        $this->clave = $clave;
        $this->beneficiario = $beneficiario;
        $this->cuenta = $cuenta;
        $this->unidadCTA = $unidadCTA;
        $this->activoBEN = $activoBEN;
        $this->estatusCTA = $estatusCTA;
        $this->curp = $curp;
        $this->claveBanco = $claveBanco;
		$this->banco = $banco;
		$this->sicop = $sicop;
    }

    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getClave() {
        return $this->clave;
    }

    public function getBeneficiario() {
        return $this->beneficiario;
    }

    public function getCuenta() {
        return $this->cuenta;
    }

    public function getUnidadCTA() {
        return $this->unidadCTA;
    }

    public function getActivoBEN() {
        return $this->activoBEN;
    }

    public function getEstatusCTA() {
        return $this->estatusCTA;
    }

    public function getCurp() {
        return $this->curp;
    }

    public function getClaveBanco() {
        return $this->claveBanco;
    }
	
	public function getBanco() {
        return $this->banco;
    }
	
	public function getSicop() {
        return $this->sicop;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setClave($clave) {
        $this->clave = $clave;
    }

    public function setBeneficiario($beneficiario) {
        $this->beneficiario = $beneficiario;
    }

    public function setCuenta($cuenta) {
        $this->cuenta = $cuenta;
    }

    public function setUnidadCTA($unidadCTA) {
        $this->unidadCTA = $unidadCTA;
    }

    public function setActivoBEN($activoBEN) {
        $this->activoBEN = $activoBEN;
    }
	
	public function setEstatusCTA($estatusCTA) {
        $this->estatusCTA = $estatusCTA;
    }

    public function setCurp($curp) {
        $this->curp = $curp;
    }

    public function setClaveBanco($claveBanco) {
        $this->claveBanco = $claveBanco;
    }

    public function setBanco($banco) {
        $this->banco = $banco;
    }
	
	public function setSicop($sicop){
		$this->sicop = $sicop;
	}
	
	
}
