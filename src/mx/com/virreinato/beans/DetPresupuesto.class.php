<?php

class DetPresupuesto {
    private $id;
    private $idPresupuesto;
    private $idActiviad;
    private $idPartida;
    private $monto;
    private $productos_obtenidos;
    private $detalle_productos;
    private $Actividad;
    private $Partida;
    private $Proyecto;
    private $Meta;
    private $programaInstitucional;
    private $programaOperativo;
    
    function constructor1($id, $idPresupuesto, $idActiviad, $idPartida, $monto, $productos_obtenidos, $detalle_productos) {
        $this->id = $id;
        $this->idPresupuesto = $idPresupuesto;
        $this->idActiviad = $idActiviad;
        $this->idPartida = $idPartida;
        $this->monto = $monto;
        $this->productos_obtenidos = $productos_obtenidos;
        $this->detalle_productos = $detalle_productos;
    }
    
    function constructor2($id, $idPresupuesto, $idActiviad, $monto, $productos_obtenidos, $detalle_productos, $programaInstitucional, $programaOperativo) {
        $this->id = $id;
        $this->idPresupuesto = $idPresupuesto;
        $this->idActiviad = $idActiviad;
        $this->monto = $monto;
        $this->productos_obtenidos = $productos_obtenidos;
        $this->detalle_productos = $detalle_productos;
        $this->programaInstitucional = $programaInstitucional;
        $this->programaOperativo = $programaOperativo;
    }

    function constructor3($id, $idPresupuesto, $monto, $productos_obtenidos, $detalle_productos, $Actividad, $Partida, $Proyecto, $Meta) {
        $this->id = $id;
        $this->idPresupuesto = $idPresupuesto;
        $this->monto = $monto;
        $this->productos_obtenidos = $productos_obtenidos;
        $this->detalle_productos = $detalle_productos;
        $this->Actividad = $Actividad;
        $this->Partida = $Partida;
        $this->Proyecto = $Proyecto;
        $this->Meta = $Meta;
    }

    function constructor4($id, $idPresupuesto, $monto, $productos_obtenidos, $detalle_productos, $Actividad, $Partida, $Proyecto, $Meta, $programaInstitucional, $programaOperativo) {
        $this->id = $id;
        $this->idPresupuesto = $idPresupuesto;
        $this->monto = $monto;
        $this->productos_obtenidos = $productos_obtenidos;
        $this->detalle_productos = $detalle_productos;
        $this->Actividad = $Actividad;
        $this->Partida = $Partida;
        $this->Proyecto = $Proyecto;
        $this->Meta = $Meta;
        $this->programaInstitucional = $programaInstitucional;
        $this->programaOperativo = $programaOperativo;
    }

    public function getId() {
        return $this->id;
    }

    public function getIdPresupuesto() {
        return $this->idPresupuesto;
    }

    public function getIdActiviad() {
        return $this->idActiviad;
    }

    public function getIdPartida() {
        return $this->idPartida;
    }

    public function getMonto() {
        return $this->monto;
    }

    public function getProductos_obtenidos() {
        return $this->productos_obtenidos;
    }

    public function getDetalle_productos() {
        return $this->detalle_productos;
    }

    public function getActividad() {
        return $this->Actividad;
    }

    public function getPartida() {
        return $this->Partida;
    }

    public function getProyecto() {
        return $this->Proyecto;
    }

    public function getMeta() {
        return $this->Meta;
    }

    public function getProgramaInstitucional() {
        return $this->programaInstitucional;
    }

    public function getProgramaOperativo() {
        return $this->programaOperativo;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setIdPresupuesto($idPresupuesto) {
        $this->idPresupuesto = $idPresupuesto;
    }

    public function setIdActiviad($idActiviad) {
        $this->idActiviad = $idActiviad;
    }

    public function setIdPartida($idPartida) {
        $this->idPartida = $idPartida;
    }

    public function setMonto($monto) {
        $this->monto = $monto;
    }

    public function setProductos_obtenidos($productos_obtenidos) {
        $this->productos_obtenidos = $productos_obtenidos;
    }

    public function setDetalle_productos($detalle_productos) {
        $this->detalle_productos = $detalle_productos;
    }

    public function setActividad($Actividad) {
        $this->Actividad = $Actividad;
    }

    public function setPartida($Partida) {
        $this->Partida = $Partida;
    }

    public function setProyecto($Proyecto) {
        $this->Proyecto = $Proyecto;
    }

    public function setMeta($Meta) {
        $this->Meta = $Meta;
    }

    public function setProgramaInstitucional($programaInstitucional) {
        $this->programaInstitucional = $programaInstitucional;
    }

    public function setProgramaOperativo($programaOperativo) {
        $this->programaOperativo = $programaOperativo;
    }


    
}
