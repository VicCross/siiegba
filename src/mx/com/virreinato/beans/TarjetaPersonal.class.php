<?php

class TarjetaPersonal {
    private $idPersonal;
    private $idTarjeta;
    private $num_Personal;
    private $nombre;
    private $grado;
    private $puesto;
    private $instituciones;
    private $asignado;
    
    function setAll($idPersonal, $idTarjeta, $num_Personal, $nombre, $grado, $puesto, $instituciones, $asignado) {
        $this->idPersonal = $idPersonal;
        $this->idTarjeta = $idTarjeta;
        $this->num_Personal = $num_Personal;
        $this->nombre = $nombre;
        $this->grado = $grado;
        $this->puesto = $puesto;
        $this->instituciones = $instituciones;
        $this->asignado = $asignado;
    }

    function constructor2($idPersonal, $num_Personal, $nombre, $grado, $puesto, $instituciones, $asignado) {
        $this->idPersonal = $idPersonal;
        $this->num_Personal = $num_Personal;
        $this->nombre = $nombre;
        $this->grado = $grado;
        $this->puesto = $puesto;
        $this->instituciones = $instituciones;
        $this->asignado = $asignado;
    }

    function __construct() {
        
    }
    
    public function getIdPersonal() {
        return $this->idPersonal;
    }

    public function getIdTarjeta() {
        return $this->idTarjeta;
    }

    public function getNum_Personal() {
        return $this->num_Personal;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function getGrado() {
        return $this->grado;
    }

    public function getPuesto() {
        return $this->puesto;
    }

    public function getInstituciones() {
        return $this->instituciones;
    }

    public function getAsignado() {
        return $this->asignado;
    }

    public function setIdPersonal($idPersonal) {
        $this->idPersonal = $idPersonal;
    }

    public function setIdTarjeta($idTarjeta) {
        $this->idTarjeta = $idTarjeta;
    }

    public function setNum_Personal($num_Personal) {
        $this->num_Personal = $num_Personal;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function setGrado($grado) {
        $this->grado = $grado;
    }

    public function setPuesto($puesto) {
        $this->puesto = $puesto;
    }

    public function setInstituciones($instituciones) {
        $this->instituciones = $instituciones;
    }

    public function setAsignado($asignado) {
        $this->asignado = $asignado;
    }
}
