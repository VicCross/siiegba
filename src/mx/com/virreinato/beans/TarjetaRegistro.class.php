<?php

class TarjetaRegistro {
    private $id;
    private $Responsable;
    private $nombreProyecto;
    private $Meta;
    private $AreaNormativa;
    private $lineaAccion;
    private $num_Proyecto;
    private $costoTotal;
    private $FechaInicio_Total;
    private $FechaFin_Total;
    private $fechaInicio_Presente;
    private $fechaFin_Presente;
    private $unidadAdministrativa;
    private $idMeta;
    private $idProyecto;
    private $claveTecnica;
    private $claveAccion;
    private $autorizadoSinFinanciamiento;
    private $funcion;
    private $subFuncion;
    private $programaGeneral;
    private $actividadInstitucional;
    private $actividadPrioritaria;
    private $tipoProyecto;
    private $concluir;
    private $reactivado;
    private $Objetivo;
    private $productoPrincipal;
    private $acumulado;
    private $programado;
    private $recursos_Terceros;
    private $del_Fase1;
    private $del_Fase2;
    private $del_Fase3;
    private $del_Fase4;
    private $del_Fase5;
    private $del_Fase6;
    private $al_Fase1;
    private $al_Fase2;
    private $al_Fase3;
    private $al_Fase4;
    private $al_Fase5;
    private $al_Fase6;
    private $costoTotal_Modificado;
    private $productoPrincipal_Modificado;
    private $lineaAccion_Modificado;
    private $nombreProyecto_Modificado;
    private $permanente_Modificado;
    private $concluir_Modificado;
    private $anio_Modificacion;
    private $al_Modificacion;
    private $fechaElaboracion;
    private $fechaAutorizacion;
    private $elabora;
    private $propone;
    private $autoriza_Tecnico;
    private $autoriza_Adminstrativo;
    private $Justificacion;
    
    function constructor1($id, $Responsable, $nombreProyecto, $Meta) {
        $this->id = $id;
        $this->Responsable = $Responsable;
        $this->nombreProyecto = $nombreProyecto;
        $this->Meta = $Meta;
    }

    function constructor2($AreaNormativa, $lineaAccion, $num_Proyecto, $costoTotal, $FechaInicio_Total, $FechaFin_Total, $fechaInicio_Presente, $fechaFin_Presente) {
        $this->AreaNormativa = $AreaNormativa;
        $this->lineaAccion = $lineaAccion;
        $this->num_Proyecto = $num_Proyecto;
        $this->costoTotal = $costoTotal;
        $this->FechaInicio_Total = $FechaInicio_Total;
        $this->FechaFin_Total = $FechaFin_Total;
        $this->fechaInicio_Presente = $fechaInicio_Presente;
        $this->fechaFin_Presente = $fechaFin_Presente;
    }

    function constructor3($id, $Responsable, $unidadAdministrativa, $idMeta, $idProyecto, $claveTecnica, $claveAccion, $autorizadoSinFinanciamiento, $funcion, $subFuncion, $programaGeneral, $actividadInstitucional, $actividadPrioritaria, $tipoProyecto, $concluir, $reactivado, $Objetivo, $productoPrincipal, $acumulado, $programado, $recursos_Terceros, $del_Fase1, $del_Fase2, $del_Fase3, $del_Fase4, $del_Fase5, $del_Fase6, $al_Fase1, $al_Fase2, $al_Fase3, $al_Fase4, $al_Fase5, $al_Fase6, $costoTotal_Modificado, $productoPrincipal_Modificado, $lineaAccion_Modificado, $nombreProyecto_Modificado, $permanente_Modificado, $concluir_Modificado, $anio_Modificacion, $al_Modificacion, $fechaElaboracion, $fechaAutorizacion, $elabora, $propone, $autoriza_Tecnico, $autoriza_Adminstrativo, $Justificacion) {
        $this->id = $id;
        $this->Responsable = $Responsable;
        $this->unidadAdministrativa = $unidadAdministrativa;
        $this->idMeta = $idMeta;
        $this->idProyecto = $idProyecto;
        $this->claveTecnica = $claveTecnica;
        $this->claveAccion = $claveAccion;
        $this->autorizadoSinFinanciamiento = $autorizadoSinFinanciamiento;
        $this->funcion = $funcion;
        $this->subFuncion = $subFuncion;
        $this->programaGeneral = $programaGeneral;
        $this->actividadInstitucional = $actividadInstitucional;
        $this->actividadPrioritaria = $actividadPrioritaria;
        $this->tipoProyecto = $tipoProyecto;
        $this->concluir = $concluir;
        $this->reactivado = $reactivado;
        $this->Objetivo = $Objetivo;
        $this->productoPrincipal = $productoPrincipal;
        $this->acumulado = $acumulado;
        $this->programado = $programado;
        $this->recursos_Terceros = $recursos_Terceros;
        $this->del_Fase1 = $del_Fase1;
        $this->del_Fase2 = $del_Fase2;
        $this->del_Fase3 = $del_Fase3;
        $this->del_Fase4 = $del_Fase4;
        $this->del_Fase5 = $del_Fase5;
        $this->del_Fase6 = $del_Fase6;
        $this->al_Fase1 = $al_Fase1;
        $this->al_Fase2 = $al_Fase2;
        $this->al_Fase3 = $al_Fase3;
        $this->al_Fase4 = $al_Fase4;
        $this->al_Fase5 = $al_Fase5;
        $this->al_Fase6 = $al_Fase6;
        $this->costoTotal_Modificado = $costoTotal_Modificado;
        $this->productoPrincipal_Modificado = $productoPrincipal_Modificado;
        $this->lineaAccion_Modificado = $lineaAccion_Modificado;
        $this->nombreProyecto_Modificado = $nombreProyecto_Modificado;
        $this->permanente_Modificado = $permanente_Modificado;
        $this->concluir_Modificado = $concluir_Modificado;
        $this->anio_Modificacion = $anio_Modificacion;
        $this->al_Modificacion = $al_Modificacion;
        $this->fechaElaboracion = $fechaElaboracion;
        $this->fechaAutorizacion = $fechaAutorizacion;
        $this->elabora = $elabora;
        $this->propone = $propone;
        $this->autoriza_Tecnico = $autoriza_Tecnico;
        $this->autoriza_Adminstrativo = $autoriza_Adminstrativo;
        $this->Justificacion = $Justificacion;
    }

    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getResponsable() {
        return $this->Responsable;
    }

    public function getNombreProyecto() {
        return $this->nombreProyecto;
    }

    public function getMeta() {
        return $this->Meta;
    }

    public function getAreaNormativa() {
        return $this->AreaNormativa;
    }

    public function getLineaAccion() {
        return $this->lineaAccion;
    }

    public function getNum_Proyecto() {
        return $this->num_Proyecto;
    }

    public function getCostoTotal() {
        return $this->costoTotal;
    }

    public function getFechaInicio_Total() {
        return $this->FechaInicio_Total;
    }

    public function getFechaFin_Total() {
        return $this->FechaFin_Total;
    }

    public function getFechaInicio_Presente() {
        return $this->fechaInicio_Presente;
    }

    public function getFechaFin_Presente() {
        return $this->fechaFin_Presente;
    }

    public function getUnidadAdministrativa() {
        return $this->unidadAdministrativa;
    }

    public function getIdMeta() {
        return $this->idMeta;
    }

    public function getIdProyecto() {
        return $this->idProyecto;
    }

    public function getClaveTecnica() {
        return $this->claveTecnica;
    }

    public function getClaveAccion() {
        return $this->claveAccion;
    }

    public function getAutorizadoSinFinanciamiento() {
        return $this->autorizadoSinFinanciamiento;
    }

    public function getFuncion() {
        return $this->funcion;
    }

    public function getSubFuncion() {
        return $this->subFuncion;
    }

    public function getProgramaGeneral() {
        return $this->programaGeneral;
    }

    public function getActividadInstitucional() {
        return $this->actividadInstitucional;
    }

    public function getActividadPrioritaria() {
        return $this->actividadPrioritaria;
    }

    public function getTipoProyecto() {
        return $this->tipoProyecto;
    }

    public function getConcluir() {
        return $this->concluir;
    }

    public function getReactivado() {
        return $this->reactivado;
    }

    public function getObjetivo() {
        return $this->Objetivo;
    }

    public function getProductoPrincipal() {
        return $this->productoPrincipal;
    }

    public function getAcumulado() {
        return $this->acumulado;
    }

    public function getProgramado() {
        return $this->programado;
    }

    public function getRecursos_Terceros() {
        return $this->recursos_Terceros;
    }

    public function getDel_Fase1() {
        return $this->del_Fase1;
    }

    public function getDel_Fase2() {
        return $this->del_Fase2;
    }

    public function getDel_Fase3() {
        return $this->del_Fase3;
    }

    public function getDel_Fase4() {
        return $this->del_Fase4;
    }

    public function getDel_Fase5() {
        return $this->del_Fase5;
    }

    public function getDel_Fase6() {
        return $this->del_Fase6;
    }

    public function getAl_Fase1() {
        return $this->al_Fase1;
    }

    public function getAl_Fase2() {
        return $this->al_Fase2;
    }

    public function getAl_Fase3() {
        return $this->al_Fase3;
    }

    public function getAl_Fase4() {
        return $this->al_Fase4;
    }

    public function getAl_Fase5() {
        return $this->al_Fase5;
    }

    public function getAl_Fase6() {
        return $this->al_Fase6;
    }

    public function getCostoTotal_Modificado() {
        return $this->costoTotal_Modificado;
    }

    public function getProductoPrincipal_Modificado() {
        return $this->productoPrincipal_Modificado;
    }

    public function getLineaAccion_Modificado() {
        return $this->lineaAccion_Modificado;
    }

    public function getNombreProyecto_Modificado() {
        return $this->nombreProyecto_Modificado;
    }

    public function getPermanente_Modificado() {
        return $this->permanente_Modificado;
    }

    public function getConcluir_Modificado() {
        return $this->concluir_Modificado;
    }

    public function getAnio_Modificacion() {
        return $this->anio_Modificacion;
    }

    public function getAl_Modificacion() {
        return $this->al_Modificacion;
    }

    public function getFechaElaboracion() {
        return $this->fechaElaboracion;
    }

    public function getFechaAutorizacion() {
        return $this->fechaAutorizacion;
    }

    public function getElabora() {
        return $this->elabora;
    }

    public function getPropone() {
        return $this->propone;
    }

    public function getAutoriza_Tecnico() {
        return $this->autoriza_Tecnico;
    }

    public function getAutoriza_Adminstrativo() {
        return $this->autoriza_Adminstrativo;
    }

    public function getJustificacion() {
        return $this->Justificacion;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setResponsable($Responsable) {
        $this->Responsable = $Responsable;
    }

    public function setNombreProyecto($nombreProyecto) {
        $this->nombreProyecto = $nombreProyecto;
    }

    public function setMeta($Meta) {
        $this->Meta = $Meta;
    }

    public function setAreaNormativa($AreaNormativa) {
        $this->AreaNormativa = $AreaNormativa;
    }

    public function setLineaAccion($lineaAccion) {
        $this->lineaAccion = $lineaAccion;
    }

    public function setNum_Proyecto($num_Proyecto) {
        $this->num_Proyecto = $num_Proyecto;
    }

    public function setCostoTotal($costoTotal) {
        $this->costoTotal = $costoTotal;
    }

    public function setFechaInicio_Total($FechaInicio_Total) {
        $this->FechaInicio_Total = $FechaInicio_Total;
    }

    public function setFechaFin_Total($FechaFin_Total) {
        $this->FechaFin_Total = $FechaFin_Total;
    }

    public function setFechaInicio_Presente($fechaInicio_Presente) {
        $this->fechaInicio_Presente = $fechaInicio_Presente;
    }

    public function setFechaFin_Presente($fechaFin_Presente) {
        $this->fechaFin_Presente = $fechaFin_Presente;
    }

    public function setUnidadAdministrativa($unidadAdministrativa) {
        $this->unidadAdministrativa = $unidadAdministrativa;
    }

    public function setIdMeta($idMeta) {
        $this->idMeta = $idMeta;
    }

    public function setIdProyecto($idProyecto) {
        $this->idProyecto = $idProyecto;
    }

    public function setClaveTecnica($claveTecnica) {
        $this->claveTecnica = $claveTecnica;
    }

    public function setClaveAccion($claveAccion) {
        $this->claveAccion = $claveAccion;
    }

    public function setAutorizadoSinFinanciamiento($autorizadoSinFinanciamiento) {
        $this->autorizadoSinFinanciamiento = $autorizadoSinFinanciamiento;
    }

    public function setFuncion($funcion) {
        $this->funcion = $funcion;
    }

    public function setSubFuncion($subFuncion) {
        $this->subFuncion = $subFuncion;
    }

    public function setProgramaGeneral($programaGeneral) {
        $this->programaGeneral = $programaGeneral;
    }

    public function setActividadInstitucional($actividadInstitucional) {
        $this->actividadInstitucional = $actividadInstitucional;
    }

    public function setActividadPrioritaria($actividadPrioritaria) {
        $this->actividadPrioritaria = $actividadPrioritaria;
    }

    public function setTipoProyecto($tipoProyecto) {
        $this->tipoProyecto = $tipoProyecto;
    }

    public function setConcluir($concluir) {
        $this->concluir = $concluir;
    }

    public function setReactivado($reactivado) {
        $this->reactivado = $reactivado;
    }

    public function setObjetivo($Objetivo) {
        $this->Objetivo = $Objetivo;
    }

    public function setProductoPrincipal($productoPrincipal) {
        $this->productoPrincipal = $productoPrincipal;
    }

    public function setAcumulado($acumulado) {
        $this->acumulado = $acumulado;
    }

    public function setProgramado($programado) {
        $this->programado = $programado;
    }

    public function setRecursos_Terceros($recursos_Terceros) {
        $this->recursos_Terceros = $recursos_Terceros;
    }

    public function setDel_Fase1($del_Fase1) {
        $this->del_Fase1 = $del_Fase1;
    }

    public function setDel_Fase2($del_Fase2) {
        $this->del_Fase2 = $del_Fase2;
    }

    public function setDel_Fase3($del_Fase3) {
        $this->del_Fase3 = $del_Fase3;
    }

    public function setDel_Fase4($del_Fase4) {
        $this->del_Fase4 = $del_Fase4;
    }

    public function setDel_Fase5($del_Fase5) {
        $this->del_Fase5 = $del_Fase5;
    }

    public function setDel_Fase6($del_Fase6) {
        $this->del_Fase6 = $del_Fase6;
    }

    public function setAl_Fase1($al_Fase1) {
        $this->al_Fase1 = $al_Fase1;
    }

    public function setAl_Fase2($al_Fase2) {
        $this->al_Fase2 = $al_Fase2;
    }

    public function setAl_Fase3($al_Fase3) {
        $this->al_Fase3 = $al_Fase3;
    }

    public function setAl_Fase4($al_Fase4) {
        $this->al_Fase4 = $al_Fase4;
    }

    public function setAl_Fase5($al_Fase5) {
        $this->al_Fase5 = $al_Fase5;
    }

    public function setAl_Fase6($al_Fase6) {
        $this->al_Fase6 = $al_Fase6;
    }

    public function setCostoTotal_Modificado($costoTotal_Modificado) {
        $this->costoTotal_Modificado = $costoTotal_Modificado;
    }

    public function setProductoPrincipal_Modificado($productoPrincipal_Modificado) {
        $this->productoPrincipal_Modificado = $productoPrincipal_Modificado;
    }

    public function setLineaAccion_Modificado($lineaAccion_Modificado) {
        $this->lineaAccion_Modificado = $lineaAccion_Modificado;
    }

    public function setNombreProyecto_Modificado($nombreProyecto_Modificado) {
        $this->nombreProyecto_Modificado = $nombreProyecto_Modificado;
    }

    public function setPermanente_Modificado($permanente_Modificado) {
        $this->permanente_Modificado = $permanente_Modificado;
    }

    public function setConcluir_Modificado($concluir_Modificado) {
        $this->concluir_Modificado = $concluir_Modificado;
    }

    public function setAnio_Modificacion($anio_Modificacion) {
        $this->anio_Modificacion = $anio_Modificacion;
    }

    public function setAl_Modificacion($al_Modificacion) {
        $this->al_Modificacion = $al_Modificacion;
    }

    public function setFechaElaboracion($fechaElaboracion) {
        $this->fechaElaboracion = $fechaElaboracion;
    }

    public function setFechaAutorizacion($fechaAutorizacion) {
        $this->fechaAutorizacion = $fechaAutorizacion;
    }

    public function setElabora($elabora) {
        $this->elabora = $elabora;
    }

    public function setPropone($propone) {
        $this->propone = $propone;
    }

    public function setAutoriza_Tecnico($autoriza_Tecnico) {
        $this->autoriza_Tecnico = $autoriza_Tecnico;
    }

    public function setAutoriza_Adminstrativo($autoriza_Adminstrativo) {
        $this->autoriza_Adminstrativo = $autoriza_Adminstrativo;
    }

    public function setJustificacion($Justificacion) {
        $this->Justificacion = $Justificacion;
    }


}
