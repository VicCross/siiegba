<?php

class Banco {
    
    private $id;
    private $descripcion;
    private $observaciones;
    
    function setAll($id, $descripcion, $observaciones) {
        $this->id = $id;
        $this->descripcion = $descripcion;
        $this->observaciones = $observaciones;
    }
    
    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function getObservaciones() {
        return $this->observaciones;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function setObservaciones($observaciones) {
        $this->observaciones = $observaciones;
    }

}
