<?php

class LineaAccion {
    private $Id;
    private $eje;
    private $lineaAccion;
    private $descripcion;
	private $estatus;
	private $idLider;
	private $firma;
	private $saldoInicial;
	private $saldoAnterior;
	private $periodo;
	private $inba;
	private $patronato;
	private $pagado;
	private	$pinba;
	private	$ppatronato;
	private	$pexterno;
    
    function setAll($Id, $eje, $lineaAccion, $descripcion, $estatus, $idLider, $firma, $saldoInicial, $saldoAnterior,$periodo,$inba,$patronato,$pagado,$pinba,$ppatronato,$pexterno) 
		{
        $this->Id = $Id;
        $this->eje = $eje;
        $this->lineaAccion = $lineaAccion;
        $this->descripcion = $descripcion;
		$this->estatus = $estatus;
		$this->idLider = $idLider;
		$this->firma = $firma;
		$this->saldoInicial = $saldoInicial;
		$this->saldoAnterior = $saldoAnterior;
		$this->periodo = $periodo;
		$this->inba = $inba;
		$this->patronato = $patronato;
		$this->pagado=$pagado;
		$this->pinba=$pinba;
		$this->ppatronato=$ppatronato;
		$this->pexterno=$pexterno;
		}

    function __construct() {
        
    }

    public function getId() {
        return $this->Id;
    }

    public function getEje() {
        return $this->eje;
    }

    public function getLineaAccion() {
        return $this->lineaAccion;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }
	
	public function getEstatus(){
		return $this->estatus;	
	}
	
	public function getIdLider(){
		return $this->idLider;	
	}
	
	public function getFirma(){
		return $this->firma;	
	}
	
	public function getSaldoInicial(){
		return $this->saldoInicial;
	}
	
	public function getSaldoAnterior(){
		return $this->saldoAnterior;	
	}
	
	public function getPeriodo(){
		return $this->periodo;	
	}
	
	public function getInba(){
		return $this->inba;	
	}
	
	public function getPatronato(){
		return $this->patronato;	
	}
	
	public function getPagado(){
		return $this->pagado;	
	}
	
	public function getPinba(){
		return $this->pinba;	
	}
	public function getPpatronato(){
		return $this->ppatronato;	
	}
	public function getPexterno(){
		return $this->pexterno;	
	}
	
	public function setSaldoInicial($saldoInicial){
		$this->saldoInicial = $saldoInicial;	
	}
	
	public function setSaldoAnterior($saldoAnterior){
		$this->saldoAnterior = $saldoAnterior;	
	}

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setEje($eje) {
        $this->eje = $eje;
    }

    public function setLineaAccion($lineaAccion) {
        $this->lineaAccion = $lineaAccion;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }
	
	public function setEstatus($estatus){
		$this->estatus = $estatus;	
	}
	
	public function setIdLider($idLider){
		$this->idLider = $idLider;	
	}
	
	public function setFirma($firma){
		$this->firma = $firma;	
	}
	
	public function setPeriodo($periodo){
		$this->firma = $periodo;	
	}
	
	public function setInba($inba){
		$this->inba = $inba;	
	}
	
	public function setPatronato($patronato){
		$this->patronato = $patronato;	
	}
	public function setPagado($pagado){
		$this->pagado=$pagado;
		
	}
	public function setPinba($pinba){
		$this->pinba=$pinba;

	}
	public function setPpatronato($ppatronato){
		$this->ppatronato=$ppatronato;
				
	}
	public function setPexterno($pexterno){
		$this->pexterno=$pexterno;
		
	}
}
