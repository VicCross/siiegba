<?php
	class GestionPresupuestal
	{
		private $id;
		private $folio;
		private $fechaSol;
		private $id_area;
		private $id_proyecto;	
		private $id_expotemp;
		private $estatus;
		private $importe;
		private $meta;
		private $submeta;
		private $numFactura;
		private $conceptoFactura;
		private $beneficiario;
		private $importeLetra;
		private $entregable;
		private $entregable_desc;
		private $reporte;
		
		function setAll($id, $folio, $fechaSol, $id_area, $id_proyecto, $id_expotemp, $estatus, $importe, $meta, $submeta, $numFactura, $conceptoFactura, $beneficiario, $importeLetra, $entregable, $entregable_desc, $reporte)
		{
			$this->id = $id;
			$this->folio = $folio;
			$this->fechaSol = $fechaSol;
			$this->id_area = $id_area;
			$this->id_proyecto = $id_proyecto;
			$this->id_expotemp = $id_expotemp;
			$this->estatus = $estatus;
			$this->importe = $importe;
			$this->meta = $meta;
			$this->submeta = $submeta;
			$this->numFactura = $numFactura;
			$this->conceptoFactura = $conceptoFactura;
			$this->beneficiario = $beneficiario;
			$this->importeLetra = $importeLetra;
			$this->entregable = $entregable;
			$this->entregable_desc = $entregable_desc;
			$this->reporte = $reporte;
		}
		
		function __construct(){
			
		}
		
		public function getId(){
			return $this->id;	
		}
		
		public function getFolio(){
			return $this->folio;	
		}
		
		public function getIdArea(){
			return $this->id_area;	
		}
		
		public function getIdProyecto(){
			return $this->id_proyecto;	
		}
		
		public function getIdExpoTemp(){
			return $this->id_expotemp;	
		}
		
		public function getEstatus(){
			return $this->estatus;
		}
		
		public function getImporte(){
			return $this->importe;
		}
		
		public function getMeta(){
			return $this->meta;
		}
		
		public function getSubmeta(){
			return $this->submeta;
		}
		
		public function getNumFactura(){
			return $this->numFactura;
		}
		
		public function getConceptoFactura(){
			return $this->conceptoFactura;
		}
		
		public function getBeneficiario(){
			return $this->beneficiario;
		}
		
		public function getImporteLetra(){
			return $this->importeLetra;			
		}
		
		public function getEntregable(){
			return $this->entregable;
		}
		
		public function getEntregableDesc(){
			return $this->entregable_desc;
		}
		
		public function getFechaSol(){
			return $this->fechaSol;	
		}
		
		public function getReporte(){
			return $this->reporte;	
		}
		
		public function setReporte($reporte){
			$this->reporte = $reporte;	
		}
		
		public function setFechaSol($fechaSol){
			$this->fechaSol = $fechaSol;	
		}
		
		public function setId($id){
			$this->id = $id;
		}
		
		public function setFolio($folio){
			$this->folio = $folio;
		}
		
		public function setIdArea($id_area){
			$this->id_area = $id_area;
		}
		
		public function setIdProyecto($id_proyecto){
			$this->id_proyecto = $id_proyecto;
		}
		
		public function setIdExpoTemp($id_expotemp){
			$this->id_expotemp = $id_expotemp;
		}
		
		public function setEstatus($estatus){
			$this->estatus = $estatus;
		}
		
		public function setImporte($importe){
			$this->importe = $importe;
		}
		
		public function setMeta($meta){
			$this->meta = $meta;
		}
		
		public function setSubmeta($submeta){
			$this->submeta = $submeta;
		}
		
		public function setNumFactura($numFactura){
			$this->numFactura = $numFactura;
		}
		
		public function setConceptoFactura($conceptoFactura){
			$this->conceptoFactura = $conceptoFactura;
		}
		
		public function setBeneficiario($beneficiario){
			$this->beneficiario = $beneficiario;
		}
		
		public function setImporteLetra($importeLetra){
			$this->importeLetra = $importeLetra;
		}
		
		public function setEntregable($entregable){
			$this->entregable = $entregable;
		}
		
		public function setEntregableDesc($entregable_desc){
			$this->entregable_desc = $entregable_desc;
		}
	}
?>