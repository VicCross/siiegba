<?php

class CuentaBancaria {
    private $id;
    private $banco;
    private $numeroCuenta;
    private $tipoCuenta;
    private $clabe;
    private $sucursal;
    private $ejecutivoCuenta;
    private $ejecutivoTel;
    private $ejecutivoCorreo;
    private $descripcion;
    
    function setAll($id, $banco, $numeroCuenta, $tipoCuenta, $clabe, $sucursal, $ejecutivoCuenta, $ejecutivoTel, $ejecutivoCorreo, $descripcion) {
        $this->id = $id;
        $this->banco = $banco;
        $this->numeroCuenta = $numeroCuenta;
        $this->tipoCuenta = $tipoCuenta;
        $this->clabe = $clabe;
        $this->sucursal = $sucursal;
        $this->ejecutivoCuenta = $ejecutivoCuenta;
        $this->ejecutivoTel = $ejecutivoTel;
        $this->ejecutivoCorreo = $ejecutivoCorreo;
        $this->descripcion = $descripcion;
    }
    
    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getBanco() {
        return $this->banco;
    }

    public function getNumeroCuenta() {
        return $this->numeroCuenta;
    }

    public function getTipoCuenta() {
        return $this->tipoCuenta;
    }

    public function getClabe() {
        return $this->clabe;
    }

    public function getSucursal() {
        return $this->sucursal;
    }

    public function getEjecutivoCuenta() {
        return $this->ejecutivoCuenta;
    }

    public function getEjecutivoTel() {
        return $this->ejecutivoTel;
    }

    public function getEjecutivoCorreo() {
        return $this->ejecutivoCorreo;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setBanco($banco) {
        $this->banco = $banco;
    }

    public function setNumeroCuenta($numeroCuenta) {
        $this->numeroCuenta = $numeroCuenta;
    }

    public function setTipoCuenta($tipoCuenta) {
        $this->tipoCuenta = $tipoCuenta;
    }

    public function setClabe($clabe) {
        $this->clabe = $clabe;
    }

    public function setSucursal($sucursal) {
        $this->sucursal = $sucursal;
    }

    public function setEjecutivoCuenta($ejecutivoCuenta) {
        $this->ejecutivoCuenta = $ejecutivoCuenta;
    }

    public function setEjecutivoTel($ejecutivoTel) {
        $this->ejecutivoTel = $ejecutivoTel;
    }

    public function setEjecutivoCorreo($ejecutivoCorreo) {
        $this->ejecutivoCorreo = $ejecutivoCorreo;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }


}
