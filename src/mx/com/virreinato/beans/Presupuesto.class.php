<?php

class Presupuesto {
    private $id;
    private $fecha;
    private $idProyecto;
    private $idMeta;
    private $idEmpleado;
    private $idProveedor;  
    private $otro;    /*Empleado o proveedor u otro*/
    private $VoBo;
    private $fechaVoBo;
    private $Observaciones;
    private $Monto;
    private $desc;
    private $nombreProyecto;
    private $nombreMeta;
    private $Elabora;
    private $Estatus;
    private $area;
    private $destino;
    private $idPeriodo;
    private $idPrograma;
    
    function constructor1($id, $fecha, $idProyecto, $idMeta, $idEmpleado, $idProveedor, $otro, $VoBo, $fechaVoBo, $Observaciones, $Elabora, $Estatus, $area, $destino, $idPrograma) {
        $this->id = $id;
        $this->fecha = $fecha;
        $this->idProyecto = $idProyecto;
        $this->idMeta = $idMeta;
        $this->idEmpleado = $idEmpleado;
        $this->idProveedor = $idProveedor;
        $this->otro = $otro;
        $this->VoBo = $VoBo;
        $this->fechaVoBo = $fechaVoBo;
        $this->Observaciones = $Observaciones;
        $this->Elabora = $Elabora;
        $this->Estatus = $Estatus;
        $this->area = $area;
        $this->destino = $destino;
        $this->idPrograma = $idPrograma;
    }

    function constructor2($id, $fecha, $idProyecto, $idMeta, $Monto, $desc, $nombreMeta, $Estatus, $destino) {
        $this->id = $id;
        $this->fecha = $fecha;
        $this->idProyecto = $idProyecto;
        $this->idMeta = $idMeta;
        $this->Monto = $Monto;
        $this->desc = $desc;
        $this->nombreMeta = $nombreMeta;
        $this->Estatus = $Estatus;
        $this->destino = $destino;
    }

    function constructor3($id, $fecha, $idProyecto, $Monto, $nombreProyecto, $destino) {
        $this->id = $id;
        $this->fecha = $fecha;
        $this->idProyecto = $idProyecto;
        $this->Monto = $Monto;
        $this->nombreProyecto = $nombreProyecto;
        $this->destino = $destino;
    }

    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getFecha() {
        return $this->fecha;
    }

    public function getIdProyecto() {
        return $this->idProyecto;
    }

    public function getIdMeta() {
        return $this->idMeta;
    }

    public function getIdEmpleado() {
        return $this->idEmpleado;
    }

    public function getIdProveedor() {
        return $this->idProveedor;
    }

    public function getOtro() {
        return $this->otro;
    }

    public function getVoBo() {
        return $this->VoBo;
    }

    public function getFechaVoBo() {
        return $this->fechaVoBo;
    }

    public function getObservaciones() {
        return $this->Observaciones;
    }

    public function getMonto() {
        return $this->Monto;
    }

    public function getDesc() {
        return $this->desc;
    }

    public function getNombreProyecto() {
        return $this->nombreProyecto;
    }

    public function getNombreMeta() {
        return $this->nombreMeta;
    }

    public function getElabora() {
        return $this->Elabora;
    }

    public function getEstatus() {
        return $this->Estatus;
    }

    public function getArea() {
        return $this->area;
    }

    public function getDestino() {
        return $this->destino;
    }

    public function getIdPeriodo() {
        return $this->idPeriodo;
    }

    public function getIdPrograma() {
        return $this->idPrograma;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setFecha($fecha) {
        $this->fecha = $fecha;
    }

    public function setIdProyecto($idProyecto) {
        $this->idProyecto = $idProyecto;
    }

    public function setIdMeta($idMeta) {
        $this->idMeta = $idMeta;
    }

    public function setIdEmpleado($idEmpleado) {
        $this->idEmpleado = $idEmpleado;
    }

    public function setIdProveedor($idProveedor) {
        $this->idProveedor = $idProveedor;
    }

    public function setOtro($otro) {
        $this->otro = $otro;
    }

    public function setVoBo($VoBo) {
        $this->VoBo = $VoBo;
    }

    public function setFechaVoBo($fechaVoBo) {
        $this->fechaVoBo = $fechaVoBo;
    }

    public function setObservaciones($Observaciones) {
        $this->Observaciones = $Observaciones;
    }

    public function setMonto($Monto) {
        $this->Monto = $Monto;
    }

    public function setDesc($desc) {
        $this->desc = $desc;
    }

    public function setNombreProyecto($nombreProyecto) {
        $this->nombreProyecto = $nombreProyecto;
    }

    public function setNombreMeta($nombreMeta) {
        $this->nombreMeta = $nombreMeta;
    }

    public function setElabora($Elabora) {
        $this->Elabora = $Elabora;
    }

    public function setEstatus($Estatus) {
        $this->Estatus = $Estatus;
    }

    public function setArea($area) {
        $this->area = $area;
    }

    public function setDestino($destino) {
        $this->destino = $destino;
    }

    public function setIdPeriodo($idPeriodo) {
        $this->idPeriodo = $idPeriodo;
    }

    public function setIdPrograma($idPrograma) {
        $this->idPrograma = $idPrograma;
    }


}
