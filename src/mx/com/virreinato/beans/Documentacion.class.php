<?php
	class Documentacion
	{
		private $id;
		private $idProyecto;
		private $idArea;
		private $idEmpleado;
		private $fecha;
		private $version;
		private $archivo;
		private $observaciones;
		
		function setAll($id, $idProyecto, $idArea, $idEmpleado, $fecha, $version, $archivo, $observaciones){
			$this->id = $id;
			$this->idProyecto = $idProyecto;
			$this->idArea = $idArea;
			$this->idEmpleado = $idEmpleado;
			$this->fecha = $fecha;
			$this->version = $version;
			$this->archivo = $archivo;
			$this->observaciones = $observaciones;
		}
		
		function __construct(){
			
		}
		
		function getId(){
			return $this->id;	
		}
		
		function setId($id){
			$this->id = $id;	
		}
		
		function getIdProyecto(){
			return $this->idProyecto;	
		}
		
		function setIdProyecto($idProyecto){
			$this->idProyecto = $idProyecto;	
		}
		
		function getIdArea(){
			return $this->idArea;
		}
		
		function setIdArea($idArea){
			$this->idArea = $idArea;	
		}
		
		function getIdEmpleado(){
			return $this->idEmpleado;	
		}
		
		function setIdEmpleado($idEmpleado){
			$this->idEmpleado = $idEmpleado;	
		}
		
		function getFecha(){
			return $this->fecha;	
		}
		
		function setFecha($fecha){
			$this->fecha = $fecha;
		}
		
		function getVersion(){
			return $this->version;	
		}
		
		function setVersion($version){
			$this->version = $version;	
		}
		
		function getArchivo(){
			return $this->archivo;			
		}
		
		function setArchivo($archivo){
			$this->archivo = $archivo;
		}
		
		function getObservaciones(){
			return $this->observaciones;	
		}
		
		function setObservaciones($observaciones){
			$this->observaciones = $observaciones;	
		}
		
	}

?>