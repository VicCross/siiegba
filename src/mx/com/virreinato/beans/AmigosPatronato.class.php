<?php

class Amigos { // Productos

    private $id_tipo_amigos;
    private $nombre_tipo_amigos;
    private $descripcion_tipo_amigos;
	private $monto_tipo_amigos;
	

    function setAll($id_tipo_amigos,$nombre_tipo_amigos,$descripcion_tipo_amigos,$monto_tipo_amigos) {
        $this->id_tipo_amigos = $id_tipo_amigos;
        $this->nombre_tipo_amigos = $nombre_tipo_amigos;
        $this->descripcion_tipo_amigos = $descripcion_tipo_amigos;
        $this->monto_tipo_amigos = $monto_tipo_amigos;
		
        
      
    }
    
    function __construct() {
        
    }

        public function getId_tipo_amigos() {
        return $this->id_tipo_amigos;
    }

    public function setId_tipo_amigos($id_tipo_amigos) {
        $this->id_tipo_amigos = $id_tipo_amigos;
    }

    public function getNombre_tipo_amigos() {
        return $this->nombre_tipo_amigos;
    }

    public function setNombre_tipo_amigos($nombre_tipo_amigos) {
        $this->nombre_tipo_amigos = $nombre_tipo_amigos;
    }

    public function getDescripcion_tipo_amigos() {
        return $this->descripcion_tipo_amigos;
    }

    public function setDescripcion_tipo_amigos($descripcion_tipo_amigos) {
        $this->descripcion_tipo_amigos = $descripcion_tipo_amigos;
    }

    

}

?>
