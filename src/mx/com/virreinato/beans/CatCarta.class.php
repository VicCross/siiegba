<?php

class CatCarta {
    
    private $Id;
    private $idProyecto; 
    private $idMeta;
    private $meta;
    private $Dependencia;
    private $Version;
    private $costo;
    private $objetivoEsp;
    private $eje;
    private $nombreProyecto;
    private $nombreLider;
    private $fechaInicio;
    private $fechaFin;
    private $DescEntregable;
    private $Caracteristicas_Entregable;
    private $Entregado;
    private $Validado;
    
    function setAll($Id, $idProyecto, $idMeta, $meta, $Dependencia, $Version, $costo, $objetivoEsp, $eje, $DescEntregable, $Caracteristicas_Entregable, $Entregado, $Validado) {
        $this->Id = $Id;
        $this->idProyecto = $idProyecto;
        $this->idMeta = $idMeta;
        $this->meta = $meta;
        $this->Dependencia = $Dependencia;
        $this->Version = $Version;
        $this->costo = $costo;
        $this->objetivoEsp = $objetivoEsp;
        $this->eje = $eje;
        $this->DescEntregable = $DescEntregable;
        $this->Caracteristicas_Entregable = $Caracteristicas_Entregable;
        $this->Entregado = $Entregado;
        $this->Validado = $Validado;
    }

    function catCart($Id, $meta, $Version, $costo, $nombreProyecto, $nombreLider, $fechaInicio, $fechaFin) {
        $this->Id = $Id;
        $this->meta = $meta;
        $this->Version = $Version;
        $this->costo = $costo;
        $this->nombreProyecto = $nombreProyecto;
        $this->nombreLider = $nombreLider;
        $this->fechaInicio = $fechaInicio;
        $this->fechaFin = $fechaFin;
    }

    function __construct() {
        
    }

    public function getId() {
        return $this->Id;
    }

    public function getIdProyecto() {
        return $this->idProyecto;
    }

    public function getIdMeta() {
        return $this->idMeta;
    }

    public function getMeta() {
        return $this->meta;
    }

    public function getDependencia() {
        return $this->Dependencia;
    }

    public function getVersion() {
        return $this->Version;
    }

    public function getCosto() {
        return $this->costo;
    }

    public function getObjetivoEsp() {
        return $this->objetivoEsp;
    }

    public function getEje() {
        return $this->eje;
    }

    public function getNombreProyecto() {
        return $this->nombreProyecto;
    }

    public function getNombreLider() {
        return $this->nombreLider;
    }

    public function getFechaInicio() {
        return $this->fechaInicio;
    }

    public function getFechaFin() {
        return $this->fechaFin;
    }

    public function getDescEntregable() {
        return $this->DescEntregable;
    }

    public function getCaracteristicas_Entregable() {
        return $this->Caracteristicas_Entregable;
    }

    public function getEntregado() {
        return $this->Entregado;
    }

    public function getValidado() {
        return $this->Validado;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setIdProyecto($idProyecto) {
        $this->idProyecto = $idProyecto;
    }

    public function setIdMeta($idMeta) {
        $this->idMeta = $idMeta;
    }

    public function setMeta($meta) {
        $this->meta = $meta;
    }

    public function setDependencia($Dependencia) {
        $this->Dependencia = $Dependencia;
    }

    public function setVersion($Version) {
        $this->Version = $Version;
    }

    public function setCosto($costo) {
        $this->costo = $costo;
    }

    public function setObjetivoEsp($objetivoEsp) {
        $this->objetivoEsp = $objetivoEsp;
    }

    public function setEje($eje) {
        $this->eje = $eje;
    }

    public function setNombreProyecto($nombreProyecto) {
        $this->nombreProyecto = $nombreProyecto;
    }

    public function setNombreLider($nombreLider) {
        $this->nombreLider = $nombreLider;
    }

    public function setFechaInicio($fechaInicio) {
        $this->fechaInicio = $fechaInicio;
    }

    public function setFechaFin($fechaFin) {
        $this->fechaFin = $fechaFin;
    }

    public function setDescEntregable($DescEntregable) {
        $this->DescEntregable = $DescEntregable;
    }

    public function setCaracteristicas_Entregable($Caracteristicas_Entregable) {
        $this->Caracteristicas_Entregable = $Caracteristicas_Entregable;
    }

    public function setEntregado($Entregado) {
        $this->Entregado = $Entregado;
    }

    public function setValidado($Validado) {
        $this->Validado = $Validado;
    }


}
