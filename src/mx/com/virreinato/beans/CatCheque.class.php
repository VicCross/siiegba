<?php

class CatCheque {
    
    private $id;
    private $folio;
    private $destino;
    private $idCuenta;
    private $monto;
    private $montoLetra;
    private $fechaEmision;
    private $Estatus;
    private $Observaciones;
    private $num_Poliza;
    private $idSolicitud;
    private $idPeriodo;
    private $idProyecto;
    private $area;
    private $idEmpleado;
    
    function setCheq($id, $folio, $monto, $idProyecto) {
        $this->id = $id;
        $this->folio = $folio;
        $this->monto = $monto;
        $this->idProyecto = $idProyecto;
    }
    
    function catCheqDesti($id, $folio, $destino, $monto, $idProyecto) {
        $this->id = $id;
        $this->folio = $folio;
        $this->destino = $destino;
        $this->monto = $monto;
        $this->idProyecto = $idProyecto;
    }
    
    function setAll($id, $folio, $destino, $idCuenta, $monto, $montoLetra, $fechaEmision, $Estatus, $Observaciones, $num_Poliza, $idSolicitud, $idPeriodo, $idProyecto, $area, $idEmpleado) {
        $this->id = $id;
        $this->folio = $folio;
        $this->destino = $destino;
        $this->idCuenta = $idCuenta;
        $this->monto = $monto;
        $this->montoLetra = $montoLetra;
        $this->fechaEmision = $fechaEmision;
        $this->Estatus = $Estatus;
        $this->Observaciones = $Observaciones;
        $this->num_Poliza = $num_Poliza;
        $this->idSolicitud = $idSolicitud;
        $this->idPeriodo = $idPeriodo;
        $this->idProyecto = $idProyecto;
        $this->area = $area;
        $this->idEmpleado = $idEmpleado;
    }

    function setAll2($id, $folio, $destino, $idCuenta, $monto, $montoLetra, $fechaEmision, $Estatus, $Observaciones, $num_Poliza, $idSolicitud, $idPeriodo, $idProyecto, $area) {
        $this->id = $id;
        $this->folio = $folio;
        $this->destino = $destino;
        $this->idCuenta = $idCuenta;
        $this->monto = $monto;
        $this->montoLetra = $montoLetra;
        $this->fechaEmision = $fechaEmision;
        $this->Estatus = $Estatus;
        $this->Observaciones = $Observaciones;
        $this->num_Poliza = $num_Poliza;
        $this->idSolicitud = $idSolicitud;
        $this->idPeriodo = $idPeriodo;
        $this->idProyecto = $idProyecto;
        $this->area = $area;
    }
    
    public function getId() {
        return $this->id;
    }

    public function getFolio() {
        return $this->folio;
    }

    public function getDestino() {
        return $this->destino;
    }

    public function getIdCuenta() {
        return $this->idCuenta;
    }

    public function getMonto() {
        return $this->monto;
    }

    public function getMontoLetra() {
        return $this->montoLetra;
    }

    public function getFechaEmision() {
        return $this->fechaEmision;
    }

    public function getEstatus() {
        return $this->Estatus;
    }

    public function getObservaciones() {
        return $this->Observaciones;
    }

    public function getNum_Poliza() {
        return $this->num_Poliza;
    }

    public function getIdSolicitud() {
        return $this->idSolicitud;
    }

    public function getIdPeriodo() {
        return $this->idPeriodo;
    }

    public function getIdProyecto() {
        return $this->idProyecto;
    }

    public function getArea() {
        return $this->area;
    }

    public function getIdEmpleado() {
        return $this->idEmpleado;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setFolio($folio) {
        $this->folio = $folio;
    }

    public function setDestino($destino) {
        $this->destino = $destino;
    }

    public function setIdCuenta($idCuenta) {
        $this->idCuenta = $idCuenta;
    }

    public function setMonto($monto) {
        $this->monto = $monto;
    }

    public function setMontoLetra($montoLetra) {
        $this->montoLetra = $montoLetra;
    }

    public function setFechaEmision($fechaEmision) {
        $this->fechaEmision = $fechaEmision;
    }

    public function setEstatus($Estatus) {
        $this->Estatus = $Estatus;
    }

    public function setObservaciones($Observaciones) {
        $this->Observaciones = $Observaciones;
    }

    public function setNum_Poliza($num_Poliza) {
        $this->num_Poliza = $num_Poliza;
    }

    public function setIdSolicitud($idSolicitud) {
        $this->idSolicitud = $idSolicitud;
    }

    public function setIdPeriodo($idPeriodo) {
        $this->idPeriodo = $idPeriodo;
    }

    public function setIdProyecto($idProyecto) {
        $this->idProyecto = $idProyecto;
    }

    public function setArea($area) {
        $this->area = $area;
    }

    public function setIdEmpleado($idEmpleado) {
        $this->idEmpleado = $idEmpleado;
    }

}
