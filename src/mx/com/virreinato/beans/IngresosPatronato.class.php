<?php
	class IngresosPatronato
	{
		private $id;
		private $patrocinador;
		private $monto;
		private $fecha;
		private $id_tipo_amigos;
		private $idProyecto;
		private $idExpoTemp;
		private $archivo;
		private $periodo;
		
		function setAll($id, $patrocinador, $monto, $fecha,$id_tipo_amigos, $idProyecto, $idExpoTemp, $archivo)
		{
			$this->id = $id;
			$this->patrocinador = $patrocinador;
			$this->monto = $monto;
			$this->fecha = $fecha;
			$this->id_tipo_amigos = $id_tipo_amigos;
			$this->idProyecto = $idProyecto;
			$this->idExpoTemp = $idExpoTemp;
			$this->archivo = $archivo;
			$this->periodo = $periodo;
		}
		
		function __construct(){
			
		}
		
		function getId(){
			return $this->id;
		}
		
		function getPatrocinador(){
			return $this->patrocinador;
		}
		
		function getMonto(){
			return $this->monto;
		}
		
		function getFecha(){
			return $this->fecha;
		}
		
		function getId_Tipo_Amigos(){
			return $this->id_tipo_amigos;
		}
		
		function getIdProyecto(){
			return $this->idProyecto;
		}
		
		function getIdExpoTemp(){
			return $this->idExpoTemp;
		}
		
		function getArchivo(){
			return $this->archivo;	
		}
		
			function getPeriodo(){
			return $this->periodo;	
		}
		
		
		
		function setId($id){
			$this->id = $id;
		}
		
		function setPatrocinador($patrocinador){
			$this->patrocinador = $patrocinador;
		}
		
		function setMonto($monto){
			$this->monto = $monto;
		}
		
		function setFecha($fecha){
			$this->fecha = $fecha;
		}
		
		function setId_Tipo_Amigos($id_tipo_amigos){
			$this->id_tipo_amigos = $id_tipo_amigos;
		}
		
		function setIdProyecto($idProyecto){
			$this->idProyecto = $idProyecto;
		}
		
		function setIdExpoTemp($idExpoTemp){
			$this->idExpoTemp = $idExpoTemp;
		}
		
		function setArchivo($archivo){
			$this->archivo = $archivo;	
		}
		function setPeriodo($periodo){
			$this->periodo = $periodo;	
		}
	
	}
	
?>