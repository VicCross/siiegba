<?php

class ConsultaResumen {
    
    private $id;
    private $numProyecto;
    private $proyecto;

    private $idMeta;
    private $meta;

    private $idPartida;
    private $partida;
    private $descPartida;

    private $idMinistracion;

    private $autorizado;
    private $ministrado;
    private $porMinistrar;
    private $gastado;
    private $porGastarMinistrado;
    private $porGastar;
    private $comprobadoINAH;
    private $porComprobarINAH;

    private $autEne;
    private $autFeb;
    private $autMar;
    private $autAbr;
    private $autMay;
    private $autJun;
    private $autJul;
    private $autAgo;
    private $autSep;
    private $autOct;
    private $autNov;
    private $autDic;
    
    function consulta($id, $idPartida, $descPartida, $comprobadoINAH) {
        $this->id = $id;
        $this->idPartida = $idPartida;
        $this->descPartida = $descPartida;
        $this->comprobadoINAH = $comprobadoINAH;
    }
    
    function proyecto($id, $numProyecto, $proyecto) {
        $this->id = $id;
        $this->numProyecto = $numProyecto;
        $this->proyecto = $proyecto;
    }
    
    function resumenMeta($idMeta, $meta, $autorizado, $ministrado, $porMinistrar, $gastado, $porGastarMinistrado, $porGastar, $comprobadoINAH, $porComprobarINAH, $autEne, $autFeb, $autMar, $autAbr, $autMay, $autJun, $autJul, $autAgo, $autSep, $autOct, $autNov, $autDic) {
        $this->idMeta = $idMeta;
        $this->meta = $meta;
        $this->autorizado = $autorizado;
        $this->ministrado = $ministrado;
        $this->porMinistrar = $porMinistrar;
        $this->gastado = $gastado;
        $this->porGastarMinistrado = $porGastarMinistrado;
        $this->porGastar = $porGastar;
        $this->comprobadoINAH = $comprobadoINAH;
        $this->porComprobarINAH = $porComprobarINAH;
        $this->autEne = $autEne;
        $this->autFeb = $autFeb;
        $this->autMar = $autMar;
        $this->autAbr = $autAbr;
        $this->autMay = $autMay;
        $this->autJun = $autJun;
        $this->autJul = $autJul;
        $this->autAgo = $autAgo;
        $this->autSep = $autSep;
        $this->autOct = $autOct;
        $this->autNov = $autNov;
        $this->autDic = $autDic;
    }

    function resumenProyecto($id, $numProyecto, $proyecto, $autorizado, $ministrado, $porMinistrar, $gastado, $porGastarMinistrado, $porGastar, $comprobadoINAH, $porComprobarINAH, $autEne, $autFeb, $autMar, $autAbr, $autMay, $autJun, $autJul, $autAgo, $autSep, $autOct, $autNov, $autDic) {
        $this->id = $id;
        $this->numProyecto = $numProyecto;
        $this->proyecto = $proyecto;
        $this->autorizado = $autorizado;
        $this->ministrado = $ministrado;
        $this->porMinistrar = $porMinistrar;
        $this->gastado = $gastado;
        $this->porGastarMinistrado = $porGastarMinistrado;
        $this->porGastar = $porGastar;
        $this->comprobadoINAH = $comprobadoINAH;
        $this->porComprobarINAH = $porComprobarINAH;
        $this->autEne = $autEne;
        $this->autFeb = $autFeb;
        $this->autMar = $autMar;
        $this->autAbr = $autAbr;
        $this->autMay = $autMay;
        $this->autJun = $autJun;
        $this->autJul = $autJul;
        $this->autAgo = $autAgo;
        $this->autSep = $autSep;
        $this->autOct = $autOct;
        $this->autNov = $autNov;
        $this->autDic = $autDic;
    }

    function resumenPartida($idPartida, $partida, $descPartida, $autorizado, $ministrado, $porMinistrar, $gastado, $porGastarMinistrado, $porGastar, $comprobadoINAH, $porComprobarINAH, $autEne, $autFeb, $autMar, $autAbr, $autMay, $autJun, $autJul, $autAgo, $autSep, $autOct, $autNov, $autDic) {
        $this->idPartida = $idPartida;
        $this->partida = $partida;
        $this->descPartida = $descPartida;
        $this->autorizado = $autorizado;
        $this->ministrado = $ministrado;
        $this->porMinistrar = $porMinistrar;
        $this->gastado = $gastado;
        $this->porGastarMinistrado = $porGastarMinistrado;
        $this->porGastar = $porGastar;
        $this->comprobadoINAH = $comprobadoINAH;
        $this->porComprobarINAH = $porComprobarINAH;
        $this->autEne = $autEne;
        $this->autFeb = $autFeb;
        $this->autMar = $autMar;
        $this->autAbr = $autAbr;
        $this->autMay = $autMay;
        $this->autJun = $autJun;
        $this->autJul = $autJul;
        $this->autAgo = $autAgo;
        $this->autSep = $autSep;
        $this->autOct = $autOct;
        $this->autNov = $autNov;
        $this->autDic = $autDic;
    }
    
    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getNumProyecto() {
        return $this->numProyecto;
    }

    public function getProyecto() {
        return $this->proyecto;
    }

    public function getIdMeta() {
        return $this->idMeta;
    }

    public function getMeta() {
        return $this->meta;
    }

    public function getIdPartida() {
        return $this->idPartida;
    }

    public function getPartida() {
        return $this->partida;
    }

    public function getDescPartida() {
        return $this->descPartida;
    }

    public function getIdMinistracion() {
        return $this->idMinistracion;
    }

    public function getAutorizado() {
        return $this->autorizado;
    }

    public function getMinistrado() {
        return $this->ministrado;
    }

    public function getPorMinistrar() {
        return $this->porMinistrar;
    }

    public function getGastado() {
        return $this->gastado;
    }

    public function getPorGastarMinistrado() {
        return $this->porGastarMinistrado;
    }

    public function getPorGastar() {
        return $this->porGastar;
    }

    public function getComprobadoINAH() {
        return $this->comprobadoINAH;
    }

    public function getPorComprobarINAH() {
        return $this->porComprobarINAH;
    }

    public function getAutEne() {
        return $this->autEne;
    }

    public function getAutFeb() {
        return $this->autFeb;
    }

    public function getAutMar() {
        return $this->autMar;
    }

    public function getAutAbr() {
        return $this->autAbr;
    }

    public function getAutMay() {
        return $this->autMay;
    }

    public function getAutJun() {
        return $this->autJun;
    }

    public function getAutJul() {
        return $this->autJul;
    }

    public function getAutAgo() {
        return $this->autAgo;
    }

    public function getAutSep() {
        return $this->autSep;
    }

    public function getAutOct() {
        return $this->autOct;
    }

    public function getAutNov() {
        return $this->autNov;
    }

    public function getAutDic() {
        return $this->autDic;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setNumProyecto($numProyecto) {
        $this->numProyecto = $numProyecto;
    }

    public function setProyecto($proyecto) {
        $this->proyecto = $proyecto;
    }

    public function setIdMeta($idMeta) {
        $this->idMeta = $idMeta;
    }

    public function setMeta($meta) {
        $this->meta = $meta;
    }

    public function setIdPartida($idPartida) {
        $this->idPartida = $idPartida;
    }

    public function setPartida($partida) {
        $this->partida = $partida;
    }

    public function setDescPartida($descPartida) {
        $this->descPartida = $descPartida;
    }

    public function setIdMinistracion($idMinistracion) {
        $this->idMinistracion = $idMinistracion;
    }

    public function setAutorizado($autorizado) {
        $this->autorizado = $autorizado;
    }

    public function setMinistrado($ministrado) {
        $this->ministrado = $ministrado;
    }

    public function setPorMinistrar($porMinistrar) {
        $this->porMinistrar = $porMinistrar;
    }

    public function setGastado($gastado) {
        $this->gastado = $gastado;
    }

    public function setPorGastarMinistrado($porGastarMinistrado) {
        $this->porGastarMinistrado = $porGastarMinistrado;
    }

    public function setPorGastar($porGastar) {
        $this->porGastar = $porGastar;
    }

    public function setComprobadoINAH($comprobadoINAH) {
        $this->comprobadoINAH = $comprobadoINAH;
    }

    public function setPorComprobarINAH($porComprobarINAH) {
        $this->porComprobarINAH = $porComprobarINAH;
    }

    public function setAutEne($autEne) {
        $this->autEne = $autEne;
    }

    public function setAutFeb($autFeb) {
        $this->autFeb = $autFeb;
    }

    public function setAutMar($autMar) {
        $this->autMar = $autMar;
    }

    public function setAutAbr($autAbr) {
        $this->autAbr = $autAbr;
    }

    public function setAutMay($autMay) {
        $this->autMay = $autMay;
    }

    public function setAutJun($autJun) {
        $this->autJun = $autJun;
    }

    public function setAutJul($autJul) {
        $this->autJul = $autJul;
    }

    public function setAutAgo($autAgo) {
        $this->autAgo = $autAgo;
    }

    public function setAutSep($autSep) {
        $this->autSep = $autSep;
    }

    public function setAutOct($autOct) {
        $this->autOct = $autOct;
    }

    public function setAutNov($autNov) {
        $this->autNov = $autNov;
    }

    public function setAutDic($autDic) {
        $this->autDic = $autDic;
    }


}
