<?php

class Honorarios_rot {
    
    private $id_otros_ing;
    private $honorariosInicial;
    private $honorariosActual;
	private $fondorot;
	private $periodo;
	
    
    function setAll($id_otros_ing, $honorariosInicial, $honorariosActual,$fondorot,$periodo) 
	{
        $this->id_otros_ing = $id_otros_ing;
        $this->honorariosInicial = $honorariosInicial;
        $this->honorariosActual = $honorariosActual;
		$this->fondorot = $fondorot;
		$this->periodo = $periodo;
    }
    
    function __construct() {
        
    }

    public function getId_otros_ing() {
        return $this->id_otros_ing;
    }

    public function getHonorariosInicial() {
        return $this->honorariosInicial;
    }

    public function getHonorariosActual() {
        return $this->honorariosActual;
    }
	
	public function getFondorot() {
        return $this->fondorot;
    }
	
	public function getPeriodo() {
        return $this->periodo;
    }

    public function setId_otros_ing($id_otros_ing) {
        $this->id_otros_ing = $id_otros_ing;
    }

    public function setHonorariosInicial($honorariosInicial) {
        $this->honorariosInicial = $honorariosInicial;
    }

    public function setHonorariosActual($honorariosActual) {
        $this->honorariosActual = $honorariosActual;
    }
	
	public function setFondorot($fondorot) {
        $this->fondorot = $fondorot;
    }
	
	public function setPeriodo($periodo) {
        $this->periodo = $periodo;
    }

}
