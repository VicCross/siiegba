<?php
	class IngresosINBA
	{
		private $id;
		private $idProyecto;
		private $idExpoTemp;
		private $fecha;
		private $monto;
		private $archivo;
		private $periodo;
		
		function setAll($id, $idProyecto, $idExpoTemp, $fecha, $monto,$archivo,$periodo)
		{
			$this->id = $id;
			$this->idProyecto = $idProyecto;
			$this->fecha = $fecha;
			$this->monto = $monto;			
			$this->idExpoTemp = $idExpoTemp;
			$this->archivo = $archivo;
			$this->periodo=$periodo;
		}
		
		function __construct(){
			
		}
		
		function getId(){
			return $this->id;
		}
		
		function getMonto(){
			return $this->monto;
		}
		
		function getFecha(){
			return $this->fecha;
		}
		
		function getIdProyecto(){
			return $this->idProyecto;
		}
		
		function getIdExpoTemp(){
			return $this->idExpoTemp;
		}
		
		function getArchivo(){
			return $this->archivo;	
		}
		
		function getPeriodo(){
			return $this->periodo;	
		}
		
		
		
		
		function setId($id){
			$this->id = $id;
		}
		
		function setMonto($monto){
			$this->monto = $monto;
		}
		
		function setFecha($fecha){
			$this->fecha = $fecha;
		}
		
		function setIdProyecto($idProyecto){
			$this->idProyecto = $idProyecto;
		}
		
		function setIdExpoTemp($idExpoTemp){
			$this->idExpoTemp = $idExpoTemp;
		}
		
		function setArchivo($archivo){
			$this->archivo = $archivo;	
		}
		
		
		function setPeriodo($periodo){
			$this->periodo=$periodo;	
		}
	}
?>