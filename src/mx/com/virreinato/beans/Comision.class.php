<?php

 class Comision {
    private $id;
    private $idCCosto;
    private $fechaOficio;
    private $numeroOficio;
    private $periodoInicial;
    private $periodoFinal;
    private $objetivo;
    private $resultados;
    private $nacional;
    private $lugar;
    private $tarifaDiaria;
    private $numeroDias;
    private $montoTotal;
    private $montoDoctos;
    private $montoSinDoctos;
    private $letraSinDoctos;
    private $saldoPendiente;
    private $estatus;
    private $empleado;
    private $nombre;
    private $appaterno;
    private $amaterno;
    
    function setAll($id, $idCCosto, $fechaOficio, $numeroOficio, $periodoInicial, $periodoFinal, $objetivo, $resultados, $nacional, $lugar, $tarifaDiaria, $numeroDias, $montoTotal, $montoDoctos, $montoSinDoctos, $letraSinDoctos, $saldoPendiente, $estatus, $empleado){
        $this->id = $id;
        $this->idCCosto = $idCCosto;
        $this->fechaOficio = $fechaOficio;
        $this->numeroOficio = $numeroOficio;
        $this->periodoInicial = $periodoInicial;
        $this->periodoFinal = $periodoFinal;
        $this->objetivo = $objetivo;
        $this->resultados = $resultados;
        $this->nacional = $nacional;
        $this->lugar = $lugar;
        $this->tarifaDiaria = $tarifaDiaria;
        $this->numeroDias = $numeroDias;
        $this->montoTotal = $montoTotal;
        $this->montoDoctos = $montoDoctos;
        $this->montoSinDoctos = $montoSinDoctos;
        $this->letraSinDoctos = $letraSinDoctos;
        $this->saldoPendiente = $saldoPendiente;
        $this->estatus = $estatus;
        $this->empleado = $empleado;
    }
    
    function __construct() {
        
    }

    
    public function getId() {
        return $this->id;
    }

    public function getIdCCosto() {
        return $this->idCCosto;
    }

    public function getFechaOficio() {
        return $this->fechaOficio;
    }

    public function getNumeroOficio() {
        return $this->numeroOficio;
    }

    public function getPeriodoInicial() {
        return $this->periodoInicial;
    }

    public function getPeriodoFinal() {
        return $this->periodoFinal;
    }

    public function getObjetivo() {
        return $this->objetivo;
    }

    public function getResultados() {
        return $this->resultados;
    }

    public function getNacional() {
        return $this->nacional;
    }

    public function getLugar() {
        return $this->lugar;
    }

    public function getTarifaDiaria() {
        return $this->tarifaDiaria;
    }

    public function getNumeroDias() {
        return $this->numeroDias;
    }

    public function getMontoTotal() {
        return $this->montoTotal;
    }

    public function getMontoDoctos() {
        return $this->montoDoctos;
    }

    public function getMontoSinDoctos() {
        return $this->montoSinDoctos;
    }

    public function getLetraSinDoctos() {
        return $this->letraSinDoctos;
    }

    public function getSaldoPendiente() {
        return $this->saldoPendiente;
    }

    public function getEstatus() {
        return $this->estatus;
    }

    public function getEmpleado() {
        return $this->empleado;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setIdCCosto($idCCosto) {
        $this->idCCosto = $idCCosto;
    }

    public function setFechaOficio($fechaOficio) {
        $this->fechaOficio = $fechaOficio;
    }

    public function setNumeroOficio($numeroOficio) {
        $this->numeroOficio = $numeroOficio;
    }

    public function setPeriodoInicial($periodoInicial) {
        $this->periodoInicial = $periodoInicial;
    }

    public function setPeriodoFinal($periodoFinal) {
        $this->periodoFinal = $periodoFinal;
    }

    public function setObjetivo($objetivo) {
        $this->objetivo = $objetivo;
    }

    public function setResultados($resultados) {
        $this->resultados = $resultados;
    }

    public function setNacional($nacional) {
        $this->nacional = $nacional;
    }

    public function setLugar($lugar) {
        $this->lugar = $lugar;
    }

    public function setTarifaDiaria($tarifaDiaria) {
        $this->tarifaDiaria = $tarifaDiaria;
    }

    public function setNumeroDias($numeroDias) {
        $this->numeroDias = $numeroDias;
    }

    public function setMontoTotal($montoTotal) {
        $this->montoTotal = $montoTotal;
    }

    public function setMontoDoctos($montoDoctos) {
        $this->montoDoctos = $montoDoctos;
    }

    public function setMontoSinDoctos($montoSinDoctos) {
        $this->montoSinDoctos = $montoSinDoctos;
    }

    public function setLetraSinDoctos($letraSinDoctos) {
        $this->letraSinDoctos = $letraSinDoctos;
    }

    public function setSaldoPendiente($saldoPendiente) {
        $this->saldoPendiente = $saldoPendiente;
    }

    public function setEstatus($estatus) {
        $this->estatus = $estatus;
    }

    public function setEmpleado($empleado) {
        $this->empleado = $empleado;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function getAppaterno() {
        return $this->appaterno;
    }

    public function getAmaterno() {
        return $this->amaterno;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function setAppaterno($appaterno) {
        $this->appaterno = $appaterno;
    }

    public function setAmaterno($amaterno) {
        $this->amaterno = $amaterno;
    }


}
