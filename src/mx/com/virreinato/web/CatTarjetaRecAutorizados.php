<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/TarjetaRecAutorizadoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/TarjetaRecAutorizados.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$respuesta = "";
if (isset($_POST['id_Tarjeta'])){
    $elemento=new TarjetaRecAutorizados();
    $elemento->setIdTarjeta((int)($_POST["id_Tarjeta"]));
    $elemento->setAnio((int)($_POST["anio_Autorizacion"]));
    $elemento->setIngresoAutogenerado((float)($_POST["ingreso_Autogenerado"]));
    $elemento->setAportacionesTerceros((float)($_POST["recursos_Terceros"]));
    if ($_POST["id"] != null){
        $elemento->setIdRecurso((int)($_POST["id"]));
        $dao=new TarjetaRecAutorizadoDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new TarjetaRecAutorizadoDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
    header("Location: ../../../../../Proyectos/lista_RecursosAutorizados.php?respuesta=" . $respuesta."&idTarjeta=".$_POST['id_Tarjeta']);
}else if (isset($_GET['id'])){
    $dao=new TarjetaRecAutorizadoDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
    header("Location: ../../../../../Proyectos/lista_RecursosAutorizados.php?respuesta=" . $respuesta."&idTarjeta=".$_GET['id_Tarjeta']);
} else {
    $respuesta = "No se detecto la acción a realizar.";
    header("Location: ../../../../../Proyectos/lista_RecursosAutorizados.php?respuesta=" . $respuesta);
}
?>


