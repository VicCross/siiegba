<?php

session_start();
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/CartaCronogramaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CartaCronograma.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$respuesta = "";
if (isset($_POST['id_Carta'])){
    $elemento = new CartaCronograma();

    $elemento->setIdCarta( (int)( $_POST["id_Carta"] ));
    if( $_POST["orden_act"] != "" )$elemento->setOrden( (int)( $_POST["orden_act"] ));
    $elemento->setActividad( $_POST["actividad_carta"]);
    $elemento->setFechaInicio( date("Y-m-d",strtotime( $_POST["inicioActividad"])));
    $elemento->setFechaFin( date("Y-m-d",strtotime( $_POST["finActividad"])));
    $elemento->setFase($_POST["fase_cronograma"]);
    if (isset($_POST['id'])){
        $elemento->setId((int)($_POST["id"]));
        $dao=new CartaCronogramaDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new CartaCronogramaDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
    header("Location: ../../../../../Proyectos/lista_Cronograma.php?respuesta=" . $respuesta."&idCarta=".$_POST['id_Carta']);
}else if (isset($_GET['id'])){
    $dao=new CartaCronogramaDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
    header("Location: ../../../../../Proyectos/lista_Cronograma.php?respuesta=" . $respuesta."&idCarta=".$_GET['idCarta']);
} else {
    $respuesta = "No se detecto la acción a realizar.";
    header("Location: ../../../../../Proyectos/lista_Cronograma.php?respuesta=" . $respuesta);
}
?>


