<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/CatProyectoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatProyecto.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/ConciliacionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Conciliacion.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/CuentaBancariaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CuentaBancaria.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/MovimientoBancoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/MovimientoBanco.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
if(isset($_POST['continuar'])){
    if(isset($_POST['finicio']) && isset($_POST['ffin']))
    {
        $finicio = $_POST['finicio'];
        $ffin = $_POST['ffin'];
        
        header("Location: ../../../../../bancos/lista_movimiento_banco.php?finicio=" . $finicio."&ffin=".$ffin);
    }else if(isset($_POST['mes']) && isset($_POST['id_periodo'])){
        $mes=(int)((String)$_POST['mes']);

        $id_periodo=(int)((String)$_POST['id_periodo']);

        //obtengo la descripcion del periodo
        $dp=new PeriodoDaoJdbc();
        $periodo=$dp->obtieneElemento((String)($id_periodo));
        if(strlen((String)$mes) == 1){
            $mes = "0".$mes;
            $mes2 = $mes+1;
            if(strlen((String)$mes) == 1){
                $mes2 = "0".$mes2;
            }
        }
        
        $finicio = $periodo->getPeriodo()."-".$mes2."-01";
        if((String)$mes == "12"){
            $finicio = $periodo->getPeriodo()."-".$mes."-31";
        }
        $ffin = $periodo->getPeriodo()."-".$mes."-01";

        //verifico si esta conciliado este mes y periodo
        $conciliaciondao=new ConciliacionDaoJdbc();
        $conciliacion=$conciliaciondao->obtieneElemento((int)$_POST['mes'],(int)$_POST['id_periodo']);

        if($conciliacion!=null && $conciliacion->getId()!=null)
        {    $conciliado = "1";   }
        else
        {    $conciliado = "0";    }
        header("Location: ../../../../../bancos/lista_movimiento_banco.php?finicio=" . $finicio."&ffin=".$ffin."&mes=".$mes."&id_periodo=".$id_periodo."&conciliado=".$conciliado);
    }    
}else if (isset($_GET['id'])){
    $id = null;
    if(isset($_GET['modificar']) && $_GET['modificar'] == "1"){
        $id = $_GET['id'];
    }else{
        $dao=new MovimientoBancoDaoJdbc();
        $res=$dao->eliminaElemento((int)($_GET['id']));

        if ($res) {
            $respuesta = "El registro se eliminó exitosamente.";
        } else {
            $respuesta = "No fue posible eliminar el registro de la base de datos.";
        }
    }
    
    $finicio = $_GET['finicio'];
    $ffin = $_GET['ffin'];        
    $mes=$_GET['mes'];
    $id_periodo=$_GET['id_periodo'];
    
    $conciliaciondao = new ConciliacionDaoJdbc();
    $conciliacion=$conciliaciondao->obtieneElemento((int)($_GET['mes']),(int)($_GET['id_periodo']));
    
    if($conciliacion!=null && $conciliacion->getId()!=null)
    {    $conciliado = "1";   }
    else
    {    $conciliado = "0";    }
    
    header("Location: ../../../../../bancos/lista_movimiento_banco.php?finicio=" . $finicio."&ffin=".$ffin."&mes=".$mes."&id_periodo=".$id_periodo."&conciliado=".$conciliado."&respuesta=".$respuesta."&id=".$id);
}else if(isset($_POST['guardar'])){
    $elemento=new MovimientoBanco();
    $elemento->setNumeroOperacion($_POST["numoperacion"]);
    $cargo=$_POST["cargo"];
    $abono=$_POST["abono"];
    if($cargo!=null && $cargo!="")
    {    $elemento->setCargo((double)($cargo)); }
    else
    {    $elemento->setCargo(0.0);  }
    if($abono!=null && $abono!="")
    {    $elemento->setAbono((double)($abono)); }
    else
    {    $elemento->setAbono(0.0);  }
 
    $fecha=date("Y-m-d",strtotime($_POST["fecha"]));
    $elemento->setFecha($fecha);

    $cuenta=new CuentaBancaria();
    $cuentadao=new CuentaBancariaDaoJdbc();
    $cuenta=$cuentadao->obtieneElemento($_POST["id_cuentabancaria"]);
    $elemento->setCuenta($cuenta);
    $elemento->setDestino($_POST["destino"]);
    $elemento->setReferencia($_POST["referencia"]);

    $elemento->setFormaCarga("Captura");

    $periodo=new Periodo();
    $periododao=new PeriodoDaoJdbc();
    $periodo=$periododao->obtieneElemento($_POST["id_periodo"]);
    $elemento->setPeriodo($periodo);

    $proyecto=new CatProyecto();
    $proyectodao=new CatProyectoDaoJdbc();
    $proyecto=$proyectodao->obtieneElemento($_POST["id_proyecto"]);
    $elemento->setProyecto($proyecto);

    $conciliaciondao=new ConciliacionDaoJdbc();
    if (isset($_POST['id'])){
        $elemento->setId((int)($_POST["id"]));
        $dao=new MovimientoBancoDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new MovimientoBancoDaoJdbc();

        //verifico si esta conciliado
        $conciliacion=$conciliaciondao->obtieneElemento((int)(date("n",strtotime($fecha))),$periodo->getId());
        $res=false;
        $res_concilia=null;
        if($conciliacion!=null && $conciliacion->getId()!=null)
        {    $res_concilia="No fue posible almacenar este movimiento porque este mes ya fue conciliado."; }
        else
        {    $res=$dao->guardaElemento($elemento); }
        

        if($res)
        {        $respuesta = "Su información se almacenó exitosamente.";  }
        else if($res==false && $res_concilia==null)
        {        $respuesta = "No fue posible almacenar su información."; }
        else if($res==false && $res_concilia!=null)
        {        $respuesta = $res_concilia; }
    }
        
        $finicio = $_POST['finicio'];
        $ffin = $_POST['ffin'];        
        $mes=$_POST['mes'];
        $id_periodo=$_POST['id_periodo'];
        
        $conciliacion=$conciliaciondao->obtieneElemento((int)($_POST['mes']),(int)($_POST['id_periodo']));
 
        if($conciliacion!=null && $conciliacion->getId()!=null)
        {    $conciliado = "1";   }
        else
        {    $conciliado = "0";    }
        
        header("Location: ../../../../../bancos/lista_movimiento_banco.php?finicio=" . $finicio."&ffin=".$ffin."&mes=".$mes."&id_periodo=".$id_periodo."&conciliado=".$conciliado."&respuesta=".$respuesta);
}
