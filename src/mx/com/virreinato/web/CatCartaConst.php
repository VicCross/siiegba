<?php

session_start();
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/CatCartaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatCarta.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$respuesta = "";
if( isset($_POST["obtenerMetas"])){

    $query = "SELECT proyecto_id_meta, proyecto_desc_meta FROM sie_proyecto_metas WHERE cpr_id_proyecto = ".$_POST["obtenerMetas"];
    $catalogo = new Catalogo();
        
    $result = $catalogo->obtenerLista($query);

    while ($rs = mysql_fetch_array($result)){
        $sel = "";
        if( (int)($_POST["sel"]) != 0 && (int)($_POST["sel"]) == $rs["proyecto_id_meta"] ){ $sel = "selected='selected'";  }
        echo("<option value='".$rs["proyecto_id_meta"]."' ".$sel.">".$rs["proyecto_desc_meta"]."</option>");
    }			
}else if(isset($_POST['proyectoCC'])){
    $elemento = new CatCarta();
    $elemento->setIdProyecto( (int)( $_POST["proyectoCC"] ));
    $elemento->setIdMeta((int)( $_POST["meta_Proyecto"] ));
    $elemento->setDependencia( $_POST["dependenciaCC"]);
    $elemento->setVersion( $_POST["fechaVersionCC"]);
    $elemento->setCosto((double)($_POST["costoCC"]));
    $elemento->setObjetivoEsp( $_POST["objEspCC"] );
    $elemento->setDescEntregable($_POST["desc_Entregable"]);
    $elemento->setCaracteristicas_Entregable($_POST["carac_Entregable"]);
    $elemento->setEntregado($_POST["entregado"]);
    $elemento->setValidado($_POST["validado"]);
    if (isset($_POST['id'])){
        $elemento->setId((int)($_POST["id"]));
        $dao=new CatCartaDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    header("Location: ../../../../../Proyectos/CartaCons.php?respuesta=" . $respuesta."&idCarta=".$_POST['id']);
    }else{
        $dao=new CatCartaDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        $idCarta = null;
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
            $idCarta = $dao->obtieneIdentificador();
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    header("Location: ../../../../../Proyectos/CartaCons.php?respuesta=" . $respuesta."&idCarta=".(String)$idCarta);
    }
}else if (isset($_GET['id'])){
    $dao=new CatCartaDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
    header("Location: ../../../../../Proyectos/CartaCons.php?respuesta=" . $respuesta);
} else {
    $respuesta = "No se detecto la acción a realizar.";
    header("Location: ../../../../../Proyectos/CartaCons.php?respuesta=" . $respuesta);
}
?>