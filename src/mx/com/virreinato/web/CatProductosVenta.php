<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/ProductosVentaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProductosVenta.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$respuesta = "";




if (isset($_POST['guardar'])){
	
    $elemento=new ProductosVenta();
	
	
	$elemento->setId_producto($_POST["nom_producto"]); 
	$elemento->setPrecio_unitario($_POST["precio_unitario"]);
	$elemento->setPiezas_vendidas($_POST["piezas_vendidas"]);
	$elemento->setMonto_total($_POST["monto_total"]);
    $elemento->setMonto_museo($_POST["monto_museo"]);
	$elemento->setMonto_patronato($_POST["monto_patronato"]);
    
    if ($_POST["id"] != null){
        $elemento->setId_Venta((int)($_POST["id"]));
        $dao=new ProductosVentaDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new ProductosVentaDaoJdbc();
	$res=$dao->guardaElemento($elemento);
      
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
}else if (isset($_GET['id'])){
    $dao=new ProductosVentaDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
} else {
    $respuesta = "No se detecto la acción a realizar.";
}
header("Location: ../../../../../patronato/lista_patronato.php?respuesta=" . $respuesta);

?>

