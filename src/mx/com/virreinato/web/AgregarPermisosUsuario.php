<?php
session_start();
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatProyecto.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/CatProyectoDaoJdbc.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php?err=2");
}

if(isset($_POST['idPerfil']) && $_POST['idPerfil'] > 0){
	$catalogo = new Catalogo();
	if(isset($_POST['idMenu']) && $_POST['idMenu'] > 0){
		//Perfil = 1 porque él tiene todos los submenus
		$query = "SELECT s.id_submenu, s.nom_submenu FROM sie_cat_submenu s, sie_cat_MSM msm, sie_cat_perfilmenu pm WHERE pm.id_perfil_menu = msm.id_perfil_menu AND msm.id_submenu = s.id_submenu AND pm.id_perfil = 1 AND pm.id_menu = ". $_POST['idMenu'] ." ORDER BY id_submenu;";
		$result = $catalogo->obtenerLista($query);
		if(mysql_num_rows($result) > 0){
			echo('<option value="0">----Selecciona un submenú----</option>');
			while($rs = mysql_fetch_array($result)){
				echo('<option value="' . $rs['id_submenu'] . '">' . $rs['nom_submenu'] . '</option>');
			}
		}else{
			echo('<option value="No">No hay submenus disponibles.<option>');	
		}
		
	}else{
		$query = "SELECT id_menu, nom_menu FROM sie_cat_menu WHERE id_menu != 11 ORDER BY id_menu";
		$result = $catalogo->obtenerLista($query);
		echo ('<option value="0">----Selecciona un menú----</option>');
		while($rs = mysql_fetch_array($result)){
			echo ('<option value="' . $rs['id_menu'] . '">' . $rs['nom_menu'] . '</option>');
		}
	}
}
