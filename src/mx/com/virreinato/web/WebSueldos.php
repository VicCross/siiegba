<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/SueldoGbDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/MinistraDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Presupuesto.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/SueldoGB.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
if(isset($_POST['inicio'])){
    $inicio = $_POST["inicio"];
    $fin = $_POST["fin"];
    $periodo = $_POST["periodo"];
    $tipo_nomina = $_POST["tipo_nomina"];
    
    header("Location: ../../../../../gasto_basico/Sueldos.php?inicio=".$inicio."&fin=".$fin."&periodo=".$periodo."&tipo_nomina=".$tipo_nomina."&recargado=1");
}else if(isset($_POST['idMinistracion'])){
    $elemento = new SueldoGb();

    $elemento->setIdPeriodo((int)($_POST["periodo"]));
    $elemento->setFecha(date("Y-m-d",strtotime($_POST["fecha"])));
    $elemento->setIdMinistracion((int)($_POST["idMinistracion"]));
    $elemento->setTipoNomina($_POST["tipoNomina"]);
    $elemento->setQuincena($_POST["quincena"]);

    $daoMin=new MinistraDaoJdbc();
    $min=$daoMin->obtieneElemento($_POST["idMinistracion"]);
    $elemento->setMonto((float)((String)($min->getMonto())));
    if (isset($_POST['id']) && $_POST["id"] != null){
        $elemento->setId((int)($_POST["id"]));
        $dao=new SueldoGbDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
        header("Location: ../../../../../gasto_basico/Sueldos.php?respuesta=".$respuesta);    
    }else{
        $dao=new SueldoGbDaoJdbc();
	$res=$dao->guardaElementoId($elemento);
        
        if ($res!=0) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
        header("Location: ../../../../../gasto_basico/SueldosGeneraCheques.php?respuesta=".$respuesta."&id=".$res); 
    }
}else if (isset($_GET['id'])){
    $dao=new SueldoGbDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
    header("Location: ../../../../../gasto_basico/Sueldos.php?respuesta=" . $respuesta);
} else {
    $respuesta = "No se detecto la acción a realizar.";
    header("Location: ../../../../../gasto_basico/Sueldos.php?respuesta=" . $respuesta);
}
?>



