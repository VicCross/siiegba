<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/CentroCostosDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CentroCostos.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$respuesta = "";
if (isset($_POST['guardar'])) {
    $elemento = new CentroCostos();
    $elemento->setClave($_POST["cve_ccostos"]);
    $elemento->setDescripcion($_POST["nombre_ccostos"]);
    if ($_POST["id"] != null) {
        $elemento->setId((int) ($_POST['id']));
        $dao = new CentroCostosDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    } else {
        $dao = new CentroCostosDaoJdbc();
        $res = $dao->guardaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
    //header("Location: ../../../../../catalogos/lista_ccostos.php");
} else if (isset($_GET['id'])) {
    $dao = new CentroCostosDaoJdbc();
    $res = $dao->eliminaElemento((int) ($_GET['id']));
    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
} else {
    $respuesta = "No se detecto la acción a realizar.";
}
header("Location: ../../../../../catalogos/lista_ccostos.php?respuesta=" . $respuesta);
?>