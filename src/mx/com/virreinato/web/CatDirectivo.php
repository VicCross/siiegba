<?php

if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Directivo.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/CentroCostosDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/DirectivoDaoJdbc.class.php");

if(isset($_POST['guardar'])){
    
    $elemento=new Directivo();
    $elemento->setNombre($_POST["nombre"]);
    $elemento->setApPaterno($_POST["apaterno"]);
    $elemento->setApMaterno($_POST["amaterno"]);
    $elemento->setCargo($_POST["cargo"]);
    $elemento->setTipoCargo((int)($_POST["tipo_cargo"]));
    $elemento->setIniciales($_POST["iniciales"]);
    
    $cdao=new CentroCostosDaoJdbc();
    $ccostos= $cdao->obtieneElemento($_REQUEST["id_ccosto"]);
    $elemento->setCcostos($ccostos);
    if($_POST["id"]!= null){
            $elemento->setId((int)($_POST["id"]));
            $dao=new DirectivoDaoJdbc();
            $res=$dao->actualizaElemento($elemento);
            if($res)
            {        $respuesta = "Su información se actualizó exitosamente."; }
            else
            {        $respuesta = "No fue posible actualizar su información."; }
    }
    else {
            $dao=new DirectivoDaoJdbc();
            $res=$dao->guardaElemento($elemento);

            if($res)
            {       $respuesta = "Su información se almacenó exitosamente."; }
            else
            {       $respuesta = "No fue posible almacenar su información."; }
    }
}else if (isset($_GET['id'])) {
    $dao = new DirectivoDaoJdbc();
    $res = $dao->eliminaElemento((int) ($_GET['id']));
    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
} else {
    $respuesta = "No se detecto la acción a realizar.";
}
header("Location: ../../../../../catalogos/lista_directivo.php?respuesta=" . $respuesta);
?>


