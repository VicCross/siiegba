<?php
session_start();
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: ../../index.php");
}
include_once("../beans/Empleado.class.php");
include_once("../beans/Comision.class.php");
include_once("../dao/ComisionDaoJdbc.class.php");

if(isset($_POST['inicioFecha']) && $_POST['inicioFecha']!=""){
    $inicio = $_POST["inicioFecha"];
    $fin= $_POST["finFecha"];
    header("Location: ../../../../../Formatos/lista_Comision.php?inicio=".$inicio."&fin=".$fin);
}else if(isset ($_POST['ccosto'])){
    $elemento= new Comision();
    $elemento->setIdCCosto((int)($_REQUEST["ccosto"]));
    $fechaof=date("Y-m-d",strtotime($_POST["Fecha"]));

    $elemento->setFechaOficio($fechaof);
    $elemento->setNumeroOficio($_POST["numeroOficio"]);
									
    $elemento->setPeriodoInicial(date("Y-m-d",strtotime($_POST["FechaInicio"])));
    $elemento->setPeriodoFinal(date("Y-m-d",strtotime($_POST["FechaFinal"])));
    $elemento->setObjetivo($_POST["objetivo"]);
    $elemento->setResultados($_POST["resultados"]);
    $elemento->setLugar($_POST["lugar"]);
    $elemento->setNacional((int)($_POST["destino"]));

    $elemento->setTarifaDiaria((float)($_POST["tarifaDiaria"]));
    $elemento->setNumeroDias((int)($_POST["numDias"]));
    $elemento->setMontoTotal((float)($_POST["montoTotal"]));
    
    if($_POST["montoDocumentos"]!=null && $_POST["montoDocumentos"]!="")
    {    $elemento->setMontoDoctos((float)($_POST["montoDocumentos"])); }
    else
    {        $elemento->setMontoDoctos((float)("0.0")); }

    if($_POST["montoSinDocumentos"]!=null && $_POST["montoSinDocumentos"]!="")
    {        $elemento->setMontoSinDoctos((float)($_POST["montoSinDocumentos"])); }
    else
    {        $elemento->setMontoSinDoctos((float)("0.0")); }

    if($_POST["saldoPendiente"]!=null && $_POST["saldoPendiente"]!="")
    {        $elemento->setSaldoPendiente((float)($_POST["saldoPendiente"])); }
    else
    {       $elemento->setSaldoPendiente((float)("0.0")); }

    if($_POST["montoSinDocumentosLetra"]!=null && $_POST["montoSinDocumentosLetra"]!="")
    {        $elemento->setLetraSinDoctos($_POST["montoSinDocumentosLetra"]); }
    else
    {        $elemento->setLetraSinDoctos(""); }

    $elemento->setEstatus(1);
    
    $emp=new Empleado();
    $emp->setId((int)($_POST["empleado"]));
    $elemento->setEmpleado($emp);

    if (isset($_POST['id']) && $_POST["id"] != null){
        $elemento->setId((int)($_POST["id"]));
        $dao=new ComisionDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new ComisionDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
    header("Location: ../../../../../Formatos/lista_Comision.php?respuesta=" . $respuesta);
}else if (isset($_GET['id'])){
    $dao=new ComisionDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
    header("Location: ../../../../../Formatos/lista_Comision.php?respuesta=" . $respuesta);
} else {
    $respuesta = "No se detecto la acción a realizar.";
    header("Location: ../../../../../Formatos/lista_Comision.php?respuesta=" . $respuesta);
}
?>