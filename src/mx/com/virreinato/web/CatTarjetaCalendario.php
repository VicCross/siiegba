<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/TarjetaCalendarioDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/TarjetaCalendario.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$respuesta = "";
if (isset($_POST['id_Tarjeta'])){
    $elemento=new TarjetaCalendario();
    $elemento->setNum_Partida((int)($_POST["partida_Calendario"]));
    $elemento->setIdMeta((int)($_POST["idMeta"]));
    if( $_POST["enero_Calendario"] != "" ) $elemento->setEnero((float)($_POST["enero_Calendario"])); else $elemento->setEnero(0);
    if( $_POST["febrero_Calendario"] != "" ) $elemento->setFebrero((float)($_POST["febrero_Calendario"])); else $elemento->setFebrero(0);
    if( $_POST["marzo_Calendario"] != "" ) $elemento->setMarzo((float)($_POST["marzo_Calendario"])); else $elemento->setMarzo(0);
    if( $_POST["abril_Calendario"] != "" ) $elemento->setAbril((float)($_POST["abril_Calendario"])); else $elemento->setAbril(0);
    if( $_POST["mayo_Calendario"] != "" ) $elemento->setMayo((float)($_POST["mayo_Calendario"])); else $elemento->setMayo(0);
    if( $_POST["junio_Calendario"] != "" ) $elemento->setJunio((float)($_POST["junio_Calendario"])); else $elemento->setJunio(0);
    if( $_POST["julio_Calendario"] != "" ) $elemento->setJulio((float)($_POST["julio_Calendario"])); else $elemento->setJulio(0);
    if( $_POST["agosto_Calendario"] != "" ) $elemento->setAgosto((float)($_POST["agosto_Calendario"])); else $elemento->setAgosto(0);
    if( $_POST["septiembre_Calendario"] != "" ) $elemento->setSeptiembre((float)($_POST["septiembre_Calendario"])); else $elemento->setSeptiembre(0);
    if( $_POST["octubre_Calendario"] != "" ) $elemento->setOctubre((float)($_POST["octubre_Calendario"])); else $elemento->setOctubre(0);
    if( $_POST["noviembre_Calendario"] != "" ) $elemento->setNoviembre((float)($_POST["noviembre_Calendario"])); else $elemento->setNoviembre(0);
    if( $_POST["diciembre_Calendario"] != "" ) $elemento->setDiciembre((float)($_POST["diciembre_Calendario"])); else $elemento->setDiciembre(0);
    $elemento->setIdTarjeta((int)($_POST["id_Tarjeta"]));
    if ($_POST["id"] != null){
        $elemento->setIdCalendario((int)($_POST["id"]));
        $dao=new TarjetaCalendarioDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new TarjetaCalendarioDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
    header("Location: ../../../../../Proyectos/lista_Calendario.php?respuesta=" . $respuesta."&idTarjeta=".$_POST['id_Tarjeta']);
}else if (isset($_GET['id'])){
    $dao=new TarjetaCalendarioDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
    header("Location: ../../../../../Proyectos/lista_Calendario.php?respuesta=" . $respuesta."&idTarjeta=".$_GET['id_Tarjeta']);
} else {
    $respuesta = "No se detecto la acción a realizar.";
    header("Location: ../../../../../Proyectos/lista_Calendario.php?respuesta=" . $respuesta);
}
?>


