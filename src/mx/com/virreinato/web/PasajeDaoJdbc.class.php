<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Pasaje.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Empleado.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class PasajeDaoJdbc {
    
    public function obtieneListado($inicio,$fin) {
		
	$lista= array();
		
	$query=strtolower("SELECT * FROM sie_pasajes P, CAT_EMPLEADO E  WHERE P.CEM_ID_EMPLEADO=E.CEM_ID_EMPLEADO AND P.pas_estatus=1 AND PAS_FECHA_MIN >= '".date("Y-m-d",strtotime($inicio))."' AND PAS_FECHA_MIN <= '".date("Y-m-d",strtotime($fin))."' ORDER BY E.CEM_NOMBRE, E.CEM_APPATERNO, E.CEM_APMATERNO ");
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("id_pasajes")];
            $id_ccosto= $rs[strtoupper("ccc_id_ccosto")];
            $fecha = $rs[strtoupper("pas_fecha_min")];
            $mes= $rs[strtoupper("Pas_mes")];
            $tarifadiaria = $rs[strtoupper("pas_tarifadiaria")];
            $numdias= $rs[strtoupper("pas_numdias")];
            $montotal = $rs[strtoupper("pas_montotal")];
            $letra= $rs[strtoupper("pas_letramonto")];
            $concepto= $rs[strtoupper("pas_concepto")];
            $estatus= $rs[strtoupper("pas_estatus")];

            $idEmp= $rs[strtoupper("cem_id_empleado")];
            $nombre= $rs[strtoupper("cem_nombre")];
            $apaterno= $rs[strtoupper("cem_appaterno")];
            $amaterno= $rs[strtoupper("cem_apmaterno")];
            $sueldoNeto= $rs[strtoupper("cem_sueldo_neto")];
            $sueldoLetra= $rs[strtoupper("cem_sueldo_letra")];
            $puesto= $rs[strtoupper("cem_puesto")];
            $rfc= $rs[strtoupper("cem_rfc")];
            $tipoNomina= $rs[strtoupper("cem_tipo_nomina")];
            $numero= $rs[strtoupper("cem_numero_emp")];

            $empleado = new Empleado();
            $empleado->setAll($idEmp,$nombre,$apaterno,$amaterno,$sueldoNeto,$sueldoLetra,$puesto,$rfc,$tipoNomina,$numero);

            $elemento = new Pasaje();
            $elemento->setAll($id,$id_ccosto,$fecha,$mes, $tarifadiaria,$numdias,$montotal,$letra,$concepto,$estatus,$empleado);
            array_push($lista, $elemento);
        }	
	return $lista;
    }
    
    public function obtieneElemento($idElemento) {
		
	$elemento=new Pasaje();

	$query=strtolower("SELECT * FROM sie_pasajes P, CAT_EMPLEADO E  WHERE P.CEM_ID_EMPLEADO=E.CEM_ID_EMPLEADO AND P.id_pasajes=".$idElemento." ");
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("id_pasajes")];
            $id_ccosto= $rs[strtoupper("ccc_id_ccosto")];
            $fecha = $rs[strtoupper("pas_fecha_min")];
            $mes= $rs[strtoupper("Pas_mes")];
            $tarifadiaria = $rs[strtoupper("pas_tarifadiaria")];
            $numdias= $rs[strtoupper("pas_numdias")];
            $montotal = $rs[strtoupper("pas_montotal")];
            $letra= $rs[strtoupper("pas_letramonto")];
            $concepto= $rs[strtoupper("pas_concepto")];
            $estatus= $rs[strtoupper("pas_estatus")];

            $idEmp= $rs[strtoupper("cem_id_empleado")];
            $nombre= $rs[strtoupper("cem_nombre")];
            $apaterno= $rs[strtoupper("cem_appaterno")];
            $amaterno= $rs[strtoupper("cem_apmaterno")];
            $sueldoNeto= $rs[strtoupper("cem_sueldo_neto")];
            $sueldoLetra= $rs[strtoupper("cem_sueldo_letra")];
            $puesto= $rs[strtoupper("cem_puesto")];
            $rfc= $rs[strtoupper("cem_rfc")];
            $tipoNomina= $rs[strtoupper("cem_tipo_nomina")];
            $numero= $rs[strtoupper("cem_numero_emp")];

            $empleado = new Empleado();
            $empleado->setAll($idEmp,$nombre,$apaterno,$amaterno,$sueldoNeto,$sueldoLetra,$puesto,$rfc,$tipoNomina,$numero);

            $elemento = new Pasaje();
            $elemento->setAll($id,$id_ccosto,$fecha,$mes, $tarifadiaria,$numdias,$montotal,$letra,$concepto,$estatus,$empleado);
        }	
	return $elemento;
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $empleado = $elemento->getEmpleado();
        $query="INSERT INTO sie_pasajes(ccc_id_ccosto, pas_fecha_min, Pas_mes, pas_tarifadiaria, pas_numdias, " .
				"pas_montotal, pas_letramonto, pas_concepto, pas_estatus, cem_id_empleado)".
				" VALUES ( ".$elemento->getIdCCosto().", '".date("Y-m-d",strtotime($elemento->getFecha()))."', '".date("Y-m-d",strtotime($elemento->getMes()))."', ".$elemento->getTarifaDiaria().", ".$elemento->getNumeroDias().", " .
				" ".$elemento->getMontoTotal().", '".$elemento->getLetraMonto()."', '".$elemento->getConcepto()."', ".$elemento->getEstatus().", ".$empleado->getId()."  )";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
        $empleado = $elemento->getEmpleado();
	$query="UPDATE sie_pasajes set  ccc_id_ccosto=".$elemento->getIdCCosto().", pas_fecha_min='".date("Y-m-d",strtotime($elemento->getFecha()))."', Pas_mes='".date("Y-m-d",strtotime($elemento->getMes()))."', " .
				" pas_tarifadiaria=".$elemento->getTarifaDiaria().", pas_numdias=".$elemento->getNumeroDias().", pas_montotal=".$elemento->getMontoTotal().", pas_letramonto='".$elemento->getLetraMonto()."'," .
				" pas_concepto='".$elemento->getConcepto()."', pas_estatus=".$elemento->getEstatus().", cem_id_empleado=".$empleado->getId()." WHERE id_pasajes=".$elemento->getId();
        $res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_pasajes set pas_estatus=0 WHERE id_pasajes=".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}
