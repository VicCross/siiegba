<?php
	session_start();
	include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/IngresosPatronato.class.php");
	include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");
	class IngresosPatronatoDaoJdbc{
		public function obtieneLista(){
			$catalogo = new Catalogo();
			$lista = array();
			$query = "SELECT * FROM sie_ingresos_patronato ORDER BY id_ingresosP";
			$result = $catalogo->obtenerLista($query);
			while($rs = mysql_fetch_array($result)){
				$elemento = new IngresosPatronato();
				$elemento->setAll($rs[0], $rs[1], $rs[2], $rs[3], $rs[4], $rs[5], $rs[6]);	
				array_push($lista, $elemento);
			}
			return $lista;
		}
		
		public function obtieneElemento($id){
			$catalogo = new Catalogo();
			$elemento = new IngresosPatronato();
			$query = "SELECT * FROM sie_ingresos_patronato WHERE id_ingresosP = " . $id . " ORDER BY id_ingresosP";
			$result = $catalogo->obtenerLista($query);
			if($rs = mysql_fetch_array($result)){
				$elemento->setAll($rs[0], $rs[1], $rs[2], $rs[3], $rs[4], $rs[5], $rs[6]);
			}
			return $elemento;
		}
		
		public function guardaElemento($elemento){
			$catalogo = new Catalogo();
			$ponemosExpoTemp = ""; $expoTemp = "";
			$ponemosArchivo = ""; $archivo = "";
			if($elemento->getIdProyecto() == 48){
				$ponemosExpoTemp = ", idExpoTemp";
				$expoTemp = ", " . $elemento->getIdExpoTemp();	
			}
			if(!is_null($elemento->getArchivo())){
				$ponemosArchivo = ", archivo";
				$archivo = ", '" . $elemento->getArchivo() . "'";
			}
			$insert = "INSERT INTO sie_ingresos_patronato (nombrePatrocinador, monto, fecha, idProyecto" . $ponemosExpoTemp . $ponemosArchivo . ") VALUES ('" . $elemento->getPatrocinador() . "', " . $elemento->getMonto() . ", curdate(), " . $elemento->getIdProyecto() . $expoTemp . $archivo . ")";
			$res = $catalogo->obtenerLista($insert);
			if($res != "1") return false;
			$update = "UPDATE sie_cat_linea_accion SET saldoInicial = saldoInicial + " . $elemento->getMonto() . " , saldoAnterior = saldoAnterior + " . $elemento->getMonto() . " WHERE cla_id_lineaaccion = " . $elemento->getIdProyecto();
			$insert = $insert . " " . $update;
			$res = $catalogo->obtenerLista($update);			
			if($res != "1") return false;
			if($elemento->getIdProyecto() == 48){
				$update = "UPDATE sie_cat_expo_temp SET saldo = saldo + " . $elemento->getMonto() . ", saldoAnterior = saldoAnterior + " . $elemento->getMonto() . " WHERE id_Subproyecto = " . $elemento->getIdExpoTemp();	
				$res = $catalogo->obtenerLista($update);	
				if($res == "1") return true;
				else return false;
			}else return true;
			
		}
		
		public function eliminaElemento($elemento){
			$catalogo = new Catalogo();
			$delete = "DELETE FROM sie_ingresos_patronato WHERE id_ingresosP = " . $elemento->getId();
			$res = $catalogo->obtenerLista($delete);
			if($res == "1"){
				$update = "UPDATE sie_cat_linea_accion SET saldoInicial = saldoInicial - " . $elemento->getMonto() . ", saldoAnterior = saldoAnterior - " . $elemento->getMonto() . " WHERE cla_id_lineaaccion = " . $elemento->getIdProyecto();
				$catalogo->obtenerLista($update);
				if($elemento->getIdProyecto() == 48){
					$update = "UPDATE sie_cat_expo_temp SET saldo = saldo - " . $elemento->getMonto() . ", saldoAnterior = saldoAnterior - " . $lelemento->getMonto() . " WHERE id_Subproyecto = " . $elemento->getIdExpoTemp();	
					$catalogo->obtenerLista($update);
				}
				
				return true;	
			}
			else return false;
		}
		
	}
?>