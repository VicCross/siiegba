<?php

session_start();
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/PresupuestoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/SueldoGbDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/SueldoGB.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/DetalleSueldoGB.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/DetalleSueldoGbDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/MinistraDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Ministra.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/CatChequeDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatCheque.class.php");

if(isset($_POST['inicio'])){
    $inicio = $_POST["inicio"];
    $fin = $_POST["fin"];
    $periodo = $_POST["periodo"];
    $estatus = $_POST["estatus"];
    
    header("Location: ../../../../../Proyectos/Solicitud.php?inicio=".$inicio."&fin=".$fin."&periodo=".$periodo."&estatus=".$estatus);
}else if(isset($_POST['id_empleado1'])){
        
    $numero = (int)$_POST['numero'];
    $j = 1;
    for($j=1; $j<$numero; $j++){
        $foliosEmpleado[$j-1] = $_POST["id_empleado".$j];
        $montos[$j-1] = $_POST["monto".$j];
        $letras[$j-1] = $_POST["letra".$j];
        $cheques[$j-1] = $_POST["cheque".$j];
    }
    
    $i = 0;
    for($i = 0; $i < count($foliosEmpleado); $i++) {
        
        if($montos[$i]!=null && $montos[$i]!="" && $letras[$i]!=null && $letras[$i]!="" && $cheques[$i]!=null && $cheques[$i]!="" ){
            $elemento = new CatCheque();
            $detalle = new DetalleSueldoGb();

            $elemento->setArea((int)($_POST["area"]));
            $elemento->setDestino("SL");
            $elemento->setEstatus("EM");
            $elemento->setFechaEmision(date("Y-m-d",strtotime($_POST["fecha"])));

            $elemento->setIdCuenta((int)($_POST["cuenta"]));
            $elemento->setIdPeriodo((int)($_POST["idPeriodo"]));
            //$elemento->setIdProyecto(0);
            $elemento->setIdSolicitud((int)($_POST["idSueldo"]));
            $elemento->setFolio((int)($cheques[$i]));
            
            $monto = $montos[$i];
            $monto = str_replace("$", ' ', $monto);
            
            $monto = str_replace(","," ",$monto);
            $elemento->setMonto((float)($monto));
            $elemento->setMontoLetra($letras[$i]);
            $elemento->setNum_Poliza((int)($cheques[$i]));
            $elemento->setObservaciones("");
            $elemento->setIdEmpleado((int)($foliosEmpleado[$i]));
            
            $detalle->setIdSueldo((int)($_POST["idSueldo"]));
            $detalle->setFolioCheque($cheques[$i]);
            $detalle->setMontoLetra($letras[$i]);
            $detalle->setIdEmpleado((int)($foliosEmpleado[$i]));
            $detalle->setSueldo((float)($monto));
            
            $dao = new CatChequeDaoJdbc();
            $res = $dao->guardaElemento($elemento);

            $daod = new DetalleSueldoGbDaoJdbc();
            $resd = $daod->guardaElemento($detalle);
            
            if ($res) {
                $respuesta = "Su información se actualizó exitosamente.";
            } else {
                $respuesta = "No fue posible actualizar su información.";
            }
        }
    }
    header("Location: ../../../../../gasto_basico/SueldosGeneraCheques.php?respuesta=".$respuesta."&idSueldo=".$_POST['idSueldo']); 
}else if(isset($_GET['id'])){
    $dao=new PresupuestoDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
    header("Location: ../../../../../Proyectos/Solicitud.php?respuesta=".$respuesta); 
} else {
    $respuesta = "No se detecto la acción a realizar.";
}


