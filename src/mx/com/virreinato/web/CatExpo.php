<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/ExposicionesTemporalesDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ExposicionesTemporales.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$respuesta = "";
if (isset($_POST['guardar'])){
    $elemento=new ExposicionesTemporales();
    $elemento->setNombre($_POST["nombre"]);
	$elemento->setFechaI($_POST["fechaI"]);
	$elemento->setFechaF($_POST["fechaF"]);
	$elemento->setTituloFinal($_POST["tituloFinal"]);
	$elemento->setPeriodo($_POST["periodo"]);
	$elemento->setSaldo($_POST["saldo"]);
	$elemento->setSaldoAnterior($_POST["saldoAnterior"]);
	
	
    
    
    if ($_POST["id"] != null){
        $elemento->getIdExposicionT((int)($_POST["id"]));
        $dao=new ExposicionesTemporalesDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new ExposicionesTemporalesDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
}else if (isset($_GET['id'])){
    $dao=new ExposicionesTemporalesDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
} else {
    $respuesta = "No se detecto la acción a realizar.";
}
header("Location: ../../../../../catalogos/lista_expo.php?respuesta=" . $respuesta);
?>


