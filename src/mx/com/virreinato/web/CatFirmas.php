<?php
session_start();
//include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatProyecto.class.php");
//include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/CatProyectoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/LineaAccionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/LineaAccion.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/AreaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Area.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/EmpleadoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Empleado.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php?err=2");
}
if(isset($_POST['opc']) && $_POST['opc'] > 0 && isset($_POST['idAP']) && $_POST['idAP'] > 0){
	$daoEmpleado = new EmpleadoDaoJdbc();
	$empleado = new Empleado();
	echo('<option value="0">---- Selecciona ---- </option>');
	if($_POST['opc'] == 1){//proyecto
		$dao = new LineaAccionDaoJdbc();
		$elemento = new LineaAccion();
		$elemento = $dao->obtieneElemento($_POST['idAP']);
		$empleado = $daoEmpleado->obtieneElemento($elemento->getIdLider());
		if(is_null($elemento->getFirma())){//solo listamos los proyectos cuyos lideres no tienen firma
			echo('<option value="' . $elemento->getIdLider() . '">' . $empleado->getNombre() . " " . $empleado->getApPaterno() . " " . $empleado->getApMaterno() . '</option>');
		}
	}else if($_POST['opc'] == 2){//Área
		$dao = new AreaDaoJdbc();
		$elemento = new Area();
		$elemento = $dao->obtieneArea($_POST['idAP']);
		$empleado = $daoEmpleado->obtieneElemento($elemento->getIdLider());
		if(is_null($elemento->getFirma())){//solo listamos los proyectos cuyos lideres no tienen firma
			echo('<option value="' . $elemento->getIdLider() . '">' . $empleado->getNombre() . " " . $empleado->getApPaterno() . " " . $empleado->getApMaterno() . '</option>');
		}	
	}
}else if(isset($_POST['opc']) && $_POST['opc'] > 0){
	echo('<option value="0">---- Selecciona ---- </option>');
	if($_POST['opc'] == 1){//proyecto
		$dao = new LineaAccionDaoJdbc();
		$lista = $dao->obtieneListado();
		$elemento = new LineaAccion();
		foreach($lista as $elemento){
			echo('<option value="' . $elemento->getId() . '">' . $elemento->getLineaAccion() . '</option>');
		}
	}else if($_POST['opc'] == 2){//Área
		$dao = new AreaDaoJdbc();
		$lista = $dao->obtieneAreasFiltro();
		$elemento = new Area();
		foreach($lista as $elemento){
			echo('<option value="' . $elemento->getId() . '">' . $elemento->getDescripcion() . '</option>');
		}	
	}	
}

