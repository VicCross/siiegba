<?php

if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Area.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/AreaDaoJdbc.class.php");

if(isset($_POST['guardar'])){
    $area=new Area();
	$dao = new AreaDaoJdbc();
    
    if($_POST["id"]!= null){
		$area = $dao->obtieneArea($_POST['id']);
		$area->setDescripcion($_POST['area']);
		$area->setIdLider((int)($_POST["lider"]));       
        $res = $dao->actualizaArea($area);
        if($res)
        {        $respuesta = "Su información se actualizó exitosamente."; }
        else
        {        $respuesta = "No fue posible actualizar su información."; }
    }
    else{
		$area->setDescripcion($_POST['area']);
		$area->setIdLider((int)($_POST["lider"]));
		$res = $dao->guardaArea($area);
        if($res)
        {       $respuesta = "Su información se almacenó exitosamente."; }
        else
        {       $respuesta = "No fue posible almacenar su información."; }
    }
}else if (isset($_GET['id'])){
    $dao = new AreaDaoJdbc();
    $res= $dao->eliminaArea((int)($_GET['id']));
    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
}else{
    $respuesta = "No se detecto la acción a realizar.";
}
header("Location: ../../../../../catalogos/lista_area.php?respuesta=" . $respuesta);
?>
