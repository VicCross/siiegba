<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/ExpoTempDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatExpoTemp.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$respuesta = "";
if (isset($_POST['guardar'])){
    $elemento=new ExpoTemp();
    $elemento->setNombre($_POST["nombre"]);
    $elemento->setInicio($_POST["inicio"]);
    $elemento->setFin($_POST["fin"]);
	$elemento->setTitulo($_POST["titulo"]);
	$elemento->setTipo($_POST["tipo"]);
	$elemento->setPeriodo($_POST["periodo"]);
	$elemento->setArea($_POST["area"]);
	$elemento->setAccion($_POST["accion"]);
	$elemento->setSaldo($_POST["saldo"]);
    if ($_POST["id"] != null){
        $elemento->setId((int)($_POST["id"]));
        $dao=new ExpoTempDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new ExpoTempDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
}else if (isset($_GET['id'])){
    $dao=new ExpoTempDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
} else {
    $respuesta = "No se detecto la acción a realizar.";
}
header("Location: ../../../../../catalogos/lista_proveedor.php?respuesta=" . $respuesta);
?>


