<?php
session_start();
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");

if (isset($_POST['guardar'])){
    $elemento = new Periodo();
    $elemento->setPeriodo($_POST["periodo"]);
    if(isset($_POST['cerrado']) && $_POST['cerrado']!=null){
        $elemento->setCerrado(1);
    }	
    else
    {    $elemento->setCerrado(0); }
    $finicio = date("d/m/Y", strtotime($_POST["finicio"]));
    $ffin= date("d/m/Y", strtotime($_POST["ffin"]));

    $elemento->setFechaInicial($finicio);
    $elemento->setFechaFinal($ffin);
    
    if($_POST["id"]!= null){
        $elemento->setId((int)($_POST["id"]));
        $dao=new PeriodoDaoJdbc();
        $res= $dao->actualizaElemento($elemento);
        if($res)
        {        $respuesta = "Su información se actualizó exitosamente."; }
        else
        {        $respuesta = "No fue posible actualizar su información."; }
    }else{
        $dao=new PeriodoDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        if($res)
        {       $respuesta = "Su información se almacenó exitosamente."; }
        else
        {       $respuesta = "No fue posible almacenar su información."; }
    }
}
else if(isset($_GET['id'])){
    $dao=new PeriodoDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
}else{
    $respuesta = "No se detecto la acción a realizar.";
}
header("Location: ../../../../../catalogos/lista_periodo.php?respuesta=" . $respuesta);
?>