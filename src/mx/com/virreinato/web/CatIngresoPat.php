<?php
	session_start();
	include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/IngresosPatronatoDaoJdbc.class.php");
	include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/IngresosPatronato.class.php");
	if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    	header("Location: " . $_SESSION['RAIZ'] . "/index.php?opc=2");
	}
	$respuesta = "";
	if(isset($_POST['guardar'])){
		$elemento = new IngresosPatronato();
		$elemento->setPatrocinador($_POST['patrocinador']);
		$elemento->setMonto($_POST['monto']);
		$elemento->setPeriodo($_POST['periodo']);
		$elemento->setId_Tipo_Amigos($_POST['tipo_ingreso']);
		$elemento->setIdProyecto($_POST['proyecto']);
		//checamos si el proyecto es una exposicion temporal
		$elemento->setIdExpoTemp($_POST['expotemp']);
		//Para subir el archivo en caso de que se haya agregado.
		if(!is_null($_FILES['archivo']) && $_FILES['archivo']['tmp_name'] != ""){
			$carpeta = "/ArchivosIngresos/Patronato/";
			$nomArchivo = $_FILES['archivo']['name'];
			opendir($_SESSION['RAIZ'] . $carpeta);
			$destino = ".." . $carpeta . $nomArchivo;
			copy($_FILES['archivo']['tmp_name'], $_SESSION['RAIZ'] . $carpeta . $nomArchivo);
			$elemento->setArchivo($destino);	
		}
		if(isset($_POST['id'])){
			$elemento->setId($_POST['id']);
			$dao = new IngresosPatronatoDaoJdbc();
			$res = $dao->modificaElemento($elemento);
			if($res) $respuesta = "Registro modificado con éxito.";	
			else $respuesta = "El registro no pudo modificarse con éxito.";
		}else{
			$dao = new IngresosPatronatoDaoJdbc();
			$res = $dao->guardaElemento($elemento);
			if($res) $respuesta = "Éxito al guardar el ingreso.";
			else $respuesta = "Error al guardar el ingreso. Inténtelo más tarde"; 
			//echo($res);
		}
	}else if(isset($_GET['id'])){
		$dao = new IngresosPatronatoDaoJdbc();
		$elemento = new IngresosPatronato();
		$elemento = $dao->obtieneElemento($_GET['id']);
		$res = $dao->eliminaElemento($elemento);
		if($res) $respuesta = "Registro borrado con éxito.";	
	}
	
	header("Location: ../../../../../catalogos/lista_ingresosPatronato.php?respuesta=".$respuesta);
?>