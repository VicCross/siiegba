<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/TarjetaRegistroDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/TarjetaRegistro.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$respuesta = "";
if(isset($_POST['AnioTarjeta'])){
    $anio = $_POST['AnioTarjeta'];
    header("Location: ../../../../../Proyectos/lista_TarjetaRegistro.php?anioTarjeta=".$anio);
}else if( isset($_POST["obtenerMetas"])){

    $query = "SELECT proyecto_id_meta, proyecto_desc_meta FROM sie_proyecto_metas WHERE cpr_id_proyecto = ".$_POST["obtenerMetas"];
    $catalogo = new Catalogo();
        
    $result = $catalogo->obtenerLista($query);

    while ($rs = mysql_fetch_array($result)){
        $sel = "";
        if( (int)($_POST["sel"]) != 0 && (int)($_POST["sel"]) == $rs["proyecto_id_meta"] ){ $sel = "selected='selected'";  }
        echo("<option value='".$rs["proyecto_id_meta"]."' ".$sel.">".$rs["proyecto_desc_meta"]."</option>");
    }
}else if(isset($_POST['nombre_Proyecto'])){
    $elemento = new TarjetaRegistro();

    $elemento->setIdProyecto((int)($_POST["nombre_Proyecto"]));
    $elemento->setIdMeta(1);
    $elemento->setUnidadAdministrativa($_POST["unidadAdministrativa"]);
    $elemento->setResponsable($_POST["id_Responsable"]);
    $elemento->setClaveTecnica($_POST["claveTecnica"]);
    $elemento->setClaveAccion($_POST["claveLineaAccion"]);
    $elemento->setAutorizadoSinFinanciamiento($_POST["autorizadoSinFinanciamiento"]);
    if( $_POST["elaboracion_Tarjeta"] != "" ) $elemento->setFechaElaboracion(date("Y-m-d",strtotime( $_POST["elaboracion_Tarjeta"])) );
    if( $_POST["autorizacion_Tarjeta"]!= "" ) $elemento->setFechaAutorizacion(date("Y-m-d",strtotime($_POST["autorizacion_Tarjeta"])));
    $elemento->setFuncion($_POST["funcion"]);
    $elemento->setSubFuncion($_POST["Subfuncion"]);
    $elemento->setProgramaGeneral($_POST["programaGeneral"]);
    $elemento->setActividadInstitucional($_POST["act_Institucional"]);
    $elemento->setActividadPrioritaria($_POST["act_Prioritaria"]);
    $elemento->setTipoProyecto($_POST["tipoProyecto"]);
    $elemento->setConcluir($_POST["seguimiento_Programado"]);
    $elemento->setReactivado($_POST["seguimiento_Reactivado"]);
    $elemento->setProductoPrincipal($_POST["producto"]);
    $elemento->setObjetivo($_POST["objetivo"]);
    $elemento->setJustificacion($_POST["justificacion"]);
    $elemento->setRecursos_Terceros($_POST["recursos_Nombre"]);
    if( $_POST["del1"]!= "" ) $elemento->setDel_Fase1((int)($_POST["del1"]));
    if( $_POST["del2"]!= "" ) $elemento->setDel_Fase2((int)($_POST["del2"]));
    if( $_POST["del3"]!= "" ) $elemento->setDel_Fase3((int)($_POST["del3"]));
    if( $_POST["del4"]!= "" ) $elemento->setDel_Fase4((int)($_POST["del4"]));
    if( $_POST["del5"]!= "" ) $elemento->setDel_Fase5((int)($_POST["del5"]));
    if( $_POST["del6"]!= "" ) $elemento->setDel_Fase6((int)($_POST["del6"]));
    if( $_POST["al1"]!= "" ) $elemento->setAl_Fase1((int)($_POST["al1"]));
    if( $_POST["al2"]!= "" ) $elemento->setAl_Fase2((int)($_POST["al2"]));
    if( $_POST["al3"]!= "" ) $elemento->setAl_Fase3((int)($_POST["al3"]));
    if( $_POST["al4"]!= "" ) $elemento->setAl_Fase4((int)($_POST["al4"]));
    if( $_POST["al5"]!= "" ) $elemento->setAl_Fase5((int)($_POST["al5"]));
    if( $_POST["al6"]!= "" ) $elemento->setAl_Fase6((int)($_POST["al6"]));
    if( $_POST["total_Modificado"] != "" ) $elemento->setCostoTotal_Modificado((float)($_POST["total_Modificado"]));
    $elemento->setProductoPrincipal_Modificado($_POST["prod_PrincipalMod"]);
    $elemento->setLineaAccion_Modificado($_POST["linea_Modificado"]);
    $elemento->setNombreProyecto_Modificado($_POST["nombre_Modificado"]);
    $elemento->setPermanente_Modificado($_POST["Permanente"]);
    $elemento->setConcluir_Modificado($_POST["concluir_Modificado"]);
    $elemento->setAnio_Modificacion($_POST["anio_Modificado"]);
    $elemento->setAl_Modificacion($_POST["al_Modificado"]);
    $elemento->setAcumulado($_POST["acumuladoAnterior"]);
    $elemento->setProgramado($_POST["Programado_X"]);
    $elemento->setElabora($_POST["elabora_Tarjeta"]);
    $elemento->setPropone($_POST["propone_Tarjeta"]);
    $elemento->setAutoriza_Tecnico($_POST["autorizaTecnico_Tarjeta"]);
    $elemento->setAutoriza_Adminstrativo($_POST["adminsitrativo_Tarjeta"]);
    if (isset($_POST['id'])){
        $elemento->setId((int)($_POST["id"]));
        $dao=new TarjetaRegistroDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    header("Location: ../../../../../Proyectos/lista_TarjetaRegistro.php?respuesta=" . $respuesta."&idTarjeta=".$_POST['id']);
    }else{
        $dao=new TarjetaRegistroDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        $idCarta = null;
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    header("Location: ../../../../../Proyectos/lista_TarjetaRegistro.php?respuesta=" . $respuesta);
    }
}else if (isset($_GET['id'])){
    $dao=new TarjetaRegistroDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
    header("Location: ../../../../../Proyectos/lista_TarjetaRegistro.php?respuesta=" . $respuesta);
} else {
    $respuesta = "No se detecto la acción a realizar.";
    header("Location: ../../../../../Proyectos/lista_TarjetaRegistro.php?respuesta=" . $respuesta);
}
?>