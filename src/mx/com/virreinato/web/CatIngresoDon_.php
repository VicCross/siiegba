<?php
	session_start();
	include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/IngresosDonativoDaoJdbc.class.php");
	include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/IngresosDonativo.class.php");
	if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    	header("Location: " . $_SESSION['RAIZ'] . "/index.php?opc=2");
	}
	$respuesta = "";
	if(isset($_POST['guardar'])){
		$elemento = new IngresosDonativo();
		$elemento->setNom_patrocinador($_POST['nom_patrocinador']);
		$elemento->setMonto($_POST['monto']);
		$elemento->setPeriodo($_POST['periodo']);
		$elemento->setIdProyecto($_POST['proyecto_destino']);
		//checamos si el proyecto es una exposicion temporal
		$elemento->setIdExpoTemp($_POST['expotemp']);
		//Para subir el archivo en caso de que se haya agregado.
		if(!is_null($_FILES['documento']) && $_FILES['documento']['tmp_name'] != ""){
			$carpeta = "/ArchivosIngresos/Donativo/";
			$nomArchivo = $_FILES['documento']['name'];
			opendir($_SESSION['RAIZ'] . $carpeta);
			$destino = ".." . $carpeta . $nomArchivo;
			copy($_FILES['documento']['tmp_name'], $_SESSION['RAIZ'] . $carpeta . $nomArchivo);
			$elemento->setArchivo($destino);	
		}
		if(isset($_POST['id'])){
			$elemento->setId($_POST['id']);
			$dao = new IngresosDonativoDaoJdbc();
			$res = $dao->modificaElemento($elemento);
			if($res) $respuesta = "Registro modificado con éxito.";	
			else $respuesta = "El registro no pudo modificarse con éxito.";
		}else{
			$dao = new IngresosDonativoDaoJdbc();
			$res = $dao->guardaElemento($elemento);
			if($res) $respuesta = "Éxito al guardar el ingreso.";
			else $respuesta = "Error al guardar el ingreso. Inténtelo más tarde"; 
			//echo($res);
		}
	}else if(isset($_GET['id'])){
		$dao = new IngresosDonativoDaoJdbc();
		$elemento = new IngresosDonativo();
		$elemento = $dao->obtieneElemento($_GET['id']);
		$res = $dao->eliminaElemento($elemento);
		if($res) $respuesta = "Registro borrado con éxito.";	
	}
	
	header("Location: ../../../../../donativos_palacio/lista_donativospalacio.php?respuesta=".$respuesta);
?>