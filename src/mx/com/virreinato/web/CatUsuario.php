<?php

if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Persona.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Perfil.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Usuario.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProyectoMeta.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/ProyectoMetaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/UsuarioDaoJdbc.class.php");

if(isset($_POST['MetasProyecto'])){
    $dao = new ProyectoMetaDaoJdbc();
    $lista = $dao->obtenerListado($_POST['MetasProyecto']);
    $elemento=new ProyectoMeta();
    foreach($lista as $elemento){	
                  echo("<option value='".$elemento->getIdMeta()."/".$elemento.getDescripcion()."'> ".$elemento->getDescripcion()." </option> ");
          }
}else if(isset($_POST['guardar'])){
    $elemento=new Usuario();
			
    $persona=new Persona();
    $persona->setId((int)($_POST['id_persona']));
    $elemento->setPersona($persona);

    $perfil=new Perfil();
    $perfil->setId((int)($_POST['id_perfil']));
    $elemento->setPerfil($perfil);

    $elemento->setUsuario($_POST['usr']);
    $elemento->setPassword($_POST['pwd']);

    if(isset($_POST['id_metas'])){
        $metas = $_POST['id_metas'];
    }else{
        $metas = NULL;
    }
    $idMetas= array();
    if($metas!=null){
        foreach($metas as $met){
            array_push($idMetas,(int)($met));
        }
    }
    $elemento->setIdMetas($idMetas);
    if(isset($_POST['id'])){
        $elemento->setId((int)($_POST['id']));
        $dao=new UsuarioDaoJdbc();
        $res=$dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new UsuarioDaoJdbc();
        //verifica si esta duplicado
        $usrtmp=$dao->obtieneElementoByDesc($elemento->getUsuario());
        if($usrtmp!=null && $usrtmp->getId()!=null){
            $respuesta = "No fue posible almacenar su información porque el nombre de usuario que solicitó ya esta siendo usado por alguien más, por favor proporcione otro usuario.".$usrtmp->getId();
            $id_persona = $_POST["id_persona"];
            $id_perfil = $_POST["id_perfil"];
            header("Location: ../../../../../catalogos/lista_ccostos.php?respuesta=" . $respuesta."&id_persona=".$id_persona."&id_perfil=".$id_perfil);
        }else{
            $res=$dao->guardaElemento($elemento);
            if ($res) {
                $respuesta = "Su información se almacenó exitosamente.";
            } else {
                $respuesta = "No fue posible almacenar su información.";
            }   
        }
    }
}else if(isset($_GET['id'])){
    $dao=new UsuarioDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));
    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
}else{
    $respuesta = "No se detecto la acción a realizar.";
}
header("Location: ../../../../../catalogos/lista_usuario.php?respuesta=" . $respuesta);
?>
