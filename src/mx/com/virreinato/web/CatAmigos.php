<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/AmigosDaoJdbc.class.php"); // ProductosDaoJdbc.class.php
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/AmigosPatronato.class.php"); // ProductosPatronato.class.php
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$respuesta = "";
if (isset($_POST['guardar'])){
    $elemento=new Amigos(); // Productos
    $elemento->setNombre_tipo_amigos($_POST["nombre_tipo_amigos"]);
    $elemento->setDescripcion_tipo_amigos($_POST["descripcion_tipo_amigos"]);
    
	
    
    if ($_POST["id"] != null){
        $elemento->setId_tipo_amigos((int)($_POST["id"]));
        $dao=new AmigosDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new AmigosDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
}else if (isset($_GET['id'])){
    $dao=new AmigosDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
} else {
    $respuesta = "No se detecto la acción a realizar.";
}
header("Location: ../../../../../catalogos/lista_amigos.php?respuesta=" . $respuesta);

?>

