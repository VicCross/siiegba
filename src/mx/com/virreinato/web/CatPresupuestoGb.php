<?php

session_start();
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/PresupuestoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Presupuesto.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/ProveedorDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Proveedor.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}

if(isset($_POST['inicio'])){
    $inicio = $_POST["inicio"];
    $fin = $_POST["fin"];
    $estatus = $_POST["estatus"];
    $periodo = $_POST["periodo"];
    
    header("Location: ../../../../../gasto_basico/SolicitudGb.php?inicio=".$inicio."&fin=".$fin."&estatus=".$estatus."&periodo=".$periodo."&recargado=1");
}else if (isset($_POST['destino']) && $_POST['destino']!=null){
    $elemento = new Presupuesto();

    $elemento->setFecha(date("Y-m-d",strtotime($_POST["fecha"])));
    $elemento->setObservaciones($_POST["obs_solicitud"]);
    $elemento->setIdEmpleado((int)($_POST["idEmpleado"]));
    $elemento->setIdProveedor((int)($_POST["idProveedor"]));
    $elemento->setIdMeta(0);
    $elemento->setElabora($_POST["elabora_sol"]);
    $elemento->setDestino($_POST["destino"]);
    $elemento->setIdPeriodo($_POST["periodo"]);
    $elemento->setIdPrograma($_POST["programa"]);
    
    if ($_POST["otro"] != ""){
					
        //verifico si tengo que insertar en el catálogo de porveedores
        if($_POST["catalogo_proveedor"] != null){
            $nombre=$_POST["otro"];
            $domicilio= $_POST["domicilio_prov"];
            $cp=$_POST["cp_prov"];
            $representante=$_POST["representante_prov"];
            $telefono=$_POST["telefono_prov"];
            $correo=$_POST["correo_prov"];

            $prov=new Proveedor();

            $prov->setProveedor($nombre);
            $prov->setDomicilio($domicilio);
            $prov->setCp($cp);
            $prov->setRepresentante($representante);
            $prov->setTelefono($telefono);
            $prov->setCorreo($correo);
            $prov->setEstatus(1);
            $prov->setOtroTelefono("");

            $dao=new ProveedorDaoJdbc();
            $dao->guardaElemento(prov);

            $pro=$dao->obtieneElementoDesc($nombre);
            $elemento->setIdProveedor($pro->getId());

        }
        else{
                $elemento->setOtro($_POST["otro"]);
        }
    }else
        {   $elemento->setOtro(null);   }
    
    if ($_POST["id"] != null){
        $elemento->setId((int)($_POST["id"]));
        $dao=new PresupuestoDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new PresupuestoDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
    header("Location: ../../../../../gasto_basico/SolicitudGb.php?respuesta=" . $respuesta);
}else if (isset($_GET['id'])){
    $dao=new PresupuestoDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
    header("Location: ../../../../../gasto_basico/SolicitudGb.php?respuesta=" . $respuesta);
} else if (isset($_GET['VoboDirector'])){
    $dao = new PresupuestoDaoJdbc();
    $dao->VoBoDirector((int)$_GET['VoboDirector']);
    $respuesta = "Se ha dado el visto bueno del director";
    header("Location: ../../../../../gasto_basico/SolicitudGb.php?respuesta=" . $respuesta);
} else if (isset($_GET['VoboST'])){
    $dao = new PresupuestoDaoJdbc();
    $dao->VoBoST((int)$_GET['VoboST']);
    $respuesta = "Se ha dado el visto bueno del subdirector técnico";
    header("Location: ../../../../../gasto_basico/SolicitudGb.php?respuesta=" . $respuesta);
} else if (isset($_GET['VoboSA'])){
    $dao = new PresupuestoDaoJdbc();
    $dao->VoBoSA((int)$_GET['VoboSA']);
    $respuesta = "Se ha dado el visto bueno del subdirector administrativo";
    header("Location: ../../../../../gasto_basico/SolicitudGb.php?respuesta=" . $respuesta);
} else if(isset($_GET['NoAceptado'])){
    $dao = new PresupuestoDaoJdbc();
    $dao->NoAceptado((int)$_GET['NoAceptado'], $_GET['Vobo']);
    $respuesta = "El proyecto ha sido rechazado";
    header("Location: ../../../../../gasto_basico/SolicitudGb.php?respuesta=" . $respuesta);
} else {
    $respuesta = "No se detecto la acción a realizar.";
}
?>


