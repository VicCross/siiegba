<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/ConciliacionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Conciliacion.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/MovimientoBancoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/MovimientoBanco.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
if(isset($_POST['continuar'])){
    $mes = $_POST['mes'];
    $id_periodo = $_POST['id_periodo'];
    
    header("Location: ../../../../../bancos/conciliacion_bancaria.php?mes=".$mes."&id_periodo=".$id_periodo);
}else if(isset($_POST['conciliar'])){
    $respuesta = "";
    $elemento=new Conciliacion();
				
    $elemento->setMes((int)($_POST["mes"]));
    $elemento->setIdPeriodo((int)($_POST["id_periodo"]));
    $elemento->setSaldoInicial((double)($_POST["saldo_inicial"]));
    $elemento->setIngresosMuseo((double)($_POST["total_am"]));
    $elemento->setEgresosMuseo((double)($_POST["total_cm"]));
    $elemento->setSaldoFinalMuseo((double)($_POST["saldo_final_m"]));

    $elemento->setIngresosBanco((double)($_POST["total_ab"]));
    $elemento->setEgresosBanco((double)($_POST["total_cb"]));
    $elemento->setSaldoFinalBanco((double)($_POST["saldo_final_b"]));

    $elemento->setDiferencia((double)($_POST["diferencia"]));
    $elemento->setObservaciones($_POST["observaciones"]);
    $elemento->setStatus("1");
    $dao=new ConciliacionDaoJdbc();
    $res=$dao->guardaElemento($elemento);

    if($res)
    {   $respuesta = "Su información se almacenó exitosamente.";    }
    else
    {    $respuesta = "No fue posible almacenar su información.";   }
    
    $mes = $_POST['mes'];
    $id_periodo = $_POST['id_periodo'];
    
    header("Location: ../../../../../bancos/conciliacion_bancaria.php?mes=".$mes."&id_periodo=".$id_periodo."&respuesta=".$respuesta);
}else if(isset($_GET['id'])){
    $dao=new MovimientoBancoDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
    
    $finicio = $_GET['finicio'];
    $ffin = $_GET['ffin'];
    
    header("Location: ../../../../../bancos/lista_movimiento_banco.php?finicio=".$finicio."&ffin=".$ffin."&respuesta=".$respuesta);
} else {
    $respuesta = "No se detecto la acción a realizar.";
}