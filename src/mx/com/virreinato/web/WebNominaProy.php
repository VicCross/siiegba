<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/NominaProyDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/NominaProy.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$respuesta = "";
if (isset($_POST['guardar'])){
    $elemento = new NominaProy();
    $elemento->setIdProy((int)($_POST["proyecto"]));
    $elemento->setResponsable($_POST["responsable"]);
    $elemento->setPeriodoPago($_POST["periodoPago"]);
    $elemento->setDias((double)($_POST["dias"]));
    $elemento->setSalarioDiario((double)($_POST["salarioDiario"]));
    $elemento->setImporte((double)($_POST["importe"]));
    $elemento->setCreditoAlSalario((double)($_POST["creditoAlSalario"]));
    $elemento->setBonificacion((double)($_POST["bonificacion"]));
    $elemento->setNeto((double)($_POST["neto"]));
    $elemento->setIdPersonal((int)($_POST["idPersonal"]));
    if ($_POST["id"] != null){
        $elemento->setId((int)($_POST["id"]));
        $dao=new NominaProyDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new NominaProyDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
}else if (isset($_GET['id'])){
    $dao=new NominaProyDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
} else {
    $respuesta = "No se detecto la acción a realizar.";
}
header("Location: ../../../../../Proyectos/lista_NominaProy.php?respuesta=" . $respuesta);
?>
