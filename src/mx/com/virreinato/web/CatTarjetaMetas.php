<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/TarjetaMetasDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/TarjetaMetas.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$respuesta = "";
if (isset($_POST['id_Tarjeta'])){
    $elemento=new TarjetaMetas();
    $elemento->setNum_orden((int)($_POST["orden"]));
    $elemento->setIdTarjeta((int)($_POST["id_Tarjeta"]));
    $elemento->setClave($_POST["clave_Meta"]);
    $elemento->setNombreAcuerdo($_POST["acuerdo_Meta"]);

    if( $_POST["primero_Programacion"] != "" ) $elemento->setTrimestral1((int)( $_POST["primero_Programacion"] ));
    if( $_POST["segundo_Programacion"] != "" ) $elemento->setTrimestral2((int)( $_POST["segundo_Programacion"] ));
    if( $_POST["tercero_Programacion"] != "" ) $elemento->setTrimestral3((int)( $_POST["tercero_Programacion"] ));
    if( $_POST["cuarto_Programacion"] != "" ) $elemento->setTrimestral4((int)( $_POST["cuarto_Programacion"] ));
    if ($_POST["id"] != null){
        $elemento->setIdMeta((int)($_POST["id"]));
        $dao=new TarjetaMetasDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new TarjetaMetasDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
    header("Location: ../../../../../Proyectos/lista_Metas.php?respuesta=" . $respuesta."&idTarjeta=".$_POST['id_Tarjeta']);
}else if (isset($_GET['id'])){
    $dao=new TarjetaMetasDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
    header("Location: ../../../../../Proyectos/lista_Metas.php?respuesta=" . $respuesta."&idTarjeta=".$_GET['id_Tarjeta']);
} else {
    $respuesta = "No se detecto la acción a realizar.";
    header("Location: ../../../../../Proyectos/lista_Metas.php?respuesta=" . $respuesta);
}
?>
