<?php

session_start();
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/EntregablesDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Entregables.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$respuesta = "";
if (isset($_POST['id_Carta'])){
    $elemento = new Entregables();
    $elemento->setIdCarta((int)( $_POST["id_Carta"] ));
    if(  $_POST["orden_entregable"] != "" )$elemento->setNum_orden((int)( $_POST["orden_entregable"] ) );
    $elemento->setDescripcion( $_POST["descripcion"]);
    $elemento->setCaracteristicas( $_POST["carac"]);
    $elemento->setSolicitado( $_POST["solicitado"] );
    $elemento->setEntregado( $_POST["entregado"] );
    $elemento->setValido( $_POST["validado"] );
    if ($_POST["id"] != null){
        $elemento->setId((int)($_POST["id"]));
        $dao=new EntregablesDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new EntregablesDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
    header("Location: ../../../../../Proyectos/lista_Entregables.php?respuesta=" . $respuesta."&idCarta=".$_POST['id_Carta']);
}else if (isset($_GET['id'])){
    $dao=new EntregablesDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
    header("Location: ../../../../../Proyectos/lista_Entregables.php?respuesta=" . $respuesta."&idCarta=".$_GET['idCarta']);
} else {
    $respuesta = "No se detecto la acción a realizar.";
    header("Location: ../../../../../Proyectos/lista_Entregables.php?respuesta=" . $respuesta);
}
?>

