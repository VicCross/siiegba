<?php

session_start();
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/PresupuestoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Presupuesto.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/ProveedorDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Proveedor.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");;
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}

if(isset($_POST['inicio'])){
    $inicio = $_POST["inicio"];
    $fin = $_POST["fin"];
    $estatus = $_POST["estatus"];
    $periodo = $_POST["periodo"];
    
    header("Location: ../../../../../Proyectos/Solicitud.php?inicio=".$inicio."&fin=".$fin."&estatus=".$estatus."&periodo=".$periodo."&recargado=1");
}else if(isset($_POST['obtieneMetas'])){
    $query;
	        
    if( isset($_POST["idUsuario"]) )
    {    $query = "SELECT m.proyecto_id_meta, m.proyecto_desc_meta FROM sie_usuario_x_meta um, sie_proyecto_metas m WHERE um.cus_id_usuario = ".$_POST["idUsuario"]." AND um.proyecto_id_meta = m.proyecto_id_meta AND m.cpr_id_proyecto = ".$_POST["obtieneMetas"]." AND PROYECTO_ESTATUS_META=1"; }
    else
    {    $query = "SELECT proyecto_id_meta, proyecto_desc_meta FROM sie_proyecto_metas WHERE cpr_id_proyecto = ".$_POST["obtieneMetas"]." AND PROYECTO_ESTATUS_META=1"; }

    echo("<option value='0'>Selecciona</option>");
    $catalogo = new Catalogo();
        
    $result = $catalogo->obtenerLista($query);

    while ($rs = mysql_fetch_array($result)){
        $sel = "";
        if( (int)($_POST["sel"]) != 0 && (int)($_POST["sel"]) == $rs["proyecto_id_meta"] ) $sel = "selected='selected'";
        echo("<option value='".$rs["proyecto_id_meta"]."' ".$sel.">".$rs["proyecto_desc_meta"]."</option>");
    }
}else if (isset($_POST['fecha'])){
    $elemento = new Presupuesto();

    $elemento->setFecha(date("Y-m-d",strtotime($_POST["fecha"])));
    $elemento->setObservaciones($_POST["obs_solicitud"]);
    $elemento->setIdEmpleado((int)($_POST["idEmpleado"]));
    $elemento->setIdProveedor((int)($_POST["idProveedor"]));
    $elemento->setIdMeta((int)($_POST["idMeta"]));
    $elemento->setElabora($_POST["elabora_sol"]);
    $elemento->setDestino($_POST["destino"]);
    $elemento->setIdPrograma($_POST["programa"]);
    $elemento->setArea($_POST['areaEmpleado']);
    
    if ($_POST["otro"] != ""){
					
        //verifico si tengo que insertar en el catálogo de porveedores
        if($_POST["catalogo_proveedor"] != null){
            $nombre=$_POST["otro"];
            $domicilio= $_POST["domicilio_prov"];
            $cp=$_POST["cp_prov"];
            $representante=$_POST["representante_prov"];
            $telefono=$_POST["telefono_prov"];
            $correo=$_POST["correo_prov"];

            $prov=new Proveedor();

            $prov->setProveedor($nombre);
            $prov->setDomicilio($domicilio);
            $prov->setCp($cp);
            $prov->setRepresentante($representante);
            $prov->setTelefono($telefono);
            $prov->setCorreo($correo);
            $prov->setEstatus(1);
            $prov->setOtroTelefono("");

            $dao=new ProveedorDaoJdbc();
            $dao->guardaElemento(prov);

            $pro=$dao->obtieneElementoDesc($nombre);
            $elemento->setIdProveedor($pro->getId());

        }
        else{
            $elemento->setOtro($_POST["otro"]);
        }
    }else
        {   $elemento->setOtro(null);   }
    
    if (isset($_POST["id"])){
        $elemento->setId((int)($_POST["id"]));
        $dao=new PresupuestoDaoJdbc();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new PresupuestoDaoJdbc();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
    header("Location: ../../../../../Proyectos/Solicitud.php?respuesta=" . $respuesta);
}else if (isset($_GET['id'])){
    $dao=new PresupuestoDaoJdbc();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
    header("Location: ../../../../../Proyectos/Solicitud.php?respuesta=" . $respuesta);
} else if (isset($_GET['VoboDirector'])){
    $dao = new PresupuestoDaoJdbc();
    $dao->VoBoDirector((int)$_GET['VoboDirector']);
    $respuesta = "Se ha dado el visto bueno del director";
    header("Location: ../../../../../Proyectos/Solicitud.php?respuesta=" . $respuesta);
} else if (isset($_GET['VoboST'])){
    $dao = new PresupuestoDaoJdbc();
    $dao->VoBoST((int)$_GET['VoboST']);
    $respuesta = "Se ha dado el visto bueno del subdirector técnico";
    header("Location: ../../../../../Proyectos/Solicitud.php?respuesta=" . $respuesta);
} else if (isset($_GET['VoboSA'])){
    $dao = new PresupuestoDaoJdbc();
    $dao->VoBoSA((int)$_GET['VoboSA']);
    $respuesta = "Se ha dado el visto bueno del subdirector administrativo";
    header("Location: ../../../../../Proyectos/Solicitud.php?respuesta=" . $respuesta);
} else if(isset($_GET['NoAceptado'])){
    $dao = new PresupuestoDaoJdbc();
    $dao->NoAceptado((int)$_GET['NoAceptado'], $_GET['Vobo']);
    $respuesta = "El proyecto ha sido rechazado";
    header("Location: ../../../../../Proyectos/Solicitud.php?respuesta=" . $respuesta);
}else {
    $respuesta = "No se detecto la acción a realizar.";
    header("Location: ../../../../../Proyectos/Solicitud.php?respuesta=" . $respuesta);
}
?>


