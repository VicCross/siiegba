<?php
	session_start();
	include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/IngresosDonativo.class.php");
	include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");
	
	
	class IngresosDonativoDaoJdbc{
		
		public function obtieneListaDonativo(){
			$catalogo = new Catalogo();
			$lista = array();
			$query = "select * from sie_cat_donativos_palacio";
			$result = $catalogo->obtenerLista($query);
			while($rs = mysql_fetch_array($result)){
				$elemento = new IngresosDonativo();
				$elemento->setAll($rs[0], $rs[1], $rs[2], $rs[3], $rs[4], $rs[5], $rs[12], $rs[13],$rs[8],$rs[9],$rs[10],$rs[11]);	
				array_push($lista, $elemento);
			}
			return $lista;
		}
		
		
		
		public function obtieneElemento($id_donativo){
			$catalogo = new Catalogo();
			$elemento = new IngresosDonativo();
			$query = "SELECT * FROM sie_cat_donativos_palacio WHERE id_donativo = " . $id_donativo;
			$result = $catalogo->obtenerLista($query);
			if($rs = mysql_fetch_array($result)){
				$elemento->setAll($rs[0], $rs[1], $rs[2], $rs[3], $rs[4], $rs[5], $rs[12], $rs[13],$rs[8],$rs[9],$rs[10],$rs[11]);
			}
			return $elemento;
		}
		
		public function guardaElemento($elemento){
			$catalogo = new Catalogo();
			$ponemosExpo_temp = ""; $expo_temp = "";
			$ponemosDocumento = ""; $documento = "";
			if($elemento->getProyecto_destino() == 48){
				$ponemosExpo_temp = ", idExpoTemp";
				$expo_temp = ", " . $elemento->getExpo_temp();	
			}
			$algo = $elemento->getDocumento();
			if(isset($algo)){
				$ponemosDocumento = ", documento";
				$documento = ", '" . $elemento->getDocumento() . "'";
			}
			$insert = 	"INSERT INTO sie_cat_donativos_palacio (nom_patrocinador, tipo_donacion, area, monto, fecha, descripcion " . $ponemosExpo_temp . $ponemosDocumento . ",status, periodo, idProyecto) " .
						"VALUES ('" . $elemento->getNom_patrocinador() . "', '" . $elemento->getTipo_donacion() . "', '" . $elemento->getArea() . "', " . $elemento->getMonto() . ", NOW(), '" . $elemento->getDescripcion() . "'" . $expo_temp . $documento . ", " . $elemento->getStatus() . ",". $elemento->getPeriodo().", " . $elemento->getProyecto_destino() . ")";
			$res = $catalogo->obtenerLista($insert);
			$update=" update sie_cat_linea_accion set donativo=(select sum(monto) from sie_cat_donativos_palacio where idProyecto=". $elemento->getProyecto_destino() .")where cla_id_lineaaccion=". $elemento->getProyecto_destino();
			$res2 = $catalogo->obtenerLista($update);
			if($res != "1") return false;
			$update = "UPDATE sie_cat_linea_accion SET saldoInicial = saldoInicial + " . $elemento->getMonto() . " , saldoAnterior = saldoAnterior + " . $elemento->getMonto() . " WHERE cla_id_lineaaccion = " . $elemento->getProyecto_destino();
			$insert = $insert . " " . $update;
			$res = $catalogo->obtenerLista($update);			
			if($res != "1") return false;
			if($elemento->getProyecto_destino() == 48){
				$update = "UPDATE sie_cat_expo_temp SET saldo = saldo + " . $elemento->getMonto() . ", saldoAnterior = saldoAnterior + " . $elemento->getMonto() . " WHERE id_Subproyecto = " . $elemento->getExpo_temp();	
				$res = $catalogo->obtenerLista($update);	
				if($res == "1") return true;
				else return false;
			}else return true;
			
		}
		
		public function eliminaElemento($elemento){
			$catalogo = new Catalogo();
			$delete = "DELETE FROM sie_cat_donativos_palacio WHERE id_donativo = " . $elemento->getId_donativo();
			$res = $catalogo->obtenerLista($delete);
			if($res == "1"){
				$update = "UPDATE sie_cat_linea_accion SET saldoInicial = saldoInicial - " . $elemento->getMonto() . ", saldoAnterior = saldoAnterior - " . $elemento->getMonto() . " WHERE cla_id_lineaaccion = " . $elemento->getProyecto_destino();
				$catalogo->obtenerLista($update);
				if($elemento->getProyecto_destino() == 48){
					$update = "UPDATE sie_cat_expo_temp SET saldo = saldo - " . $elemento->getMonto() . ", saldoAnterior = saldoAnterior - " . $elemento->getMonto() . " WHERE id_Subproyecto = " . $elemento->getExpo_temp();	
					$catalogo->obtenerLista($update);
				}
				
				return true;	
			}
			else return false;
		}
		
		public function modificaElemento($elemento){//Solo se modifica el nombre del Patrocinador, el monto y el nombre del archivo
			/***********************/
			$ip = new IngresosDonativo();
			$ip = $this->obtieneElemento($elemento->getId_donativo()); //verificar si getId es el de donativos o el del formulario
			$nuevoMonto = $elemento->getMonto() - $ip->getMonto();
			/***********************/
			$catalogo = new Catalogo();
			$documento = "";
			//Checamos lo del archivo.
			if(!is_null($elemento->getDocumento())){				
				$documento = ", documento = '" . $elemento->getDocumento() . "'";
			}
			$update = "UPDATE sie_cat_donativos_palacio SET nom_patrocinador = '" . $elemento->getNom_patrocinador() . "', monto = " . $elemento->getMonto() . $documento . " WHERE id_donativo = " . $elemento->getId_donativo();
			$res = $catalogo->obtenerLista($update);
			if($res == "1"){
				$update = "UPDATE sie_cat_linea_accion SET saldoInicial = saldoInicial + " . $nuevoMonto . ", saldoAnterior = saldoAnterior + " . $nuevoMonto . " WHERE cla_id_lineaaccion = " . $ip->getProyecto_destino();
				$catalogo->obtenerLista($update);
				if($ip->getProyecto_destino() == 48){
					$update = "UPDATE sie_cat_expo_temp SET saldo = saldo + " . $nuevoMonto . ", saldoAnterior = saldoAnterior + " . $nuevoMonto . " WHERE id_Subproyecto = " . $ip->getExpo_temp();
					$catalogo->obtenerLista($update);
				}
				return true;
			}else{
				return false;
			}
		}
		
		public function obtieneMontoProy($idProy, $periodo)
		{
			$catalogo = new Catalogo();
			$select = "SELECT SUM(i.monto) FROM sie_cat_donativos_palacio WHERE  idProyecto = ". $idProy ." AND PERIODO = ". $periodo;
			$res = $catalogo->obtenerLista($select);
			
			return false;
		}
		
		public function obtieneMontoExpos($idET, $periodo)
		{
			$catalogo = new Catalogo();
			$select = "SELECT SUM(i.monto) FROM sie_cat_donativos_palacio i, sie_cat_periodos p WHERE i.idExpoTemp = " . $idET . " AND p.CPE_ID_PERIODO = " . $periodo . " AND i.fecha BETWEEN p.CPE_FECHA_INI AND p.CPE_FECHA_FIN";
			$res = $catalogo->obtenerLista($select);
			
			return false;
		}
		
	}
?>