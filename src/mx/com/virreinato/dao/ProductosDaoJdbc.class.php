<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProductosPatronato.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class ProductosDaoJdbc {
    
    public function obtieneListadoProd() {
		
	$lista= array();
		
	$query="SELECT * FROM cat_productos where estatus_prod = 1 order by id_prod asc ";
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id_prod= $rs["id_prod"];
            $nombre_prod= $rs["nombre_prod"];
            $descripcion_prod= $rs["descripcion_prod"];
            $inventario_ini= $rs["inventario_inicial"];
            $inventario_act= $rs["inventario_actual"];
            $estatus_prod= $rs["estatus_prod"];
            

            $elemento = new Productos();
            $elemento->setAll($id_prod,$nombre_prod,$descripcion_prod,$inventario_ini,$inventario_act,$estatus_prod);
            array_push($lista, $elemento);
        }	
        return $lista;
    }
	
	
	public function obtieneElementoProd($elementoid) {
		
	$elemento=new Productos();
		
	$query="SELECT * FROM cat_productos where id_prod= ".$elementoid;
	
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
			
            $id_prod= $rs["id_prod"];
            $nombre_prod= $rs["nombre_prod"];
            $descripcion_prod= $rs["descripcion_prod"];
            $inventario_ini= $rs["inventario_inicial"];
            $inventario_act= $rs["inventario_actual"];
            $estatus_prod= $rs["estatus_prod"];
            

            $elemento = new Productos();
			
            $elemento->setAll($id_prod,$nombre_prod,$descripcion_prod,$inventario_ini,$inventario_act,$estatus_prod);
            
        }	
        return $elemento;
		
    }
	
	
	
    
		public function guardaElemento($elemento) {

		$con=new Catalogo();

        $query="INSERT INTO cat_productos(nombre_prod,descripcion_prod,inventario_inicial,inventario_actual,estatus_prod)".

		" VALUES ('".mb_strtoupper($elemento->getNombre_prod(),'UTF-8')."', '".mb_strtoupper($elemento->getDescripcion_prod(),'UTF-8')."', '".mb_strtoupper($elemento->getInventario_ini(),'UTF-8')."', '".mb_strtoupper($elemento->getInventario_act(),'UTF-8')."','".$elemento->getEstatus_prod()."' )";

        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	

        }

    
   
    public function actualizaElemento($elemento) {

		$con=new Catalogo();
		$query="UPDATE cat_productos set  nombre_prod='".mb_strtoupper($elemento->getNombre_prod(),'UTF-8')."', descripcion_prod='".mb_strtoupper($elemento->getDescripcion_prod(),'UTF-8')."', inventario_inicial='".mb_strtoupper($elemento->getInventario_ini(),'UTF-8')."' , inventario_actual='".mb_strtoupper($elemento->getInventario_act(),'UTF-8')."' , estatus_prod=".$elemento->getEstatus_prod()."  WHERE id_prod=".$elemento->getId_prod();

		$res = $con->obtenerLista($query);

		

		if($res == "1")

        {	return true; }

        else

        {	return false; }

		

		}

    

	public function eliminaElemento($idElemento){

		

        $con=new Catalogo();

        $query="UPDATE cat_productos set  estatus_prod = 0 WHERE id_prod=".$idElemento;

        $res = $con->obtenerLista($query);



        if($res=="1")

        {	return true; }

        else

        {	return false; }

		

    }
}
