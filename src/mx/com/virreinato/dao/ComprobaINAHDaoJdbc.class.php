<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ComprobaINAH.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class ComprobaINAHDaoJdbc {
    
    public function obtieneListado( $inicio, $fin, $periodo ) {
		
	$lista= array();
		
	$query="SELECT c.*,p.cpe_periodo,m.min_descripcion,cc.ccc_descripcion"
            ." FROM sie_comprobaciones_inah c, sie_cat_periodos p, sie_ministraciones m, sie_cat_centrocosto cc"
            ." WHERE c.cpe_id_periodo = ".$periodo." AND c.cin_fecha BETWEEN '".$inicio."' AND '".$fin."' AND c.cpe_id_periodo=p.cpe_id_periodo AND c.min_id_ministracion=m.min_id_ministracion "
            ." AND c.ccc_id_ccosto=cc.ccc_id_ccosto AND c.cin_estatus>0"
            ." ORDER BY cin_folio_cinah";

	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id = $rs[strtoupper("cin_id_cinah")];
            $idPeriodo = $rs[strtoupper("cpe_id_periodo")];
            $desPeriodo = $rs["cpe_periodo"];
            $idMinistracion = $rs[strtoupper("min_id_ministracion")];
            $desMinistracion = $rs["min_descripcion"];
            $url = $rs[strtoupper("cin_urlcomprobacion")];
            $fecha = $rs[strtoupper("cin_fecha")];
            $idCCosto = $rs[strtoupper("ccc_id_ccosto")];
            $desCCosto = $rs["ccc_descripcion"];

            $monto = 0.0;

            $query = "select SUM(dci_monto) as monto from sie_det_comprobaciones_inah where dci_estatus = 1 AND cin_id_cinah  = ". id;
            $resultMonto = $catalogo->obtenerLista($query);
            if($rsMonto = mysql_fetch_array($resultMonto)){  $monto = $rsMonto["monto"]; }

            $elemento = new ComprobaINAH();
            $elemento->setAll($id,$idPeriodo,$desPeriodo,$monto,$idMinistracion,$desMinistracion,$url,$fecha,$idCCosto,$desCCosto);
            array_push($lista, $elemento);
        }	
        return $lista;
    }
    
    public function obtieneListadoDestino($inicio,$fin,$periodo,$destino ) {
		
	$lista= array();
		
        if($periodo == null){
            $periodo = "null";
        }
        
        $inicio = date("Y-m-d",strtotime($inicio));
        $fin = date("Y-m-d",strtotime($fin));
        
	$query="SELECT c.*,p.cpe_periodo,m.min_descripcion,cc.ccc_descripcion"
            ." FROM sie_comprobaciones_inah c, sie_cat_periodos p, sie_ministraciones m, sie_cat_centrocosto cc"
            ." WHERE c.cpe_id_periodo = ".$periodo." AND c.cin_fecha BETWEEN '".$inicio."' AND '".$fin."' AND c.cpe_id_periodo=p.cpe_id_periodo AND c.min_id_ministracion=m.min_id_ministracion "
            ." AND c.ccc_id_ccosto=cc.ccc_id_ccosto AND c.cin_estatus>0 AND m.MIN_DESTINO='".$destino."' "
            ." ORDER BY cin_folio_cinah";
		
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id = $rs[strtoupper("cin_id_cinah")];
            $idPeriodo = $rs[strtoupper("cpe_id_periodo")];
            $desPeriodo = $rs["cpe_periodo"];
            $idMinistracion = $rs[strtoupper("min_id_ministracion")];
            $desMinistracion = $rs["min_descripcion"];
            $url = $rs[strtoupper("cin_urlcomprobacion")];
            $fecha = $rs[strtoupper("cin_fecha")];
            $idCCosto = $rs[strtoupper("ccc_id_ccosto")];
            $desCCosto = $rs["ccc_descripcion"];

            $monto = 0.0;

            $query = "select SUM(dci_monto) as monto from sie_det_comprobaciones_inah where dci_estatus = 1 AND cin_id_cinah  = ".$id;
            $resultMonto = $catalogo->obtenerLista($query);
            if($rsMonto = mysql_fetch_array($resultMonto)){  $monto = $rsMonto["monto"]; }

            $elemento = new ComprobaINAH();
            $elemento->setAll($id,$idPeriodo,$desPeriodo,$monto,$idMinistracion,$desMinistracion,$url,$fecha,$idCCosto,$desCCosto);
            array_push($lista, $elemento);
        }	
        return $lista;
    }
    
    public function obtieneElemento($idElemento) {
		
	$elemento=new ComprobaINAH();
		
	$query="SELECT * FROM sie_comprobaciones_inah WHERE cin_id_cinah=".$idElemento;
		
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id = $rs[strtoupper("cin_id_cinah")];
            $idPeriodo = $rs[strtoupper("cpe_id_periodo")];
            $idMinistracion = $rs[strtoupper("min_id_ministracion")];
            $url = $rs[strtoupper("cin_urlcomprobacion")];
            $fecha = $rs[strtoupper("cin_fecha")];
            $idCCosto = $rs[strtoupper("ccc_id_ccosto")];

            $elemento = new ComprobaINAH();
            $elemento->comproba($id,$idPeriodo, $idMinistracion, $url, $fecha,$idCCosto);
        }	
        return $elemento;
		
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $query="INSERT INTO sie_comprobaciones_inah(cpe_id_periodo,cin_folio_cinah,cin_monto,cin_numcomprobantes,min_id_ministracion,cin_urlcomprobacion,cin_fecha,cin_descripcion,cin_observaciones,ccc_id_ccosto,cin_estatus) VALUES (".$elemento->getIdPeriodo().", null,null,null,".$elemento->getIdMinistracion().", '".$elemento->getUrl()."', '".date("Y-m-d",strtotime($elemento->getFecha()))."', null, null, ".$elemento->getIdCCosto().", 1 )";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
	$query="UPDATE sie_comprobaciones_inah set "
            ." cpe_id_periodo=".$elemento->getIdPeriodo().", "
            ." min_id_ministracion=".$elemento->getIdMinistracion().", "
            ." cin_urlcomprobacion='".$elemento->getUrl()."', "
            ." cin_fecha='".date("Y-m-d",strtotime($elemento->getFecha()))."', "
            ." ccc_id_ccosto=".$elemento->getIdCCosto()." "
            ." WHERE cin_id_cinah=".$elemento->getId();
        $res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_comprobaciones_inah set cin_estatus=0 WHERE cin_id_cinah=".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}
