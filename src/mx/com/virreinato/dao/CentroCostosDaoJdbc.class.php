<?php
if(session_id() == '') {
    session_start();
}

include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CentroCostos.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

/**
 * Description of CentroCostosDaoJdbc
 *
 * @author HPdv6
 */
class CentroCostosDaoJdbc {

    public function obtieneElemento($idElemento) {
        //$elemento = new CentroCostos();

        $query = strtolower("SELECT * FROM sie_cat_centrocosto WHERE ccc_id_ccosto=" . $idElemento);
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)) {
            $id = $rs[strtoupper("ccc_id_ccosto")];
            $clave = $rs[strtoupper("ccc_clave_cc")];
            $descripcion = $rs[strtoupper("ccc_descripcion")];

            $elemento = new CentroCostos();
            $elemento->setAll($id, $clave, $descripcion);
        }
        return $elemento;
    }

    public function obtieneListado() {
		
                $lista= array();
		
		$query="SELECT * FROM sie_cat_centrocosto WHERE ccc_estatus=1 ORDER BY ccc_clave_cc";
		

                $con=new Catalogo();
                $result = $con->obtenerLista($query);

                while($rs = mysql_fetch_array($result)){
                        $id= $rs[strtoupper("ccc_id_ccosto")];
                        $clave= $rs[strtoupper("ccc_clave_cc")];
                        $descripcion= $rs[strtoupper("ccc_descripcion")];

                        $elemento= new CentroCostos();
                        $elemento->setAll($id,$clave,$descripcion);
                        array_push($lista, $elemento);
                }	
                //iterar el resultado y llenar el objeto, construir la lista
		return $lista;
	}
        
        public function actualizaElemento($elemento) {
		
		$con=new Catalogo();
		$query="UPDATE sie_cat_centrocosto set  ccc_descripcion='".mb_strtoupper($elemento->getDescripcion())."', ccc_clave_cc='".$elemento->getClave()."' WHERE ccc_id_ccosto=".$elemento->getId();
		$res=$con->obtenerLista($query);
		
		if($res == "1"){return true;}
                else{ return false; }
		
	}
        
        public function eliminaElemento($idElemento){
            $con=new Catalogo();
            $query="UPDATE sie_cat_centrocosto set  ccc_estatus=0 WHERE ccc_id_ccosto=".$idElemento;
            $res=$con->obtenerLista($query);
            if($res=="1"){ 
                return true; 
                
            }
            else{ return false; }
		
	}
        
        public function guardaElemento($elemento) {
		
		$con=new Catalogo();
		$query="INSERT INTO sie_cat_centrocosto(ccc_clave_cc,ccc_descripcion) VALUES ('".mb_strtoupper($elemento->getClave(),'UTF-8')."', '".mb_strtoupper($elemento->getDescripcion(),'UTF-8')."')";
		$res=$con->insertarRegistro($query);
		
		if($res!=NULL && $res!=0)
                {	return true; }
		else
                {	return false; }
		
	}
}
