<?php
if(session_id() == '') {
    session_start();
}

include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/DetalleSueldoGB.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/SueldoGB.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class SueldoGbDaoJdbc {
    
    public function obtieneListado($inicio, $fin, $periodo, $tipoNomina){
		
        $lista= array();
        $query = null;
        
        $inicio = date("Y-m-d",strtotime($inicio));
        $fin = date("Y-m-d",strtotime($fin));
        if($periodo == null){
            $periodo = 'null';
        }
        
        $tipoNominaAux="";
        if($tipoNomina == "TD"){
                $tipoNominaAux="'Proyectos','ATM','Apoyo a confianza','Mandos medios','Compactados','Pensión alimenticia'";

        }
        else
                $tipoNominaAux="'".$tipoNomina."'";

        $query = strtolower(" select s.*, p.CPE_PERIODO from sie_sueldos s, SIE_CAT_PERIODOS p where s.cpe_id_periodo= p.cpe_id_periodo and s.sue_fecha BETWEEN '".$inicio."' AND '".$fin."' AND s.cpe_id_periodo = ".$periodo." AND s.sue_tiponomina in (".$tipoNominaAux.") and s.sue_estatus=1 ORDER BY(s.sue_fecha) ");	

        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("sue_id_sueldos")];
            $idPeriodo = $rs[strtoupper("cpe_id_periodo")];
            $periododes=$rs["cpe_periodo"];
            $fecha = $rs[strtoupper("sue_fecha")];
            $idMinistracion = $rs[strtoupper("min_id_ministracion")];
            $nomina = $rs[strtoupper("sue_tiponomina")];
            $monto = $rs[strtoupper("sue_montototal")];
            $quincena = $rs[strtoupper("sue_quincena")];
            $estatus =  $rs[strtoupper("sue_estatus")];
			     
            $elemento = new SueldoGB();
            $elemento->constructor2($id, $idPeriodo, $periododes, $fecha, $idMinistracion, $nomina, $monto, $quincena, $estatus, null);
            array_push($lista, $elemento);		
        }	
        return $lista;
    }
    
    public function obtieneElemento($idElemento) {	
		
        $elemento = null;
        $query=strtolower("SELECT s.*, p.CPE_PERIODO from sie_sueldos s, SIE_CAT_PERIODOS p   where s.cpe_id_periodo= p.cpe_id_periodo and s.sue_id_sueldos = ".$idElemento);
        
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("sue_id_sueldos")];
            $idPeriodo = $rs[strtoupper("cpe_id_periodo")];
            $periododes=$rs["cpe_periodo"];
            $fecha = $rs[strtoupper("sue_fecha")];
            $idMinistracion = $rs[strtoupper("min_id_ministracion")];
            $nomina = $rs[strtoupper("sue_tiponomina")];
            $monto = $rs[strtoupper("sue_montototal")];
            $quincena = $rs[strtoupper("sue_quincena")];
            $estatus =  $rs[strtoupper("sue_estatus")];
            
            $detalle = array();
            $query = "select * from sie_det_sueldos where sds_estatus = 1 AND sue_id_sueldos  = ".$id;
            $resultdetalle = $catalogo->obtenerLista($query);
            $id_DetalleSueldos = array();

            while ($rsdetalle = mysql_fetch_array($resultdetalle)){
                $idDetalle= $rsdetalle[strtoupper("SDS_ID_DET_SUE")];
                $folioCheque = $rsdetalle[strtoupper("sds_folio_ch")];
                $montoLetra = $rsdetalle[strtoupper("sds_montoletra")];
                $idEmpleado = $rsdetalle[strtoupper("CEM_ID_EMPLEADO")];
                $estatusDet =  $rsdetalle[strtoupper("SDS_ESTATUS")];
                $sueldo=$rsdetalle[strtoupper("sds_montonumero")];
                
                $elementoDet = new DetalleSueldoGB();
                $elementoDet->setAll($idDetalle, $id, $folioCheque, $sueldo, $montoLetra,$idEmpleado, $estatusDet );
                array_push($detalle, $elementoDet);
                
                $id_DetalleSueldos[$idEmpleado] = $elementoDet;
            }
            
            $elemento = new SueldoGB();
            $elemento->setAll($id, $idPeriodo, $periododes, $fecha, $idMinistracion, $nomina, $monto, $quincena, $estatus, $detalle, $id_DetalleSueldos);
        }
        return $elemento;
    }
    
    public function guardaElemento($elemento) {
		
        $fecha = "";
		
        if($elemento->getFecha()!=null)
        {    $fecha= "'".date("Y-m-d",strtotime($elemento->getFecha()))."'"; }
        else
        {     $fecha = "null"; }
        
        $con=new Catalogo();
        $query = "INSERT INTO sie_sueldos (cpe_id_periodo,sue_fecha,min_id_ministracion,sue_tiponomina,sue_montototal,sue_quincena,sue_estatus)" .
				" VALUES(".$elemento->getIdPeriodo()." ,".$fecha.", ".$elemento->getIdMinistracion()." , '".$elemento->getTipoNomina()."'," .
				" ".$elemento->getMonto()." , '".$elemento->getQuincena()."', 1)";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	
            return true; }
        else
        {	return false; }	
    }
    
    public function guardaElementoId($elemento) {
		
        $fecha = "";
        $id = 0;
		
        if($elemento->getFecha()!=null)
        {    $fecha= "'".date("Y-m-d",strtotime($elemento->getFecha()))."'"; }
        else
        {     $fecha = "null"; }
        
        $con=new Catalogo();
        $query = "INSERT INTO sie_sueldos (cpe_id_periodo,sue_fecha,min_id_ministracion,sue_tiponomina,sue_montototal,sue_quincena,sue_estatus)" .
				" VALUES(".$elemento->getIdPeriodo()." ,".$fecha.", ".$elemento->getIdMinistracion()." , '".mb_strtoupper($elemento->getTipoNomina(),'UTF-8')."'," .
				" ".$elemento->getMonto()." , '".$elemento->getQuincena()."', 1)";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {   $queryseq="select distinct MAX(sue_id_sueldos) as folio from sie_sueldos";
            $resultId = $con->obtenerLista($queryseq);
            while($rsId = mysql_fetch_array($resultId)){
                $id = $rsId['folio'];
            }
        }
        return $id;
    }
    
    public function actualizaElemento($elemento) {
        
        $id = 0;
        
	$fecha = "";
		
        if($elemento->getFecha()!=null)
        {    $fecha= "'".date("Y-m-d",strtotime($elemento->getFecha()))."'"; }
        else
        {     $fecha = "null"; }
        
	$con=new Catalogo();
	$query= "UPDATE sie_sueldos  SET cpe_id_periodo=".$elemento->getIdPeriodo().", sue_fecha = ".$fecha.", min_id_ministracion=".$elemento->getIdMinistracion()." , sue_tiponomina ='".mb_strtoupper($elemento->getTipoNomina(),'UTF-8')."', sue_montototal =".$elemento->getMonto().", sue_quincena = '".$elemento->getQuincena()."'   WHERE sue_id_sueldos = ". $elemento->getId() ." ";
        $res = $con->obtenerLista($query);
		
	if($res == "1")
        {
            return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $resu = null;
        $query="SELECT c.che_estatusborrado FROM sie_cheques c WHERE c.che_destino='SL' and c.spr_id_solicitud= ".$idElemento;
        $res = $con->obtenerLista($query);

        if($rs = mysql_fetch_array($res)){
            $estatus = $rs["che_estatusborrado"];
            $query2 = "UPDATE sie_sueldos set  sue_estatus = 0 WHERE sue_id_sueldos = ".$idElemento;
        }else{
            $query2 = "UPDATE sie_sueldos set  sue_estatus = 0 WHERE sue_id_sueldos = ".$idElemento;
        }
        $resu = $con->obtenerLista($query2);
        
        if($resu=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}

