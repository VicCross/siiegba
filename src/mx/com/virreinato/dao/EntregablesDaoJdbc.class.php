<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Entregables.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class EntregablesDaoJdbc {
    
    public function obtieneListado($id_carta) {
		
	$lista= array();
	$query="SELECT * FROM sie_carta_cons_det_entregas WHERE ccde_estatus = 1 AND  cco_id_carta = ".(int)($id_carta)." ";
		
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $idCarta= $rs[strtoupper("cco_id_carta")];
            $idEntregable = $rs[strtoupper("ccde_id_entregas")];
            $orden = $rs[strtoupper("ccde_ordenentregable")];
            $descripcion = $rs[strtoupper("ccde_descripcion")];
            $caracteristicas  = $rs[strtoupper("ccde_caracteristicas")];
            $solicitado = $rs[strtoupper("ccde_solicitadopor")];
            $entregado = $rs[strtoupper("ccde_entregadopor")];
            $valido = $rs[strtoupper("ccde_validadopor")];

            $elemento = new Entregables();
            $elemento->setAll($idEntregable,$idCarta,$orden,$descripcion,$caracteristicas,$solicitado, $entregado, $valido);
            array_push($lista, $elemento);
        }	

        return $lista;
    }
    
    public function obtieneElemento($idElemento) {	
		
    $elemento = new Entregables();
    $query="SELECT * FROM sie_carta_cons_det_entregas WHERE  ccde_estatus = 1 AND ccde_id_entregas = ".$idElemento;
    $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $idCarta= $rs[strtoupper("cco_id_carta")];
            $idEntregable = $rs[strtoupper("ccde_id_entregas")];
            $orden = $rs[strtoupper("ccde_ordenentregable")];
            $descripcion = $rs[strtoupper("ccde_descripcion")];
            $caracteristicas  = $rs[strtoupper("ccde_caracteristicas")];
            $solicitado = $rs[strtoupper("ccde_solicitadopor")];
            $entregado = $rs[strtoupper("ccde_entregadopor")];
            $valido = $rs[strtoupper("ccde_validadopor")];

            $elemento = new Entregables();
            $elemento->setAll($idEntregable,$idCarta,$orden,$descripcion,$caracteristicas,$solicitado, $entregado, $valido);
        }	

        return $elemento;
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $query="INSERT INTO sie_carta_cons_det_entregas(CCO_ID_CARTA,CCDE_ORDENENTREGABLE,CCDE_DESCRIPCION,CCDE_CARACTERISTICAS,CCDE_SOLICITADOPOR,CCDE_ENTREGADOPOR,CCDE_VALIDADOPOR,CCDE_ESTATUS) VALUES (".$elemento->getIdCarta().", ". $elemento->getNum_orden() .", '".  mb_strtoupper($elemento->getDescripcion(),'UTF-8') ."','".
		mb_strtoupper($elemento->getCaracteristicas(),'UTF-8')."','". mb_strtoupper($elemento->getSolicitado(),'UTF-8') ."','". mb_strtoupper($elemento->getEntregado(),'UTF-8') ."', '". mb_strtoupper($elemento->getValido(),'UTF-8') ."', 1)";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
	$query="UPDATE sie_carta_cons_det_entregas  SET  ccde_ordenentregable = ".$elemento->getNum_orden().", ccde_descripcion = '".mb_strtoupper($elemento->getDescripcion(),'UTF-8')."', ccde_caracteristicas='".mb_strtoupper($elemento->getCaracteristicas(),'UTF-8')."' , ccde_solicitadopor ='".mb_strtoupper($elemento->getSolicitado(),'UTF-8')."', ccde_entregadopor = '". mb_strtoupper($elemento->getEntregado(),'UTF-8') ."', ccde_validadopor = '". mb_strtoupper($elemento->getValido(),'UTF-8') ."'  WHERE ccde_id_entregas = ". $elemento->getId() ."";
	$res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_carta_cons_det_entregas set  ccde_estatus = 0 WHERE ccde_id_entregas = ".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}
