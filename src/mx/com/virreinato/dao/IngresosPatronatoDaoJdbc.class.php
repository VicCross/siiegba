<?php
	session_start();
	include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/IngresosPatronato.class.php");
	include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");
	class IngresosPatronatoDaoJdbc{
		public function obtieneLista(){
			$catalogo = new Catalogo();
			$lista = array();
			$query = "SELECT * FROM sie_ingresos_patronato ORDER BY id_ingresosP";
			$result = $catalogo->obtenerLista($query);
			while($rs = mysql_fetch_array($result)){
				$elemento = new IngresosPatronato();
				$elemento->setAll($rs[0], $rs[1], $rs[2], $rs[3], $rs[4], $rs[5], $rs[6], $rs[7], $rs[8]);	 // 
				array_push($lista, $elemento);
			}
			return $lista;
		}
		
		public function obtieneElemento($id){
			$catalogo = new Catalogo();
			$elemento = new IngresosPatronato();
			$query = "SELECT * FROM sie_ingresos_patronato WHERE id_ingresosP = " . $id . " ORDER BY id_ingresosP";
			$result = $catalogo->obtenerLista($query);
			if($rs = mysql_fetch_array($result)){
				$elemento->setAll($rs[0], $rs[1], $rs[2], $rs[3], $rs[4], $rs[5], $rs[6], $rs[7], $rs[8]);
			}
			return $elemento;
		}
		
		public function guardaElemento($elemento){
			$catalogo = new Catalogo();
			$ponemosExpoTemp = ""; $expoTemp = "";
			$ponemosArchivo = ""; $archivo = "";
			if($elemento->getIdProyecto() == 48){
				$ponemosExpoTemp = ", idExpoTemp";
				$expoTemp = ", " . $elemento->getIdExpoTemp();	
			}
			if(!is_null($elemento->getArchivo())){
				$ponemosArchivo = ", archivo";
				$archivo = ", '" . $elemento->getArchivo() . "'";
			}
			$insert = "INSERT INTO sie_ingresos_patronato (nombrePatrocinador, monto, fecha,id_tipo_amigos, idProyecto". $ponemosExpoTemp . $ponemosArchivo . ",periodo) VALUES ('" . $elemento->getPatrocinador() . "', " . $elemento->getMonto() . ", curdate(), " .$elemento->getId_Tipo_Amigos() . ","  . $elemento->getIdProyecto(). $expoTemp . $archivo . ",". $elemento->getPeriodo().")";
			$res = $catalogo->obtenerLista($insert);
			$update=" update sie_cat_linea_accion set patronato=(select sum(monto) from sie_ingresos_patronato where idproyecto=". $elemento->getIdProyecto() .")where cla_id_lineaaccion=". $elemento->getIdProyecto();
			$res2 = $catalogo->obtenerLista($update);
			if($res != "1") return false;
			$update = "UPDATE sie_cat_linea_accion SET saldoInicial = saldoInicial + " . $elemento->getMonto() . " , saldoAnterior = saldoAnterior + " . $elemento->getMonto() . " WHERE cla_id_lineaaccion = " . $elemento->getIdProyecto();
			$insert = $insert . " " . $update;
			$res = $catalogo->obtenerLista($update);			
			if($res != "1") return false;
			if($elemento->getIdProyecto() == 48){
				$update = "UPDATE sie_cat_expo_temp SET saldo = saldo + " . $elemento->getMonto() . ", saldoAnterior = saldoAnterior + " . $elemento->getMonto() . ", patronato = patronato +".$elemento->getMonto()." WHERE id_Subproyecto = " . $elemento->getIdExpoTemp();	
				$res = $catalogo->obtenerLista($update);	
				if($res == "1") return true;
				else return false;
			}else return true;
			
		}
		
		public function eliminaElemento($elemento){
			$catalogo = new Catalogo();
			$delete = "DELETE FROM sie_ingresos_patronato WHERE id_ingresosP = " . $elemento->getId();
			$res = $catalogo->obtenerLista($delete);
			if($res == "1"){
				$update = "UPDATE sie_cat_linea_accion SET saldoInicial = saldoInicial - " . $elemento->getMonto() . ", saldoAnterior = saldoAnterior - " . $elemento->getMonto() . ", patronato = patronato - ". $elemento->getMonto(). "  WHERE cla_id_lineaaccion = " . $elemento->getIdProyecto();
				$catalogo->obtenerLista($update);
				if($elemento->getIdProyecto() == 48){
					$update = "UPDATE sie_cat_expo_temp SET saldo = saldo - " . $elemento->getMonto() . ", saldoAnterior = saldoAnterior - " . $elemento->getMonto() . ", patronato = patronato - ". $elemento->getMonto()."  WHERE id_Subproyecto = " . $elemento->getIdExpoTemp();	
					$catalogo->obtenerLista($update);
				}
				
				return true;	
			}
			else return false;
		}
		
		public function modificaElemento($elemento){//Solo se modifica el nombre del Patrocinador, el monto y el nombre del archivo // y tambien el tipo de ingreso amigos
			/***********************/
			$ip = new IngresosPatronato();
			$ip = $this->obtieneElemento($elemento->getId());
			$nuevoMonto = $elemento->getMonto() - $ip->getMonto();
			/***********************/
			$catalogo = new Catalogo();
			$archivo = "";
			//Checamos lo del archivo.
			if(!is_null($elemento->getArchivo())){				
				$archivo = ", archivo = '" . $elemento->getArchivo() . "'";
			}
			$update = "UPDATE sie_ingresos_patronato SET nombrePatrocinador = '" . $elemento->getPatrocinador() . "', monto = " . $elemento->getMonto() . $archivo . " WHERE id_ingresosP = " . $elemento->getId();
			$res = $catalogo->obtenerLista($update);
			if($res == "1"){
				$update = "UPDATE sie_cat_linea_accion SET saldoInicial = saldoInicial + " . $nuevoMonto . ", saldoAnterior = saldoAnterior + " . $nuevoMonto . ", patronato = patronato +". $nuevoMonto. " WHERE cla_id_lineaaccion = " . $ip->getIdProyecto();
				$catalogo->obtenerLista($update);
				if($ip->getIdProyecto() == 48){
					$update = "UPDATE sie_cat_expo_temp SET saldo = saldo + " . $nuevoMonto . ", saldoAnterior = saldoAnterior + " . $nuevoMonto . ", patronato = patronato + ".$nuevoMonto. " WHERE id_Subproyecto = " . $ip->getIdExpoTemp();
					$catalogo->obtenerLista($update);
				}
				return true;
			}else{
				return false;
			}
		}
		
		public function obtieneMontoProy($idProy, $periodo)
		{
			$catalogo = new Catalogo();
			$select = "SELECT SUM(i.monto) FROM sie_ingresos_patronato WHERE  idProyecto = ". $idProy ." AND PERIODO = ". $periodo;
			$res = $catalogo->obtenerLista($select);
			
			return false;
		}
		
		public function obtieneMontoExpos($idET, $periodo)
		{
			$catalogo = new Catalogo();
			$select = "SELECT SUM(i.monto) FROM sie_ingresos_patronato i, sie_cat_periodos p WHERE i.idExpoTemp = " . $idET . " AND p.CPE_ID_PERIODO = " . $periodo . " AND i.fecha BETWEEN p.CPE_FECHA_INI AND p.CPE_FECHA_FIN";
			$res = $catalogo->obtenerLista($select);
			
			return false;
		}
		
	}
?>