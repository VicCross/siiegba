<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/TarjetaRegistro.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class TarjetaRegistroDaoJdbc {

    public function obtieneListado($anio) {

        $lista = array();
        $query = "SELECT t.tre_id_tarjeta, t.tre_responsable, p.cpr_descripcion " .
                " FROM sie_cat_proyectos p, sie_tarjeta_registro t WHERE p.cpr_id_proyecto=t.cpr_id_proyecto AND" .
                " t.tre_fechaautorizacion BETWEEN '" . $anio . "-01-01' AND '" . $anio . "-12-31' AND t.tre_estatus = 1 ORDER BY p.cpr_id_proyecto ";

        $catalogo = new Catalogo();

        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)) {

            $idTarjeta = $rs["tre_id_tarjeta"];
            $Responsable = $rs["tre_responsable"];
            $proyecto = $rs["cpr_descripcion"];
            $meta = "";

            $elemento = new TarjetaRegistro();
            $elemento->constructor1($idTarjeta, $Responsable, $proyecto, $meta);
            array_push($lista, $elemento);
        }
        return $lista;
    }

    public function Imprimir($idProyecto) {

        $elemento = null;
        $query = "SELECT a.can_descripcion , l.cla_descripcion, p.cpr_numeroproyecto, p.cpr_costo, p.cpr_fecha_ini_total, p.cpr_fecha_fin_total, p.cpr_fecha_ini_ejer, p.cpr_fecha_fin_ejer FROM sie_cat_areanormativa a, sie_cat_proyectos p, sie_cat_linea_accion l WHERE p.can_id_areanormativa = a.can_id_areanormativa AND p.cla_id_lineaaccion = l.cla_id_lineaaccion AND p.cpr_id_proyecto =  " . $idProyecto;

        $catalogo = new Catalogo();

        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)) {
            $area = $rs["can_descripcion"];
            $lineaAccion = $rs["cla_descripcion"];
            $numeroProyecto = $rs["cpr_numeroproyecto"];
            $costo = $rs["cpr_costo"];
            $fechaInicio = $rs["cpr_fecha_ini_total"];
            $fechaFin = $rs["cpr_fecha_fin_total"];
            $Inicio = $rs["cpr_fecha_ini_ejer"];
            $Fin = $rs["cpr_fecha_fin_ejer"];

            $elemento = new TarjetaRegistro();
            $elemento->constructor2($area, $lineaAccion, $numeroProyecto, $costo, $fechaInicio, $fechaFin, $Inicio, $Fin);
        }

        return $elemento;
    }

    public function obtieneElemento($idElemento) {

        $elemento = new TarjetaRegistro();
        $query = "SELECT t.*, t.cpr_id_proyecto FROM sie_tarjeta_registro t WHERE  t.tre_estatus = 1 AND t.tre_id_tarjeta = " . $idElemento;

        $catalogo = new Catalogo();

        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)) {

            $idTarjeta = $rs[strtoupper("tre_id_tarjeta")];
            $idProyecto = $rs[strtoupper("cpr_id_proyecto")];
            $idMeta = 1;
            $unidad = $rs[strtoupper("tre_unidadadministrativa")];
            $responsble = $rs[strtoupper("tre_responsable")];
            $clave = $rs[strtoupper("tre_clavesecretecnica")];
            $claveAccion = $rs[strtoupper("tre_clavelineaaccion")];
            $financiamiento = $rs[strtoupper("tre_autorizadosinfin")];
            $fechaElaboracion = $rs[strtoupper("tre_fechaelaboracion")];
            $fechaAutorizacion = $rs[strtoupper("tre_fechaautorizacion")];
            $funcion = $rs[strtoupper("tre_funcion")];
            $subFuncion = $rs[strtoupper("tre_subfuncion")];
            $programaGeneral = $rs[strtoupper("tre_programageneral")];
            $institucional = $rs[strtoupper("tre_actividadinstitucional")];
            $prioritaria = $rs[strtoupper("tre_actividadprioritaria")];
            $tipo = $rs[strtoupper("tre_tipoproyecto")];
            $concluir = $rs[strtoupper("tre_programadoaconcluir")];
            $reactivado = $rs[strtoupper("tre_reactivado")];
            $producto = $rs[strtoupper("tre_productoprincipal")];
            $acumulado = $rs[strtoupper("tre_acumuladoanterior")];
            $programado = $rs[strtoupper("tre_programadoparax")];
            $terceros = $rs[strtoupper("tre_encasorecursosaportados")];
            $objetivo = $rs[strtoupper("tre_objetivoproyecto")];
            $del1 = $rs[strtoupper("tre_f1delanio")];
            $del2 = $rs[strtoupper("tre_f2delanio")];
            $del3 = $rs[strtoupper("tre_f3delanio")];
            $del4 = $rs[strtoupper("tre_f4delanio")];
            $del5 = $rs[strtoupper("tre_f5delanio")];
            $del6 = $rs[strtoupper("tre_f6delanio")];
            $al1 = $rs[strtoupper("tre_f1alanio")];
            $al2 = $rs[strtoupper("tre_f2alanio")];
            $al3 = $rs[strtoupper("tre_f3alanio")];
            $al4 = $rs[strtoupper("tre_f4alanio")];
            $al5 = $rs[strtoupper("tre_f5alanio")];
            $al6 = $rs[strtoupper("tre_f6alanio")];
            $Total_mod = $rs[strtoupper("tre_costototalmodifica")];
            $prod_mod = $rs[strtoupper("tre_productoprincipalmodifica")];
            $linea_mod = $rs[strtoupper("tre_lineaaccionmodifica")];
            $nombre_mod = $rs[strtoupper("tre_nombreproymodifica")];
            $permanente_mod = $rs[strtoupper("tre_apermanentemodifica")];
            $concluir_mod = $rs[strtoupper("tre_programadoconcluirmodifica")];
            $anio_mod = $rs[strtoupper("tre_aniodemodifica")];
            $al_mod = $rs[strtoupper("tre_almodifica")];
            $justificacion = $rs[strtoupper("tre_justificacionpresupporcap")];
            $elabora = $rs[strtoupper("tre_elabora")];
            $propone = $rs[strtoupper("tre_propone")];
            $autorizaT = $rs[strtoupper("tre_autorizast")];
            $autorizaA = $rs[strtoupper("tre_autorizasa")];

            $elemento->constructor3($idTarjeta, $responsble, $unidad, 0, $idProyecto, $clave, $claveAccion, $financiamiento, $funcion, $subFuncion, $programaGeneral, $institucional, $prioritaria, $tipo, $concluir, $reactivado, $objetivo, $producto, $acumulado, $programado, $terceros, $del1, $del2, $del3, $del4, $del5, $del6, $al1, $al2, $al3, $al4, $al5, $al6, $Total_mod, $prod_mod, $linea_mod, $nombre_mod, $permanente_mod, $concluir_mod, $anio_mod, $al_mod, $fechaElaboracion, $fechaAutorizacion, $elabora, $propone, $autorizaT, $autorizaA, $justificacion);
        }


        return $elemento;
    }

    public function guardaElemento($elemento) {

        $fechaElaboracion = "";
        $Autorizacion = "";

        if ($elemento->getFechaElaboracion() != null) {
            $fechaElaboracion = "'" . date("Y-m-d", strtotime($elemento->getFechaElaboracion())) . "'";
        } else {
            $fechaElaboracion = "null";
        }

        if ($elemento->getFechaAutorizacion() != null) {
            $Autorizacion = "'" . date("Y-m-d", strtotime($elemento->getFechaAutorizacion())) . "'";
        } else {
            $Autorizacion = "null";
        }
        
        if(($elemento->getDel_Fase1() == null) || $elemento->getDel_Fase1() == ""){
            $elemento->setDel_Fase1("null");
        }
        
        if(($elemento->getAl_Fase1()  == null) || $elemento->getAl_Fase1() == ""){
            $elemento->setAl_Fase1("null");
        }                
        
        if(($elemento->getDel_Fase2()  == null) || $elemento->getDel_Fase2() == ""){
            $elemento->setDel_Fase2("null");
        }
        
        if(($elemento->getAl_Fase2() == null) || $elemento->getAl_Fase2() == ""){
            $elemento->setAl_Fase2("null");
        }
        
        if(($elemento->getDel_Fase3() == null) || $elemento->getDel_Fase3() == ""){
            $elemento->setDel_Fase3("null");
        }
        
        if(($elemento->getAl_Fase3() == null) || $elemento->getAl_Fase3() == ""){
            $elemento->setAl_Fase3("null");
        }
        
        if(($elemento->getDel_Fase4() == null) || $elemento->getDel_Fase4() == ""){
            $elemento->setDel_Fase4("null");
        }
        
        if(($elemento->getAl_Fase4() == null) || $elemento->getAl_Fase4() == ""){
            $elemento->setAl_Fase4("null");
        }
        
        if(($elemento->getDel_Fase5() == null) || $elemento->getDel_Fase5() == ""){
            $elemento->setDel_Fase5("null");
        }
        
        if(($elemento->getAl_Fase5() == null) || $elemento->getAl_Fase5() == ""){
            $elemento->setAl_Fase5("null");
        }
        
        if(($elemento->getDel_Fase6() == null) || $elemento->getDel_Fase6() == ""){
            $elemento->setDel_Fase6("null");
        }
        
        if(($elemento->getAl_Fase6() == null) || $elemento->getAl_Fase6() == ""){
            $elemento->setAl_Fase6("null");
        }

        if(($elemento->getCostoTotal_Modificado() == null) || $elemento->getCostoTotal_Modificado() == ""){
            $elemento->setCostoTotal_Modificado("null");
        }
        
        $con = new Catalogo();
        $query = "INSERT INTO sie_tarjeta_registro(CPR_ID_PROYECTO,TRE_FECHAELABORACION,TRE_FECHAAUTORIZACION, " .
                "TRE_UNIDADADMINISTRATIVA, TRE_RESPONSABLE, TRE_CLAVELINEAACCION, TRE_CLAVESECRETECNICA,TRE_AUTORIZADOSINFIN,TRE_FUNCION, " .
                "TRE_SUBFUNCION, TRE_PROGRAMAGENERAL, TRE_ACTIVIDADINSTITUCIONAL, TRE_ACTIVIDADPRIORITARIA, TRE_TIPOPROYECTO, " .
                "TRE_PROGRAMADOACONCLUIR, TRE_REACTIVADO, TRE_PRODUCTOPRINCIPAL, TRE_ACUMULADOANTERIOR, TRE_PROGRAMADOPARAX, " .
                "TRE_ENCASORECURSOSAPORTADOS, TRE_OBJETIVOPROYECTO, TRE_F1DELANIO, TRE_F1ALANIO, TRE_F2DELANIO, TRE_F2ALANIO, " .
                "TRE_F3DELANIO, TRE_F3ALANIO, TRE_F4DELANIO, TRE_F4ALANIO, TRE_F5DELANIO, TRE_F5ALANIO, TRE_F6DELANIO, " .
                "TRE_F6ALANIO, TRE_COSTOTOTALMODIFICA, TRE_PRODUCTOPRINCIPALMODIFICA, TRE_LINEAACCIONMODIFICA, " .
                "TRE_NOMBREPROYMODIFICA, TRE_APERMANENTEMODIFICA, TRE_PROGRAMADOCONCLUIRMODIFICA, TRE_ANIODEMODIFICA, TRE_ALMODIFICA, " .
                "TRE_JUSTIFICACIONPRESUPPORCAP, TRE_ELABORA, TRE_PROPONE, TRE_AUTORIZAST, TRE_AUTORIZASA, TRE_ESTATUS) " .
                "VALUES ('" . $elemento->getIdProyecto() . "', " . $fechaElaboracion . ", " . $Autorizacion . ",'" .
                mb_strtoupper($elemento->getUnidadAdministrativa(),'UTF-8') . "' , '" . mb_strtoupper($elemento->getResponsable(),'UTF-8') . "','" . $elemento->getClaveAccion() . "','" . $elemento->getClaveTecnica() . "', " .
                " '" . mb_strtoupper($elemento->getAutorizadoSinFinanciamiento(),'UTF-8') . "', '" . mb_strtoupper($elemento->getFuncion(),'UTF-8') . "' , '" . mb_strtoupper($elemento->getSubFuncion(),'UTF-8') . "', '" . mb_strtoupper($elemento->getProgramaGeneral(),'UTF-8') . "' ," .
                " '" . mb_strtoupper($elemento->getActividadInstitucional(),'UTF-8') . "', '" . mb_strtoupper($elemento->getActividadPrioritaria(),'UTF-8') . "', '" . mb_strtoupper($elemento->getTipoProyecto(),'UTF-8') . "', '" . mb_strtoupper($elemento->getConcluir(),'UTF-8') . "', '" . mb_strtoupper($elemento->getReactivado(),'UTF-8') . "'," .
                " '" . mb_strtoupper($elemento->getProductoPrincipal(),'UTF-8') . "', '" . mb_strtoupper($elemento->getAcumulado(),'UTF-8') . "', '" . mb_strtoupper($elemento->getProgramado(),'UTF-8') . "'," .
                " '" . mb_strtoupper($elemento->getRecursos_Terceros(),'UTF-8') . "','" . mb_strtoupper($elemento->getObjetivo(),'UTF-8') . "'," . $elemento->getDel_Fase1() . ", " . $elemento->getAl_Fase1() . ", " . $elemento->getDel_Fase2() . ", " . $elemento->getAl_Fase2() . ", " . $elemento->getDel_Fase3() . "," .
                " " . $elemento->getAl_Fase3() . "," . $elemento->getDel_Fase4() . ", " . $elemento->getAl_Fase4() . ", " . $elemento->getDel_Fase5() . "," . $elemento->getAl_Fase5() . ", " . $elemento->getDel_Fase6() . ", " . $elemento->getAl_Fase6() . "," .
                " " . $elemento->getCostoTotal_Modificado() . ", '" . mb_strtoupper($elemento->getProductoPrincipal_Modificado(),'UTF-8') . "','" . mb_strtoupper($elemento->getLineaAccion_Modificado(),'UTF-8') . "', '" . mb_strtoupper($elemento->getNombreProyecto_Modificado(),'UTF-8') . "'," .
                " '" . mb_strtoupper($elemento->getPermanente_Modificado(),'UTF-8') . "', '" . mb_strtoupper($elemento->getConcluir_Modificado(),'UTF-8') . "', '" . $elemento->getAnio_Modificacion() . "', '" . $elemento->getAl_Modificacion() . "','" . mb_strtoupper($elemento->getJustificacion(),'UTF-8') . "','" . mb_strtoupper($elemento->getElabora(),'UTF-8') . "'," .
                " '" . mb_strtoupper($elemento->getPropone(),'UTF-8') . "', '" . mb_strtoupper($elemento->getAutoriza_Tecnico(),'UTF-8') . "', '" . mb_strtoupper($elemento->getAutoriza_Adminstrativo(),'UTF-8') . "',1)";
        
        $res = $con->obtenerLista($query);

        if ($res == "1") {
            return true;
        } else {
            return false;
        }
    }

    public function actualizaElemento($elemento) {
        $con = new Catalogo();
        $fechaElaboracion = "";
        $Autorizacion = "";

        if ($elemento->getFechaElaboracion() != null) {
            $fechaElaboracion = "'" . date("Y-m-d", strtotime($elemento->getFechaElaboracion())) . "'";
        } else {
            $fechaElaboracion = "null";
        }

        if ($elemento->getFechaAutorizacion() != null) {
            $Autorizacion = "'" . date("Y-m-d", strtotime($elemento->getFechaAutorizacion())) . "'";
        } else {
            $Autorizacion = "null";
        }
        
        if(($elemento->getDel_Fase1() == null) || $elemento->getDel_Fase1() == ""){
            $elemento->setDel_Fase1("null");
        }
        
        if(($elemento->getAl_Fase1()  == null) || $elemento->getAl_Fase1() == ""){
            $elemento->setAl_Fase1("null");
        }                
        
        if(($elemento->getDel_Fase2()  == null) || $elemento->getDel_Fase2() == ""){
            $elemento->setDel_Fase2("null");
        }
        
        if(($elemento->getAl_Fase2() == null) || $elemento->getAl_Fase2() == ""){
            $elemento->setAl_Fase2("null");
        }
        
        if(($elemento->getDel_Fase3() == null) || $elemento->getDel_Fase3() == ""){
            $elemento->setDel_Fase3("null");
        }
        
        if(($elemento->getAl_Fase3() == null) || $elemento->getAl_Fase3() == ""){
            $elemento->setAl_Fase3("null");
        }
        
        if(($elemento->getDel_Fase4() == null) || $elemento->getDel_Fase4() == ""){
            $elemento->setDel_Fase4("null");
        }
        
        if(($elemento->getAl_Fase4() == null) || $elemento->getAl_Fase4() == ""){
            $elemento->setAl_Fase4("null");
        }
        
        if(($elemento->getDel_Fase5() == null) || $elemento->getDel_Fase5() == ""){
            $elemento->setDel_Fase5("null");
        }
        
        if(($elemento->getAl_Fase5() == null) || $elemento->getAl_Fase5() == ""){
            $elemento->setAl_Fase5("null");
        }
        
        if(($elemento->getDel_Fase6() == null) || $elemento->getDel_Fase6() == ""){
            $elemento->setDel_Fase6("null");
        }
        
        if(($elemento->getAl_Fase6() == null) || $elemento->getAl_Fase6() == ""){
            $elemento->setAl_Fase6("null");
        }

        if(($elemento->getCostoTotal_Modificado() == null) || $elemento->getCostoTotal_Modificado() == ""){
            $elemento->setCostoTotal_Modificado("null");
        }

        $query = "UPDATE sie_tarjeta_registro  SET CPR_ID_PROYECTO='" . $elemento->getIdProyecto() . "',  tre_fechaelaboracion = " . $fechaElaboracion . ", tre_fechaautorizacion = " . $Autorizacion . ", tre_unidadadministrativa = '" . mb_strtoupper($elemento->getUnidadAdministrativa(),'UTF-8') . "', tre_responsable = '" . mb_strtoupper($elemento->getResponsable(),'UTF-8') . "', tre_clavelineaaccion = '" . $elemento->getClaveAccion() . "',  tre_clavesecretecnica='" . $elemento->getClaveTecnica() . "', tre_autorizadosinfin = '" . mb_strtoupper($elemento->getAutorizadoSinFinanciamiento(),'UTF-8') . "'" .
                ",tre_funcion = '" . mb_strtoupper($elemento->getFuncion(),'UTF-8') . "', tre_subfuncion = '" . mb_strtoupper($elemento->getSubFuncion(),'UTF-8') . "', tre_programageneral = '" . mb_strtoupper($elemento->getProgramaGeneral(),'UTF-8') . "' , tre_actividadinstitucional = '" . mb_strtoupper($elemento->getActividadInstitucional(),'UTF-8') . "', tre_actividadprioritaria = '" . mb_strtoupper($elemento->getActividadPrioritaria(),'UTF-8') . "', tre_tipoproyecto = '" . $elemento->getTipoProyecto() . "', tre_programadoaconcluir = '" . mb_strtoupper($elemento->getConcluir(),'UTF-8') . "' , tre_reactivado = '" . mb_strtoupper($elemento->getReactivado(),'UTF-8') . "'" .
                ",tre_productoprincipal = '" . mb_strtoupper($elemento->getProductoPrincipal(),'UTF-8') . "' , tre_acumuladoanterior = '" . mb_strtoupper($elemento->getAcumulado(),'UTF-8') . "' , tre_programadoparaX = '" . mb_strtoupper($elemento->getProgramado(),'UTF-8') . "' , tre_encasorecursosaportados = '" . mb_strtoupper($elemento->getRecursos_Terceros(),'UTF-8') . "', tre_objetivoproyecto='" . mb_strtoupper($elemento->getObjetivo(),'UTF-8') . "', tre_f1delanio = " . $elemento->getDel_Fase1() . " ,tre_f2delanio = " . $elemento->getDel_Fase2() . " , tre_f3delanio = " . $elemento->getDel_Fase3() . " , tre_f4delanio = " . $elemento->getDel_Fase4() . "" .
                " , tre_f5delanio = " . $elemento->getDel_Fase5() . " , tre_f6delanio = " . $elemento->getDel_Fase6() . ", tre_f1alanio = " . $elemento->getAl_Fase1() . " , tre_f2alanio = " . $elemento->getAl_Fase2() . " , tre_f3alanio = " . $elemento->getAl_Fase3() . " , tre_f4alanio = " . $elemento->getAl_Fase4() . " , tre_f5alanio = " . $elemento->getAl_Fase5() . " , tre_f6alanio = " . $elemento->getAl_Fase6() . " , tre_costototalmodifica = " . $elemento->getCostoTotal_Modificado() . " , tre_productoprincipalmodifica = '" . mb_strtoupper($elemento->getProductoPrincipal_Modificado(),'UTF-8') . "'" .
                " , tre_lineaaccionmodifica = '" . mb_strtoupper($elemento->getLineaAccion_Modificado(),'UTF-8') . "', tre_nombreproymodifica = '" . mb_strtoupper($elemento->getNombreProyecto_Modificado(),'UTF-8') . "' , tre_apermanentemodifica = '" . mb_strtoupper($elemento->getPermanente_Modificado(),'UTF-8') . "' , tre_programadoconcluirmodifica = '" . mb_strtoupper($elemento->getConcluir_Modificado(),'UTF-8') . "' , tre_aniodemodifica = '" . $elemento->getAnio_Modificacion() . "', tre_almodifica = '" . $elemento->getAl_Modificacion() . "' , tre_elabora = '" . mb_strtoupper($elemento->getElabora(),'UTF-8') . "' , tre_propone = '" . mb_strtoupper($elemento->getPropone(),'UTF-8') . "'" .
                " , tre_autorizast = '" . mb_strtoupper($elemento->getAutoriza_Tecnico(),'UTF-8') . "' , tre_autorizasa = '" . mb_strtoupper($elemento->getAutoriza_Adminstrativo(),'UTF-8') . "' , tre_justificacionpresupporcap = '" . mb_strtoupper($elemento->getJustificacion(),'UTF-8') . "'   WHERE tre_id_tarjeta = " . $elemento->getId() . " ";
        $res = $con->obtenerLista($query);

        if ($res == "1") {
            return true;
        } else {
            return false;
        }
    }

    public function eliminaElemento($idElemento) {

        $con = new Catalogo();
        $query = "UPDATE sie_tarjeta_registro  set  tre_estatus = 0 WHERE tre_id_tarjeta = " . $idElemento;
        $res = $con->obtenerLista($query);

        if ($res == "1") {
            return true;
        } else {
            return false;
        }
    }

}
