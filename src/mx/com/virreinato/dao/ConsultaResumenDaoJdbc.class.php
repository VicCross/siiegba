<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ConsultaResumen.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class ConsultaResumenDaoJdbc {
    
    public function obtieneProyectos($periodo){
		
	$lista = array();
	$query = " SELECT cpr_id_proyecto, cpr_numeroproyecto, cpr_descripcion " .
            "FROM sie_cat_proyectos " .
            "WHERE cpe_id_periodo = ".$periodo." AND cpr_estatus = 1 ORDER BY cpr_descripcion ";
		    
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $idProyecto = $rs["cpr_id_proyecto"];
            $num_proyecto = $rs["cpr_numeroproyecto"];
            $proyecto = $rs["cpr_descripcion"];

            $elemento = new ConsultaResumen();
            $elemento->proyecto($idProyecto,$num_proyecto,$proyecto);
            array_push($lista, $elemento);
        }
		    
        return $lista;	    
    }
    
    public function obtieneListado($periodo) {
		
        $lista=array();
        
        if($periodo == null){
            $periodo = "null";
        }

        $query="".
            " SELECT *FROM(SELECT(CAST(1 AS SIGNED)) AS identificador,p.cpr_id_proyecto,p.cpr_numeroproyecto,p.cpr_descripcion,".
            "sum(d.trdc_ene) + sum(d.trdc_feb) + sum(d.trdc_mar) + sum(d.trdc_abr) + sum(d.trdc_may) + sum(d.trdc_jun) + sum(d.trdc_jul) + sum(d.trdc_ago) + sum(d.trdc_sep) + sum(d.trdc_oct) + sum(d.trdc_nov) + sum(d.trdc_dic) AS autorizado,".
            "sum(d.trdc_ene) AS ene,sum(d.trdc_feb) AS feb,sum(d.trdc_mar) AS mar,sum(d.trdc_abr) AS abr,sum(d.trdc_may) AS may,sum(d.trdc_jun) AS jun,	sum(d.trdc_jul) AS jul,	sum(d.trdc_ago) AS ago,	sum(d.trdc_sep) AS sep,	sum(d.trdc_oct) AS oct,	sum(d.trdc_nov) AS nov,	sum(d.trdc_dic) AS dic".
            " FROM sie_cat_proyectos p, sie_tarjeta_registro t, sie_tarjeta_reg_det_cal d WHERE	p.cpr_id_proyecto = t.cpr_id_proyecto AND t.tre_id_tarjeta = d.tre_id_tarjeta AND p.cpr_estatus = 1 AND t.tre_estatus = 1 AND d.trdc_estatus = 1 AND p.cpe_id_periodo = ".$periodo." GROUP BY p.cpr_id_proyecto,p.cpr_numeroproyecto,p.cpr_descripcion".
            " UNION SELECT (CAST(2 AS SIGNED)) AS identificador, (CAST(0 AS SIGNED)) AS cpr_id_proyecto, (CAST(1 AS CHAR(20))) AS cpr_numeroproyecto,(CAST('Gasto Básico' AS CHAR (255))) AS cpr_descripcion,".
            "MONTO_ENERO + MONTO_FEBRERO + MONTO_MARZO + MONTO_ABRIL + MONTO_MAYO + MONTO_JUNIO + MONTO_JULIO + MONTO_AGOSTO + MONTO_SEPTIEMBRE + MONTO_OCTUBRE + MONTO_NOVIEMBRE + MONTO_DICIEMBRE AS autorizado,".
            "MONTO_ENERO AS ene,MONTO_FEBRERO AS feb,MONTO_MARZO AS mar,MONTO_ABRIL AS abr,MONTO_MAYO AS may,MONTO_JUNIO AS jun,MONTO_JULIO AS jul,MONTO_AGOSTO AS ago,	MONTO_SEPTIEMBRE AS sep, MONTO_OCTUBRE AS oct,MONTO_NOVIEMBRE AS nov,MONTO_DICIEMBRE AS dic".
            " FROM sie_presup_asignado WHERE cpe_id_periodo = ".$periodo." AND pas_estatus = 1 AND destino = 'GB' UNION SELECT (CAST(4 AS SIGNED)) AS identificador, (CAST(- 1 AS SIGNED)) AS cpr_id_proyecto, (CAST(1 AS CHAR(20))) AS cpr_numeroproyecto,(CAST('Logística Sueldos ' AS CHAR (255))) AS cpr_descripcion,".
            "sum(MIN_MONTO) AS autorizado,(SELECT SUM(MIN_MONTO) FROM sie_ministraciones WHERE cpe_id_periodo = ".$periodo." AND MIN_destino = 'SL' AND EXTRACT(MONTH FROM MIN_FECHA) = 1 GROUP BY	EXTRACT(MONTH FROM MIN_FECHA)) AS ene,(SELECT SUM(MIN_MONTO) FROM sie_ministraciones WHERE cpe_id_periodo = ".$periodo."".
            " AND MIN_destino = 'SL' AND EXTRACT(MONTH FROM MIN_FECHA) = 2 GROUP BY EXTRACT(MONTH FROM MIN_FECHA)) AS feb, (SELECT SUM(MIN_MONTO) FROM sie_ministraciones WHERE cpe_id_periodo = ".$periodo." AND MIN_destino = 'SL' AND EXTRACT(MONTH FROM MIN_FECHA) = 3 GROUP BY EXTRACT(MONTH FROM MIN_FECHA)) AS mar,".
            "(SELECT SUM(MIN_MONTO) FROM sie_ministraciones WHERE cpe_id_periodo = ".$periodo." AND MIN_destino = 'SL' AND EXTRACT(MONTH FROM MIN_FECHA) = 4 GROUP BY EXTRACT(MONTH FROM MIN_FECHA)) AS abr, (SELECT SUM(MIN_MONTO) FROM sie_ministraciones WHERE cpe_id_periodo = ".$periodo." AND MIN_destino = 'SL' AND EXTRACT(MONTH FROM MIN_FECHA) = 5 GROUP BY EXTRACT(MONTH FROM MIN_FECHA)) AS may,".
            "(SELECT SUM(MIN_MONTO) FROM sie_ministraciones WHERE cpe_id_periodo = ".$periodo." AND MIN_destino = 'SL' AND EXTRACT(MONTH FROM MIN_FECHA) = 6 GROUP BY EXTRACT(MONTH FROM MIN_FECHA)) AS jun, (SELECT SUM(MIN_MONTO) FROM sie_ministraciones WHERE cpe_id_periodo = ".$periodo." AND MIN_destino = 'SL' AND EXTRACT(MONTH FROM MIN_FECHA) = 7 GROUP BY EXTRACT(MONTH FROM MIN_FECHA)) AS jul,".
            "(SELECT SUM(MIN_MONTO) FROM sie_ministraciones WHERE cpe_id_periodo = ".$periodo." AND MIN_destino = 'SL' AND EXTRACT(MONTH FROM MIN_FECHA) = 8 GROUP BY EXTRACT(MONTH FROM MIN_FECHA)) AS ago, (SELECT SUM(MIN_MONTO) FROM sie_ministraciones WHERE cpe_id_periodo = ".$periodo." AND MIN_destino = 'SL' AND EXTRACT(MONTH FROM MIN_FECHA) = 9 GROUP BY EXTRACT(MONTH FROM MIN_FECHA)) AS sep,".
            "(SELECT SUM(MIN_MONTO) FROM sie_ministraciones WHERE cpe_id_periodo = ".$periodo." AND MIN_destino = 'SL' AND EXTRACT(MONTH FROM MIN_FECHA) = 10 GROUP BY EXTRACT(MONTH FROM MIN_FECHA)) AS oct, (SELECT SUM(MIN_MONTO) FROM sie_ministraciones WHERE cpe_id_periodo = ".$periodo." AND MIN_destino = 'SL' AND EXTRACT(MONTH FROM MIN_FECHA) = 11 GROUP BY EXTRACT(MONTH FROM MIN_FECHA)) AS nov,".
            "(SELECT SUM(MIN_MONTO) FROM sie_ministraciones WHERE cpe_id_periodo = ".$periodo." AND MIN_destino = 'SL' AND EXTRACT(MONTH FROM MIN_FECHA) = 12 GROUP BY EXTRACT(MONTH FROM MIN_FECHA)) AS dic".
            " FROM sie_ministraciones WHERE cpe_id_periodo = ".$periodo." AND MIN_destino = 'SL' GROUP BY MIN_destino UNION SELECT (CAST(3 AS SIGNED)) AS identificador, (CAST(- 2 AS SIGNED)) AS cpr_id_proyecto, (CAST(1 AS CHAR(20))) AS cpr_numeroproyecto, (CAST('Terceros' AS CHAR(255))) AS cpr_descripcion,".
            "MONTO_ENERO + MONTO_FEBRERO + MONTO_MARZO + MONTO_ABRIL + MONTO_MAYO + MONTO_JUNIO + MONTO_JULIO + MONTO_AGOSTO + MONTO_SEPTIEMBRE + MONTO_OCTUBRE + MONTO_NOVIEMBRE + MONTO_DICIEMBRE AS autorizado,".
            "MONTO_ENERO AS ene, MONTO_FEBRERO AS feb, MONTO_MARZO AS mar, MONTO_ABRIL AS abr, MONTO_MAYO AS may, MONTO_JUNIO AS jun, MONTO_JULIO AS jul, MONTO_AGOSTO AS ago, MONTO_SEPTIEMBRE AS sep, MONTO_OCTUBRE AS oct, MONTO_NOVIEMBRE AS nov, MONTO_DICIEMBRE AS dic".
            " FROM sie_presup_asignado WHERE cpe_id_periodo = ".$periodo." AND pas_estatus = 1 AND destino = 'TR' UNION SELECT (CAST(5 AS SIGNED)) AS identificador, (CAST(- 3 AS SIGNED)) AS cpr_id_proyecto, (CAST(1 AS CHAR(20))) AS cpr_numeroproyecto, (CAST('Donativos' AS CHAR (255))) AS cpr_descripcion,".
            "sum(MIN_MONTO) AS autorizado, (SELECT SUM(MIN_MONTO) FROM sie_ministraciones WHERE cpe_id_periodo = ".$periodo." AND MIN_destino = 'DN' AND EXTRACT(MONTH FROM MIN_FECHA) = 1 GROUP BY EXTRACT(MONTH FROM MIN_FECHA)) AS ene,".
            "(SELECT SUM(MIN_MONTO) FROM sie_ministraciones WHERE cpe_id_periodo = ".$periodo." AND MIN_destino = 'DN' AND EXTRACT(MONTH FROM MIN_FECHA) = 2 GROUP BY EXTRACT(MONTH FROM MIN_FECHA)) AS feb,".
            "(SELECT SUM(MIN_MONTO) FROM sie_ministraciones WHERE cpe_id_periodo = ".$periodo." AND MIN_destino = 'DN' AND EXTRACT(MONTH FROM MIN_FECHA) = 3 GROUP BY EXTRACT(MONTH FROM MIN_FECHA)) AS mar,".
            "(SELECT SUM(MIN_MONTO) FROM sie_ministraciones WHERE cpe_id_periodo = ".$periodo." AND MIN_destino = 'DN' AND EXTRACT(MONTH FROM MIN_FECHA) = 4 GROUP BY EXTRACT(MONTH FROM MIN_FECHA)) AS abr,".
            "(SELECT SUM(MIN_MONTO) FROM sie_ministraciones WHERE cpe_id_periodo = ".$periodo." AND MIN_destino = 'DN' AND EXTRACT(MONTH FROM MIN_FECHA) = 5 GROUP BY EXTRACT(MONTH FROM MIN_FECHA)) AS may,".
            "(SELECT SUM(MIN_MONTO) FROM sie_ministraciones WHERE cpe_id_periodo = ".$periodo." AND MIN_destino = 'DN' AND EXTRACT(MONTH FROM MIN_FECHA) = 6 GROUP BY EXTRACT(MONTH FROM MIN_FECHA)) AS jun,".
            "(SELECT SUM(MIN_MONTO) FROM sie_ministraciones WHERE cpe_id_periodo = ".$periodo." AND MIN_destino = 'DN' AND EXTRACT(MONTH FROM MIN_FECHA) = 7 GROUP BY EXTRACT(MONTH FROM MIN_FECHA)) AS jul,".
            "(SELECT SUM(MIN_MONTO) FROM sie_ministraciones WHERE cpe_id_periodo = ".$periodo." AND MIN_destino = 'DN' AND EXTRACT(MONTH FROM MIN_FECHA) = 8 GROUP BY EXTRACT(MONTH FROM MIN_FECHA)) AS ago,".
            "(SELECT SUM(MIN_MONTO) FROM sie_ministraciones WHERE cpe_id_periodo = ".$periodo." AND MIN_destino = 'DN' AND EXTRACT(MONTH FROM MIN_FECHA) = 9 GROUP BY EXTRACT(MONTH FROM MIN_FECHA)) AS sep,".
            "(SELECT SUM(MIN_MONTO) FROM sie_ministraciones WHERE cpe_id_periodo = ".$periodo." AND MIN_destino = 'DN' AND EXTRACT(MONTH FROM MIN_FECHA) = 10 GROUP BY EXTRACT(MONTH FROM MIN_FECHA)) AS oct,".
            "(SELECT SUM(MIN_MONTO) FROM sie_ministraciones WHERE cpe_id_periodo = ".$periodo." AND MIN_destino = 'DN' AND EXTRACT(MONTH FROM MIN_FECHA) = 11 GROUP BY EXTRACT(MONTH FROM MIN_FECHA)) AS nov,".
            "(SELECT SUM(MIN_MONTO) FROM sie_ministraciones WHERE cpe_id_periodo = ".$periodo." AND MIN_destino = 'DN' AND EXTRACT(MONTH FROM MIN_FECHA) = 12 GROUP BY EXTRACT(MONTH FROM MIN_FECHA)) AS dic".
            " FROM sie_ministraciones WHERE cpe_id_periodo = ".$periodo." AND MIN_destino = 'DN' GROUP BY MIN_destino) AS tabla_1 ORDER BY identificador, cpr_descripcion";
        
        //echo $query;
        
        $rsComprobado = null;
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs["cpr_id_proyecto"];
            $numProyecto= $rs["cpr_numeroproyecto"];
            $proyecto= $rs["cpr_descripcion"];
            $autorizado=$rs["autorizado"];
            $ene=$rs["ene"];
            $feb=$rs["feb"];
            $mar=$rs["mar"];
            $abr=$rs["abr"];
            $may=$rs["may"];
            $jun=$rs["jun"];
            $jul=$rs["jul"];
            $ago=$rs["ago"];
            $sep=$rs["sep"];
            $oct=$rs["oct"];
            $nov=$rs["nov"];
            $dic=$rs["dic"];
            $comprobado = 0.0;
            
            if($id == 0){
                //gasto basico
                $query = "SELECT  sum(d.dci_monto) as comprobadoINAH " .
                "FROM sie_comprobaciones_inah c, sie_det_comprobaciones_inah d, sie_ministraciones m " .
                "WHERE  m.min_id_ministracion = c.min_id_ministracion AND c.cin_id_cinah = d.cin_id_cinah " .
                " AND c.cin_estatus = 1  AND d.dci_estatus = 1 AND m.cpr_id_proyecto = ".$id." AND m.min_formacarga = 1 " .
                " AND c.cpe_id_periodo= ".$periodo." and m.min_destino='GB' " .
                " GROUP BY(d.dci_id_detcinah) ";
            }else if($id == -1){
                //suledos. lo comprobado es igual a la suma de los cheques, es decir lo gastado
                $query="SELECT sum(ch.che_monto) as comprobadoINAH " .
                   " FROM  sie_cheques ch " .
                   " WHERE ch.che_estatus<>'CA' AND ch.che_estatusborrado=1 AND  " .
                   " che_destino='SL' ".
                   " GROUP BY  (ch.che_destino)";
            }else if($id == -2 || $id == -3 ){
                //terceros. lo comprobado es igual a la suma de los cheques, es decir lo gastado
                $query="SELECT 0.0 as comprobadoINAH " .
                   " FROM  sie_comprobaciones_inah LIMIT 1 " ;
            }else{
                $query = "SELECT  sum(d.dci_monto) as comprobadoINAH " .
                    "FROM sie_comprobaciones_inah c, sie_det_comprobaciones_inah d, sie_ministraciones m " .
                    "WHERE  m.min_id_ministracion = c.min_id_ministracion AND c.cin_id_cinah = d.cin_id_cinah " .
                    " AND c.cin_estatus = 1  AND d.dci_estatus = 1 AND m.cpr_id_proyecto = ".$id." AND m.min_formacarga = 1 and m.min_destino='PR' " .
                    " GROUP BY(d.dci_id_detcinah) ";
            }
            
            $rsComprobado = $catalogo->obtenerLista($query);
            if($rsComprobado!=null){
                while($rs = mysql_fetch_array($rsComprobado)){
                    $comprobado = $rs["comprobadoINAH"];
                }
            }
            
            $ministrado=0.0;
					
            if($id == 0){
                $query="SELECT sum(min_monto) as ministrado " .
                  "FROM sie_ministraciones m  ". 
                  "WHERE m.min_formacarga=1 AND m.cpr_id_proyecto = ".$id." AND " .
                  "m.cpe_id_periodo= ".$periodo." and m.min_destino='GB' " .
                  " GROUP BY m.cpr_id_proyecto ";
            }
            else if($id == -1){
                $query="SELECT sum(min_monto) as ministrado " .
                  "FROM sie_ministraciones m  ". 
                  "WHERE m.min_formacarga=1 AND  " .
                  "m.cpe_id_periodo= ".$periodo." and m.min_destino='SL' " .
                  " GROUP BY m.min_destino ";
            }
            else if($id == -2){
                    //terceros
                $query="SELECT sum(min_monto) as ministrado " .
                  "FROM sie_ministraciones m  ". 
                  "WHERE m.min_formacarga=1 AND  " .
                  "m.cpe_id_periodo= ".$periodo." and m.min_destino='TR' " .
                  " GROUP BY m.min_destino ";
            }
            else if($id == -3){
                    //donativos
                $query="SELECT sum(min_monto) as ministrado " .
                      "FROM sie_ministraciones m  ". 
                      "WHERE m.min_formacarga=1 AND  " .
                      "m.cpe_id_periodo= ".$periodo." and m.min_destino='DN' " .
                      " GROUP BY m.min_destino ";
            }
            else{
                $query="SELECT sum(min_monto) as ministrado " .
                  "FROM sie_ministraciones m  ". 
                  "WHERE m.min_formacarga=1 AND m.cpr_id_proyecto = ".$id." and m.min_destino='PR' ".
                  " and m.cpe_id_periodo= ".$periodo." GROUP BY m.cpr_id_proyecto ";

            }
            
            $rsmin = $catalogo->obtenerLista($query);
												
            if($rsmin!=null){
                while($rs = mysql_fetch_array($rsmin)){
                    $ministrado=$rs["ministrado"];
                }
            }
            
            $gastado=0.0;
            
            if($id == 0){
                $query="SELECT sum(ch.che_monto) as gastado " .
                           " FROM  sie_cheques ch, sie_solicitud_presup s " .
                           " WHERE ch.spr_id_solicitud=s.spr_id_solicitud and ch.che_estatus<>'CA' and ch.che_estatus<>'RE'  AND ch.che_estatusborrado=1 " .
                           " and che_destino='GB' and ch.cpe_id_periodo= ".$periodo." and s.spr_pre_estatus=1 and s.spr_destino='GB' ".
                           " GROUP BY  (ch.che_destino)";
            }
            else if($id == -1){
                $query="SELECT sum(ch.che_monto) as gastado " .
                           " FROM  sie_cheques ch, sie_solicitud_presup s " .
                           " WHERE ch.spr_id_solicitud=s.spr_id_solicitud and ch.che_estatus<>'CA' and ch.che_estatus<>'RE'  AND ch.che_estatusborrado=1 AND  " .
                           " che_destino='SL' and ch.cpe_id_periodo= ".$periodo." and s.spr_pre_estatus=1 and s.spr_destino='SL' ".
                           " GROUP BY  (ch.che_destino)";
            }
            else if($id == -2){
                $query="SELECT sum(ch.che_monto) as gastado " .
                           " FROM  sie_cheques ch, sie_solicitud_presup s " .
                           " WHERE ch.spr_id_solicitud=s.spr_id_solicitud and ch.che_estatus<>'CA' and ch.che_estatus<>'RE'  AND ch.che_estatusborrado=1 AND  " .
                           " che_destino='TR' and ch.cpe_id_periodo= ".$periodo." and s.spr_pre_estatus=1 and s.spr_destino='TR' ".
                           " GROUP BY  (ch.che_destino)";
            }
            else if($id == -3){
                $query="SELECT sum(ch.che_monto) as gastado " .
                           " FROM  sie_cheques ch, sie_solicitud_presup s " .
                           " WHERE ch.spr_id_solicitud=s.spr_id_solicitud and ch.che_estatus<>'CA' and ch.che_estatus<>'RE'  AND ch.che_estatusborrado=1 AND  " .
                           " che_destino='DN' and ch.cpe_id_periodo= ".$periodo." and s.spr_pre_estatus=1 and s.spr_destino='DN' ".
                           " GROUP BY  (ch.che_destino)";
            }

            else{
                $query="SELECT sum(ch.che_monto) as gastado " .
                   " FROM  sie_cheques ch, sie_solicitud_presup s " .
                   " WHERE ch.spr_id_solicitud=s.spr_id_solicitud and ch.che_estatus<>'CA' and ch.che_estatus<>'RE'  AND ch.che_estatusborrado=1 AND ch.cpr_id_proyecto = ".$id.
                   " and che_destino='PR' and ch.cpe_id_periodo= ".$periodo." and s.spr_pre_estatus=1 and s.spr_destino='PR' ".
                   " GROUP BY  (ch.cpr_id_proyecto)";
            }

            $rsgas = $catalogo->obtenerLista($query);
																	
            if($rsgas!=null){
                while($rs = mysql_fetch_array($rsgas)){
                    if($rs!=null )
                    {        $gastado=$rs["gastado"]; }
                }
            }
            
            $elemento = new ConsultaResumen();
            $elemento->resumenProyecto($id,$numProyecto,$proyecto,$autorizado,$ministrado,0.0,$gastado,0.0,0.0,$comprobado,0.0, $ene,$feb,$mar,$abr,$may,$jun,$jul,$ago,$sep,$oct,$nov,$dic);
            array_push($lista, $elemento);
        }
        
        return $lista;
    }
    
    public function obtieneMetas( $idProyecto ){
        $lista= array();
	$query  = "";

	$query="SELECT pm.proyecto_id_meta ,pm.proyecto_desc_meta, " .
            " sum(d.trdc_ene)+sum(d.trdc_feb)+sum(d.trdc_mar)+sum(d.trdc_abr)+sum(d.trdc_may)+sum(d.trdc_jun)+sum(d.trdc_jul)+sum(d.trdc_ago)+sum(d.trdc_sep)+sum(d.trdc_oct)+sum(d.trdc_nov)+sum(d.trdc_dic) as autorizado ," .
            " sum(d.trdc_ene) as ene,sum(d.trdc_feb) as feb,sum(d.trdc_mar) as mar,sum(d.trdc_abr) as abr, sum(d.trdc_may) as may," .
            " sum(d.trdc_jun) as jun,sum(d.trdc_jul) as jul,sum(d.trdc_ago) as ago,sum(d.trdc_sep) as sep,sum(d.trdc_oct) as oct, sum(d.trdc_nov) as nov,sum(d.trdc_dic) as dic " .
            " FROM  sie_cat_proyectos p, sie_proyecto_metas pm, sie_tarjeta_registro t, sie_tarjeta_reg_det_cal d ".
            " WHERE p.cpr_id_proyecto=pm.cpr_id_proyecto AND  pm.cpr_id_proyecto=t.cpr_id_proyecto AND t.tre_id_tarjeta=d.tre_id_tarjeta AND " .
            " pm.proyecto_id_meta=d.proyecto_id_meta AND " .
            " p.cpr_estatus=1 AND  pm.proyecto_estatus_meta = 1 AND t.tre_estatus = 1 AND  d.trdc_estatus = 1 AND " .
            " p.cpr_id_proyecto = ".$idProyecto." ".
            "GROUP BY pm.proyecto_id_meta,pm.proyecto_desc_meta ORDER BY( pm.proyecto_desc_meta)";

	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $idMeta = $rs["proyecto_id_meta"];
            $meta = $rs["proyecto_desc_meta"];
            $autorizado=$rs["autorizado"];
            $ene=$rs["ene"];
            $feb=$rs["feb"];
            $mar=$rs["mar"];
            $abr=$rs["abr"];
            $may=$rs["may"];
            $jun=$rs["jun"];
            $jul=$rs["jul"];
            $ago=$rs["ago"];
            $sep=$rs["sep"];
            $oct=$rs["oct"];
            $nov=$rs["nov"];
            $dic=$rs["dic"];

            $elemento = new ConsultaResumen();
            $elemento->resumenMeta($idMeta,$meta,$autorizado,0.0,0.0,0.0,0.0,0.0,0.0,0.0,$ene,$feb,$mar,$abr,$may,$jun,$jul,$ago,$sep,$oct,$nov,$dic);
            array_push($lista, $elemento);
				
        }
		
	$query=" SELECT pm.proyecto_id_meta, pm.proyecto_desc_meta, sum(ch.che_monto) as gastado " .
            "FROM sie_solicitud_presup pre, sie_cheques ch, sie_proyecto_metas pm " .
            "WHERE  ch.cpr_id_proyecto = ".$idProyecto." AND ch.che_estatusborrado = 1  AND ch.che_estatus<>'CA' " .
            "AND ch.spr_id_solicitud = pre.spr_id_solicitud AND pre.spr_pre_estatus =  1 " .
            "AND  pre.proyecto_id_meta =  pm.proyecto_id_meta  " .
            "GROUP BY pm.proyecto_id_meta, pm.proyecto_desc_meta ORDER BY pm.proyecto_desc_meta ";

	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);
    
        $i = 0;
        while ($rs = mysql_fetch_array($result)){
            $gastado=$rs["gastado"];
            $lista[$i]->setGastado($gastado);
            $i++;
        }	
        return $lista;
    }
    
    public function obtieneMetas2($Usuario, $idPeriodo){
        
	$lista= array();
	$query  = "";
	if($idPeriodo == null){
            $idPeriodo = "null";
        }	
        if($Usuario == null){
            $Usuario = "null";
        }
        $query="SELECT pm.proyecto_id_meta ,pm.proyecto_desc_meta, " .
          "sum(d.trdc_ene)+sum(d.trdc_feb)+sum(d.trdc_mar)+sum(d.trdc_abr)+sum(d.trdc_may)+sum(d.trdc_jun)+sum(d.trdc_jul)+sum(d.trdc_ago)+sum(d.trdc_sep)+sum(d.trdc_oct)+sum(d.trdc_nov)+sum(d.trdc_dic) as autorizado, " .
          "sum(d.trdc_ene) as ene,sum(d.trdc_feb) as feb,sum(d.trdc_mar) as mar,sum(d.trdc_abr) as abr, sum(d.trdc_may) as may," .
          "sum(d.trdc_jun) as jun,sum(d.trdc_jul) as jul,sum(d.trdc_ago) as ago,sum(d.trdc_sep) as sep,sum(d.trdc_oct) as oct, sum(d.trdc_nov) as nov,sum(d.trdc_dic) as dic " .
          " FROM  sie_cat_proyectos p, sie_proyecto_metas pm, sie_tarjeta_registro t, sie_tarjeta_reg_det_cal d, sie_usuario_x_meta um ".
          " WHERE p.cpr_id_proyecto=pm.cpr_id_proyecto AND  pm.cpr_id_proyecto=t.cpr_id_proyecto AND t.tre_id_tarjeta=d.tre_id_tarjeta AND " .
          " pm.proyecto_id_meta=d.proyecto_id_meta AND um.proyecto_id_meta = pm.proyecto_id_meta AND" .
          " p.cpr_estatus=1 AND  pm.proyecto_estatus_meta = 1 AND t.tre_estatus = 1 AND  d.trdc_estatus = 1 AND " .
          " p.cpe_id_periodo = ".$idPeriodo."  AND um.cus_id_usuario = ".$Usuario.
          " GROUP BY pm.proyecto_id_meta ,pm.proyecto_desc_meta ORDER BY( pm.proyecto_desc_meta)";
        
        $catalogo = new Catalogo();
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $idMeta = $rs["proyecto_id_meta"];
            $meta = $rs["proyecto_desc_meta"];
            $autorizado=$rs["autorizado"];
            $ene=$rs["ene"];
            $feb=$rs["feb"];
            $mar=$rs["mar"];
            $abr=$rs["abr"];
            $may=$rs["may"];
            $jun=$rs["jun"];
            $jul=$rs["jul"];
            $ago=$rs["ago"];
            $sep=$rs["sep"];
            $oct=$rs["oct"];
            $nov=$rs["nov"];
            $dic=$rs["dic"];

            $elemento = new ConsultaResumen();
            $elemento->resumenMeta($idMeta,$meta,$autorizado,0.0,0.0,0.0,0.0,0.0,0.0,0.0,$ene,$feb,$mar,$abr,$may,$jun,$jul,$ago,$sep,$oct,$nov,$dic);
            array_push($lista, $elemento);
        }
		
		//------------------------------------------------------------------------------------------------------------
		
		//Gastado------------------------------------------------------------------------------------
		
	$query=" SELECT pm.proyecto_id_meta, pm.proyecto_desc_meta, sum(ch.che_monto) as gastado " .
            "FROM sie_solicitud_presup pre, sie_cheques ch, sie_proyecto_metas pm, sie_usuario_x_meta um, sie_cat_proyectos pro " .
            "WHERE pro.cpr_id_proyecto=pm.cpr_id_proyecto AND pm.proyecto_id_meta=pre.proyecto_id_meta AND " .
            "pm.proyecto_id_meta=um.proyecto_id_meta AND  pre.spr_id_solicitud=ch.spr_id_solicitud AND  pro.cpr_id_proyecto = ch.cpr_id_proyecto" .
            " AND pro.cpr_estatus=1 AND pm.proyecto_estatus_meta= 1 AND  pre.spr_pre_estatus =  1 " .
            " AND ch.che_estatusborrado = 1  AND ch.che_estatus<>'CA' " .
            " AND um.cus_id_usuario = ".$Usuario.
            " AND pro.cpe_id_periodo = ".$idPeriodo.
            " AND ch.cpe_id_periodo = ".$idPeriodo.
            " GROUP BY pm.proyecto_id_meta, pm.proyecto_desc_meta " .
            " ORDER BY pm.proyecto_desc_meta ";	
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);
    
        $i = 0;
        while ($rs = mysql_fetch_array($result)){
            $gastado=$rs["gastado"];
            $lista[i]->setGastado($gastado);
            $i++;
        }	
        return $lista;
    }
    
    public function obtienePartidasComprobadas ($idMinistracion){
	$lista= array();
	$query  = "";

        $query = "SELECT par.cpa_id_partida, par.cpa_partida, par.cpa_descripcion, detC.dci_monto " .
            "FROM sie_cat_partidas par, sie_comprobaciones_inah c, sie_det_comprobaciones_inah detC " .
            "WHERE c.min_id_ministracion = ".$idMinistracion." AND c.cin_estatus = 1 " .
            "AND c.cin_id_cinah = detC.cin_id_cinah AND detC.dci_estatus = 1 " .
            "AND detC.cpa_id_partida = par.cpa_id_partida ORDER BY(par.cpa_partida)";

	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $idPartida = $rs["cpa_id_partida"];
            $partida = $rs["cpa_partida"];
            $parDesc = $rs["cpa_descripcion"];
            $comprobado = $rs["dci_monto"];

            $elemento = new ConsultaResumen();
            $elemento->consulta($idPartida,$partida,$parDesc,$comprobado);
            array_push($lista, $elemento);
					
        }
		
        return $lista;
		
    }
    
    public function obtienePartidas($idMeta ){
	$lista= array();
	$query  = "";
		
        //Autorizado-------NO SE PUEDE SACAR AUTORIZADO POR META, FALTA ESTA-----------------------------------------------------------------------------
        /*
        query="SELECT p.cpa_id_partida, p.cpa_partida, p.cpa_descripcion, " .
                  "sum(RH.CCDH_HONORARIOS).sum(RM.CCDM_COSTOTOTAL) as autorizado  " .
                  "FROM SIE_CARTA_CONSTITUTIVA C, SIE_CARTA_CONS_DET_RMAT RM, SIE_CARTA_CONS_DET_RHUM RH, sie_cat_partidas p " .
                  "WHERE C.CCO_ID_CARTA=RH.CCO_ID_CARTA AND C.CCO_ID_CARTA=RM.CCO_ID_CARTA AND RM.CCDM_PARTIDA=P.CPA_PARTIDA " .
                  "AND RH.CCDA_PARTIDA=P.CPA_PARTIDA AND C.PROYECTO_ID_META= ".idMeta." AND C.CCO_ESTATUS='Y' AND RM.CCDM_ESTATUS = 1 " .
                  "AND RH.CCDH_ESTATUS = 1 " .
                  "GROUP BY( p.cpa_id_partida ,p.cpa_partida, p.cpa_descripcion ) ORDER BY( p.cpa_id_partida )";
        */		
        $query="SELECT p.cpa_id_partida,p.cpa_partida, p.cpa_descripcion, " .
            "sum(d.trdc_ene)+sum(d.trdc_feb)+sum(d.trdc_mar)+sum(d.trdc_abr)+sum(d.trdc_may)+sum(d.trdc_jun)+sum(d.trdc_jul)+sum(d.trdc_ago)+sum(d.trdc_sep)+sum(d.trdc_oct)+sum(d.trdc_nov)+sum(d.trdc_dic) as autorizado , " .
            "sum(d.trdc_ene) as ene,sum(d.trdc_feb) as feb,sum(d.trdc_mar) as mar,sum(d.trdc_abr) as abr, " .
            "sum(d.trdc_may) as may,sum(d.trdc_jun) as jun,sum(d.trdc_jul) as jul,sum(d.trdc_ago) as ago," .
            "sum(d.trdc_sep) as sep,sum(d.trdc_oct) as oct,sum(d.trdc_nov) as nov,sum(d.trdc_dic) as dic " .
            "FROM sie_tarjeta_registro t, sie_tarjeta_reg_det_cal d, sie_cat_partidas p " .
            "WHERE  t.tre_id_tarjeta = d.tre_id_tarjeta AND d.cpa_id_partida = p.cpa_id_partida " .
            " AND d.proyecto_id_meta = '".$idMeta."' AND t.tre_estatus = 1 AND d.trdc_estatus = 1  " .
            "GROUP BY p.cpa_id_partida ,p.cpa_partida, p.cpa_descripcion ORDER BY( p.cpa_partida )";
        
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $idPartida = $rs["cpa_id_partida"];
            $partida = $rs["cpa_partida"];
            $parDesc = $rs["cpa_descripcion"];
            $autorizado=$rs["autorizado"];
            $ene=$rs["ene"];
            $feb=$rs["feb"];
            $mar=$rs["mar"];
            $abr=$rs["abr"];
            $may=$rs["may"];
            $jun=$rs["jun"];
            $jul=$rs["jul"];
            $ago=$rs["ago"];
            $sep=$rs["sep"];
            $oct=$rs["oct"];
            $nov=$rs["nov"];
            $dic=$rs["dic"];

            $ministrado = 0.0;
            $gastado = 0.0;
            $ministracion = 0;
					
            $queryg=strtolower(" SELECT  sum(D.SPR_MONTO) as gastado " .
            "FROM sie_solicitud_presup pre, SIE_DET_SOL_PRESUP D, sie_cheques ch, sie_proyecto_metas pm " .
            "WHERE  ch.che_estatusborrado = 1  AND ch.che_estatus<>'CA' " .
            "AND ch.spr_id_solicitud = pre.spr_id_solicitud AND pre.spr_pre_estatus =  1 " .
            "AND D.SPR_ID_SOLICITUD=pre.spr_id_solicitud AND D.CPA_ID_PARTIDA=".$idPartida."  " .
            "AND  pre.proyecto_id_meta =  pm.proyecto_id_meta AND pm.proyecto_id_meta= ".$idMeta."  " .
            " GROUP BY pm.proyecto_id_meta, D.CPA_ID_PARTIDA ORDER BY pm.proyecto_desc_meta ");

            $resultGastado = $catalogo->obtenerLista($queryg);

            if($rsGastado = mysql_fetch_array($resultGastado)){
                $gastado = $rsGastado["gastado"];
            }

            /*$Pquery = "SELECT min.min_id_ministracion,par.cpa_id_partida, min.min_monto as ministrado, f.sfo_id_solfondos " .
                            "FROM sie_solicitud_fondos f, sie_proyecto_metas m, sie_tarjeta_registro tr, sie_tarjeta_reg_det_cal d, sie_cat_partidas par, sie_ministraciones min, sie_cat_proyectos pro " .
                            "WHERE par.cpa_id_partida = ".idPartida." AND par.cpa_id_partida = d.cpa_id_partida AND d.trdc_estatus = 1 AND d.tre_id_tarjeta = tr.tre_id_tarjeta AND tr.tre_estatus = 1 " .
                            "AND tr.proyecto_id_meta = m.proyecto_id_meta AND m.cpr_id_proyecto = pro.cpr_id_proyecto AND  pro.cpr_id_proyecto = min.cpr_id_proyecto " .
                            "AND min.min_formacarga = 1 AND  min.sfo_id_solfondos  = f.sfo_id_solfondos AND f.sfo_estatus = 1 " .
                            "GROUP BY(min.min_id_ministracion,par.cpa_id_partida,min.min_monto,f.sfo_id_solfondos)";


            *
            rsMinistrado = con.retornarConsulta(Pquery);

            if( rsMinistrado.next() ){
                     ministrado = rsMinistrado.getDouble("ministrado");
                     ministracion = rsMinistrado.getInt("min_id_ministracion");

            }*/
					
            $elemento = new ConsultaResumen();
            $elemento->resumenPartida($idPartida,$partida,$parDesc,$autorizado,$ministrado,0.0,$gastado,0.0,0.0,0.0,0.0,$ene,$feb,$mar,$abr,$may,$jun,$jul,$ago,$sep,$oct,$nov,$dic);
					
            $elemento->setIdMinistracion($ministracion);
            array_push($lista, $elemento);
        }
		//-----------------------------------------------------------------------------------------------------------------------
	return $lista;
    }
    
    public function obtienePlaneacionGb($periodo) {
		
	$lista= array();
		
	$query  = "";
        if($periodo == null){
            $periodo = "null";
        }
		
	$query=" Select * FROM (".
            " SELECT (CAST('Autorizado Acumulado' AS CHAR(255)))  as cpr_descripcion, (CAST(1 AS CHAR(20)))  as cpr_numeroproyecto, " .
            " (MONTO_ENERO) as ene," .
            " (MONTO_ENERO+MONTO_FEBRERO) as feb, " .
            " (MONTO_ENERO+MONTO_FEBRERO+MONTO_MARZO) as mar, ".
            " (MONTO_ENERO+MONTO_FEBRERO+MONTO_MARZO+MONTO_ABRIL) as abr, " .
            " (MONTO_ENERO+MONTO_FEBRERO+MONTO_MARZO+MONTO_ABRIL+MONTO_MAYO) as may," .
            " (MONTO_ENERO+MONTO_FEBRERO+MONTO_MARZO+MONTO_ABRIL+MONTO_MAYO+MONTO_JUNIO) as jun, " .
            " (MONTO_ENERO+MONTO_FEBRERO+MONTO_MARZO+MONTO_ABRIL+MONTO_MAYO+MONTO_JUNIO+MONTO_JULIO) as jul, " .
            " (MONTO_ENERO+MONTO_FEBRERO+MONTO_MARZO+MONTO_ABRIL+MONTO_MAYO+MONTO_JUNIO+MONTO_JULIO+MONTO_AGOSTO) as ago, " .
            " (MONTO_ENERO+MONTO_FEBRERO+MONTO_MARZO+MONTO_ABRIL+MONTO_MAYO+MONTO_JUNIO+MONTO_JULIO+MONTO_AGOSTO+MONTO_SEPTIEMBRE) as sep, " .
            " (MONTO_ENERO+MONTO_FEBRERO+MONTO_MARZO+MONTO_ABRIL+MONTO_MAYO+MONTO_JUNIO+MONTO_JULIO+MONTO_AGOSTO+MONTO_SEPTIEMBRE+MONTO_OCTUBRE) as oct, ".
            " (MONTO_ENERO+MONTO_FEBRERO+MONTO_MARZO+MONTO_ABRIL+MONTO_MAYO+MONTO_JUNIO+MONTO_JULIO+MONTO_AGOSTO+MONTO_SEPTIEMBRE+MONTO_OCTUBRE+MONTO_NOVIEMBRE) as nov,  ".
            " (MONTO_ENERO+MONTO_FEBRERO+MONTO_MARZO+MONTO_ABRIL+MONTO_MAYO+MONTO_JUNIO+MONTO_JULIO+MONTO_AGOSTO+MONTO_SEPTIEMBRE+MONTO_OCTUBRE+MONTO_NOVIEMBRE+MONTO_DICIEMBRE) as dic ".
            " FROM sie_presup_asignado WHERE cpe_id_periodo= ".$periodo." and pas_estatus=1 AND destino='GB' " .
            " UNION " .
            " SELECT (CAST('Ministrado' AS CHAR(255)))  as cpr_descripcion, (CAST(2 AS CHAR(20)))  as cpr_numeroproyecto, " .
            " (SELECT SUM(MIN_MONTO) from sie_ministraciones WHERE cpe_id_periodo= ".$periodo." and  MIN_destino='GB' and EXTRACT(month FROM MIN_FECHA)=1 group by EXTRACT(month FROM MIN_FECHA) ) as ene, ".
            " (SELECT SUM(MIN_MONTO) from sie_ministraciones WHERE cpe_id_periodo= ".$periodo." and  MIN_destino='GB' and EXTRACT(month FROM MIN_FECHA)=2 group by EXTRACT(month FROM MIN_FECHA) ) as feb, ".
            " (SELECT SUM(MIN_MONTO) from sie_ministraciones WHERE cpe_id_periodo= ".$periodo." and  MIN_destino='GB' and EXTRACT(month FROM MIN_FECHA)=3 group by EXTRACT(month FROM MIN_FECHA) ) as mar, ".
            " (SELECT SUM(MIN_MONTO) from sie_ministraciones WHERE cpe_id_periodo= ".$periodo." and  MIN_destino='GB' and EXTRACT(month FROM MIN_FECHA)=4 group by EXTRACT(month FROM MIN_FECHA) ) as abr, ".
            " (SELECT SUM(MIN_MONTO) from sie_ministraciones WHERE cpe_id_periodo= ".$periodo." and  MIN_destino='GB' and EXTRACT(month FROM MIN_FECHA)=5 group by EXTRACT(month FROM MIN_FECHA) ) as may, ".
            " (SELECT SUM(MIN_MONTO) from sie_ministraciones WHERE cpe_id_periodo= ".$periodo." and  MIN_destino='GB' and EXTRACT(month FROM MIN_FECHA)=6 group by EXTRACT(month FROM MIN_FECHA) ) as jun, ".
            " (SELECT SUM(MIN_MONTO) from sie_ministraciones WHERE cpe_id_periodo= ".$periodo." and  MIN_destino='GB' and EXTRACT(month FROM MIN_FECHA)=7 group by EXTRACT(month FROM MIN_FECHA) ) as jul, ".
            " (SELECT SUM(MIN_MONTO) from sie_ministraciones WHERE cpe_id_periodo= ".$periodo." and  MIN_destino='GB' and EXTRACT(month FROM MIN_FECHA)=8 group by EXTRACT(month FROM MIN_FECHA) ) as ago, ".
            " (SELECT SUM(MIN_MONTO) from sie_ministraciones WHERE cpe_id_periodo= ".$periodo." and  MIN_destino='GB' and EXTRACT(month FROM MIN_FECHA)=9 group by EXTRACT(month FROM MIN_FECHA) ) as sep, ".
            " (SELECT SUM(MIN_MONTO) from sie_ministraciones WHERE cpe_id_periodo= ".$periodo." and  MIN_destino='GB' and EXTRACT(month FROM MIN_FECHA)=10 group by EXTRACT(month FROM MIN_FECHA) ) as oct, ".
            " (SELECT SUM(MIN_MONTO) from sie_ministraciones WHERE cpe_id_periodo= ".$periodo." and  MIN_destino='GB' and EXTRACT(month FROM MIN_FECHA)=11 group by EXTRACT(month FROM MIN_FECHA) ) as nov, ".
            " (SELECT SUM(MIN_MONTO) from sie_ministraciones WHERE cpe_id_periodo= ".$periodo." and  MIN_destino='GB' and EXTRACT(month FROM MIN_FECHA)=12 group by EXTRACT(month FROM MIN_FECHA) ) as dic ".
            " FROM sie_ministraciones  WHERE cpe_id_periodo= ".$periodo." and  MIN_destino='GB' group by MIN_destino " .
            " UNION " .
            " SELECT (CAST('Gastado' AS CHAR(255)))  as cpr_descripcion, (CAST(3 AS CHAR(20)))  as cpr_numeroproyecto, " .
            " (SELECT sum(ch.che_monto) FROM  sie_cheques ch WHERE ch.che_estatus<>'CA' AND ch.che_estatusborrado=1 and cpe_id_periodo= ".$periodo." AND che_destino='GB' and EXTRACT(month FROM che_fecha_emision)=1 group by EXTRACT(month FROM che_fecha_emision) ) as ene,  ".
            " (SELECT sum(ch.che_monto) FROM  sie_cheques ch WHERE ch.che_estatus<>'CA' AND ch.che_estatusborrado=1 and cpe_id_periodo= ".$periodo." AND che_destino='GB' and EXTRACT(month FROM che_fecha_emision)=2 group by EXTRACT(month FROM che_fecha_emision) ) as feb,  ".
            " (SELECT sum(ch.che_monto) FROM  sie_cheques ch WHERE ch.che_estatus<>'CA' AND ch.che_estatusborrado=1 and cpe_id_periodo= ".$periodo." AND che_destino='GB' and EXTRACT(month FROM che_fecha_emision)=3 group by EXTRACT(month FROM che_fecha_emision) ) as mar,  ".
            " (SELECT sum(ch.che_monto) FROM  sie_cheques ch WHERE ch.che_estatus<>'CA' AND ch.che_estatusborrado=1 and cpe_id_periodo= ".$periodo." AND che_destino='GB' and EXTRACT(month FROM che_fecha_emision)=4 group by EXTRACT(month FROM che_fecha_emision) ) as abr,  ".
            " (SELECT sum(ch.che_monto) FROM  sie_cheques ch WHERE ch.che_estatus<>'CA' AND ch.che_estatusborrado=1 and cpe_id_periodo= ".$periodo." AND che_destino='GB' and EXTRACT(month FROM che_fecha_emision)=5 group by EXTRACT(month FROM che_fecha_emision) ) as may,  ".
            " (SELECT sum(ch.che_monto) FROM  sie_cheques ch WHERE ch.che_estatus<>'CA' AND ch.che_estatusborrado=1 and cpe_id_periodo= ".$periodo." AND che_destino='GB' and EXTRACT(month FROM che_fecha_emision)=6 group by EXTRACT(month FROM che_fecha_emision) ) as jun,  ".
            " (SELECT sum(ch.che_monto) FROM  sie_cheques ch WHERE ch.che_estatus<>'CA' AND ch.che_estatusborrado=1 and cpe_id_periodo= ".$periodo." AND che_destino='GB' and EXTRACT(month FROM che_fecha_emision)=7 group by EXTRACT(month FROM che_fecha_emision) ) as jul,  ".
            " (SELECT sum(ch.che_monto) FROM  sie_cheques ch WHERE ch.che_estatus<>'CA' AND ch.che_estatusborrado=1 and cpe_id_periodo= ".$periodo." AND che_destino='GB' and EXTRACT(month FROM che_fecha_emision)=8 group by EXTRACT(month FROM che_fecha_emision) ) as ago,  ".
            " (SELECT sum(ch.che_monto) FROM  sie_cheques ch WHERE ch.che_estatus<>'CA' AND ch.che_estatusborrado=1 and cpe_id_periodo= ".$periodo." AND che_destino='GB' and EXTRACT(month FROM che_fecha_emision)=9 group by EXTRACT(month FROM che_fecha_emision) ) as sep,  ".
            " (SELECT sum(ch.che_monto) FROM  sie_cheques ch WHERE ch.che_estatus<>'CA' AND ch.che_estatusborrado=1 and cpe_id_periodo= ".$periodo." AND che_destino='GB' and EXTRACT(month FROM che_fecha_emision)=10 group by EXTRACT(month FROM che_fecha_emision) ) as oct,  ".
            " (SELECT sum(ch.che_monto) FROM  sie_cheques ch WHERE ch.che_estatus<>'CA' AND ch.che_estatusborrado=1 and cpe_id_periodo= ".$periodo." AND che_destino='GB' and EXTRACT(month FROM che_fecha_emision)=11 group by EXTRACT(month FROM che_fecha_emision) ) as nov,  ".
            " (SELECT sum(ch.che_monto) FROM  sie_cheques ch WHERE ch.che_estatus<>'CA' AND ch.che_estatusborrado=1 and cpe_id_periodo= ".$periodo." AND che_destino='GB' and EXTRACT(month FROM che_fecha_emision)=12 group by EXTRACT(month FROM che_fecha_emision) ) as dic  ".
            " FROM sie_cheques   WHERE cpe_id_periodo= ".$periodo." and  che_destino='GB' group by che_destino " .
            " UNION " .
            "  SELECT (CAST('Comprobado INAH' AS CHAR(255)))  as cpr_descripcion, (CAST(4 AS CHAR(20)))  as cpr_numeroproyecto, " .
            " (SELECT  sum(d.dci_monto) FROM sie_comprobaciones_inah c, sie_det_comprobaciones_inah d, sie_ministraciones m WHERE  m.min_id_ministracion = c.min_id_ministracion AND c.cin_id_cinah = d.cin_id_cinah AND c.cin_estatus = 1  AND d.dci_estatus = 1 AND m.min_formacarga = 1  AND c.cpe_id_periodo= ".$periodo." and m.min_destino='GB' and EXTRACT(month FROM CIN_FECHA)=1 group by EXTRACT(month FROM CIN_FECHA) ) as ene,  " .
            " (SELECT  sum(d.dci_monto) FROM sie_comprobaciones_inah c, sie_det_comprobaciones_inah d, sie_ministraciones m WHERE  m.min_id_ministracion = c.min_id_ministracion AND c.cin_id_cinah = d.cin_id_cinah AND c.cin_estatus = 1  AND d.dci_estatus = 1 AND m.min_formacarga = 1  AND c.cpe_id_periodo= ".$periodo." and m.min_destino='GB' and EXTRACT(month FROM CIN_FECHA)=2 group by EXTRACT(month FROM CIN_FECHA) ) as feb,  " .
            " (SELECT  sum(d.dci_monto) FROM sie_comprobaciones_inah c, sie_det_comprobaciones_inah d, sie_ministraciones m WHERE  m.min_id_ministracion = c.min_id_ministracion AND c.cin_id_cinah = d.cin_id_cinah AND c.cin_estatus = 1  AND d.dci_estatus = 1 AND m.min_formacarga = 1  AND c.cpe_id_periodo= ".$periodo." and m.min_destino='GB' and EXTRACT(month FROM CIN_FECHA)=3 group by EXTRACT(month FROM CIN_FECHA) ) as mar,  " .
            " (SELECT  sum(d.dci_monto) FROM sie_comprobaciones_inah c, sie_det_comprobaciones_inah d, sie_ministraciones m WHERE  m.min_id_ministracion = c.min_id_ministracion AND c.cin_id_cinah = d.cin_id_cinah AND c.cin_estatus = 1  AND d.dci_estatus = 1 AND m.min_formacarga = 1  AND c.cpe_id_periodo= ".$periodo." and m.min_destino='GB' and EXTRACT(month FROM CIN_FECHA)=4 group by EXTRACT(month FROM CIN_FECHA) ) as abr,  " .
            " (SELECT  sum(d.dci_monto) FROM sie_comprobaciones_inah c, sie_det_comprobaciones_inah d, sie_ministraciones m WHERE  m.min_id_ministracion = c.min_id_ministracion AND c.cin_id_cinah = d.cin_id_cinah AND c.cin_estatus = 1  AND d.dci_estatus = 1 AND m.min_formacarga = 1  AND c.cpe_id_periodo= ".$periodo." and m.min_destino='GB' and EXTRACT(month FROM CIN_FECHA)=5 group by EXTRACT(month FROM CIN_FECHA) ) as may,  " .
            " (SELECT  sum(d.dci_monto) FROM sie_comprobaciones_inah c, sie_det_comprobaciones_inah d, sie_ministraciones m WHERE  m.min_id_ministracion = c.min_id_ministracion AND c.cin_id_cinah = d.cin_id_cinah AND c.cin_estatus = 1  AND d.dci_estatus = 1 AND m.min_formacarga = 1  AND c.cpe_id_periodo= ".$periodo." and m.min_destino='GB' and EXTRACT(month FROM CIN_FECHA)=6 group by EXTRACT(month FROM CIN_FECHA) ) as jun,  " .
            " (SELECT  sum(d.dci_monto) FROM sie_comprobaciones_inah c, sie_det_comprobaciones_inah d, sie_ministraciones m WHERE  m.min_id_ministracion = c.min_id_ministracion AND c.cin_id_cinah = d.cin_id_cinah AND c.cin_estatus = 1  AND d.dci_estatus = 1 AND m.min_formacarga = 1  AND c.cpe_id_periodo= ".$periodo." and m.min_destino='GB' and EXTRACT(month FROM CIN_FECHA)=7 group by EXTRACT(month FROM CIN_FECHA) ) as jul,  " .
            " (SELECT  sum(d.dci_monto) FROM sie_comprobaciones_inah c, sie_det_comprobaciones_inah d, sie_ministraciones m WHERE  m.min_id_ministracion = c.min_id_ministracion AND c.cin_id_cinah = d.cin_id_cinah AND c.cin_estatus = 1  AND d.dci_estatus = 1 AND m.min_formacarga = 1  AND c.cpe_id_periodo= ".$periodo." and m.min_destino='GB' and EXTRACT(month FROM CIN_FECHA)=8 group by EXTRACT(month FROM CIN_FECHA) ) as ago,  " .
            " (SELECT  sum(d.dci_monto) FROM sie_comprobaciones_inah c, sie_det_comprobaciones_inah d, sie_ministraciones m WHERE  m.min_id_ministracion = c.min_id_ministracion AND c.cin_id_cinah = d.cin_id_cinah AND c.cin_estatus = 1  AND d.dci_estatus = 1 AND m.min_formacarga = 1  AND c.cpe_id_periodo= ".$periodo." and m.min_destino='GB' and EXTRACT(month FROM CIN_FECHA)=9 group by EXTRACT(month FROM CIN_FECHA) ) as sep,  " .
            " (SELECT  sum(d.dci_monto) FROM sie_comprobaciones_inah c, sie_det_comprobaciones_inah d, sie_ministraciones m WHERE  m.min_id_ministracion = c.min_id_ministracion AND c.cin_id_cinah = d.cin_id_cinah AND c.cin_estatus = 1  AND d.dci_estatus = 1 AND m.min_formacarga = 1  AND c.cpe_id_periodo= ".$periodo." and m.min_destino='GB' and EXTRACT(month FROM CIN_FECHA)=10 group by EXTRACT(month FROM CIN_FECHA) ) as oct,  " .
            " (SELECT  sum(d.dci_monto) FROM sie_comprobaciones_inah c, sie_det_comprobaciones_inah d, sie_ministraciones m WHERE  m.min_id_ministracion = c.min_id_ministracion AND c.cin_id_cinah = d.cin_id_cinah AND c.cin_estatus = 1  AND d.dci_estatus = 1 AND m.min_formacarga = 1  AND c.cpe_id_periodo= ".$periodo." and m.min_destino='GB' and EXTRACT(month FROM CIN_FECHA)=11 group by EXTRACT(month FROM CIN_FECHA) ) as nov,  " .
            " (SELECT  sum(d.dci_monto) FROM sie_comprobaciones_inah c, sie_det_comprobaciones_inah d, sie_ministraciones m WHERE  m.min_id_ministracion = c.min_id_ministracion AND c.cin_id_cinah = d.cin_id_cinah AND c.cin_estatus = 1  AND d.dci_estatus = 1 AND m.min_formacarga = 1  AND c.cpe_id_periodo= ".$periodo." and m.min_destino='GB' and EXTRACT(month FROM CIN_FECHA)=12 group by EXTRACT(month FROM CIN_FECHA) ) as dic  " .
            " FROM sie_comprobaciones_inah " .
            " )AS tabla_1 ORDER BY cpr_numeroproyecto";

	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= 0;
            $numProyecto= $rs["cpr_numeroproyecto"];
            $proyecto= $rs["cpr_descripcion"];
            $autorizado=0.0;
            $ene=$rs["ene"];
            $feb=$rs["feb"];
            $mar=$rs["mar"];
            $abr=$rs["abr"];
            $may=$rs["may"];
            $jun=$rs["jun"];
            $jul=$rs["jul"];
            $ago=$rs["ago"];
            $sep=$rs["sep"];
            $oct=$rs["oct"];
            $nov=$rs["nov"];
            $dic=$rs["dic"];
            $comprobado = 0.0;
            
            $elemento = new ConsultaResumen();
            $elemento->resumenProyecto($id,$numProyecto,$proyecto,$autorizado,0.0,0.0,0.0,0.0,0.0,$comprobado,0.0,$ene,$feb,$mar,$abr,$may,$jun,$jul,$ago,$sep,$oct,$nov,$dic);

            array_push($lista, $elemento);
        }

        return $lista;
    }
}
