<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/TarjetaCalendario.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class TarjetaCalendarioDaoJdbc {
    
    public function obtieneListado($id_tarjeta) {
		
	$lista= array();
	$query="SELECT c.trdc_id_calendario, c.trdc_ene, c.trdc_feb, c.trdc_mar, c.trdc_abr, c.trdc_may, c.trdc_jun," .
            "c.trdc_jul, c.trdc_ago, c.trdc_sep, c.trdc_oct, c.trdc_nov, c.trdc_dic,m.proyecto_id_meta, m.proyecto_desc_meta," .
            " p.cpa_partida, p.cpa_descripcion " .
            "FROM sie_tarjeta_reg_det_cal c, sie_proyecto_metas m,  sie_cat_partidas p " .
            "WHERE  c.proyecto_id_meta=m.proyecto_id_meta AND c.cpa_id_partida = p.cpa_id_partida AND c.trdc_estatus = 1 AND " .
            "c.tre_id_tarjeta = ". (int)($id_tarjeta) ." ";
		
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $idCalendario= $rs["trdc_id_calendario"];
            $Partida = $rs["cpa_partida"];
            $descPartida = $rs["cpa_descripcion"];
            $enero = $rs["trdc_ene"];
            $feb = $rs["trdc_feb"];
            $mar = $rs["trdc_mar"];
            $abr = $rs["trdc_abr"];
            $may = $rs["trdc_may"];
            $jun = $rs["trdc_jun"];
            $jul = $rs["trdc_jul"];
            $agost = $rs["trdc_ago"];
            $sep = $rs["trdc_sep"];
            $oct = $rs["trdc_oct"];
            $nov = $rs["trdc_nov"];
            $dic = $rs["trdc_dic"];
            $idMeta=$rs["proyecto_id_meta"];
            $descMeta = $rs["proyecto_desc_meta"];

            $elemento = new TarjetaCalendario();
            $elemento->constructor3($idCalendario, $Partida, $descPartida, $enero,  $feb,  $mar,  $abr,  $may, $jun, $jul, $agost,$sep, $oct, $nov,  $dic, $idMeta,$descMeta);
            array_push($lista, $elemento);
        }	
        return $lista;
    }
    
    public function obtieneElemento($idElemento) {
		
	$elemento = null;
	$query=strtolower("SELECT t.*, m.proyecto_desc_meta FROM sie_tarjeta_reg_det_cal t, SIE_PROYECTO_METAS m " .
            "WHERE t.PROYECTO_ID_META= m.PROYECTO_ID_META AND trdc_estatus = 1 AND trdc_id_calendario = ".$idElemento);

	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
				
            $idCalendario= $rs[strtoupper("trdc_id_calendario")];
            $idTarjeta= $rs[strtoupper("tre_id_tarjeta")];
            $Partida = $rs[strtoupper("cpa_id_partida")];
            $enero = $rs[strtoupper("trdc_ene")];
            $feb = $rs[strtoupper("trdc_feb")];
            $mar = $rs[strtoupper("trdc_mar")];
            $abr = $rs[strtoupper("trdc_abr")];
            $may = $rs[strtoupper("trdc_may")];
            $jun = $rs[strtoupper("trdc_jun")];
            $jul = $rs[strtoupper("trdc_jul")];
            $ags = $rs[strtoupper("trdc_ago")];
            $sep = $rs[strtoupper("trdc_sep")];
            $oct = $rs[strtoupper("trdc_oct")];
            $nov = $rs[strtoupper("trdc_nov")];
            $dic = $rs[strtoupper("trdc_dic")];
            $idMeta=$rs[strtoupper("PROYECTO_ID_META")];
            $descMeta = $rs["proyecto_desc_meta"];

            $elemento = new TarjetaCalendario();
            $elemento->constructor2($idCalendario,$idTarjeta,$Partida,$enero,$feb,$mar,$abr,$may, $jun,$jul,$ags,$sep,$oct,$nov,$dic,$idMeta,$descMeta);

        }
		
        return $elemento;
		
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $query="INSERT INTO sie_tarjeta_reg_det_cal(tre_id_tarjeta,cpa_id_partida,trdc_ene,trdc_feb,trdc_mar,trdc_abr,trdc_may,trdc_jun,trdc_jul,trdc_ago,trdc_sep,trdc_oct,trdc_nov,trdc_dic,trdc_estatus,proyecto_id_meta) VALUES (".$elemento->getIdTarjeta().", ". $elemento->getNum_Partida() .", ".  $elemento->getEnero() .",".
		$elemento->getFebrero()." ,". $elemento->getMarzo() .",". $elemento->getAbril() .",". $elemento->getMayo().",". $elemento->getJunio()." ,". $elemento->getJulio() .",".$elemento->getAgosto() .",".
		$elemento->getSeptiembre()." , ".$elemento->getOctubre().", ".$elemento->getNoviembre().", ".$elemento->getDiciembre().", 1, ".$elemento->getIdMeta()." )";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
	$query="UPDATE sie_tarjeta_reg_det_cal SET cpa_id_partida=". $elemento->getNum_Partida() .", trdc_ene= ".  $elemento->getEnero() .", trdc_feb = ".$elemento->getFebrero()." , trdc_mar=". $elemento->getMarzo() .", trdc_abr=". $elemento->getAbril() .",trdc_may=". $elemento->getMayo().",trdc_jun=". $elemento->getJunio()." ,trdc_jul=". $elemento->getJulio() .",trdc_ago=".$elemento->getAgosto() .",trdc_sep= " .$elemento->getSeptiembre()." , trdc_oct= ".$elemento->getOctubre().", trdc_nov=".$elemento->getNoviembre().",trdc_dic=".$elemento->getDiciembre()." ,PROYECTO_ID_META=".$elemento->getIdMeta()." WHERE trdc_id_calendario=".$elemento->getIdCalendario()." ";
	$res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_tarjeta_reg_det_cal set  trdc_estatus = 0 WHERE trdc_id_calendario = ".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}
