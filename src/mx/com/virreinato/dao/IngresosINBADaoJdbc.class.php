<?php
	session_start();

	include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
	include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/IngresosINBA.class.php");
	include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");
	class IngresosINBADaoJdbc{
		public function obtieneLista(){
			$catalogo = new Catalogo();
			$lista = array();
			$query = "SELECT * FROM sie_ingresos_INBA ORDER BY idIngreso";
			$result = $catalogo->obtenerLista($query);
			while($rs = mysql_fetch_array($result)){
				$elemento = new IngresosINBA();
				$elemento->setAll($rs[0], $rs[1], $rs[2], $rs[3], $rs[4], $rs[5],$rs[6]);	
				array_push($lista, $elemento);
			}
			return $lista;
		}
		
		public function obtieneElemento($id){
			$catalogo = new Catalogo();
			$elemento = new IngresosINBA();
			$query = "SELECT * FROM sie_ingresos_INBA WHERE idIngreso = " . $id;
			$result = $catalogo->obtenerLista($query);
			if($rs = mysql_fetch_array($result)){
				$elemento->setAll($rs[0], $rs[1], $rs[2], $rs[3], $rs[4], $rs[5],$rs[6]);
			}
			return $elemento;
		}
		
		public function guardaElemento($elemento){
			$catalogo = new Catalogo();
			$expot = ""; $hayexpot = "";
			$archivo = ""; $hayArchivo = "";
			if($elemento->getIdProyecto() == 48){
				$hayexpot = "idExpoTemp, ";
				$expot = ", " . $elemento->getIdExpoTemp();	
			}
			if(!is_null($elemento->getArchivo())){
				$hayArchivo = ", archivo";
				$archivo = ", '" . $elemento->getArchivo() . "'";
			}
 			//$insert = "INSERT INTO sie_ingresos_INBA (idProyecto,".$hayexpot.",fecha, monto" . $hayArchivo .",periodo) VALUES ( ". $elemento->getIdProyecto() .",". $expot . ", curdate(), " . $elemento->getMonto()."," . $archivo .",41);";
			$insert = "INSERT INTO sie_ingresos_INBA (idProyecto, " . $hayexpot . "fecha, monto" . $hayArchivo .",periodo) VALUES (" . $elemento->getIdProyecto() . $expot . ", curdate(), " . $elemento->getMonto() . $archivo .",".$elemento->getPeriodo().");";
			$res = $catalogo->obtenerLista($insert);
			
			$update=" update sie_cat_linea_accion set inba=(select sum(monto) from sie_ingresos_INBA where idproyecto=".$elemento->getIdProyecto().")  where cla_id_lineaaccion=".$elemento->getIdProyecto();
			$res2 = $catalogo->obtenerLista($update);
			
			if($res == "1"){
				$update = "UPDATE sie_cat_linea_accion SET saldoInicial = saldoInicial + " . $elemento->getMonto() . ", saldoAnterior = saldoAnterior + " . $elemento->getMonto() . " WHERE cla_id_lineaaccion = " . $elemento->getIdProyecto();
				$res = $catalogo->obtenerLista($update);
				$update = "UPDATE sie_cat_expo_temp SET saldo = saldo + " . $elemento->getMonto() . ", saldoAnterior = saldoAnterior + " . $elemento->getMonto() . ", inba = inba + ". $elemento->getMonto()."  WHERE id_linea_accion = " . $elemento->getIdProyecto() . " AND id_Subproyecto = " . $elemento->getIdExpoTemp();
				$catalogo->obtenerLista($update);
				return true;
			}else{
				return false;	
			}			
		}
		
		public function eliminaElemento($elemento){
			$catalogo = new Catalogo();
			$delete = "DELETE FROM sie_ingresos_INBA WHERE idIngreso = " . $elemento->getId();
			$res = $catalogo->obtenerLista($delete);
			if($res == "1"){
				$update = "UPDATE sie_cat_linea_accion SET saldoInicial = saldoInicial - " . $elemento->getMonto() . ", saldoAnterior = saldoAnterior - " . $elemento->getMonto() . ", inba = inba - ". $elemento->getMonto(). "  WHERE cla_id_lineaaccion = " . $elemento->getIdProyecto();
				$catalogo->obtenerLista($update);
				if($elemento->getIdProyecto() == 48){
					$update = "UPDATE sie_cat_expo_temp SET saldo = saldo - " . $elemento->getMonto() . ", saldoAnterior = saldoAnterior - " . $elemento->getMonto() . ", inba = inba - ". $elemento->getMonto(). "  WHERE id_Subproyecto = " . $elemento->getIdExpoTemp();	
					$catalogo->obtenerLista($update);
				}
				
				return true;	
			}
			else return false;
		}
		
		public function modificaElemento($elemento){//Solo se modifica el nombre del Patrocinador, el monto y el nombre del archivo
			/***********************/
			$ip = new IngresosINBA();
			$ip = $this->obtieneElemento($elemento->getId());
			$nuevoMonto = $elemento->getMonto() - $ip->getMonto();
			/***********************/
			$catalogo = new Catalogo();
			$archivo = "";
			//Checamos lo del archivo.
			if(!is_null($elemento->getArchivo())){				
				$archivo = ", archivo = '" . $elemento->getArchivo() . "'";
			}
			$update = "UPDATE sie_ingresos_INBA SET monto = " . $elemento->getMonto() . $archivo . " WHERE idIngreso = " . $elemento->getId();
			$res = $catalogo->obtenerLista($update);
			if($res == "1"){
				$update = "UPDATE sie_cat_linea_accion SET saldoInicial = saldoInicial + " . $nuevoMonto . ", saldoAnterior = saldoAnterior + " . $nuevoMonto . ", inba = inba + ". $nuevoMonto. " WHERE cla_id_lineaaccion = " . $ip->getIdProyecto();
				$catalogo->obtenerLista($update);
				if($ip->getIdProyecto() == 48){
					$update = "UPDATE sie_cat_expo_temp SET saldo = saldo + " . $nuevoMonto . ", saldoAnterior = saldoAnterior + " . $nuevoMonto . ", inba = inba + ". $nuevoMonto ." WHERE id_Subproyecto = " . $ip->getIdExpoTemp();
					$catalogo->obtenerLista($update);
				}
				return true;
			}else{
				return false;
			}
		}
		
		public function obtieneMontoProy($idProy, $periodo)
		{
			$catalogo = new Catalogo();
			$select = "SELECT SUM(i.monto) FROM sie_ingresos_INBA i, sie_cat_periodos p WHERE i.idProyecto = " . $idProy . " AND p.CPE_ID_PERIODO = " . $periodo . " AND i.fecha BETWEEN p.CPE_FECHA_INI AND p.CPE_FECHA_FIN";
			$res = $catalogo->obtenerLista($select);
			
			return false;
		}
		
		public function obtieneMontoExpos($idET, $periodo)
		{
			$catalogo = new Catalogo();
			$select = "SELECT SUM(i.monto) FROM sie_ingresos_INBA i, sie_cat_periodos p WHERE i.idExpoTemp = " . $idET . " AND p.CPE_ID_PERIODO = " . $periodo . " AND i.fecha BETWEEN p.CPE_FECHA_INI AND p.CPE_FECHA_FIN";
			$res = $catalogo->obtenerLista($select);
			
			return false;
		}
		
	}
?>