<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/FondosDet.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class FondosDetDaoJdbc {
   
    public function obtieneListado($idFondo, $mes) {
		
        $lista= array();

        $query="SELECT f.sfo_id_solfondos, par.cpa_id_partida, par.cpa_partida, par.cpa_descripcion, " .
            " SUM(cal.".$mes.") as mes " .
            " FROM sie_solicitud_fondos f, sie_tarjeta_registro tr, sie_tarjeta_reg_det_cal cal, " .
            " sie_cat_partidas par " .
            " WHERE  f.cpr_id_proyecto = tr.cpr_id_proyecto AND  " .
            " tr.tre_id_tarjeta = cal.tre_id_tarjeta AND cal.cpa_id_partida = par.cpa_id_partida AND" .
            " f.sfo_id_solfondos =".$idFondo." AND f.SFO_ESTATUS=1  " .
            " AND  tr.tre_estatus = 1 AND cal.trdc_estatus = 1  " .
            "GROUP BY f.sfo_id_solfondos, par.cpa_id_partida, " .
            " par.cpa_partida, par.cpa_descripcion " .
            "ORDER BY(par.cpa_id_partida)";

	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $idSolFondo = $rs["sfo_id_solfondos"];
            $idPartida = $rs["cpa_id_partida"];
            $desPartida = $rs["cpa_partida"]." ".$rs["cpa_descripcion"];
            $monto = $rs["mes"];
				
                if( $monto != 0  ){

                    $elemento = new FondosDet();
                    $elemento->setAll($idSolFondo,$idPartida,$desPartida,$monto);
                    array_push($lista, $elemento);	
                }
								
				
            }	
        return $lista;
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="DELETE FROM sie_det_sol_fondos WHERE dsf_id_detsolfondos=".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
    }
    
}
