<?php
if(session_id() == '') {
    session_start();
}

include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CartaCronograma.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class CartaCronogramaDaoJdbc {
    
    public function obtieneListado($id_carta) {
		
	$lista= array();
	$query="SELECT cco_id_carta, ccd_id_cronograma , ccd_actividadplan, ccd_fechainicio, ccd_fechafin, ccd_fase FROM sie_carta_cons_det_cronograma WHERE ccd_estatus = 1 AND  cco_id_carta = ".(int)($id_carta) ." order by(ccd_fechainicio) ";
		
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $idCarta= $rs["cco_id_carta"];
            $idCronograma = $rs["ccd_id_cronograma"];
            $descripcion = $rs["ccd_actividadplan"];
            $fechaInicio = $rs["ccd_fechainicio"];
            $fechaFin = $rs["ccd_fechafin"];
            $fase = $rs["ccd_fase"];

            $elemento = new CartaCronograma();
            $elemento->cartaCrono($idCronograma,$idCarta,$descripcion,$fechaInicio, $fechaFin, $fase);
            array_push($lista, $elemento);
        }	

        return $lista;
    }
    
    public function Imprimir($id_carta, $inicio, $fin){
        
	$lista= array();
	$query="SELECT cco_id_carta, ccd_id_cronograma , ccd_actividadplan, ccd_fechainicio, ccd_fechafin, ccd_fase FROM sie_carta_cons_det_cronograma WHERE ccd_estatus = 1 AND  cco_id_carta = ".(int)($id_carta)." AND ccd_fechainicio BETWEEN '01-07-2011' AND '30-06-2013' AND ccd_fechafin BETWEEN '".$inicio."-07-01' AND '".$fin."-06-30'  order by(ccd_fechainicio) ";
		
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $idCarta= $rs["cco_id_carta"];
            $idCronograma = $rs["ccd_id_cronograma"];
            $descripcion = $rs["ccd_actividadplan"];
            $fechaInicio = $rs["ccd_fechainicio"];
            $fechaFin = $rs["ccd_fechafin"];
            $fase = $rs["ccd_fase"];

            $elemento = new CartaCronograma();
            $elemento->cartaCrono($idCronograma,$idCarta,$descripcion,$fechaInicio, $fechaFin, $fase);
            array_push($lista, $elemento);
        }	

        return $lista;
    }
    
    public function obtieneElemento($idElemento) {
		
	$elemento = new CartaCronograma();
	$query="SELECT * FROM sie_carta_cons_det_cronograma WHERE  ccd_estatus = 1 AND ccd_id_cronograma = ".$idElemento;
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $idCarta= $rs[strtoupper("cco_id_carta")];
            $idCronograma = $rs[strtoupper("ccd_id_cronograma")];
            $orden = $rs[strtoupper("ccd_ordenactividadplan")];
            $actividad = $rs[strtoupper("ccd_actividadplan")];
            $inicio  = $rs[strtoupper("ccd_fechainicio")];
            $fin = $rs[strtoupper("ccd_fechafin")];
            $fase = $rs[strtoupper("ccd_fase")];

            $elemento->setAll($idCronograma,$idCarta,$orden,$actividad,$inicio,$fin,$fase);
        }
        return $elemento;
    }
    
    public function guardaElemento($elemento) {
		
        $fechainicio="";
        $fechafin = "";
        
        if($elemento->getFechaFin()!=null)
        {   $fechafin= "'".date("Y-m-d",strtotime($elemento->getFechaFin()))."'";  }
		
        if($elemento->getFechaInicio()!=null)
        {    $fechainicio= "'".date("Y-m-d",strtotime($elemento->getFechaInicio()))."'"; }
        
        $con=new Catalogo();
        $query="INSERT INTO sie_carta_cons_det_cronograma( CCO_ID_CARTA,CCD_ORDENACTIVIDADPLAN,CCD_ACTIVIDADPLAN,CCD_FECHAINICIO,CCD_FECHAFIN,CCD_ESTATUS,CCD_FASE) VALUES (".$elemento->getIdCarta().", ".$elemento->getOrden().", '". mb_strtoupper($elemento->getActividad(),'UTF-8') ."',".
            $fechainicio.", ".$fechafin.",1, '".$elemento->getFase()."')";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
        $fechainicio="";
        $fechafin = "";
        
        if($elemento->getFechaFin()!=null)
        {   $fechafin= "'".date("Y-m-d",strtotime($elemento->getFechaFin()))."'";  }
		
        if($elemento->getFechaInicio()!=null)
        {    $fechainicio= "'".date("Y-m-d",strtotime($elemento->getFechaInicio()))."'"; }
        
	$con=new Catalogo();
	$query="UPDATE sie_carta_cons_det_cronograma  SET  ccd_ordenactividadplan = ".$elemento->getOrden().", ccd_actividadplan = '".mb_strtoupper($elemento->getActividad(),'UTF-8')."', ccd_fechainicio=".$fechainicio." , ccd_fechafin =".$fechafin.", ccd_fase = '".$elemento->getFase()."' WHERE ccd_id_cronograma = ". $elemento->getId() ."";
        $res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
     public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_carta_cons_det_cronograma set  ccd_estatus = 0 WHERE ccd_id_cronograma = ".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}
