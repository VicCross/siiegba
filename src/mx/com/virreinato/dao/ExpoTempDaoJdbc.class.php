<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatExpoTemp.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class ExpoTempDaoJdbc {
    
    public function obtieneListado() {
		
        $lista= array();

        $query="SELECT * FROM sie_cat_expo_temp WHERE tipo=1 order by nombre ";
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
			 $id= $rs[strtoupper("id_Subproyecto")];
            $nombre= $rs[strtoupper("nombre")];
            $inicio= $rs[strtoupper("fecha_inicio")];
            $fin= $rs[strtoupper("fecha_fin")];
            $titulo= $rs[strtoupper("titulo_final")];
            $tipo= $rs[strtoupper("tipo")];
            $periodo= $rs[strtoupper("periodo")];
            $area= $rs[strtoupper("area_coordina")];
            $accion= $rs[strtoupper("id_linea_accion")];
			$saldo= $rs[strtoupper("saldo")];
			
            $elemento = new ExpoTemp();
            $elemento->setAll($id,$nombre,$inicio,$fin,$titulo,$tipo,$periodo,$area,$accion,$saldo);
            array_push($lista, $elemento);
        }	

        return $lista;
    }
    
    public function obtieneListadoCatalogos(){
        
        $lista= array();

        $query="SELECT * FROM sie_cat_expo_temp ORDER BY nombre";
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
           $id= $rs[strtoupper("id_Subproyecto")];
            $nombre= $rs[strtoupper("nombre")];
            $inicio= $rs[strtoupper("fecha_inicio")];
            $fin= $rs[strtoupper("fecha_fin")];
            $titulo= $rs[strtoupper("titulo_final")];
            $tipo= $rs[strtoupper("tipo")];
            $periodo= $rs[strtoupper("periodo")];
            $area= $rs[strtoupper("area_coordina")];
            $accion= $rs[strtoupper("id_linea_accion")];
			$saldo= $rs[strtoupper("saldo")];

            $elemento = new ExpoTemp();
              $elemento->setAll($id,$nombre,$inicio,$fin,$titulo,$tipo,$periodo,$area,$accion,$saldo);
            array_push($lista, $elemento);
        }	

        return $lista;
    }
    
    public function obtieneElemento($idElemento) {
		
	$elemento=new Proveedor();
		
	$query="SELECT * FROM sie_cat_expo_temp WHERE id_Subproyecto=".$idElemento;
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("id_Subproyecto")];
            $nombre= $rs[strtoupper("nombre")];
            $inicio= $rs[strtoupper("fecha_inicio")];
            $fin= $rs[strtoupper("fecha_fin")];
            $titulo= $rs[strtoupper("titulo_final")];
            $tipo= $rs[strtoupper("tipo")];
            $periodo= $rs[strtoupper("periodo")];
            $area= $rs[strtoupper("area_coordina")];
            $accion= $rs[strtoupper("id_linea_accion")];
			$saldo= $rs[strtoupper("saldo")];

            $elemento = new ExpoTemp();
              $elemento->setAll($id,$nombre,$inicio,$fin,$titulo,$tipo,$periodo,$area,$accion,$saldo);
        }
		
	return $elemento;	
    }
    
    public function obtieneElementoDesc($desc) {
        
        $elemento=new ExpoTemp();
		
	$query="SELECT * FROM sie_cat_expo_temp WHERE nombre='".$desc."'";
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("id_Subproyecto")];
            $nombre= $rs[strtoupper("nombre")];
            $inicio= $rs[strtoupper("fecha_inicio")];
            $fin= $rs[strtoupper("fecha_fin")];
            $titulo= $rs[strtoupper("titulo_final")];
            $tipo= $rs[strtoupper("tipo")];
            $periodo= $rs[strtoupper("periodo")];
            $area= $rs[strtoupper("area_coordina")];
            $accion= $rs[strtoupper("id_linea_accion")];
			$saldo= $rs[strtoupper("saldo")];

            $elemento = new ExpoTemp();
            $elemento->setAll($id,$nombre,$inicio,$fin,$titulo,$tipo,$periodo,$area,$accion,$saldo);
        }
		
	return $elemento;
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $query="INSERT INTO sie_cat_expo_temp(nombre,fecha_inicio,fecha_fin_titulo_final_tipo,periodo,area_coordina,id_linea_accion,saldo) " .
	       "VALUES ('".mb_strtoupper($elemento->getNombre(),'UTF-8')."', '".mb_strtoupper($elemento->getInicio(),'UTF-8')."', '".mb_strtoupper($elemento->getFinal(),'UTF-8')."', ".mb_strtoupper($elemento->getTipo(),'UTF-8').",".mb_strtoupper($elemento->getPeriodo(),'UTF-8').", ".mb_strtoupper($elemento->getArea(),'UTF-8').", ".mb_strtoupper($elemento->getAccion(),'UTF-8').", ".mb_strtoupper($elemento->getSaldo(),'UTF-8').")";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
	$query="UPDATE sie_cat_expo_temp set nombre='".mb_strtoupper($elemento->getNombre(),'UTF-8')."', fecha_inicio='".mb_strtoupper($elemento->getInicio(),'UTF-8')."', fecha_fin='".mb_strtoupper($elemento->getFin(),'UTF-8')."', titulo_final=".mb_strtoupper($elemento->getTitulo(),'UTF-8').",tipo=".mb_strtoupper($elemento->getTipo(),'UTF-8').", periodo=".mb_strtoupper($elemento->getPeriodo(),'UTF-8').",area_coordina=".mb_strtoupper($elemento->getArea(),'UTF-8').",id_linea_accion=".$elemento->getAccion()." ,saldo=".$elemento->getSaldo()." " .
				"WHERE id_Subproyecto=".$elemento->getId();
	$res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_cat_expo_temp set  tipo=0 WHERE id_Subproyecto=".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}
