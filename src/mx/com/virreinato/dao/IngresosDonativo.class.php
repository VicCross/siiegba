<?php
	class IngresosDonativo
	{
		private $id_donativo;
		private $nom_patrocinador;
		private $tipo_donacion;
		private $area;
		private $monto;
		private $fecha;
		private $proyecto_destino;
		private $expo_temp;
		private $descripcion;
		private $documento;
		private $status;
		private $periodo;
		
		function setAll($id_donativo, $nom_patrocinador, $tipo_donacion, $area, $monto, $fecha, $proyecto_destino, $expo_temp, $descripcion, $documento, $status, $periodo)
		{
			$this->id_donativo = $id_donativo;
			$this->nom_patrocinador = $nom_patrocinador;
			$this->tipo_donacion = $tipo_donacion;
			$this->area = $area;
			$this->monto = $monto;
			$this->fecha = $fecha;
			$this->proyecto_destino = $proyecto_destino;
			$this->expo_temp = $expo_temp;
			$this->descripcion = $descripcion;
			$this->documento = $documento;
			$this->status = $status;
			$this->periodo = $periodo;
		}
		
		function __construct(){
			
		}
		
		function getId_donativo(){
			return $this->id_donativo;
		}
		
		function getNom_patrocinador(){
			return $this->nom_patrocinador;
		}
		
		function getTipo_donacion(){
			return $this->tipo_donacion;
		}
		
		function getArea(){
			return $this->area;
		}
		
		function getMonto(){
			return $this->monto;
		}
		
		function getFecha(){
			return $this->fecha;
		}
		
		function getProyecto_destino(){
			return $this->proyecto_destino;	
		}
		
		function getExpo_temp(){
			return $this->expo_temp;	
		}
		function getDescripcion(){
			return $this->descripcion;
		}
		
		function getDocumento(){
			return $this->documento;
		}
		
		function getStatus(){
			return $this->status;	
		}
		
		function getPeriodo(){
			return $this->periodo;	
		}
		
		
		
		
		function setId_donativo($id_donativo){
			$this->id_donativo = $id_donativo;
		}
		
		function setNom_patrocinador($nom_patrocinador){
			$this->nom_patrocinador = $nom_patrocinador;
		}
		
		function setTipo_donacion($tipo_donacion){
			$this->tipo_donacion = $tipo_donacion;
		}
		
		function setArea($area){
			$this->area = $area;
		}
		
		function setMonto($monto){
			$this->monto = $monto;
		}
		
		function setFecha($fecha){
			$this->fecha = $fecha;
		}
		
		function setProyecto_destino($proyecto_destino){
			$this->proyecto_destino = $proyecto_destino;	
		}
		function setExpo_temp($expo_temp){
			$this->expo_temp = $expo_temp;	
		}
		function setDescripcion($descripcion){
			$this->descripcion = $descripcion;	
		}
		function setDocumento($documento){
			$this->documento = $documento;	
		}
		function setStatus($status){
			$this->status = $status;	
		}
		function setPeriodo($periodo){
			$this->periodo = $periodo;	
		}
	
	}
	
?>