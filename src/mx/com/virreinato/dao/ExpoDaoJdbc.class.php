<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Expo.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class ExpoDaoJdbc {
    
    public function obtieneListado() {
		
	$lista= array();
		
	$query="SELECT * FROM sie_cat_temp WHERE ET_ESTATUS=1 ORDER BY et_nombre";
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("et_id_sub")];
			$nombre= $rs[strtoupper("et_nombre")];
			$inicio= $rs[strtoupper("et_fecha_inicio")];
			$fin= $rs[strtoupper("et_fecha_fin")];
			$titulo= $rs[strtoupper("et_titulo")];
			$tipo= $rs[strtoupper("et_tipo")];
			$periodo= $rs[strtoupper("et_periodo")];
			$area= $rs[strtoupper("et_area_coordina")];
			$accion= $rs[strtoupper("et_id_linea_accion")];
			$saldo= $rs[strtoupper("et_saldo")];
			$anterior= $rs[strtoupper("et_saldo_ant")];
			
            
            $elemento= new Eje();
            $elemento->setAll($id, $nombre, $inicio, $fin, $titulo, $tipo, $periodo, $area, $accion, $saldo,$anterior);
            array_push($lista, $elemento);
	}	
	return $lista;
    }
    
    public function obtieneElemento($idElemento){
        
        $elemento = new Eje();
        
        $query="SELECT * FROM sie_cat_temp WHERE et_id_sub=".$idElemento;
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
           $id= $rs[strtoupper("et_id_sub")];
		    $nombre= $rs[strtoupper("et_nombre")];
			$inicio= $rs[strtoupper("et_fecha_inicio")];
			$fin= $rs[strtoupper("et_fecha_fin")];
			$titulo= $rs[strtoupper("et_titulo")];
			$tipo= $rs[strtoupper("et_tipo")];
			$periodo= $rs[strtoupper("et_periodo")];
			$area= $rs[strtoupper("et_area_coordina")];
			$accion= $rs[strtoupper("et_id_linea_accion")];
			$saldo= $rs[strtoupper("et_saldo")];
			$anterior= $rs[strtoupper("et_saldo_ant")];

            $elemento= new Eje();
           $elemento->setAll($id,$nombre, $inicio, $fin, $titulo, $tipo, $periodo, $area, $accion, $saldo, $anterior);

	}	
	return $elemento;
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
		$query="INSERT INTO sie_cat_temp(et_nombre,et_fecha_inicio,et_fecha_fin,et_titulo,et_tipo,et_periodo,et_area_coordina,et_id_linea_accion,et_saldo,et_saldo_ant) " .
	       "VALUES ('".mb_strtoupper($elemento->getNombre(),'UTF-8')."', '".mb_strtoupper($elemento->getInicio(),'UTF-8')."', '".mb_strtoupper($elemento->getFin(),'UTF-8')."','".mb_strtoupper($elemento->getTitulo(),'UTF-8')."', ".mb_strtoupper($elemento->getTipo(),'UTF-8').",".mb_strtoupper($elemento->getPeriodo(),'UTF-8').", ".mb_strtoupper($elemento->getArea(),'UTF-8').", ".mb_strtoupper($elemento->getAccion(),'UTF-8').", ".mb_strtoupper($elemento->getSaldo(),'UTF-8').",  ".mb_strtoupper($elemento->getAnterior(),'UTF-8').")";
		  
        $res=$con->obtenerLista($query);
echo $query;
        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
	$query="UPDATE sie_cat_temp set et_nombre='".mb_strtoupper($elemento->getNombre(),'UTF-8')."', et_fecha_inicio=".mb_strtoupper($elemento->getInicio(),'UTF-8').", et_fecha_fin=".mb_strtoupper($elemento->getFin(),'UTF-8').", et_titulo=".mb_strtoupper($elemento->getTitulo(),'UTF-8').",et_tipo=".mb_strtoupper($elemento->getTipo(),'UTF-8').", et_periodo=".mb_strtoupper($elemento->getPeriodo(),'UTF-8').",et_area_coordina=".mb_strtoupper($elemento->getArea(),'UTF-8').",et_id_linea_accion=".$elemento->getAccion()." ,et_saldo=".$elemento->getSaldo().", et_saldo_ant=".mb_strtoupper($elemento->getAnterior(),'UTF-8')." " .
				"WHERE et_id_subproyecto=".$elemento->getId();
	$res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_cat_temp set  et_estatus=0 WHERE et_id_sub=".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}
