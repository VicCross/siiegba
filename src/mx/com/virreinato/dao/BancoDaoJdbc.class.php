<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Banco.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class BancoDaoJdbc {
    
    public function obtieneListado() {

        $lista= array();

        $query="SELECT * FROM sie_cat_bancos WHERE cba_estatus=1 ORDER BY cba_descripcion";
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cba_id_banco")];
            $descripcion= $rs[strtoupper("cba_descripcion")];
            $observaciones= $rs[strtoupper("cba_observaciones")];

            $elemento= new Banco();
            $elemento->setAll($id, $descripcion, $observaciones);
            array_push($lista,$elemento);
        }	
        return $lista;
    }
    
    public function obtieneElemento($idElemento) {
        
        $elemento= new Banco();

        $query="SELECT * FROM sie_cat_bancos WHERE cba_id_banco=".$idElemento;
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cba_id_banco")];
            $descripcion= $rs[strtoupper("cba_descripcion")];
            $observaciones= $rs[strtoupper("cba_observaciones")];

            $elemento= new Banco();
            $elemento->setAll($id, $descripcion, $observaciones);
        }	
        return $elemento;
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $query="INSERT INTO sie_cat_bancos(cba_descripcion,cba_observaciones) VALUES ('".mb_strtoupper($elemento->getDescripcion(),'UTF-8')."', '".mb_strtoupper($elemento->getObservaciones(),'UTF-8')."')";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
	$query="UPDATE sie_cat_bancos set  cba_descripcion='".mb_strtoupper($elemento->getDescripcion(),'UTF-8')."', cba_observaciones='".mb_strtoupper($elemento->getObservaciones(),'UTF-8')."' WHERE cba_id_banco=".$elemento->getId();
	$res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_cat_bancos set  cba_estatus=0 WHERE cba_id_banco=".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
	}
}
