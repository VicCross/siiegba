<?php
if(session_id() == '') {
    session_start();
}

include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Eje.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/LineaAccion.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class LineaAccionDaoJdbc {
    
    public function obtieneListado2($variable) {
		
	$lista= array();
		
	$query="SELECT * FROM sie_cat_ejes E, sie_cat_linea_accion LA WHERE E.cej_id_eje=LA.cej_id_eje AND LA.periodo=".$variable."  AND cla_estatus=1 ORDER BY cej_eje";
	$catalogo = new Catalogo();
    $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cla_id_lineaaccion")];
            $id_eje= $rs[strtoupper("cej_id_eje")];
            $cej_eje= $rs[strtoupper("cej_eje")];
            $cej_descripcion= $rs[strtoupper("cej_descripcion")];
            $lineaaccion= $rs[strtoupper("cla_lineaaccion")];
            $descripcion= $rs[strtoupper("cla_descripcion")];
			$estatus = $rs["cla_estatus"];
			$idLider = $rs["ID_LIDER"];
			$firma = $rs["firma"];
			$saldoInicial = $rs["saldoInicial"];
			$saldoAnterior = $rs["saldoAnterior"];			
			$periodo= $rs["periodo"];
			$inba=$rs["inba"];
			$patronato=$rs["patronato"];
			$pagado=$rs["pagado"];
			$pinba=$rs["pinba"];
			$ppratonato=$rs["ppatronato"];
			$pexterno=$rs["pexterno"];
            $eje = new Eje();
			
            $eje->setAll($id_eje,$cej_eje,$cej_descripcion,$periodo);

            $elemento = new LineaAccion();
            $elemento->setAll($id,$eje,$lineaaccion,$descripcion,$estatus, $rs[10],$firma, $saldoInicial,$saldoAnterior,$periodo,$inba,$patronato,$pagado,$pinba,$ppatronato,$pexterno);
			
            array_push($lista, $elemento);
        }	
	return $lista;
    }
    
	
	 public function obtieneListado() {
		
	$lista= array();
		
	$query="SELECT * FROM sie_cat_ejes E, sie_cat_linea_accion LA WHERE E.cej_id_eje=LA.cej_id_eje AND cla_estatus=1 ORDER BY cej_eje";
	$catalogo = new Catalogo();
    $result = $catalogo->obtenerLista($query);
    
        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cla_id_lineaaccion")];
            $id_eje= $rs[strtoupper("cej_id_eje")];
            $cej_eje= $rs[strtoupper("cej_eje")];
            $cej_descripcion= $rs[strtoupper("cej_descripcion")];
            $lineaaccion= $rs[strtoupper("cla_lineaaccion")];
            $descripcion= $rs[strtoupper("cla_descripcion")];
			$estatus = $rs["cla_estatus"];
			$idLider = $rs["ID_LIDER"];
			$firma = $rs["firma"];
			$saldoInicial = $rs["saldoInicial"];
			$saldoAnterior = $rs["saldoAnterior"];			
			$periodo= $rs["periodo"];
			$inba=$rs["inba"];
			$patronato=$rs["patronato"];
            $pagado=$rs["pagado"];
			$pinba=$rs["pinba"];
			$ppatronato=$rs["ppatronato"];
			$pexterno=$rs["pexterno"];
			$eje = new Eje();
			
            $eje->setAll($id_eje,$cej_eje,$cej_descripcion,$periodo);

            $elemento = new LineaAccion();
            $elemento->setAll($id,$eje,$lineaaccion,$descripcion,$estatus, $rs[10],$firma, $saldoInicial,$saldoAnterior,$periodo,$inba,$patronato,$pagado,$pinba,$ppatronato,$pexterno);
			
            array_push($lista, $elemento);
        }	
	return $lista;
    }
    
	
    public function obtieneElemento($idElemento) {
		
		
	$elemento=new LineaAccion();
		
	$query="SELECT * FROM sie_cat_ejes E, sie_cat_linea_accion LA WHERE E.cej_id_eje=LA.cej_id_eje AND LA.cla_id_lineaaccion=".$idElemento;
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cla_id_lineaaccion")];
            $id_eje= $rs[strtoupper("cej_id_eje")];
            $cej_eje= $rs[strtoupper("cej_eje")];
            $cej_descripcion= $rs[strtoupper("cej_descripcion")];
            $lineaaccion= $rs[strtoupper("cla_lineaaccion")];
            $descripcion= $rs[strtoupper("cla_descripcion")];
			$estatus = $rs["cla_estatus"];
			$idLider = $rs["ID_LIDER"];
			$firma = $rs["firma"];			
			$saldoInicial = $rs["saldoInicial"];
			$saldoAnterior = $rs["saldoAnterior"];	
			$periodo = $rs["periodo"];	
			$inba=$rs["inba"];
			$patronato=$rs["patronato"];
			$pagado=$rs["pagado"];
			$pinba=$rs["pinba"];
			$ppatronato=$rs["ppatronato"];
			$pexterno=$rs["pexterno"];
            $eje = new Eje();
            $eje->setAll($id_eje,$cej_eje,$cej_descripcion,$periodo);

            $elemento->setAll($id,$eje,$lineaaccion,$descripcion, $estatus, $rs[10], $firma, $saldoInicial, $saldoAnterior,$periodo,$inba,$patronato,$pagado,$pinba,$ppatronato,$pexterno);

        }	
	return $elemento;
		
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $aux = $elemento->getEje();
		
        $query="INSERT INTO sie_cat_linea_accion(cej_id_eje, cla_lineaaccion, cla_descripcion,periodo) VALUES ('".$aux->getId()."', '".mb_strtoupper($elemento->getLineaAccion(),'UTF-8')."','".mb_strtoupper($elemento->getDescripcion(),'UTF-8')."',".$aux->getPeriodo().")";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
        $aux = $elemento->getEje();
	$query="UPDATE sie_cat_linea_accion set  cej_id_eje='".$aux->getId()."', cla_descripcion='".mb_strtoupper($elemento->getDescripcion(),'UTF-8')."', cla_lineaaccion='".mb_strtoupper($elemento->getLineaAccion(),'UTF-8')."' WHERE cla_id_lineaaccion=".$elemento->getId();
	$res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
	
	public function actualizaSaldos($elemento){
		$catalogo = new Catalogo();
		$update = "UPDATE sie_cat_linea_accion SET saldoInicial = " . $elemento->getSaldoInicial() . ", saldoAnterior = " . $elemento->getSaldoAnterior() . " WHERE cla_id_lineaaccion = " . $elemento->getId();
		$res = $catalogo->obtenerLista($update);
		if($res == "1") return true;
		else return false;
	}
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_cat_linea_accion set  cla_estatus=0 WHERE cla_id_lineaaccion=".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
	
	public function guardaFirma($elemento){
		$catalogo = new Catalogo();
		$firma = "null";
		if(!is_null($elemento->getFirma())) $firma = "'" . $elemento->getFirma() . "'";
		$update = "UPDATE sie_cat_linea_accion set firma = " . $firma . " WHERE cla_id_lineaaccion = " . $elemento->getId();
		$res = $catalogo->obtenerLista($update);
		if($res == "1") return true;
		else return false;	
		
	}
	
	
	
	public function obtieneListadofiltro($periodo) {
		
	$lista= array();
	
	if($periodo == "" || $periodo == NULL || $periodo == 48){
    $query="SELECT * FROM sie_cat_ejes E, sie_cat_linea_accion LA WHERE E.cej_id_eje=LA.cej_id_eje and cla_estatus =1  ORDER BY cej_eje";
	
  
	}	
	
	else{
	$query="SELECT * FROM sie_cat_ejes E, sie_cat_linea_accion LA WHERE E.cej_id_eje=LA.cej_id_eje AND periodo='".$periodo."' ORDER BY cej_eje";
	}
	$catalogo = new Catalogo();
    $result = $catalogo->obtenerLista($query);
	
     
        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cla_id_lineaaccion")];
            $id_eje= $rs[strtoupper("cej_id_eje")];
            $cej_eje= $rs[strtoupper("cej_eje")];
            $cej_descripcion= $rs[strtoupper("cej_descripcion")];
            $lineaaccion= $rs[strtoupper("cla_lineaaccion")];
            $descripcion= $rs[strtoupper("cla_descripcion")];
			$estatus = $rs["cla_estatus"];
			$idLider = $rs["ID_LIDER"];
			$firma = $rs["firma"];
			$saldoInicial = $rs["saldoInicial"];
			$saldoAnterior = $rs["saldoAnterior"];			
			$periodo= $rs["periodo"];
			$inba=$rs["inba"];
			$patronato=$rs["patronato"];
            $pagado=$rs["pagado"];
			$pinba=$rs["pinba"];
			$ppatronato=$rs["ppatronato"];
			$pexterno=$rs["pexterno"];
			$eje = new Eje();
			
            $eje->setAll($id_eje,$cej_eje,$cej_descripcion,$periodo);

            $elemento = new LineaAccion();
            $elemento->setAll($id,$eje,$lineaaccion,$descripcion,$estatus, $rs[10],$firma, $saldoInicial,$saldoAnterior,$periodo,$inba,$patronato,$pagado,$pinba,$ppatronato,$pexterno);
			
            array_push($lista, $elemento);
        }
	
	
	 
	 
	return $lista;
    }
    
	
	
	
	
	
}
