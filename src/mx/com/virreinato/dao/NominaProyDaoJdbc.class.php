<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/NominaProy.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class NominaProyDaoJdbc {
    
    public function obtieneListado() {
		
	$lista= array();
		
	$query="SELECT p.cpr_numeroproyecto,p.cpr_descripcion,(concat_ws(' ',per.ppr_app,per.ppr_apm,per.ppr_nombre)) as describePersona,n.* "
            ." FROM sie_proy_nomina n,sie_cat_proyectos p,sie_proy_personal per"
            ." WHERE n.cpr_id_proyecto=p.cpr_id_proyecto AND n.ppr_id_personalproyectos=per.ppr_id_personalproyectos"
            ." ORDER BY  p.cpr_numeroproyecto,per.ppr_app,per.ppr_apm,per.ppr_nombre";

	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id = $rs[strtoupper("pno_id_proynomina")];
            $idProyecto = $rs[strtoupper("cpr_id_proyecto")];
            $desProyecto = $rs["cpr_descripcion"];
            $responsable = $rs[strtoupper("pno_responsable")];
            $periodoPago = $rs[strtoupper("pno_periododepago")];
            $dias = $rs[strtoupper("pno_dias")];
            $salarioDiario= $rs[strtoupper("pno_salariodiario")];
            $importe= $rs[strtoupper("pno_importe")];
            $creditoAlSalario= $rs[strtoupper("pno_creditoalsalario")];
            $bonificacion= $rs[strtoupper("pno_bonificacion")];
            $neto= $rs[strtoupper("pno_neto")];
            $idPersonal = $rs[strtoupper("ppr_id_personalproyectos")];
            $desPersonal = $rs["describePersona"];
        
            $elemento = new NominaProy();
            $elemento->setAll($id,$idProyecto, $desProyecto, $responsable,$periodoPago,$dias,$salarioDiario,$importe, $creditoAlSalario,$bonificacion,$neto,$idPersonal,$desPersonal);
            array_push($lista, $elemento);
        }	
        return $lista;
    }
    
    public function obtieneElemento($idElemento) {
		
	$elemento=new NominaProy();
		
	$query="SELECT n.* FROM sie_proy_nomina n WHERE n.pno_id_proynomina=".$idElemento;
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id = $rs[strtoupper("pno_id_proynomina")];
            $idProyecto = $rs[strtoupper("cpr_id_proyecto")];
            $desProyecto = "";
            $responsable = $rs[strtoupper("pno_responsable")];
            $periodoPago = $rs[strtoupper("pno_periododepago")];
            $dias = $rs[strtoupper("pno_dias")];
            $salarioDiario= $rs[strtoupper("pno_salariodiario")];
            $importe= $rs[strtoupper("pno_importe")];
            $creditoAlSalario= $rs[strtoupper("pno_creditoalsalario")];
            $bonificacion= $rs[strtoupper("pno_bonificacion")];
            $neto= $rs[strtoupper("pno_neto")];
            $idPersonal = $rs[strtoupper("ppr_id_personalproyectos")];
            $desPersonal = "";

            $elemento->setAll($id,$idProyecto, $desProyecto, $responsable,$periodoPago,$dias,$salarioDiario,$importe, $creditoAlSalario,$bonificacion,$neto,$idPersonal,$desPersonal);
        }	
        return $elemento;
    }

    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $query="INSERT INTO sie_proy_nomina(cpr_id_proyecto,pno_responsable,pno_periododepago,pno_dias,pno_salariodiario,pno_importe,pno_creditoalsalario,pno_bonificacion,pno_neto,ppr_id_personalproyectos) VALUES (".$elemento->getIdProy().", '".mb_strtoupper($elemento->getResponsable(),'UTF-8')."', '"
            .$elemento->getPeriodoPago()."', "
            .$elemento->getDias().", "
            .$elemento->getSalarioDiario().", "
            .$elemento->getImporte().", "
            .$elemento->getCreditoAlSalario().", "
            .$elemento->getBonificacion().", "
            .$elemento->getNeto().", "
            .$elemento->getIdPersonal().")";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
	$query="UPDATE sie_proy_nomina set "
            ." cpr_id_proyecto=".$elemento->getIdProy().", "
            ." pno_responsable='".mb_strtoupper($elemento->getResponsable(),'UTF-8')."', "
            ." pno_periododepago='".$elemento->getPeriodoPago()."', "
            ." pno_dias=".$elemento->getDias().", "
            ." pno_salariodiario=".$elemento->getSalarioDiario().", "
            ." pno_importe=".$elemento->getImporte().", "
            ." pno_creditoalsalario=".$elemento->getCreditoAlSalario().", "
            ." pno_bonificacion=".$elemento->getBonificacion().", "
            ." pno_neto=".$elemento->getNeto().", "
            ." ppr_id_personalproyectos=".$elemento->getIdPersonal()." "
            ." WHERE pno_id_proynomina=".$elemento->getId();
        $res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="DELETE FROM sie_proy_nomina WHERE pno_id_proynomina=".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}
