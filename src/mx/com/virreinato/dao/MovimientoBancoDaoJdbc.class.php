<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatProyecto.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CuentaBancaria.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/CuentaBancariaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/CatProyectoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/MovimientoBanco.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class MovimientoBancoDaoJdbc {
    
    public function obtieneListado($fechaIni, $fechaFin) {
		
	$lista= array();
		
	$query="SELECT * FROM sie_bancos WHERE ban_fecha >= '".date("Y-m-d",strtotime($fechaFin))."' and ban_fecha < '".date("Y-m-d",strtotime($fechaIni))."' and ban_estatus=1 ORDER BY ban_fecha,ban_numoperacion";
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);
	$mcuenta= array();
	$mperiodo= array();
	$mproyecto= array();
			
	while ($rs = mysql_fetch_array($result)){
				
            $id= $rs[strtoupper("ban_id_bancos")];
            $id_cuenta= $rs[strtoupper("ccb_id_cuentabancaria")];
            $cuenta=null;

            if(array_key_exists($id_cuenta, $mcuenta)){
                $cuenta=$mcuenta[$id_cuenta];
            }
            else{
                $daocta=new CuentaBancariaDaoJdbc();
                $cuenta=$daocta->obtieneElemento($id_cuenta);
                $mcuenta[$id_cuenta] = $cuenta;
            }

            $numOperacion= $rs[strtoupper("ban_numoperacion")];
            $cargo= $rs[strtoupper("ban_cargo")];
            $abono= $rs[strtoupper("ban_abono")];
            $fecha= $rs[strtoupper("ban_fecha")];
            $destino= $rs[strtoupper("ban_destino")];
            $referencia= $rs[strtoupper("ban_referencia")];

            $id_proyecto= $rs[strtoupper("cpr_id_proyecto")];
            $proyecto=new CatProyecto();

            if($id_proyecto!=null && array_key_exists($id_proyecto, $mproyecto)){
                $proyecto=$mproyecto[$id_proyecto];
            }
            else if($id_proyecto!=null){
                $daopyt=new CatProyectoDaoJdbc();
                $proyecto=$daopyt->obtieneElemento($id_proyecto);
                $mproyecto[$id_proyecto] = $proyecto;
            }

            $ban_formacarga = $rs[strtoupper("ban_formacarga")];

            $id_periodo= $rs[strtoupper("cpe_id_periodo")];
           $periodo=null;

            if(array_key_exists($id_periodo, $mperiodo)){
                $periodo=$mperiodo[$id_periodo];
            }
            else{
                $daoprd=new PeriodoDaoJdbc();
                $periodo=$daoprd->obtieneElemento($id_periodo);
                $mperiodo[$id_periodo] = $periodo;
            }
            $che_id_cheque= $rs[strtoupper("che_id_cheque")];

            $elemento = new MovimientoBanco();
            $elemento->setAll($id,$cuenta,$numOperacion,$cargo, $abono, $fecha, $destino, $referencia, $proyecto, $ban_formacarga,$periodo, $che_id_cheque);
            array_push($lista, $elemento);
        }	

        return $lista;
    }
    
    public function obtieneElemento($idElemento) {
		
	$elemento=new MovimientoBanco();
		
	$query="SELECT * FROM sie_bancos WHERE ban_id_bancos=".$idElemento;
		
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("ban_id_bancos")];
            $id_cuenta= $rs[strtoupper("ccb_id_cuentabancaria")];
            $daocta=new CuentaBancariaDaoJdbc();
            $cuenta=$daocta->obtieneElemento($id_cuenta);

            $numOperacion= $rs[strtoupper("ban_numoperacion")];
            $cargo= $rs[strtoupper("ban_cargo")];
            $abono= $rs[strtoupper("ban_abono")];
            $fecha= $rs[strtoupper("ban_fecha")];
            $destino= $rs[strtoupper("ban_destino")];
            $referencia= $rs[strtoupper("ban_referencia")];

            $id_proyecto= $rs[strtoupper("cpr_id_proyecto")];
            $daopyt=new CatProyectoDaoJdbc();
            $proyecto=$daopyt->obtieneElemento($id_proyecto);

            $ban_formacarga= $rs[strtoupper("ban_formacarga")];

            $id_periodo= $rs[strtoupper("cpe_id_periodo")];
            $daoprd=new PeriodoDaoJdbc();
            $periodo=$daoprd->obtieneElemento($id_periodo);

            $che_id_cheque= $rs[strtoupper("che_id_cheque")];
            
            $elemento = new MovimientoBanco();
            $elemento->setAll($id,$cuenta,$numOperacion,$cargo, $abono, $fecha, $destino, $referencia, $proyecto, $ban_formacarga,$periodo, $che_id_cheque);
				
				
        }
		
        return $elemento;
		
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $idProyecto="null";
        $cheque = null;
        if($elemento->getProyecto()!=null){
            $proyecto = $elemento->getProyecto();
            $idProyecto=(String)($proyecto->getId());	
            if($idProyecto == null){
                $idProyecto = "null";
            }
        }
        if($elemento->getIdCheque() == null){ $cheque = "null"; }else{ $cheque = $elemento->getIdCheque(); }
        $periodo = $elemento->getPeriodo();
        $cuenta = $elemento->getCuenta();
        $query="INSERT INTO sie_bancos(ccb_id_cuentabancaria,ban_numoperacion,ban_cargo,ban_abono, ban_fecha, ban_destino, ban_referencia, cpr_id_proyecto, ban_formacarga, cpe_id_periodo, che_id_cheque,ban_estatus) VALUES (".$cuenta->getId().", '".$elemento->getNumeroOperacion()."', ".$elemento->getCargo()." , ".$elemento->getAbono()." , '".date("Y-m-d",strtotime($elemento->getFecha()))."' , '".$elemento->getDestino()."' , '".$elemento->getReferencia()."' , ".$idProyecto." , '".$elemento->getFormaCarga()."' , ".$periodo->getId()." , ".$cheque.", 1)";
        $res=$con->obtenerLista($query);
        
        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
        $cuenta = $elemento->getCuenta();
        $proyecto = $elemento->getProyecto();
        $periodo = $elemento->getPeriodo();
        $cheque = $id = null;
        
        if($proyecto->getId() == null){ $id = "null"; }else{ $id = $proyecto->getId(); }
        if($elemento->getIdCheque() == null){ $cheque = "null"; }else{ $cheque = $elemento->getIdCheque(); }
        
	$query="UPDATE sie_bancos set ccb_id_cuentabancaria=".$cuenta->getId().", ban_numoperacion='".$elemento->getNumeroOperacion()."' , ban_cargo=".$elemento->getCargo()." , ban_abono=".$elemento->getAbono()." , ban_fecha='".date("Y-m-d",strtotime($elemento->getFecha()))."' , ban_destino='".$elemento->getDestino()."' , ban_referencia='".$elemento->getReferencia()."' , cpr_id_proyecto=".$id." ,  cpe_id_periodo=".$periodo->getId()." , che_id_cheque=".$cheque." WHERE ban_id_bancos=".$elemento->getId();
	$res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_bancos set  ban_estatus=0 WHERE ban_id_bancos=".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}
