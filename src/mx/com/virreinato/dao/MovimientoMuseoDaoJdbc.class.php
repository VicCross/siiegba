<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Parametro.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class MovimientoMuseoDaoJdbc {
    
    public function obtieneListado($fechaIni, $fechaFin) {
		
	$lista= array();
		
	$query=strtolower("SELECT MIN_FECHA as fecha, 0.0 as cargo, MIN_MONTO as abono, CAST(MIN_DESCRIPCION AS CHAR CHARACTER SET utf8) as descripcion FROM SIE_MINISTRACIONES WHERE MIN_FECHA >= '".date("Y-m-d",strtotime($fechaIni))."' and MIN_FECHA <= '".date("Y-m-d",strtotime($fechaFin))."' AND MIN_FORMACARGA = 1 UNION " .
            "SELECT  CHE_FECHA_EMISION as fecha, CHE_MONTO as cargo, 0.0 as abono, CAST(CHE_OBSERVACIONES AS CHAR CHARACTER SET utf8) as descripcion FROM SIE_CHEQUES WHERE CHE_ESTATUS='CO' AND CHE_DESTINO='PR' AND CHE_FECHA_EMISION >= '".date("Y-m-d",strtotime($fechaIni))."' and CHE_FECHA_EMISION <= '".date("Y-m-d",strtotime($fechaFin))."' AND CHE_ESTATUSBORRADO = 1 UNION ".
            "SELECT REI_FECHA as fecha, REI_HONORARIOS as cargo, 0.0 as abono,  CAST('Reintegro de Impuestos' AS CHAR CHARACTER SET utf8) as descripcion FROM SIE_REINTEGRO_IMPUESTOS WHERE REI_FECHA >= '".date("Y-m-d",strtotime($fechaIni))."' and REI_FECHA <= '".date("Y-m-d",strtotime($fechaFin))."' AND REI_ESTATUS = 1 ");
		
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
				
            $fecha= $rs["fecha"];
            $cargo= $rs["cargo"];
            $abono= $rs["abono"];
            $descripcion= $rs["descripcion"];

            $elemento = new MovimientoMuseo();
            $elemento->setAll($fecha,$cargo, $abono, $descripcion);
            array_push($lista, $elemento);
        }	
        return $lista;
    }
}
