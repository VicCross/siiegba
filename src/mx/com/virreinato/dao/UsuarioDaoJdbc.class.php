<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Perfil.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Usuario.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Persona.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class UsuarioDaoJdbc {
    
    public function obtieneListado() {
		
        $lista= array();

        $query="SELECT U.*, P.*,PE.* FROM sie_cat_usuarios U, sie_cat_perfiles P,cat_empleado PE WHERE U.cper_id_perfil=P.cper_id_perfil AND U.cpe_id_persona=PE.cem_id_empleado and CUS_ESTATUS=1";
        $catalogo = new Catalogo();       
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cus_id_usuario")];
            $usuario=$rs[strtoupper("usr")];
            $password=$rs[strtoupper("pwd")];

            $id_perfil= $rs[strtoupper("cper_id_perfil")];
            $per_descripcion= $rs[strtoupper("cper_descripcion")];
            
            $perfil = new Perfil();
            $perfil->setAll($id_perfil,$per_descripcion);

            $id_persona= $rs[strtoupper("cem_id_empleado")];
            $nombre= $rs[strtoupper("cem_nombre")];
            $appaterno= $rs[strtoupper("cem_appaterno")];
            $apmaterno= $rs[strtoupper("cem_apmaterno")];
            $sueldo= $rs[strtoupper("cem_sueldo_neto")];
			$puesto= $rs[strtoupper("cem_puesto")];
			$rfc= $rs[strtoupper("cem_rfc")];
			$nomina= $rs[strtoupper("cem_tipo_nomina")];
			$letra= $rs[strtoupper("cem_sueldo_letra")];
			$emp= $rs[strtoupper("cem_numero_emp")];
            
            $persona = new Persona();
            $persona->setAll($id_persona,$nombre,$appaterno,$apmaterno,$sueldo,$puesto,$rfc,$nomina,$letra,$emp);

            $elemento=null;

            if($id_perfil == 7){
                //obtengo el id de los proyectos a los que tiene acceso
                $query_p="SELECT * FROM sie_usuario_x_meta WHERE cus_id_usuario=".$id;
                $rsp2 = $catalogo->obtenerLista($query_p);
                if($rsp2!=null){
                    $id_metas=array();

                    while ($rsp = mysql_fetch_array($rsp2)){
                            $id_meta= $rsp[strtoupper("proyecto_id_meta")];
                            array_push($id_metas,$id_meta);
                    }

                    $elemento = new Usuario();
                    $elemento->setAll($id,$perfil,$persona,$usuario,$password,$id_metas);
                }
                else{
                    $elemento = new Usuario();
                    $elemento->setAll($id,$perfil,$persona,$usuario,$password,null);
                }

            }
            else{
                    $elemento = new Usuario();
                    $elemento->setAll($id,$perfil,$persona,$usuario,$password,null);
            }


           array_push($lista,$elemento);
        }	
        return $lista;
    }
    
    public function obtieneElemento($idElemento) {
		
		
        $elemento=new Usuario();

        $query="SELECT U.*, P.*,PE.* FROM sie_cat_usuarios U, sie_cat_perfiles P,cat_empleado PE WHERE U.cper_id_perfil=P.cper_id_perfil AND U.cpe_id_persona=PE.cem_id_empleado AND U.cus_id_usuario=".$idElemento;
        $catalogo = new Catalogo();       
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cus_id_usuario")];
            $usuario=$rs[strtoupper("usr")];
            $password=$rs[strtoupper("pwd")];

            $id_perfil= $rs[strtoupper("cper_id_perfil")];
            $per_descripcion= $rs[strtoupper("cper_descripcion")];
            
            $perfil = new Perfil();
            $perfil->setAll($id_perfil,$per_descripcion);

           $id_persona= $rs[strtoupper("cem_id_empleado")];
            $nombre= $rs[strtoupper("cem_nombre")];
            $appaterno= $rs[strtoupper("cem_appaterno")];
            $apmaterno= $rs[strtoupper("cem_apmaterno")];
            $sueldo= $rs[strtoupper("cem_sueldo_neto")];
			$puesto= $rs[strtoupper("cem_puesto")];
			$rfc= $rs[strtoupper("cem_rfc")];
			$nomina= $rs[strtoupper("cem_tipo_nomina")];
			$letra= $rs[strtoupper("cem_sueldo_letra")];
			$emp= $rs[strtoupper("cem_numero_emp")];
            
            $persona = new Persona();
            $persona->setAll($id_persona,$nombre,$appaterno,$apmaterno,$sueldo,$puesto,$rfc,$nomina,$letra,$emp);
				
            if($id_perfil == 7){
                    //obtengo el id de los proyectos a los que tiene acceso
                $query_p="SELECT * FROM sie_usuario_x_meta WHERE cus_id_usuario=".$id;
                $rsp2 = $catalogo->obtenerLista($query_p);
                if($rsp2!=null){
                        $id_metas=array();

                        while ($rsp = mysql_fetch_array($rsp2)){
                                $id_meta= $rsp[strtoupper("proyecto_id_meta")];
                                array_push($id_metas,$id_meta);
                        }

                        $elemento = new Usuario();
                        $elemento->setAll($id,$perfil,$persona,$usuario,$password,$id_metas);
                }
                else{
                        $elemento = new Usuario();
                        $elemento->setAll($id,$perfil,$persona,$usuario,$password,null);
                }

            }
            else{
                    $elemento = new Usuario();
                    $elemento->setAll($id,$perfil,$persona,$usuario,$password,null);
            }														
	}
		
	return $elemento;	
    }
    
    public function obtieneElementoByDesc($desc) {
		
		
        $elemento=new Usuario();

        $query="SELECT U.*, P.*,PE.* FROM sie_cat_usuarios U, sie_cat_perfiles P,cat_empleado PE WHERE U.cper_id_perfil=P.cper_id_perfil AND U.cpe_id_persona=PE.cem_id_empleado AND usr='".$desc."' AND U.CUS_ESTATUS='1' ";
        $catalogo = new Catalogo();       
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cus_id_usuario")];
            $usuario=$rs[strtoupper("usr")];
            $password=$rs[strtoupper("pwd")];

            $id_perfil= $rs[strtoupper("cper_id_perfil")];
            $per_descripcion= $rs[strtoupper("cper_descripcion")];
            
            $perfil = new Perfil();
            $perfil->setAll($id_perfil,$per_descripcion);

            $id_persona= $rs[strtoupper("cem_id_empleado")];
            $nombre= $rs[strtoupper("cem_nombre")];
            $appaterno= $rs[strtoupper("cem_appaterno")];
            $apmaterno= $rs[strtoupper("cem_apmaterno")];
            $sueldo= $rs[strtoupper("cem_sueldo_neto")];
			$puesto= $rs[strtoupper("cem_puesto")];
			$rfc= $rs[strtoupper("cem_rfc")];
			$nomina= $rs[strtoupper("cem_tipo_nomina")];
			$letra= $rs[strtoupper("cem_sueldo_letra")];
			$emp= $rs[strtoupper("cem_numero_emp")];
            
            $persona = new Persona();
            $persona->setAll($id_persona,$nombre,$appaterno,$apmaterno,$sueldo,$puesto,$rfc,$nomina,$letra,$emp);
				
            if($id_perfil == 7){
                    //obtengo el id de los proyectos a los que tiene acceso
                $query_p="SELECT * FROM sie_usuario_x_meta WHERE cus_id_usuario=".$id;
                $rsp2 = $catalogo->obtenerLista($query);
                if($rsp2!=null){
                        $id_metas=array();

                        while ($rsp = mysql_fetch_array($rsp2)){
                                $id_meta= $rsp[strtoupper("proyecto_id_meta")];
                                array_push($id_metas,$id_meta);
                        }

                        $elemento = new Usuario();
                        $elemento->setAll($id,$perfil,$persona,$usuario,$password,$id_metas);
                }
                else{
                        $elemento = new Usuario();
                        $elemento->setAll($id,$perfil,$persona,$usuario,$password,null);
                }

            }
            else{
                    $elemento = new Usuario();
                    $elemento->setAll($id,$perfil,$persona,$usuario,$password,null);
            }														
	}
		
	return $elemento;	
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $persona = $elemento->getPersona();
        $perfil = $elemento->getPerfil();
        $query="INSERT INTO sie_cat_usuarios(CPER_ID_PERFIL,CPE_ID_PERSONA,USR,PWD,CUS_ESTATUS) VALUES ('".$perfil->getId()."', '".$persona->getId()."', '".$elemento->getUsuario()."', '".$elemento->getPassword()."',1)";        
        $res = $con->obtenerLista($query);

        //verifico si el perfil es 7 para guardar los id_proyectos
        if($perfil->getId() == 7 && $elemento->getIdMetas()!=null){
                //obtengo el id de los proyectos a los que tiene acceso

                $itMetas =  $elemento->getIdMetas();
                foreach($itMetas as $resu){
                        $consulta = "SELECT MAX(CUS_ID_USUARIO) AS id FROM sie_cat_usuarios;";
                        $rta = $con->obtenerLista($consulta);
                        while($rta2 = mysql_fetch_array($rta))
                        { $num = (int)$rta2['id']; }
                        $query_metas = "INSERT INTO sie_usuario_x_meta VALUES(".$resu.",".$num.")";
                        //System.out.println(query_metas);
                        $con->obtenerLista($query_metas);
                }

        }
        if($res == "1"){return true;}
        else{ return false; }
		
    }
    
    public function actualizaElemento($elemento) {
		
        $con=new Catalogo();
        $persona = $elemento->getPersona();
        $perfil = $elemento->getPerfil();
        $query="UPDATE sie_cat_usuarios set  cper_id_perfil='".$perfil->getId()."', cpe_id_persona='".$persona->getId()."' , usr='".$elemento->getUsuario()."' , pwd='".$elemento->getPassword()."' WHERE cus_id_usuario=".$elemento->getId();
        //echo $query;
        $res = $con->obtenerLista($query);
		
        if($perfil->getId() == 7 && $elemento->getIdMetas()){
                //actualizo el id de los proyectos a los que tiene acceso

                //borro los id_proyectos actuales
                $query_p="DELETE FROM sie_usuario_x_meta WHERE cus_id_usuario=".$elemento->getId();
                $con->obtenerLista($query_p);

                $itMetas =  $elemento->getIdMetas();
                foreach($itMetas as $resu){
                        $query_metas = "INSERT INTO sie_usuario_x_meta VALUES(".$resu.", ".$elemento->getId().")";
                        $con->obtenerLista($query_metas);
                }			
        }

        if($res == "1"){return true;}
        else{ return false; }	
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_cat_usuarios set  cus_estatus=0 WHERE cus_id_usuario=".$idElemento;
        $res=$con->obtenerLista($query);
		
	if($res == "1"){return true;}
        else{ return false; }	
    }
}
