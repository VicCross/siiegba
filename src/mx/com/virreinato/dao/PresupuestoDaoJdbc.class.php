<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Presupuesto.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class PresupuestoDaoJdbc {
    
    public function obtieneListado($inicio, $fin, $periodo, $Estatus, $idPerfil, $idUsuario) {

        $lista= array();
        $query = null;

        if( $idPerfil == 7 ){
                $query = " select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director, pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino,  p.cpr_descripcion , p.cpr_id_proyecto, pm.proyecto_desc_meta, pm.proyecto_id_meta " .
                                "from sie_solicitud_presup pr, sie_cat_proyectos p, sie_usuario_x_meta um, sie_proyecto_metas pm where um.cus_id_usuario  = ".$idUsuario." AND " .
                                "um.proyecto_id_meta = pm.proyecto_id_meta AND pm.cpr_id_proyecto = p.cpr_id_proyecto AND pm.proyecto_id_meta = pr.proyecto_id_meta " .
                                "AND pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND p.cpe_id_periodo = ".$periodo." AND pr.spr_pre_estatus = 1 ORDER BY(pr.spr_fecha) ";


        }else{

                if( $Estatus =="TD"){
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director, pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino, p.cpr_descripcion , p.cpr_id_proyecto, m.proyecto_desc_meta, m.proyecto_id_meta from sie_solicitud_presup pr, sie_cat_proyectos p, sie_proyecto_metas m where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND p.cpe_id_periodo = ".$periodo." AND pr.spr_pre_estatus = 1 and pr.proyecto_id_meta = m.proyecto_id_meta AND m.cpr_id_proyecto = p.cpr_id_proyecto ORDER BY(pr.spr_fecha)";
                }else if( $Estatus == "ST") {
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director, pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino, p.cpr_descripcion , p.cpr_id_proyecto,m.proyecto_desc_meta, m.proyecto_id_meta from sie_solicitud_presup pr, sie_cat_proyectos p, sie_proyecto_metas m where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND p.cpe_id_periodo = ".$periodo." AND pr.spr_pre_estatus = 1 AND pr.spr_vobo_st = 'Y' and pr.proyecto_id_meta = m.proyecto_id_meta AND m.cpr_id_proyecto = p.cpr_id_proyecto ORDER BY(pr.spr_fecha)";	
                }else if( $Estatus == "SA") {
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director, pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino, p.cpr_descripcion , p.cpr_id_proyecto,m.proyecto_desc_meta, m.proyecto_id_meta from sie_solicitud_presup pr, sie_cat_proyectos p, sie_proyecto_metas m where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND p.cpe_id_periodo = ".$periodo." AND pr.spr_pre_estatus = 1 AND pr.spr_vobo_sa = 'Y' and pr.proyecto_id_meta = m.proyecto_id_meta AND m.cpr_id_proyecto = p.cpr_id_proyecto ORDER BY(pr.spr_fecha)";	
                }else if( $Estatus == "DR") {
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director, pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino, p.cpr_descripcion , p.cpr_id_proyecto,m.proyecto_desc_meta, m.proyecto_id_meta from sie_solicitud_presup pr, sie_cat_proyectos p, sie_proyecto_metas m where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND p.cpe_id_periodo = ".$periodo." AND pr.spr_pre_estatus = 1 AND pr.spr_vobo_director = 'Y' and pr.proyecto_id_meta = m.proyecto_id_meta AND m.cpr_id_proyecto = p.cpr_id_proyecto ORDER BY(pr.spr_fecha)";	
                }else if( $Estatus == "CA" || $Estatus == "IP"){
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director,  pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino,  p.cpr_descripcion , p.cpr_id_proyecto, m.proyecto_desc_meta, m.proyecto_id_meta from sie_solicitud_presup pr, sie_cat_proyectos p, sie_proyecto_metas m where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND p.cpe_id_periodo = ".$periodo." AND pr.spr_pre_estatus = 1 AND pr.spr_estatus = '".$Estatus."' and pr.proyecto_id_meta = m.proyecto_id_meta AND m.cpr_id_proyecto = p.cpr_id_proyecto ORDER BY(pr.spr_fecha)";
                }else if( $Estatus == "NT"){
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director, pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino, p.cpr_descripcion , p.cpr_id_proyecto, m.proyecto_desc_meta, m.proyecto_id_meta from sie_solicitud_presup pr, sie_cat_proyectos p, sie_proyecto_metas m where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND p.cpe_id_periodo = ".$periodo." AND pr.spr_pre_estatus = 1 AND pr.spr_vobo_st is null and pr.proyecto_id_meta = m.proyecto_id_meta AND m.cpr_id_proyecto = p.cpr_id_proyecto ORDER BY(pr.spr_fecha)";
                }else if( $Estatus == "NA"){
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director, pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino, p.cpr_descripcion , p.cpr_id_proyecto, m.proyecto_desc_meta, m.proyecto_id_meta from sie_solicitud_presup pr, sie_cat_proyectos p, sie_proyecto_metas m where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND p.cpe_id_periodo = ".$periodo." AND pr.spr_pre_estatus = 1 and pr.spr_vobo_sa is null and pr.proyecto_id_meta = m.proyecto_id_meta AND m.cpr_id_proyecto = p.cpr_id_proyecto ORDER BY(pr.spr_fecha)";
                }else if( $Estatus == "ND"){
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director, pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino, p.cpr_descripcion , p.cpr_id_proyecto, m.proyecto_desc_meta, m.proyecto_id_meta from sie_solicitud_presup pr, sie_cat_proyectos p, sie_proyecto_metas m where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND p.cpe_id_periodo = ".$periodo." AND pr.spr_pre_estatus = 1 and pr.spr_vobo_director is null and pr.proyecto_id_meta = m.proyecto_id_meta AND m.cpr_id_proyecto = p.cpr_id_proyecto ORDER BY(pr.spr_fecha)";
                }else if( $Estatus == "NO"){
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director,  pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino, p.cpr_descripcion , p.cpr_id_proyecto, m.proyecto_desc_meta, m.proyecto_id_meta from sie_solicitud_presup pr, sie_cat_proyectos p, sie_proyecto_metas m where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND p.cpe_id_periodo = ".$periodo." AND pr.spr_pre_estatus = 1 AND ( pr.spr_vobo_sa = 'N' or pr.spr_vobo_st = 'N' or pr.spr_vobo_director = 'N'  ) and pr.proyecto_id_meta = m.proyecto_id_meta AND m.cpr_id_proyecto = p.cpr_id_proyecto ORDER BY(pr.spr_fecha)";
                }
                else if ( $Estatus == "DOS"){
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director,  pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino, p.cpr_descripcion , p.cpr_id_proyecto, m.proyecto_desc_meta, m.proyecto_id_meta from sie_solicitud_presup pr, sie_cat_proyectos p, sie_proyecto_metas m where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND p.cpe_id_periodo = ".$periodo." AND pr.spr_pre_estatus = 1 AND" .
                                        "( (pr.spr_vobo_st = 'Y' and pr.spr_vobo_sa = 'Y') or (pr.spr_vobo_st = 'Y' and pr.spr_vobo_director = 'Y') or (pr.spr_vobo_sa = 'Y' and pr.spr_vobo_director = 'Y')   ) " .
                                        " and pr.proyecto_id_meta = m.proyecto_id_meta AND m.cpr_id_proyecto = p.cpr_id_proyecto ORDER BY(pr.spr_fecha)";
                }

        }

        $rsMonto = null;
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $idPresupuesto= $rs[strtoupper("spr_id_solicitud")];
            $fecha = $rs[strtoupper("spr_fecha")];
            $idProyecto = $rs[strtoupper("cpr_id_proyecto")];
            $idMeta = $rs[strtoupper("proyecto_id_meta")];
            $desc = $rs[strtoupper("cpr_descripcion")];
            $meta = $rs[strtoupper("proyecto_desc_meta")];
            $destino=$rs[strtoupper("spr_destino")];
            $estatus = "";
            $monto = 0;

                $query = "select SUM(spr_monto) as monto from sie_det_sol_presup where spr_estatus = 1 AND spr_id_solicitud  = ".$idPresupuesto;
                $resultMonto = $catalogo->obtenerLista($query);
                if( $rsMonto = mysql_fetch_array($resultMonto)){  $monto = $rsMonto[strtoupper("monto")]; }

                if( $rs["spr_estatus"] == "CA"){ $estatus = "Capturado"; }
                else{

                        if( $rs[strtoupper("spr_vobo_st")] != null && $rs[strtoupper("spr_vobo_st")] == "Y"){  $estatus = $estatus." Con VoBo de ST"; }
                        else if($rs[strtoupper("spr_vobo_st")] != null &&  $rs[strtoupper("spr_vobo_st")] == "N"){ $estatus = $estatus." No Aceptado por ST"; }

                        if(  $rs[strtoupper("spr_vobo_sa")] != null && $rs[strtoupper("spr_vobo_sa")] == "Y"){ $estatus = $estatus."<br/> Con VoBo de SA"; }
                        else if( $rs[strtoupper("spr_vobo_sa")] != null && $rs[strtoupper("spr_vobo_sa")] == "N"){ $estatus = $estatus."<br/> No Aceptado por SA"; }

                        if( $rs["spr_vobo_director"] != null && $rs["spr_vobo_director"] == "Y"){ $estatus = $estatus."<br/> Con VoBo de Director"; }
                        else if( $rs["spr_vobo_director"] != null && $rs["spr_vobo_director"] == "N"){ $estatus = $estatus."<br/> No Aceptado por Director"; }

                        if( $rs["spr_estatus"] == "IP"){ $estatus = $estatus."<br/> Impreso"; }
                }

                $elemento = new Presupuesto();
                $elemento->constructor2($idPresupuesto, $fecha, $idProyecto, $idMeta, $monto,$desc,$meta,$estatus,$destino);
                array_push($lista, $elemento);		
        }
        return $lista;
    }
    
    public function obtieneListadoDestino($inicio, $fin, $periodo, $Estatus, $idPerfil, $idUsuario, $destino) {
		
	$lista= array();
	$query = null;
		
        $inicio = date("Y-m-d",strtotime($inicio));
        $fin = date("Y-m-d",strtotime($fin));
        
        if($idUsuario == null){
            $idUsuario = "null";
        }
        
        if($periodo ==null){
            $periodo = 'null';
        }
        if( $idPerfil == 7 && $destino == "PR" ){
                $query = " select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director, pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino,  p.cpr_descripcion , p.cpr_id_proyecto, pm.proyecto_desc_meta, pm.proyecto_id_meta " .
                "from sie_solicitud_presup pr, sie_cat_proyectos p, sie_usuario_x_meta um, sie_proyecto_metas pm where um.cus_id_usuario  = ".$idUsuario." AND " .
                "um.proyecto_id_meta = pm.proyecto_id_meta AND pm.cpr_id_proyecto = p.cpr_id_proyecto AND pm.proyecto_id_meta = pr.proyecto_id_meta " .
                "AND pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND p.cpe_id_periodo = ".$periodo." AND pr.spr_destino='".$destino."' AND pr.spr_pre_estatus = 1 ORDER BY(pr.spr_fecha) ";


        }else{

            if($destino == "PR"){
                if( $Estatus == "TD" ){
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director, pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino, p.cpr_descripcion , p.cpr_id_proyecto, m.proyecto_desc_meta, m.proyecto_id_meta from sie_solicitud_presup pr, sie_cat_proyectos p, sie_proyecto_metas m where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND p.cpe_id_periodo = ".$periodo." AND pr.spr_destino='".$destino."' AND pr.spr_pre_estatus = 1 and pr.proyecto_id_meta = m.proyecto_id_meta AND m.cpr_id_proyecto = p.cpr_id_proyecto ORDER BY(pr.spr_fecha)";
                }else if( $Estatus == "ST" ) {
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director, pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino, p.cpr_descripcion , p.cpr_id_proyecto,m.proyecto_desc_meta, m.proyecto_id_meta from sie_solicitud_presup pr, sie_cat_proyectos p, sie_proyecto_metas m where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND p.cpe_id_periodo = ".$periodo." AND pr.spr_destino='".$destino."' AND pr.spr_pre_estatus = 1 AND pr.spr_vobo_st = 'Y' and pr.proyecto_id_meta = m.proyecto_id_meta AND m.cpr_id_proyecto = p.cpr_id_proyecto ORDER BY(pr.spr_fecha)";	
                }else if( $Estatus == "SA" ) {
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director, pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino, p.cpr_descripcion , p.cpr_id_proyecto,m.proyecto_desc_meta, m.proyecto_id_meta from sie_solicitud_presup pr, sie_cat_proyectos p, sie_proyecto_metas m where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND p.cpe_id_periodo = ".$periodo." AND pr.spr_destino='".$destino."' AND pr.spr_pre_estatus = 1 AND pr.spr_vobo_sa = 'Y' and pr.proyecto_id_meta = m.proyecto_id_meta AND m.cpr_id_proyecto = p.cpr_id_proyecto ORDER BY(pr.spr_fecha)";	
                }else if( $Estatus == "DR") {
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director, pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino, p.cpr_descripcion , p.cpr_id_proyecto,m.proyecto_desc_meta, m.proyecto_id_meta from sie_solicitud_presup pr, sie_cat_proyectos p, sie_proyecto_metas m where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND p.cpe_id_periodo = ".$periodo." AND pr.spr_destino='".$destino."' AND pr.spr_pre_estatus = 1 AND pr.spr_vobo_director = 'Y' and pr.proyecto_id_meta = m.proyecto_id_meta AND m.cpr_id_proyecto = p.cpr_id_proyecto ORDER BY(pr.spr_fecha)";	
                }else if( $Estatus == "CA" || $Estatus == "IP" ){
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director,  pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino,  p.cpr_descripcion , p.cpr_id_proyecto, m.proyecto_desc_meta, m.proyecto_id_meta from sie_solicitud_presup pr, sie_cat_proyectos p, sie_proyecto_metas m where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND p.cpe_id_periodo = ".$periodo." AND pr.spr_destino='".$destino."' AND pr.spr_pre_estatus = 1 AND pr.spr_estatus = '".$Estatus."' and pr.proyecto_id_meta = m.proyecto_id_meta AND m.cpr_id_proyecto = p.cpr_id_proyecto ORDER BY(pr.spr_fecha)";
                }else if( $Estatus == "NT"){
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director, pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino, p.cpr_descripcion , p.cpr_id_proyecto, m.proyecto_desc_meta, m.proyecto_id_meta from sie_solicitud_presup pr, sie_cat_proyectos p, sie_proyecto_metas m where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND p.cpe_id_periodo = ".$periodo." AND pr.spr_destino='".$destino."' AND pr.spr_pre_estatus = 1 AND pr.spr_vobo_st is null and pr.proyecto_id_meta = m.proyecto_id_meta AND m.cpr_id_proyecto = p.cpr_id_proyecto ORDER BY(pr.spr_fecha)";
                }else if( $Estatus == "NA"){
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director, pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino, p.cpr_descripcion , p.cpr_id_proyecto, m.proyecto_desc_meta, m.proyecto_id_meta from sie_solicitud_presup pr, sie_cat_proyectos p, sie_proyecto_metas m where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND p.cpe_id_periodo = ".$periodo." AND pr.spr_destino='".$destino."' AND pr.spr_pre_estatus = 1 and pr.spr_vobo_sa is null and pr.proyecto_id_meta = m.proyecto_id_meta AND m.cpr_id_proyecto = p.cpr_id_proyecto ORDER BY(pr.spr_fecha)";
                }else if( $Estatus == "ND"){
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director, pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino, p.cpr_descripcion , p.cpr_id_proyecto, m.proyecto_desc_meta, m.proyecto_id_meta from sie_solicitud_presup pr, sie_cat_proyectos p, sie_proyecto_metas m where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND p.cpe_id_periodo = ".$periodo." AND pr.spr_destino='".$destino."' AND pr.spr_pre_estatus = 1 and pr.spr_vobo_director is null and pr.proyecto_id_meta = m.proyecto_id_meta AND m.cpr_id_proyecto = p.cpr_id_proyecto ORDER BY(pr.spr_fecha)";
                }else if( $Estatus == "NO"){
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director,  pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino, p.cpr_descripcion , p.cpr_id_proyecto, m.proyecto_desc_meta, m.proyecto_id_meta from sie_solicitud_presup pr, sie_cat_proyectos p, sie_proyecto_metas m where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND p.cpe_id_periodo = ".$periodo." AND pr.spr_destino='".$destino."' AND pr.spr_pre_estatus = 1 AND ( pr.spr_vobo_sa = 'N' or pr.spr_vobo_st = 'N' or pr.spr_vobo_director = 'N'  ) and pr.proyecto_id_meta = m.proyecto_id_meta AND m.cpr_id_proyecto = p.cpr_id_proyecto ORDER BY(pr.spr_fecha)";
                }
                else if ( $Estatus == "DOS"){
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director,  pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino, p.cpr_descripcion , p.cpr_id_proyecto, m.proyecto_desc_meta, m.proyecto_id_meta from sie_solicitud_presup pr, sie_cat_proyectos p, sie_proyecto_metas m where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND p.cpe_id_periodo = ".$periodo." AND pr.spr_destino='".$destino."' AND pr.spr_pre_estatus = 1 AND" .
                                        "( (pr.spr_vobo_st = 'Y' and pr.spr_vobo_sa = 'Y') or (pr.spr_vobo_st = 'Y' and pr.spr_vobo_director = 'Y') or (pr.spr_vobo_sa = 'Y' and pr.spr_vobo_director = 'Y')   ) " .
                                        " and pr.proyecto_id_meta = m.proyecto_id_meta AND m.cpr_id_proyecto = p.cpr_id_proyecto ORDER BY(pr.spr_fecha)";
                }
            }
            else{

                if( $Estatus == "TD" ){
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director, pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino from sie_solicitud_presup pr where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND pr.cpe_id_periodo = ".$periodo." AND pr.spr_destino='".$destino."' AND pr.spr_pre_estatus = 1  ORDER BY(pr.spr_fecha)";
                }else if( $Estatus == "ST") {
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director, pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino from sie_solicitud_presup pr where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND pr.cpe_id_periodo = ".$periodo." AND pr.spr_destino='".$destino."' AND pr.spr_pre_estatus = 1 AND pr.spr_vobo_st = 'Y'  ORDER BY(pr.spr_fecha)";	
                }else if( $Estatus == "SA") {
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director, pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino from sie_solicitud_presup pr where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND pr.cpe_id_periodo = ".$periodo." AND pr.spr_destino='".$destino."' AND pr.spr_pre_estatus = 1 AND pr.spr_vobo_sa = 'Y'  ORDER BY(pr.spr_fecha)";	
                }else if( $Estatus == "DR") {
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director, pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino from sie_solicitud_presup pr where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND pr.cpe_id_periodo = ".$periodo." AND pr.spr_destino='".$destino."' AND pr.spr_pre_estatus = 1 AND pr.spr_vobo_director = 'Y'  ORDER BY(pr.spr_fecha)";	
                }else if( $Estatus == "CA" || $Estatus == "IP"){
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director, pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino  from sie_solicitud_presup pr where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND pr.cpe_id_periodo = ".$periodo." AND pr.spr_destino='".$destino."' AND pr.spr_pre_estatus = 1 AND pr.spr_estatus = '".$Estatus."'  ORDER BY(pr.spr_fecha)";
                }else if( $Estatus == "NT"){
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director, pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino  from sie_solicitud_presup pr where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND pr.cpe_id_periodo = ".$periodo." AND pr.spr_destino='".$destino."' AND pr.spr_pre_estatus = 1 AND pr.spr_vobo_st is null  ORDER BY(pr.spr_fecha)";
                }else if( $Estatus == "NA"){
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director, pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino  from sie_solicitud_presup pr where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND pr.cpe_id_periodo = ".$periodo." AND pr.spr_destino='".$destino."' AND pr.spr_pre_estatus = 1 and pr.spr_vobo_sa is null  ORDER BY(pr.spr_fecha)";
                }else if( $Estatus == "ND"){
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director, pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino  from sie_solicitud_presup pr where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND pr.cpe_id_periodo = ".$periodo." AND pr.spr_destino='".$destino."' AND pr.spr_pre_estatus = 1 and pr.spr_vobo_director is null  ORDER BY(pr.spr_fecha)";
                }else if( $Estatus == "NO"){
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director, pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino  from sie_solicitud_presup pr where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND pr.cpe_id_periodo = ".$periodo." AND pr.spr_destino='".$destino."' AND pr.spr_pre_estatus = 1 AND ( pr.spr_vobo_sa = 'N' or pr.spr_vobo_st = 'N' or pr.spr_vobo_director = 'N'  )  ORDER BY(pr.spr_fecha)";
                }
                else if ( $Estatus == "DOS"){
                        $query = "select pr.spr_estatus, pr.spr_vobo_st, pr.spr_vobo_sa, pr.spr_vobo_director,  pr.spr_id_solicitud , pr.spr_fecha, pr.spr_destino  from sie_solicitud_presup pr where pr.spr_fecha BETWEEN '".$inicio."' AND '".$fin."' AND pr.cpe_id_periodo = ".$periodo." AND pr.spr_destino='".$destino."' AND pr.spr_pre_estatus = 1 AND" .
                                        "( (pr.spr_vobo_st = 'Y' and pr.spr_vobo_sa = 'Y') or (pr.spr_vobo_st = 'Y' and pr.spr_vobo_director = 'Y') or (pr.spr_vobo_sa = 'Y' and pr.spr_vobo_director = 'Y')   ) " .
                                        "  ORDER BY(pr.spr_fecha)";
                }
            }
        }

	$rsMonto = null;
        $catalogo = new Catalogo();

        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
                $idPresupuesto= $rs["spr_id_solicitud"];
                $fecha = $rs["spr_fecha"];

                $idProyecto =null;
                $desc = null;
                $idMeta=null;
                $meta =null;

                if($destino == "PR"){

                    $idProyecto = $rs["cpr_id_proyecto"];
                    $desc = $rs["cpr_descripcion"];
                    $idMeta = $rs["proyecto_id_meta"];
                    $meta = $rs["proyecto_desc_meta"];
                }
                else{
                    $idProyecto = 0;
                    $desc = "";
                    $idMeta = 0;
                    $meta = "";
                }

            $destiny=$rs["spr_destino"];
            $estatus = "";
            $monto = 0;

                $query = "select SUM(spr_monto) as monto from sie_det_sol_presup where spr_estatus = 1 AND spr_id_solicitud  = ".$idPresupuesto;
                $resultMonto = $catalogo->obtenerLista($query);
                if(  $rsMonto = mysql_fetch_array($resultMonto) ){  $monto = $rsMonto["monto"]; }

                if( $rs["spr_estatus"] == "CA" ){ $estatus = "Capturado"; }
                else{

                        if( $rs["spr_vobo_st"]!= null && $rs["spr_vobo_st"] == "Y" ){ $estatus = $estatus." Con VoBo de ST"; }
                        else if($rs["spr_vobo_st"]!= null &&  $rs["spr_vobo_st"] == "N" ){ $estatus = $estatus." No Aceptado por ST"; }

                        if(  $rs["spr_vobo_sa"] != null && $rs["spr_vobo_sa"] == "Y"){ $estatus = $estatus."<br/> Con VoBo de SA"; }
                        else if( $rs["spr_vobo_sa"] != null && $rs["spr_vobo_sa"] == "N"){ $estatus = $estatus."<br/> No Aceptado por SA"; }

                        if( $rs["spr_vobo_director"] != null && $rs["spr_vobo_director"] == "Y" ){ $estatus = $estatus."<br/> Con VoBo de Director"; }
                        else if( $rs["spr_vobo_director"] != null && $rs["spr_vobo_director"] == "N"){ $estatus = $estatus."<br/> No Aceptado por Director"; }

                        if( $rs["spr_estatus"] == "IP"){ $estatus = $estatus."<br/> Impreso"; }
                }

                $elemento = new Presupuesto();
                $elemento->constructor2($idPresupuesto, $fecha, $idProyecto, $idMeta,$monto,$desc, $meta,$estatus, $destiny);
                array_push($lista, $elemento);		
	}
        return $lista;
    }
    
    public function obtieneElemento($idElemento) {
		
		
        $elemento = new Presupuesto();
        $query="SELECT pre.*, m.cpr_id_proyecto, m.PROYECTO_DESC_META, a.car_area FROM sie_solicitud_presup pre, sie_proyecto_metas m, sie_cat_proyectos p, cat_areas a WHERE pre.spr_pre_estatus = 1 AND pre.spr_id_solicitud = ".$idElemento." AND pre.proyecto_id_meta = m.proyecto_id_meta AND m.cpr_id_proyecto = p.cpr_id_proyecto AND p.car_id_area = a.car_id_area";
        $catalogo = new Catalogo();
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){

            $idPresupuesto = $rs[strtoupper("spr_id_solicitud")];
            $idProyecto = $rs["cpr_id_proyecto"];
            $idMeta = $rs[strtoupper("proyecto_id_meta")];
            $idEmpleado = $rs[strtoupper("cem_id_empleado")];
            $idProveedor = $rs[strtoupper("cpr_id_proveedor")];
            $fecha = $rs[strtoupper("spr_fecha")];
            $fechaVobo = $rs[strtoupper("spr_fecha_vobo_st")];
            $otro = $rs[strtoupper("spr_nombre_ch_otro")];
            $vobo = $rs[strtoupper("spr_vobo_st")];
            $obs = $rs[strtoupper("spr_observaciones")];
            $elabora = $rs[strtoupper("spr_elabora")];
            $area = $rs["car_area"];
            $estatus = "";
            $destino=$rs[strtoupper("spr_destino")];
            $idPrograma=$rs["CPI_ID_PROGRAMA"];
            $descMeta=$rs["PROYECTO_DESC_META"];

            //System.out.print("idProyecto".idProyecto);

            if($rs[strtoupper("spr_estatus")] == "CA" ){ $estatus = "Capturado"; }
            else{
                if( $rs[strtoupper("spr_vobo_st")]!= null && $rs[strtoupper("spr_vobo_st")] == "Y"){ $estatus = $estatus." Con VoBo de ST"; }
                else if($rs[strtoupper("spr_vobo_st")]!= null &&  $rs[strtoupper("spr_vobo_st")] == "N"){ $estatus = $estatus." No Aceptado por ST"; }

                if(  $rs[strtoupper("spr_vobo_sa")]!= null && $rs[strtoupper("spr_vobo_sa")] == "Y" ){ $estatus = $estatus.", Con VoBo de SA"; }
                else if( $rs[strtoupper("spr_vobo_sa")]!= null && $rs[strtoupper("spr_vobo_sa")] == "N"){ $estatus = $estatus.", No Aceptado por SA"; }

                if( $rs[strtoupper("spr_vobo_director")]!= null && $rs[strtoupper("spr_vobo_director")] == "Y"){ $estatus = $estatus.", Con VoBo de Director"; }
                else if( $rs[strtoupper("spr_vobo_director")]!= null && $rs[strtoupper("spr_vobo_director")] == "N"){ $estatus = $estatus.", No Aceptado por Director"; }

                if( $rs[strtoupper("spr_estatus")] == "IP"){ $estatus = $estatus.", Impreso"; }
            }

            $elemento->constructor1($idPresupuesto, $fecha, $idProyecto,$idMeta, $idEmpleado, $idProveedor, $otro, $vobo, $fechaVobo,$obs,$elabora,$estatus, $area,$destino,$idPrograma);
            $elemento->setNombreMeta($descMeta);			
        }
		
        return $elemento;		
    }
    
    public function obtieneElementoGb($idElemento) {
		
        $elemento = new Presupuesto();

        $query="SELECT pre.* FROM sie_solicitud_presup pre WHERE pre.spr_pre_estatus = 1 AND pre.spr_id_solicitud = ".$idElemento." ";
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $idPresupuesto = $rs[strtoupper("spr_id_solicitud")];
            $idProyecto = 0;
            $idMeta = 0;
            $idEmpleado = $rs[strtoupper("cem_id_empleado")];
            $idProveedor = $rs[strtoupper("cpr_id_proveedor")];
            $fecha = $rs[strtoupper("spr_fecha")];
            $fechaVobo = $rs[strtoupper("spr_fecha_vobo_st")];
            $otro = $rs[strtoupper("spr_nombre_ch_otro")];
            $vobo = $rs[strtoupper("spr_vobo_st")];
            $obs = $rs[strtoupper("spr_observaciones")];
            $elabora = $rs[strtoupper("spr_elabora")];
            $area = "";
            $estatus = "";
            $destino=$rs[strtoupper("spr_destino")];
            $idPeriodo = $rs[strtoupper("cpe_id_periodo")];
            $idPrograma=$rs[strtoupper("cpi_id_programa")];

            //System.out.print("idProyecto".idProyecto);

            if($rs[strtoupper("spr_estatus")] == "CA" ){ $estatus = "Capturado"; }
            else{
                if( $rs[strtoupper("spr_vobo_st")]!= null && $rs[strtoupper("spr_vobo_st")] == "Y"){ $estatus = $estatus." Con VoBo de ST"; }
                else if($rs[strtoupper("spr_vobo_st")]!= null &&  $rs[strtoupper("spr_vobo_st")] == "N"){ $estatus = $estatus." No Aceptado por ST"; }

                if(  $rs[strtoupper("spr_vobo_sa")]!= null && $rs[strtoupper("spr_vobo_sa")] == "Y" ){ $estatus = $estatus.", Con VoBo de SA"; }
                else if( $rs[strtoupper("spr_vobo_sa")]!= null && $rs[strtoupper("spr_vobo_sa")] == "N"){ $estatus = $estatus.", No Aceptado por SA"; }

                if( $rs[strtoupper("spr_vobo_director")]!= null && $rs[strtoupper("spr_vobo_director")] == "Y"){ $estatus = $estatus.", Con VoBo de Director"; }
                else if( $rs[strtoupper("spr_vobo_director")]!= null && $rs[strtoupper("spr_vobo_director")] == "N"){ $estatus = $estatus.", No Aceptado por Director"; }

                if( $rs[strtoupper("spr_estatus")] == "IP"){ $estatus = $estatus.", Impreso"; }
            }

            $elemento->constructor1($idPresupuesto, $fecha, $idProyecto,$idMeta, $idEmpleado, $idProveedor, $otro, $vobo, $fechaVobo,$obs,$elabora,$estatus, $area,$destino,$idPrograma);
            $elemento->setIdPeriodo($idPeriodo);			
        }
		
        return $elemento;
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $fecha = "";

        if($elemento->getFecha()!=null)
        {    $fecha= "'".date("Y-m-d",strtotime($elemento->getFecha()))."'"; }
        else
        {    $fecha = "null";  }

        $idEmpleado = "";
        $idProveedor = "";

        if($elemento->getIdEmpleado() == 0){ 
                $idEmpleado = "null";
        }else{ $idEmpleado = (String)($elemento->getIdEmpleado()); }
        if($elemento->getIdProveedor() == 0){
                $idProveedor = "null";
        }else{ $idProveedor = (String)( $elemento->getIdProveedor()); }

        $query="";
        if($elemento->getDestino() == "PR"){
            $query="INSERT INTO sie_solicitud_presup (SPR_FECHA,CEM_ID_EMPLEADO,CPR_ID_PROVEEDOR,SPR_NOMBRE_CH_OTRO,SPR_ESTATUS,SPR_VOBO_ST,SPR_FECHA_VOBO_ST,SPR_VOBO_SA,SPR_FECHA_VOBO_SA,SPR_OBSERVACIONES,PROYECTO_ID_META,SPR_PRE_ESTATUS,SPR_ELABORA,SPR_VOBO_DIRECTOR,SPR_FECHA_VOBO_DIRECTOR, SPR_DESTINO, CPI_ID_PROGRAMA) " .
                "VALUES(".$fecha.", ".$idEmpleado." , ".$idProveedor."," .
                " '".mb_strtoupper($elemento->getOtro(),'UTF-8')."' , 'CA', null, null, null ,null, '".mb_strtoupper($elemento->getObservaciones(),'UTF-8')."' , ".$elemento->getIdMeta()." , 1, '".mb_strtoupper($elemento->getElabora(),'UTF-8')."', null, null, '".$elemento->getDestino()."' , '".$elemento->getIdPrograma()."')";

        }
        else{
            $query="INSERT INTO sie_solicitud_presup (SPR_FECHA,CEM_ID_EMPLEADO,CPR_ID_PROVEEDOR,SPR_NOMBRE_CH_OTRO,SPR_ESTATUS,SPR_VOBO_ST,SPR_FECHA_VOBO_ST,SPR_VOBO_SA,SPR_FECHA_VOBO_SA,SPR_OBSERVACIONES,PROYECTO_ID_META,SPR_PRE_ESTATUS,SPR_ELABORA,SPR_VOBO_DIRECTOR,SPR_FECHA_VOBO_DIRECTOR, SPR_DESTINO, CPE_ID_PERIODO, CPI_ID_PROGRAMA) " .
                "VALUES(".$fecha.", ".$idEmpleado." , ".$idProveedor."," .
                " '".mb_strtoupper($elemento->getOtro(),'UTF-8')."' , 'CA', null, null, null ,null, '".mb_strtoupper($elemento->getObservaciones(),'UTF-8')."' , ".$elemento->getIdMeta()." , 1, '".mb_strtoupper($elemento->getElabora(),'UTF-8')."', null, null, '".$elemento->getDestino()."', '".$elemento->getIdPeriodo()."' , '".$elemento->getIdPrograma()."')";
        }
        $res=$con->obtenerLista($query);
        if($res=="1")
        {	return true; }
        else
        {	return false; }
    }
    
    public function actualizaElemento($elemento) {
		
        $con=new Catalogo();
        $fecha = "";

        if($elemento->getFecha()!=null)
        {    $fecha= "'".date("Y-m-d",strtotime($elemento->getFecha()))."'"; }
        else
        {    $fecha = "null"; }

        $idEmpleado = "";
        $idProveedor = "";

        if($elemento->getIdEmpleado() == 0){ 
                $idEmpleado = "null";
        }else{ $idEmpleado = (String)( $elemento->getIdEmpleado() ); }
        if($elemento->getIdProveedor() == 0){
                $idProveedor = "null";
        }else{ $idProveedor = (String)( $elemento->getIdProveedor() ); }


        $query="UPDATE sie_solicitud_presup  SET  spr_fecha = ".$fecha.", cem_id_empleado=".$idEmpleado." , cpr_id_proveedor =".$idProveedor.", spr_nombre_ch_otro = '".mb_strtoupper($elemento->getOtro(),'UTF-8') ."', spr_observaciones = '".mb_strtoupper($elemento->getObservaciones(),'UTF-8') ."' , proyecto_id_meta = ".$elemento->getIdMeta()." , spr_elabora = '".mb_strtoupper($elemento->getElabora(),'UTF-8')."', CPI_ID_PROGRAMA='".$elemento->getIdPrograma()."'  WHERE spr_id_solicitud = ".$elemento->getId() ." ";
        $res=$con->obtenerLista($query);
        
        if($res=="1"){
                $query = "UPDATE sie_cheques SET cpr_id_proyecto = ".$elemento->getIdProyecto()." WHERE spr_id_solicitud = ".$elemento->getId()." AND che_estatusborrado = 1";
                $con->obtenerLista($query);
                return true;
        } else
        {        return false; }	
    }
    
    public function eliminaElemento($idElemento){
		
        $res = 1;

        $query= "SELECT c.che_estatusborrado FROM sie_cheques c, sie_solicitud_presup p WHERE p.spr_id_solicitud = c.spr_id_solicitud AND p.spr_id_solicitud = ".$idElemento;

        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        if($rs = mysql_fetch_array($result)){
            $estatus = $rs[strtoupper("che_estatusborrado")];
                if( $estatus == 0){
                        $query = "UPDATE sie_solicitud_presup set  spr_pre_estatus = 0 WHERE spr_id_solicitud = ".$idElemento;
                        $res = $catalogo->obtenerLista($query);
                }
        }else{
                $query = "UPDATE sie_solicitud_presup set  spr_pre_estatus = 0 WHERE spr_id_solicitud = ".$idElemento;
                $res = $catalogo->obtenerLista($query);
        }

        if($res=="1")
        {       return true; }
        else
        {       return false; }
    }
    
    public function VoBoST($idElemento){
        $con=new Catalogo();

        $dia= date('d');
        $mes= date('m');
		
			
        $query= "UPDATE sie_solicitud_presup SET  spr_vobo_st = 'Y', spr_estatus = 'SB', spr_fecha_vobo_st = '".date('Y')."-".$mes."-".$dia."' WHERE spr_id_solicitud = ".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {        return true; }
        else
        {        return false; }
    }
    
    public function VoBoSA($idElemento){
		
        $con=new Catalogo();

        $dia= date('d');
        $mes= date('m');
			
        $query= "UPDATE sie_solicitud_presup SET  spr_vobo_sa = 'Y', spr_estatus = 'SB', spr_fecha_vobo_sa = '".date('Y')."-".$mes."-".$dia."' WHERE spr_id_solicitud = ".$idElemento;
        //System.out.println("query vo sa"+query);
        $res = $con->obtenerLista($query);

        if($res=="1")
        {        return true; }
        else
        {        return false; }
    }
    
    public function VoBoDirector($idElemento){
		
	$con=new Catalogo();

        $dia= date('d');
        $mes= date('m');
		
			
	$query= "UPDATE sie_solicitud_presup SET  spr_vobo_director = 'Y', spr_estatus = 'SB', spr_fecha_vobo_director = '".date('Y')."-".$mes."-".$dia."' WHERE spr_id_solicitud = ".$idElemento;
	$res = $con->obtenerLista($query);
		
	if($res=="1")
        {        return true; }
        else
        {        return false; }
    }
    
    public function NoAceptado($idElemento,$vobo){
	$con=new Catalogo();

        $dia= date('d');
        $mes= date('m');
        $query= null;	
			
        if($vobo == "dr"){
                $query = "UPDATE sie_solicitud_presup SET  spr_vobo_director = 'N', spr_estatus = 'SB', spr_fecha_vobo_director = '".date('Y')."-".$mes."-".$dia."' WHERE spr_id_solicitud = ".$idElemento;
        }else{
                $query= "UPDATE sie_solicitud_presup SET  spr_vobo_".$vobo." = 'N', spr_estatus = 'SB', spr_fecha_vobo_".$vobo." = '".date('Y')."-".$mes."-".$dia."' WHERE spr_id_solicitud = ".$idElemento;
        }
	$res = $con->obtenerLista($query);
		
	if($res=="1")
        {        return true; }
        else
        {        return false; }
    }
}
