<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Proveedor.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class ProveedorDaoJdbc {
    
    public function obtieneListado() {
		
        $lista= array();

        $query="SELECT * FROM sie_cat_proveedores WHERE cpr_estatus=1 ORDER BY cpr_beneficiario";
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cpr_id_proveedor")];
            $clave= $rs[strtoupper("cpr_clave")];
			$beneficiario= $rs[strtoupper("cpr_beneficiario")];
            $cuenta= $rs[strtoupper("cpr_cuenta")];
            $unidad_cta= $rs[strtoupper("cpr_unidad_cta")];
            $activo_ben= $rs[strtoupper("cpr_activo_ben")];
            $estatus_cta= $rs[strtoupper("cpr_estatus_cta")];
            $curp= $rs[strtoupper("cpr_curp")];
            $clave_banco= $rs[strtoupper("cpr_clave_banco")];
			$banco = $rs[strtoupper("cpr_banco")];
			$sicop = $rs[strtoupper("cpr_sicop")];
            $elemento = new Proveedor();
            $elemento->setAll($id,$clave,$beneficiario,$cuenta,$unidad_cta,$activo_ben,$estatus_cta,$curp,$clave_banco,$banco,$sicop);
            array_push($lista, $elemento);
        }	

        return $lista;
    }
    
    public function obtieneListadoCatalogos(){
        
        $lista= array();

        $query="SELECT * FROM sie_cat_proveedores  ORDER BY cpr_proveedor";
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cpr_id_proveedor")];
            $clave= $rs[strtoupper("cpr_clave")];
			$beneficiario= $rs[strtoupper("cpr_beneficiario")];
            $cuenta= $rs[strtoupper("cpr_cuenta")];
            $unidad_cta= $rs[strtoupper("cpr_unidad_cta")];
            $activo_ben= $rs[strtoupper("cpr_activo_ben")];
            $estatus_cta= $rs[strtoupper("cpr_estatus_cta")];
            $curp= $rs[strtoupper("cpr_curp")];
            $clave_banco= $rs[strtoupper("cpr_clave_banco")];
			$banco = $rs[strtoupper("cpr_banco")];
			$sicop = $rs[strtoupper("cpr_sicop")];

            $elemento = new Proveedor();
             $elemento->setAll($id,$clave,$beneficiario,$cuenta,$unidad_cta,$activo_ben,$estatus_cta,$curp,$clave_banco,$banco,$sicop);
            array_push($lista, $elemento);
        }	

        return $lista;
    }
    
    public function obtieneElemento($idElemento) {
		
	$elemento=new Proveedor();
		
	$query="SELECT * FROM sie_cat_proveedores WHERE cpr_id_proveedor=".$idElemento;
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cpr_id_proveedor")];
            $clave= $rs[strtoupper("cpr_clave")];
			$beneficiario= $rs[strtoupper("cpr_beneficiario")];
            $cuenta= $rs[strtoupper("cpr_cuenta")];
            $unidad_cta= $rs[strtoupper("cpr_unidad_cta")];
            $activo_ben= $rs[strtoupper("cpr_activo_ben")];
            $estatus_cta= $rs[strtoupper("cpr_estatus_cta")];
            $curp= $rs[strtoupper("cpr_curp")];
            $clave_banco= $rs[strtoupper("cpr_clave_banco")];
			$banco = $rs[strtoupper("cpr_banco")];
			$sicop = $rs[strtoupper("cpr_sicop")];

            $elemento = new Proveedor();
             $elemento->setAll($id,$clave,$beneficiario,$cuenta,$unidad_cta,$activo_ben,$estatus_cta,$curp,$clave_banco,$banco,$sicop);
        }
		
	return $elemento;	
    }
    
    public function obtieneElementoDesc($desc) {
        
        $elemento=new Proveedor();
		
	$query="SELECT * FROM sie_cat_proveedores WHERE cpr_proveedor='".$desc."'";
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cpr_id_proveedor")];
            $clave= $rs[strtoupper("cpr_clave")];
			$beneficiario= $rs[strtoupper("cpr_beneficiario")];
            $cuenta= $rs[strtoupper("cpr_cuenta")];
            $unidad_cta= $rs[strtoupper("cpr_unidad_cta")];
            $activo_ben= $rs[strtoupper("cpr_activo_ben")];
            $estatus_cta= $rs[strtoupper("cpr_estatus_cta")];
            $curp= $rs[strtoupper("cpr_curp")];
            $clave_banco= $rs[strtoupper("cpr_clave_banco")];
			$banco = $rs[strtoupper("cpr_banco")];
			$sicop = $rs[strtoupper("cpr_sicop")];

            $elemento = new Proveedor();
            $elemento->setAll($id,$clave,$beneficiario,$cuenta,$unidad_cta,$activo_ben,$estatus_cta,$curp,$clave_banco,$banco,$sicop);
        }
		
	return $elemento;
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $query="INSERT INTO sie_cat_proveedores(cpr_clave,cpr_beneficiario,cpr_cuenta,cpr_unidad_cta,cpr_activo_ben,cpr_estatus_cta,cpr_curp, cpr_clave_banco,cpr_banco,cpr_sicop)".
		" VALUES ('".mb_strtoupper($elemento->getClave(),'UTF-8')."', '".mb_strtoupper($elemento->getBeneficiario(),'UTF-8')."', '".mb_strtoupper($elemento->getCuenta(),'UTF-8')."', '".mb_strtoupper($elemento->getUnidadCTA(),'UTF-8')."' , " . $elemento->getActivoBEN() .", " . $elemento->getEstatusCTA() .", '".mb_strtoupper($elemento->getCurp(),'UTF-8')."','".$elemento->getClaveBanco()."','".$elemento->getBanco()."','".$elemento->getSicop()."' )";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
	$query="UPDATE sie_cat_proveedores set  cpr_clave='".mb_strtoupper($elemento->getClave(),'UTF-8')."', cpr_beneficiario='".mb_strtoupper($elemento->getBeneficiario(),'UTF-8')."', cpr_cuenta='".mb_strtoupper($elemento->getCuenta(),'UTF-8')."' , cpr_unidad_cta='".mb_strtoupper($elemento->getUnidadCTA(),'UTF-8')."' , cpr_activo_ben = ". $elemento->getActivoBEN() .", cpr_estatus_cta = ". $elemento->getEstatusCTA() .", cpr_cpr_curp='".mb_strtoupper($elemento->getCurp(),'UTF-8')."' , cpr_clave_banco='".mb_strtoupper($elemento->getClaveBanco(),'UTF-8')."' , cpr_banco='".mb_strtoupper($elemento->getBanco(),'UTF-8')."', cpr_sicop='".$elemento->getSicop()."' WHERE cpr_id_proveedor=".$elemento->getId();
	$res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_cat_proveedores set  cpr_estatus=0 WHERE cpr_id_proveedor=".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}
