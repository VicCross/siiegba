<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/RecMateriales.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class RecMaterialDaoJdbc {
    public function obtieneListado($id_carta) {
		
        $lista= array();
        $query="SELECT * FROM sie_carta_cons_det_rmat WHERE ccdm_estatus = 1 AND  cco_id_carta = ".(int)($id_carta)." ";

        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $idCarta= $rs[strtoupper("cco_id_carta")];
            $idRecurso = $rs[strtoupper("ccdm_id_recurso")];
            $orden = $rs[strtoupper("ccdm_ordenrecursomat")];
            $recurso = $rs[strtoupper("ccdm_recursomat")];
            $partida = $rs[strtoupper("ccdm_partida")];
            $unitario = $rs[strtoupper("ccdm_preciounitario")];
            $costo = $rs[strtoupper("ccdm_costototal")];
            $fecha = $rs[strtoupper("ccdm_fechaejercicio")];
            $factura = $rs[strtoupper("ccdm_facturaentregada")];
            $cheque = $rs[strtoupper("ccdm_chequeanombrede")];
            $num_cheque = $rs[strtoupper("ccdm_numcheque")];

            $elemento = new RecMateriales();
            $elemento->setAll($idRecurso,$idCarta,$orden,$recurso,$partida,$unitario,$costo,$fecha, $factura, $cheque, $num_cheque);
            array_push($lista, $elemento);
         }	
        return $lista;
    }
    
    public function obtieneElemento($idElemento) {
			
        $elemento = new RecMateriales();
        $query="SELECT * FROM sie_carta_cons_det_rmat WHERE  ccdm_estatus = 1 AND ccdm_id_recurso = ".$idElemento;
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $idCarta= $rs[strtoupper("cco_id_carta")];
            $idRecurso = $rs[strtoupper("ccdm_id_recurso")];
            $orden = $rs[strtoupper("ccdm_ordenrecursomat")];
            $recurso = $rs[strtoupper("ccdm_recursomat")];
            $partida = $rs[strtoupper("ccdm_partida")];
            $unitario = $rs[strtoupper("ccdm_preciounitario")];
            $costo = $rs[strtoupper("ccdm_costototal")];
            $fecha = $rs[strtoupper("ccdm_fechaejercicio")];
            $factura = $rs[strtoupper("ccdm_facturaentregada")];
            $cheque = $rs[strtoupper("ccdm_chequeanombrede")];
            $num_cheque = $rs[strtoupper("ccdm_numcheque")];

            $elemento = new RecMateriales();
            $elemento->setAll($idRecurso,$idCarta,$orden,$recurso,$partida,$unitario,$costo,$fecha, $factura, $cheque, $num_cheque);
         }	
        return $elemento;
    }
    
    public function guardaElemento($elemento) {
		
        $fecha = "";
		
        if($elemento->getFecha()!=null)
        {        $fecha= "'".date("Y-m-d",strtotime($elemento->getFecha()))."'";   }
        else
        {        $fecha = "null";   }
        $con=new Catalogo();
        $query="INSERT INTO sie_carta_cons_det_rmat(CCO_ID_CARTA,CCDM_ORDENRECURSOMAT,CCDM_RECURSOMAT,CCDM_PARTIDA,CCDM_PRECIOUNITARIO,CCDM_COSTOTOTAL,CCDM_FECHAEJERCICIO,CCDM_FACTURAENTREGADA,CCDM_CHEQUEANOMBREDE,CCDM_NUMCHEQUE,CCDM_ESTATUS) VALUES (".$elemento->getIdCarta().", ". $elemento->getNum_orden() .", '". mb_strtoupper($elemento->getRecurso(),'UTF-8') ."','".
		$elemento->getPartida()."' ,". $elemento->getUnitario() .",". $elemento->getCosto() .", ". $fecha .", '". $elemento->getFacturaEntregada() ."' , '". mb_strtoupper($elemento->getNombreCheque(),'UTF-8') ."'," .
                " '". $elemento->getNo_cheque() ."' , 1)";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
        $fecha = "";
		
        if($elemento->getFecha()!=null)
        {        $fecha= "'".date("Y-m-d",strtotime($elemento->getFecha()))."'";   }
        else
        {        $fecha = "null";   }
        
	$con=new Catalogo();
	$query="UPDATE sie_carta_cons_det_rmat  SET  ccdm_ordenrecursomat = ".$elemento->getNum_orden().", ccdm_recursomat = '".mb_strtoupper($elemento->getRecurso(),'UTF-8').  "', ccdm_partida='" . $elemento->getPartida() ."'" .
            ",ccdm_preciounitario =".$elemento->getUnitario().", ccdm_costototal = ". $elemento->getCosto() ." , ccdm_fechaejercicio = " . $fecha . ", ccdm_facturaentregada = '". $elemento->getFacturaEntregada() ."'," .
            " ccdm_chequeanombrede = '". mb_strtoupper($elemento->getNombreCheque(),'UTF-8') ."', ccdm_numcheque = '". $elemento->getNo_cheque() ."' WHERE ccdm_id_recurso = ". $elemento->getId() ." ";
        $res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_carta_cons_det_rmat set  ccdm_estatus = 0 WHERE ccdm_id_recurso = ".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}
