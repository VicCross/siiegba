<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Capitulo.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class CapituloDaoJdbc {
    
    public function obtieneListado() {

        $lista= array();

        $query="SELECT * FROM sie_cat_capitulos WHERE cca_estatus=1 ORDER BY cca_capitulo";

        $con=new Catalogo();
        $result = $con->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cca_id_capitulo")];
            $capitulo= $rs[strtoupper("cca_capitulo")];
            $descripcion= $rs[strtoupper("cca_descripcion")];

            $elemento = new Capitulo($id, $descripcion, $capitulo);
            array_push($lista, $elemento);
        }	
        return $lista;
    }
    
    public function obtieneElemento($idElemento) {
		
	$query="SELECT * FROM sie_cat_capitulos WHERE cca_id_capitulo=".$idElemento;

        $con=new Catalogo();
        $result = $con->obtenerLista($query);
			
        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cca_id_capitulo")];
            $capitulo= $rs[strtoupper("cca_capitulo")];
            $descripcion= $rs[strtoupper("cca_descripcion")];
				
            $elemento = new Capitulo($id, $descripcion, $capitulo);
	}
		
        return $elemento;
		
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $query="INSERT INTO sie_cat_capitulos(cca_capitulo,cca_descripcion) VALUES ('".$elemento->getCapitulo()."', '".mb_strtoupper($elemento->getDescripcion(),'UTF-8')."')";
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }

    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
	$query="UPDATE sie_cat_capitulos set  cca_descripcion='".mb_strtoupper($elemento->getDescripcion(),'UTF-8')."', cca_capitulo='".$elemento->getCapitulo()."' WHERE cca_id_capitulo=".$elemento->getId();
	$res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_cat_capitulos set  cca_estatus=0 WHERE cca_id_capitulo=".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
	}
}
