<?php
	session_start();
	include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
	include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ExposicionesTemporales.class.php");
	include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");
	
	class ExposicionesTemporalesDaoJdbc{
		
		public function getListado(){
			$lista = array();
			$catalogo = new Catalogo();
			$query = "SELECT * FROM sie_cat_expo_temp ORDER BY id_Subproyecto";
			$resul = $catalogo->obtenerLista($query);
			while($rs = mysql_fetch_array($resul)){
				$elemento = new ExposicionesTemporales();
				$elemento->setAll($rs[0], $rs[1], $rs[2], $rs[3], $rs[4], $rs[5], $rs[6], $rs[7], $rs[8], $rs[9], $rs[10],$rs[11],$rs[12],$rs[13],$rs[14],$rs[15]);
				array_push($lista, $elemento);
			}
			
			return $lista;
		}
		
		public function getElemento($id){
			$catalogo = new Catalogo();
			$elemento = new ExposicionesTemporales();
			$query = "SELECT * FROM sie_cat_expo_temp WHERE id_Subproyecto = " . $id . " ORDER BY id_Subproyecto";	
			$result = $catalogo->obtenerLista($query);
			if($rs = mysql_fetch_array($result)){
			$elemento->setAll($rs[0], $rs[1], $rs[2], $rs[3], $rs[4], $rs[5], $rs[6], $rs[7], $rs[8], $rs[9], $rs[10],$rs[11],$rs[12],$rs[13],$rs[14],$rs[15]);
			}
			return $elemento;
		}
		
		public function actualizaSaldos($elemento){
			$catalogo = new Catalogo();
			$update = "UPDATE sie_cat_expo_temp SET saldo = " . $elemento->getSaldo() . ", saldoAnterior = " . $elemento->getSaldoAnterior() . " WHERE id_Subproyecto = " . $elemento->getIdExposicionT();
			$res = $catalogo->obtenerLista($update);
			if($res == "1") return true;
			else return false;
		}
		
		
		 public function obtieneElemento($idElemento){
        
        $elemento = new ExposicionesTemporales();
        
        $query="SELECT * FROM sie_cat_expo_temp WHERE id_Subproyecto=".$idElemento;
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

       if($rs = mysql_fetch_array($result)){
				$elemento->setAll($rs[0], $rs[1], $rs[2], $rs[3], $rs[4], $rs[5], $rs[6], $rs[7], $rs[8], $rs[9], $rs[10],$rs[11],$rs[12],$rs[13],$rs[14],$rs[15]);
			 $elemento= new ExposicionesTemporales();
			}
           
          		
	return $elemento;
    }
		
		public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $query="INSERT INTO sie_cat_expo_temp(nombre,fecha_inicio,fecha_fin,titulo_final,periodo,saldo,saldoAnterior) " .
	       "VALUES ('".mb_strtoupper($elemento->getNombre(),'UTF-8')."','".mb_strtoupper($elemento->getFechaI(),'UTF-8')."','".mb_strtoupper($elemento->getFechaF(),'UTF-8')."',null ,".mb_strtoupper($elemento->getPeriodo(),'UTF-8').", null, null)";
        $res=$con->obtenerLista($query);
		
        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
	
	public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
	$query="UPDATE sie_cat_temp set nombre='".mb_strtoupper($elemento->getNombre(),'UTF-8')."', fecha_inicio=".mb_strtoupper($elemento->getFechaI(),'UTF-8').", fecha_fin=".mb_strtoupper($elemento->getFechaF(),'UTF-8').", titulo_final=".mb_strtoupper($elemento->getTituloFinal(),'UTF-8').", periodo=".mb_strtoupper($elemento->getPeriodo(),'UTF-8').",saldo=".$elemento->getSaldo().", saldoAnterior=".mb_strtoupper($elemento->getSaldoAnterior(),'UTF-8')." " .
				"WHERE id_Subproyecto=".$elemento->getIdExposicionT();
	$res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
	
	public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="DELETE FROM sie_cat_expo_temp WHERE  id_Subproyecto=".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
	
	}
?>