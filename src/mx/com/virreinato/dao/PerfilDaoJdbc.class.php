<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Perfil.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class PerfilDaoJdbc {
    
    public function obtieneListado() {
		
        $lista= array();

        $query="SELECT * FROM sie_cat_perfiles  ORDER BY cper_descripcion";
		
        $catalogo = new Catalogo();       
        $result = $catalogo->obtenerLista($query);
			
	while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cper_id_perfil")];
            $descripcion= $rs[strtoupper("cper_descripcion")];

            $elemento= new Perfil();
            $elemento->setAll($id, $descripcion);
            array_push($lista, $elemento);
        }	
		
        return $lista;
    }
    
    public function obtieneElemento($idElemento) {
		
		
	$elemento=new Perfil();
	$query="SELECT * FROM sie_cat_perfiles WHERE cper_id_perfil=".$idElemento;

			
        $catalogo = new Catalogo();       
        $result = $catalogo->obtenerLista($query);
			
	while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("cper_id_perfil")];
            $descripcion= $rs[strtoupper("cper_descripcion")];

            $elemento= new Perfil();
            $elemento->setAll($id, $descripcion);
				
	}
		
	return $elemento;
		
    }
}
