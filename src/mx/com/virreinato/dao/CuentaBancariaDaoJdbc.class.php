<?php
if(session_id() == '') {
    session_start();
}

include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CuentaBancaria.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Banco.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class CuentaBancariaDaoJdbc {
    
    public function obtieneListado() {
		
        $lista= array();

        $query="SELECT * FROM sie_cuentabancaria C, sie_cat_bancos B where C.cba_id_banco=B.cba_id_banco AND C.ccb_estatus=1 ORDER BY cba_descripcion ";
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("ccb_id_cuentabancaria")];
            $cba_id_banco= $rs[strtoupper("cba_id_banco")];
            $cba_descripcion= $rs[strtoupper("cba_descripcion")];
            $cba_observaciones= $rs[strtoupper("cba_observaciones")];
            $ccb_numerocuenta= $rs[strtoupper("ccb_numerocuenta")];
            $ccb_tipocuenta= $rs[strtoupper("ccb_tipocuenta")];
            $ccb_clabe= $rs[strtoupper("ccb_clabe")];
            $ccb_sucursal= $rs[strtoupper("ccb_sucursal")];
            $ccb_ejecutivocuenta= $rs[strtoupper("ccb_ejecutivocuenta")];
            $ccb_ejecutivotel= $rs[strtoupper("ccb_ejecutivotel")];
            $ccb_ejecutivocorreo= $rs[strtoupper("ccb_ejecutivocorreo")];
            $ccb_descripcion= $rs[strtoupper("ccb_descripcion")];

            $banco = new Banco();
            $banco->setAll($cba_id_banco,$cba_descripcion,$cba_observaciones);

            $elemento = new CuentaBancaria();
            $elemento->setAll($id,$banco,$ccb_numerocuenta,$ccb_tipocuenta,$ccb_clabe,$ccb_sucursal,$ccb_ejecutivocuenta,$ccb_ejecutivotel,$ccb_ejecutivocorreo,$ccb_descripcion);
            array_push($lista, $elemento);
        }	
	return $lista;
    }
    
    public function obtieneElemento($idElemento){
        
        $elemento = new CuentaBancaria();
        
        $query="SELECT * FROM sie_cuentabancaria C, sie_cat_bancos B where C.cba_id_banco=B.cba_id_banco AND C.ccb_estatus=1 ORDER BY cba_descripcion ";
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("ccb_id_cuentabancaria")];
            $cba_id_banco= $rs[strtoupper("cba_id_banco")];
            $cba_descripcion= $rs[strtoupper("cba_descripcion")];
            $cba_observaciones= $rs[strtoupper("cba_observaciones")];
            $ccb_numerocuenta= $rs[strtoupper("ccb_numerocuenta")];
            $ccb_tipocuenta= $rs[strtoupper("ccb_tipocuenta")];
            $ccb_clabe= $rs[strtoupper("ccb_clabe")];
            $ccb_sucursal= $rs[strtoupper("ccb_sucursal")];
            $ccb_ejecutivocuenta= $rs[strtoupper("ccb_ejecutivocuenta")];
            $ccb_ejecutivotel= $rs[strtoupper("ccb_ejecutivotel")];
            $ccb_ejecutivocorreo= $rs[strtoupper("ccb_ejecutivocorreo")];
            $ccb_descripcion= $rs[strtoupper("ccb_descripcion")];

            $banco = new Banco();
            $banco->setAll($cba_id_banco,$cba_descripcion,$cba_observaciones);

            $elemento = new CuentaBancaria();
            $elemento->setAll($id,$banco,$ccb_numerocuenta,$ccb_tipocuenta,$ccb_clabe,$ccb_sucursal,$ccb_ejecutivocuenta,$ccb_ejecutivotel,$ccb_ejecutivocorreo,$ccb_descripcion);
        }	
	return $elemento;
    } 
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $banco = $elemento->getBanco();
        $query="INSERT INTO sie_cuentabancaria(cba_id_banco, ccb_numerocuenta, ccb_tipocuenta, ccb_clabe, ccb_sucursal, ccb_ejecutivocuenta, ccb_ejecutivotel, ccb_ejecutivocorreo, ccb_descripcion)".
		" VALUES ( '".$banco->getId()."', '".mb_strtoupper($elemento->getNumeroCuenta(),'UTF-8')."', '".mb_strtoupper($elemento->getTipoCuenta(),'UTF-8')."', '".mb_strtoupper($elemento->getClabe(),'UTF-8')."', '".mb_strtoupper($elemento->getSucursal(),'UTF-8')."' , '".mb_strtoupper($elemento->getEjecutivoCuenta(),'UTF-8')."' , '".mb_strtoupper($elemento->getEjecutivoTel(),'UTF-8')."', '".mb_strtoupper($elemento->getEjecutivoCorreo(),'UTF-8')."', '".mb_strtoupper($elemento->getDescripcion(),'UTF-8')."')";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
        $banco = $elemento->getBanco();
	$query="UPDATE sie_cuentabancaria set  cba_id_banco='".$banco->getId()."', ccb_numerocuenta='".mb_strtoupper($elemento->getNumeroCuenta(),'UTF-8')."', ccb_tipocuenta='".mb_strtoupper($elemento->getTipoCuenta(),'UTF-8')."' , ccb_clabe='".mb_strtoupper($elemento->getClabe(),'UTF-8')."' , ccb_sucursal='".mb_strtoupper($elemento->getSucursal(),'UTF-8')."' , ccb_ejecutivocuenta='".mb_strtoupper($elemento->getEjecutivoCuenta(),'UTF-8')."' , ccb_ejecutivotel='".mb_strtoupper($elemento->getEjecutivoTel(),'UTF-8')."', ccb_ejecutivocorreo='".mb_strtoupper($elemento->getEjecutivoCorreo(),'UTF-8')."', ccb_descripcion='".mb_strtoupper($elemento->getDescripcion(),'UTF-8')."'  WHERE ccb_id_cuentabancaria=".$elemento->getId();
	$res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_cuentabancaria set  ccb_estatus=0 WHERE ccb_id_cuentabancaria=".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}

