<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Ministra.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatProyecto.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/dao/CatProyectoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class MinistraDaoJdbc {
    
    public function obtieneListado( $inicio, $fin, $periodo ) {
		
	$lista= array();
        $inicio2 = date("Y-m-d",strtotime($inicio));
        $fin2 = date("Y-m-d",strtotime($fin));
        if($periodo == null){
            $periodo = "null";
        }
		
        $query="SELECT * FROM sie_ministraciones m, sie_cat_periodos p,  sie_cuentabancaria cb"
            ." WHERE  m.cpe_id_periodo = ".$periodo." AND  m.min_fecha BETWEEN '".$inicio2."' AND '".$fin2."' AND  " .
            "m.cpe_id_periodo=p.cpe_id_periodo AND m.ccb_id_cuentabancaria=cb.ccb_id_cuentabancaria"
            ." AND m.min_formacarga>0 and  p.cpe_estatus=1  "
            ." ORDER BY m.MIN_FECHA desc, m.min_numoperacion";

        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id = $rs[strtoupper("min_id_ministracion")];
            $monto = $rs[strtoupper("min_monto")];
            $fecha = $rs[strtoupper("min_fecha")];
            $descripcion = $rs[strtoupper("min_descripcion")];
            $destino = $rs[strtoupper("min_destino")];
            $observaciones = $rs[strtoupper("min_observaciones")];
            $formaCarga = $rs[strtoupper("min_formacarga")];
            $idCuentaBancaria = $rs[strtoupper("ccb_id_cuentabancaria")];
            $idPeriodo = $rs[strtoupper("cpe_id_periodo")];
            $idProyecto = $rs[strtoupper("cpr_id_proyecto")];
            $numOperacion = $rs[strtoupper("min_numoperacion")];
            $desPeriodo = $rs[strtoupper("cpe_periodo")];
            $desProyecto = "";
            $desCuentaBancaria = $rs[strtoupper("ccb_numerocuenta")];
            $idSolicitud = $rs[strtoupper("sfo_id_solfondos")];

            if($idProyecto!=null && $idProyecto!=0){
                $pdao=new CatProyectoDaoJdbc();
                $proy= $pdao->obtieneElemento((String)($idProyecto));
                $desProyecto =$proy->getDescripcion();
            }
	
        $elemento = new Ministra();    
        $elemento->setAll($id,$monto, $fecha, $descripcion,$destino,$observaciones,$formaCarga,$idCuentaBancaria, $idPeriodo,$idProyecto,$numOperacion,$desPeriodo,$desProyecto,$desCuentaBancaria,$idSolicitud);
        array_push($lista, $elemento);
        }	
        return $lista;
    }
    
    public function obtieneListado2() {
		
	$lista= array();
		
	$query="SELECT * FROM sie_ministraciones m, sie_cat_periodos p,  sie_cuentabancaria cb"
            ." WHERE m.cpe_id_periodo=p.cpe_id_periodo AND m.ccb_id_cuentabancaria=cb.ccb_id_cuentabancaria"
            ." AND m.min_formacarga>0"
            ." ORDER BY m.MIN_FECHA desc, m.min_numoperacion";
		
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id = $rs[strtoupper("min_id_ministracion")];
            $monto = $rs[strtoupper("min_monto")];
            $fecha = $rs[strtoupper("min_fecha")];
            $descripcion = $rs[strtoupper("min_descripcion")];
            $destino = $rs[strtoupper("min_destino")];
            $observaciones = $rs[strtoupper("min_observaciones")];
            $formaCarga = $rs[strtoupper("min_formacarga")];
            $idCuentaBancaria = $rs[strtoupper("ccb_id_cuentabancaria")];
            $idPeriodo = $rs[strtoupper("cpe_id_periodo")];
            $idProyecto = $rs[strtoupper("cpr_id_proyecto")];
            $numOperacion = $rs[strtoupper("min_numoperacion")];
            $desPeriodo = $rs[strtoupper("cpe_periodo")];
            $desProyecto = "";
            $desCuentaBancaria = $rs[strtoupper("ccb_numerocuenta")];
            $idSolicitud = $rs[strtoupper("sfo_id_solfondos")];

            if($idProyecto!=null && $idProyecto!=0){
                $pdao=new CatProyectoDaoJdbc();
                $proy= $pdao->obtieneElemento((String)($idProyecto));
                $desProyecto =$proy->getDescripcion();
            }
	
        $elemento = new Ministra();    
        $elemento->setAll($id,$monto, $fecha, $descripcion,$destino,$observaciones,$formaCarga,$idCuentaBancaria, $idPeriodo,$idProyecto,$numOperacion,$desPeriodo,$desProyecto,$desCuentaBancaria,$idSolicitud);
        array_push($lista, $elemento);
        }	
        return $lista;
    }
    
    public function obtieneListadoDestino($destino ){
		
	$lista= array();
		
	$query="SELECT m.*, p.* FROM sie_ministraciones m, sie_cat_periodos p "
            ." WHERE m.cpe_id_periodo=p.cpe_id_periodo  "
            ." AND m.min_formacarga>0 AND m.min_destino='".$destino."'  "
            ." ORDER BY m.min_descripcion, m.min_numoperacion";
		
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id = $rs[strtoupper("min_id_ministracion")];
            $monto = $rs[strtoupper("min_monto")];
            $fecha = $rs[strtoupper("min_fecha")];
            $descripcion = $rs[strtoupper("min_descripcion")];
            $destino = $rs[strtoupper("min_destino")];
            $observaciones = $rs[strtoupper("min_observaciones")];
            $formaCarga = $rs[strtoupper("min_formacarga")];
            $idCuentaBancaria = $rs[strtoupper("ccb_id_cuentabancaria")];
            $idPeriodo = $rs[strtoupper("cpe_id_periodo")];
            $idProyecto = $rs[strtoupper("cpr_id_proyecto")];
            $numOperacion = $rs[strtoupper("min_numoperacion")];
            $desPeriodo = $rs[strtoupper("cpe_periodo")];
            $desProyecto = "";
            $desCuentaBancaria = "";
            $idSolicitud = $rs[strtoupper("sfo_id_solfondos")];

            if($idProyecto!=null && $idProyecto!=0){
                $pdao=new CatProyectoDaoJdbc();
                $proy= $pdao->obtieneElemento((String)($idProyecto));
                $desProyecto =$proy->getDescripcion();
            }
	
        $elemento = new Ministra();    
        $elemento->setAll($id,$monto, $fecha, $descripcion,$destino,$observaciones,$formaCarga,$idCuentaBancaria, $idPeriodo,$idProyecto,$numOperacion,$desPeriodo,$desProyecto,$desCuentaBancaria,$idSolicitud);
        array_push($lista, $elemento);
        }	
        return $lista;
    }
    
    public function obtieneListadoDestinoPer( $destino, $periodo ){
		
	$lista= array();
		
	$query="SELECT * FROM sie_ministraciones m, sie_cat_periodos p "
            ." WHERE m.cpe_id_periodo=p.cpe_id_periodo  "
            ." AND m.min_formacarga>0 AND m.min_destino='".$destino."' and p.cpe_periodo='".$periodo."' "
            ." ORDER BY m.min_descripcion, m.min_numoperacion";

	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id = $rs[strtoupper("min_id_ministracion")];
            $monto = $rs[strtoupper("min_monto")];
            $fecha = $rs[strtoupper("min_fecha")];
            $descripcion = $rs[strtoupper("min_descripcion")];
            $destino = $rs[strtoupper("min_destino")];
            $observaciones = $rs[strtoupper("min_observaciones")];
            $formaCarga = $rs[strtoupper("min_formacarga")];
            $idCuentaBancaria = $rs[strtoupper("ccb_id_cuentabancaria")];
            $idPeriodo = $rs[strtoupper("cpe_id_periodo")];
            $idProyecto = $rs[strtoupper("cpr_id_proyecto")];
            $numOperacion = $rs[strtoupper("min_numoperacion")];
            $desPeriodo = $rs[strtoupper("cpe_periodo")];
            $desProyecto = "";
            $desCuentaBancaria = "";
            $idSolicitud = $rs[strtoupper("sfo_id_solfondos")];

            if($idProyecto!=null && $idProyecto!=0){
                $pdao=new CatProyectoDaoJdbc();
                $proy= $pdao->obtieneElemento((String)($idProyecto));
                $desProyecto =$proy->getDescripcion();
            }
	
        $elemento = new Ministra();    
        $elemento->setAll($id,$monto, $fecha, $descripcion,$destino,$observaciones,$formaCarga,$idCuentaBancaria, $idPeriodo,$idProyecto,$numOperacion,$desPeriodo,$desProyecto,$desCuentaBancaria,$idSolicitud);
        array_push($lista, $elemento);
        }	
        return $lista;
    }
    
    public function obtieneElemento($idElemento) {
		
		
	$elemento=new Ministra();
		
	$query="SELECT * FROM sie_ministraciones m, sie_cat_periodos p, sie_cuentabancaria cb"
            ." WHERE m.cpe_id_periodo=p.cpe_id_periodo AND  m.ccb_id_cuentabancaria=cb.ccb_id_cuentabancaria"
            ." AND m.min_id_ministracion=".$idElemento;
		
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id = $rs[strtoupper("min_id_ministracion")];
            $monto = $rs[strtoupper("min_monto")];
            $fecha = $rs[strtoupper("min_fecha")];
            $descripcion = $rs[strtoupper("min_descripcion")];
            $destino = $rs[strtoupper("min_destino")];
            $observaciones = $rs[strtoupper("min_observaciones")];
            $formaCarga = $rs[strtoupper("min_formacarga")];
            $idCuentaBancaria = $rs[strtoupper("ccb_id_cuentabancaria")];
            $idPeriodo = $rs[strtoupper("cpe_id_periodo")];
            $idProyecto = $rs[strtoupper("cpr_id_proyecto")];
            $numOperacion = $rs[strtoupper("min_numoperacion")];
            $desPeriodo = $rs[strtoupper("cpe_periodo")];
            $desProyecto = "";
            $desCuentaBancaria = $rs[strtoupper("ccb_numerocuenta")];
            $idSolicitud = $rs[strtoupper("sfo_id_solfondos")];

            if($idProyecto!=null && $idProyecto!=0){
                $pdao=new CatProyectoDaoJdbc();
                $proy= $pdao->obtieneElemento((String)($idProyecto));
                $desProyecto =$proy->getDescripcion();
            }
    
        $elemento->setAll($id,$monto, $fecha, $descripcion,$destino,$observaciones,$formaCarga,$idCuentaBancaria, $idPeriodo,$idProyecto,$numOperacion,$desPeriodo,$desProyecto,$desCuentaBancaria,$idSolicitud);
        }	
        return $elemento;
		
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $query="INSERT INTO sie_ministraciones(min_monto,min_fecha,min_descripcion,min_destino,min_observaciones,min_formaCarga,ccb_id_cuentabancaria,cpe_id_periodo,cpr_id_proyecto,min_numoperacion,sfo_id_solfondos) VALUES ("
				.$elemento->getMonto().", '"
				.date("Y-m-d",strtotime($elemento->getFecha()))."', '"
				.mb_strtoupper($elemento->getDescripcion(),'UTF-8')."', '"
				.$elemento->getDestino()."', '"
				.mb_strtoupper($elemento->getObservaciones(),'UTF-8')."', '"
				.$elemento->getFormaCarga()."', "
				.$elemento->getIdCuentaBancaria().", "
				.$elemento->getIdPeriodo().", "
				.$elemento->getIdProyecto().", '"
				.$elemento->getNumOperacion()."',".$elemento->getIdSolicitud().")";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
	$query="UPDATE sie_ministraciones set "
            ." min_monto=".$elemento->getMonto().", "
            ." min_fecha='".date("Y-m-d",strtotime($elemento->getFecha()))."', "
            ." min_descripcion='".mb_strtoupper($elemento->getDescripcion(),'UTF-8')."', "
            ." min_destino='".$elemento->getDestino()."', "
            ." min_observaciones='".mb_strtoupper($elemento->getObservaciones(),'UTF-8')."', "
            ." min_formaCarga='".$elemento->getFormaCarga()."', "
            ." ccb_id_cuentabancaria=".$elemento->getIdCuentaBancaria().", "
            ." cpe_id_periodo=".$elemento->getIdPeriodo().", "
            ." cpr_id_proyecto=".$elemento->getIdProyecto().", "
            ." min_numoperacion='".$elemento->getNumOperacion()."', "
            ." sfo_id_solfondos= ".$elemento->getIdSolicitud()." "
            ." WHERE min_id_ministracion=".$elemento->getId();
	$res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_ministraciones set  min_formacarga=0 WHERE min_id_ministracion=".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}
