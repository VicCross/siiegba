<?php
	session_start();
	include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Documentacion.class.php");
	include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");
	
	class DocumentacionDaoJdbc
	{
		public function obtieneListado($inicio, $fin)
		{
			$catalogo = new Catalogo();
			$lista = array();
			$select = "SELECT * FROM sie_documentacion WHERE fecha BETWEEN '".date("Y-m-d",strtotime($inicio))."' AND '".date("Y-m-d",strtotime($fin))."' ORDER BY id_Documento";
			$res = $catalogo->obtenerLista($select);
			while($rs = mysql_fetch_array($res)){
				$elemento = new Documentacion();
				$elemento->setAll($rs[0], $rs[1], $rs[2], $rs[3], $rs[4], $rs[5], $rs[6], $rs[7]);
				array_push($lista, $elemento);
			}
			return $lista;			
		}
		
		public function guardaElemento($elemento){
			$catalogo = new Catalogo();
			$hayArea = ""; $area = "";
			$hayProy = ""; $proyecto = "";
			if($elemento->getIdArea() != 0){
				$hayArea = ", idArea";
				$area = "," . $elemento->getIdArea();	
			}if($elemento->getIdProyecto() != 0){
				$hayProy = ", idProyecto";
				$proyecto = ", " . $elemento->getIdProyecto();	
			}
			$insert = "INSERT INTO sie_documentacion (idEmpleado, fecha, noVersion, archivo, observaciones" . $hayProy . $hayArea .") VALUES (" . $elemento->getIdEmpleado() . ", curdate(), " . $elemento->getVersion() . ", '" . $elemento->getArchivo() . "', '" . utf8_decode($elemento->getObservaciones()) ."'" . $proyecto . $area .")";
			$res = $catalogo->obtenerLista($insert);
			if($res == "1") return true;
			else return false;
		}
		
		public function obtieneElemento($id){
			$catalogo = new Catalogo();
			$select = "SELECT * FROM sie_documentacion WHERE id_Documento = " . $id;
			$res = $catalogo->obtenerLista($select);
			$elemento = new Documentacion();
			if($rs = mysql_fetch_array($res)){
				$elemento->setAll($rs[0], $rs[1], $rs[2], $rs[3], $rs[4], $rs[5], $rs[6], $rs[7]);	
			}
			return $elemento;
		}
		
		public function eliminaElemento($elemento){
			$catalogo = new Catalogo();
			$delete = "DELETE FROM sie_documentacion WHERE id_Documento = " . $elemento->getId();
			$res = $catalogo->obtenerLista($delete);
			if($res == "1") return true;
			else return false;
		}
	}
?>