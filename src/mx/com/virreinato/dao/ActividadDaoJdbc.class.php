<?php
if(session_id() == '') {
    session_start();
}

include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Actividad.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class ActividadDaoJdbc {
    
    public function obtieneActividades(){
		
        $lista= array();
        $query="SELECT * FROM cat_log_activgral ";

        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs["cag_id_log_activgral"];
            $idArea= $rs[strtoupper("car_id_area")];
            $desc = $rs[strtoupper("cag_descripcion")];
            $orden = $rs[strtoupper("cag_orden")];
            
            $elemento = new Actividad();
            $elemento->setAll($id,$idArea,$desc,$orden);
            array_push($lista, $elemento);
        }	
        return $lista;	
    }
    
    public function obtieneActividad($idActividad){
        
        $elemento = new Actividad();
        $query="SELECT * FROM cat_log_activgral WHERE  cag_id_los_activgral = ".$idActividad;
        
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs["cag_id_log_activgral"];
            $idArea= $rs[strtoupper("car_id_area")];
            $desc = $rs[strtoupper("cag_descripcion")];
            $orden = $rs[strtoupper("cag_orden")];
            
            $elemento = new Actividad();
            $elemento->setAll($id,$idArea,$desc,$orden);
        }	

        return $elemento;
    }
}
