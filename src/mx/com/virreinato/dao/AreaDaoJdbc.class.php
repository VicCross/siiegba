<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Area.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");


class AreaDaoJdbc {
    
    public function obtieneAreas() {
        $lista= array();

        $query="SELECT * FROM cat_areas WHERE car_estatus=1 ORDER BY car_area";

        $con=new Catalogo();
        $result = $con->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs["car_id_area"];
            $descripcion= $rs[strtoupper("car_area")];
			$idLider = $rs["id_lider"];
			$presupuesto = $rs["presupuesto_inicial"];
			$saldo = $rs["saldo"];
            $elemento= new Area();
            $elemento->setAll($rs[0], $rs[1], $rs[3], $rs[4], $rs[5], $rs[6]);
            array_push($lista,$elemento);
        }	
        return $lista;
    }
	
	public function obtieneAreasFiltro(){
		$lista= array();

        $query="SELECT * FROM cat_areas WHERE car_estatus = 1 AND (car_id_area = 1 OR car_id_area = 4 OR car_id_area = 6 OR car_id_area = 7 OR car_id_area = 8 OR car_id_area = 9 OR car_id_area = 10 OR car_id_area = 12 OR car_id_area = 13 OR car_id_area = 14 OR car_id_area = 16 OR car_id_area = 44 OR car_id_area = 109) ORDER BY car_id_area";

        $con=new Catalogo();
        $result = $con->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            /*$id= $rs["car_id_area"];
            $descripcion= $rs[strtoupper("car_area")];
			$idLider = $rs["id_lider"];
			$presupuesto = $rs["presupuesto_inicial"];
			$saldo = $rs["saldo"];
            $elemento= new Area();
            $elemento->setAll($id, $descripcion, $idLider, $presupuesto, $saldo);*/
			$elemento = new Area();
			$elemento->setAll($rs[0], $rs[1], $rs[3], $rs[4], $rs[5], $rs[6]);
            array_push($lista,$elemento);
        }	
        return $lista;	
	}
        
    public function obtieneArea($idArea) {
		
		
        $elemento=new Area();

        $query="SELECT * FROM cat_areas WHERE car_id_area=".$idArea;

        $con=new Catalogo();
        $result = $con->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs["car_id_area"];
            $descripcion= $rs[strtoupper("car_area")];
			$idLider = $rs["id_lider"];
			$presupuesto = $rs["presupuesto_inicial"];
			$saldo = $rs["saldo"];
            $elemento= new Area();
            $elemento->setAll($rs[0], $rs[1], $rs[3], $rs[4], $rs[5], $rs[6]);
        }

        return $elemento;		
    }
    
    public function guardaArea($area) {
		
        $con=new Catalogo();
        $query="INSERT INTO cat_areas(car_area, id_lider) VALUES ('".mb_strtoupper($area->getDescripcion(), 'UTF-8')."', " . $area->getIdLider() .")";
        $res = $con->obtenerLista($query);

        if($res==1)
        {        return true; }
        else
        {        return false; }
		
    }
    
    public function actualizaArea($area) {

        $con=new Catalogo();
        $query="UPDATE cat_areas set car_area='".mb_strtoupper($area->getDescripcion(),'UTF-8')."', id_lider = " . $area->getIdLider() . ", presupuesto_inicial = " . $area->getPresupuesto() .", saldo = " . $area->getSaldo() . " WHERE car_id_area=".$area->getId();
        $res = $con->obtenerLista($query);
        
        if($res==1)
        {        return true; }
        else
        {        return false; }

    }
    public function eliminaArea($idElemento) {

        $con=new Catalogo();
        $query="UPDATE cat_areas set car_estatus=0 WHERE car_id_area=".$idElemento;
        $res = $con->obtenerLista($query);

        if($res==1)
        {        return true; }
        else
        {        return false; }

    }
	
	public function guardaFirma($area){
		$catalogo = new Catalogo();
		$firma = "null";
		if(!is_null($area->getFirma())) $firma = "'" . $area->getFirma() . "'";
		$update = "UPDATE cat_areas set firma = " . $firma . " WHERE car_id_area = " . $area->getId();
		$res = $catalogo->obtenerLista($update);
		if($res == "1") return true;
		else return false;
	}
}
