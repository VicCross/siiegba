<?php
if(session_id() == '') {
    session_start();
}

include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Empleado.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Comision.class.php");

/**
 * Description of ComisionDaoJdbc
 *
 * @author HPdv6
 */
class ComisionDaoJdbc {

    /**
     * 
     * @return array
     */
    public function obtieneListado($inicio,$fin) {
        $lista = array();
        $query = strtolower("SELECT * FROM sie_comisiones C, CAT_EMPLEADO E  WHERE C.CEM_ID_EMPLEADO=E.CEM_ID_EMPLEADO AND C.com_estatus=1 AND C.COM_PERIODO_INI >= '".date("Y-m-d",strtotime($inicio))."' AND C.COM_PERIODO_FIN < '".date("Y-m-d",strtotime($fin))."' ORDER BY com_num_ofic, E.CEM_NOMBRE, E.CEM_APPATERNO, E.CEM_APMATERNO ");
        $catalogo = new Catalogo();
        $result = $catalogo->obtenerLista($query);
        while ($rs = mysql_fetch_array($result)) {
            $id = $rs[strtoupper("id_comisiones")];
            $id_ccosto = $rs[strtoupper("com_id_ccosto")];
            $fecha_ofic = $rs[strtoupper("com_fecha_ofic")];
            $num_ofic = $rs[strtoupper("com_num_ofic")];
            $periodo_ini = $rs[strtoupper("com_periodo_ini")];
            $periodo_fin = $rs[strtoupper("com_periodo_fin")];
            $objetivo = $rs[strtoupper("com_objetivo")];
            $resultados = $rs[strtoupper("com_resultados")];
            $nacional = $rs[strtoupper("com_nacional")];
            $lugarcom = $rs[strtoupper("com_lugarcom")];
            $tarifadiaria = $rs[strtoupper("com_tarifadiaria")];
            $numdias = $rs[strtoupper("com_numdias")];
            $montotal = $rs[strtoupper("com_montotal")];
            $mondoctos = $rs[strtoupper("com_mondoctos")];
            $monsindoc = $rs[strtoupper("com_monsindoc")];
            $letrasindoc = $rs[strtoupper("com_letrasindoc")];
            $saldopend = $rs[strtoupper("com_saldopend")];
            $estatus = $rs[strtoupper("com_estatus")];

            $idEmp = $rs[strtoupper("cem_id_empleado")];
            $nombre = $rs[strtoupper("cem_nombre")];
            $apaterno = $rs[strtoupper("cem_appaterno")];
            $amaterno = $rs[strtoupper("cem_apmaterno")];
            $sueldoNeto = $rs[strtoupper("cem_sueldo_neto")];
            $sueldoLetra = $rs[strtoupper("cem_sueldo_letra")];
            $puesto = $rs[strtoupper("cem_puesto")];
            $rfc = $rs[strtoupper("cem_rfc")];
            $tipoNomina = $rs[strtoupper("cem_tipo_nomina")];
            $numero = $rs[strtoupper("cem_numero_emp")];

            $empleado = new Empleado();
            $empleado->setAll($idEmp, $nombre, $apaterno, $amaterno, $sueldoNeto, $sueldoLetra, $puesto, $rfc, $tipoNomina, $numero);
            
            $elemento = new Comision();
            $elemento->setAll($id, $id_ccosto, $fecha_ofic, $num_ofic, $periodo_ini, $periodo_fin, $objetivo, $resultados, $nacional, $lugarcom, $tarifadiaria, $numdias, $montotal, $mondoctos, $monsindoc, $letrasindoc, $saldopend, $estatus, $empleado);
            array_push($lista, $elemento);
        }
        return $lista;
    }

    public function obtieneElemento($idElemento) {
        $elemento = new Comision();
        $query = strtolower("SELECT * FROM sie_comisiones C, CAT_EMPLEADO E  WHERE C.CEM_ID_EMPLEADO=E.CEM_ID_EMPLEADO AND C.id_comisiones=" . $idElemento . " ");        

        $catalogo = new Catalogo();
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)) {
            $id = $rs[strtoupper("id_comisiones")];
            $id_ccosto = $rs[strtoupper("com_id_ccosto")];
            $fecha_ofic = $rs[strtoupper("com_fecha_ofic")];
            $num_ofic = $rs[strtoupper("com_num_ofic")];
            $periodo_ini = $rs[strtoupper("com_periodo_ini")];
            $periodo_fin = $rs[strtoupper("com_periodo_fin")];
            $objetivo = $rs[strtoupper("com_objetivo")];
            $resultados = $rs[strtoupper("com_resultados")];
            $nacional = $rs[strtoupper("com_nacional")];
            $lugarcom = $rs[strtoupper("com_lugarcom")];
            $tarifadiaria = $rs[strtoupper("com_tarifadiaria")];
            $numdias = $rs[strtoupper("com_numdias")];
            $montotal = $rs[strtoupper("com_montotal")];
            $mondoctos = $rs[strtoupper("com_mondoctos")];
            $monsindoc = $rs[strtoupper("com_monsindoc")];
            $letrasindoc = $rs[strtoupper("com_letrasindoc")];
            $saldopend = $rs[strtoupper("com_saldopend")];
            $estatus = $rs[strtoupper("com_estatus")];

            $idEmp = $rs[strtoupper("cem_id_empleado")];
            $nombre = $rs[strtoupper("cem_nombre")];
            $apaterno = $rs[strtoupper("cem_appaterno")];
            $amaterno = $rs[strtoupper("cem_apmaterno")];
            $sueldoNeto = $rs[strtoupper("cem_sueldo_neto")];
            $sueldoLetra = $rs[strtoupper("cem_sueldo_letra")];
            $puesto = $rs[strtoupper("cem_puesto")];
            $rfc = $rs[strtoupper("cem_rfc")];
            $tipoNomina = $rs[strtoupper("cem_tipo_nomina")];
            $numero = $rs[strtoupper("cem_numero_emp")];
            
            $empleado = new Empleado();
            $empleado->setAll($idEmp, $nombre, $apaterno, $amaterno, $sueldoNeto, $sueldoLetra, $puesto, $rfc, $tipoNomina, $numero);
            
            $elemento = new Comision();
            $elemento->setAll($id, $id_ccosto, $fecha_ofic, $num_ofic, $periodo_ini, $periodo_fin, $objetivo, 
                    $resultados, $nacional, $lugarcom, $tarifadiaria, $numdias, $montotal, $mondoctos, $monsindoc, 
                    $letrasindoc, $saldopend, $estatus, $empleado);
        }

        return $elemento;
    }

    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $empleado = $elemento->getEmpleado();
        $query="INSERT INTO sie_comisiones(com_id_ccosto, com_fecha_ofic, com_num_ofic, com_periodo_ini, com_periodo_fin, " .
				"com_objetivo, com_resultados, com_nacional, com_lugarcom, com_tarifadiaria, com_numdias, com_montotal, com_mondoctos, " .
				"com_monsindoc, com_letrasindoc, com_saldopend, com_estatus, cem_id_empleado)".
				" VALUES (".$elemento->getIdCCosto().", '".date("Y-m-d h:i:s",strtotime($elemento->getFechaOficio()))."', '".$elemento->getNumeroOficio()."', '".date("Y-m-d",strtotime($elemento->getPeriodoInicial()))."', '".date("Y-m-d",strtotime($elemento->getPeriodoFinal()))."' , " .
				" '".$elemento->getObjetivo()."', '".$elemento->getResultados()."', ".$elemento->getNacional()." , '".$elemento->getLugar()."', ".$elemento->getTarifaDiaria().", ".$elemento->getNumeroDias()." , ".$elemento->getMontoTotal().", ".$elemento->getMontoDoctos().", " .
				" ".$elemento->getMontoSinDoctos().", '".$elemento->getLetraSinDoctos()."', ".$elemento->getSaldoPendiente()." , ".$elemento->getEstatus().", ".$empleado->getId()."  )";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
        $empleado = $elemento->getEmpleado();
	$query="UPDATE sie_comisiones set  com_id_ccosto=".$elemento->getIdCCosto().", com_fecha_ofic='".date("Y-m-d",strtotime($elemento->getFechaOficio()))."', com_num_ofic='".$elemento->getNumeroOficio()."', " .
				" com_periodo_ini='".date("Y-m-d",strtotime($elemento->getPeriodoInicial()))."', com_periodo_fin='".date("Y-m-d",strtotime($elemento->getPeriodoFinal()))."', com_objetivo='".$elemento->getObjetivo()."', com_resultados='".$elemento->getResultados()."'," .
				" com_nacional=".$elemento->getNacional().", com_lugarcom='".$elemento->getLugar()."', com_tarifadiaria=".$elemento->getTarifaDiaria().", com_numdias=".$elemento->getNumeroDias()."," .
				" com_montotal=".$elemento->getMontoTotal().", com_mondoctos=".$elemento->getMontoDoctos().", com_monsindoc=".$elemento->getMontoSinDoctos().", com_letrasindoc='".$elemento->getLetraSinDoctos()."'," .
				" com_saldopend=".$elemento->getSaldoPendiente().", com_estatus=".$elemento->getEstatus().", cem_id_empleado=".$empleado->getId()."  WHERE id_comisiones=".$elemento->getId();
        $res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_comisiones set  com_estatus=0 WHERE id_comisiones=".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}
