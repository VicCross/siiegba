<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/TarjetaPersonal.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class TarjetaPersonalDaoJdbc {
    
    public function obtieneListado($id_tarjeta) {
		
	$lista= array();
        $asignado = null;
	$query="SELECT * FROM sie_tarjeta_reg_det_personal WHERE trdp_estatus = 1 AND  tre_id_tarjeta = ".(int)($id_tarjeta)." ";
		
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $idPersonal= $rs[strtoupper("trdp_id_personal")];
            $nombre = $rs[strtoupper("trdp_nombre")];
            $numero= $rs[strtoupper("trdp_numero")];
            $grado = $rs[strtoupper("trdp_grado")];
            $puesto = $rs[strtoupper("trdp_puestoycatego")];
            $instituciones = $rs[strtoupper("trdp_instituciones")];
            $asignado = null;

            if( $rs[strtoupper("trdp_porinah")]!= null ) $asignado = "INAH";
            else if( $rs[strtoupper("trdp_porproy")]!= null ) $asignado = "Proyecto";
            else if( $rs[strtoupper("trdp_externo")]!= null ) $asignado = "Externo";

            $elemento = new TarjetaPersonal();
            $elemento->constructor2($idPersonal,$numero,$nombre,$grado,$puesto,$instituciones,$asignado);
            array_push($lista, $elemento);
        }	
        return $lista;
    }
    
    public function obtieneElemento($idElemento) {
		
	$elemento = new TarjetaPersonal();
	$query="SELECT * FROM sie_tarjeta_reg_det_personal WHERE  trdp_estatus = 1 AND trdp_id_personal = ".$idElemento;

	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
				
            $idPersonal= $rs[strtoupper("trdp_id_personal")];
            $idTarjeta = $rs[strtoupper("tre_id_tarjeta")];
            $numero= $rs[strtoupper("trdp_numero")];
            $nombre = $rs[strtoupper("trdp_nombre")];
            $grado = $rs[strtoupper("trdp_grado")];
            $puesto = $rs[strtoupper("trdp_puestoycatego")];
            $instituciones = $rs[strtoupper("trdp_instituciones")];
            $asignado = null;
            if( $rs[strtoupper("trdp_porinah")]!= null ) $asignado = "INAH";
            else if( $rs[strtoupper("trdp_porproy")]!= null ) $asignado = "Proyecto";
            else if( $rs[strtoupper("trdp_externo")]!= null ) $asignado = "Externo";

            $elemento = new TarjetaPersonal();
            $elemento->setAll($idPersonal,$idTarjeta,$numero,$nombre,$grado,$puesto,$instituciones,$asignado);
							
        }
		
	return $elemento;
		
    }
    
    public function guardaElemento($elemento) {
		
        $inah = "";
        $proy = "";
        $ext = "";


        if( $elemento->getAsignado() == "INAH" ) $inah = "INAH";
        else if( $elemento->getAsignado() == "Proyecto" ) $proy = "Proy";
        else if( $elemento->getAsignado() == "Externo") $ext = "Ext";
        
        $con=new Catalogo();
        $query="INSERT INTO sie_tarjeta_reg_det_personal(tre_id_tarjeta,trdp_numero,trdp_nombre,trdp_grado,trdp_puestoycatego,trdp_porinah,trdp_porproy,trdp_externo,trdp_instituciones,trdp_estatus) VALUES (".$elemento->getIdTarjeta().", ". $elemento->getNum_Personal().", '". mb_strtoupper($elemento->getNombre(),'UTF-8')."','".
            $elemento->getGrado()."' ,'". mb_strtoupper($elemento->getPuesto(),'UTF-8')."','".$inah."', '".$proy."','".$ext."','". mb_strtoupper($elemento->getInstituciones(),'UTF-8')."', 1)";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
	
        $inah = "";
        $proy = "";
        $ext = "";


        if( $elemento->getAsignado() == "INAH" ) $inah = "INAH";
        else if( $elemento->getAsignado() == "Proyecto" ) $proy = "Proy";
        else if( $elemento->getAsignado() == "Externo") $ext = "Ext";
        
	$con=new Catalogo();
	$query="UPDATE sie_tarjeta_reg_det_personal SET  trdp_numero = ".$elemento->getNum_Personal().", trdp_nombre = '".mb_strtoupper($elemento->getNombre(),'UTF-8').  "', trdp_grado ='" . $elemento->getGrado() ."'" .
				",trdp_puestoycatego = '".mb_strtoupper($elemento->getPuesto(),'UTF-8')."', trdp_instituciones = '". mb_strtoupper($elemento->getInstituciones(),'UTF-8')."' , trdp_porinah = '" . $inah . "', trdp_porproy = '".$proy."', trdp_externo='".$ext."'  WHERE trdp_id_personal = ". $elemento->getIdPersonal() ." ";
	$res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_tarjeta_reg_det_personal SET  trdp_estatus = 0 WHERE trdp_id_personal = ".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}
