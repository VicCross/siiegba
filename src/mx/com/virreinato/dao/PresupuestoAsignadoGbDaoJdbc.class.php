<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/PresupuestoAsignadoGb.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/TarjetaCalendario.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class PresupuestoAsignadoGbDaoJdbc {
    
    public function obtieneListado($destino){
		
	$lista= array();
        $query=strtolower("SELECT * FROM SIE_PRESUP_ASIGNADO " .
            "WHERE  pas_estatus=1 and destino='".$destino."'");
		
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("PAS_ID_PRESUP_ASIGNADO")];
            $idPeriodo=$rs[strtoupper("CPE_ID_PERIODO")];
            $montoEne = $rs[strtoupper("MONTO_ENERO")];
            $montoFeb = $rs[strtoupper("MONTO_FEBRERO")];
            $montoMar = $rs[strtoupper("MONTO_MARZO")];
            $montoAbr = $rs[strtoupper("MONTO_ABRIL")];
            $montoMay = $rs[strtoupper("MONTO_MAYO")];
            $montoJun = $rs[strtoupper("MONTO_JUNIO")];
            $montoJul = $rs[strtoupper("MONTO_JULIO")];
            $montoAgo = $rs[strtoupper("MONTO_AGOSTO")];
            $montoSep = $rs[strtoupper("MONTO_SEPTIEMBRE")];
            $montoOct = $rs[strtoupper("MONTO_OCTUBRE")];
            $montoNov = $rs[strtoupper("MONTO_NOVIEMBRE")];
            $montoDic = $rs[strtoupper("MONTO_DICIEMBRE")];
            $destiny= $rs[strtoupper("destino")];

            $elemento = new PresupuestoAsignadoGb();
            $elemento->setAll($id,$idPeriodo,$montoEne, $montoFeb, $montoMar, $montoAbr, $montoMay, $montoJun, $montoJul, $montoAgo, $montoSep, $montoOct, $montoNov, $montoDic, $destiny);
            array_push($lista, $elemento);
        }	
        return $lista;
    }
    
    public function obtieneElemento($idElemento) {
		
		
        $elemento = new PresupuestoAsignadoGb();

        $query=strtolower("SELECT * FROM SIE_PRESUP_ASIGNADO " .
        "WHERE PAS_ID_PRESUP_ASIGNADO = ".$idElemento." ");
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id= $rs[strtoupper("PAS_ID_PRESUP_ASIGNADO")];
            $idPeriodo=$rs[strtoupper("CPE_ID_PERIODO")];
            $montoEne = $rs[strtoupper("MONTO_ENERO")];
            $montoFeb = $rs[strtoupper("MONTO_FEBRERO")];
            $montoMar = $rs[strtoupper("MONTO_MARZO")];
            $montoAbr = $rs[strtoupper("MONTO_ABRIL")];
            $montoMay = $rs[strtoupper("MONTO_MAYO")];
            $montoJun = $rs[strtoupper("MONTO_JUNIO")];
            $montoJul = $rs[strtoupper("MONTO_JULIO")];
            $montoAgo = $rs[strtoupper("MONTO_AGOSTO")];
            $montoSep = $rs[strtoupper("MONTO_SEPTIEMBRE")];
            $montoOct = $rs[strtoupper("MONTO_OCTUBRE")];
            $montoNov = $rs[strtoupper("MONTO_NOVIEMBRE")];
            $montoDic = $rs[strtoupper("MONTO_DICIEMBRE")];
            $destiny= $rs[strtoupper("destino")];

            $elemento = new PresupuestoAsignadoGb();
            $elemento->setAll($id,$idPeriodo,$montoEne, $montoFeb, $montoMar, $montoAbr, $montoMay, $montoJun, $montoJul, $montoAgo, $montoSep, $montoOct, $montoNov, $montoDic, $destiny);
        }	
        return $elemento;
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $query=strtolower("INSERT INTO SIE_PRESUP_ASIGNADO (CPE_ID_PERIODO, PAS_ESTATUS,MONTO_ENERO,MONTO_FEBRERO,MONTO_MARZO,MONTO_ABRIL,MONTO_MAYO,MONTO_JUNIO,MONTO_JULIO,MONTO_AGOSTO,MONTO_SEPTIEMBRE,MONTO_OCTUBRE,MONTO_NOVIEMBRE,MONTO_DICIEMBRE, destino) " .
            " VALUES (".$elemento->getIdPeriodo()." ,1, ".$elemento->getMontoEne()." , ".$elemento->getMontoFeb().", ".$elemento->getMontoMar().", ".$elemento->getMontoAbr().", ".$elemento->getMontoMay().", ".$elemento->getMontoJun().", ".$elemento->getMontoJul().", ".$elemento->getMontoAgo().", ".$elemento->getMontoSep()." , ".$elemento->getMontoOct()." , ".$elemento->getMontoNov()." , ".$elemento->getMontoDic().", '".$elemento->getDestino()."')");
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
	$query=strtolower("UPDATE SIE_PRESUP_ASIGNADO SET  CPE_ID_PERIODO = ".$elemento->getIdPeriodo().", MONTO_ENERO = ".$elemento->getMontoEne()." ,MONTO_FEBRERO = ".$elemento->getMontoFeb()." ,MONTO_MARZO = ".$elemento->getMontoMar()." ,MONTO_ABRIL = ".$elemento->getMontoAbr()." ,MONTO_MAYO = ".$elemento->getMontoMay()." ,MONTO_JUNIO = ".$elemento->getMontoJun()." ,MONTO_JULIO = ".$elemento->getMontoJul()." ,MONTO_AGOSTO = ".$elemento->getMontoAgo()." ,MONTO_SEPTIEMBRE = ".$elemento->getMontoSep()." ,MONTO_OCTUBRE = ".$elemento->getMontoOct()." ,MONTO_NOVIEMBRE = ".$elemento->getMontoNov()." ,MONTO_DICIEMBRE = ".$elemento->getMontoDic()."  WHERE PAS_ID_PRESUP_ASIGNADO=".$elemento->getId()." ");
	$res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query=strtolower("UPDATE SIE_PRESUP_ASIGNADO set  PAS_ESTATUS = 0 WHERE PAS_ID_PRESUP_ASIGNADO = ".$idElemento);
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}
