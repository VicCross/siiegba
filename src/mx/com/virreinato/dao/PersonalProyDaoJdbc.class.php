<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/PersonalProy.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class PersonalProyDaoJdbc {
    
    public function obtieneListado() {
		
	$lista= array();
		
	$query=" SELECT * FROM sie_proy_personal WHERE ppr_estatus>0 ORDER BY ppr_app,ppr_apm,ppr_nombre";
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id = $rs[strtoupper("ppr_id_personalproyectos")];
            $rfc = $rs[strtoupper("ppr_rfc")];
            $curp = $rs[strtoupper("ppr_curp")];
            $nombre = $rs[strtoupper("ppr_nombre")];
            $app = $rs[strtoupper("ppr_app")];
            $apm = $rs[strtoupper("ppr_apm")];
            $estatus = $rs[strtoupper("ppr_estatus")];

            $elemento = new PersonalProy();
            $elemento->setAll($id,$rfc,$curp,$nombre,$app,$apm,$estatus);
            array_push($lista, $elemento);
        }	

	return $lista;
    }
    
    public function obtieneElemento($idElemento) {
		
		
	$elemento=new PersonalProy();
		
	$query="SELECT * FROM sie_proy_personal WHERE ppr_id_personalproyectos=".$idElemento;
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id = $rs[strtoupper("ppr_id_personalproyectos")];
            $rfc = $rs[strtoupper("ppr_rfc")];
            $curp = $rs[strtoupper("ppr_curp")];
            $nombre = $rs[strtoupper("ppr_nombre")];
            $app = $rs[strtoupper("ppr_app")];
            $apm = $rs[strtoupper("ppr_apm")];
            $estatus = $rs[strtoupper("ppr_estatus")];

            $elemento = new PersonalProy();
            $elemento->setAll($id,$rfc,$curp,$nombre,$app,$apm,$estatus);
        }	

	return $elemento;
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $query="INSERT INTO sie_proy_personal "
				." (ppr_rfc,ppr_curp,ppr_nombre,ppr_app,ppr_apm) "
				." VALUES ('"
				.mb_strtoupper($elemento->getRfc(),'UTF-8')."', '"
				.mb_strtoupper($elemento->getCurp(),'UTF-8')."', '"
				.mb_strtoupper($elemento->getNombre(),'UTF-8')."', '"
				.mb_strtoupper($elemento->getApp(),'UTF-8')."', '"
				.mb_strtoupper($elemento->getApm(),'UTF-8')."')";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
	$query="UPDATE sie_proy_personal set "
				." ppr_rfc='".mb_strtoupper($elemento->getRfc(),'UTF-8')."', "
				." ppr_curp='".mb_strtoupper($elemento->getCurp(),'UTF-8')."', "
				." ppr_nombre='".mb_strtoupper($elemento->getNombre(),'UTF-8')."', "
				." ppr_app='".mb_strtoupper($elemento->getApp(),'UTF-8')."', "
				." ppr_apm='".mb_strtoupper($elemento->getApm(),'UTF-8')."' "
				." WHERE ppr_id_personalproyectos=".$elemento->getId();
        $res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_proy_personal set ppr_estatus=0 WHERE ppr_id_personalproyectos=".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}
