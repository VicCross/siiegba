<?php
if(session_id() == '') {
    session_start();
}

include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/TarjetaMetas.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class TarjetaMetasDaoJdbc {
    
    public function obtieneListado($id_tarjeta) {
		
	$lista= array();
	$query="SELECT * FROM sie_tarjeta_reg_det_metas WHERE trdm_estatus = 1 AND tre_id_tarjeta = ".(int)($id_tarjeta)." ";
		
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $idMeta= $rs[strtoupper("trdm_id_meta")];
            $orden = $rs[strtoupper("trdm_orden")];
            $acuerdo = $rs[strtoupper("trdm_nombredeacuerdo")];
            $clave = $rs[strtoupper("trdm_clave")];
            $trimestral1 = $rs[strtoupper("trdm_trim1")];
            $trimestral2 = $rs[strtoupper("trdm_trim2")];
            $trimestral3 = $rs[strtoupper("trdm_trim3")];
            $trimestral4 = $rs[strtoupper("trdm_trim4")];

            $elemento = new TarjetaMetas();
            $elemento->constructor2($idMeta,$orden,$clave,$acuerdo,$trimestral1,$trimestral2,$trimestral3,$trimestral4);
            array_push($lista, $elemento);
        }	
        return $lista;
    }
    
    public function obtieneElemento($idElemento) {
		
	$elemento = null;
	$query="SELECT * FROM sie_tarjeta_reg_det_metas WHERE  trdm_estatus = 1 AND trdm_id_meta = ".$idElemento;

	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
				
            $idMeta= $rs[strtoupper("trdm_id_meta")];
            $idTarjeta= $rs[strtoupper("tre_id_tarjeta")];
            $orden = $rs[strtoupper("trdm_orden")];
            $acuerdo = $rs[strtoupper("trdm_nombredeacuerdo")];
            $clave = $rs[strtoupper("trdm_clave")];
            $trimestral1 = $rs[strtoupper("trdm_trim1")];
            $trimestral2 = $rs[strtoupper("trdm_trim2")];
            $trimestral3 = $rs[strtoupper("trdm_trim3")];
            $trimestral4 = $rs[strtoupper("trdm_trim4")];

            $elemento = new TarjetaMetas();
            $elemento->setAll($idMeta,$idTarjeta,$orden,$clave,$acuerdo,$trimestral1,$trimestral2,$trimestral3, $trimestral4);

        }
		
        return $elemento;
		
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $query="INSERT INTO sie_tarjeta_reg_det_metas(tre_id_tarjeta,trdm_orden,trdm_clave,trdm_nombredeacuerdo,trdm_trim1,trdm_trim2,trdm_trim3,trdm_trim4,trdm_estatus) VALUES (".$elemento->getIdTarjeta().", ". $elemento->getNum_orden() .", '".  $elemento->getClave() ."','".
        mb_strtoupper($elemento->getNombreAcuerdo(),'UTF-8')."' ,". $elemento->getTrimestral1() .",". $elemento->getTrimestral2() .", ". $elemento->getTrimestral3() .", ". $elemento->getTrimestral4() ." , 1 )";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
	$query="UPDATE sie_tarjeta_reg_det_metas  SET  trdm_orden = ".$elemento->getNum_orden().", trdm_nombredeacuerdo = '".mb_strtoupper($elemento->getNombreAcuerdo(),'UTF-8')."', trdm_clave ='" . $elemento->getClave() ."' " .
				",trdm_trim1 =".$elemento->getTrimestral1().", trdm_trim2 = ". $elemento->getTrimestral2() ." , trdm_trim3 = ". $elemento->getTrimestral3().", trdm_trim4 = ". $elemento->getTrimestral4() ."  WHERE trdm_id_meta = ". $elemento->getIdMeta() ." ";
        $res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_tarjeta_reg_det_metas set  trdm_estatus = 0 WHERE trdm_id_meta = ".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}
