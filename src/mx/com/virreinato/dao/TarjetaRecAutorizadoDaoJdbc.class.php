<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/TarjetaRecAutorizados.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class TarjetaRecAutorizadoDaoJdbc {
    
    public function obtieneListado($id_tarjeta) {
		
	$lista= array();
	$query="SELECT  *  FROM sie_tarjeta_reg_det_recaut WHERE trdr_estatus = 1  AND tre_id_tarjeta =  ".(int)($id_tarjeta)." order by(trdr_anio)";
		
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id = $rs[strtoupper("trdr_id_recursoautorizado")];
            $idTarjeta= $rs[strtoupper("tre_id_tarjeta")];
            $anio = $rs[strtoupper("trdr_anio")];
            $ingreso = $rs[strtoupper("trdr_ingresosautogen")];
            $aportaciones = $rs[strtoupper("trdr_aportacionesterceros")];

            $elemento = new TarjetaRecAutorizados();
            $elemento->constructor2($id,$idTarjeta,$anio,$ingreso,$aportaciones);
            array_push($lista, $elemento);
        }	
        return $lista;
    }
    
    public function Imprimir ($idTarjeta,$limite){
	
        $lista= array();
	$query="SELECT  *  FROM sie_tarjeta_reg_det_recaut WHERE trdr_estatus = 1  AND tre_id_tarjeta =  ".(int)($idTarjeta)." AND trdr_anio > ".$limite." order by(trdr_anio)";
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id = $rs[strtoupper("trdr_id_recursoautorizado")];
            $idTarjeta= $rs[strtoupper("tre_id_tarjeta")];
            $anio = $rs[strtoupper("trdr_anio")];
            $ingreso = $rs[strtoupper("trdr_ingresosautogen")];
            $aportaciones = $rs[strtoupper("trdr_aportacionesterceros")];
            $SumRec = $ingreso + $aportaciones;

            $elemento = new TarjetaRecAutorizados();
            $elemento->setAll($id,$idTarjeta,$anio,$ingreso,$aportaciones,$SumRec);
            array_push($lista, $elemento);
        }	
        return $lista;
    }
    
    public function obtieneElemento($idElemento) {		
		
        $elemento = new TarjetaRecAutorizados();
        $query="SELECT * FROM sie_tarjeta_reg_det_recaut WHERE  trdr_estatus = 1 AND trdr_id_recursoautorizado = ".$idElemento;
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id = $rs[strtoupper("trdr_id_recursoautorizado")];
            $idTarjeta= $rs[strtoupper("tre_id_tarjeta")];
            $anio = $rs[strtoupper("trdr_anio")];
            $ingreso = $rs[strtoupper("trdr_ingresosautogen")];
            $aportaciones = $rs[strtoupper("trdr_aportacionesterceros")];
            $SumRec = $ingreso + $aportaciones;

            $elemento = new TarjetaRecAutorizados();
            $elemento->setAll($id,$idTarjeta,$anio,$ingreso,$aportaciones,$SumRec);
        }	
        return $elemento;
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $query="INSERT INTO sie_tarjeta_reg_det_recaut(tre_id_tarjeta,trdr_anio,trdr_ingresosautogen,trdr_aportacionesterceros,trdr_estatus) VALUES (".$elemento->getIdTarjeta().", ".$elemento->getAnio() .", ".$elemento->getIngresoAutogenerado() ." ,".$elemento->getAportacionesTerceros().", 1)";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
	$query="UPDATE sie_tarjeta_reg_det_recaut SET trdr_anio = ".$elemento->getAnio().", trdr_ingresosautogen = ".$elemento->getIngresoAutogenerado().  ", " .
            "trdr_aportacionesterceros = ".$elemento->getAportacionesTerceros()."  WHERE trdr_id_recursoautorizado = ".$elemento->getIdRecurso() ."";
	$res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_tarjeta_reg_det_recaut set trdr_estatus = 0 WHERE trdr_id_recursoautorizado = ".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}