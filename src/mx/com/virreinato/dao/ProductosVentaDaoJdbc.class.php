<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProductosVenta.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class ProductosVentaDaoJdbc {
    
    public function obtieneListadoProdVenta() {
		
	$lista= array();
		
	$query="select b.nombre_prod,b.inventario_actual,a.precio_unitario,a.piezas_vendidas,a.monto_total,a.monto_museo,a.monto_patronato from sie_cat_productos_vendidos a, cat_productos b where a.id_producto=b.id_prod ";
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id_venta= $rs["id_venta"];
            $id_producto= $rs["id_producto"];
            $nom_producto= $rs["nombre_prod"];
            $inventario_act= $rs["inventario_actual"];
            $precio_unitario= $rs["precio_unitario"];
            $piezas_vendidas= $rs["piezas_vendidas"];
			$monto_total= $rs["monto_total"];
			$monto_museo= $rs["monto_museo"];
			$monto_patronato= $rs["monto_patronato"];
            

            $elemento = new ProductosVenta();
            $elemento->setAll($id_venta, $id_producto, $nom_producto, $inventario_act, $precio_unitario, $piezas_vendidas, $monto_total, $monto_museo, $monto_patronato);
            array_push($lista, $elemento);
        }	
        return $lista;
    }
	
	
	public function obtieneElementoProdVenta($elementoid) {
		
	$elemento=new ProductosVenta();
		
	$query="SELECT * FROM sie_cat_productos_vendidos where id_venta= ".$elementoid;
	
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
			
            $id_venta= $rs["id_venta"];
            $id_producto= $rs["id_producto"];
            $nom_producto= $rs["nom_producto"];
            $inventario_act= $rs["inventario_act"];
            $precio_unitario= $rs["precio_unitario"];
            $piezas_vendidas= $rs["piezas_vendidas"];
			$monto_total= $rs["monto_total"];
			$monto_museo= $rs["monto_museo"];
			$monto_patronato= $rs["monto_patronato"];
            

            $elemento = new ProductosVenta();
			
            $elemento->setAll($id_venta, $id_producto, $nom_producto, $inventario_act, $precio_unitario, $piezas_vendidas, $monto_total, $monto_museo, $monto_patronato);
            
        }	
        return $elemento;
		
    }
	
	
	
    
		public function guardaElemento($elemento) {
     
		$con=new Catalogo();

        $query="INSERT INTO sie_cat_productos_vendidos(id_producto,precio_unitario,piezas_vendidas,monto_total,monto_museo,monto_patronato)".

		" VALUES ('".mb_strtoupper($elemento->getId_producto(),'UTF-8')."','".mb_strtoupper($elemento->getPrecio_unitario(),'UTF-8')."', '".mb_strtoupper($elemento->getPiezas_vendidas(),'UTF-8')."', '".mb_strtoupper($elemento->getMonto_total(),'UTF-8')."', '".mb_strtoupper($elemento->getMonto_museo(),'UTF-8')."', '".mb_strtoupper($elemento->getMonto_patronato(),'UTF-8')."')";

        $res=$con->obtenerLista($query);
		
		$query = "update cat_productos set inventario_actual = inventario_actual - ".$elemento->getPiezas_vendidas(). " where id_prod = ".$elemento->getId_producto();
		$res1=$con->obtenerLista($query);
		
		if($res=="1" && $res1=="1")
        {	return true; }
        else
        {	return false; }	

/*
        echo $elemento->getId_producto();
		echo $elemento->getPrecio_unitario();
		echo $elemento->getPiezas_vendidas();

		echo $elemento->getMonto_total();
		echo $elemento->getMonto_museo();
		echo $elemento->getMonto_patronato();
	*/

        }

    
   
    public function actualizaElemento($elemento) {

		$con=new Catalogo();
		$query="UPDATE sie_cat_productos_vendidos set  nom_productos='".mb_strtoupper($elemento->getNom_productos(),'UTF-8')."', inventario_act='".mb_strtoupper($elemento->getInventario_act(),'UTF-8')."', precio_unitario='".mb_strtoupper($elemento->Precio_unitario(),'UTF-8')."' , piezas_vendidas='".mb_strtoupper($elemento->getPiezas_vendidas(),'UTF-8')."' , monto_total='".mb_strtoupper($elemento->getMonto_total(),'UTF-8')."', monto_museo='".mb_strtoupper($elemento->getMonto_museo(),'UTF-8')."', monto_patronato='".mb_strtoupper($elemento->getMonto_patronato(),'UTF-8')."' WHERE id_venta=".$elemento->getId_venta();		
		
		$res = $con->obtenerLista($query);

		

		if($res == "1")

        {	return true; }

        else

        {	return false; }

		

		}

    //falta estatus

	public function eliminaElemento($idElemento){

		

        $con=new Catalogo();

        $query="UPDATE sie_cat_productos_vendidos set  estatus_prod = 0 WHERE id_prod=".$idElemento;

        $res = $con->obtenerLista($query);



        if($res=="1")

        {	return true; }

        else

        {	return false; }

		

    }
}
