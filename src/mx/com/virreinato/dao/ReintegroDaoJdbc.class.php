<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Parametro.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class ReintegroDaoJdbc {
    
    public function obtieneListado($inicio, $fin, $periodo) {
		
	$lista= array();
		
        $inicio = date("Y-m-d",strtotime($inicio));
        $fin = date("Y-m-d",strtotime($fin));
        
	$query="SELECT * FROM sie_reintegro_impuestos r, sie_proy_personal p, sie_cat_periodos per"
            ." WHERE r.rei_fecha BETWEEN '".$inicio."' AND '".$fin."' AND r.cpe_id_periodo = ".$periodo."  AND r.cpe_id_periodo = per.cpe_id_periodo AND  r.ppr_id_personalproyectos=p.ppr_id_personalproyectos"
            ." AND r.rei_estatus>0"
            ." ORDER BY r.rei_fecha,r.rei_folio";
        
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id =$rs[strtoupper("rei_id_reintegroimp")];
            $refNumerica =$rs[strtoupper("rei_refnumerica")];
            $fecha =$rs[strtoupper("rei_fecha")];
            $folio =$rs[strtoupper("rei_folio")];
            $honorarios =$rs[strtoupper("rei_honorarios")];
            $idPersona =$rs[strtoupper("ppr_id_personalproyectos")];
            $rfc =$rs[strtoupper("ppr_rfc")];
            $curp =$rs[strtoupper("ppr_curp")];
            $nombre =$rs[strtoupper("ppr_nombre")];
            $app =$rs[strtoupper("ppr_app")];
            $apm =$rs[strtoupper("ppr_apm")];

            $elemento = new Reintegro();
            $elemento->constructor2($id,$refNumerica,$fecha,$folio,$honorarios,$idPersona,$rfc,$curp,$nombre,$app,$apm);
            array_push($lista, $elemento);
        }	

        return $lista;
    }
    
    public function obtieneElemento($idElemento) {
		
	$elemento=new Reintegro();
		
	$query="SELECT * FROM sie_reintegro_impuestos r, sie_proy_personal p"
            ." WHERE r.ppr_id_personalproyectos=p.ppr_id_personalproyectos"
            ." AND r.rei_id_reintegroimp=".$idElemento;
		
	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id =$rs[strtoupper("rei_id_reintegroimp")];
            $refNumerica =$rs[strtoupper("rei_refnumerica")];
            $fecha =$rs[strtoupper("rei_fecha")];
            $folio =$rs[strtoupper("rei_folio")];
            $honorarios =$rs[strtoupper("rei_honorarios")];
            $idPersona =$rs[strtoupper("ppr_id_personalproyectos")];
            $rfc =$rs[strtoupper("ppr_rfc")];
            $curp =$rs[strtoupper("ppr_curp")];
            $nombre =$rs[strtoupper("ppr_nombre")];
            $app =$rs[strtoupper("ppr_app")];
            $apm =$rs[strtoupper("ppr_apm")];
            $periodo = $rs[strtoupper("cpe_id_periodo")];

            $elemento = new Reintegro();
            $elemento->setAll($id,$refNumerica,$fecha,$folio,$honorarios,$idPersona,$rfc,$curp,$nombre,$app,$apm,$periodo);
        }	

        return $elemento;
		
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $query="INSERT INTO sie_reintegro_impuestos(rei_refnumerica,rei_fecha,rei_folio,rei_honorarios,ppr_id_personalproyectos,rei_estatus,cpe_id_periodo) VALUES ('".$elemento->getRefNumerica()."', '"
            .date("Y-m-d",strtotime($elemento->getFecha()))."', '".$elemento->getFolio()."', ".$elemento->getHonorarios().", ".$elemento->getIdPersonal().",1, ".$elemento->getPeriodo().") ";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
	$query="UPDATE sie_reintegro_impuestos set "
            ." rei_refnumerica='".$elemento->getRefNumerica()."', "
            ." rei_fecha='".date("Y-m-d",strtotime($elemento->getFecha()))."', "
            ." rei_folio='".$elemento->getFolio()."', "
            ." rei_honorarios=".$elemento->getHonorarios().", "
            ." ppr_id_personalproyectos=".$elemento->getIdPersonal().", "
            ." cpe_id_periodo = ".$elemento->getPeriodo().""
            ." WHERE rei_id_reintegroimp=".$elemento->getId();
        $res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_reintegro_impuestos set rei_estatus=0 WHERE rei_id_reintegroimp=".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
}