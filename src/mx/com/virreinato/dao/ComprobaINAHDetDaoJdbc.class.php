<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ComprobaINAHDet.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class ComprobaINAHDetDaoJdbc {
 
    public function obtieneListado($idElementoMaestro) {
		
	$lista= array();
		
	$query="SELECT d.*,p.cpa_partida, p.cpa_descripcion FROM sie_det_comprobaciones_inah d, sie_cat_partidas p WHERE d.cpa_id_partida = p.cpa_id_partida AND d.dci_estatus = 1 AND d.cin_id_cinah=".$idElementoMaestro." ORDER BY dci_id_detcinah";

	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id = $rs[strtoupper("dci_id_detcinah")];
            $idComprobacionINAH = $rs[strtoupper("cin_id_cinah")];
            $idPartida = $rs[strtoupper("cpa_id_partida")];
            $desPartida = $rs["cpa_descripcion"];
            $partida = $rs["cpa_partida"];
            $monto = $rs[strtoupper("dci_monto")];
            $num_notas = $rs[strtoupper("dci_num_notas")];

            $elemento =  new ComprobaINAHDet();
            $elemento->setAll($id,$idComprobacionINAH,$idPartida,$desPartida,$partida , $monto,$num_notas);
            array_push($lista, $elemento);
        }	

        return $lista;
    }
    
    public function MontoMinistracion($idMaestro){
	
        $query = " SELECT  m.min_monto FROM sie_ministraciones m, sie_comprobaciones_inah c WHERE  c.cin_id_cinah = ".$idMaestro." AND c.min_id_ministracion = m.min_id_ministracion";
	$monto = 0.0;

	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $monto = $rs["min_monto"];
        }
        return $monto;
    }
    
    public function obtieneElemento($idElemento) {
		
	$elemento=new ComprobaINAHDet();
		
	$query="SELECT * FROM sie_det_comprobaciones_inah d WHERE dci_id_detcinah=".$idElemento;

	$catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $id = $rs[strtoupper("dci_id_detcinah")];
            $idComprobacionINAH = $rs[strtoupper("cin_id_cinah")];
            $idPartida = $rs[strtoupper("cpa_id_partida")];
            $monto = $rs[strtoupper("dci_monto")];
            $num_notas = $rs[strtoupper("dci_num_notas")];
            
            $elemento = new ComprobaINAHDet();
            $elemento->comprobaINAHDet($id,$idComprobacionINAH,$idPartida,$monto,$num_notas);
        }

        return $elemento;
		
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        $query="INSERT INTO sie_det_comprobaciones_inah(cin_id_cinah,cpa_id_partida,dci_monto,dci_estatus,dci_num_notas) VALUES (".$elemento->getIdComprobacionINAH().", ".$elemento->getIdPartida().", ".$elemento->getMonto().", 1, ".$elemento->getNum_notas().")";
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
	$query="UPDATE sie_det_comprobaciones_inah SET cpa_id_partida =".$elemento->getIdPartida().", dci_monto=".$elemento->getMonto().", dci_num_notas = ".$elemento->getNum_notas()."  WHERE dci_id_detcinah=".$elemento->getId();
        $res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="UPDATE sie_det_comprobaciones_inah SET dci_estatus = 0 WHERE dci_id_detcinah=".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }		
    }
}
