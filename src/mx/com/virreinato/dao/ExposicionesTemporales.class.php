<?php
	class ExposicionesTemporales
	{
		private $idExposicionT;
		private $nombre;
		private $fechaI;
		private $fechaF;
		private $tituloFinal;
		private $tipo;
		private $periodo;
		private $area_coordina;
		private $id_linea_accion;
		private $saldo;
		private $saldoAnterior;
		
		function setAll($idExposicionT, $nombre, $fechaI, $fechaF, $tituloFinal, $tipo, $periodo, $area_coordina, $id_linea_accion, $saldo, $saldoAnterior,$inba,$patronato)
		{
			$this->idExposicionT = $idExposicionT;
			$this->nombre = $nombre;
			$this->fechaI = $fechaI;
			$this->fechaF = $fechaF;
			$this->tituloFinal = $tituloFinal;
			$this->tipo = $tipo;
			$this->periodo = $periodo;
			$this->area_coordina = $area_coordina;
			$this->id_linea_accion = $id_linea_accion;
			$this->saldo = $saldo;
			$this->saldoAnterior = $saldoAnterior;
			$this->inba=$inba;
			$this->patronato=$patronato;
		}
		
		function __construct(){
			
		}
		
		function getIdExposicionT(){
			return $this->idExposicionT;	
		}
		
		function getNombre(){
			return $this->nombre;
		}
		
		function getFechaI(){
			return $this->fechaI;	
		}
		
		function getFechaF(){
			return $this->fechaF;
		}
		
		function getTituloFinal(){
			return $this->tituloFinal;
		}
		
		function getTipo(){
			return $this->tipo;
		}
		
		function getPeriodo(){
			return $this->periodo;
		}
		
		function getAreaCoordina(){
			return $this->area_coordina;
		}
		
		function getIdLineaAccion(){
			return $this->id_linea_accion;
		}
		
		function getSaldo(){
			return $this->saldo;	
		}
		
		function getSaldoAnterior(){
			return $this->saldoAnterior;	
		}
		
		
		function getInba(){
			return $this->inba;	
		}
		
		function getPatronato(){
			return $this->patronato;	
		}
		
		
		function setIdExposicionT($idExpoT){
			 $this->idExposicionT = $idExpoT;	
		}
		
		function setNombre($nombre){
			 $this->nombre = $nombre;
		}
		
		function setFechaI($fechaI){
			 $this->fechaI = $fechaI;	
		}
		
		function setFechaF($fechaF){
			 $this->fechaF = $fechaF;
		}
		
		function setTituloFinal($tituloFinal){
			 $this->tituloFinal = $tituloFinal;
		}
		
		function setTipo($tipo){
			 $this->tipo = $tipo;
		}
		
		function setPeriodo($periodo){
			 $this->periodo = $periodo;
		}
		
		function setAreaCoordina($areaCoordina){
			 $this->area_coordina = $areaCoordina;
		}
		
		function setIdLineaAccion($idLineaAccion){
			 $this->id_linea_accion = $idLineaAccion;
		}
		
		function setSaldo($saldo){
			$this->saldo = $saldo;
		}
		
		function setSaldoAnterior($saldoAnterior){
			$this->saldoAnterior = $saldoAnterior;	
		}
		
		function setInba($inba){
			$this->inba = $inba;	
		}
		
		function setPatronato($pratonato){
			$this->patronato = $patronato;	
		}
		
	}
?>