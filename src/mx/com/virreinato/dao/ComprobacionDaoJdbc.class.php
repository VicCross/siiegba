<?php
if(session_id() == '') {
    session_start();
}
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Comprobacion.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");

class ComprobacionDaoJdbc {
   
    public function obtieneListado() {
        
	$lista = array();

	$query = "select ".
                "com.COM_ID_COMPROBACION as idComprobacion, ".  
                "com.CPE_ID_PERIODO as idPeriodo, ". 
                "com.COM_FOLIOCOMPROB as folio, ". 
                "com.COM_DESCRIPCION as descripcion, ".  
                "com.COM_FECHA as fecha, ".
                "com.COM_MONTO as montoComprobacion, ".  
                "com.COM_DESTINO as destino, ". 
                "com.CPR_ID_PROYECTO as idProyecyo, ".  
                "com.CHE_ID_CHEQUE as idCheque, ". 
                "cte.CEM_NOMBRE||' '|| cte.CEM_APPATERNO ||' '|| cte.CEM_APMATERNO as empleado,".
                "pro.CPR_PROVEEDOR as proveedor,".
                "p.spr_nombre_ch_otro as beneficiario,".
                "che.CHE_FOLIO as folioCheque, ".
                "com.COM_OBSERVACIONES as observaciones,".  
                "com.COM_ESTATUS as estatus ".
                "from sie_cheques che, sie_solicitud_presup p, sie_comprobaciones com,". 
                "cat_empleado cte, sie_cat_proveedores pro  ".
                "where com.CHE_ID_CHEQUE = che.CHE_ID_CHEQUE ".
                "and che.SPR_ID_SOLICITUD = p.SPR_ID_SOLICITUD and p.CEM_ID_EMPLEADO = cte.CEM_ID_EMPLEADO ".
                "and p.CPR_ID_PROVEEDOR = pro.CPR_ID_PROVEEDOR ".
                "and com.COM_PRE_ESTATUS = 1";

        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){

                $idComprobacion = $rs["idComprobacion"]; 
                $idPeriodo = $rs["idPeriodo"]; 
                $folio = $rs["folio"]; 
                $descripcion = $rs["descripcion"];
                $fecha = $rs["fecha"];
                $montoComprobacion = $rs["montoComprobacion"]; 
                $destino = $rs["destino"];
                $idProyecyo = $rs["idProyecyo"]; 
                $idCheque = $rs["idCheque"]; 
                $folioCheque = $rs["folioCheque"];
                $empleado = $rs["empleado"];
                $proveedor = $rs["proveedor"];
                $beneficiario = $rs["beneficiario"];
                $observaciones = $rs["observaciones"];
                $estatus = $rs["estatus"];

                $elemento = new Comprobacion();
                $elemento->setAll($idComprobacion,$idPeriodo, $folio, $fecha, $descripcion, $montoComprobacion,  $destino, $idProyecyo, $idCheque, $folioCheque, $empleado, $proveedor, $beneficiario, $observaciones, $estatus);

                array_push($lista, $elemento);

        }

        return $lista;
    }
    
    public function obtieneListadoDestino($dest) {
        
	$lista = array();

	$query = "select ".
            "com.COM_ID_COMPROBACION as idComprobacion, ".  
            "com.CPE_ID_PERIODO as idPeriodo, ". 
            "com.COM_FOLIOCOMPROB as folio, ". 
            "com.COM_DESCRIPCION as descripcion, ".  
            "com.COM_FECHA as fecha, ".
            "com.COM_MONTO as montoComprobacion, ".  
            "com.COM_DESTINO as destino, ". 
            "com.CPR_ID_PROYECTO as idProyecyo, ".  
            "com.CHE_ID_CHEQUE as idCheque, ". 
            "(select cte.CEM_NOMBRE||' '|| cte.CEM_APPATERNO ||' '|| cte.CEM_APMATERNO from cat_empleado cte where p.cem_id_empleado=cte.cem_id_empleado) as empleado, ".
            "(select pro.CPR_PROVEEDOR from sie_cat_proveedores pro WHERE p.cpr_id_proveedor=pro.cpr_id_proveedor) as proveedor, ".
            "p.spr_nombre_ch_otro as beneficiario,".
            "che.CHE_FOLIO as folioCheque, ".
            "com.COM_OBSERVACIONES as observaciones,".  
            "com.COM_ESTATUS as estatus ".
            "from sie_cheques che, sie_solicitud_presup p, sie_comprobaciones com ". 
            "where com.CHE_ID_CHEQUE = che.CHE_ID_CHEQUE ".
            "and che.SPR_ID_SOLICITUD = p.SPR_ID_SOLICITUD and p.CEM_ID_EMPLEADO = cte.CEM_ID_EMPLEADO ".
            "and p.CPR_ID_PROVEEDOR = pro.CPR_ID_PROVEEDOR ".
            "and com.COM_PRE_ESTATUS = 1 and com.COM_DESTINO='".$dest."'";

        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){

                $idComprobacion = $rs["idComprobacion"]; 
                $idPeriodo = $rs["idPeriodo"]; 
                $folio = $rs["folio"]; 
                $descripcion = $rs["descripcion"];
                $fecha = $rs["fecha"];
                $montoComprobacion = $rs["montoComprobacion"]; 
                $destino = $rs["destino"];
                $idProyecyo = $rs["idProyecyo"]; 
                $idCheque = $rs["idCheque"]; 
                $folioCheque = $rs["folioCheque"];
                $empleado = $rs["empleado"];
                $proveedor = $rs["proveedor"];
                $beneficiario = $rs["beneficiario"];
                $observaciones = $rs["observaciones"];
                $estatus = $rs["estatus"];

                $elemento = new Comprobacion();
                $elemento->setAll($idComprobacion,$idPeriodo, $folio, $fecha, $descripcion, $montoComprobacion,  $destino, $idProyecyo, $idCheque, $folioCheque, $empleado, $proveedor, $beneficiario, $observaciones, $estatus);

		array_push($lista, $elemento);

	}

        return $lista;
    }
    
    public function obtieneListado2($inicio,$fin,$periodo)
    {
        $lista = array();
		
	$query = "select com.COM_ID_COMPROBACION as idComprobacion, com.CPE_ID_PERIODO as idPeriodo, com.COM_FOLIOCOMPROB as folio, ".
            "com.COM_DESCRIPCION as descripcion, com.COM_FECHA as fecha, com.COM_MONTO as montoComprobacion, com.COM_DESTINO as destino, ". 
            "com.CPR_ID_PROYECTO as idProyecyo, com.CHE_ID_CHEQUE as idCheque, (select cte.CEM_NOMBRE||' '|| cte.CEM_APPATERNO ||' '|| cte.CEM_APMATERNO from cat_empleado cte where p.cem_id_empleado=cte.cem_id_empleado) as empleado, ".
            "(select pro.CPR_PROVEEDOR from sie_cat_proveedores pro WHERE p.cpr_id_proveedor=pro.cpr_id_proveedor) as proveedor,p.spr_nombre_ch_otro as beneficiario,che.CHE_FOLIO as folioCheque, com.COM_OBSERVACIONES as observaciones, ".
            "com.COM_ESTATUS as estatus ".
            "from sie_cheques che, sie_solicitud_presup p, sie_comprobaciones com ".  
            "where p.SPR_ID_SOLICITUD=che.SPR_ID_SOLICITUD AND  che.CHE_ID_CHEQUE= com.CHE_ID_CHEQUE ".    
            "and com.COM_PRE_ESTATUS = 1 and che.che_estatusborrado=1 and p.spr_pre_estatus=1  ".
            "and com.COM_FECHA BETWEEN '".$inicio."' AND '".$fin."' " .
            "and com.CPE_ID_PERIODO=".$periodo;
        
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){

                $idComprobacion = $rs["idComprobacion"]; 
                $idPeriodo = $rs["idPeriodo"]; 
                $folio = $rs["folio"]; 
                $descripcion = $rs["descripcion"];
                $fecha = $rs["fecha"];
                $montoComprobacion = $rs["montoComprobacion"]; 
                $destino = $rs["destino"];
                $idProyecyo = $rs["idProyecyo"]; 
                $idCheque = $rs["idCheque"]; 
                $folioCheque = $rs["folioCheque"];
                $empleado = "";
                if($rs["empleado"]!=null){
                    $empleado = $rs["empleado"];
                }
                $proveedor = "";
                if($rs["proveedor"]!=null){
                    $proveedor = $rs["proveedor"];
                }
                $beneficiario = $rs["beneficiario"];
                $observaciones = $rs["observaciones"];
                $estatus = $rs["estatus"];

                $elemento = new Comprobacion();
                $elemento->setAll($idComprobacion,$idPeriodo, $folio, $fecha, $descripcion, $montoComprobacion,  $destino, $idProyecyo, $idCheque, $folioCheque, $empleado, $proveedor, $beneficiario, $observaciones, $estatus);

		array_push($lista, $elemento);

	}

        return $lista; 
    }
    
    public function obtieneListadoDestino2($inicio,$fin,$periodo,$dest)
    {
	$lista = array();
        $inicio = date("Y-m-d",strtotime($inicio));
        $fin = date("Y-m-d",strtotime($fin));
        if($periodo ==null){
            $periodo = 'null';
        }
		
	$query = "select com.COM_ID_COMPROBACION as idComprobacion, com.CPE_ID_PERIODO as idPeriodo, com.COM_FOLIOCOMPROB as folio, ".
            "com.COM_DESCRIPCION as descripcion, com.COM_FECHA as fecha, com.COM_MONTO as montoComprobacion, com.COM_DESTINO as destino, ". 
            "com.CPR_ID_PROYECTO as idProyecyo, com.CHE_ID_CHEQUE as idCheque, (select CONCAT_WS(' ',cte.CEM_NOMBRE,cte.CEM_APPATERNO, cte.CEM_APMATERNO) as empleado from cat_empleado cte where p.cem_id_empleado=cte.cem_id_empleado) as empleado, ".
            "(select pro.CPR_PROVEEDOR from sie_cat_proveedores pro WHERE p.cpr_id_proveedor=pro.cpr_id_proveedor) as proveedor,p.spr_nombre_ch_otro as beneficiario,che.CHE_FOLIO as folioCheque, com.COM_OBSERVACIONES as observaciones, ".
            "com.COM_ESTATUS as estatus ".
            "from sie_cheques che, sie_solicitud_presup p, sie_comprobaciones com ".  
            "where p.SPR_ID_SOLICITUD=che.SPR_ID_SOLICITUD AND  che.CHE_ID_CHEQUE= com.CHE_ID_CHEQUE ".    
            "and com.COM_PRE_ESTATUS = 1 and che.che_estatusborrado=1 and p.spr_pre_estatus=1 and com.com_destino='".$dest."' ".
            "and com.COM_FECHA BETWEEN '".$inicio."' AND '".$fin."' " .
            "and com.CPE_ID_PERIODO=".$periodo;
        
        $catalogo = new Catalogo();
        $result = $catalogo->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){

                $idComprobacion = $rs["idComprobacion"]; 
                $idPeriodo = $rs["idPeriodo"]; 
                $folio = $rs["folio"]; 
                $descripcion = $rs["descripcion"];
                $fecha = $rs["fecha"];
                $montoComprobacion = $rs["montoComprobacion"]; 
                $destino = $rs["destino"];
                $idProyecyo = $rs["idProyecyo"]; 
                $idCheque = $rs["idCheque"]; 
                $folioCheque = $rs["folioCheque"];
                $empleado = "";
                if($rs["empleado"]!=null){
                    $empleado = $rs["empleado"];
                }
                $proveedor = "";
                if($rs["proveedor"]!=null){
                    $proveedor = $rs["proveedor"];
                }
                $beneficiario = $rs["beneficiario"];
                $observaciones = $rs["observaciones"];
                $estatus = $rs["estatus"];

                $elemento = new Comprobacion();
                $elemento->setAll($idComprobacion,$idPeriodo, $folio, $fecha, $descripcion, $montoComprobacion,  $destino, $idProyecyo, $idCheque, $folioCheque, $empleado, $proveedor, $beneficiario, $observaciones, $estatus);

		array_push($lista, $elemento);

	}
        return $lista; 
    }
    
    public function obtieneElemento($idElemento){
		
        $elemento = new Comprobacion();

        $query = "select ".
            "com.COM_ID_COMPROBACION as idComprobacion, ".  
            "com.CPE_ID_PERIODO as idPeriodo, ". 
            "com.COM_FOLIOCOMPROB as folio, ". 
            "com.COM_DESCRIPCION as descripcion, ".  
            "com.COM_FECHA as fecha, ".
            "com.COM_MONTO as montoComprobacion, ".  
            "com.COM_DESTINO as destino, ". 
            "com.CPR_ID_PROYECTO as idProyecyo, ".  
            "com.CHE_ID_CHEQUE as idCheque, ". 
            "cte.CEM_NOMBRE||' '|| cte.CEM_APPATERNO ||' '|| cte.CEM_APMATERNO as empleado,".
            "pro.CPR_PROVEEDOR as proveedor,".
            "che.CHE_BENEFICIARIO as beneficiario,".
            "che.CHE_FOLIO as folioCheque, ".
            "com.COM_OBSERVACIONES as observaciones,".  
            "com.COM_ESTATUS as estatus ".
            "from sie_cheques che, sie_comprobaciones com,". 
            "cat_empleado cte, sie_cat_proveedores pro  ".
            "where com.CHE_ID_CHEQUE = che.CHE_ID_CHEQUE ".
            //"and che.CEM_ID_EMPLEADO = cte.CEM_ID_EMPLEADO ".
            //"and che.CPR_ID_PROVEEDOR = pro.CPR_ID_PROVEEDOR".
            "and com.COM_PRE_ESTATUS = 1 " .
            "and com.COM_ID_COMPROBACION = " .$idElemento;
        
        $catalogo = new Catalogo();
        
        $result = $catalogo->obtenerLista($query);

        if($rs = mysql_fetch_array($result)){

                $idComprobacion = $rs["idComprobacion"]; 
                $idPeriodo = $rs["idPeriodo"]; 
                $folio = $rs["folio"]; 
                $descripcion = $rs["descripcion"];
                $fecha = $rs["fecha"];
                $montoComprobacion = $rs["montoComprobacion"]; 
                $destino = $rs["destino"];
                $idProyecyo = $rs["idProyecyo"]; 
                $idCheque = $rs["idCheque"]; 
                $folioCheque = $rs["folioCheque"];
                $empleado = "";
                if($rs["empleado"]!=null){
                    $empleado = $rs["empleado"];
                }
                $proveedor = "";
                if($rs["proveedor"]!=null){
                    $proveedor = $rs["proveedor"];
                }
                $beneficiario = $rs["beneficiario"];
                $observaciones = $rs["observaciones"];
                $estatus = $rs["estatus"];

                $elemento = new Comprobacion();
                $elemento->setAll($idComprobacion,$idPeriodo, $folio, $fecha, $descripcion, $montoComprobacion,  $destino, $idProyecyo, $idCheque, $folioCheque, $empleado, $proveedor, $beneficiario, $observaciones, $estatus);

	}

        return $elemento; 
    }
    
    public function guardaElemento($elemento) {
		
        $con=new Catalogo();
        
        $fecha = "";
		
        if($elemento->getFecha()!=null)
        {        $fecha= "'".date("Y-m-d",strtotime($elemento->getFecha()))."'"; }
        else
        {        $fecha = "null"; }
                
        $query=strtolower("insert into sie_comprobaciones(CPE_ID_PERIODO,COM_FOLIOCOMPROB,COM_DESCRIPCION,COM_MONTO,COM_DESTINO,CPR_ID_PROYECTO,CHE_ID_CHEQUE,COM_ESTATUS,COM_OBSERVACIONES,COM_PRE_ESTATUS,COM_FECHA) values (".
            $elemento->getIdPeriodo().", ".
            $elemento->getFolio().", '".
            mb_strtoupper($elemento->getDescripcion(),'UTF-8')."', ".
            $elemento->getMontoComprobacion().", '".
            $elemento->getDestino()."', ".
            $elemento->getIdProyecto().", ".
            $elemento->getIdCheque().", '".
            $elemento->getEstatus()."', '".
            mb_strtoupper($elemento->getObservaciones(),'UTF-8')."', ".
            "1, ".$fecha. ")");
        
        $res=$con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }	
    }
    
    public function actualizaElemento($elemento) {
		
	$con=new Catalogo();
        
        $fecha = "";
		
        if($elemento->getFecha()!=null)
        {        $fecha= "'".date("Y-m-d",strtotime($elemento->getFecha()))."'"; }
        else
        {        $fecha = "null"; }
        
	$query=strtolower("update sie_comprobaciones set ".
            "CPE_ID_PERIODO = " . $elemento->getIdPeriodo() . ", ".
            "COM_FOLIOCOMPROB = " . $elemento->getFolio() . ", ".
            "COM_DESCRIPCION = '" .  mb_strtoupper($elemento->getDescripcion(),'UTF-8') . "', ".
            "COM_MONTO = " . $elemento->getMontoComprobacion() . ", ".
            "COM_DESTINO = '" . $elemento->getDestino() . "', ".
            "CPR_ID_PROYECTO = " . $elemento->getIdProyecto() . ", ".
            "CHE_ID_CHEQUE = " . $elemento->getIdCheque() . ", ".
            "COM_ESTATUS = '" . $elemento->getEstatus() . "', ".
            "COM_OBSERVACIONES = '" . mb_strtoupper($elemento->getObservaciones(),'UTF-8') . "', ".
            "COM_FECHA=" . $fecha . " " .
            "where COM_ID_COMPROBACION = " . $elemento->getIdComprobacion());
        $res = $con->obtenerLista($query);
		
	if($res == "1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function eliminaElemento($idElemento){
		
        $con=new Catalogo();
        $query="update sie_comprobaciones set com_pre_estatus = 0 where com_id_comprobacion = ".$idElemento;
        $res = $con->obtenerLista($query);

        if($res=="1")
        {	return true; }
        else
        {	return false; }
		
    }
    
    public function obtieneId() {
        $con=new Catalogo();
        $idComprobacion = 0;
        $query="SELECT MAX(COM_ID_COMPROBACION) as idComprobacion FROM sie_comprobaciones";

        $result = $con->obtenerLista($query);

        while ($rs = mysql_fetch_array($result)){
            $idComprobacion = $rs["idComprobacion"];
        }

        return $idComprobacion;
    }
}