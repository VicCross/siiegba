<!DOCTYPE html>
<html lang = "en">
<head>
 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
<?php
    $MYSQL_server = "mysql1002.mochahost.com";
    $MYSQL_usuario = "ccilidon_mba13";
    $MYSQL_contraseña = "s11egba2015";
    $MYSQL_bd = "ccilidon_siiegba_pruebas";
    $conexion = mysqli_connect ($MYSQL_server, $MYSQL_usuario, $MYSQL_contraseña, $MYSQL_bd)
        or die ("Error en la Conexion");
?>
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Presupuesto'],
          ['Ingresos',     128988148.89],
          ['Egresos',      10700],
        ]);

        var options = {
          title: 'Ingresos y Egresos',
          colors: ['#2d862d', '#79d279']
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>
        <meta charset= "UTF-8">
        <title>Dirección</title>
        <link rel="stylesheet" type="text/css" href="../css/estilos_indicadores.css">
        <link rel="stylesheet" type="text/css" href="../css/fonts.css">
        <link rel="stylesheet" type="text/css" href="../css/style.css" />
        <script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
        <link rel="stylesheet" type="text/css" href="../css/lista.css">
        <script type="text/javascript" src="../media/js/complete.js"></script>
        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
        <script type="text/javascript" src="../js/lista.js"></script>
</head>
<body>
<div class="liston">Indicadores</div>
<div class="contenido">
    <div class= ingresovegre> 
        <div class= "ingrvegre"><p>Ingresos y Egresos Anuales 2016</p></div>
        <div class= "ingrvegre_tabla">

                        <?php 
                            
                            $consulta1 = "SELECT SUM(saldoInicial) as suma FROM ccilidon_siiegba_pruebas.sie_cat_linea_accion where periodo = '42'";
                            $ingresos =  mysqli_query( $conexion, $consulta1)
                            or die ("Error en la Con");
                            $consulta2 = "SELECT SUM(importe) as suma2 FROM ccilidon_siiegba_pruebas.sie_cat_gestpresup";
                            $egresos =  mysqli_query( $conexion, $consulta2)
                            or die ("Error en la Con");
                            echo '<table border="1" cellspacing="0">';
                            while ($extraido = mysqli_fetch_array($ingresos)) {
                            echo '<tr>';
                            echo '<td width= "30%"> Ingreso total MPBA </td>';
                            echo '<td width= "30%"> $'.$extraido ['suma'].'</td>';
                            echo '<td width= "30%"> Consultar </td>';
                            echo '</tr>';
                            }
                            while ($extraido = mysqli_fetch_array($egresos)) {
                            echo '<tr>';
                            echo '<td width= "30%"> Egreso total MPBA </td>';
                            echo '<td width= "30%"> $ '.$extraido ['suma2'].'</td>';
                            echo '<td width= "30%"> Consultar </td>';
                            echo '</tr>';
                            }

                            echo '</table>';
                        ?>

        </div>          
    </div>

<div class= ingresovegre2016> 
        <div class= "ingrvegre2016"><p>Ingresos y Egresos Comparativos 2016</p></div>
        <div class= "ingrvegre_tabla2016">

                        <?php 
                            
                            $consulta1 = "SELECT SUM(saldoInicial) as suma FROM ccilidon_siiegba_pruebas.sie_cat_linea_accion where periodo = '42'";
                            $ingresos =  mysqli_query( $conexion, $consulta1)
                            or die ("Error en la Con");
                            $consulta2 = "SELECT SUM(importe) as suma2 FROM ccilidon_siiegba_pruebas.sie_cat_gestpresup";
                            $egresos =  mysqli_query( $conexion, $consulta2)
                            or die ("Error en la Con");
                            echo '<table border="1" cellspacing="0" cellpadding="3" align="center">';
                            while ($extraido = mysqli_fetch_array($ingresos)) {
                            echo '<tr>';
                            echo '<td width= "30%"> Ingreso total MPBA </td>';
                            echo '<td width= "30%"> $ '.$extraido ['suma'].'</td>';
                            echo '<td width= "30%"> Consultar </td>';
                            echo '</tr>';
                            }
                            while ($extraido = mysqli_fetch_array($egresos)) {
                            echo '<tr>';
                            echo '<td width= "30%"> Egreso total MPBA </td>';
                            echo '<td width= "30%"> $ '.$extraido ['suma2'].'</td>';
                            echo '<td width= "30%"> Consultar </td>';
                            echo '</tr>';
                            }

                            echo '</table>';
                        ?>

        </div>          
    </div>

<div class= ingresovegre2015> 
        <div class= "ingrvegre2015"><p>Ingresos y Egresos Comparativos 2015</p></div>
        <div class= "ingrvegre_tabla2015">

                        <?php 
                            
                            $consulta1 = "SELECT SUM(saldoInicial) as suma FROM ccilidon_siiegba_pruebas.sie_cat_linea_accion where periodo = '41'";
                            $ingresos =  mysqli_query( $conexion, $consulta1)
                            or die ("Error en la Con");
                            $consulta2 = "SELECT SUM(importe) as suma2 FROM ccilidon_siiegba_pruebas.sie_cat_gestpresup where fechaSol >= '31-12-2015'";
                            $egresos =  mysqli_query( $conexion, $consulta2)
                            or die ("Error en la Con");
                            echo '<table border="1" cellspacing="0" cellpadding="3" align="center">';
                            while ($extraido = mysqli_fetch_array($ingresos)) {
                            echo '<tr>';
                            echo '<td width= "30%"> Ingreso total MPBA </td>';
                            echo '<td width= "30%"> $ '.$extraido ['suma'].'</td>';
                            echo '<td width= "30%"> Consultar </td>';
                            echo '</tr>';
                            }
                            while ($extraido = mysqli_fetch_array($egresos)) {
                            echo '<tr>';
                            echo '<td width= "30%"> Egreso total MPBA </td>';
                            echo '<td width= "30%"> $ '.$extraido ['suma2'].'</td>';
                            echo '<td width= "30%"> Consultar </td>';
                            echo '</tr>';
                            }

                            echo '</table>';

                            mysqli_close($conexion);
                        ?>

        </div>          
    </div>

    <div class="siguiente1" align="center"> <a href='indicadores_3.php' target= "destino">Siguiente</a> </div>
    <div class="principal1" align="center"> <a href='indicadores_general.php' target= "destino">Principal</a> </div>
    <div id="piechart" class="grafico2" style= " " ></div>
</div>
</body>
</html>

