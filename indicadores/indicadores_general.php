<!DOCTYPE html>
<html lang = "en">
<head>
 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
<?php
	$MYSQL_server = "mysql1002.mochahost.com";
	$MYSQL_usuario = "ccilidon_mba13";
	$MYSQL_contraseña = "s11egba2015";
	$MYSQL_bd = "ccilidon_siiegba_pruebas";
	$conexion = mysqli_connect ($MYSQL_server, $MYSQL_usuario, $MYSQL_contraseña, $MYSQL_bd)
		or die ("Error en la Conexion");
?>
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Presupuesto'],
          ['Ingresos',     128988148.89],
          ['Egresos',      10700],
        ]);

        var options = {
          title: 'Ingresos y Egresos',
          colors: ['#2d862d', '#79d279']
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>
		<meta charset= "UTF-8">
		<title>Dirección</title>
		<link rel="stylesheet" type="text/css" href="../css/estilos_indicadores.css">
		<link rel="stylesheet" type="text/css" href="../css/fonts.css">
		<link rel="stylesheet" type="text/css" href="../css/style.css" />
        <script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
        <link rel="stylesheet" type="text/css" href="../css/lista.css">
        <script type="text/javascript" src="../media/js/complete.js"></script>
        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
        <script type="text/javascript" src="../js/lista.js"></script>
	</head>
		<body>
		<div class="caja">
		<div class="liston">Indicadores</div>
			<div class="tablas">
			
				<div class= ingreso> 
			
					<div class= "ingr"><p>Ingresos</p></div>
					<div class= "ingr_INBA"><p>INBA</p></div>
					<div class= "ingr_tabla">
						<?php 
							
							$consulta = mysqli_query ( $conexion, "SELECT * from sie_ingresos_INBA")
								or die ("Error en la Con");
								echo '<table border="1" cellspacing="0" cellpadding="3" class="dataTable" align="right">';
								echo '<tr>';
								echo '<th id= cantidad>MONTO</th>';
								echo '<th id= fecha>FECHA</th>';
								echo '<th id= Documento>ARCHIVO</th>';
								echo '</tr>';
							while ($extraido = mysqli_fetch_array($consulta)){
								echo '<tr>';
								echo '<td> $ '.$extraido ['monto'].'</td>';
								echo '<td>'.$extraido ['fecha'].'</td>';
								echo '<td><a href="'.$extraido ['archivo'].'" target= "destino">Abrir Archivo</a></td>';
								echo '</tr>';
								}
								echo '</table>';
				 		?>
					</div>			
				</div>
			
				<div class= egreso>
			
					<div class= "egre"><p>Egresos</p></div>
					<div class= "egre_INBA"><p>INBA</p></div>
					<div class= "egre_tabla">	
						<?php
							$consulta = mysqli_query ( $conexion, "SELECT * from sie_cat_gestpresup")
								or die ("Error en la Con");
								echo '<table border="1" cellspacing="0" cellpadding="3" class="dataTable" align="right">';
								echo '<tr>';
								echo '<th id= cantidad>MONTO</th>';
								echo '<th id= fecha>FECHA</th>';
								echo '<th id= Documento>ARCHIVO</th>';
								echo '</tr>';

						while ($extraido = mysqli_fetch_array($consulta)) {
								echo '<tr>';
								echo '<td> $ '.$extraido ['importe'].'</td>';
								echo '<td>'.$extraido ['fechaSol'].'</td>';
								echo '<td><a href="'.$extraido ['entregable'].'" target= "destino">Archivo</a></td>';
								echo '</tr>';
								}
								echo '</table>';
				 		?>
					</div>
				</div>

				<div class= patronato> 
			
					<div class= "ingr_patronato"><p>Patronato</p></div>
					<div class= "patronato_tabla">
						<?php
							$consulta = mysqli_query ( $conexion, "SELECT * from sie_ingresos_patronato")
								or die ("Error en la Con");
								echo '<table border="1" cellspacing="0" cellpadding="3" class="dataTable" align="right">';
								echo '<tr>';
								echo '<th width= "24%" id= nombre>NOMBRE</th>';
								echo '<th width= "24%" id= cantidad>MONTO</th>';
								echo '<th width= "24%" id= fecha>FECHA</th>';
								echo '<th width= "24%" id= Documento>ARCHIVO</th>';
								echo '</tr>';
							while ($extraido = mysqli_fetch_array($consulta)) {
								echo '<tr>';
								echo '<td>'.$extraido ['nombrePatrocinador'].'</td>';
								echo '<td> $ '.$extraido ['monto'].'</td>';
								echo '<td>'.$extraido ['fecha'].'</td>';
								echo '<td><a href="'.$extraido ['archivo'].'" target= "destino">Archivo</a></td>';
								echo '</tr>';
								}
								echo '</table>';
								mysqli_close($conexion);
						 ?>
					</div>
					</div>
					</div>
		         
    </div>		
				<div class="siguiente" align="center"> <a href='indicadores_2.php' target= "destino">Siguiente</a> </div>
				<div id="piechart" class="grafico" style= " " ></div>
	
		</div>
	</body>

</html>   