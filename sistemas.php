	<!DOCTYPE html>
<html lang = "en">
<head>
<body>
	</head> 
		<meta charset= "UTF-8">
		<title>SIIEGBA PRO</title>
		<link rel="stylesheet" type="text/css" href="css/estilos.css">
		<link rel="stylesheet" type="text/css" href="css/estilos_lateral.css">
		<link rel="stylesheet" type="text/css" href="css/fonts.css">
	</head>
		 <header>
        <div class="menu_bar">
            <a href="#" class="bt-menu"><span class= "icon-menu3"></span>Areas</a>
        </div>  
        <div class="logo">
            <img clas="S" src="img/SIIEGBA_01.png" width="100%"/>
        </div>
         <div class=menu_de_area>
            <nav>
            <ul class="menu_area">
                    <li class= "submenu">
                        <a href= "#"><span class="icon-user-tie"></span>Directivas
                        <span class="caret icon-circle-down"></span></a>
                        <ul class="children"> 
                            <li id="uno"><a class="uno" href="direccion.php">Dirección</a></li>
                            <li id="uno"><a class="uno" href="#">C. Operativa</a></li>
                            <li id="uno"><a class="uno" href="#">R. Públicas</a></li>
                            <li id="uno"><a class="uno" href="sistemas.php">Sistemas</a></li>
                        </ul>
                    </li>
                    <li class= "submenu"><a href= "#"><span class="icon-cogs"></span>Tecnicas
                    <span class="caret icon-circle-down"></span></a>
                        <ul class= "children">
                                <li id="dos"><a class="dos" href="#">S. Tecnica</a></li>
                                <li id="dos"><a class="dos" href="#">Indicadores</a></li>
                                <li id="dos"><a class="dos" href="#">Exhibición</a></li>
                                <li id="dos"><a class="dos" href="#">Registro</a></li>
                                <li id="dos"><a class="dos" href="#">Fotografia</a></li>
                                <li id="dos"><a class="dos" href="#">Comunicación</a></li>
                                <li id="dos"><a class="dos" href="#">Difusión</a></li>
                                <li id="dos"><a class="dos" href="#">Electrónicos</a></li>
                                <li id="dos"><a class="dos" href="#">Prensa</a></li>
                                <li id="dos"><a class="dos" href="#">Mediación</a></li>
                                <li id="dos"><a class="dos" href="#">Academicos</a></li>
                                <li id="dos"><a class="dos" href="#">Editorial</a></li>
                                <li id="dos"><a class="dos" href="#">Arquitectura</a></li>
                                <li id="dos"><a class="dos" href="#">Museografía</a></li>
                        </ul>
                    </li>
                    <li class= "submenu"><a href= "#"><span class="icon-office"></span>Administrativas
                    <span class="caret icon-circle-down"></span></a>
                        <ul class= "children">
                                <li id="tres"><a class="tres" href="#">Administración </a></li>
                                <li id="tres"><a class="tres" href="#">Presupuesto</a></li>
                                <li id="tres"><a class="tres" href="#">R. Humanos</a></li>
                                <li id="tres"><a class="tres" href="#">R. Materiales</a></li>
                                <li id="tres"><a class="tres" href="#">R. Financieros</a></li>
                                <li id="tres"><a class="tres" href="#">Jurídico</a></li>
                                <li id="tres"><a class="tres" href="#">Custodios</a></li>
                                <li id="tres"><a class="tres" href="#">Seguridad</a></li>
                        </ul>

                    </li>
                    <li class= "submenu"><a href= "#"><span class="icon-users"></span>Amigos
                    <span class="caret icon-circle-down"></span></a>
                        <ul class= "children">      
                                <li id="cuatro"><a class="cuatro" href="#">Amigos P.</a></li>
                                <li id="cuatro"><a class="cuatro" href="#">Amigos D. E.</a></li>
                                <li id="cuatro"><a class="cuatro" href="#">Amigos S.</a></li>
                                <li id="cuatro"><a class="cuatro" href="#">Circulo Joven</a></li>
                                <li id="cuatro"><a class="cuatro" href="#">Tienda</a></li>
                        </ul>       
                    </li>
                    <li><a href= "#"><span class="icon-library"></span>CULTURA</a></li>
                    <li><a href= "#"><span class="icon-library"></span>INBA</a></li>
                </ul>
        </nav>
        </div>
        </header>
		<body>
		<div class="caja">
			<div class="area_m1">Sistemas</div>
			<div class="area_m2"> Indicadores y Aplicaciones</div>

			<div class="caja2_1">
			
			<p>Gestión de Presupuesto</p>

				<div class="caja2_2">
					
					<a href= "GestionPresupuesto/nvaGestion.php" class="caja_boton">Redactar Gestión <i class="icon-pencil"></i></a>
					<p class="caja2_3_1">Solicitudes Realizadas (Generadas) <input class="solicitud" value= 0></p>
					<p class="caja2_3_2">Solicitudes en lider de proyecto <input class="solicitud2" value= 0></p>
					<p class="caja2_3_3">Solicitudes en  Sub-direccion Téc <input class="solicitud3" value= 0></p>
					<p class="caja2_3_4">Solicitudes en control presupuestal <input class="solicitud4" value= 0></p>
					<p class="caja2_3_5">Solicitudes aprobadas  <input class="solicitud5" value= 0></p>
					<p class="caja2_3_6">Solicitudes pagadas <input class="solicitud6" value= 0></p>

				</div>

			</div>


				<div class="caja3_1">
			
			<p>Presupuesto por área</p>

				<div class="caja3_2">
					
					<a href= "#" class="caja3_boton">Gestionar Presupuesto<i class="icon-coin-dollar"></i></a>
					<p class="caja3_3_1">Solicitudes Aceptadas </p>
					<p class="caja3_3_2">Solicitudes Denegadas</p>

				</div>

			</div>

		</div>

		 <div class= "lateral">
            <ul>
                <li><a href="../siiegba/Principal.php" class="icon-home" title="Inicio"></a></li>
                <li><a href="#" class="icon-stats-bars" title="Indicadores"></a></li>
                <li><a href="#" class="icon-books" title="Aplicaciones"></a></li>
                <li><a href="Cerrar.php" class="icon-exit" title="Salir"></a></li>
            </ul>
        </div>

	</body>
</html>
