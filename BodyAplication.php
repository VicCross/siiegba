<?php
session_start();
if (isset($_SESSION['id'])) {
    include_once("src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");	
	include_once("src/classes/Menu.class.php");
    include_once("src/classes/Catalogo.class.php");
    //Obtenemos el valor del color, su id es el número 5...
    $parametro = new ParametroDaoJdbc();
    $parametro = $parametro->obtieneElemento(5);
    $id = $_SESSION['id'];    
    ?>
<!DOCTYPE HTML>
    <html>
        <head>
            <title>MENU</title>
            <meta http-equiv="X-UA-Compatible" content="IE=8" />
            <meta http-equiv="X-UA-Compatible" content="IE=7" />
            <link rel="stylesheet" type="text/css" href="css/pro_dop_1.css">
            <link rel="stylesheet" type="text/css" href="css/estilos_lateral.css">
            <script language="JavaScript" type="text/javascript" src="js/jquery-1.7.2.js" ></script>
            <script language="javascript" type="text/javascript" src="js/session.js"></script>
            <!-- Este Script nos ayuda a usar el valor del color que se encuentra como parámetro -->
            <script>
                $(document).ready(function() {
                    $(".menu ul").css({
                        background: "<?php echo $parametro->getValor(); ?>"
                    });
                    $("#.menu a").css({
                        background: "<?php echo $parametro->getValor(); ?>"
                    });
                    
                    $(".menu ul ul").css({
                        position: "absolute",
                        display: "none"
                    });

                    $(".menu li").hover(function() {
                        $(this).find(' > ul').stop(true, true).slideDown('fast');
                    }, function() {
                        $(this).find(' > ul').stop(true, true).slideUp('fast');
                    });
                });
				
            </script>
            
        </head>
        <header>
        <div class="menu_bar">
            <a href="#" class="bt-menu"><span class= "icon-menu3"></span>Areas</a>
        </div>  
        <div class="logo">
            <img clas="S" src="img/SIIEGBA_01.png" width="100%"/>
        </div>
        <div class=menu_de_area>
            <nav>
            <ul class="menu_area">
                    <li class= "submenu">
                        <a href= "#"><span class="icon-user-tie"></span>Directivas
                        <span class="caret icon-circle-down"></span></a>
                        <ul class="children"> 
                            <li id="uno"><a class="uno" href="direccion.php" target= "destino">Dirección</a></li> 
                            <li id="uno"><a class="uno" href="#">C. Operativa</a></li>
                            <li id="uno"><a class="uno" href="#">R. Públicas</a></li>
                            <li id="uno"><a class="uno" href="sistemas.php">Sistemas</a></li>
                        </ul>
                    </li>
                    <li class= "submenu"><a href= "#"><span class="icon-cogs"></span>Tecnicas
                    <span class="caret icon-circle-down"></span></a>
                        <ul class= "children">
                                <li id="dos"><a class="dos" href="#">S. Tecnica</a></li>
                                <li id="dos"><a class="dos" href="#">Indicadores</a></li>
                                <li id="dos"><a class="dos" href="#">Exhibición</a></li>
                                <li id="dos"><a class="dos" href="#">Registro</a></li>
                                <li id="dos"><a class="dos" href="#">Fotografia</a></li>
                                <li id="dos"><a class="dos" href="#">Comunicación</a></li>
                                <li id="dos"><a class="dos" href="#">Difusión</a></li>
                                <li id="dos"><a class="dos" href="#">Electrónicos</a></li>
                                <li id="dos"><a class="dos" href="#">Prensa</a></li>
                                <li id="dos"><a class="dos" href="#">Mediación</a></li>
                                <li id="dos"><a class="dos" href="#">Academicos</a></li>
                                <li id="dos"><a class="dos" href="#">Editorial</a></li>
                                <li id="dos"><a class="dos" href="#">Arquitectura</a></li>
                                <li id="dos"><a class="dos" href="#">Museografía</a></li>
                        </ul>
                    </li>
                    <li class= "submenu"><a href= "#"><span class="icon-office"></span>Administrativas
                    <span class="caret icon-circle-down"></span></a>
                        <ul class= "children">
                                <li id="tres"><a class="tres" href="#">Administración </a></li>
                                <li id="tres"><a class="tres" href="#">Presupuesto</a></li>
                                <li id="tres"><a class="tres" href="#">R. Humanos</a></li>
                                <li id="tres"><a class="tres" href="#">R. Materiales</a></li>
                                <li id="tres"><a class="tres" href="#">R. Financieros</a></li>
                                <li id="tres"><a class="tres" href="#">Jurídico</a></li>
                                <li id="tres"><a class="tres" href="#">Custodios</a></li>
                                <li id="tres"><a class="tres" href="#">Seguridad</a></li>
                        </ul>

                    </li>
                    <li class= "submenu"><a href= "#"><span class="icon-users"></span>Amigos
                    <span class="caret icon-circle-down"></span></a>
                        <ul class= "children">      
                                <li id="cuatro"><a class="cuatro" href="#">Amigos P.</a></li>
                                <li id="cuatro"><a class="cuatro" href="#">Amigos D. E.</a></li>
                                <li id="cuatro"><a class="cuatro" href="#">Amigos S.</a></li>
                                <li id="cuatro"><a class="cuatro" href="#">Circulo Joven</a></li>
                                <li id="cuatro"><a class="cuatro" href="#">Tienda</a></li>
                        </ul>       
                    </li>
                    <li><a href= "#"><span class="icon-library"></span>CULTURA</a></li>
                    <li><a href= "#"><span class="icon-library"></span>INBA</a></li>
                </ul>
        </nav> 
        </div>
        
        </header>
        <body>
         <!--   <div class="menu" style='padding:0px;margin:0px;'  >
                <ul style='background-color:<?php echo $parametro->getValor(); ?> ;height:40px' style='padding:0px;margin:0px;'> 
                	<?php
						$menu = new Menu();
						//Para el tipo de menú que verá...
						$result = $menu -> obtieneMenu($id);
						while($rs = mysql_fetch_array($result)){
							$idMenu = $rs['id_perfil_menu'];
							$nombreMenu = $rs['nom_menu'];
							$referencia = $rs['referencia'];
							$conReferencia = "href=\"".$referencia."\" target=\"destino\" onClick=\"verificarSesion()\"";
							if($nombreMenu === "Salir") $conReferencia = "href=\"".$referencia."\" target=\"_parent\"";
							else if($referencia === "")$conReferencia = "href=\"\" onClick=\"return false;\"";
							echo('<li style="width:110px"><a '.$conReferencia.' style="width:110px">'.$nombreMenu.'</a>');
							//Para el submenu, si es que tiene
							$tieneSubMenus = $menu->tieneSubMenus($idMenu);
							if($tieneSubMenus === true){
								echo('<ul>');
								$resultSubMenu = $menu->obtieneSubMenu($idMenu);
								while($rsSubMenu = mysql_fetch_array($resultSubMenu)){
									$idSubMenu = $rsSubMenu['id_submenu'];
									$nombreSubMenu = $rsSubMenu['nom_submenu'];
									$refSubMenu = $rsSubMenu['referencia'];
									$conReferenciaSM = "href=\"".$refSubMenu."\" target=\"destino\" onClick=\"verificarSesion()\"";
									if($refSubMenu === "" || $refSubMenu == NULL) $conReferenciaSM = "href=\"\"";
									echo('<li><a '.$conReferenciaSM.' >'.$nombreSubMenu.'</a>');
									//Para ver si los submenus tienen submenus
									$tieneSSubMenus = $menu->tieneSSubMenus($idSubMenu);
									if($tieneSSubMenus === true){
										echo('<ul>');
										$resultSSubMenu = $menu->obtieneSSubMenu($idSubMenu);
										while($rsSSubMenu = mysql_fetch_array($resultSSubMenu)){
											$nombreSSubMenu = $rsSSubMenu['nom_ssubmenu'];
											$refSSubMenu = $rsSSubMenu['referencia'];
											echo('<li><a href="'.$refSSubMenu.'" target="destino" onClick="verificarSesion()">'.$nombreSSubMenu.'</a></li>');
										}										
										echo('</ul>');										
									}
									echo('</li>');
								}
								echo('</ul>');
							}
							echo('</li>');							
						}
						//Para el perfil
						$result = $menu->obtienePerfil($id);
						if($rs = mysql_fetch_array($result)){
							$perfil = $rs['cper_descripcion'];	
						}
					?>
                </ul>
                <?php
                    echo("<div  style='width:100%;padding-left:5px;padding-bottom:5px; font-size:9px;text-align:right;pading-top:0px;background-color: ".$parametro->getValor()." margin-top:0px;'> <img src='img/iconUser.jpg' width='15' height='15' style='' align='bottom' />  <b>Usuario: </b>" . $_SESSION["nombre"] . " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Perfil: </b>" . $perfil . " &nbsp;&nbsp;</div> ");
                ?>
            </div> -->
        </body>
    </html>
    <?php
} else {
    header("Location: index.php?err=2");
}
?>