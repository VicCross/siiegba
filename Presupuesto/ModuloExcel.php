<?php

session_start();
include_once("../src/mx/com/virreinato/dao/ProveedorDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/beans/Proveedor.class.php");
include_once("../src/mx/com/virreinato/dao/ProveedorAreaDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/beans/ProveedorArea.class.php");
include_once("../src/mx/com/virreinato/dao/PartidaDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/beans/Partida.class.php");
include_once("../src/mx/com/virreinato/dao/AreaDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/beans/Area.class.php");
include_once("../src/mx/com/virreinato/dao/LineaAccionDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/beans/LineaAccion.class.php");
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/dao/Presupuesto_AreaDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/beans/Presupuesto_Area.class.php");

$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
header('Content-Type: text/html; charset=UTF-8');
?>
<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width, user-scalable-no, initial-scale=1">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" href="css/bootstrap.min.css">

<link rel="stylesheet" href="css/theme.bootstrap.css">
<link rel="stylesheet" href="css/estilos.css">

<meta charset="utf-8">

 

<style>
 body {
	 color: #0B610B;
 }
 

table, th, td {
   
    padding: 5px;
	 
}

table th{
background-color:#04B431;
    color: white;
	vertical-align:middle;
	border-style:double;
}

table {
    
	vertical-align:middle;
	border-collapse:collapse;
	
}

th{
	text-align: center;
	vertical-align:middle;
}

</style>

</head>

<body>
<!--Inicia formulario -->
	<div class="container">
		
			<div class="table-responsive">
			
		         <table class="table table-hover table-condensed table-striped table-collapse" data-toggle="table">
					<thead>
						<tr>
							<th>Metas</th>
							<th>Descripción de Metas</th>
							<th>Nombre del proveedor de servicio</th>
							<th>
								<th>Cotización</th>
								<th>Mes</th>
								<th>Partida</th>
							</th>
							<th>
								<th>Pagos Foráneos</th>
								<th>Moneda</th>
							</th>
							<th>
								<th><font color="black">INBA 2015</th></font>
								<th><font color="black">PASIVO 2014</th></font>
							</th>
						</tr>
						
					</thead>
					<thead>
						<tr>
							<th><font color="black">NombreProyecto</th></font>
							<th></th>
							<th></th>
							<th>
								<th></th>
								<th></th>
								<th></th>
							</th>
							<th>
								<th></th>
								<th></th>
							</th>
							<th>
								<th></th>
								<th><font color="black">PAGADO</th></font>
							</th>
						</tr>
					</thead>
						<tbody>
							<?php
							    $totinba = 0.0; 
								$totpasivo = 0.0;
								
								$dao = new Presupuesto_AreaDaoJdbc();
								$lista = $dao->obtieneListadoPresup();
							    $elemento = new PresupuestoArea();
								$i = 1;
								
								foreach($lista as $elemento)
								{
									$proyecto = $elemento->getProyecto();
									$descripcion = $elemento->getDescripcion();
									$proveedor = $elemento->getProveedor();
									$cotizacion = $elemento->getCotizacion();
									$mes= $elemento->getMes();
									$partida = $elemento->getPartida();
									$pagosforaneos = $elemento->getPagosforaneos();
									$moneda = $elemento->getMoneda();
									$inba = $elemento->getInba();
									$pasivo = $elemento->getPasivo();
									
							 		$totinba += $inba;
						         	$totpasivo += $pasivo;
								
							
							
							?>
						
						
						
						
							<tr>
								<td><?php echo $proyecto;?></td>
								<td><?php echo $descripcion; ?></td>
								<td><?php echo $proveedor; ?></td>
								<td></td>
								<td><?php echo $cotizacion; ?></td>
								<td><?php echo $mes; ?></td>
								<td><?php echo $partida; ?></td>
								<td></td>
								<td><?php echo (number_format($pagosforaneos,2)); ?></td>
								<td><?php echo $moneda; ?></td>
								<td></td>
								<td>$<?php echo (number_format($inba,2)); ?></td>
								<td>$<?php echo (number_format($pasivo,2));?></td>
							</tr>
							
							
								<?php $i += 1; }?>	
								
							<tr>
								<th><font color="black">Totales:</th></font>
								<th></th>
								<th></th>
								<th>
									<th></th>
									<th></th>
									<th></th>
								</th>
								<th>
									<th></th>
									<th></th>
								</th>
								<th>
									<th>$<?php echo(number_format($totinba,2));?></th>
									<th>$<?php echo(number_format($totpasivo,2));?></th>
								</th>
							</tr>
					
						</tbody>
				</table>
	</div>
</div>	
		<?php include 'footerExcel.php' ?>	
<script src="js/jquery.js"></script>
<script src="js/table.js"></script>
<script src="js/bootstrap.min.js"></script>

<!-- tablesorter plugin 
<script src="js/jquery.tablesorter.js"></script>
<!-- tablesorter widget file - loaded after the plugin 
<script src="js/jquery.tablesorter.widgets.js"></script>

<!-- pager plugin 
<link rel="stylesheet" href="css/jquery.tablesorter.pager.css">
<script src="js/jquery.tablesorter.pager.min.js"></script>
-->

</body>
</html>