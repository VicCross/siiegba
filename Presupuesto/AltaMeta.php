<?php
session_start();
include_once("../src/mx/com/virreinato/dao/ProveedorDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/beans/Proveedor.class.php");
include_once("../src/mx/com/virreinato/dao/ProveedorAreaDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/beans/ProveedorArea.class.php");
include_once("../src/mx/com/virreinato/dao/PartidaDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/beans/Partida.class.php");
include_once("../src/mx/com/virreinato/dao/AreaDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/beans/Area.class.php");
include_once("../src/mx/com/virreinato/dao/LineaAccionDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/beans/LineaAccion.class.php");
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
header('Content-Type: text/html; charset=UTF-8');
?>
<!doctype html>
<head>
<meta name="viewport" content="width=device-width, user-scalable-no, initial-scale=1">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/estilos.css">
<link rel="stylesheet" href="css/bootstrapValidator.min.css">

<meta charset="utf-8">

<style>
 body {
	 color: #0B610B;
 }
 

.jumbotron{
  color: #0B610B;
  background-color: #CEF6CE;

}
</style>

<title>Alta</title>
<div class="container">
<div class="jumbotron">
<div class="container">
<h1> Alta de Presupuesto</h1>
<p> * Complete todos los campos.</p>
</div>
</div>

<script type="text/javascript">
	function cargarProveedores()
	{
		var area1=document.getElementById("area").value;
		var xHttp;
		if (window.XMLHttpRequest) 
		{
			xHttp = new XMLHttpRequest();
		}
		else
		{
			xHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xHttp.onreadystatechange = function()
		{
			if (xHttp.readyState == 4 && xHttp.status == 200) 
			{
				
				document.getElementById("ObtieneProv").innerHTML=xHttp.responseText;
			}
		};
		xHttp.open("GET","recuperaProv.php?Area="+ area1,true);
		xHttp.send();
	}
</script>

</head>

<body>
<!--Inicia formulario -->
<form id="registrationForm" method="post"  action="inserta.php">
  <div class="container form-control" id="alta" name="alta">
  <div class="row">
  
    <form method="post" action="inserta.php">
    	<div class="row" border="1px">
		    <!--Inicia campo Proyecto-->
		    <div class="form-group col-xs-12 col-sm-6 col-md-4">
			    <label for="cursar" id="ej">Proyecto:</label>
			    <select class="form-control" name="proyecto" id="proyecto">
			        <option value="" selected="selected">----Selecciona un Proyecto----</option>
			            <?php
			            $dao = new LineaAccionDaoJdbc();
					    $lista = $dao->obtieneListado();
					    $elemento = new LineaAccion();
			            foreach($lista as $elemento){
			            ?>
			            <option value="<?php echo($elemento->getId());?>">
			            <?php echo($elemento->getLineaAccion());?></option>
			            <?php } ?>
			    </select>
		    </div>
		    <!--Inicia campo Area-->
		    <div class="form-group col-xs-12 col-sm-6 col-md-4">
			    <label for="proyecto">Area: </label>
			    <select class="form-control" name="area" id="area" onchange="cargarProveedores()">
			      	<option value="" selected="selected">----Selecciona una Área----</option>
			        <?php $dao = new AreaDaoJdbc();
			        $lista = $dao->obtieneAreas();
			        $elemento = new Area();
					foreach($lista as $elemento){?>
			        <option value="<?php echo($elemento->getId());?>"><?php echo($elemento->getDescripcion());?></option>
			        <?php } ?>
			    </select>
		    </div>
			<!--Campo Proveedor -->
			<div id="ObtieneProv" class="form-group col-xs-12 col-sm-6 col-md-4">
		        <label for="proveedor">Proveedor: </label>
		        <select class="form-control" id="proveedor" name="proveedor"> 
					<option value="">----Selecciona un Proveedor----</option>
		           <!-- < ?php				 
		            $dao = new ProveedorAreaDaoJdbc();
		            $lista=$dao->obtieneListadoArea();
		            $elemento=new ProveedorArea();
		            foreach($lista as $elemento){							 
		            ?>	
					<option value="< ?php echo($elemento->getId());?>">
					< ? php echo($elemento->getProveedor());?></option>
					< ? php } ?>	-->
				</select>
			</div>
		</div>
		<!-- Campo descripción-->
		<div class="form-group">		
		    <label for="proyecto">Descripción: </label>
		    <textarea class="form-control" id="descripcion" name="descripcion"  rows="4" cols="40" placeholder="Descripción:"  required/> </textarea>
	    </div>
		<div class="row">
	    <!-- Campo cotización-->
	    <div class="form-group col-xs-12 col-sm-6 col-md-4">
		    <label for="cotizacion">Cotización: </label>
		    <select id="cotizacion" name="cotizacion" class="form-control">
		        <option value="" selected="selected">- selecciona -</option>
		        <option value="Si">Si</option>
		        <option value="No">No</option>
		    </select>
	    </div>
	    <!-- Campo Mes-->
	    <div class="form-group col-xs-12 col-sm-6 col-md-4">
		    <label for="mes">Mes: </label>
		    <input class="form-control" id="mes" name="mes" type="date"   step="1" min="2013-01" max="2020-12" value ="2016-01" placeholder="Mes:" >
		    <br>
	    </div>
		<!-- Campo Partida-->
		<div class="form-group col-xs-12 col-sm-6 col-md-4">						
		    <label for="cursar">Partida:</label>
		    <select class="form-control" id="partida" name="partida"> 
		        <option value="">----Selecciona una Partida----</option>
		            <?php
		            $dao=new PartidaDaoJdbc();
		            $lista=$dao->obtieneListado();
		            $elemento=new Partida(); 
		            foreach($lista as $elemento){
			        ?>
		        <option value="<?php echo($elemento->getId());?>"><?php echo($elemento->getEspecifica());?></option>
			        <?php }?>
		    </select>
	    </div>
		</div>
		<div class="row">
	    <!-- Campo Pagos Foraneos-->
	    <div class="form-group col-xs-12 col-sm-6 col-md-4">
			<label for="pagosforaneos">Pagos Foraneos: </label>
		    <input class="form-control" id="pagosforaneos" name="pagosforaneos" type="number_format" value="0" placeholder="Pagos Foraneos:"  required/>
	    </div>
		<!-- Campo tipo Moneda-->
		<div class="form-group col-xs-12 col-sm-6 col-md-4">
			<label for="moneda">Tipo Moneda: </label>
		    <input class="form-control" id="moneda" name="moneda" type="text"  placeholder="Tipo Moneda:"  required/>
	    </div>
		<!-- Campo Pagos inba-->
	    <div class="form-group col-xs-12 col-sm-6 col-md-4">
			<label for="pagosinba">INBA: </label>
		    <input class="form-control" id="pagosinba" name="pagosinba" type="number_format" value="0"  placeholder="Pagos Inba:" required/>
	    </div>
		<!-- Campo Pagos pasivo-->
	    <div class="form-group col-xs-12 col-sm-6 col-md-4">
			<label for="pagospasivo">PASIVO: </label>
		    <input class="form-control" id="pagospasivo" name="pagospasivo" type="number_format" value="0" placeholder="Pagos Pasivo:" required/>
	    </div>
		</div>
	    <!--Boton Envio-->
	    <div class="form-group">
	    	<input type="submit" class="btn btn-success" value= "Guardar Registro"></input>
	    </div>
		
		<a href="ModuloExcel.php"> Ver tabla</a>
    </form>
	
  </div>
  </div>
  </form>
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="js/validator.js"></script>
</body>
</html>