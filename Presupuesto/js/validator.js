 $('#registrationForm').bootstrapValidator({
	 feedbackIcons: {
		 valid: 'glyphicon glyphicon-ok',
		 invalid: 'glyphicon glyphicon-remove',
		 validating: 'glyphicon glyphicon-refresh'
	 },
	 fields: {
		 descripcion: {
			 validators: {
				 notEmpty: {
					 message: 'Descripcion requerido'
				 }
			 }
		 },
		 area: {
			 validators:{
				 notEmpty:{
					 message: 'Area requerido'
				 }
			 }
		 },
		 proyecto: {
			 validators:{
				 notEmpty:{
					 message: 'Proyecto requerido'
				 }
			 }
		 },
		 cotizacion: {
			 validators: {
				 notEmpty: {
					 message: 'cotizacion requerido'
				 }
			 }
		 },
		 partida: {
			 validators: {
				 notEmpty: {
					 message: 'Partida requerido'
				 }
			 }
		 },
		 pagosforaneos: {
			 validators: {
				 notEmpty: {
					 message: 'Pago requerido'
				 
				 }
			 }
		 },
		 moneda: {
			 validators: {
				 notEmpty: {
					 message: 'Moneda requerido'
				 
				 }
			 }
		 },
		 pagosinba: {
			 validators: {
				 notEmpty: {
					 message: 'Pago requerido'
				 }
			 }
		 },
		 pagospasivo: {
			 validators: {
				 notEmpty: {
					 message: 'Pago requerido'
				 
				 }
			 }
		 },
		 
 
	 }
});