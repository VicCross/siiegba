<?php
$perfil = null;
$user = null;

session_start();
include_once("../src/mx/com/virreinato/dao/CatProyectoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatProyecto.class.php");
include_once("../src/mx/com/virreinato/dao/ProveedorDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Proveedor.class.php");
include_once("../src/mx/com/virreinato/dao/EmpleadoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Empleado.class.php");
include_once("../src/mx/com/virreinato/dao/AreaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Area.class.php");
include_once("../src/mx/com/virreinato/dao/PresupuestoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Presupuesto.class.php");
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/ProgramaInstitucionalDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ProgramaInstitucional.class.php");
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}else{
    if(isset($_SESSION['id'])){ $perfil = $_SESSION['id'];}
    if(isset($_SESSION['user'])){ $user = $_SESSION['user'];}
}
?>
<!DOCTYPE html">
<html>
<head>

<link rel="stylesheet" type="text/css" href="../css/style.css" />
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<link href="../css/calendario.css" type="text/css" rel="stylesheet">
<script src="../js/calendar.js" type="text/javascript"></script>
<script src="../js/calendar-es.js" type="text/javascript"></script>
<script src="../js/calendar-setup.js" type="text/javascript"></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
<script language="JavaScript">
	
	var cheque = "";
	var idCh = "";
	var perfil;
	
		
	function Mostrar(value,name){
			$("#idProveedor").hide();
			$("#idEmpleado").hide();
			$("#otro").hide();
			$("#cat_proveedor").hide();
			$("#idProveedor").val(0);
			$("#idEmpleado").val(0);
			$("#otro").val("");
			cheque = value;
		
		  $("#"+value).show();		
	}
	function MostrarCampo(id){
		/* alert('id:'+id); */
		  document.getElementById(id).style.display='block';		
	}
	
	function MostrarTabla(id){
		/* alert('id:'+id); */
		  check= document.getElementById('catalogo_proveedor').checked;
		  if(check)
		  	document.getElementById(id).style.display='block';
		  else 			
		  document.getElementById(id).style.display='none';
	}
	function Regresar(){
		window.location="SolicitudTr.php";
	}
	
	function Validar(){
		if( $("#fecha").val() == "" ){
			alert("Por favor ingrese la fecha de solicitud.");
			$("#fecha").focus();
			return false;
		}
		
		if( $("#destino").val() == "0"){
				 alert("Por favor seleccione el destino.");
				 $("#destino").focus();
				 return false;
		}		  
		
		if( $("#programa").val() == "0"){
				 alert("Por favor seleccione el Programa Institucional.");
				 $("#programa").focus();
				 return false;
		}		  
		if( $("#areaEmpleado").val() == "0"){
				 alert("Por favor seleccione el área.");
				 $("#areaEmpleado").focus();
				 return false;
		}	
		
		if( cheque != "" ){
			if( $("#"+cheque).val() == "0" ){
				alert("Por favor seleccione el nombre para elaborar el cheque.");
				$("#"+cheque).focus();
				return false;
		   }else if( $("#"+cheque).val() == "" ){
		   	alert("Por favor ingrese el nombre para elaborar el cheque.");
				$("#"+cheque).focus();
				return false;
		   }
		}else{
			alert("Por favor indique a nombre de quién se elabora el cheque.");
			return false;
		}
		
		check= document.getElementById('catalogo_proveedor').checked;
		if(check){
			if( $("#domicilio_prov").val() == "" ){
		   	alert("Por favor ingrese el domicilio del proveedor.");
				$("#domicilio_prov").focus();
				return false;
		   }
		   if( $("#cp_prov").val() == "" ){
		   	alert("Por favor ingrese el código postal para el proveedor.");
				$("#cp_prov").focus();
				return false;
		   }
		
		}  
		if( $("#obs_solicitud").val() == "" ){
			alert("Por favor ingrese las observaciones de la Solicitud.");
			$("#obs_solicitud").focus();
			return false;
		}else if( $("#elabora_sol").val() == "" ){
			alert("Por favor ingrese el nombre de quien elabora la Solicitud.");
			$("#elabora_sol").focus();
			return false;
		}
		
		document.frmSolicitudPresup.submit();
		
	}

</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Editar Presupuesto</title>
</head>
<body>
<?php 
    $respuesta = null;
    $error = null;
    $id = null;
    
   if(isset($_GET["respuesta"]))
        {$respuesta= (String) $_GET["respuesta"];}
   if(isset($_GET["error"]))
        {$error = (String) $_GET["error"];}
?>
<div class="contenido">
<br/>
<p class="titulo_cat1">Terceros > <a class="linkTitulo_cat1" href="SolicitudTr.jsp" >Solicitud de Presupuesto</a> </p>
<h2 class="titulo_cat2">
     <?php
   $daoPre = new PresupuestoDaoJdbc();
   $p = new Presupuesto();
   $aux = 0;
  
   if(isset($_GET['idSolicitud']) && $_GET['idSolicitud']!= null){
		 $id = (String) $_GET['idSolicitud'];
		 echo("");
		 $p = $daoPre->obtieneElementoGb($id);
		 //echoln(id);
	}else if(isset($_GET['id']) && $_GET['id']!= null){
		 $id = (String) $_GET['id'];
		 
		 $p = $daoPre->obtieneElementoGb($id);
		 echo("Solicitud de Presupuesto");		 
	}else{
		echo("");
	
	}	
?>
</h2>
<br/>
<?php 
     if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>");
     if($error!=null) echo("<div align='center' class='msj'>".$error."</div>");
?>
<form name="frmSolicitudPresup" id="frmSolicitudPresup" method="POST" action="../src/mx/com/virreinato/web/CatPresupuestoTr.php">
    <table width="48%" border="0" cellspacing="0" cellpadding="5" class='tb_add_cat' align='center'>
    <tr class="SizeText">
        <td >
            <br/> &nbsp; &nbsp; Estatus: <?php  if( $p != null && $p->getEstatus()!=null ){ echo( $p->getEstatus() ); }else {echo("Capturado"); }?>

            <br/><br/>
            Periodo*:
            <select name="periodo" id="periodo">
                <option value='0'>Selecciona</option>
                <?php 

                    $periodo=new PeriodoDaoJdbc();
                    $periodos=$periodo->obtieneListadoAbierto(); 
                    $per = new Periodo();

                    foreach($periodos as $per) {
                      $sel="";
                      if($p!= null && $p->getIdPeriodo()!= null ){
                              if($per->getId() == (int)($p->getIdPeriodo())){
                                    $sel="selected='selected'";
                              }	

                      }	  
                      echo("<option value='".$per->getId()."' ".$sel." >".$per->getPeriodo()."</option>");
                    }
                ?>
                </select>
            </select>

            <br/><br/>&nbsp; &nbsp; Fecha* <input type="text" name="fecha" id="fecha" size="20" value='<?php if($p != null && $p->getFecha()!= null) echo(date("d-m-Y",strtotime( $p->getFecha())));  ?>' />
            
            <br><br/> &nbsp;&nbsp;&nbsp;Destino*:
            <select name="destino" id="destino" style="width:160px" >
                <option value="TR" <?php if($p!=null && $p->getDestino()!= null && $p->getDestino() == "TR") echo( "selected='selected'");?>> Terceros</option>
            </select>
            <br> <br>
            &nbsp; &nbsp;Programa Institucional *: <select name="programa" id="programa">
            <option value="0"> Selecciona  </option>
            <?php 
                $daoInst = new ProgramaInstitucionalDaoJdbc();
                $listaInst = $daoInst->obtieneListado();
                $elementoInst=new ProgramaInstitucional();

                foreach($listaInst as $elementoInst){
                        $sel = "";
                        if($p!= null && $p->getIdPrograma()!=null && (String)($elementoInst->getIdProgramaInstitucional()) == $p->getIdPrograma()){
                                $sel = "selected='selected'";
                        }
                        echo("<option value='".$elementoInst->getIdProgramaInstitucional()."'  ".$sel.">".$elementoInst->getProgramaInstitucional()."</option>");

                }
            ?> 
            </select>
            <br> <br> &nbsp;&nbsp;&nbsp;Elaborar cheque a nombre de*:

            <?php  if($p!= null && $p->getIdProveedor() != null && !(String)($p->getIdProveedor()) == "0" ){ $aux  = 1;?>
                    <br/>&nbsp; &nbsp;  &nbsp; &nbsp; <input name="cheqTo" type="radio" checked='checked' value="idProveedor" onclick="Mostrar(this.value,this.name)"  />Proveedor
                    <script> idCh = "idProveedor"; </script>
            <?php  }else{?>
               <br/>&nbsp; &nbsp;  &nbsp; &nbsp; <input name="cheqTo" type="radio" value="idProveedor" onclick="Mostrar(this.value,this.name)"  />Proveedor
            <?php  }?>	
            &nbsp; &nbsp; <select style="display:none" name="idProveedor" id="idProveedor">
            <option value="0"> Selecciona  </option>
            <?php 
                $daoProv=new ProveedorDaoJdbc();
                $listaProv = $daoProv->obtieneListado();
                $elementoProv=new Proveedor();

                foreach($listaProv as $elementoProv){
                $sel = "";
                if($p!= null && $p->getIdProveedor()!= null ){
                    if( $p->getIdProveedor() == $elementoProv->getId()){   $sel = "selected='selected'"; }
                }
                    echo("<option value=".$elementoProv->getId()." ".$sel." >" .$elementoProv->getProveedor()."</option>");
                }
            ?> 
            </select>
            
            <?php  if( $p!= null && $p->getIdEmpleado()!= null && !(String)($p->getIdEmpleado()) == "0"  ){ $aux  = 1;?>  
                    <br/>&nbsp; &nbsp;  &nbsp; &nbsp; <input name="cheqTo" type="radio" checked='checked' value="idEmpleado" onclick="Mostrar(this.value,this.name)"  />Empleado
                    <script> idCh = "idEmpleado"; </script>
            <?php  }else{?>
                    <br/>&nbsp; &nbsp;  &nbsp; &nbsp; <input name="cheqTo" type="radio" value="idEmpleado" onclick="Mostrar(this.value,this.name)"  />Empleado
            <?php  }?>
            &nbsp; &nbsp; <select style="display:none" name="idEmpleado" id="idEmpleado">
            <option value="0"> Selecciona  </option>
            <?php 
                $daoEm=new EmpleadoDaoJdbc();
                $listaEm = $daoEm->obtieneListado();
                $elementoEm=new Empleado();

                foreach($listaEm as $elementoEm){
                    $sel = "";
                    if($p!= null && $p->getIdEmpleado()!= null ){
                        if( $p->getIdEmpleado() == $elementoEm->getId()){ $sel = "selected='selected'"; }
                    }
                        echo("<option value=".$elementoEm->getId()." ".$sel." >" .$elementoEm->getApPaterno(). " ".$elementoEm->getApMaterno()." ".$elementoEm->getNombre()."</option>");
                }
            ?> 
            </select>
            
            <?php  if( $aux== 0 && $p!= null && $p->getOtro() != null ){?>
                <br/>&nbsp; &nbsp;  &nbsp; &nbsp; <input name="cheqTo" type="radio" checked='checked' value="otro" onclick="Mostrar(this.value,this.name);MostrarCampo('cat_proveedor');"  />Otro
                <script> idCh = "otro"; </script>
            <?php  }else{?>
                <br/>&nbsp; &nbsp;  &nbsp; &nbsp; <input name="cheqTo" type="radio" value="otro" onclick="Mostrar(this.value,this.name);MostrarCampo('cat_proveedor');"  />Otro
            <?php  }?>
            &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; <input style="display:none" type="text" maxlength="100" name="otro" id="otro" size="30" value="<?php if($p!= null && $p->getOtro()!= null) echo($p->getOtro());?>" > 
             <div id='cat_proveedor' style="display:none"> &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; Incluir en catálogo de proveedores <input type='checkbox' name='catalogo_proveedor' id='catalogo_proveedor' onclick="MostrarTabla('datos_proveedor');"> </div>
             
             <table border='0' width='70%' id='datos_proveedor'  style="display:none">
                <tr>
                    <td>Domicilio*:</td>
                    <td colspan='3'><input type='text' name='domicilio_prov' id='domicilio_prov' maxsize='255' size='50'></td>
                    <td>Código postal*:</td>
                    <td><input type='text' name='cp_prov' id='cp_prov' size='15'></td>
                </tr> 
                <tr>
                    <td>Representante:</td>
                    <td><input type='text' name='representante_prov' id='representante_prov' ></td>
                    <td>Teléfono:</td>
                    <td><input type='text' name='telefono_prov' id='telefono_prov' size='15'></td>
                    <td>Correo:</td>
                    <td><input type='text' name='correo_prov' id='correo_prov' size='15'></td>
                </tr>
                </table>
             
             <br> <br> &nbsp;&nbsp;&nbsp;Observaciones del Presupuesto*:
            <br/>&nbsp; &nbsp;  &nbsp; &nbsp;<textarea name="obs_solicitud" id="obs_solicitud" rows="5"  maxlength="255" cols="40"><?php 
            if($p!= null && $p->getObservaciones() != null) 
            echo($p->getObservaciones());
            ?></textarea> 
            
            <br/><br/>&nbsp;&nbsp;&nbsp;Elabora Solicitud*: <input type="text" name="elabora_sol" id="elabora_sol" maxlength="100" size="30" value="<?php if($p!=null && $p->getElabora()!=null) echo($p->getElabora());?>"  />
            <script> $("#"+idCh).show(); cheque = idCh; </script>

        </td>
    </tr>
    
    <tr>
        <td align="center" colspan="3">
            <input name="guardar" style="cursor:pointer" type="button" onClick = "Validar()" value="Guardar"  class='btn' />
            &nbsp; &nbsp; &nbsp;
            <input name="cancelar" type="button" style="cursor:pointer"  onclick="Regresar()" value="Regresar"  class='btn' />
            <?php  if( $perfil == 2 )  echo( " &nbsp; &nbsp; &nbsp;<a class='btn' href='../src/mx/com/virreinato/web/CatPresupuestoTr.php?VoboSA=".$p->getId()."'> VoBo Administrativo </a> &nbsp; &nbsp; &nbsp; <a class='btn' href='../src/mx/com/virreinato/web/CatPresupuestoTr.php?NoAceptado=".$p->getId()."&Vobo=sa '> No Aceptado </a>" );
               if( $perfil == 3 )  echo( "&nbsp; &nbsp; &nbsp; <a class='btn' href='../src/mx/com/virreinato/web/CatPresupuestoTr.php?VoboST=".$p->getId()."'> VoBo Técnico </a> &nbsp; &nbsp; &nbsp; <a class='btn' href='../src/mx/com/virreinato/web/CatPresupuestoTr.php?NoAceptado=".$p->getId()."&Vobo=st '> No Aceptado </a>" );
               if( $perfil == 4 )  echo( "&nbsp; &nbsp; &nbsp; <a class='btn' href='../src/mx/com/virreinato/web/CatPresupuestoTr.php?VoboDirector=".$p->getId()."'> VoBo Director </a> &nbsp; &nbsp; &nbsp; <a class='btn' href='../src/mx/com/virreinato/web/CatPresupuestoTr.php?NoAceptado=".$p->getId()."&Vobo=dr '> No Aceptado </a>" );?>
        </td>
    </tr>
    </table>
    
    <?php if($p!= null && $p->getId()!= null) echo("<input type='hidden' name='id' id='id' value='".$p->getId()."' />"); ?>	 
	 
   </form>
 </div>
 <?php  if( $id != null){ ?>
   <br/><br/><br/><br/>
<?php include 'lista_DetSolicitudTr.php' ?>
 <?php }?>
 <br/><br/>
</body>
</html>
<script>
  
 Calendar.setup({ 
       inputField : "fecha", ifFormat :"%d-%m-%Y", button :"fecha"      
    });
    
</script>