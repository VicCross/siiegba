<?php

session_start();
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/dao/PresupuestoAsignadoGbDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/beans/PresupuestoAsignadoGb.class.php");
header('Content-Type: text/html; charset=UTF-8'); 
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<link rel="stylesheet" type="text/css" href="../css/pro_dop_1.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
	function Eliminar(idCalendario){
		alert("Registro Eliminado!");
	}
</script>

<!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="../css/lista.css">
        <script type="text/javascript" src="../media/js/complete.js"></script>
        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
        <script type="text/javascript" src="../js/lista.js"></script>
        
           <input type="hidden" id="columnas_visibles" name="columnas_visibles" value="13"/>
    <input type="hidden" id="total_columnas" name="total_columnas" value="15"/>


<script>
	$(document).ready(function() {
		$(".menu ul").css({
			background: "<?php echo $parametro->getValor(); ?>"
		});
		$("#.menu a").css({
			background: "<?php echo $parametro->getValor(); ?>"
		});
	});
</script>

</head>
<body>
<?php 
    $respuesta = null;
    $error = null;
    
   if(isset($_GET["respuesta"]))
        {$respuesta= (String) $_GET["respuesta"];}
    if(isset($_GET["error"]))
        {$error= (String) $_GET["error"];}
?>
<div class="contenido">
    <br/>
    <p class="titulo_cat1">Terceros > Presupuesto Asignado</p>

    <h2 class="titulo_cat2"></h2>
<?php
    if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>");
    if($error!=null) echo("<div align='center' class='msj'>".$error."</div>");
?>
    <div align="right"><a href='AgregarCalendarioTr.php' class='liga_btn'> Agregar Nuevo Calendario</a></div>
    <br></br>
    <table  border="0" cellspacing="0" cellpadding="5" class='dataTable' align='center' >
        
		<thead>
		<tr>
            <th width="10%">Periodo</th>
            <th width="2%">Enero</th>
            <th width="2%">Febrero</th>
            <th width="2%">Marzo</th>
            <th width="2%">Abril</th>
            <th width="2%">Mayo</th>
            <th width="2%">Junio</th>
            <th width="2%">Julio</th>
            <th width="2%">Agosto</th>
            <th width="2%">Septiembre</th>
            <th width="2%">Octubre</th>
            <th width="2%">Noviembre</th>
            <th width="2%">Diciembre</th>
            <th width="2%"></th>
            <th width="2%"></th>
        </tr>
		
		</thead>
		<tfoot>
		<tr>
            <th width="10%"></th>
            <th width="2%"></th>
            <th width="2%"></th>
            <th width="2%"></th>
            <th width="2%"></th>
            <th width="2%"></th>
            <th width="2%"></th>
            <th width="2%"></th>
            <th width="2%"></th>
            <th width="2%"></th>
            <th width="2%"></th>
            <th width="2%"></th>
            <th width="2%"></th>
            <th width="2%"></th>
            <th width="2%"></th>
        </tr>
		
		
		</tfoot>
		<tbody>
    <?php
        $dao = new PresupuestoAsignadoGbDaoJdbc();
        $lista=$dao->obtieneListado("TR");
        $elemento = new PresupuestoAsignadoGb();

        foreach($lista as $elemento){
             $daop=new PeriodoDaoJdbc();
             $per=$daop->obtieneElemento((String)($elemento->getIdPeriodo()));
    ?>
    <tr class="SizeText">
        <td align="center" ><?php echo($per->getPeriodo());?> </td>
        <td align="center" ><?php echo("$".number_format($elemento->getMontoEne(),2));?> </td>
        <td align="center" ><?php echo("$".number_format($elemento->getMontoFeb(),2));?> </td>
        <td align="center" ><?php echo("$".number_format($elemento->getMontoMar(),2));?> </td>
        <td align="center" ><?php echo("$".number_format($elemento->getMontoAbr(),2));?> </td>
        <td align="center" ><?php echo("$".number_format($elemento->getMontoMay(),2));?> </td>
        <td align="center" ><?php echo("$".number_format($elemento->getMontoJun(),2));?> </td>
        <td align="center" ><?php echo("$".number_format($elemento->getMontoJul(),2));?> </td>
        <td align="center" ><?php echo("$".number_format($elemento->getMontoAgo(),2));?> </td>
        <td align="center" ><?php echo("$".number_format($elemento->getMontoSep(),2));?> </td>
        <td align="center" ><?php echo("$".number_format($elemento->getMontoOct(),2));?> </td>
        <td align="center" ><?php echo("$".number_format($elemento->getMontoNov(),2));?> </td>
        <td align="center" ><?php echo("$".number_format($elemento->getMontoDic(),2));?> </td>
        <td><a href="AgregarCalendarioTr.php?id=<?php echo($elemento->getId());?>" class='liga_cat'><acronym title="Editar"><img src="../img/Pencil3.png" width="16" height="16" alt="Editar" style="border:0;" /></acronym></a></td>
        <td><a href="../src/mx/com/virreinato/web/CalendarioTr.php?id=<?php echo($elemento->getId());?>" class='liga_cat' ><acronym title="Borrar"><img src="../img/DeleteRed.png" width="16" height="16" alt="Borrar" style="border:0;" /></acronym> </a></td>
            </tr>	
					
		<?php }?>	
		</tbody>
	</table>
	<br>
	
</div>
<br/><br/>
</body>
</html>