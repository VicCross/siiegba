<?php
$id = null;
$user = null;

session_start();
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/CatChequeDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/CatCheque.class.php");
include_once("../src/mx/com/virreinato/dao/ConsultaResumenDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ConsultaResumen.class.php");
include_once("../src/mx/com/virreinato/dao/PresupuestoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Presupuesto.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}else{
    if(isset($_SESSION['id'])){ $id = $_SESSION['id'];}
    if(isset($_SESSION['user'])){ $user = $_SESSION['user'];}
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Solicitud de Presupuesto </title>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link href="../css/calendario.css" type="text/css" rel="stylesheet">
<script src="../js/calendar.js" type="text/javascript"></script>
<script src="../js/calendar-es.js" type="text/javascript"></script>
<script src="../js/calendar-setup.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="../css/lista.css">
        <script type="text/javascript" src="../media/js/complete.js"></script>
        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
        <script type="text/javascript" src="../js/lista.js"></script>
          
<input type="hidden" id="columnas_visibles" name="columnas_visibles" value="5"/>
    <input type="hidden" id="total_columnas" name="total_columnas" value="10"/>


<script>
 
  function Imprimir(pagina,folio) {
		var dir = pagina + "?folio=" + folio;
		var left = Math.floor((screen.width - 1200) / 2);
		var opciones = 'titlebar=0, menubar=0, toolbar=0, location=0, directories=0, status=0, scrollbars=0, resizable=0, width=1200, height=650,top=50,left='
				+ left + '';
		window.open(dir, "", opciones);
	}
</script>
</head>
<body>
<?php 
    $respuesta = null;
    $error = null;
    
   if(isset($_GET["respuesta"]))
        {$respuesta= (String) $_GET["respuesta"];}
   if(isset($_GET["error"]))
        {$error= (String) $_GET["error"];}
        
   $dia = (String)(date('d'));
   $mes = (String)(date('m'));
   $mesInicio="01";
    $diaInicio="01";
    //if(date('n')>1){
            //$mesInicio= (String)( date('n') - 1 ); if(strlen($mesInicio) == 1){ $mesInicio = "0".$mesInicio; }

    //}
    //else{
            //$diaInicio="01";
    //} 
 
?>
<div class="contenido">
    <br/>
    <p class="titulo_cat1">Donativos  > Solicitud de Presupuesto</p>
	
	 <?php
        if (isset($_GET['recargado']) && $_GET['recargado'] == "1") {
            echo '<input type="hidden" id="nombre_form" name="nombre_form" value=""/>';
        } else {
            echo '<input type="hidden" id="nombre_form" name="nombre_form" value="frmFiltroSolicitud"/>';
        }
        ?>
    <h2 class="titulo_cat2"></h2>

    <p class="titulo_cat1">
    <form name="frmFiltroSolicitud" id="frmFiltroSolicitud" method="POST" action="../src/mx/com/virreinato/web/CatPresupuestoDn.php" >
          &nbsp; &nbsp; Fecha de Inicio: <input type="text" size="10" name="inicio" id="inicioFecha" maxlength="20" value=" <?php  if(isset($_GET["inicio"]) && $_GET["inicio"] !=null ){ echo( $_GET["inicio"]); } ?>"  >
          Fecha de Fin: <input type="text" size="10" name="fin" id="finFecha" maxlength="20" value=" <?php  if(isset($_GET["fin"]) && $_GET["fin"]!=null ){ echo( $_GET["fin"] ); }  else { echo($dia."-".$mes."-".date('Y'));} ?>"  >
              <?php 

                    $aux = $diaInicio;
                    if( ($mesInicio == "04" || $mesInicio == "06" || $mesInicio == "09" || $mesInicio == "11") && $aux == "31" )
                    {        $aux = "30"; }

                    if( $mesInicio == "02" && (  $dia == "29" || $dia == "30" || $dia == "31" ) )
                    {        $aux = "28"; }

                    $inicio =  $aux."-".$mesInicio."-".date('Y');
                            $fin = $dia."-".$mes."-".date('Y');
                            $periodo = "";
                            $estatus = "TD";
                            if( $id < 7 ){ 
                                
            $daoPer=new PeriodoDaoJdbc();
            $listaPer = $daoPer->obtieneListado();
            $elementoPer = new Periodo();
             ?>
        &nbsp; Período: <select name="periodo" id="periodo" style="width: 110px">
        <?php
            foreach($listaPer as $elementoPer){

            $sel = "";
            if(isset($_GET["periodo"]) && $_GET['periodo']!=null && (int)( $_GET["periodo" ] ) == $elementoPer->getId()){ $sel = "selected='selected'"; }
            else if( $_GET["periodo"] == null && (int)($elementoPer->getPeriodo()) == date('Y')  ){ $sel = "selected='selected'";  $periodo = (String)($elementoPer->getId() ); }
            echo("<option value=".$elementoPer->getId()." ".$sel." >".$elementoPer->getPeriodo()."</option>");
        }
    ?>
    </select>
        &nbsp; &nbsp; Estatus:
            <select style="width:120px" name="estatus" id="estatus">
                <?php if( $_GET["estatus"]!= null && $_GET["estatus"] == "TD"){ ?>
                            <option value="TD" selected="selected" >  Todos </option>
                    <?php }else{ ?>
                    <option value="TD" selected="selected" >  Todos </option>

                    <?php }if( $_GET["estatus"]!= null && $_GET["estatus"] == "DOS" ){?>
            <option value="DOS" selected="selected">  Con dos VoBos</option>
            <?php }else{?>
            <option value="DOS">  Con dos VoBos</option>

                    <?php }if( $_GET["estatus"]!= null && $_GET["estatus"] == "CA" ){?>
                            <option value="CA" selected="selected">  Capturado </option>
                    <?php }else{?>
                    <option value="CA">  Capturado </option>
                    <?php }if( $_GET["estatus"]!= null && $_GET["estatus"] == "ST" ){?>
            <option value="ST" selected="selected">  Con VoBo de ST </option>
            <?php }else{?>
            <option value="ST">  Con VoBo de ST </option>
    <?php }if( $_GET["estatus"]!= null && $_GET["estatus"] == "NT" || ( $id == 3) && $_GET["estatus"]== null  ){?>
                    <option value="NT" selected="selected"> Sin VoBo de ST </option>
            <?php }else{?>
            <option value="NT"> Sin VoBo de ST </option>
            <?php }if( $_GET["estatus"]!= null && $_GET["estatus"] == "SA" ){?>
            <option value="SA" selected="selected"> Con VoBo de SA </option>
            <?php }else{?>
            <option value="SA"> Con VoBo de SA </option>
            <?php }if( $_GET["estatus"]!= null && $_GET["estatus"] == "NA" || ( $id == 2) && $_GET["estatus"]== null ){?>
                    <option value="NA" selected="selected"> Sin VoBo de SA </option>
            <?php }else{?>
            <option value="NA"> Sin VoBo de SA </option>
            <?php }
            if( $_GET["estatus"]!= null && $_GET["estatus"] == "DR" ){
                echo '<option value="DR" selected="selected"> Con VoBo de Director </option>';
            }else{
                echo '<option value="DR"> Con VoBo de Director </option>';
            }
            if( $_GET["estatus"]!= null && $_GET["estatus"] == "ND" || ( $id == 4) && $_GET["estatus"]== null ){
                echo '<option value="ND" selected="selected"> Sin VoBo de Director </option>';
            }else{
                echo '<option value="ND"> Sin VoBo de Director </option>';
            }if( $_GET["estatus"]!= null && $_GET["estatus"] == "IP" ){?>
                    <option value="IP" selected="selected"> Impreso </option>
            <?php }else{?>
            <option value="IP"> Impreso </option>
            <?php }if( $_GET["estatus"]!= null && $_GET["estatus"] == "NO" ){?>
                    <option value="NO" selected="selected"> No Aceptado </option>
            <?php }else{?>
            <option value="NO"> No Aceptado </option>
                    <?php } ?>
            </select>
	   	<?php     } ?>	
	   	&nbsp; <input name="Filtro" style="cursor:pointer" type="submit" value="Filtrar"  class='btn' />
	   </form>
    <div align="right"><a href='SolicitudEditDn.php' class='liga_btn'> Agregar Nueva Solicitud</a></div>
    <br> </br>
    <br>		
<?php
	    if($respuesta!=null) echo("<div align='center' class='msj'>".$respuesta."</div>");
	    if($error!=null) echo("<div align='center' class='msj'>".$error."</div>");
	?>
	
	<div id="Descripcion" >
	  <?php
    if(isset($_GET['inicio']) && $_GET["inicio"] !=null ){
 	       $inicio = ( String )$_GET["inicio"];
 	       $fin = (String)$_GET["fin"];
  	 }
 
        if( $id == 7 && $_GET['estatus']!=null ){
         $estatus = "TD"; 
        }else if(isset($_GET['estatus']) && $_GET['estatus']!=null) {
         $estatus = (String)$_GET['estatus'];
        }
        else if (isset($_GET['estatus']) && $_GET['estatus']==null && $id == 2){
         $estatus = "NA";
        }
        else if (isset($_GET['estatus']) && $_GET['estatus']==null && $id == 3){
         $estatus = "NT";
        }
        else if ( isset($_GET['estatus']) && $_GET['estatus']==null && $id == 4){
            $estatus = "ND";
        }
        if(isset($_GET['periodo']) && $_GET['periodo'] != null )  $periodo = (String)$_GET['periodo'];  
        else {
	      	$aniosel = (String)(date('Y'));
	      	$per=new PeriodoDaoJdbc();
	      	
                $periodo=$per->obtieneidElemento($aniosel);	        
	      }
    ?>
    <table  border="0" cellspacing="0" cellpadding="5" class='dataTable' align='center' >
        
		<thead>
		<tr>
        <th width="10%">Fecha</th>
        <th width="20%">Destino</th>
        <th width="20%">Cheque</th>
        <th width="10%">Monto Total</th>
        <th width="15%">Estatus</th>
        <th width="2%"></th>
        <th width="2%"></th>
        <?php  if( $id == 1 || $id == 2  || $id == 3 || $id == 4 || $id== 5 || $id==6 ){ ?>
            <th width="2%"></th>
        <?php }?>
        <th width="2%"></th>
        </tr>
		</thead>
		<tfoot>
		<tr>
        <th width="10%"></th>
        <th width="20%"></th>
        <th width="20%"></th>
        <th width="10%"></th>
        <th width="15%"></th>
        <th width="2%"></th>
        <th width="2%"></th>
        <?php  if( $id == 1 || $id == 2  || $id == 3 || $id == 4 || $id== 5 || $id==6 ){ ?>
            <th width="2%"></th>
        <?php }?>
        <th width="2%"></th>
        </tr>
		
		
		</tfoot>
		<tbody>
        <?php	
            $dao=new PresupuestoDaoJdbc();
            $daoC = new CatChequeDaoJdbc();
            $lista=$dao->obtieneListadoDestino($inicio, $fin , $periodo,  $estatus, $id, $user,"DN" );
            $elemento = new Presupuesto(); 
            foreach($lista as $elemento){
	?>
        
    <tr class="SizeText">
        
    <?php 
        if( $id== 1 || $id== 2 || $id== 3 || $id == 4 || $id==5 || $id==6 ){     
          $idCheque = $daoC->obtieneCheque((String)( $elemento->getId() ));
          $cheque=$daoC->obtieneElemento((String)($idCheque));
        ?>
        <td align="center"><?php echo(date("d-m-Y",strtotime($elemento->getFecha())) ); ?></td>
        <td align="center"><?php 
               if($elemento->getDestino() == "PR"){
                       echo("Proyecto");	
               }
               else if($elemento->getDestino() == "GB"){
                       echo("Gasto Básico");	
               }
               else if($elemento->getDestino() == "TR"){
                       echo("Terceros");	
               }
               else if($elemento->getDestino() == "DN"){
                       echo("Donativos");	
               }

            ?></td>
        <td align="center"><?php if($cheque!=null && $cheque->getFolio()!= null ) echo($cheque->getFolio()); ?></td>
        <td align="center"><?php echo("$".number_format($elemento->getMonto(),2)); ?></td>
        <td align="center">
            <?php  
                echo( $elemento->getEstatus()); 
                if($cheque!=null && $cheque->getEstatus()!=null){ 
                        if( $cheque->getEstatus() == "RE") 
                                echo( " - Registrado "); 
                        if( $cheque->getEstatus() == "EM") 
                                echo( " - Emitido "); 
                        if( $cheque->getEstatus() == "CO") 
                                echo( " - Cobrado ");
                        if( $cheque->getEstatus() == "CA") 
                                echo( " - Cancelado "); 
               } ?></td>
        <td align="right"><a href='SolicitudEditDn.php?id=<?php echo($elemento->getId());?>' class='liga_cat'><acronym title="Editar"><img src="../img/Pencil3.png" width="16" height="16" alt="Editar" style="border:0;" /></acronym> </a></td>
            <td><a href='../src/mx/com/virreinato/web/CatPresupuestoDn.php?id=<?php echo($elemento->getId());?>' class='liga_cat' onclick='return confirm("¿Esta seguro que desea eliminar este registro?")' ><acronym title="Borrar"><img src="../img/DeleteRed.png" width="16" height="16" alt="Borrar" style="border:0;" /></acronym></a></td>

               <td><a href='Imprimir_ChequeDn.php?idCheque=<?php  echo$idCheque; ?>&idSolicitud=<?php echo($elemento->getId());?>&monto=<?php  echo( $elemento->getMonto()); ?>&Fmonto=<?php echo(number_format($elemento->getMonto(),2));?>' class='liga_cat'><acronym title="Imprimir Cheque"><img src="../img/dollar_coin.png" width="18" height="18" alt="Imprimir Cheque" style="border:0;" /></acronym></a></td>
			     	<?php  }?>
                <td><a style="cursor:pointer" onclick="Imprimir('../Formatos/Imprimir_SolPresupuesto.php',<?php echo($elemento->getId());?>)" class='liga_cat'><acronym title="Imprimir"><img src="../img/printer.png" width="18" height="18" alt="Imprimir" style="border:0;" /></acronym></a></td>  
			</tr>	
	      <?php  } ?>
		  </tbody>
	    </table>
	</div>
	<br>
        
</div>

<br/><br/>
</body>
</html>	
<script language="JavaScript">
   Calendar.setup({ inputField :"finFecha", ifFormat : "%d-%m-%Y", button:"finFecha" });
    
   Calendar.setup({ inputField : "inicioFecha", ifFormat : "%d-%m-%Y", button:"inicioFecha" });
   
   var ini = '<?php echo$inicio ?>';
   var fin = '<?php echo$fin ?>';
   $("#inicioFecha").val(ini);
   $("#finFecha").val(fin); 

   
</script>