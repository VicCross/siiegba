<?php
header('Content-Type: text/html; charset=UTF-8');
include_once("src/classes/Configuracion.class.php");

$config = new Configuracion();
session_start();
$_SESSION['RAIZ'] = $config->getRoot();

include_once("src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
//Obtenemos el valor del color, su id es el número 5 
$parametro = new ParametroDaoJdbc();
$parametro->obtieneElemento(5);
session_destroy();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
         <meta name="viewport" content="width=device-width, user-scalable-no, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script language="JavaScript" type="text/javascript" src="js/jquery-1.7.2.js" ></script>
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="css/estilos_log.css" />
        <link rel="stylesheet" type="text/css" href="css/fonts.css">
        <title>Login SIIEGBA-Museo</title>
        <style>
            
        body{
            background: url('img/login mod siiegba.png') no-repeat center center;
            background-size:;
             }
        </style>
    </head>
    <body class= "body_login">
            <form class="form_login" action="Verifica.php" method="post">                
                <input class "usuario" type="text" placeholder= "&#8997; Usuario" name="user">
                <input class "password" type="password" placeholder= "&#128272; Password" name="pwd">
                <input class= "boton" type= "submit" value="Enviar" name="envio">
                    <label class="Error">        	    	
                        <?php
						$mensaje = "";
						if(isset($_GET['err'])){
							switch($_GET['err']){
								case "1":
									$mensaje = "Error en usuario y/o contraseña.";
									break;
								case "2":
									$mensaje = "Atención, su sesión ha caducado. Por favor, inicie de nuevo.";
									break;	
							}
						}
						echo ($mensaje);
                        ?>
                    </label></center>
            </form>
    </body>
</html>