<?php
include_once("dao/ProveedorDaoJdbc_Coordinacion.class.php");  // para que funcione primero un Dao 

 // ProveedorDaoJdbc.class.php
include_once($_SESSION['RAIZ'] . "/proveedores/beans/Proveedor_area.class.php");
// proveedor.class.php
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<script>
$(document).ready(function() {                    
	$("#Perfil").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".div_menu").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuesto th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_presupuestoResumen th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_cat th").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
	$(".tb_add_cat").css({
		background: "<?php echo $parametro->getValor(); ?>"
	});
});
</script>
</head>
<body>
 <?php
    $error = NULL;
    if (isset($_REQUEST['error'])) {
        $error = $_REQUEST['error'];
    }
    ?>
<div class="contenido"> 
    <p class="titulo_cat1">Catálogos > Catálogos de Operación >
    <?php
	$dao=new ProveedorDaoJdbc_Coordinacion();
	$elemento=new Proveedor();

	if(isset($_GET['id'])){
		echo("Modificar Proveedor");
		$elemento=$dao->obtieneElemento($_GET['id']);	
	}	
	else{
		echo("Alta de Nuevo Proveedor");
	}	
    ?>
    </p>
    <br/>
    <?php if ($error != null) echo("<div align='center' class='msj'>" . $error . "</div>"); ?>
    <form id="fproveedor" name="fproveedor" method="post" action="web/CatProveedor_Coordinacion.php">
        <table width="90%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
          <tr>
            <td width='15%'>Proveedor*:</td>
            <td width='35%'><input type="text" name="proveedor" id="proveedor" size='50' value='<?php if($elemento!=null && $elemento->getProveedor()!= null) echo($elemento->getProveedor());?>'/></td>
            <td width='15%'>Domicilio*:</td>
            <td width='35%'><input type="text" name="domicilio" id="domicilio" size='50' value='<?php if($elemento!=null && $elemento->getDomicilio()!= null) echo($elemento->getDomicilio());?>'/></td>
          </tr>

          <tr>
            <td>Código postal*:</td>
                <td><input type="text" name="cp" id="cp" maxlength='5' value='<?php if($elemento!=null && $elemento->getCp()!= null) echo($elemento->getCp());?>' /></td>
            <td>Representante:</td>
            <td><input type="text" name="representante"  id="representante" size='50' value='<?php if($elemento!=null && $elemento->getRepresentante()!= null) echo($elemento->getRepresentante());?>'/></td>
          </tr>

          <tr>
            <td>Teléfono:</td>
                <td><input type="text" name="tel1" id="tel1" size='50' value='<?php if($elemento!=null && $elemento->getTelefono()!= null) echo($elemento->getTelefono());?>'/></td>
            <td>Otro Teléfono:</td>
            <td><input type="text" name="tel2" id="tel2" size='50' value='<?php if($elemento!=null && $elemento->getOtroTelefono()!= null) echo($elemento->getOtroTelefono());?>'/></td>
          </tr>


          <tr>
            <td>Correo electrónico:</td>
            <td ><input type="text" name="correo" id="correo" size='50' value='<?php if($elemento!=null && $elemento->getCorreo()!= null) echo($elemento->getCorreo());?>'/></td>
            <td>Estatus:</td>
            <td ><select name="estatus" id="estatus">

                <option value='1' <?php if($elemento!=null && $elemento->getEstatus()!= null && $elemento->getEstatus()==1 ) echo("selected='selected'"); ?>>Activo</option>
                <option value='0' <?php if($elemento!=null && $elemento->getEstatus()!= null && $elemento->getEstatus()==0 ) echo("selected='selected'"); ?>>Inactivo</option>
                </select>
          </tr>

          <tr>
            <td align="center" colspan="4"><input name="guardar" type="submit" value="Guardar"  class='btn' />
            &nbsp;&nbsp;&nbsp;&nbsp;<a href='lista_coordinacion_operativa.php' class='liga_btn'> Cancelar </a>
            </td>
          </tr>

        </table>
        <?php if($elemento!=null && $elemento->getId()!=null) echo("<input type='hidden' name='id' value='".$elemento->getId()."' />");?>
    </form>
</div>
<?php include 'footer.php' ?>
<script type="text/javascript">
 var frmvalidator  = new Validator("fproveedor");
 
 frmvalidator.addValidation("proveedor","req","Por favor capture el nombre del proveedor.");
 frmvalidator.addValidation("domicilio","req","Por favor capture el domicilio.");
 frmvalidator.addValidation("cp","req","Por favor capture el código postal.");
 frmvalidator.addValidation("cp","numeric","El código postal debe ser numerico");
 frmvalidator.addValidation("correo","email","El correo electrónico no es válido");
 
</script>

</body>
</html>
