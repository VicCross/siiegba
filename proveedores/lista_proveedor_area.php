<?php

session_start();

header('Content-Type: text/html; charset=UTF-8');

include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");

$parametro = new ParametroDaoJdbc();

$parametro = $parametro->obtieneElemento(5);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Sistema de Ingresos y Egresos</title>

<link rel="stylesheet" type="text/css" href="../css/style.css">

<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>

<script >

	function verificarSesion(){	

		$("#destino").load("../verificaSession.php", function(data){

			if(data.toString().indexOf("false") === -1){}

			else{

				window.location = "index.php?err=2";	

			}

		});

	}

</script>

<script>

$(document).ready(function() {                    

	$("#Perfil").css({

		background: "<?php echo $parametro->getValor(); ?>"

	});

	$(".div_menu").css({

		background: "<?php echo $parametro->getValor(); ?>"

	});

	$(".tb_presupuesto th").css({

		background: "<?php echo $parametro->getValor(); ?>"

	});

	$(".tb_presupuestoResumen th").css({

		background: "<?php echo $parametro->getValor(); ?>"

	});

	$(".tb_cat th").css({

		background: "<?php echo $parametro->getValor(); ?>"

	});

	$(".tb_add_cat").css({

		background: "<?php echo $parametro->getValor(); ?>"

	});

});

</script>

</head>

<body>

<div class="contenido">

	<div align='center' >

	  	<table align='center' width='90%' >

	  		<tr valign='top'>

	  			

				<td width='33%'>

				<h3 class="titulo_cat">Directivas</h3>

	  			<ul class='menu_catalogos'>

				

	  				<li><a href='lista_direccion.php'>Dirección</a></li> 

	  				<li><a href='lista_coordinacion_operativa.php'>Coordinación Operativa</a></li> 

					<li><a href='lista_proyectos_especiales.php'>Proyectos especiales</a></li> 

					<li><a href='lista_relaciones_publicas.php'>Relaciones públicas</a></li> 

					<li><a href='lista_sistemas.php'>Sistemas</a></li>

					
		    	        </ul>

				</td>

				

				

				<td width='33%'>

	  				<h3 class="titulo_cat">Técnicas</h3>

	  				<ul class='menu_catalogos'>

		    		<li><a href='lista_subdireccion_tecnica.php'>Subdirección Técnica</a></li>

		    		<li><a href='lista_ssypp.php'>Servicio Social y Prácticas Profesionales</a></li>

					<li><a href='lista_voluntariado.php'>Voluntariado</a></li> 

					<li><a href='lista_fotografia.php'>Fotografía</a></li> 

					<li><a href='lista_exhibicion.php'>Exhibición</a></li>

					<li><a href='lista_registro_control.php'>Registro y Control</a></li> 

					<li><a href='lista_comunicacion_enlace.php'>Comunicación y enlace</a></li> 

					<li><a href='lista_difusion.php'>Difusión</a></li> 

					<li><a href='lista_medios_electronicos.php'>Medios Electrónicos</a></li> 

		    		<li><a href='lista_prensa.php'>Prensa</a></li> 

					<li><a href='lista_mediacion.php'>Mediación</a></li> 

					<li><a href='lista_editorial.php'>Editorial</a></li> 

					<li><a href='lista_museografia.php'>Museografía</a></li> 

					<li><a href='lista_arquitectura.php'>Arquitectura</a></li> 

		           </ul>

				   

				</td>

				

				<td width='33%'>

	  				<h3 class="titulo_cat">Administrativas</h3>

	  				<ul class='menu_catalogos'>

		    		<li><a href='lista_subdireccion_admin.php'>Subdirección Administrativa</a></li>

		    		<li><a href='lista_recursos_humanos.php'>Recursos Humanos</a></li>

					<li><a href='lista_recursos_materiales.php'>Recursos Materiales</a></li> 

					<li><a href='lista_recursos_financieros.php'>Recursos Financieros</a></li> 

					<li><a href='lista_departamento_legal.php'>Departamento Legal</a></li>

					<li><a href='lista_taquillas.php'>Taquillas</a></li> 

					<li><a href='lista_custodios.php'>Custodios</a></li> 

					

		           </ul>

				

	  			</td>

				

				

				

				

	  		</tr>

	  	</table>

	</div>

	<br>

</div>

</body>

</html>