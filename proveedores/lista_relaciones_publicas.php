<?php

 session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION

 
include_once("dao/ProveedorDaoJdbc_Relaciones_Pub.class.php");

include_once($_SESSION['RAIZ'] . "/proveedores/beans/Proveedor_area.class.php");


if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {

    header("Location: " . $_SESSION['RAIZ'] . "/index.php");

}

 header('Content-Type: text/html; charset=UTF-8');
 
include_once("dao/ParametroDaoJdbc.class.php");



$parametro = new ParametroDaoJdbc();

$parametro = $parametro->obtieneElemento(5);

?>

<!DOCTYPE html PUBLIC >

<html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Sistema de Ingresos y Egresos</title>

<link rel="stylesheet" type="text/css" href="../css/style.css">

<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>



<!-- DataTables -->

        <link rel="stylesheet" type="text/css" href="../css/lista.css">

        <script type="text/javascript" src="../media/js/complete.js"></script>

        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>

        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>

        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>

        <script type="text/javascript" src="../js/lista.js"></script>

		

			   <input type="hidden" id="columnas_visibles" name="columnas_visibles" value="8"/>

    <input type="hidden" id="total_columnas" name="total_columnas" value="10"/>



</head>

<body>

<?php $respuesta = NULL;

    if(isset($_GET['respuesta'])){ $respuesta = $_GET['respuesta']; }?> 

<div class="contenido">

	<p class="titulo_cat1">Catálogos > Catálogos de Operación  > Catálogo de Proveedores  Relaciones públicas</p><br/>

    <?php 

        if($respuesta!=null){

            echo("<div align='center' class='msj'>".$respuesta."</div>");

        }

    ?>

        <div align="right"><a href='agrega_proveedor_relaciones.php' class='liga_btn'> Agregar Proveedor</a></div>

        <br> </br>

    <table  border="0" cellspacing="0" cellpadding="5" class='	dataTable' align='center' >

        <thead>

		<tr>

          <th width="19%">Proveedor</th>

          <th>Domicilio</th>

          <th>Código postal</th>

          <th>Representante</th>

          <th>Teléfono</th>

          <th>Otro Teléfono</th>

          <th>Correo electrónico</th>

          <th>Estatus</th>

          <th></th>

          <th></th>

        </tr>

		</thead>

		<tfoot>

		<tr>

          <th width="19%"></th>

          <th></th>

          <th></th>

          <th></th>

          <th></th>

          <th></th>

          <th></th>

          <th></th>

          <th></th>

          <th></th>

        </tr>

		</tfoot>

		<tbody>

    <?php

        $dao = new ProveedorDaoJdbc_Relaciones_Pub();

        $lista=$dao->obtieneListado();

        $elemento=new Proveedor();

 

        foreach($lista as $elemento){

                ?>

        <tr>

            <td><?php echo($elemento->getProveedor());?></td>

            <td><?php echo($elemento->getDomicilio());?></td>

            <td><?php echo($elemento->getCp());?></td>

            <td><?php if($elemento->getRepresentante()!=null){echo($elemento->getRepresentante());}?></td>

            <td><?php if($elemento->getTelefono()!=null){echo($elemento->getTelefono());}?></td>

            <td><?php if($elemento->getOtroTelefono()!=null ){echo($elemento->getOtroTelefono());}?></td>

            <td><?php if($elemento->getCorreo()!=null){echo($elemento->getCorreo());}?></td>

            <td><?php if($elemento->getEstatus()!=null){

                if($elemento->getEstatus()==1)

                        echo("Activo");

                else if($elemento->getEstatus()==0)

                                echo("Inactivo");

                }?></td>

            <td><a href='agrega_proveedor_relaciones.php?id=<?php echo($elemento->getId());?>' class='liga_cat'><img src="../img/Pencil3.png" alt="Editar" style="border: 0px none;" ></a></td>

            <td><a href='web/CatProveedor_Relaciones.php?id=<?php echo($elemento->getId());?>' onclick='return confirm("¿Esta seguro que desea eliminar este registro?")' class='liga_cat'><img src="../img/DeleteRed.png" alt="Borrar" style="border: 0px none;"></a></td>

          </tr>	

    <?php			

    	}

    ?>

	</tbody>

    </table>

	<br>

	

</div>

<?php include 'footer.php' ?>

</body>

</html>

