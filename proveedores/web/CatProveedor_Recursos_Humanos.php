<?php

session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once($_SESSION['RAIZ'] . "/proveedores/dao/ProveedorDaoJdbc_Recursos_Humanos.class.php");
include_once($_SESSION['RAIZ'] . "/proveedores/beans/Proveedor_area.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
$respuesta = "";
if (isset($_POST['guardar'])){
    $elemento=new Proveedor();
    $elemento->setProveedor($_POST["proveedor"]);
    $elemento->setDomicilio($_POST["domicilio"]);
    $elemento->setCp($_POST["cp"]);
    $elemento->setRepresentante($_POST["representante"]);
    $elemento->setTelefono($_POST["tel1"]);
    $elemento->setOtroTelefono($_POST["tel2"]);
    $elemento->setCorreo($_POST["correo"]);
    $elemento->setEstatus((int)($_POST["estatus"]));
    
    if ($_POST["id"] != null){
        $elemento->setId((int)($_POST["id"]));
        $dao=new ProveedorDaoJdbc_Recursos_Humanos();
        $res = $dao->actualizaElemento($elemento);
        if ($res) {
            $respuesta = "Su información se actualizó exitosamente.";
        } else {
            $respuesta = "No fue posible actualizar su información.";
        }
    }else{
        $dao=new ProveedorDaoJdbc_Recursos_Humanos();
	$res=$dao->guardaElemento($elemento);
        
        if ($res) {
            $respuesta = "Su información se almacenó exitosamente.";
        } else {
            $respuesta = "No fue posible almacenar su información.";
        }
    }
}else if (isset($_GET['id'])){
    $dao=new ProveedorDaoJdbc_Recursos_Humanos();
    $res=$dao->eliminaElemento((int)($_GET['id']));

    if ($res) {
        $respuesta = "El registro se eliminó exitosamente.";
    } else {
        $respuesta = "No fue posible eliminar el registro de la base de datos.";
    }
} else {
    $respuesta = "No se detecto la acción a realizar.";
}
header("Location: ../lista_recursos_humanos.php?respuesta=" . $respuesta);


?>