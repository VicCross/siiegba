<?php

if(session_id() == '') {

    session_start();

}

include_once($_SESSION['RAIZ'] . "/proveedores/beans/Proveedor_area.class.php");

include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");



class ProveedorDaoJdbc_Coordinacion {

    

    public function obtieneListado() {

		

        $lista= array();



        $query="SELECT * FROM sie_prov_coordinacion_operativa WHERE cpr_estatus=1 ORDER BY cpr_proveedor";

        $catalogo = new Catalogo();

        

        $result = $catalogo->obtenerLista($query);



        while ($rs = mysql_fetch_array($result)){

            $id= $rs[strtoupper("cpr_id_proveedor")];

            $proveedor= $rs[strtoupper("cpr_proveedor")];

            $domicilio= $rs[strtoupper("cpr_domicilio")];

            $cp= $rs[strtoupper("cpr_cp")];

            $representante= $rs[strtoupper("cpr_representate")];

            $telefono= $rs[strtoupper("cpr_telefono")];

            $otro_tel= $rs[strtoupper("cpr_otro_tel")];

            $correo= $rs[strtoupper("cpr_correo")];

            $estatus= $rs[strtoupper("cpr_estatus")];



            $elemento = new Proveedor();

            $elemento->setAll($id,$proveedor,$domicilio,$cp,$representante,$telefono,$otro_tel,$correo,$estatus);

            array_push($lista, $elemento);

        }	



        return $lista;

    }

    

    public function obtieneListadoCatalogos(){

        

        $lista= array();



        $query="SELECT * FROM sie_prov_coordinacion_operativa  ORDER BY cpr_proveedor";

        $catalogo = new Catalogo();

        

        $result = $catalogo->obtenerLista($query);



        while ($rs = mysql_fetch_array($result)){

            $id= $rs[strtoupper("cpr_id_proveedor")];

            $proveedor= $rs[strtoupper("cpr_proveedor")];

            $domicilio= $rs[strtoupper("cpr_domicilio")];

            $cp= $rs[strtoupper("cpr_cp")];

            $representante= $rs[strtoupper("cpr_representate")];

            $telefono= $rs[strtoupper("cpr_telefono")];

            $otro_tel= $rs[strtoupper("cpr_otro_tel")];

            $correo= $rs[strtoupper("cpr_correo")];

            $estatus= $rs[strtoupper("cpr_estatus")];



            $elemento = new Proveedor();

            $elemento->setAll($id,$proveedor,$domicilio,$cp,$representante,$telefono,$otro_tel,$correo,$estatus);

            array_push($lista, $elemento);

        }	



        return $lista;

    }

    

    public function obtieneElemento($idElemento) {

		

	$elemento=new Proveedor();

		

	$query="SELECT * FROM sie_prov_coordinacion_operativa WHERE cpr_id_proveedor=".$idElemento;

        $catalogo = new Catalogo();

        

        $result = $catalogo->obtenerLista($query);



        while ($rs = mysql_fetch_array($result)){

            $id= $rs[strtoupper("cpr_id_proveedor")];

            $proveedor= $rs[strtoupper("cpr_proveedor")];

            $domicilio= $rs[strtoupper("cpr_domicilio")];

            $cp= $rs[strtoupper("cpr_cp")];

            $representante= $rs[strtoupper("cpr_representate")];

            $telefono= $rs[strtoupper("cpr_telefono")];

            $otro_tel= $rs[strtoupper("cpr_otro_tel")];

            $correo= $rs[strtoupper("cpr_correo")];

            $estatus= $rs[strtoupper("cpr_estatus")];



            $elemento = new Proveedor();

            $elemento->setAll($id,$proveedor,$domicilio,$cp,$representante,$telefono,$otro_tel,$correo,$estatus);

        }

		

	return $elemento;	

    }

    

    public function obtieneElementoDesc($desc) {

        

        $elemento=new Proveedor();

		

	$query="SELECT * FROM sie_prov_coordinacion_operativa WHERE cpr_proveedor='".$desc."'";

        $catalogo = new Catalogo();

        

        $result = $catalogo->obtenerLista($query);



        while ($rs = mysql_fetch_array($result)){

            $id= $rs[strtoupper("cpr_id_proveedor")];

            $proveedor= $rs[strtoupper("cpr_proveedor")];

            $domicilio= $rs[strtoupper("cpr_domicilio")];

            $cp= $rs[strtoupper("cpr_cp")];

            $representante= $rs[strtoupper("cpr_representate")];

            $telefono= $rs[strtoupper("cpr_telefono")];

            $otro_tel= $rs[strtoupper("cpr_otro_tel")];

            $correo= $rs[strtoupper("cpr_correo")];

            $estatus= $rs[strtoupper("cpr_estatus")];



            $elemento = new Proveedor();

            $elemento->setAll($id,$proveedor,$domicilio,$cp,$representante,$telefono,$otro_tel,$correo,$estatus);

        }

		

	return $elemento;

    }

    

    public function guardaElemento($elemento) {

		

        $con=new Catalogo();

        $query="INSERT INTO sie_prov_coordinacion_operativa(cpr_proveedor,cpr_domicilio,cpr_cp,cpr_representate,cpr_telefono,cpr_otro_tel,cpr_correo,cpr_estatus)".

		" VALUES ('".mb_strtoupper($elemento->getProveedor(),'UTF-8')."', '".mb_strtoupper($elemento->getDomicilio(),'UTF-8')."', '".mb_strtoupper($elemento->getCp(),'UTF-8')."', '".mb_strtoupper($elemento->getRepresentante(),'UTF-8')."', '".mb_strtoupper($elemento->getTelefono(),'UTF-8')."' , '".mb_strtoupper($elemento->getOtroTelefono(),'UTF-8')."' , '".mb_strtoupper($elemento->getCorreo(),'UTF-8')."','".$elemento->getEstatus()."' )";

        $res=$con->obtenerLista($query);



        if($res=="1")

        {	return true; }

        else

        {	return false; }	

    }

    

    public function actualizaElemento($elemento) {

		

	$con=new Catalogo();

	$query="UPDATE sie_prov_coordinacion_operativa set  cpr_proveedor='".mb_strtoupper($elemento->getProveedor(),'UTF-8')."', cpr_domicilio='".mb_strtoupper($elemento->getDomicilio(),'UTF-8')."', cpr_cp='".mb_strtoupper($elemento->getCp(),'UTF-8')."' , cpr_representate='".mb_strtoupper($elemento->getRepresentante(),'UTF-8')."' , cpr_telefono='".mb_strtoupper($elemento->getTelefono(),'UTF-8')."' , cpr_otro_tel='".mb_strtoupper($elemento->getOtroTelefono(),'UTF-8')."' , cpr_correo='".mb_strtoupper($elemento->getCorreo(),'UTF-8')."', cpr_estatus=".$elemento->getEstatus()."  WHERE cpr_id_proveedor=".$elemento->getId();

	$res = $con->obtenerLista($query);

		

	if($res == "1")

        {	return true; }

        else

        {	return false; }

		

    }

    

    public function eliminaElemento($idElemento){

		

        $con=new Catalogo();

        $query="UPDATE sie_prov_coordinacion_operativa set  cpr_estatus=0 WHERE cpr_id_proveedor=".$idElemento;

        $res = $con->obtenerLista($query);



        if($res=="1")

        {	return true; }

        else

        {	return false; }

		

    }

}
