<?php
session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once("../src/mx/com/virreinato/dao/IngresosDonativoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/IngresosDonativo.class.php");
include_once("../src/mx/com/virreinato/dao/ExposicionesTemporalesDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ExposicionesTemporales.class.php");
include_once("../src/mx/com/virreinato/dao/LineaAccionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/LineaAccion.class.php");
include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}

header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>


<!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="../css/lista.css">
        <script type="text/javascript" src="../media/js/complete.js"></script>
        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
        <script type="text/javascript" src="../js/lista.js"></script>
		<input type="hidden" id="columnas_visibles" name="columnas_visibles" value="5"/>
    	<input type="hidden" id="total_columnas" name="total_columnas" value="7"/>
</head>
<body>
    <?php $respuesta = NULL;
        if(isset($_GET['respuesta'])){ $respuesta = $_GET['respuesta']; }?>  
    <div class="contenido">
    <br>
	 <p class="titulo_cat1">Menú de Catálogos &gt; Menú de Donativos</p><br/>
         <?php 
            if($respuesta!=null){
                echo("<div align='center' class='msj'>".$respuesta."</div>");
            }
        ?>
         <!--div align="right"><a href='agrega_usuario.php' class='liga_btn'> Agregar Usuario </a></div-->
         <br> </br>
    <table  border="0" cellspacing="0" cellpadding="5" class='dataTable' align='center' >
	  <thead>
	  <tr>
	    <th width='10%'>Patrocinador</th>
	    <th width='10%'>Tipo de Donación</th>
	    <th width='10%'>Área</th>
        <th width="10%">Monto</th>
        <th width='10%'>Fecha</th>
        <th width="10%">Proyecto Destino</th>
		<th width="10%">Exposición Temporal</th>
		<th width="10%">Descripción</th>
		<th width="10%">Documento</th>
		<th width="5%"></th>
		<th width='5%'></th>
	  </tr>
	  </thead>
	  <tbody>
		<?php
        //$dao = new IngresosDonativoDaoJdbc();
        //$lista = $dao->obtieneListaDonativo();
        //$elemento = new IngresosDonativo();
		
		$query = "select scdp.*, ca.CAR_AREA, scla.CLA_LINEAACCION, scet.nombre from sie_cat_donativos_palacio scdp
				left join cat_areas ca ON ca.car_id_area = scdp.area
				left join sie_cat_linea_accion scla ON scla.CLA_ID_LINEAACCION = scdp.idProyecto
				left join sie_cat_expo_temp scet ON scet.id_Subproyecto = scdp.idExpoTemp";
		$catalogo = new Catalogo();
		$lista = array();
		$result = $catalogo->obtenerLista($query);
		
		while($rs = mysql_fetch_array($result)){
		?>
    <tr class="SizeText">
        <td align="center"><?php echo $rs['nom_patrocinador'];?></td>
        <td align="center"><?php echo $rs['tipo_donacion'];?></td>
        <td align="center"><?php echo $rs['CAR_AREA'];?></td>
        <td align="center"><?php echo $rs['monto'];?></td>
	    <td align="center"><?php echo $rs['fecha'];?></td>
        <td align="center"><?php echo $rs['nombre'];?></td>
        <td align="center"><?php echo $rs['CLA_LINEAACCION'];?></td>
        <td align="center"><?php echo $rs['descripcion'];?></td>
		<td align="center">
		<?php
		if(isset($rs['documento'])){
			$documentoA = "../../../pruebas/siiegba".substr($rs['documento'],2);
		?>
		<a href="<?php echo $documentoA;?>" target="_blank">Ver documento</a>
		<?php
		}
		?>
		</td>
		<td align="center"><a href='agrega_donativoPalacio.php?id=<?php echo $rs['id_donativo'];?>' class='liga_cat'><img src="../img/Pencil3.png" alt="Aceptar" style="border: 0px none;" ></a></td>
        <td align="center"><a href='../src/mx/com/virreinato/web/CatIngresoDon.php?id=<?php echo $rs['id_donativo']; ?>' onclick='return confirm("¿Esta seguro que desea eliminar este registro?")' class='liga_cat'><img src="../img/DeleteRed.png" alt="Borrar" style="border: 0px none;"></a></td>
	</tr>	
    <?php			
		}
	  ?>
	  </tbody>
	  <tfoot>
	  <tr>
	    <th width='10%'></th>
	    <th width='10%'></th>
	    <th width='10%'></th>
        <th width="10%"></th>
        <th width='10%'></th>
        <th width="10%"></th>
		<th width="10%"></th>
		<th width="10%"></th>
		<th width="10%"></th>
		<th width="5%"></th>
		<th width='5%'></th>
	  </tr>
	  </tfoot>
    </table>
    
    
        <br /><br />
        <div align="center"> <a href='agrega_donativoPalacio.php' class='liga_btn'>Agregar Donativo</a> </div>
	<br>
	
</div>
</body>
</html>
