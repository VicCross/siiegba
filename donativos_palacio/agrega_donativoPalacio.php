<?php
session_start();
//include_once("../src/mx/com/virreinato/dao/IngresosPatronatoDaoJdbc.class.php");
//include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/IngresosPatronato.class.php");
include_once("../src/mx/com/virreinato/dao/IngresosDonativoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/IngresosDonativo.class.php");
include_once("../src/mx/com/virreinato/dao/LineaAccionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/LineaAccion.class.php");
include_once("../src/mx/com/virreinato/dao/ExposicionesTemporalesDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ExposicionesTemporales.class.php");
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");

$nota = "";

if(isset($_GET['id'])){
$dao = new IngresosDonativoDaoJdbc();
$IngPat = new IngresosDonativo();
$IngPat = $dao->obtieneElemento($_GET['id']);
$TipoDonacion = $IngPat->getTipo_donacion();
$areaAux2 = $IngPat->getArea();
$descripcion = $IngPat->getDescripcion();
$nota = "Solo se modifica el nombre del patrocinador, el monto y el archivo";
}
	$s = "";
	$s2 = "";
	$s3 = "";
	$MYSQL_server = "mysql1002.mochahost.com";
	$MYSQL_usuario = "ccilidon_mba13";
	$MYSQL_contraseña = "s11egba2015";
	$MYSQL_bd = "ccilidon_siiegba_pruebas";
	$conexion = @new mysqli($MYSQL_server, $MYSQL_usuario, $MYSQL_contraseña, $MYSQL_bd);
		if ($conexion->connect_error) //verificamos si hubo un error al conectar, recuerden que pusimos el @ para evitarlo
		{
    	die('Error de conexión: '); //si hay un error termina la aplicación y mostramos el error
        }
	    $sql="SELECT * from cat_areas";
	    $result = $conexion->query($sql); //usamos la conexion para dar un resultado a la variable
	    if ($result->num_rows > 0) //si la variable tiene al menos 1 fila entonces seguimos con el codigo
	    {
			$combobit="";
			while ($row = $result->fetch_array(MYSQLI_ASSOC)) 
			{
				if(isset($IngPat) && isset($areaAux2) && $areaAux2 == $row['car_id_area']){ $s = "selected"; }
				$combobit .=" <option value='". $row['car_id_area'] ."' $s >".utf8_encode($row['CAR_AREA'])."</option>"; //concatenamos el los options para luego ser insertado en el HTML
			}
		}
        else
        {
			echo "No hubo resultados";
		}
    ?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Donativo</title>
</head>
<script>
	
	function mostrarExpoTemp()
	{
		var id = $("#proyecto").val();
		if(id == 48){
			$("#expotemp").show();	
		}else{
			$("#expotemp").hide();
		}		
	}
</script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<body onload="mostrarExpoTemp()">
	<div class="contenido"> 
        <p class="titulo_cat1">Menú de Catálogos &gt; Menú de Donativo > Nuevo Ingreso</p>
        <form name="ingresoP" id="ingresoP" method="post" action="../src/mx/com/virreinato/web/CatIngresoDon.php" enctype="multipart/form-data">
            <table width="55%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
            	<tr>
                	<td colspan="2" align="center"><h3>Donativo</h3></td>
    			</tr>
				<?php
					if($nota != ""){
						echo "<tr>";
						echo "<td colspan='2' align='center'><h4>$nota</h4></td>";
						echo "</tr>";
					}
				?>
                <tr>
                	<td width="20%">Nombre del Patrocinador*:</td>
                    <td><input type="text" name="patrocinador" id="patrocinador" value="<?php if(!is_null($IngPat)) echo($IngPat->getNom_patrocinador());?>"/></td>
                </tr>
				
				<tr>
                	<td width="20%">Tipo de donación*:</td>
                	<!--combo box que lleva especie o  efectivo -->
                    <td><select name="tipo_donacion" id="tipo_donacion" style="width:200px" >
                    <option value="C01">-- ELEGIR UNA OPCIÓN --</option>
					<?php
						if(isset($IngPat) && isset($TipoDonacion ) && $TipoDonacion  == "EFECTIVO"){ $s2 = "selected";}
					?>
                    <option value="EFECTIVO" <?php echo $s2?> >EFECTIVO</option>
					<?php
						if(isset($IngPat) && isset($TipoDonacion ) && $TipoDonacion  == "ESPECIE"){ $s3 = "selected";}
					?>
  					<option value="ESPECIE" <?php echo $s3?> >ESPECIE</option>  
  					</select></td>
					
                </tr>
                
				<tr>
                	<td width="20%">Área*:</td>
					<td><select name="area" id="area" style="width:200px" >
                    <!-- área que consiguio el donativo  se muestran  un listado con todas las áreas -->
                    <option value="C01">-- ELEGIR UNA OPCIÓN --</option>
					<?php echo $combobit; ?>
                    </select></td>
                </tr>
				
                <tr>
                	<td width="20%">Monto*:</td>
                    <td><input type="text" name="monto" id="monto" value="<?php if(!is_null($IngPat)) echo($IngPat->getMonto());?>"/></td>
                </tr>
				<td width="25%">Periodo:</td>
                    <td><select name="periodo" id="periodo" style="width:110px" > 
                    <?php
                    $daoPer = new PeriodoDaoJdbc();
                    $listaPer = $daoPer->obtieneListado();
                    $elemento = new Periodo();
                    foreach ($listaPer as $elemento) {
                        $sel = "";
                        if (isset($_GET['periodo']) && (int) ( $_GET['periodo'] ) == $elemento->getId())
                            $sel = "selected='selected'";
                        else if ($_GET['periodo'] == NULL && (int) ($elemento->getPeriodo()) == date('Y')) {
                            $sel = "selected='selected'";
                            $periodo = (String) ($elemento->getId() );
                        }
                        echo("<option value=" . $elemento->getId() . " " . $sel . " >" . $elemento->getPeriodo() . "</option>");
                    }
                    ?>  
                </select></td>
                <tr>
                	<td width="20%">Proyecto*:</td>
                    <td>
                    	<select name="proyecto" id="proyecto" onchange="mostrarExpoTemp()">
                            
                            <?php
							if(is_null($IngPat))
                        		echo('<option value="0">----Selecciona un Proyecto----</option>');
							
                            $daoProy = new LineaAccionDaoJdbc();
							$lista = $daoProy->obtieneListado();
							$elemento = new LineaAccion();
                            foreach($lista as $elemento){
								if(is_null($IngPat) || $IngPat->getProyecto_destino() == $elemento->getId()){
                            ?>
                            <option value="<?php echo($elemento->getId());?>"><?php echo($elemento->getLineaAccion());?></option>
                            <?php
								}
                            }
                            ?>
                        </select>
                    </td>
					
                </tr>
                <tr>
                	<td width="20%">Exposición Temporal:</td>
                    <td>
                    	<select name="expotemp" id="expotemp" <?php if(is_null($IngPat) || is_null($IngPat->getExpo_temp())){?>style="display:none;"<?php }?>>
                    	<?php
						$daoET = new ExposicionesTemporalesDaoJdbc();
						$lista = $daoET->getListado();
						$elemento = new ExposicionesTemporales();
						foreach($lista as $elemento){
							if(is_null($IngPat) || $IngPat->getExpo_temp() == $elemento->getIdExposicionT()){
						?>
                        	<option value="<?php echo($elemento->getIdExposicionT());?>"><?php echo($elemento->getNombre())?></option>
                        <?php }}?>
                    	</select>
                    </td>
                </tr>
				
				<tr>
                	<td width="20%">Descripcion*:</td>
					<!--es nuestra caja de comentarios para cuando  tengamos un donativo en especie, aqui vamos a decir que es y por lo estan dando -->
                	<td><textarea name="descripcion" id="" cols="30" rows="5"><?php if(!is_null($IngPat) && isset($descripcion)) echo($descripcion);?></textarea></td>
                </tr>
                
				
				
				
				
                 <tr>
                	<td width="20%">Justificante (Menor a 2MB): <?php if(!is_null($IngPat) && !is_null($IngPat->getDocumento())) echo("<a target='_blank' href='../../../pruebas/siiegba".substr($IngPat->getDocumento(),2)."'>Ver Documento</a>");?></td>
                	<td><input name="archivo" type="file" id="archivo"/></td>
                 </tr>
                <tr>
                	<td colspan="2" align="center"><input type="submit" value="Guardar" style="cursor:pointer" class='btn' name="guardar" id="guardar"/>
                    </td>
                </tr>
            </table>
            <?php if(isset($_GET['id']))echo('<input type="hidden" id="id" name="id" value="' . $_GET['id'] . '" />');?>
        </form>
        <br /><br />
        <div align="center"> <a href='lista_donativospalacio.php' class='liga_btn'>Regresar</a> </div>
    </div>
</body>
<script>
 var frmvalidator  = new Validator("ingresoP");
 frmvalidator.addValidation("patrocinador", "req", "Por favor ingrese el nombre del patrocinador.");
 frmvalidator.addValidation("monto", "req", "Por favor ingrese el valor del monto.");
 frmvalidator.addValidation("monto", "num", "El monto debe ser númerico.");
 frmvalidator.addValidation("proyecto", "dontselect=0", "Por favor seleccione un proyecto.");

</script>

<?php $conexion->close(); //cerramos la conexión ?>
</html> 