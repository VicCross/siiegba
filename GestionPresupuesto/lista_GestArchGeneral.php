<?php
session_start(); //Importante iniciar sesion cada pagina que ocupes $_SESSION
include_once("../src/mx/com/virreinato/dao/GestionPresupuestalDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/GestionPresupuestal.class.php");
include_once("../src/classes/Catalogo.class.php");
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
}
/*Checamos su perfil*/
$esLiderArea = false;
if($_SESSION['id'] == 5) $esLiderArea = true;
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
?>
<!DOCTYPE html PUBLIC >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>


<!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="../css/lista.css">
        <script type="text/javascript" src="../media/js/complete.js"></script>
        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
        <script type="text/javascript" src="../js/lista.js"></script>
			   <input type="hidden" id="columnas_visibles" name="columnas_visibles" value="4"/>
    <input type="hidden" id="total_columnas" name="total_columnas" value="<?php if($esLiderArea)echo("4");else echo("7")?>"/>
</head>
<body>
    <?php $respuesta = NULL;
        if(isset($_GET['respuesta'])){ $respuesta = $_GET['respuesta']; }?>  
    <div class="contenido">
    <br>
	 <p class="titulo_cat1">Gestión de Presupuesto > Lista de Solicitudes</p><br/>
         <?php 
            if($respuesta!=null){
                echo("<div align='center' class='msj'>".$respuesta."</div>");
            }
        ?>
         <!--div align="right"><a href='agrega_usuario.php' class='liga_btn'> Agregar Usuario </a></div-->
         <br> </br>
    <table  border="0" cellspacing="0" cellpadding="5" class='dataTable' align='center' >
	  <thead>
	  <tr>
	    <th width='22.5%'>Proyecto</th>
	    <th width='22.5%'>Área</th>
	    <th width='12.5%'>Importe de la Factura</th>
        <th width="20%">Estatus</th>
        <th width='12.5%'></th>
	  </tr>
	  </thead>
	  <tfoot>
	  <tr>
	    <th width='22.5%'></th>
	    <th width='22.5%'></th>
	    <th width='12.5%'></th>
        <th width="20%"></th>
        <th width='12.5%'></th>
	  </tr>
	  </tfoot>
	  <tbody>
    <?php
        $dao=new GestionPresupuestalDaoJdbc();
        $lista=$dao->obtieneLista();
        $elemento=new GestionPresupuestal();
		$catalogo = new Catalogo();
            foreach($lista as $elemento){
				$idArea = $elemento->getIdArea();
				$idProyecto = $elemento->getIdProyecto();
				$idEstatus = $elemento->getEstatus();
				$query = "SELECT car_area FROM cat_areas WHERE car_id_area = " . $idArea;
				$resul = $catalogo->obtenerLista($query);
				if($rs = mysql_fetch_array($resul)) $nomArea = $rs[0];
				$query = "SELECT cla_lineaaccion FROM sie_cat_linea_accion WHERE cla_id_lineaaccion = " . $idProyecto;
				$resul = $catalogo->obtenerLista($query);
				if($rs = mysql_fetch_array($resul)) $nomProy = $rs[0];
				$query = "SELECT descripcion FROM cat_status_GP WHERE id_statusGP = " . $idEstatus;
				$resul = $catalogo->obtenerLista($query);
				if($rs = mysql_fetch_array($resul)) $nomEstatus = $rs[0];
            ?>
    <tr class="SizeText">
        <td align="center" ><?php echo($nomProy);?></td>
        <td align="center"><?php echo($nomArea);?></td>
        <td align="center"><?php echo('$' . $elemento->getImporte());?></td>
        <td align="center"><?php echo($nomEstatus);?></td>
        <td align="center"><a href="observaSolicitud.php?idG=<?php echo ($elemento->getId());?>">Revisar Datos</a></td>
      </tr>	
    <?php			
		}
	  ?>
	  </tbody>
    </table>
	<br>
	
</div>
</body>
</html>
