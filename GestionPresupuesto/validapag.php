<?php

include_once("../src/mx/com/virreinato/dao/GestionPresupuestalDaoJdbc.class.php");
include_once("../src/mx/com/virreinato/beans/GestionPresupuestal.class.php");
include_once("../src/classes/Catalogo.class.php");
header('Content-Type: text/html; charset=UTF-8');
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);

 
$idGestion = $_POST["idProyecto"];


?>
<!DOCTYPE html PUBLIC >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema de Ingresos y Egresos</title>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>

<!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="../css/lista.css">
        <script type="text/javascript" src="../media/js/complete.js"></script>
        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
        <script type="text/javascript" src="../js/lista.js"></script>
			   <input type="hidden" id="columnas_visibles" name="columnas_visibles" value="4"/>
    <input type="hidden" id="total_columnas" name="total_columnas" value="5"/>
</head>

<?php


$catalogo = new Catalogo();
$dao = new GestionPresupuestalDaoJdbc();
$gestion = $dao->obtieneDetalleSolicitud($idGestion);
?>

<body>
	<div class="contenido"> 
       
        <form name="gestion" id="gestion" method="post" action="insertaPago.php" enctype="multipart/form-data">
            <table width="55%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
            	
                
                
                <tr>
                	<td width="20%">Proyecto:</td>
                    <td>
                    <?php 
						$query = "SELECT cla_lineaaccion FROM sie_cat_linea_accion WHERE cla_id_lineaaccion = " . $gestion->getIdProyecto();
						$resul = $catalogo->obtenerLista($query);
						if($rs = mysql_fetch_array($resul))echo($rs[0]);
					?>
                    </td>
                </tr>
                <tr>
                	<td width="20%">Exposición Temporal:</td>
                    <td>
                    <?php 
						if(!is_null($gestion->getIdExpoTemp())){
							$query = "SELECT nombre FROM sie_cat_expo_temp WHERE id_Subproyecto = " . $gestion->getIdExpoTemp();
							$resul = $catalogo->obtenerLista($query);
							if($rs = mysql_fetch_array($resul))echo($rs[0]);
						}else{
							echo("-");	
						}
						
					?>
                    </td>
                </tr>
                <tr>
                	<td width="20%">Área:</td>
                    <td>
					<?php 
                        $query = "SELECT car_area, presupuesto_inicial, saldo FROM cat_areas WHERE car_id_area = " . $gestion->getIdArea();
						$resul = $catalogo->obtenerLista($query);
						if($rs = mysql_fetch_array($resul)){
							$nomArea = $rs[0];	
							$presup = $rs[1];
							$saldo = $rs[2];
						}echo($nomArea);                        
                    ?>
                    </td>
                </tr>
                <tr>
                	<td width="20%">Presupuesto del proyecto:</td>
                    <td><?php echo($presup);?></td>
                </tr>                
                <tr>
                	<td width="20%">Importe de Factura:</td>
                    <td><?php echo($gestion->getImporte());?></td>
                </tr>
                <tr>
                	<td width="20%">Saldo actual (considerando esta solicitud):</td>
                    <td><?php echo($saldo);?></td>
                </tr>
                <tr>
                	<td width="20%">Meta:</td>
                    <td><?php echo($gestion->getMeta());?></td>                    
                </tr>
                <tr>
                	<td width="20%">Submeta:</td>
                    <td><?php echo($gestion->getSubmeta());?></td>                    
                </tr>
                <tr>
                	<td width="20%">Número de factura:</td>
                    <td><?php echo($gestion->getNumFactura());?></td>                    
                </tr>
               
                <tr>
                	<td width="20%">Estatus:</td>
                    <td>
					<select name="pago">
					<option value="PAGADO">PAGADO</option>
					<option value="SIN PAGO">SIN PAGO</option>
					</select>
					</td>                    
                </tr>
          
				 <tr>
			   
			    <tr>
                	<td width="20%">Importe Pago INBA:</td>
                    <td><input type="number" name="PINBA" max="<?php echo($gestion->getImporte());?>" ></td>                    
                </tr>
				 <tr>
                	<td width="20%">Importe Pago PATRONATO:</td>
                    <td><input type="number" name="PPATRONATO" max="<?php echo($gestion->getImporte());?>" ></td>                    
                </tr>	
                <tr>
                	<td width="20%">Importe Pago EXTERNOS:</td>
                    <td><input type="number" name="PEXTERNO" max="<?php echo($gestion->getImporte());?>" ></td>                    
                </tr>
				 </tr>
				<tr><td><br><br></td> </tr>
			   <tr>
				 <!-- <tr><td><h2 width="50%">Datos Para Facturar </h2> </td></tr> -->
			     <tr>
			     </tr>
				<tr><td><br><br></td> </tr>
			   <tr>
                	<!-- <td width="50%" ><h4>INSTITUTO NACIONAL DE BELLAS ARTES Y LITERATURA INB 470101FA5 AV. JUAREZ NUM. 101 COL. CENTRO CP 06040, CUAUHTEMOC, MEXICO D.F</h4></td>
                    -->
                </tr>
				<tr><td><br><br></td> </tr>
			   <tr>
			       <!--
                	<td width="50%"> <h4>AMIGOS DEL MUSEO DEL PALACIO DE BELLAS ARTES, A.C. AMP 890926GH2 AV. JUÁREZ 4 PISO 10 COL. CENTRO, CP 06000,  DELEG. CUAUHTEMOC, MÉXICO, D.F</h4></td>
                    -->          
                </tr>              
            </table>
			
			<br><br><br>
		<input type="hidden" name="proyecto" value="<?php echo $gestion->getIdProyecto() ?>">
		<input type="hidden" name="factura" value="<?php echo  $gestion->getImporte() ?>">
		<input type="hidden" name="idGestion" value="<?php echo $gestion->getId() ?>">	
		<input type="hidden" name="idexpotemp" value="<?php echo $gestion->getIdExpoTemp() ?>">	
		<center><input type="submit" class="btn btn-primary"></center>	
        </form>
    </div>
    <div class="footer" align='center'> 
<a href='javascript:window.history.back();' class='liga_btn'> Regresar </a>
</div>
</body>

</html>