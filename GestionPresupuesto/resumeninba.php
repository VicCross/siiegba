<?php
$id = null;
$user = null;
	
session_start();
include_once("../src/mx/com/virreinato/dao/PeriodoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Periodo.class.php");
include_once("../src/mx/com/virreinato/dao/LineaAccionDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/LineaAccion.class.php");
include_once("../src/mx/com/virreinato/dao/AreaDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Area.class.php");
include_once("../src/mx/com/virreinato/dao/ExposicionesTemporalesDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ExposicionesTemporales.class.php");
include_once("../src/mx/com/virreinato/dao/GestionPresupuestalDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/GestionPresupuestal.class.php");
include_once("../src/mx/com/virreinato/dao/IngresosINBADaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/IngresosINBA.class.php");
include_once("../src/mx/com/virreinato/dao/IngresosPatronatoDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/IngresosPatronato.class.php");
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);
if (!isset($_SESSION['idUsuario']) || $_SESSION['idUsuario'] == "") {
    header("Location: " . $_SESSION['RAIZ'] . "/index.php");
} else {
    if (isset($_SESSION['id'])) {
        $id = $_SESSION['id'];
    }
    if (isset($_SESSION['user'])) {
        $user = $_SESSION['user'];
    }
}
include_once("../src/mx/com/virreinato/dao/ParametroDaoJdbc.class.php");
$parametro = new ParametroDaoJdbc();
$parametro = $parametro->obtieneElemento(5);

$array_numero_partidas = array();
$array_numero_metas = array();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="../css/style.css" />
        <script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
        <link rel="stylesheet" type="text/css" href="../css/lista.css">
        <script type="text/javascript" src="../media/js/complete.js"></script>
        <script src="../media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script type="text/javascript" src="../media/js/jquery.dataTables.columnFilter.js"></script>
        <script type="text/javascript" src="../js/lista.js"></script>
        <title>Consulta de Presupuesto (Resumen) </title>
    </head>
    <body>
        <div class="contenido">
            <br/>
            <p class="titulo_cat1">Consulta de Presupuesto > Resumen</p>
            <?php
                if(isset($_GET['recargado']) && $_GET['recargado'] == "1"){
                    echo '<input type="hidden" id="nombre_form" name="nombre_form" value=""/>';
                }else{
                    echo '<input type="hidden" id="nombre_form" name="nombre_form" value="frmFiltroResmuen"/>';
                }
            ?>            
            <input type="hidden" id="no_sort" name="no_sort" value="1"/>
            <form name="frmFiltroResumen" id="frmFiltroResmuen" method="POST" action="../src/mx/com/virreinato/web/WebConsultaPresupuesto.php" >
                <?php
                $periodo = "";
				$hayNumerosRojos = false;
				
                ?>
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Período:  
                <select name="periodo" id="periodo" style="width:110px" > 
                    <?php
                    $daoPer = new PeriodoDaoJdbc();
                    $listaPer = $daoPer->obtieneListado();
                    $elementoPer = new Periodo();
                    foreach ($listaPer as $elementoPer) {
                        $sel = "";
                        if (isset($_GET['periodo']) && (int) ( $_GET['periodo'] ) == $elementoPer->getId())
                            $sel = "selected='selected'";
                        else if ($_GET['periodo'] == NULL && (int) ($elementoPer->getPeriodo()) == date('Y')) {
                            $sel = "selected='selected'";
                            $periodo = (String) ($elementoPer->getId() );
                        }
                        echo("<option value=" . $elementoPer->getId() . " " . $sel . " >" . $elementoPer->getPeriodo() . "</option>");
                    }
                    ?>  
                </select>
                &nbsp; <input name="Filtro" style="cursor:pointer" type="submit" value="Filtrar"  class='btn' />
            </form>
            <br/>
            <?php echo("Periodo: " . $_GET['periodo']);?>
            <br/>	
            <table border="1" cellspacing="0" cellpadding="3" class="dataTable" align="right">
            	<thead>
                	<tr >
                    	<th width="23%" >INBA</th>
                        <th width="11%">Autorizado</th>
                        <th width="11%">Gestionado</th>
                        <th width="11%" >Por Gestionar</th>
                        <th width="11%" >Pagado</th>
                       
                        
                    </tr>
                </thead>	
                <tfoot>
                    <tr >
                        <th width="23%" ></th>
                        <th width="11%"></th>
                        <th width="11%"></th>
                        <th width="11%" ></th>
                        <th width="11%" ></th>
                        
                       
                    </tr>

                </tfoot>
                <tbody>
                	<?php
						
						
						$tot1 = 0.0; $tot2 = 0.0; $tot3 = 0.0; $tot4 = 0.0; $tot5 = 0.0; $tot6 = 0.0; $tot7 = 0.0;
						$daoLA = new LineaAccionDaoJdbc();
						$lista = $daoLA->obtieneListado($_GET['periodo']);
						$elemento = new LineaAccion();
						$daoGP = new GestionPresupuestalDaoJdbc();
						$daogp2= $daoGP->obtieneLista();
						$elemento2 = new IngresosINBADaoJdbc();
						$inbabean=new IngresosINBA();
						$daoPatronato = new IngresosPatronatoDaoJdbc();
						
						
						$patronato = $daoPatronato->obtieneMontoProy($elemento->getId(), $_GET['periodo']);
						
						echo $inba ;
						echo $patronato;
						foreach($lista as $elemento){
							$autorizado = $elemento->getSaldoInicial();
							$porgestionar = $elemento->getSaldoAnterior();
							$gestionado = $autorizado - $porgestionar;
							$inba=  $elemento->getInba();
							$patronato= $elemento->getPatronato();
							$pagado = $daoGP->obtieneMontoPorEstatusProy($elemento->getId(), 4, $_GET['periodo']);

							$tot1 += $autorizado;
							$tot2 += $gestionado;
							$tot3 += $porgestionar;
							$tot4 += $pagado;
							$tot6 += $inba;
							$tot7 += $patronato;
					?>
                    <tr>
                    	<td><?php if($elemento->getId() == 48){?><img src="../img/cerrar.png" style="cursor:pointer" id="cerrar" name="cerrar" onClick="cambiaStatus(true, 26)" width="14" height="14"><img src="../img/abrir.png" style="display:none;cursor:pointer" id="abrir" name="abrir" onClick="cambiaStatus(false, 26)" width="14" height="14"> <?php }?><?php echo($elemento->getLineaAccion());?></td>
                      
					    <td align="right">$<?php echo(number_format($autorizado,2));?></td>
                        <td align="right">$<?php echo(number_format($gestionado,2));?></td>
                        <td align="right">$<?php echo(number_format($porgestionar,2));?></td>
                        <td align="right">$<?php echo(number_format($pagado,2));?></td>
                        <td align="right">$<?php echo(number_format($inba,2));?></td>
                        <td align="right">$<?php echo(number_format($patronato,2));?></td>
                    </tr>
                    <?php	if($elemento->getId() == 48){
								$daoET = new ExposicionesTemporalesDaoJdbc();
								$lista2 = $daoET->getListado();
								$exposicion = new ExposicionesTemporales();
								$i = 1;
								foreach	($lista2 as $exposicion){
									$autorizado = $exposicion->getSaldo();
									$porgestionar = $exposicion->getSaldoAnterior();
									$gestionado = $autorizado - $porgestionar;
									$inba=$exposicion->getInba();
									$patronato=$exposicion->getPatronato();
			
									
					?>
                    <tr style="display:none" id="expos<?php echo($i);?>">
                    	<td><?php echo($exposicion->getNombre());?></td>
                    	<td align="right">$<?php echo(number_format($autorizado,2));?></td>
                        <td align="right">$<?php echo(number_format($gestionado,2));?></td>
                        <td align="right">$<?php echo(number_format($porgestionar,2));?></td>
                        <td align="right">$<?php echo(number_format($pagado,2));?></td>
                        <td align="right">$<?php echo(number_format($inba,2));?></td>
                        <td align="right">$<?php echo(number_format($patronato,2));?></td>
                    </tr>
                    <?php $i += 1;}}}?>		
					
					
                 
                	<tr>
                    	<td>Totales:</td>
                        <td align="right">$<?php echo(number_format($tot1,2));?></td>
                        <td align="right">$<?php echo(number_format($tot2,2));?></td>
                        <td align="right">$<?php echo(number_format($tot3,2));?></td> 
                        <td align="right">$<?php echo(number_format($tot4,2));?></td>
                        <td align="right">$<?php echo(number_format($tot6,2));?></td>
                        <td align="right">$<?php echo(number_format($tot7,2));?></td>
                    </tr>   
                </tbody>
            </table>
            
        </div>
        <script>
            function cambiaStatus(id, valor) {
                if(id == true){
					$("#cerrar").hide();
					$("#abrir").show();
					for(i = 1; i <= valor; i++){
						$("#expos" + i).show();
					}
				}else{
					$("#cerrar").show();
					$("#abrir").hide();
					for(i = 1; i <= valor; i++){
						$("#expos" + i).hide();
					}				
				}
            }

            
        </script>
    </body>
</html>