<?php

ob_start();


	include_once("../src/mx/com/virreinato/beans/LineaAccion.class.php");
	include_once("../src/mx/com/virreinato/dao/LineaAccionDaoJdbc.class.php");
	include_once("../src/mx/com/virreinato/beans/GestionPresupuestal.class.php");
	include_once("../src/mx/com/virreinato/dao/GestionPresupuestalDaoJdbc.class.php");
	include_once("../src/mx/com/virreinato/beans/ExposicionesTemporales.class.php");
	include_once("../src/mx/com/virreinato/dao/ExposicionesTemporalesDaoJdbc.class.php");
	include_once("../src/mx/com/virreinato/beans/Area.class.php");
	include_once("../src/mx/com/virreinato/dao/AreaDaoJdbc.class.php");
	include_once("../src/mx/com/virreinato/beans/Empleado.class.php");
	include_once("../src/mx/com/virreinato/dao/EmpleadoDaoJdbc.class.php");
	if(isset($_GET['idG'])){
		$idGestion = $_GET['idG'];
		$GPdao = new GestionPresupuestalDaoJdbc();
		$LAdao = new LineaAccionDaoJdbc();
		$ETdao = new ExposicionesTemporalesDaoJdbc();
		$Areadao = new AreaDaoJdbc();
		$GP = new GestionPresupuestal();
		$GP = $GPdao->obtieneDetalleSolicitud($idGestion);
		//Para generar el reporte
		require "fpdf.php";
		$pdf = new FPDF('P', 'mm', 'Letter');
		$pdf->SetMargins(20, 10);
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetTextColor(0x00, 0x00, 0x00);
		$pdf->SetFont('Arial', '', 9);
		$pdf->Cell(0, 3, $pdf->Image("../img/4.png"), 0, 1, 'L');
		$pdf->Cell(0, 0, "Formato de solicitud de pagos PATRONATO con cargo a PROYECTO", 0, 2, 'C');$pdf->Ln(2);
		$pdf->SetFont('Arial', '', 5);
		$pdf->Cell(0, 0, "FOLIO: " . $GP->getFolio(), 0, 0, 'R'); $pdf->Ln(2);
		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(20, 0, utf8_decode("Marca con una X el Proyecto y el Área solicitante."), 0, 1, "L");
		$pdf->SetFont('Arial', '', 5);
		$pdf->Cell(0, 0, "Fecha: " . $GP->getFechaSol(), 0, 1, "R");
		/*****/
		$pdf->SetFont('Arial', '', 8);$pdf->SetTextColor(255, 255, 255);$pdf->SetDrawColor(0,0,0);$pdf->Ln(4);
		$pdf->Cell(60, 6, "PROYECTO", 0, 0, "C", true);
		//$pdf->Cell(10, 0, "", 0, 0, 'C');
		$pdf->Cell(5, 0, "", 0, 0, 'C');
		$pdf->Cell(50,6, "EXPOSICIONES TEMPORALES", 0, 0, "C", true);
		$pdf->Cell(5, 0, "", 0, 0, 'C');
		$pdf->Cell(55, 6, utf8_decode("ÁREA SOLICITANTE"), 0, 1, "C", true);
		$pdf->SetTextColor(0, 0, 0);
		/***************/
		$pdf->SetFont('Arial', '', 6);
		$listaLA = $LAdao->obtieneListado();
		$listaET = $ETdao->getListado();
		$listaArea = $Areadao->obtieneAreasFiltro();
		$numLA = count($listaLA); $numET = count($listaET); $numArea = count($listaArea);
		$numerote = max($numLA, $numET, $numArea);
		$lineaAccion = new LineaAccion();
		$expoTemp = new ExposicionesTemporales();
		$areaP = new Area();

		$i = 0;
		for($i = 0; $i < $numerote ; $i += 1){
			if($i < $numLA){
				$lineaAccion = $listaLA[$i];
				$pdf->Cell(55, 3, utf8_decode($lineaAccion->getLineaAccion()), 1, 0, "C");
				if($GP->getIdProyecto() == $lineaAccion->getId()) $pdf->Cell(5, 3, "X", 1, 0, "C");
				else $pdf->Cell(5, 3, "", 1, 0, "C");
			}else{
				$pdf->Cell(55, 3, "", 0, 0, "C");
				$pdf->Cell(5, 3, "", 0, 0, "C");
			}
			$pdf->Cell(5, 0, "", 0, 0, 'C');
			if($i < $numET){
				$expoTemp = $listaET[$i];
				$pdf->Cell(45, 3, utf8_decode($expoTemp->getNombre()), 1, 0, "C");
				if($GP->getIdExpoTemp() == $expoTemp->getIdExposicionT()) $pdf->Cell(5, 3, "X", 1, 0, "C");
				else $pdf->Cell(5, 3, "", 1, 0, "C");
			}else{
				$pdf->Cell(45, 3, "", 0, 0, "C");
				$pdf->Cell(5, 3, "", 0, 0, "C");
			}
			$pdf->Cell(5, 0, "", 0, 0, 'C');
			if($i < $numArea){
				$areaP = $listaArea[$i];
				$pdf->Cell(50, 3, utf8_decode($areaP->getDescripcion()), 1, 0, "C");
				if($GP->getIdArea() == $areaP->getId()) $pdf->Cell(5, 3, "X", 1, 1, "C");
				else $pdf->Cell(5, 3, "", 1, 1, "C");
			}else{
				$pdf->Cell(50, 3, "", 0, 0, "C");
				$pdf->Cell(5, 3, "", 0, 1, "C");
			}
		}
		/******************/
		$lineaAccion = $LAdao->obtieneElemento($GP->getIdProyecto());
		$saldoAutorizado = $lineaAccion->getSaldoInicial();
		$SaldoAnterior = $lineaAccion->getSaldoAnterior();
		if($GP->getIdProyecto() == 48){
			$expoTemp = $ETdao->getElemento($GP->getIdExpoTemp());
			$saldoAutorizado = $expoTemp->getSaldo();
			$SaldoAnterior = $expoTemp->getSaldoAnterior();
		}
		$saldoAAntetior = $SaldoAnterior + $GP->getImporte();
		$pdf->SetFont('Arial', '', 8);$pdf->Ln(5);
		$pdf->Cell(90, 5, "Presupuesto Autorizado: $", 0, 0, "R");
		$pdf->Cell(90, 5, $saldoAutorizado, 0, 1, "L");
		$pdf->Cell(90, 5, "Saldo Anterior: $", 0, 0, "R");
		$pdf->Cell(90, 5, $saldoAAntetior, 0, 1, "L");
		$pdf->Cell(90, 5, "Menos (importe de la factura): $", 0, 0, "R");
		$pdf->Cell(90, 5, $GP->getImporte(), 0, 1, "L");
		$pdf->Cell(90, 5, "Saldo: $", 0, 0, "R");
		$pdf->Cell(90, 5, $SaldoAnterior, 0, 1, "L");
		$pdf->Ln(10);
		/**************************/
		$areaP = $Areadao->obtieneArea($GP->getIdArea());
		$nombreArea = $areaP->getDescripcion();
		$Empdao = new EmpleadoDaoJdbc();
		$Empleado = new Empleado();
		$Empleado = $Empdao->obtieneElemento($areaP->getIdLider());
		$pdf->Cell(90, 5, "SOLICITANTE:", 0,0, "R");
		$pdf->Cell(90, 5, utf8_decode($Empleado->getNombre() . " " . $Empleado->getApPaterno() . " " . $Empleado->getApMaterno()), 0,1, "L");
		$Empleado = $Empdao->obtieneElemento($lineaAccion->getIdLider());
		$pdf->Cell(90, 5, "LIDER DE PROYECTO:", 0,0, "R");
		$pdf->Cell(90, 5, utf8_decode($Empleado->getNombre() . " " . $Empleado->getApPaterno() . " " . $Empleado->getApMaterno()), 0,1, "L");
		$pdf->Cell(90, 5, "META:", 0,0, "R");
		$pdf->Cell(90, 5, utf8_decode($GP->getMeta()), 0,1, "L");
		$pdf->Cell(90, 5, "SUBMETA:", 0,0, "R");
		$pdf->Cell(90, 5, utf8_decode($GP->getSubmeta()), 0,1, "L");
		$pdf->Cell(90, 5, utf8_decode("NÚMERO DE FACTURA:"), 0,0, "R");
		$pdf->Cell(90, 5, utf8_decode($GP->getNumFactura()), 0,1, "L");
		$pdf->Cell(90, 5, "CONCEPTO DE LA FACTURA:", 0,0, "R");
		$pdf->Cell(90, 5, utf8_decode($GP->getConceptoFactura()), 0,1, "L");
		$pdf->Cell(90, 5, "NOMBRE DEL BENEFICIARIO:", 0,0, "R");
		$pdf->Cell(90, 5, utf8_decode($GP->getBeneficiario()), 0,1, "L");
		$pdf->Cell(90, 5, "IMPORTE Y MONEDA:", 0,0, "R");
		$pdf->Cell(90, 5, utf8_decode($GP->getImporteLetra()), 0,1, "L");
		/*FIRMAS*/
		$pdf->Cell(70, 25, $pdf->Image($lineaAccion->getFirma(), $pdf->GetX() + 15, $pdf->GetY(), NULL, 25), 0, 0, 'C');
		$pdf->Cell(40, 25, "", 0, 0, 'C');
		$pdf->Cell(70, 25, $pdf->Image($areaP->getFirma(), $pdf->GetX() + 15, $pdf->GetY(), NULL, 25), 0, 1, 'C');
		$pdf->Cell(70, 5, utf8_decode("LÍDER DEL PROYECTO"), "T", 0, 'C');
		$pdf->Cell(40, 5, "", 0, 0, 'C');
		$pdf->Cell(70, 5, utf8_decode("LÍDER DE ÁREA"), "T", 1, 'C');
		$areaP = $Areadao->obtieneArea(6);//Se ingresa el id de Subdirección Técnica
		$pdf->Cell(70, 25, $pdf->Image($areaP->getFirma(), $pdf->GetX() + 15, $pdf->GetY(), NULL, 25), 0, 0, 'C');
		$pdf->Cell(40, 25, "", 0, 0, 'C');
		$areaP = $Areadao->obtieneArea(109);//Se ingresa el id de Control Presupuestas
		$pdf->Cell(70, 25, $pdf->Image($areaP->getFirma(), $pdf->GetX() + 15, $pdf->GetY(), NULL, 25), 0, 1, 'C');
		$pdf->Cell(70, 5, utf8_decode("SUBDIRECCIÓN TÉCNICA"), "T", 0, 'C');
		$pdf->Cell(40, 5, "", 0, 0, 'C');
		$pdf->Cell(70, 5, utf8_decode("CONTROL PRESUPUESTAL"), "T", 1, 'C');

		//Salida
		/*
		I: envía el fichero al navegador de forma que se usa la extensión (plug in) si está disponible. El nombre dado en nombre se usa si el usuario escoge la opción "Guardar como..." en el enlace que genera el PDF.
		D: envía el fichero al navegador y fuerza la descarga del fichero con el nombre especificado por nombre.
		F: guarda el fichero en un fichero local de nombre nombre.
		S: devuelve el documento como una cadena. nombre se ignora.
		*/
		$ruta = "../GestionPresupuesto/Solicitudes/" . $nombreArea . "noFolio".$GP->getFolio() . ".pdf";
		$pdf->Output("F", $ruta);
		$GP->setReporte($ruta);
		$GPdao->actualizaElemento($GP);
		$respuesta = "Se ha aceptado la solicitud. El reporte se ha generado con éxito.";
	}else{
		$respuesta = "Se ha aceptado la solicitud.";
	}
	header("Location:lista_GestArch.php?respuesta=".$respuesta);
	
	ob_end_flush();
?>


