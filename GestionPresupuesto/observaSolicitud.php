<?php
session_start();
include_once("../src/mx/com/virreinato/dao/GestionPresupuestalDaoJdbc.class.php");
include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/GestionPresupuestal.class.php");
include_once("../src/classes/Catalogo.class.php");
$idGestion = $_GET['idG'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Nueva Gestión</title>
</head>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<?php
$catalogo = new Catalogo();
$dao = new GestionPresupuestalDaoJdbc();
$gestion = $dao->obtieneDetalleSolicitud($idGestion);
//$select = "SELECT MAX(gp.folio) AS noFolio FROM sie_cat_gestpresup gp, sie_cat_areas a WHERE gp.id_area = a.car_id_area";
?>
<body>
	<div class="contenido"> 
        <p class="titulo_cat1">Gestión de Presupuesto > Observa Solicitud Gestión</p>
        <form name="gestion" id="gestion" method="post" action="insertaNvaGestion.php" enctype="multipart/form-data">
            <table width="55%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
            	<tr>
                	<td colspan="2" align="center"><h3>Solicitud de Gestión</h3></td>
    			</tr>
                <tr>
                	<td width="20%">Folio:</td>
                    <td><?php echo($gestion->getFolio());?></td>
                </tr>
                <tr>
                	<td width="20%">Fecha de solicitud:</td>
                    <td><?php echo($gestion->getFechaSol());?></td>
                </tr>
                <tr>
                	<td width="20%">Proyecto:</td>
                    <td>
                    <?php 
						$query = "SELECT cla_lineaaccion FROM sie_cat_linea_accion WHERE cla_id_lineaaccion = " . $gestion->getIdProyecto();
						$resul = $catalogo->obtenerLista($query);
						if($rs = mysql_fetch_array($resul))echo($rs[0]);
					?>
                    </td>
                </tr>
                <tr>
                	<td width="20%">Exposición Temporal:</td>
                    <td>
                    <?php 
						if(!is_null($gestion->getIdExpoTemp())){
							$query = "SELECT nombre FROM sie_cat_expo_temp WHERE id_Subproyecto = " . $gestion->getIdExpoTemp();
							$resul = $catalogo->obtenerLista($query);
							if($rs = mysql_fetch_array($resul))echo($rs[0]);
						}else{
							echo("-");	
						}
						
					?>
                    </td>
                </tr>
                <tr>
                	<td width="20%">Área:</td>
                    <td>
					<?php 
                        $query = "SELECT car_area, presupuesto_inicial, saldo FROM cat_areas WHERE car_id_area = " . $gestion->getIdArea();
						$resul = $catalogo->obtenerLista($query);
						if($rs = mysql_fetch_array($resul)){
							$nomArea = $rs[0];	
							$presup = $rs[1];
							$saldo = $rs[2];
						}echo($nomArea);    
                        
					    if ($gestion->getIdProyecto() == 48)
						{
							$query = "SELECT saldo, saldoAnterior  FROM sie_cat_expo_temp WHERE id_Subproyecto = " . $gestion->getIdExpoTemp();
							$resul = $catalogo->obtenerLista($query);
							if($rs = mysql_fetch_array($resul))
							{
								$saldoinicialp = $rs[0];	
								$saldoantep = $rs[1];	
							}
						}else{
							
							$query = "SELECT saldoInicial, saldoAnterior  FROM sie_cat_linea_accion WHERE CLA_ID_LINEAACCION =  " . $gestion->getIdProyecto();
							$resul = $catalogo->obtenerLista($query);
							if($rs = mysql_fetch_array($resul))
							{
								$saldoinicialp = $rs[0];	
								$saldoantep = $rs[1];	
							}
							
						}
                    ?>
                    </td>
                </tr>
                <tr>
                	<td width="20%">Presupuesto del proyecto:</td>
                    <td>$<?php echo(number_format($saldoinicialp)); ?></td>
                </tr>                
                <tr>
                	<td width="20%">Importe de Factura:</td>
                    <td>$<?php echo(number_format($gestion->getImporte()));?></td>
                </tr>
                <tr>
                	<td width="20%">Saldo actual (considerando esta solicitud):</td>
                    <td>$<?php echo(number_format($saldoantep));?></td>
                </tr>
                <tr>
                	<td width="20%">Meta:</td>
                    <td><?php echo($gestion->getMeta());?></td>                    
                </tr>
                <tr>
                	<td width="20%">Submeta:</td>
                    <td><?php echo($gestion->getSubmeta());?></td>                    
                </tr>
                <tr>
                	<td width="20%">Número de factura:</td>
                    <td><?php echo($gestion->getNumFactura());?></td>                    
                </tr>
                <tr>
                	<td width="20%">Concepto de la factura:</td>
                    <td><?php echo($gestion->getConceptoFactura());?></td>                    
                </tr>
                <tr>
                	<td width="20%">Nombre del beneficiario:</td>
                    <td><?php echo($gestion->getBeneficiario());?></td>                    
                </tr>
                <tr>
                	<td width="20%">Importe y moneda:</td>
                    <td><?php echo($gestion->getImporteLetra());?></td>                    
                </tr>
                <tr>
                	<td width="20%">Entregable:</td>
                	<td><?php echo("<a href='".$gestion->getEntregable()."'>Ver el entregable</a>");?></td>
                 </tr>
                 <tr>
                	<td width="20%">Descripción del entregable:</td>
                    <td><?php echo($gestion->getEntregableDesc());?></td>                    
                </tr> 
                <tr>
                	<td width="20%">Documento de la Gestión:</td>
                    <td><?php if(is_null($gestion->getReporte())) echo ("No disponible hasta que Control Presupuestal apruebe."); else echo("<a href='".$gestion->getReporte()."'>Ver el reporte</a>");?></td>
                </tr>
                <!--tr>
                	<!?php
					if($_SESSION['id'] == 8){
					?>
                    <td colspan="2" align="center"><a href="creaGestionArchivo.php?idG=<!?php echo($gestion->getId());?>">Crear Archivo</a></td>
                    <!?php
					}
					?>
                </tr-->
            </table>
        </form>
    </div>
    <div class="footer" align='center'> 
<a href='javascript:window.history.back();' class='liga_btn'> Regresar </a>
</div>
</body>
<!--script>
 var frmvalidator  = new Validator("gestion");
 frmvalidator.addValidation("proyecto", "dontselect=0", "Por favor seleccione un proyecto.");
 frmvalidator.addValidation("area", "dontselect=0", "Por favor seleccione una área.");
 frmvalidator.addValidation("importeF", "req", "Por favor ingrese el importe de la factura.");
 frmvalidator.addValidation("importeF", "num", "El importe de la factura debe ser númerico.");
 frmvalidator.addValidation("importeF", "leelmnt=saldoA", "El importe de la factura no debe ser mayor al saldo anterior.");
 frmvalidator.addValidation("nofactura", "req", "Por favor ingrese el número de la factura.");
 frmvalidator.addValidation("conceptoFact", "req", "Por favor ingrese el concepto de la factura.");
 frmvalidator.addValidation("beneficiario", "req", "Por favor ingrese el nombre del beneficiario.");
 frmvalidator.addValidation("importe", "req", "Por favor ingrese el importe en letra de la factura.");
 frmvalidator.addValidation("entregable", "req", "Por favor seleccione el entregable de esta solicitud.");

</script-->
</html>