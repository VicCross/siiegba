<?php
session_start();
include_once("../src/classes/Catalogo.class.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/bootstrap.min.css">

<link rel="stylesheet" href="css/theme.bootstrap.css">
<title>Nueva Gestión</title>
</head>
<script>
	function mostrarPresupuesto()
	{
		mostrarExpoTemp();
		var proy = $("#proyecto").val();
		datos = "idProy="+proy;
		if(proy == 48){			
			var expot = $("#expotemp").val();
			datos += "&idET="+expot;	
		}
		$.ajax({
		 	url: "../src/mx/com/virreinato/web/nvaGestion.php",
		 	type: "POST",
		 	data: datos+"&opc=1",//opc 1: Mostrar Presupuesto
		 	success: function(data){ $("#presupuesto").html(data);}
		})
		$.ajax({
			url: "../src/mx/com/virreinato/web/nvaGestion.php",
		 	type: "POST",
		 	data: datos+"&opc=2",//opc 2: Mostrar Saldo Anterior
		 	success: function(data){ $("#saldoA").html(data);}
		})
		 //mostrarSaldo(id);
	}
	
	/*function mostrarSaldo(id)
	{
		$.ajax({
		 	 url: "../src/mx/com/virreinato/web/nvaGestion.php",
		 	 type: "POST",
		 	 data: "idArea="+id+"&opc=2",//opc 1: Mostrar Presupuesto
		 	 success: function(data){ $("#saldoA").html(data);}
		 })
	}*/
	
	function mostrarExpoTemp()
	{
		var id = $("#proyecto").val();
		if(id == 48){
			$("#expotemp").show();	
		}else{
			$("#expotemp").hide();
		}		
	}
</script>
<script language="JavaScript" type="text/javascript" src="../js/jquery-1.7.2.js" ></script>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script language="JavaScript" type="text/javascript" src="../js/gen_validatorv4.js" ></script>
<?php
$catalogo = new Catalogo();
//$select = "SELECT MAX(gp.folio) AS noFolio FROM sie_cat_gestpresup gp, sie_cat_areas a WHERE gp.id_area = a.car_id_area";
?>
<body>

 <div class ="container">
 
	<!--<div class="contenido" > -->
        <p class="titulo_cat1">Gestión de Presupuesto > Nueva Gestión</p>
        <form name="gestion" id="gestion" method="post" action="insertaNvaGestion.php" enctype="multipart/form-data">
		<div class="table-responsive">
            <table width="55%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center' >
            	<tr>
                	<td colspan="2" align="center"><h3>Nueva Gestión</h3></td>
    			</tr>
                <tr>
                	<td width="20%">Proyecto*:</td>
                    <td>
                    	<select name="proyecto" id="proyecto" onchange="mostrarPresupuesto()">
                        	<option value="0">----Selecciona un Proyecto----</option>
                            <?php
                            $select = "SELECT CLA_ID_LINEAACCION, CLA_LINEAACCION FROM sie_cat_linea_accion WHERE CLA_ESTATUS = 1 ORDER BY cla_id_lineaaccion";
                            $result = $catalogo->obtenerLista($select);
                            while($rs = mysql_fetch_array($result)){
                            ?>
                            <option value="<?php echo($rs['CLA_ID_LINEAACCION']);?>"><?php echo($rs['CLA_LINEAACCION']);?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                	<td width="20%">Exposición Temporal:</td>
                    <td>
                    	<select name="expotemp" id="expotemp" style="display:none;" onchange="mostrarPresupuesto()">
                            <?php
                            $select = "SELECT id_Subproyecto, nombre FROM sie_cat_expo_temp ORDER BY id_Subproyecto";
                            $result = $catalogo->obtenerLista($select);
                            while($rs = mysql_fetch_array($result)){
                            ?>
                            <option value="<?php echo($rs['id_Subproyecto']);?>"><?php echo($rs['nombre']);?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                	<td width="20%">Presupuesto Autorizado:</td>
                    <td><select name="presupuesto" id="presupuesto"></select></td>
                </tr>
                <tr>
                	<td width="20%">Saldo Anterior:</td>
                    <td><select name="saldoA" id="saldoA"></select></td>
                </tr>
                <tr>
                	<td width="20%">Área*:</td>
                    <td>
                    	<select name="area" id="area" onchange="mostrarPresupuesto()">
                            <option value="0">----Selecciona tu área----</option>
                            <?php
                            $select = "SELECT car_id_area, car_area FROM cat_areas WHERE car_estatus = 1 AND id_lider = " . $_SESSION['idEmpleado'] . "  AND (car_id_area = 1 OR car_id_area = 4 OR car_id_area = 6 OR car_id_area = 7 OR car_id_area = 8 OR car_id_area = 9 OR car_id_area = 10 OR car_id_area = 12 OR car_id_area = 13 OR car_id_area = 14 OR car_id_area = 16 OR car_id_area = 44) ORDER BY car_id_area";
                            $result = $catalogo->obtenerLista($select);
                            while($rs = mysql_fetch_array($result)){
                            ?>
                            <option value="<?php echo($rs['car_id_area']);?>"><?php echo($rs['car_area']);?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                	<td width="20%">Importe de Factura*:</td>
                    <td><input type="text" name="importeF" id="importeF"/></td>
                </tr>
                <!--tr>
                	<td width="20%">Saldo:</td>
                    <td><input type="text" id="saldo"/></td>
                </tr-->
                <tr>
                	<td width="20%">Meta:</td>
                    <td><input type="text" name="meta" id="meta" size="50"/></td>                    
                </tr>
                <tr>
                	<td width="20%">Submeta:</td>
                    <td><input type="text" name="submeta" id="submeta" size="50"/></td>                    
                </tr>
                <tr>
                	<td width="20%">Número de factura*:</td>
                    <td><input type="text" name="nofactura" id="nofactura" size="10" maxlength="10"/></td>                    
                </tr>
                <tr>
                	<td width="20%">Concepto de la factura*:</td>
                    <td><input type="text" name="conceptoFact" id="conceptoFact" size="60"/></td>                    
                </tr>
                <tr>
                	<td width="20%">Nombre del beneficiario*:</td>
                    <td><input type="text" name="beneficiario" id="beneficiario" size="50" /></td>                    
                </tr>
                <tr>
                	<td width="20%">Importe y moneda*:</td>
                    <td><input type="text" name="importe" id="importe" size="50"/></td>                    
                </tr>
                <tr>
                	<td width="20%">Entregable* (Menor a 2MB):</td>
                	<td><input name="entregable" type="file" id="entregable"/></td>
                 </tr>
                 <tr>
                	<td width="20%">Descripción del entregable:</td>
                    <td><input type="text" name="descEntregable" id="descEntregable" size="80" /></td>                    
                </tr> 
				
				
				
                     
				
				
				
				<br>
				<br>
				<tr>
                
				
				
				<!--se separa el enviar de guardar  -->
                	<td colspan="2" align="center"><input type="submit" value="Guardar" style="cursor:pointer" class='btn'/></td>
					
					
                </tr>
            </table>
			</div>
			<div class="table-responsive">
			<table width="55%" border="0" cellspacing="5" cellpadding="1" class='tb_add_cat' align='center'>
			
			<tr>
				  
			        <td><h2>Datos Para Facturar</h2></td>
					
			<tr>				
				<td>Instituto Nacional de Bellas Artes y Literatura INB 470101FA5 Av. Juárez Num. 101 Col. Centro CP 06040, Delegación Cuauhtemoc, México D.F</td> <br>
             </tr>
             <tr>				   
			    <td>Amigos del Museo del Palacio de Bellas Artes, A.C. AMP 890926GH2 Av. Juárez 4 Piso 10 Col. Centro, CP 06000,  Delegación Cuauhtemoc, México D.F</td>
              
			 </tr>   
			
			
					
			        
                </tr>         
			
			
			</table>
			<br><br>
        </form>
    </div>
	<!--</div> -->
	</div>

	 <div align="center"> <a href='../aplicaciones.php' class='liga_btn'>Regresar</a> </div>
	
    <script src="js/jquery.js"></script>
<script src="js/table.js"></script>
<script src="js/bootstrap.min.js"></script>

<!-- tablesorter plugin -->
<script src="js/jquery.tablesorter.js"></script>
<!-- tablesorter widget file - loaded after the plugin -->
<script src="js/jquery.tablesorter.widgets.js"></script>

<!-- pager plugin -->
<link rel="stylesheet" href="css/jquery.tablesorter.pager.css">
<script src="js/jquery.tablesorter.pager.min.js"></script>

	
</body>
<script>
 var frmvalidator  = new Validator("gestion");
 frmvalidator.addValidation("proyecto", "dontselect=0", "Por favor seleccione un proyecto.");
 frmvalidator.addValidation("area", "dontselect=0", "Por favor seleccione una área.");
 frmvalidator.addValidation("importeF", "req", "Por favor ingrese el importe de la factura.");
 frmvalidator.addValidation("importeF", "num", "El importe de la factura debe ser númerico.");
 frmvalidator.addValidation("importeF", "leelmnt=saldoA", "El importe de la factura no debe ser mayor al saldo anterior. SALDO INSUFICIENTE");
 frmvalidator.addValidation("nofactura", "req", "Por favor ingrese el número de la factura.");
 frmvalidator.addValidation("conceptoFact", "req", "Por favor ingrese el concepto de la factura.");
 frmvalidator.addValidation("beneficiario", "req", "Por favor ingrese el nombre del beneficiario.");
 frmvalidator.addValidation("importe", "req", "Por favor ingrese el importe en letra de la factura.");
 frmvalidator.addValidation("entregable", "req", "Por favor seleccione el entregable de esta solicitud.");
 //frmvalidator.addValidation("entregable", "maxlen=10", "El tamaño del entregable debe ser menor a 2MB.");
 //frmvalidator.addValidation("txtNumSolicitud","req","Por favor capture el Número de solicitud");
 /*Calendar.setup({ inputField : "txtFecha", ifFormat : "%d-%m-%Y", button: "txtFecha" });
 
 if( $("#periodo").val() != 0 ){
    var idProyecto = '<!?php echo$elemento->getIdProyecto()?>'
    Add_Periodo( idProyecto );

   
    Add_Mes( idProyecto,$("#mes").val());

    $("#periodo").attr('disabled','disabled');
    $("#Proyecto").attr('disabled','disabled');
    $("#mesFondos").attr('disabled','disabled');
    $("#Maestro").show();
 	 
 	 
 } */

</script>
</html>