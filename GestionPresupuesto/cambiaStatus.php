<?php 
	session_start();
	include_once("../src/mx/com/virreinato/dao/GestionPresupuestalDaoJdbc.class.php");
	include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/GestionPresupuestal.class.php");
	include_once("../src/mx/com/virreinato/dao/LineaAccionDaoJdbc.class.php");
	include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/LineaAccion.class.php");
	include_once("../src/mx/com/virreinato/dao/ExposicionesTemporalesDaoJdbc.class.php");
	include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/ExposicionesTemporales.class.php");
	include_once("../src/mx/com/virreinato/dao/AreaDaoJdbc.class.php");
	include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/Area.class.php");
	include_once("../src/classes/Catalogo.class.php");
	if(isset($_GET['opc']) && isset($_GET['idG'])){
		$dao = new GestionPresupuestalDaoJdbc();
		$gp = new GestionPresupuestal();
		$gp = $dao->obtieneDetalleSolicitud($_GET['idG']);
		//$operacion = "";
		if($_GET['opc'] == 'true'){
			//$operacion = "+ 1 ";
			$gp->setEstatus(1 + $gp->getEstatus());
		}else if($_GET['opc'] == 'false'){
			//$operacion = "+ 4 ";	
			$gp->setEstatus(4 + $gp->getEstatus());
		}
		$res = $dao->actualizaElemento($gp);
		if($res){
			/*Si Control Presupuestal ha dado el Vo. Bo. entonces los saldos de área aumenta y los proyectos disminuyen.*/
			if($_GET['opc'] == 'true' && $_SESSION['id'] == 8){
				/*Proyectos anuales*/
				$dao = new LineaAccionDaoJdbc();
				$proy = new LineaAccion();
				$proy = $dao->obtieneElemento($gp->getIdProyecto());	
				
				//$proy->setSaldoAnterior($proy->getSaldoAnterior() - $gp->getImporte());
				
				
				$res = $dao->actualizaSaldos($proy);
				/*Exposición Temporal*/
				if(!is_null($gp->getIdExpoTemp())){
					$dao = new ExposicionesTemporalesDaoJdbc();
					$expot = new ExposicionesTemporales();
					$expot = $dao->getElemento($gp->getIdExpoTemp());
					//$expot->setSaldoAnterior($expot->getSaldoAnterior() - $gp->getImporte());
					
					$dao->actualizaSaldos($expot);
				}
				/*Área*/
				$dao = new AreaDaoJdbc();
				$area = new Area();
				$area = $dao->obtieneArea($gp->getIdArea());
				$area->setPresupuesto($area->getPresupuesto() + $gp->getImporte());
				$area->setSaldo($area->getSaldo() + $gp->getImporte());
				$dao->actualizaArea($area);
				header("Location: creaGestionArchivo.php?idG=".$_GET['idG']);
				$respuesta = "A ver que ondirri";
			}else{
				$respuesta = "La solicitud ha cambiado con éxito de estatus.";
				header("Location: lista_GestArch.php?respuesta=".$respuesta);
			}
		}else{
			$respuesta = "El estatus de la solicitud no pudo ser cambiado con éxito.";
			header("Location: lista_GestArch.php?respuesta=".$respuesta);
		}
	}else{
		$respuesta = "Error. No se especifico que operación o solicitud aceptar/rechazar.";	
		header("Location: lista_GestArch.php?respuesta=".$respuesta);
	}
	
	//header("Location: lista_GestArch.php?respuesta=".$respuesta);

?>