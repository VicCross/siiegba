<?php
	session_start();
	include_once($_SESSION['RAIZ'] . "/src/mx/com/virreinato/beans/GestionPresupuestal.class.php");
	include_once($_SESSION['RAIZ'] . "/src/classes/Catalogo.class.php");
	class GestionPresupuestalDaoJdbc{
		public function obtieneLista(){
			$lista = array();
			$idPerfil = $_SESSION['id'];
			$query = "SELECT * FROM sie_cat_gestpresup ORDER BY fechaSol, id_area, folio";	
			$catalogo = new Catalogo();
			$result = $catalogo->obtenerLista($query);
			while($rs = mysql_fetch_array($result)){
				$elemento = new GestionPresupuestal();
				$elemento->setAll($rs[0],$rs[1],$rs[2],$rs[3],$rs[4],$rs[5],$rs[6],$rs[7],$rs[8],$rs[9],$rs[10],$rs[11],$rs[12],$rs[13],$rs[14],$rs[15],$rs[16]);
				array_push($lista, $elemento);	
			}
			return $lista;
		}
		
		public function obtieneListaSolicitudes(){
			$lista = array();
			$idPerfil = $_SESSION['id'];
			$query = "";
			//Encargado de área o admin general
			if($idPerfil == 5 || $idPerfil == 1){
				$query = "SELECT gp.* FROM sie_cat_gestpresup gp, cat_areas a WHERE a.id_lider = " . $_SESSION['idEmpleado'] . " AND a.car_id_area = gp.id_area ORDER BY gp.fechaSol, gp.id_area, gp.folio";	
			}else if($idPerfil == 9){//Líder del proyecto
				$query = "SELECT gp.* FROM sie_cat_gestpresup gp, sie_cat_linea_accion a WHERE a.id_lider = " . $_SESSION['idEmpleado'] . " AND a.cla_id_lineaaccion = gp.id_proyecto AND gp.estatus = 1 ORDER BY gp.fechaSol, gp.id_area, gp.folio";
			}else if($idPerfil == 3){//Subdirección técnica
				$query = "SELECT * FROM sie_cat_gestpresup WHERE estatus = 2 ORDER BY fechaSol, id_area, folio";
			}else if($idPerfil == 8){
				$query = "SELECT * FROM sie_cat_gestpresup WHERE estatus = 3 ORDER BY fechaSol, id_area, folio";	
			}
			$catalogo = new Catalogo();
			$result = $catalogo->obtenerLista($query);
			while($rs = mysql_fetch_array($result)){
				$elemento = new GestionPresupuestal();
				$elemento->setAll($rs[0],$rs[1],$rs[2],$rs[3],$rs[4],$rs[5],$rs[6],$rs[7],$rs[8],$rs[9],$rs[10],$rs[11],$rs[12],$rs[13],$rs[14],$rs[15],$rs[16]);
				array_push($lista, $elemento);	
			}
			return $lista;
		}
		
		public function obtieneDetalleSolicitud($id){
			$catalogo = new Catalogo();
			$query = "SELECT * FROM sie_cat_gestpresup WHERE id_GestPresup = " . $id;	
			$result = $catalogo->obtenerLista($query);
			$elemento = new GestionPresupuestal();
			if($rs = mysql_fetch_array($result)){
				$elemento->setAll($rs[0],$rs[1],$rs[2],$rs[3],$rs[4],$rs[5],$rs[6],$rs[7],$rs[8],$rs[9],$rs[10],$rs[11],$rs[12],$rs[13],$rs[14],$rs[15],$rs[16]);
			}
			return $elemento;
		}
		
		public function obtieneDetalleSolicitudesRechaz(){
			$lista = array();
			$idPerfil = $_SESSION['id'];
			$query = "";
			if($idPerfil == 9){//Líder del proyecto
				$query = "SELECT gp.* FROM sie_cat_gestpresup gp, sie_cat_linea_accion a WHERE a.id_lider = " . $_SESSION['idEmpleado'] . " AND a.cla_id_lineaaccion = gp.id_proyecto AND gp.estatus = 5 ORDER BY gp.fechaSol, gp.id_area, gp.folio";
			}else if($idPerfil == 3){//Subdirección técnica
				$query = "SELECT * FROM sie_cat_gestpresup WHERE estatus = 6 ORDER BY fechaSol, id_area, folio";
			}else if($idPerfil == 8){//Control presupuestal
				$query = "SELECT * FROM sie_cat_gestpresup WHERE estatus = 7 ORDER BY fechaSol, id_area, folio";	
			}
			$catalogo = new Catalogo();
			$result = $catalogo->obtenerLista($query);
			while($rs = mysql_fetch_array($result)){
				$elemento = new GestionPresupuestal();
				$elemento->setAll($rs[0],$rs[1],$rs[2],$rs[3],$rs[4],$rs[5],$rs[6],$rs[7],$rs[8],$rs[9],$rs[10],$rs[11],$rs[12],$rs[13],$rs[14],$rs[15],$rs[16]);
				array_push($lista, $elemento);	
			}
			return $lista;
		}
		
		public function obtieneDetalleSolicitudesAcept(){
			$lista = array();
			$idPerfil = $_SESSION['id'];
			$query = "";
			if($idPerfil == 9){//Líder del proyecto
				$query = "SELECT gp.* FROM sie_cat_gestpresup gp, sie_cat_linea_accion a WHERE a.id_lider = " . $_SESSION['idEmpleado'] . " AND a.cla_id_lineaaccion = gp.id_proyecto AND gp.estatus BETWEEN 2 AND 4 ORDER BY gp.fechaSol, gp.id_area, gp.folio";
			}else if($idPerfil == 3){//Subdirección técnica
				$query = "SELECT * FROM sie_cat_gestpresup WHERE estatus BETWEEN 3 AND 4 ORDER BY fechaSol, id_area, folio";
			}else if($idPerfil == 8){//Control presupuestal
				$query = "SELECT * FROM sie_cat_gestpresup WHERE estatus = 4 ORDER BY fechaSol, id_area, folio";	
			}
			$catalogo = new Catalogo();
			$result = $catalogo->obtenerLista($query);
			while($rs = mysql_fetch_array($result)){
				$elemento = new GestionPresupuestal();
				$elemento->setAll($rs[0],$rs[1],$rs[2],$rs[3],$rs[4],$rs[5],$rs[6],$rs[7],$rs[8],$rs[9],$rs[10],$rs[11],$rs[12],$rs[13],$rs[14],$rs[15],$rs[16]);
				array_push($lista, $elemento);	
			}
			return $lista;
		}
		
		public function actualizaElemento($elemento){
			$catalogo = new Catalogo();
			if(is_null($elemento->getReporte())) $reporte = "null";
			else $reporte = "'" . $elemento->getReporte() . "'";
			if(is_null($elemento->getIdExpoTemp())) $expotemp = "null";
			else $expotemp = "'" . $elemento->getIdExpoTemp() . "'";
			$update = "UPDATE sie_cat_gestpresup SET folio = " . $elemento->getFolio() . ", id_area = " . $elemento->getIdArea() . ", id_proyecto = " . $elemento->getIdProyecto() . ", id_expotemp = " . $expotemp . ", estatus = " . $elemento->getEstatus() . ", importe = " . $elemento->getImporte() . ", meta = '" . $elemento->getMeta() . "', submeta = '" . $elemento->getSubmeta() . "', numFactura = '" . $elemento->getNumFactura() . "', conceptoFactura = '" . $elemento->getConceptoFactura() . "', beneficiario = '" . $elemento->getBeneficiario() . "', importeLetra = '" . $elemento->getImporteLetra() . "', entregable = '" . $elemento->getEntregable() . "', entregable_desc = '" . $elemento->getEntregableDesc() . "', reporte = " . $reporte . " WHERE id_GestPresup = " . $elemento->getId();
			$res = $catalogo->obtenerLista($update);
			if($res == "1") return true;
			else return false;
		}
		
		public function obtieneMontoPorEstatusProy($periodo){
			$catalogo = new Catalogo();     
			$select = "SELECT SUM(gp.importe) FROM sie_cat_gestpresup gp, sie_cat_periodos p WHERE gp.PERIODO = " . $periodo;
			$res = $catalogo->obtenerLista($select);
			
			return false;
		}
		
		public function obtieneMontoPorEstatusExpos($idET, $estatus, $periodo){
			$catalogo = new Catalogo();
			$select = "SELECT SUM(gp.importe) FROM sie_cat_gestpresup gp, sie_cat_periodos p WHERE gp.id_expotemp = " . $idET . " AND p.CPE_ID_PERIODO = " . $periodo . " AND gp.estatus = " . $estatus . " AND gp.fechaSol BETWEEN p.CPE_FECHA_INI AND p.CPE_FECHA_FIN";
			$res = $catalogo->obtenerLista($select);
			
			return false;
		}
		
	}
?>